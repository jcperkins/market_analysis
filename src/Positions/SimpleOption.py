# -----------------------------------------------------------------------------
# Name:        OptionPositins
# Purpose:     File for gathering and organizing classes of option position
#               types
#
# Author:      Cory Perkins
#
# Created:     19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------
import numpy as np
import pandas as pd
from scipy import stats
from src.DataSource import OptionData
import src.Utility as Util
from src.types import RawDateType, RawDateSingle
from typing import Optional


class Option(object):
    def __init__(
        self,
        tckr: str,
        strike: float,
        exDate: RawDateSingle,
        contractQty: int,
        optionType: str,
        modelingType: Optional[str] = "BS",
        contractSize: Optional[int] = 100,
        positionName: Optional[str] = None,
        optionDB: Optional[OptionData] = None,
        source: Optional[str] = "storage",
    ):
        self.optionType = optionType.upper()
        self.tckr = Util.convertTckrToUniversalFormat(tckr)
        self.qty = contractQty * contractSize
        self.contractSize = contractSize
        self.contracts = contractQty
        self.strike = strike
        self.exDate = Util.formatDateInput(exDate)
        self.modelingType = modelingType
        self.source = source.upper()
        if positionName is None:
            self.name = tckr.replace("ix", "") + "%02i%02i%02i%s%01i" % (
                self.exDate.year % 100,
                self.exDate.month,
                self.exDate.day,
                self.optionType[0],
                strike,
            )
        else:
            self.name = positionName
        if optionDB is None:
            self.optionDB = OptionData()
        else:
            self.optionDB = optionDB
        self.stockDB = self.optionDB.stockDB
        self.liborDB = self.optionDB.liborDB

    def calculateValue(self, date, price=None):
        if self.modelingType == "BS":
            return self.calculateValue_BS(date)
        else:
            return self.calculateValue_Excised(date, price=price)

    def calculateValue_Excised(self, date, price=None):
        if price is None:
            price = self.getUnderlyingPrice(date)
        if self.optionType[0] == "C":
            value = price - self.strike
        else:
            value = self.strike - price
        if type(value) in [list, np.ndarray, pd.Series, pd.DataFrame]:
            value[value < 0] = 0
        elif (
            np.number in type(value).mro()
            or float in type(value).mro()
            or int in type(value).mro()
        ):
            value = max(value, 0)
        return value * self.qty

    def calculateValueOfContract_Excised(self, date, price=None):
        if price is None:
            price = self.getUnderlyingPrice(date)
        if self.optionType[0] == "C":
            value = price - self.strike
        else:
            value = self.strike - price
        if (
            np.number in type(value).mro()
            or float in type(value).mro()
            or int in type(value).mro()
        ):
            value = max(value, 0)
        elif type(value) in [list, np.ndarray, pd.Series, pd.DataFrame]:
            value[value < 0] = 0
        return value * 100

    def calculateValue_BS(self, date):
        return (
            self.optionDB.PriceEst_BS(
                self.tckr,
                self.optionType,
                date,
                self.strike,
                self.exDate,
                source=self.source,
            )
            * self.qty
        )

    def calculateValueOfContract_BS(self, date):
        return (
            self.optionDB.PriceEst_BS(
                self.tckr, "Call", date, self.strike, self.exDate, source=self.source
            )
            * self.contractSize
        )

    def addQty(self, qty):
        self.qty += qty
        self.contracts += qty / self.contractSize
        return self.qty

    def addContracts(self, contracts):
        self.qty += contracts * self.contractSize
        self.contracts += contracts
        return self.qty

    def getUnderlyingPrice(self, date, adjusted=False):
        return self.stockDB.getPrice(
            self.tckr, date, adjusted=adjusted, source=self.source
        )

    def getValidStockRow(self, date):
        """
        if self.source is set to 'STORAGE' then return the last row from
        the db storage table, if not, then return the last date in
        temp_db.index <= date
        """
        return self.stockDB.getValidRows(self.tckr, date, source=self.source)

    def expectedValue_AtExpiration(self, date, predictionModel, stepCnt=500):
        minPrice, maxPrice = predictionModel.get_price_from_cum_prob(
            [0.001, 0.999],
            current_date=date,
            predicted_date=self.exDate,
            tckr=self.tckr,
            direction="Below",
            source=self.source,
        )
        priceStep = (maxPrice - minPrice) / stepCnt
        priceArray = np.arange(minPrice, maxPrice, priceStep)
        probabilityOfPrice = predictionModel.get_prob_of_price(
            priceArray,
            current_date=date,
            predicted_date=self.exDate,
            tckr=self.tckr,
            source=self.source,
        )
        profit = self.calculateValue_Excised(None, price=priceArray)
        expectedValue = (profit * probabilityOfPrice * priceStep).sum()
        return expectedValue

    def MaxRisk_NoPremiums(self, currentDate=None, price=None):
        if (self.optionType[0].upper() == "C" and self.qty > 0) or (
            self.optionType[0].upper() == "P" and self.qty < 0
        ):
            maxRiskBeforePremium = self.calculateValue(currentDate, price=price)
        else:  # (spreadType in ['Bearish Put Spread', 'Bullish Call Spread'):
            maxRiskBeforePremium = 0
        return maxRiskBeforePremium * self.qty


# *****************************************************************************
# ----------------------------  Begin Call Class ------------------------------
# *****************************************************************************


class Call(Option):
    optionType = "Call"

    def __init__(
        self,
        tckr,
        strike,
        exDate,
        contractQty,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        Option.__init__(
            self,
            tckr,
            strike,
            exDate,
            contractQty,
            self.optionType,
            modelingType=modelingType,
            contractSize=contractSize,
            positionName=positionName,
            optionDB=optionDB,
            source=source,
        )

    def MaxRisk_NoPremiums(self, currentDate=None, price=None):
        if self.qty > 0:
            maxRiskBeforePremium = self.calculateValue(currentDate, price=price)
        else:  # (spreadType in ['Bearish Put Spread', 'Bullish Call Spread'):
            maxRiskBeforePremium = 0
        return maxRiskBeforePremium * self.qty

    def expectedValue_AtExpiration(
        self,
        date: RawDateType,
        predictionMean: float,
        predictionStd: float,
        stepCnt: Optional[int] = 500,
    ):
        prediction = stats.norm(loc=predictionMean, scale=predictionStd)
        cumProbArray = np.linspace(0.001, 0.999, num=stepCnt)
        priceArray = prediction.ppf(cumProbArray)
        probabilityArray = prediction.pdf(priceArray)
        profitLoss = np.array(
            map(
                lambda price: self.calculateValue_Excised(date, price=price), priceArray
            )
        )
        expectedValue = (profitLoss * probabilityArray).sum()
        return expectedValue


# *****************************************************************************
# ---------------------------- Begin Put Class --------------------------------
# *****************************************************************************


class Put(Option):
    optionType = "Put"

    def __init__(
        self,
        tckr,
        strike,
        exDate,
        contractQty,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        Option.__init__(
            self,
            tckr,
            strike,
            exDate,
            contractQty,
            self.optionType,
            modelingType=modelingType,
            contractSize=contractSize,
            positionName=positionName,
            optionDB=optionDB,
            source=source,
        )

    def MaxRisk_NoPremiums(self, currentDate=None, price=None):
        if self.qty < 0:
            maxRiskBeforePremium = self.calculateValue(currentDate, price=price)
        else:  # (spreadType in ['Bearish Put Spread', 'Bullish Call Spread'):
            maxRiskBeforePremium = 0
        return maxRiskBeforePremium * self.qty

    def expectedValue_AtExpiration(
        self, date, predictionMean, predictionStd, stepCnt=500
    ):
        prediction = stats.norm(loc=predictionMean, scale=predictionStd)
        cumProbArray = np.linspace(0.001, 0.999, num=stepCnt)
        priceArray = prediction.ppf(cumProbArray)
        probabilityArray = prediction.pdf(priceArray)
        profitLoss = np.array(
            map(
                lambda price: self.calculateValue_Excised(date, price=price), priceArray
            )
        )
        expectedValue = (profitLoss * probabilityArray).sum()
        return expectedValue
