"""
-------------------------------------------------------------------------------
 Name:        VerticalSpread
 Purpose:      File dedicated to the vertical spread class and functions that
                 involve the vertical spread strategy

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd

# from scipy import stats
import time
import itertools
from src.DataSource import OptionData
from src import Model
from src.Positions import SimpleOption
from src.Positions.ComplexOption import ComplexOption
import pylab as pl
from numba import jit


class VerticalSpread(ComplexOption):
    optionType = "VerticalSpread"

    def __init__(
        self,
        tckr,
        strikes,
        spreadType,
        purchaseDate,
        exDate,
        contractQty,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        """
        *************************** Vertical spreads ************************
            Bullish Put/Call Spread        #       Bearish Put/Call Spread
                            Sell P.-0      #  Sell P.
                        X1---------        #  --------X0
                       /  Sell C.          #  Sell C.-0 \
            Buy P.   /                     #             \ Buy P.-0
            ---------X0                    #             X1---------
            Buy C.-0                       #                   Buy C.
                                           #
        Bullish Call requires money        # Bearish Call provides a credit
            upfront (negative premium)     #    upfront (positive premium)
            but has no future liability    #    but has future liabilities
        Bullish Put provides a credit      # Bearish Put requires money
            upfront (positive premium)     #    upfront (negative premium)
            but has future liabilities     #    but has no future liability
        max loss call = premiums paid      # max loss call = X1 - X0 + prem
        max loss put  = X1 - X0 + prem     # max loss put  = premiums paid
        $ = np.clip( price, X0, X1)        # $ = np.clip( price, X0, X1)
        Value call = $ - X0 + prem = VBuC  # Value call = X0 - $ + prem = VBeC
        Value put = $ - X1 + prem = VBuP   # Value put = X1 - $ + prem = VBeP
                        VBuC = -1*VBeC; VBuP = -1*VBeP

        o  strikes are the two strike amounts for the vertical spread
        o  spreadType is one of ['Bullish Put Spread', 'Bearish Put Spread',
            'Bullish Call Spread', 'Bearish Call Spread']
        o  contractQty is the number of Vertical Spread contracts to buy and
            not necessarily the number of calls or puts that you have in
            each position
        o  everything else should be explanatory
        """
        self.strikes = strikes
        self.strikes.sort()
        self.exDate = exDate
        self.spreadType = spreadType.title()
        if optionDB is None:
            self.optionDB = OptionData()
        else:
            self.optionDB = optionDB
        if spreadType[0].upper() == "C":
            self.optionInit = SimpleOption.Call
        else:
            self.optionInit = SimpleOption.Put
        lowStrikeQty, highStrikeQty = {
            "Bullish Put Spread": (-1, 1),
            "Bearish Put Spread": (1, -1),
            "Bullish Call Spread": (1, -1),
            "Bearish Call Spread": (-1, 1),
        }[spreadType.title()]
        self.positions = []
        if "Call" in self.spreadType:
            calls, puts = self.positions, []
        else:
            calls, puts = [], self.positions
        self.positions.append(
            self.optionInit(
                tckr,
                strikes[0],
                exDate,
                lowStrikeQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="VS_%s_1" % spreadType.title(),
                optionDB=self.optionDB,
                source=source,
            )
        )
        self.positions.append(
            self.optionInit(
                tckr,
                strikes[1],
                exDate,
                highStrikeQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="VS_%s_1" % spreadType.title(),
                optionDB=self.optionDB,
                source=source,
            )
        )

        if positionName is None:
            positionName = tckr + "_%s" % self.spreadType
        ComplexOption.__init__(
            self,
            tckr,
            calls,
            puts,
            contractQty,
            self.optionType,
            modelingType=modelingType,
            contractSize=contractSize,
            positionName=positionName,
            optionDB=optionDB,
            source=source,
        )
        self.positions = self.calls if "Call" in self.spreadType else self.puts

    def calculateValue_Excised(self, date, price=None):
        price = price if price is not None else self.getUnderlyingPrice(date)
        limitedPrice = np.clip(price, self.strikes[0], self.strikes[1])
        if self.spreadType == "Bullish Put Spread":
            value = limitedPrice - self.strikes[1]
        elif self.spreadType == "Bearish Put Spread":
            value = self.strikes[1] - limitedPrice
        elif self.spreadType == "Bullish Call Spread":
            value = limitedPrice - self.strikes[0]
        elif self.spreadType == "Bearish Call Spread":
            value = self.strikes[0] - limitedPrice
        return value

    @classmethod
    def monte_carlo_single_date(
        cls,
        tckr,
        purchaseDate,
        exDate,
        offeringCode,
        predictionModel,
        strategy="Vertical Spread",
        current_price=None,
        symmetry=False,
        n_mc=100,
        cum_cycles=20,
        max_risk=0.2,
        lifetime=1,
        strikes=None,
        prices=None,
        return_all_data=False,
        optimize=True,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        source="storage",
    ):
        """
        run full monte carlo simulation for the given option strategy and
        expiration date.

        Args:
            tckr - the name of the underlying security
            spreadtype - 'Call' or 'Put'
            outlook - 'Bull' (positive) or 'Bear' (negative)
            purchaseDate - the last date that the functions will pull
                historical data for
            exDate - If a single date then call the appropriate function for
                        the date.
                     If a range of dates, then find the optimal
                        option for each date.
                     If 'Estimate' then estimate based on the range determined
                        by the offering code
                     If 'Fidelity' then pull all the exDates available in the
                        fidelity options DB for the give purchase date. If no
                        date matches then default to 'Estimate'
            offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
                whether to pull strikes for a weekly, monthly, quarterly,
                or yearly offering. The offering code is returned from the
                optionDB.findClosestExDate function call.
            predictionModel - if None then function will use the default
                prediction model in the MarketPredictions module
            lifetime - the fraction of the option lifetime that the option will
                be held before it is sold. If 'expire' or 1 then hold the
                option until it expires
            n_mc - the number of times each option combination is modeled
            cum_cycles - the number of loops to test the option combination
                so that the true return can be approximated if you have both
                good and bad prices
            max_risk - the maximum amount of the portfolio that will be risked
                in the option strategies
            total_pf - value of the total portfolio that the strategy will be
                applied too
            broker_fee - the price of the first option contract
                (or stock purchase) charged by the broker for the transaction
            addtl_option_fee - the additional fee charged by the broker
                for each contract after the initial contract
            return_all_data - Default is False. If true then return a tuple of
                the best option and a pd.DataFrame of the info for every option
            source - where to pull the stock data. options are 'storage' or
                'temp_db'

        tckr = '.SPX'
        spreadType = 'PUT'
        outlook = 'BULL'
        predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
        optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
        purchaseDate = pd.datetime(2006, 6, 20)
        exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                            purchaseDate + pd.Timedelta(days=90))
        exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                            purchaseDate + pd.Timedelta(days=30))
        currentPrice = None
        contractQty = 1
        expValStpCnt = 500
        modelingType = 'BS'
        contractSize = 100
        positionName = None
        source = 'storage'
        symmetry=False
        max_risk=0.2
        n_mc = 1000
        cum_cycles = 20
        lifetime='EXP'
        strikeArray=None
        priceArray_buy=None
        priceArray_sell=None
        """
        start = time.time()
        if predictionModel is None:
            predictionModel = Model.getDefaultPredictionModel_HDF5()
        optionDB = OptionData(stockDB=predictionModel.stockDB)
        if current_price is None:
            current_price = predictionModel.get_current_price(purchaseDate, tckr)
        positions, cashflow_sign, quantity = cls.interpret_strategy(strategy)
        if positions[0] == "Call":
            spreadType = "CALL"
            if cashflow_sign[0] == -1:
                outlook = "BULL"
                strategy = "Bullish Call Spread"
            else:
                outlook = "BEAR"
                strategy = "Bearish Call Spread"
        else:
            spreadType = "PUT"
            if cashflow_sign[0] == -1:
                outlook = "BULL"
                strategy = "Bullish Put Spread"
            else:
                outlook = "BEAR"
                strategy = "Bearish Put Spread"
        # gather all possible strikes
        daysOut = (exDate - purchaseDate).days
        if type(lifetime) == str or lifetime >= 1:
            sellDate = exDate
            sigma = rate = None
        else:
            sellDate = purchaseDate + pd.Timedelta(days=int(daysOut * lifetime))
            sigma = (
                optionDB.stockDB.PredictSPXVol_1m3mVIX(
                    purchaseDate, exDate, source=source
                )
                / 100.0
            )
            rate = (
                optionDB.liborDB.getLIBOR_Rate(purchaseDate, daysOut, source=source)
                / 100.0
            )

        days_invested = (sellDate - purchaseDate).days
        # get price prediction
        dist = predictionModel.get_prob_dist(purchaseDate, sellDate, tckr)
        predicted_price = dist.mean()
        predicted_std = dist.std()

        strikes, prices = cls.format_strikes_prices(
            positions,
            cashflow_sign,
            quantity,
            strike_list=strikes,
            price_list=prices,
            optionDB=optionDB,
            purchaseDate=purchaseDate,
            exDate=exDate,
            current_price=current_price,
            offeringCode=offeringCode,
            sigma=sigma,
            rate=rate,
            tckr=tckr,
            source=source,
        )

        legs = len(strikes)
        # Remove buy prices greater than -$0.01 and sell prices less than $0.01
        i_leg = 0
        while True:
            if prices[i_leg].sum() <= 0:
                selectBool = prices[i_leg] < -0.01
            else:
                selectBool = prices[i_leg] > 0.01
            strikes[i_leg] = strikes[i_leg][selectBool]
            prices[i_leg] = prices[i_leg][selectBool]
            i_leg += 1
            if i_leg >= legs:
                break

        # loop will reduce the strike combination counts until all of them are
        #  under the threshold
        strikes_prev = strikes
        prices_prev = prices
        strike_limit = 200
        while True:
            strikes_new, prices_new = cls.reduce_combinations_mc(
                positions,
                quantity,
                strikes_prev,
                prices_prev,
                strike_limit,
                dist,
                n_mc=n_mc,
                cum_cycles=cum_cycles,
                max_risk=max_risk,
                lifetime=1,
                symmetry=symmetry,
                tckr=tckr,
                sellDate=sellDate,
                exDate=exDate,
                sigma=sigma,
                rate=rate,
                optionDB=optionDB,
                total_pf=total_pf,
                broker_fee=broker_fee,
                addtl_option_fee=addtl_option_fee,
                return_all_data=False,
                source=source,
            )
            if np.any([x.size == 0 for x in strikes_new]):
                break
            elif np.all(
                [
                    np.all(strikes_new[j].size == strikes_prev[j].size)
                    and np.all(prices_new[j].size == prices_prev[j].size)
                    for j in range(legs)
                ]
            ):
                break
            else:
                strikes_prev, prices_prev = strikes_new, prices_new
        # strikes_r is the reduced strike lists and needs to have specific
        #  shapes based on the index position
        strikes_r = [
            strikes_prev[x_i].reshape(
                tuple([1] * x_i + [strikes_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        prices_r = [
            prices_prev[x_i].reshape(
                tuple([1] * x_i + [prices_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)

        # only keep combinations where the strikes are in sequential order
        selectBool = strikes_t[1] > strikes_t[0]
        # Only keep the IC possibilities that have a net premium > $0.03
        # If symmetry is enforced, remove the asymmetric possibilities
        selectBool = np.logical_and(selectBool, (prices_t[0] + prices_t[1]) > 0.03)
        valid_qty = selectBool.sum()
        strikes_t = np.concatenate(
            (
                strikes_t[0][selectBool].reshape(valid_qty, 1),
                strikes_t[1][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )
        prices_t = np.concatenate(
            (
                prices_t[0][selectBool].reshape(valid_qty, 1),
                prices_t[1][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )

        print("\tRunning full MC simulation")
        (
            total_returns,
            raw_volatility,
            win_perc,
            strikes_t,
            prices_t,
            selection_bridge,
        ) = cls.simulate_monte_carlo(
            positions,
            cashflow_sign,
            quantity,
            strikes_t,
            prices_t,
            dist,
            n_mc=n_mc,
            cycles=cum_cycles,
            max_risk=max_risk,
            lifetime=lifetime,
            tckr=tckr,
            sellDate=sellDate,
            exDate=exDate,
            sigma=sigma,
            rate=rate,
            optionDB=optionDB,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
            source=source,
        )
        valid_qty = strikes_t.shape[0]
        column_order = (
            [
                "Strategy",
                "Category",
                "Outcome_Qty",
                "Yield_Cycles",
                "PurchaseDate",
                "ExpirationDate",
                "DaysToExp",
                "DaysInvested",
                "Max_Risk",
                "CurrentPrice",
                "Prediction_Price",
                "Prediction_Std",
                "Total_PF",
                "Trade_Fee",
                "Addtl_Contract_Fee",
            ]
            + ["Strike_%1i" % j for j in range(legs)]
            + ["Price_%1i" % j for j in range(legs)]
            + [
                "Premium",
                "Max_Loss",
                "Max_Profit",
                "Median_PF_Return",
                "Mean_PF_Return",
                "Std_PF_Return",
                "Avg_PF_Return_PerDay",
                "Avg_Ann_Return",
                "Expected_Stock_Return",
                "AvgReturnVsStock",
                "Expected_Gain_Return",
                "Win_Percentage",
                "Ann_Volatility",
            ]
        )

        if strikes_t.size == 0:
            bestOption = pd.Series(index=column_order, name="Best " + strategy)
            bestOption["Strategy"] = strategy
            bestOption["Category"] = "Monte Carlo"
            bestOption["Outcome_Qty"] = n_mc
            bestOption["Yield_Cycles"] = cum_cycles
            bestOption["PurchaseDate"] = purchaseDate
            bestOption["ExpirationDate"] = exDate
            bestOption["DaysToExp"] = daysOut
            bestOption["DaysInvested"] = daysOut
            bestOption["Max_Risk"] = max_risk
            bestOption["CurrentPrice"] = current_price
            bestOption["Prediction_Price"] = dist.mean()
            bestOption["Prediction_Std"] = dist.std()
            bestOption["Total_PF"] = total_pf
            bestOption["Trade_Fee"] = broker_fee
            bestOption["Addtl_Contract_Fee"] = addtl_option_fee
            if return_all_data:
                options = pd.DataFrame(bestOption).T
                options.index = [0]
                return bestOption, options
            else:
                return bestOption
        # calculate the expected return of the option
        exp_return = cls.expected_gain(
            positions,
            quantity,
            strikes_t,
            prices_t,
            max_risk,
            dist,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
        )
        premium = cls.calc_premium(prices_t)
        init_invest = cls.calc_init_invest(positions, quantity, strikes_t, prices_t)
        max_profit = cls.calc_max_profit(positions, quantity, strikes_t, prices_t)

        pf_median, pf_mean, pf_stdev = total_returns
        ann_volatility = np.sqrt(
            (raw_volatility * raw_volatility).sum(-1) / raw_volatility.shape[-1]
        ) * np.sqrt(365 / float(daysOut))
        best_index = pf_mean.argmax()
        return_of_stock = predictionModel.expected_return_of_stock(
            tckr, purchaseDate, sellDate, source=source
        )
        bestOption = {
            "Strategy": strategy,
            "Category": "Monte Carlo",
            "Outcome_Qty": n_mc,
            "Yield_Cycles": cum_cycles,
            "PurchaseDate": purchaseDate,
            "ExpirationDate": exDate,
            "DaysToExp": daysOut,
            "DaysInvested": days_invested,
            "Max_Risk": max_risk,
            "CurrentPrice": current_price,
            "Prediction_Price": predicted_price,
            "Prediction_Std": predicted_std,
            "Total_PF": total_pf,
            "Trade_Fee": broker_fee,
            "Addtl_Contract_Fee": addtl_option_fee,
            "Premium": premium[best_index, 0],
            "Max_Loss": -1 * init_invest[best_index, 0],
            "Max_Profit": max_profit[best_index, 0],
            "Median_PF_Return": pf_median[best_index],
            "Mean_PF_Return": pf_mean[best_index],
            "Std_PF_Return": pf_stdev[best_index],
            "Avg_PF_Return_PerDay": pf_mean[best_index] ** (1.0 / days_invested),
            "Avg_Ann_Return": (pf_mean[best_index] ** (365.0 / days_invested)),
            "Expected_Stock_Return": return_of_stock + 1,
            "AvgReturnVsStock": (pf_mean[best_index] - 1) / (return_of_stock),
            "Expected_Gain_Return": exp_return[best_index, 0],
            "Win_Percentage": win_perc[best_index, 0],
            "Ann_Volatility": ann_volatility[best_index],
        }
        for i in range(legs):
            bestOption["Strike_%1i" % i] = strikes_t[best_index, i]
            bestOption["Price_%1i" % i] = prices_t[best_index, i]
        bestOption = pd.Series(data=bestOption)[column_order]
        bestOption.name = "Best " + strategy
        output = bestOption

        if return_all_data:
            # create the results pd.DF for comparison
            options = pd.DataFrame(index=range(valid_qty), columns=column_order)
            options["Strategy"] = strategy
            options["Category"] = "Monte Carlo"
            options["Outcome_Qty"] = n_mc
            options["Yield_Cycles"] = cum_cycles
            options["PurchaseDate"] = purchaseDate
            options["ExpirationDate"] = exDate
            options["DaysToExp"] = daysOut
            options["DaysInvested"] = daysOut
            options["Max_Risk"] = max_risk
            options["CurrentPrice"] = current_price
            options["Prediction_Price"] = dist.mean()
            options["Prediction_Std"] = dist.std()
            options["Total_PF"] = total_pf
            options["Trade_Fee"] = broker_fee
            options["Addtl_Contract_Fee"] = addtl_option_fee
            options["Premium"] = premium
            options["Max_Loss"] = -1 * init_invest
            options["Max_Profit"] = max_profit
            options["Median_PF_Return"] = pf_median
            options["Mean_PF_Return"] = pf_mean
            options["Std_PF_Return"] = pf_stdev
            options["Avg_PF_Return_PerDay"] = pf_mean ** (1.0 / days_invested)
            options["Avg_Ann_Return"] = pf_mean ** (365.0 / daysOut)
            options["Expected_Stock_Return"] = return_of_stock + 1
            options["AvgReturnVsStock"] = (pf_mean - 1) / return_of_stock
            options["Expected_Gain_Return"] = exp_return
            options["Win_Percentage"] = win_perc
            options["Ann_Volatility"] = ann_volatility
            for i in range(legs):
                options["Strike_%1i" % i] = strikes_t[:, i]
                options["Price_%1i" % i] = prices_t[:, i]
            options = options[column_order]
            output = bestOption, options

        return output

    @classmethod
    @jit(nogil=True)
    def mc_calc_expiration(
        cls,
        positions,
        cashflow_sign,
        quantity,
        strikes,
        premium,
        init_invest,
        fees,
        rand_price_block,
        max_risk,
        record_cutoff=1e7,
        return_all_data=False,
    ):
        """
        strikes are a 2d array with each column representing the x0, x1, x2, x3
            strikes
        init_investment is the abs(max_loss) or the amount that could be lost
            in the transaction
        rand_price_block is the 2d array of random numbers in the shape
            (# of cycles to test the return, # of pf's to test per option)

        returns a tuple of the median, mean, and stdev of the returns for each
            option combination
        """
        cycles, pf_cnt = rand_price_block.shape
        valid_qty = strikes.shape[0]
        record_cnt = cycles * pf_cnt * valid_qty
        outlook = "Bull" if cashflow_sign[0] == -1 else "Bear"
        if record_cnt < record_cutoff:
            premium = premium.reshape((valid_qty, 1, 1))
            init_invest = init_invest.reshape((valid_qty, 1, 1))
            fees = fees.reshape((valid_qty, 1, 1))
            strikes = strikes.reshape((valid_qty, 2, 1))
            rand_price_block = rand_price_block.T.reshape((1, pf_cnt, cycles))
            # minimum of the profit from the low side and the high side spreads
            # two methods
            if outlook == "Bull":
                profit = (
                    premium
                    - fees
                    - strikes[:, [1], :]
                    + np.clip(rand_price_block, strikes[:, [0], :], strikes[:, [1], :])
                )
            else:
                profit = (
                    premium
                    - fees
                    + strikes[:, [0]]
                    - np.clip(rand_price_block, strikes[:, [0], :], strikes[:, [1], :])
                )
            win_qty = (profit > 0).sum(axis=-1).sum(axis=-1).reshape(valid_qty, 1)
            portfolio_return = 1 + max_risk * (profit / init_invest)
            raw_volatility = portfolio_return.std(axis=-1)
            portfolio_return = portfolio_return.prod(axis=-1)
        else:
            premium = premium.reshape((valid_qty, 1))
            init_invest = init_invest.reshape((valid_qty, 1))
            fees = fees.reshape((valid_qty, 1))
            portfolio_return = np.empty((valid_qty, pf_cnt))
            portfolio_return.fill(1)
            win_qty = np.zeros_like(init_invest)
            raw_volatility = []
            loop_cnt = 0
            while True:
                rand_price = rand_price_block[loop_cnt, :]
                # minimum of the profit from the low side and the high
                #  side spreads two methods
                if outlook == "Bull":
                    profit = (
                        premium
                        - fees
                        - strikes[:, [1]]
                        + np.clip(rand_price, strikes[:, [0]], strikes[:, [1]])
                    )

                else:
                    profit = (
                        premium
                        - fees
                        + strikes[:, [0]]
                        - np.clip(rand_price, strikes[:, [0]], strikes[:, [1]])
                    )
                win_qty += (profit > 0).sum(axis=-1).reshape(valid_qty, 1)

                loop_return = 1 + max_risk * (profit / init_invest)
                portfolio_return = portfolio_return * loop_return
                raw_volatility.append(loop_return)
                loop_cnt += 1
                if loop_cnt >= cycles:
                    break
            raw_volatility = np.std(raw_volatility, axis=0)

        portfolio_return = portfolio_return ** (1.0 / cycles)
        if return_all_data:
            return (portfolio_return, raw_volatility, win_qty / float(cycles * pf_cnt))
        else:
            raw_volatility = np.sqrt((raw_volatility * raw_volatility).sum(axis=-1))
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
                raw_volatility.reshape(valid_qty, 1),
                win_qty / float(cycles * pf_cnt),
            )

    @classmethod
    def mc_calc_sell(
        cls,
        strikes,
        premium,
        init_invest,
        rand_price_block,
        max_risk,
        tckr,
        sellDate,
        exDate,
        sigma,
        rate,
        optionDB,
        spreadType,
        outlook,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        """
        strikes are a 2d array with each column representing the x0, x1, x2, x3
            strikes
        init_investment is the abs(max_loss) or the amount that could be lost
            in the transaction
        rand_price_block is the 2d array of random numbers in the shape
            (# of cycles to test the return, # of portfolios to test per option)

        returns a tuple of the median, mean, and stdev of the returns for each
            option combination
        """
        cycles, pf_cnt = rand_price_block.shape
        valid_qty = strikes.shape[0]
        premium_sq = np.tile(premium, pf_cnt).reshape((valid_qty, pf_cnt))
        init_invest_sq = np.tile(init_invest, pf_cnt).reshape((valid_qty, pf_cnt))
        portfolio_return = np.empty((valid_qty, pf_cnt))
        portfolio_return.fill(1)
        sell_sign = [1, -1] if outlook == "BULL" else [-1, 1]
        loop_cnt = 0
        while True:
            rand_price = rand_price_block[loop_cnt, :]
            # estimate of prices of the option strategy
            s_new_sq = optionDB.PriceEst_BS(
                tckr,
                spreadType,
                sellDate,
                np.concatenate((strikes[:, 0], strikes[:, 1]), axis=0).reshape(
                    2 * valid_qty, 1
                ),
                exDate,
                S0=rand_price,
                sigma=sigma,
                rate=rate,
                source=source,
            )
            sell_premium_sq = (
                sell_sign[0] * s_new_sq[:valid_qty, :]
                - sell_sign[1] * s_new_sq[valid_qty:, :]
            )
            net_profit_sq = premium_sq + sell_premium_sq
            portfolio_return = portfolio_return * (
                1 + max_risk * (net_profit_sq / init_invest_sq)
            )
            loop_cnt += 1
            if loop_cnt >= cycles:
                break
        portfolio_return = portfolio_return ** (1.0 / cycles)
        if return_all_data:
            return portfolio_return
        else:
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
            )

    @classmethod
    def expected_gain(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        max_risk,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        # outlook = 'Bull' if prices[:, 0].sum() < 0 else 'Bear'
        slope = 1 if prices[:, 0].sum() < 0 else -1
        premium = prices.sum(axis=1).reshape(strikes.shape[0], 1)
        if slope == 1:
            if premium[:5, 0].sum() > 0:
                # high side profit is premium
                profit = np.concatenate(
                    ((strikes[:, [0]] - strikes[:, [1]]) + premium, premium), axis=1
                )
            else:
                # low side profit is premium
                profit = np.concatenate(
                    (premium, (strikes[:, [1]] - strikes[:, [0]]) + premium), axis=1
                )
            init_invest = np.abs(profit[:, [0]])
        else:
            if premium[:5, 0].sum() > 0:
                # low side profit is premium
                profit = np.concatenate(
                    (premium, (strikes[:, [0]] - strikes[:, [1]]) + premium), axis=1
                )
            else:
                # high side profit is premium
                profit = np.concatenate(
                    ((strikes[:, [1]] - strikes[:, [0]]) + premium, premium), axis=1
                )
            init_invest = np.abs(profit[:, [1]])

        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_x = dist.cdf(strikes)
        pdf_x = dist.pdf(strikes)
        # exp profit = profit($<x0)*P($<x0) + profit(x0<$<x1)*P(x0<$<x1)
        #          + profit(x1<$<x2)*P(x1<$<x2) + profit(x2<$<x3)*P(x2<$<x3)
        #          + profit(x3<$)*P(x3<$)
        risk_over_invest = max_risk / init_invest
        exp_return_ltx0 = (1 + (risk_over_invest * profit[:, [0]])) * cdf_x[:, [0]]
        exp_return_gtx1 = (1 + (risk_over_invest * profit[:, [1]])) * (
            1 - cdf_x[:, [1]]
        )
        # calculate the expected price between x0 and x1
        # see block comments at start of function for deriv. of the eq. below.
        #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
        # = (1 + max_risk*(slope*(mean-x1) + high_x_profit))*(CDF(x1)-CDF(x0))
        #     - max_risk*slope*std^2 * (pdf(x1) - pdf(x0))
        exp_return_x0x1 = (
            1 + risk_over_invest * (slope * (mean - strikes[:, [1]]) + profit[:, [1]])
        ) * (cdf_x[:, [1]] - cdf_x[:, [0]]) - risk_over_invest * slope * (std ** 2) * (
            pdf_x[:, [1]] - pdf_x[:, [0]]
        )
        # calculate the expected price between x2 and x3
        # combine segments
        exp_return = exp_return_ltx0 + exp_return_x0x1 + exp_return_gtx1

        return exp_return

    @classmethod
    def expected_profit(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        # outlook = 'Bull' if prices[:, 0].sum() < 0 else 'Bear'
        slope = 1 if prices[:, 0].sum() < 0 else -1
        premium = prices.sum(axis=1).reshape(strikes.shape[0], 1)
        if slope == 1:
            if premium[:5, 0].sum() > 0:
                # high side profit is premium
                profit = np.concatenate(
                    ((strikes[:, [0]] - strikes[:, [1]]) + premium, premium), axis=1
                )
            else:
                # low side profit is premium
                profit = np.concatenate(
                    (premium, (strikes[:, [1]] - strikes[:, [0]]) + premium), axis=1
                )
            init_invest = np.abs(profit[:, [0]])
        else:
            if premium[:5, 0].sum() > 0:
                # low side profit is premium
                profit = np.concatenate(
                    (premium, (strikes[:, [0]] - strikes[:, [1]]) + premium), axis=1
                )
            else:
                # high side profit is premium
                profit = np.concatenate(
                    ((strikes[:, [1]] - strikes[:, [0]]) + premium, premium), axis=1
                )
            init_invest = np.abs(profit[:, [1]])

        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_x = dist.cdf(strikes)
        pdf_x = dist.pdf(strikes)
        # exp profit = profit($<x0)*P($<x0) + profit(x0<$<x1)*P(x0<$<x1)
        #      + profit(x1<$<x2)*P(x1<$<x2) + profit(x2<$<x3)*P(x2<$<x3)
        #      + profit(x3<$)*P(x3<$)

        exp_profit_ltx0 = profit[:, [0]] * cdf_x[:, [0]]
        exp_profit_gtx1 = profit[:, [2]] * (1 - cdf_x[:, [1]])
        # calculate the expected price between x0 and x1
        # see block comments at start of function for deriv. of the eq. below.
        #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
        #   = (slope*(mean-x1) + high_x_profit)*(CDF(x1) - CDF(x0))
        #     + std^2 * (pdf(x1) - pdf(x0))
        exp_profit_x0x1 = (slope * (mean - strikes[:, [1]]) + profit[:, [1]]) * (
            cdf_x[:, [1]] - cdf_x[:, [0]]
        ) - slope * (std ** 2) * (pdf_x[:, [1]] - pdf_x[:, [0]])
        # calculate the expected price between x2 and x3
        # combine segments
        exp_profit = exp_profit_ltx0 + exp_profit_x0x1 + exp_profit_gtx1

        return exp_profit / init_invest

    @classmethod
    def calc_max_profit(
        cls, positions, quantity, strikes, prices, current_value=np.inf
    ):
        premium = cls.calc_premium(prices)
        if premium.sum() > 0:
            return premium
        else:
            return premium + strikes[:, 1] - strikes[:, 0]

    @classmethod
    def calc_init_invest(cls, positions, quantity, strikes, prices):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        init_invest = np.abs(
            strikes[:, [0]] - strikes[:, [1]] + cls.calc_premium(prices)
        )
        return init_invest


def createVerticalSpread_2Prob(
    tckr,
    probabilityLevels,
    spreadType,
    outLook,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    """
    Initializes a vertical spread option position based on the two
    probabilities indicated
    """
    assert len(probabilityLevels) >= 2, "Not enough probabilities provided"
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)

    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = [0, 0]
    for iPrice in range(len(estimatedStrikes)):
        closestStrikes[iPrice] = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes[iPrice],
            optionRange,
            "=",
            currentPrice,
        )
    closestStrikes.sort()
    return VerticalSpread(
        tckr,
        closestStrikes,
        spreadType,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def createVerticalSpread_1Prob(
    tckr,
    probabilityLevels,
    applyProb,
    spreadType,
    outLook,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    """
    Initializes a vertical spread option position with the upper or lower
    strike (depending on applyProb) and the remaining strike selected to be
    the next available strike

    applyProb is either 'High' or 'Low' and the larger or lower strike value
    will be assigned the strike value associated with the probability
    respectively
    """
    assert len(probabilityLevels) >= 1, "Not enough probabilities provided"
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = []
    # Find strike price associated with the probability
    strikeCondition = "="
    try:
        probabilityStrike = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes,
            optionRange,
            strikeCondition,
            currentPrice,
        )
    except Exception:
        probabilityStrike = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes.values[0],
            optionRange,
            strikeCondition,
            currentPrice,
        )
        estimatedStrikes = estimatedStrikes.values[0]
    closestStrikes.append(probabilityStrike)
    strikeCondition = "<" if applyProb.upper() == "HIGH" else ">"
    closestStrikes.append(
        optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes,
            optionRange,
            strikeCondition,
            currentPrice,
        )
    )
    closestStrikes.sort()
    return VerticalSpread(
        tckr,
        closestStrikes,
        spreadType,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def createVerticalSpread_HEVForDate(
    tckr,
    spreadType,
    outlook,
    currentDate,
    exDate,
    offeringCode,
    contractQty,
    predictionModel=None,
    expValStpCnt=500,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    max_risk=0.2,
    source="storage",
):
    """
    Finds the strike values for the highest expected value of a vertical
    spread if excised on the exDate predicted by the prediction model
    offeringCode is the optionRange returned by OptionData.getClosestSPXDate
    debug:
        downward trending date - pd.datetime(2015, 6, 20)

        tckr = '.SPX'
        spreadType = 'Put'
        outlook = 'BULL'
        predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
        optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
        current_date = pd.datetime(2006, 6, 20)
        exDate, offeringCode = optionDB.findClosestExDate(current_date,
                                            current_date+pd.Timedelta(days=90))
        contractQty = 1
        expValStpCnt = 500
        modelingType = 'BS'
        contractSize = 100
        positionName = None
        source = 'storage'
    """
    if predictionModel is None:
        predictionModel = Model.getDefaultPredictionModel_HDF5()
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    # Find the optimized spread on the low side of the IC
    #    sellPositionKey = {('CALL', 'BULL'): 'High',
    #                       ('CALL', 'BEAR'): 'Low',
    #                       ('PUT', 'BULL'): 'Low',
    #                       ('PUT', 'BEAR'): 'High'}
    spreadType = "CALL" if spreadType[0].upper() == "C" else "PUT"
    outlook = "BULL" if "BULL" in outlook.upper() else "BEAR"
    #    sellPosition = sellPositionKey[(spreadType, outlook)]

    optionInfo = find_VerticalSpread_HEV(
        tckr,
        spreadType,
        outlook,
        currentDate,
        exDate,
        offeringCode,
        predictionModel,
        max_risk,
        expValStpCnt=expValStpCnt,
        source=source,
    )

    return VerticalSpread(
        tckr,
        [optionInfo["Strike_0"], optionInfo["Strike_1"]],
        spreadType,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def find_VerticalSpread_HEV(
    tckr,
    spreadType,
    outlook,
    purchaseDate,
    exDate,
    offeringCode,
    predictionModel,
    max_risk,
    expValStpCnt=500,
    return_all_data=False,
    source="storage",
):
    """
    Uses the known shape of the value function at expiration to
    calculate the expected value of the option at expiration based on the
    predictions of the predictionModel provided. This method
    is slightly faster (30ms) than the approximate method and is a little
    more precise, but is rigid and requires the vertical spread value shape

    Args:
        tckr - underlying security
        spreadType -'Call' or 'Put'
        outlook - 'Bull' for positive or 'Bear' for negative
        purchaseDate - Date of the hypothetical purchase
        exDate - Date that the option is supposed to expire
        offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
            whether to pull strikes for a weekly, monthly, quarterly,
            or yearly offering. The offering code is returned from the
            optionDB.findClosestExDate function call.
        predictionModel - if None then function will use the default
            prediction model in the MarketPredictions module
        expValStpCnt - Used for the approximation method only
        return_all_data - Default is False. If true then return a tuple of
            the best option and a pd.DataFrame of the info for every option
        source - where to pull the stock data. options are 'storage' or
            'temp_db'

    Return:
        pd.Series or (pd.Series, pd.DataFrame) dependent on return_all_data

    *************************** Vertical spreads ************************
        Bullish Put/Call Spread        #       Bearish Put/Call Spread
                        Sell P.-0      #  Sell P.
                    X1---------        #  --------X0
                   /  Sell C.          #  Sell C.-0 \
        Buy P.   /                     #             \ Buy P.-0
        ---------X0                    #             X1---------
        Buy C.-0                       #                   Buy C.
                                       #
    Bullish Call requires money        # Bearish Call provides a credit
        upfront (negative premium)     #    upfront (positive premium)
        but has no future liability    #    but has future liabilities
    Bullish Put provides a credit      # Bearish Put requires money
        upfront (positive premium)     #    upfront (negative premium)
        but has future liabilities     #    but has no future liability
    max loss call = premiums paid      # max loss call = X1 - X0 + prem
    max loss put  = X1 - X0 + prem     # max loss put  = premiums paid
    $ = np.clip( price, X0, X1)        # $ = np.clip( price, X0, X1)
    Value call = $ - X0 + prem = VBuC  # Value call = X0 - $ + prem = VBeC
    Value put = $ - X1 + prem = VBuP   # Value put = X1 - $ + prem = VBeP
                    VBuC = -1*VBeC; VBuP = -1*VBeP

    signs will indicate case flow
    max loss will be negative signifying negative cash flow
    map profit will be positive signifying positive cash flow
    ---------------------------------------------------------------------
    exp profit = max loss * P(maxloss) + max profit * P(max profit)
                 + integral(profit between X0 and X1 * dist.pdf)

    profit for a price $ between strikes X0 and X1 =
        profit bull call = $ - X0 + prem = VBuC = m * ($ - X1) + max profit
        profit bear call = X0 - $ + prem = VBeC = m * ($ - X1) + max loss
        profit bull put = $ - X1 + prem = VBuP = m * ($ - X1) + max profit
        profit bear put = X1 - $ + prem = VBeP = m * ($ - X1) + max loss
            m = (max profit - max loss) / (X1 - X0) * (-1 if bear else 1)

    from wiki:
    The expectation of X conditioned on the event that X lies in an
    interval [a,b] is given by:
            E(X given a<X<b) = mean - std^2 * (f(b) - f(a)) / (F(b) - F(a))
    where f and F respectively are the density and the cumulative
    distribution function of X

    therefore E(X) bet. a and b but not guaranteed to be be between a and b
    E(x) = E(X given a<X<b) * P(a<X<b)
    E(x) = u*(F(b) - F(a)) + std^2 * (f(b) - f(a))


    integral(P(x)*dx from a to b) = integral(P(x')*dx' from a-u to b-u)
        =  CDF(b) - CDF(a)

    integral(x*P(x)*dx from a to b)
        = u * (CDF(b) - CDF(a)) - std**2 * (pdf(b) - pdf(a))

    example case of bull put option below
    profit($) = m * ($ - X1) + max profit

    integral(profit($) * dist.pdf * dx from X0 to X1)
        = integral(profit($) * P($) from X0 to X1)
        = integral((m * ($ - X1) + max profit) * P($) from X0 to X1)
        = integral((m*$*P($) - m*X1*P($) + max profit*P($))*d$ from X0 to X1)
        = m*u*(CDF(X1)-CDF(X0)) - std**2*(pdf(X1)-pdf(X0))
            - m*X1*(CDF(X1)-CDF(X0)) + max profit*(CDF(X1)-CDF(X0))
        = (m*(u-x1)+max profit)*(CDF(X1)-CDF(X0)) - std**2*(pdf(X1)-pdf(X0))

    debug:
    downward trending date - pd.datetime(2015, 6, 20)
    tckr = '.SPX'
    spreadType = 'PUT'
    outlook = 'BULL'
    predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
    optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
    purchaseDate = pd.datetime(2006, 6, 20)
    exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                        purchaseDate + pd.Timedelta(days=90))
    currentPrice = None
    contractQty = 1
    expValStpCnt = 500
    modelingType = 'BS'
    contractSize = 100
    positionName = None
    source = 'storage'
    """
    if predictionModel is None:
        predictionModel = Model.getDefaultPredictionModel_HDF5()
    optionDB = OptionData(stockDB=predictionModel.stockDB)
    currentPrice = predictionModel.get_current_price(purchaseDate, tckr)
    spreadType = "CALL" if spreadType[0].upper() == "C" else "PUT"
    outlook = "BULL" if "BULL" in outlook.upper() else "BEAR"
    # gather all possible strikes
    daysOut = (exDate - purchaseDate).days
    strikeArray = np.array(
        optionDB.estimateSPXStrikesOffered(
            currentPrice, daysOut, offeringCode=offeringCode
        )
    )
    priceEst_BS = optionDB.PriceEst_BS(
        tckr,
        spreadType,
        purchaseDate,
        strikeArray,
        exDate,
        S0=currentPrice,
        source=source,
    )
    # Remove prices less than $0.01
    selectBool = priceEst_BS > 0.01
    strikeArray = strikeArray[selectBool]
    priceEst_BS = priceEst_BS[selectBool]
    # get price prediction
    dist = predictionModel.get_prob_dist(purchaseDate, exDate, tckr)
    # default price signs are for the bullish call spread
    price_sign = np.array([-1, 1])
    if outlook == "BEAR":
        price_sign = -price_sign
    valid_strikes = np.broadcast_arrays(
        strikeArray.reshape((strikeArray.size, 1)),
        strikeArray.reshape((1, strikeArray.size)),
    )
    strike_prices = np.broadcast_arrays(
        priceEst_BS.reshape((priceEst_BS.size, 1)),
        priceEst_BS.reshape((1, priceEst_BS.size)),
    )
    # only keep strike combinations where x1 is higher than x0 and
    #  prices are more than $0.03 apart so that strategy has enough margin
    #  to at least be plausible in the real world
    selectBool = np.logical_and(
        valid_strikes[1] > valid_strikes[0],
        np.abs(strike_prices[0] - strike_prices[1]) > 0.03,
    )
    valid_qty = selectBool.sum()
    x0 = valid_strikes[0][selectBool]
    x1 = valid_strikes[1][selectBool]
    p0 = price_sign[0] * strike_prices[0][selectBool]
    p1 = price_sign[1] * strike_prices[1][selectBool]
    premium = p0 + p1

    if (spreadType, outlook) in [("CALL", "BULL"), ("PUT", "BEAR")]:
        max_loss = premium
        max_profit = np.abs(x0 - x1) + premium
    elif (spreadType, outlook) in [("CALL", "BEAR"), ("PUT", "BULL")]:
        max_loss = -np.abs(x0 - x1) + premium
        max_profit = premium
    else:
        assert spreadType in [
            "CALL",
            "PUT",
        ], "Spreadtype is not recognized. It must be 'CALL' or 'PUT'"
        assert outlook in [
            "BULL",
            "BEAR",
        ], "outlook is not recognized. It must be 'BULL' or 'BEAR'"
    # reference point is the (strike, profit) coordinates of one of the
    # strikes that will be used in the exp profit formula later
    if outlook == "BULL":
        high_X_profit = max_profit
        low_X_profit = max_loss
    elif outlook == "BEAR":
        high_X_profit = max_loss
        low_X_profit = max_profit

    slope = (high_X_profit - low_X_profit) / (x1 - x0)
    cdf_low = dist.cdf(x0)
    cdf_high = dist.cdf(x1)
    # exp profit = max loss * P(maxloss) + max profit * P(max profit)
    #             + integral(profit between X0 and X1 * dist.pdf)
    # high and low profits calculated next
    exp_profit = low_X_profit * cdf_low + high_X_profit * (1 - cdf_high)
    mean = dist.mean()
    std = dist.std()
    # see block comments at start of function for derivation of the eq. below.
    #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
    exp_profit_middle = (1 + max_risk * (slope * (mean - x1) + high_X_profit)) * (
        dist.cdf(x1) - dist.cdf(x0)
    ) - std ** 2 * (dist.pdf(x1) - dist.pdf(x0))
    exp_profit = exp_profit + exp_profit_middle
    exp_return = exp_profit / (-1 * max_loss)
    column_order = [
        "Strategy",
        "Type",
        "OutLook",
        "Category",
        "Data_Source",
        "PurchaseDate",
        "ExpirationDate",
        "Strike_0",
        "Strike_1",
        "Price_0",
        "Price_1",
        "Premium",
        "Max_Loss",
        "Max_Profit",
        "Expected_Profit",
        "Expected_Return",
        "Expected_Return_PerDay",
        "ReturnVsStock",
    ]
    bestIndex = exp_return.argmax()
    return_of_stock = predictionModel.expected_return_of_stock(
        tckr, purchaseDate, exDate, source=source
    )
    bestOption = {
        "Strategy": "VerticalSpread",
        "Type": spreadType,
        "OutLook": outlook,
        "Category": (outlook.title() + "ish " + spreadType.title() + " Spread"),
        "Data_Source": "BS",
        "PurchaseDate": purchaseDate,
        "ExpirationDate": exDate,
        "Strike_0": x0[bestIndex],
        "Strike_1": x1[bestIndex],
        "Price_0": p0[bestIndex],
        "Price_1": p1[bestIndex],
        "Premium": premium[bestIndex],
        "Max_Loss": max_loss[bestIndex],
        "Max_Profit": max_profit[bestIndex],
        "Expected_Profit": exp_profit[bestIndex],
        "Expected_Return": exp_return[bestIndex],
        "Expected_Return_PerDay": (exp_return[bestIndex] / daysOut),
        "ReturnVsStock": (exp_return[bestIndex] / return_of_stock),
    }
    bestOption = pd.Series(data=bestOption)[column_order]
    bestOption.name = "Best " + bestOption.Category
    output = bestOption

    if False:
        # create the results pd.DF for comparison
        column_order = [
            "Strategy",
            "Type",
            "OutLook",
            "Category",
            "Data_Source",
            "PurchaseDate",
            "ExpirationDate",
            "Strike_0",
            "Strike_1",
            "Price_0",
            "Price_1",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Expected_Profit",
            "Expected_Return",
            "Expected_Return_PerDay",
            "ReturnVsStock",
        ]
        options = pd.DataFrame(index=range(valid_qty), columns=column_order)
        options["Strategy"] = "VerticalSpread"
        options["Type"] = spreadType
        options["OutLook"] = outlook
        options["Category"] = outlook.title() + "ish " + spreadType.title() + " Spread"
        options["Data_Source"] = "BS"
        options["PurchaseDate"] = purchaseDate
        options["ExpirationDate"] = exDate
        options["Strike_0"] = x0
        options["Strike_1"] = x1
        # indexPrice, columnPrice = np.broadcast_arrays(strikes_0, strikes_1)
        options["Price_0"] = p0
        options["Price_1"] = p1
        options["Premium"] = premium
        options["Max_Loss"] = max_loss
        options["Max_Profit"] = max_profit
        options["Expected_Profit"] = exp_profit
        options["Expected_Return"] = exp_return
        options["Expected_Return_PerDay"] = exp_return / daysOut
        options["ReturnVsStock"] = exp_return / return_of_stock
        options = options[column_order]
        output = bestOption, options

    return output


def find_VerticalSpread_HEV_ApproxMethod(
    tckr,
    spreadType,
    outlook,
    purchaseDate,
    exDate,
    offeringCode,
    predictionModel,
    expValStpCnt=500,
    source="storage",
):
    """
    Finds the strike values for the highest expected value of a vertical
    spread if excised on the exDate predicted by the prediction model
    Uses the known shape of the value function at expiration to
    calculate the expected value of the option at expiration based on the
    predictions of the predictionModel provided. This method
    is slightly faster (30ms) than the approximate method and is a little
    more precise, but is rigid and requires the vertical spread value shape

    Args:
        tckr - underlying security
        spreadType -'Call' or 'Put'
        outlook - 'Bull' for positive or 'Bear' for negative
        purchaseDate - Date of the hypothetical purchase
        exDate - Date that the option is supposed to expire
        offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
            whether to pull strikes for a weekly, monthly, quarterly,
            or yearly offering. The offering code is returned from the
            optionDB.findClosestExDate function call.
        predictionModel - if None then function will use the default
            prediction model in the MarketPredictions module
        expValStpCnt - determine many price steps to calculate for the
            expected value
        return_all_data - Default is False. If true then return a tuple of
            the best option and a pd.DataFrame of the info for every option
        source - where to pull the stock data. options are 'storage' or
            'temp_db'

    Return:
        pd.Series or (pd.Series, pd.DataFrame) dependent on return_all_data

    *************************** Vertical spreads ************************
        Bullish Put/Call Spread        #       Bearish Put/Call Spread
                        Sell P.-0      #  Sell P.
                    X1---------        #  --------X0
                   /  Sell C.          #  Sell C.-0 \
        Buy P.   /                     #             \ Buy P.-0
        ---------X0                    #             X1---------
        Buy C.-0                       #                   Buy C.
                                       #
    Bullish Call requires money        # Bearish Call provides a credit
        upfront (negative premium)     #    upfront (positive premium)
        but has no future liability    #    but has future liabilities
    Bullish Put provides a credit      # Bearish Put requires money
        upfront (positive premium)     #    upfront (negative premium)
        but has future liabilities     #    but has no future liability
    max loss call = premiums paid      # max loss call = X1 - X0 + prem
    max loss put  = X1 - X0 + prem     # max loss put  = premiums paid
    $ = np.clip( price, X0, X1)        # $ = np.clip( price, X0, X1)
    Value call = $ - X0 + prem = VBuC  # Value call = X0 - $ + prem = VBeC
    Value put = $ - X1 + prem = VBuP   # Value put = X1 - $ + prem = VBeP
                    VBuC = -1*VBeC; VBuP = -1*VBeP

    debug:
    downward trending date - pd.datetime(2015, 6, 20)
    tckr = '.SPX'
    spreadType = 'PUT'
    outlook = 'BULL'
    predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
    optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
    purchaseDate = pd.datetime(2006, 6, 20)
    exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                        purchaseDate + pd.Timedelta(days=90))
    currentPrice = None
    contractQty = 1
    expValStpCnt = 500
    modelingType = 'BS'
    contractSize = 100
    positionName = None
    source = 'storage'
    """
    if predictionModel is None:
        predictionModel = Model.getDefaultPredictionModel_HDF5()
    optionDB = OptionData(stockDB=predictionModel.stockDB)
    currentPrice = predictionModel.get_current_price(purchaseDate, tckr)
    spreadType = "CALL" if spreadType[0].upper() == "C" else "PUT"
    outlook = "BULL" if "BULL" in outlook.upper() else "BEAR"
    # gather all possible strikes
    daysOut = (exDate - purchaseDate).days
    strikeArray = np.array(
        optionDB.estimateSPXStrikesOffered(
            currentPrice, daysOut, offeringCode=offeringCode
        )
    )
    priceEst_BS = optionDB.PriceEst_BS(
        tckr,
        spreadType,
        purchaseDate,
        strikeArray,
        exDate,
        S0=currentPrice,
        source=source,
    )
    # Remove prices less than $0.01
    selectBool = priceEst_BS > 0.01
    strikeArray = strikeArray[selectBool]
    priceEst_BS = priceEst_BS[selectBool]
    strikes_0 = strikeArray.reshape(strikeArray.size, 1, 1)
    strikes_1 = strikeArray.reshape(1, strikeArray.size, 1)
    prices_0 = priceEst_BS.reshape(priceEst_BS.size, 1, 1)
    prices_1 = priceEst_BS.reshape(1, priceEst_BS.size, 1)
    # convert the one that will be bought to negative netCashFlow
    sign_0, sign_1 = {
        ("CALL", "BULL"): (-1, 1),
        ("CALL", "BEAR"): (1, -1),
        ("PUT", "BULL"): (-1, 1),
        ("PUT", "BEAR"): (1, -1),
    }[(spreadType, outlook)]
    prices_0 *= sign_0
    prices_1 *= sign_1
    # Now net premium = prices_1 + prices_0

    # calculate the expectedValue of the Option
    # Get an array of possible prices and calculate the probability
    #  of the price based on the prediction model
    minPrice, maxPrice = predictionModel.get_price_from_cum_prob(
        [0.001, 0.999],
        current_date=purchaseDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
        source=source,
    )
    priceStep = (maxPrice - minPrice) / expValStpCnt
    futurePriceArray = np.arange(minPrice, maxPrice, priceStep)
    probOfPrice = predictionModel.get_prob_of_price(
        futurePriceArray,
        current_date=purchaseDate,
        predicted_date=exDate,
        tckr=tckr,
        source=source,
    ).reshape(1, 1, futurePriceArray.size)
    # Calculate the value of the spread if the options were excised at
    #  the prices in priceArray. Take advantage of broadcasting used
    #  behind the curtain in numpy instead of np.broadcast_to() for
    #  each array. Only variation in value comes between the strike prices
    limitedPrice = np.clip(
        futurePriceArray.reshape(1, 1, futurePriceArray.size), strikes_0, strikes_1
    )
    # Value is the amount if excised for the future price plus the premium
    if spreadType == "PUT":
        if outlook == "BULL":
            value = limitedPrice - strikes_1 + (prices_1 + prices_0)
        elif outlook == "BEAR":
            value = strikes_1 - limitedPrice + (prices_1 + prices_0)
    elif spreadType == "CALL":
        if outlook == "BULL":
            value = limitedPrice - strikes_0 + (prices_1 + prices_0)
        elif outlook == "BEAR":
            value = strikes_0 - limitedPrice + (prices_1 + prices_0)

    # Calculate expected value which is the
    #  sum((probability of the price)*(value at that price)*(priceStep))
    exp_profit = (value * probOfPrice * priceStep).sum(axis=2)
    # Determine which conditions are valid and which are false and create
    #  condition to value key
    strikes_0, strikes_1 = np.broadcast_arrays(strikes_0, strikes_1)
    prices_0, prices_1 = np.broadcast_arrays(prices_0, prices_1)
    # only keep strike combinations where x1 is higher than x0 and
    #  prices are more than $0.03 apart so that strategy has enough margin
    #  to at least be plausible in the real world
    validConditions = np.logical_and(
        strikes_1 > strikes_0, np.abs(prices_0 - prices_1) > 0.03
    ).squeeze()
    #  Strikes and prices are 3D so need to convert to 2D before operations
    valid_qty = validConditions.sum()
    strikes_0 = strikes_0.squeeze()[validConditions]
    strikes_1 = strikes_1.squeeze()[validConditions]
    prices_0 = prices_0.squeeze()[validConditions]
    prices_1 = prices_1.squeeze()[validConditions]
    premium = prices_0 + prices_1
    exp_profit = exp_profit[validConditions]
    if (spreadType, outlook) in [("CALL", "BULL"), ("PUT", "BEAR")]:
        max_loss = premium
        max_profit = np.abs(strikes_0 - strikes_1) + premium
    elif (spreadType, outlook) in [("CALL", "BEAR"), ("PUT", "BULL")]:
        max_loss = -np.abs(strikes_0 - strikes_1) + premium
        max_profit = premium
    else:
        assert spreadType in [
            "CALL",
            "PUT",
        ], "Spreadtype is not recognized. It must be 'CALL' or 'PUT'"
        assert outlook in [
            "BULL",
            "BEAR",
        ], "outlook is not recognized. It must be 'BULL' or 'BEAR'"
    #    max_loss = (prices_1 + prices_0) + (strikes_0 - strikes_1)
    exp_return = exp_profit / np.abs(max_loss)
    return_of_stock = predictionModel.expected_return_of_stock(
        tckr, purchaseDate, exDate, source=source
    )
    #    del indexKey, columnKey, max_loss
    bestIndex = exp_return.argmax()
    column_order = [
        "Strategy",
        "Type",
        "OutLook",
        "Category",
        "Data_Source",
        "PurchaseDate",
        "ExpirationDate",
        "Strike_0",
        "Strike_1",
        "Price_0",
        "Price_1",
        "Premium",
        "Max_Loss",
        "Max_Profit",
        "Expected_Profit",
        "Expected_Return",
        "Expected_Return_PerDay",
        "ReturnVsStock",
    ]
    bestOption = {
        "Strategy": "VerticalSpread",
        "Type": spreadType,
        "OutLook": outlook,
        "Category": (outlook.title() + "ish " + spreadType.title() + " Spread"),
        "Data_Source": "BS",
        "PurchaseDate": purchaseDate,
        "ExpirationDate": exDate,
        "Strike_0": strikes_0[bestIndex],
        "Strike_1": strikes_1[bestIndex],
        "Price_0": prices_0[bestIndex],
        "Price_1": prices_1[bestIndex],
        "Premium": premium[bestIndex],
        "Max_Loss": max_loss[bestIndex],
        "Max_Profit": max_profit[bestIndex],
        "Expected_Profit": exp_profit[bestIndex],
        "Expected_Return": exp_return[bestIndex],
        "Expected_Return_PerDay": exp_return[bestIndex] / daysOut,
        "ReturnVsStock": exp_return[bestIndex] / return_of_stock,
    }
    bestOption = pd.Series(data=bestOption)[column_order]
    bestOption.name = "Best " + bestOption.Category
    output = bestOption

    if False:
        # create the results pd.DF for comparison
        column_order = [
            "Strategy",
            "Type",
            "OutLook",
            "Category",
            "Data_Source",
            "PurchaseDate",
            "ExpirationDate",
            "Strike_0",
            "Strike_1",
            "Price_0",
            "Price_1",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Expected_Profit",
            "Expected_Return",
            "Expected_Return_PerDay",
            "ReturnVsStock",
        ]
        options = pd.DataFrame(index=range(valid_qty), columns=column_order)
        options["Strategy"] = "VerticalSpread"
        options["Type"] = spreadType
        options["OutLook"] = outlook
        options["Category"] = outlook.title() + "ish " + spreadType.title() + " Spread"
        options["Data_Source"] = "BS"
        options["PurchaseDate"] = purchaseDate
        options["ExpirationDate"] = exDate
        options["Strike_0"] = strikes_0
        options["Strike_1"] = strikes_1
        options["Price_0"] = prices_0
        options["Price_1"] = prices_1
        options["Premium"] = prices_0 + prices_1
        options["Max_Loss"] = max_loss
        options["Max_Profit"] = options["Premium"].values
        options["Expected_Profit"] = exp_profit
        options["Expected_Return"] = exp_return
        options["Expected_Return_PerDay"] = exp_return / daysOut
        options["ReturnVsStock"] = exp_return / return_of_stock
        output = bestOption, options

    return output


def test_calculation_approx_HEV():
    types = [("Call", "Bull"), ("Call", "Bear"), ("Put", "Bull"), ("Put", "Bear")]
    tckr = ".SPX"
    predictionModel = Model.getDefaultPredictionModel_HDF5()
    optionDB = OptionData(stockDB=predictionModel.stockDB)
    purchaseDate = pd.datetime(2006, 6, 20)
    exDate, offeringCode = optionDB.findClosestExDate(
        purchaseDate, purchaseDate + pd.Timedelta(days=90)
    )
    max_risk = 0.2
    for spreadType, outlook in types:
        exact = find_VerticalSpread_HEV(
            tckr,
            spreadType,
            outlook,
            purchaseDate,
            exDate,
            offeringCode,
            predictionModel,
            max_risk,
        )
        exact.name = "Exact"
        approx = find_VerticalSpread_HEV_ApproxMethod(
            tckr,
            spreadType,
            outlook,
            purchaseDate,
            exDate,
            offeringCode,
            predictionModel,
        )
        approx.name = "Approx"
        comparison = exact == approx
        for i in range(comparison.size):
            if not comparison.iloc[i] and type(exact.iloc[i]) == np.float64:
                comparison.iloc[i] = (
                    abs(exact.iloc[i] - approx.iloc[i]) / exact.iloc[i]
                ) < 0.05
        if comparison.all():
            print("Exact and Approx are equivalent for " + exact.Category)
            print(exact)
        else:
            print("Exact and Approx are NOT equivalent for " + exact.Category)
            print(comparison)
            print(exact)
            print(approx)
        print("")


def find_HEV_Slow():
    """
    slow but performs the expected value calculation in the scipy function
    so use as a consistency check
    """
    # test speed
    # setup
    tckr = ".SPX"
    spreadType = "PUT"
    outlook = "BULL"
    predictionModel = Model.getDefaultPredictionModel_HDF5()
    optionDB = OptionData(stockDB=predictionModel.stockDB)
    source = "storage"
    purchaseDate = pd.datetime(2006, 6, 20)
    exDate, offeringCode = optionDB.findClosestExDate(
        purchaseDate, purchaseDate + pd.Timedelta(days=90)
    )
    currentPrice = predictionModel.get_current_price(purchaseDate, tckr)
    spreadType = "CALL" if spreadType[0].upper() == "C" else "PUT"
    outlook = "BULL" if "BULL" in outlook.upper() else "BEAR"
    # gather all possible strikes
    daysOut = (exDate - purchaseDate).days
    strikeArray = np.array(
        optionDB.estimateSPXStrikesOffered(
            currentPrice, daysOut, offeringCode=offeringCode
        )
    )
    priceEst_BS = optionDB.PriceEst_BS(
        tckr,
        spreadType,
        purchaseDate,
        strikeArray,
        exDate,
        S0=currentPrice,
        source=source,
    )
    priceDict = dict(zip(strikeArray, priceEst_BS))
    # find exp value
    dist = predictionModel.get_prob_dist(purchaseDate, exDate, tckr)
    lb = dist.ppf(0.001)
    ub = dist.ppf(0.999)
    # default price signs are for the bullish call spread
    strike_select = 0
    price_sign = np.array([-1, 1])
    strike_sign = 1
    if spreadType == "PUT":
        strike_select = 1
    if outlook == "BEAR":
        strike_sign = -1
        price_sign = -price_sign
    valid_strikes = itertools.ifilter(
        lambda x: x[1] > x[0], itertools.combinations(strikeArray, 2)
    )

    def fill_exp_profit(strike_tuple):
        exp_prices = (
            np.array([priceDict[strike_tuple[0]], priceDict[strike_tuple[1]]])
            * price_sign
        )
        ref_strike = strike_tuple[strike_select]
        profit = dist.expect(
            lambda x: (
                strike_sign
                * (min(max(x, strike_tuple[0]), strike_tuple[1]) - ref_strike)
                + (exp_prices[1] + exp_prices[0])
            ),
            lb=lb,
            ub=ub,
        )
        return [strike_tuple[0], strike_tuple[1], exp_prices[0], exp_prices[1], profit]

    exp_profit = map(fill_exp_profit, valid_strikes)
    exp_profit = pd.DataFrame(
        exp_profit,
        columns=["Strike_0", "Strike_1", "Price_0", "Price_1", "Expected_Profit"],
    )
    exp_profit["Max_Loss"] = -np.abs(exp_profit.Strike_1 - exp_profit.Strike_0) + (
        exp_profit.Price_1 + exp_profit.Price_0
    )
    exp_profit["Premium"] = exp_profit.Price_0 + exp_profit.Price_1
    exp_profit["Expected_Return"] = exp_profit.Expected_Profit / -exp_profit.Max_Loss
    exp_profit["Expected_Return_PerDay"] = exp_profit["Expected_Return"] / daysOut

    return_of_stock = predictionModel.expected_return_of_stock(
        tckr, purchaseDate, exDate, source=source
    )
    exp_profit["ReturnVsStock"] = exp_profit["Expected_Return"] / return_of_stock
    # Provide Context and Reorder.
    # OPTIMIZE - If this takes too long, don't do this until
    #  after the series is selected, but for now it helps to have all info
    exp_profit["Strategy"] = "Vertical Spread"
    exp_profit["Type"] = spreadType
    exp_profit["OutLook"] = outlook
    exp_profit["Category"] = outlook.title() + "ish " + spreadType.title() + " Spread"
    exp_profit["Data_Source"] = "BS"
    exp_profit["PurchaseDate"] = purchaseDate
    exp_profit["ExpirationDate"] = exDate
    x0 = exp_profit.Strike_0
    x1 = exp_profit.Strike_1
    if (spreadType, outlook) in [("CALL", "BULL"), ("PUT", "BEAR")]:
        exp_profit["Max_Loss_2"] = exp_profit.Premium
        exp_profit["Max_Profit"] = np.abs(x0 - x1) + exp_profit.Premium
    elif (spreadType, outlook) in [("CALL", "BEAR"), ("PUT", "BULL")]:
        exp_profit["Max_Loss_2"] = -np.abs(x0 - x1) + exp_profit.Premium
        exp_profit["Max_Profit"] = exp_profit.Premium
    column_order = [
        "Strategy",
        "Type",
        "OutLook",
        "Category",
        "Data_Source",
        "PurchaseDate",
        "ExpirationDate",
        "Strike_0",
        "Strike_1",
        "Price_0",
        "Price_1",
        "Premium",
        "Max_Loss",
        "Max_Loss_2",
        "Max_Profit",
        "Expected_Profit",
        "Expected_Return",
        "Expected_Return_PerDay",
        "ReturnVsStock",
    ]
    exp_profit = exp_profit[column_order]
    slow_calc_row = exp_profit.iloc[np.argmax(exp_profit.Expected_Return), :]
    print(slow_calc_row)
