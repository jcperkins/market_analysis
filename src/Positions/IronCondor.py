"""
-------------------------------------------------------------------------------
 Name:        IronCondor
 Purpose:      File dedicated to the IronCondor class and functions that
                 involve the IC strategy

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
import time
import numpy as np
import pandas as pd
from src.DataSource import OptionData
from src.Positions import ComplexOption, SimpleOption
import src.Utility as Util
from src.Model import getDefaultPredictionModel_HDF5
from numba import jit


class IronCondor(ComplexOption.ComplexOption):
    """
    ***************************** Iron Condor ***************************
    Provides a credit upfront (positive premium) but has future liabilities
                    Sell P.-0                   Sell C.-0
                             X1-----------------X2
                            /                    \
                Buy P.     /                      \ Buy C.
                ---------X0                       X3---------

    max loss = min((X0-X1), (X2-X3)) + prem
    max profit = premium
    profit = { $ <= X0:         (X0-X1) + prem
               X0 <= $ <= X1:   ($-X1) + prem
               X1 <= $ <= X2:   prem
               X2 <= $ <= X1:   (X2-$) + prem
               $ <= X0:         (X3-X2) + prem}

    signs will indicate case flow
    max loss will be negative signifying negative cash flow
    map profit will be positive signifying positive cash flow
    """

    optionType = "IronCondor"

    def __init__(
        self,
        tckr,
        strikes,
        purchaseDate,
        exDate,
        contractQty,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        self.strikes = strikes
        self.exDate = exDate
        if optionDB is None:
            self.optionDB = OptionData()
        else:
            self.optionDB = optionDB
        puts = [
            SimpleOption.Put(
                tckr,
                strikes[0],
                exDate,
                contractQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="IC_PUT_1",
                optionDB=self.optionDB,
                source=source,
            ),
            SimpleOption.Put(
                tckr,
                strikes[1],
                self.exDate,
                -contractQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="IC_PUT_2",
                optionDB=self.optionDB,
                source=source,
            ),
        ]
        calls = [
            SimpleOption.Call(
                tckr,
                strikes[2],
                self.exDate,
                -contractQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="IC_CALL_1",
                optionDB=self.optionDB,
                source=source,
            ),
            SimpleOption.Call(
                tckr,
                strikes[3],
                self.exDate,
                contractQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="IC_CALL_2",
                optionDB=self.optionDB,
                source=source,
            ),
        ]

        ComplexOption.ComplexOption.__init__(
            self,
            tckr,
            calls,
            puts,
            contractQty,
            self.optionType,
            modelingType=modelingType,
            contractSize=contractSize,
            positionName=positionName,
            optionDB=optionDB,
            source=source,
        )

    def calculateValue_Excised(self, date, price=None):
        price = price if price is not None else self.getUnderlyingPrice(date)
        try:
            lowSidePrice = max(price, self.strikes[0])
            highSidePrice = min(price, self.strikes[3])
            value = 0
            if price < self.strikes[1]:
                # value lies on a straight line connected the lowest two
                #  strike prices
                value = lowSidePrice - self.strikes[1]
            elif price > self.strikes[2]:
                value = self.strikes[2] - highSidePrice
        except Exception:
            limitedPrice = np.array(price)
            value = pd.Series(index=price, data=np.zeros(limitedPrice.shape))
            # set max loss price on the low side
            selectBool = limitedPrice < self.strikes[0]
            if selectBool.any():
                limitedPrice[selectBool] = self.strikes[0]
            # assign low side values
            selectBool = limitedPrice < self.strikes[1]
            if selectBool.any():
                value[selectBool] = limitedPrice - self.strikes[1]
            # set max loss price on the high side
            selectBool = limitedPrice > self.strikes[3]
            if selectBool.any():
                limitedPrice[selectBool] = self.strikes[3]
            # assign high side values
            selectBool = limitedPrice > self.strikes[2]
            if selectBool.any():
                value[selectBool] = self.strikes[2] - limitedPrice
        return value * self.contractQty

    @classmethod
    def monte_carlo_single_date(
        cls,
        tckr,
        purchaseDate,
        exDate,
        offeringCode,
        predictionModel,
        strategy="IronCondor",
        current_price=None,
        symmetry=False,
        n_mc=100,
        cum_cycles=20,
        max_risk=0.2,
        lifetime=1,
        strikes=None,
        prices=None,
        return_all_data=False,
        optimize=True,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        source="storage",
    ):
        """
        run full monte carlo simulation for the given option strategy and
        expiration date.

        Args:
            tckr - the name of the underlying security
            spreadtype - 'Call' or 'Put'
            outlook - 'Bull' (positive) or 'Bear' (negative)
            purchaseDate - the last date that the functions will pull
                historical data for
            exDate - If a single date then call the appropriate function for
                        the date.
                     If a range of dates, then find the optimal
                        option for each date.
                     If 'Estimate' then estimate based on the range determined
                        by the offering code
                     If 'Fidelity' then pull all the exDates available in the
                        fidelity options DB for the give purchase date. If no
                        date matches then default to 'Estimate'
            offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
                whether to pull strikes for a weekly, monthly, quarterly,
                or yearly offering. The offering code is returned from the
                optionDB.findClosestExDate function call.
            predictionModel - if None then function will use the default
                prediction model in the MarketPredictions module
            lifetime - the fraction of the option lifetime that the option will
                be held before it is sold. If 'expire' or 1 then hold the
                option until it expires
            n_mc - the number of times each option combination is modeled
            cum_cycles - the number of loops to test the option combination
                so that the true return can be approximated if you have both
                good and bad prices
            max_risk - the maximum amount of the portfolio that will be risked
                in the option strategies
            total_pf - value of the total portfolio that the strategy will be
                applied too
            broker_fee - the price of the first option contract
                (or stock purchase) charged by the broker for the transaction
            addtl_option_fee - the additional fee charged by the broker
                for each contract after the initial contract
            return_all_data - Default is False. If true then return a tuple of
                the best option and a pd.DataFrame of the info for every option
            source - where to pull the stock data. options are 'storage' or
                'temp_db'

        tckr = '.SPX'
        spreadType = 'PUT'
        outlook = 'BULL'
        predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
        optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
        purchaseDate = pd.datetime(2006, 6, 20)
        exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                        purchaseDate + pd.Timedelta(days=30))
        current_price = None
        contractQty = 1
        expValStpCnt = 500
        modelingType = 'BS'
        contractSize = 100
        positionName = None
        source = 'storage'
        symmetry=False
        n_mc=100
        cum_cycles=10
        max_risk=0.2
        lifetime=1
        symmetry=False
        return_all_data=False
        strikes = None
        prices = None
        total_pf = 1e5
        broker_fee = 0
        addtl_option_fee = 0
        """
        if predictionModel is None:
            predictionModel = getDefaultPredictionModel_HDF5()
        positions = ["Put", "Put", "Call", "Call"]
        cashflow_sign = [-1, 1, 1, -1]
        quantity = [1, 1, 1, 1]
        purchaseDate = Util.formatDateInput(purchaseDate)
        exDate = Util.formatDateInput(exDate)
        optionDB = OptionData(stockDB=predictionModel.stockDB)
        if current_price is None:
            current_price = predictionModel.get_current_price(purchaseDate, tckr)
        # gather all possible strikes
        daysOut = (exDate - purchaseDate).days
        if type(lifetime) == str or lifetime >= 1:
            sellDate = exDate
            sigma = rate = None
        else:
            sellDate = purchaseDate + pd.Timedelta(days=int(daysOut * lifetime))
            sigma = (
                optionDB.stockDB.PredictSPXVol_1m3mVIX(
                    purchaseDate, exDate, source=source
                )
                / 100.0
            )
            rate = (
                optionDB.liborDB.getLIBOR_Rate(purchaseDate, daysOut, source=source)
                / 100.0
            )

        days_invested = (sellDate - purchaseDate).days
        # get price prediction
        dist = predictionModel.get_prob_dist(purchaseDate, sellDate, tckr)
        predicted_price = dist.mean()
        predicted_std = dist.std()

        strikes, prices = cls.format_strikes_prices(
            positions,
            cashflow_sign,
            quantity,
            strike_list=strikes,
            price_list=prices,
            optionDB=optionDB,
            purchaseDate=purchaseDate,
            exDate=exDate,
            current_price=current_price,
            offeringCode=offeringCode,
            sigma=sigma,
            rate=rate,
            tckr=tckr,
            source=source,
        )

        legs = len(strikes)
        # Remove buy prices greater than -$0.01 and sell prices less than $0.01
        # IC - [buy put, sell put, sell call, buy call]
        i_leg = 0
        while True:
            if prices[i_leg].sum() <= 0:
                selectBool = prices[i_leg] < -0.01
            else:
                selectBool = prices[i_leg] > 0.01
            strikes[i_leg] = strikes[i_leg][selectBool]
            prices[i_leg] = prices[i_leg][selectBool]
            i_leg += 1
            if i_leg >= legs:
                break

        # loop will reduce the strike combination counts until all of them are
        #  under the threshold
        strikes_prev = strikes
        prices_prev = prices
        strike_limit = 50
        while True:
            strikes_new, prices_new = cls.reduce_combinations_mc(
                positions,
                quantity,
                strikes_prev,
                prices_prev,
                strike_limit,
                dist,
                strategy=strategy,
                n_mc=n_mc,
                cum_cycles=cum_cycles,
                max_risk=max_risk,
                lifetime=1,
                symmetry=symmetry,
                tckr=tckr,
                sellDate=sellDate,
                exDate=exDate,
                sigma=sigma,
                rate=rate,
                optionDB=optionDB,
                total_pf=total_pf,
                broker_fee=broker_fee,
                addtl_option_fee=addtl_option_fee,
                return_all_data=False,
                source=source,
            )
            if np.any([x.size == 0 for x in strikes_new]):
                break
            elif np.all(
                [
                    np.all(strikes_new[j].size == strikes_prev[j].size)
                    and np.all(prices_new[j].size == prices_prev[j].size)
                    for j in range(legs)
                ]
            ):
                break
            else:
                strikes_prev, prices_prev = strikes_new, prices_new
        # strikes_r is the reduced strike lists and needs to have specific
        #  shapes based on the index position
        strikes_r = [
            strikes_prev[x_i].reshape(
                tuple([1] * x_i + [strikes_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        prices_r = [
            prices_prev[x_i].reshape(
                tuple([1] * x_i + [prices_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)

        # only keep combinations where the strikes are in sequential order
        selectBool = cls.screen_strike_sequence(
            strikes_t, strategy=strategy, symmetry=symmetry
        )
        # Only keep the IC possibilities that have a net premium > $0.03
        # If symmetry is enforced, remove the asymmetric possibilities
        selectBool = np.logical_and(
            selectBool, (prices_t[0] + prices_t[1] + prices_t[2] + prices_t[3]) > 0.03
        )
        if symmetry:
            selectBool = np.logical_and(
                selectBool, strikes_t[1] - strikes_t[0] == strikes_t[3] - strikes_t[2]
            )
        valid_qty = selectBool.sum()

        strikes_t = np.concatenate(
            (
                strikes_t[0][selectBool].reshape(valid_qty, 1),
                strikes_t[1][selectBool].reshape(valid_qty, 1),
                strikes_t[2][selectBool].reshape(valid_qty, 1),
                strikes_t[3][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )
        prices_t = np.concatenate(
            (
                prices_t[0][selectBool].reshape(valid_qty, 1),
                prices_t[1][selectBool].reshape(valid_qty, 1),
                prices_t[2][selectBool].reshape(valid_qty, 1),
                prices_t[3][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )

        print("\tRunning full MC simulation")
        (
            total_returns,
            raw_volatility,
            win_perc,
            strikes_t,
            prices_t,
            selection_bridge,
        ) = cls.simulate_monte_carlo(
            positions,
            cashflow_sign,
            quantity,
            strikes_t,
            prices_t,
            dist,
            n_mc=n_mc,
            cycles=cum_cycles,
            max_risk=max_risk,
            lifetime=lifetime,
            tckr=tckr,
            sellDate=sellDate,
            exDate=exDate,
            sigma=sigma,
            rate=rate,
            optionDB=optionDB,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
            source=source,
        )
        valid_qty = strikes_t.shape[0]
        column_order = (
            [
                "Strategy",
                "Category",
                "Outcome_Qty",
                "Yield_Cycles",
                "PurchaseDate",
                "ExpirationDate",
                "DaysToExp",
                "DaysInvested",
                "Max_Risk",
                "CurrentPrice",
                "Prediction_Price",
                "Prediction_Std",
                "Total_PF",
                "Trade_Fee",
                "Addtl_Contract_Fee",
            ]
            + ["Strike_%1i" % j for j in range(legs)]
            + ["Price_%1i" % j for j in range(legs)]
            + [
                "Premium",
                "Max_Loss",
                "Max_Profit",
                "Median_PF_Return",
                "Mean_PF_Return",
                "Std_PF_Return",
                "Avg_PF_Return_PerDay",
                "Avg_Ann_Return",
                "Expected_Stock_Return",
                "AvgReturnVsStock",
                "Expected_Gain_Return",
                "Win_Percentage",
                "Ann_Volatility",
            ]
        )

        if strikes_t.size == 0:
            bestOption = pd.Series(index=column_order, name="Best " + strategy)
            bestOption["Strategy"] = strategy
            bestOption["Category"] = "Monte Carlo"
            bestOption["Outcome_Qty"] = n_mc
            bestOption["Yield_Cycles"] = cum_cycles
            bestOption["PurchaseDate"] = purchaseDate
            bestOption["ExpirationDate"] = exDate
            bestOption["DaysToExp"] = daysOut
            bestOption["DaysInvested"] = daysOut
            bestOption["Max_Risk"] = max_risk
            bestOption["CurrentPrice"] = current_price
            bestOption["Prediction_Price"] = dist.mean()
            bestOption["Prediction_Std"] = dist.std()
            bestOption["Total_PF"] = total_pf
            bestOption["Trade_Fee"] = broker_fee
            bestOption["Addtl_Contract_Fee"] = addtl_option_fee
            if return_all_data:
                options = pd.DataFrame(bestOption).T
                options.index = [0]
                return bestOption, options
            else:
                return bestOption
        # calculate the expected return of the option
        exp_return = cls.expected_gain(
            positions,
            quantity,
            strikes_t,
            prices_t,
            max_risk,
            dist,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
        )
        premium = cls.calc_premium(prices_t)
        init_invest = cls.calc_init_invest(positions, quantity, strikes_t, prices_t)
        max_profit = cls.calc_max_profit(positions, quantity, strikes_t, prices_t)

        pf_median, pf_mean, pf_stdev = total_returns
        ann_volatility = np.sqrt(
            (raw_volatility * raw_volatility).sum(-1) / raw_volatility.shape[-1]
        ) * np.sqrt(365 / float(daysOut))
        best_index = pf_mean.argmax()
        return_of_stock = predictionModel.expected_return_of_stock(
            tckr, purchaseDate, sellDate, source=source
        )
        bestOption = {
            "Strategy": strategy,
            "Category": "Monte Carlo",
            "Outcome_Qty": n_mc,
            "Yield_Cycles": cum_cycles,
            "PurchaseDate": purchaseDate,
            "ExpirationDate": exDate,
            "DaysToExp": daysOut,
            "DaysInvested": days_invested,
            "Max_Risk": max_risk,
            "CurrentPrice": current_price,
            "Prediction_Price": predicted_price,
            "Prediction_Std": predicted_std,
            "Total_PF": total_pf,
            "Trade_Fee": broker_fee,
            "Addtl_Contract_Fee": addtl_option_fee,
            "Premium": premium[best_index, 0],
            "Max_Loss": -1 * init_invest[best_index, 0],
            "Max_Profit": max_profit[best_index, 0],
            "Median_PF_Return": pf_median[best_index],
            "Mean_PF_Return": pf_mean[best_index],
            "Std_PF_Return": pf_stdev[best_index],
            "Avg_PF_Return_PerDay": pf_mean[best_index] ** (1.0 / days_invested),
            "Avg_Ann_Return": (pf_mean[best_index] ** (365.0 / days_invested)),
            "Expected_Stock_Return": return_of_stock + 1,
            "AvgReturnVsStock": (pf_mean[best_index] - 1) / (return_of_stock),
            "Expected_Gain_Return": exp_return[best_index, 0],
            "Win_Percentage": win_perc[best_index, 0],
            "Ann_Volatility": ann_volatility[best_index],
        }
        for i in range(legs):
            bestOption["Strike_%1i" % i] = strikes_t[best_index, i]
            bestOption["Price_%1i" % i] = prices_t[best_index, i]
        bestOption = pd.Series(data=bestOption)[column_order]
        bestOption.name = "Best " + strategy
        output = bestOption

        if return_all_data:
            # create the results pd.DF for comparison
            options = pd.DataFrame(index=range(valid_qty), columns=column_order)
            options["Strategy"] = strategy
            options["Category"] = "Monte Carlo"
            options["Outcome_Qty"] = n_mc
            options["Yield_Cycles"] = cum_cycles
            options["PurchaseDate"] = purchaseDate
            options["ExpirationDate"] = exDate
            options["DaysToExp"] = daysOut
            options["DaysInvested"] = daysOut
            options["Max_Risk"] = max_risk
            options["CurrentPrice"] = current_price
            options["Prediction_Price"] = dist.mean()
            options["Prediction_Std"] = dist.std()
            options["Total_PF"] = total_pf
            options["Trade_Fee"] = broker_fee
            options["Addtl_Contract_Fee"] = addtl_option_fee
            options["Premium"] = premium
            options["Max_Loss"] = -1 * init_invest
            options["Max_Profit"] = max_profit
            options["Median_PF_Return"] = pf_median
            options["Mean_PF_Return"] = pf_mean
            options["Std_PF_Return"] = pf_stdev
            options["Avg_PF_Return_PerDay"] = pf_mean ** (1.0 / days_invested)
            options["Avg_Ann_Return"] = pf_mean ** (365.0 / daysOut)
            options["Expected_Stock_Return"] = return_of_stock + 1
            options["AvgReturnVsStock"] = (pf_mean - 1) / return_of_stock
            options["Expected_Gain_Return"] = exp_return
            options["Win_Percentage"] = win_perc
            options["Ann_Volatility"] = ann_volatility
            for i in range(legs):
                options["Strike_%1i" % i] = strikes_t[:, i]
                options["Price_%1i" % i] = prices_t[:, i]
            options = options[column_order]
            output = bestOption, options

        return output

    @classmethod
    def screen_strike_sequence(cls, strikes, strategy="IC", symmetry=False):
        # only keep combinations where the strikes are in sequential order
        strategy = strategy.upper()
        if strategy != "IB" and "BUTTERFLY" not in strategy:
            # Iron Condor strategy so middle strikes are not equal
            selectBool = np.logical_and(
                strikes[1] > strikes[0], strikes[2] > strikes[1]
            )
            selectBool = np.logical_and(selectBool, strikes[3] > strikes[2])
        elif strategy == "IB" or ("BUTTERFLY" in strategy and "CONDOR" not in strategy):
            # Iron Butterfly strategy so middle strikes are equal
            selectBool = np.logical_and(
                strikes[1] > strikes[0], strikes[2] == strikes[1]
            )
            selectBool = np.logical_and(selectBool, strikes[3] > strikes[2])
        else:
            # allow for both iron condors and butterflies
            selectBool = np.logical_and(
                strikes[1] > strikes[0], strikes[2] >= strikes[1]
            )
            selectBool = np.logical_and(selectBool, strikes[3] > strikes[2])
        if symmetry:
            selectBool = np.logical_and(
                selectBool, strikes[1] - strikes[0] == strikes[3] - strikes[2]
            )
        return selectBool

    @classmethod
    def reduce_combinations_mc(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        strike_limit,
        dist,
        strategy=None,
        n_mc=100,
        cum_cycles=20,
        max_risk=0.2,
        lifetime=1,
        symmetry=False,
        tckr=None,
        sellDate=None,
        exDate=None,
        sigma=None,
        rate=None,
        optionDB=None,
        total_pf=100000,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        if np.any([arr.size == 0 for arr in strikes]):
            return strikes, prices
        if np.all([arr.size < strike_limit for arr in strikes]):
            return strikes, prices
        # Limit strike price to 25-50 strikes and calculate the max return
        #  then return a reduced version of the strike and price combinations
        legs = len(positions)
        n_mc_step = 3
        cashflow_sign = cls.calc_cashflow_sign(prices)
        selectBool = [
            np.arange(x.size) % max(1, x.size / (strike_limit / 2)) == 0
            for x in strikes
        ]
        # strikes_r is the reduced strike lists
        strikes_r = [
            strikes[x_i][selectBool[x_i]].reshape(
                tuple([1] * x_i + [selectBool[x_i].sum()] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        prices_r = [
            prices[x_i][selectBool[x_i]].reshape(
                tuple([1] * x_i + [selectBool[x_i].sum()] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        # create a boolean indicator to signal which legs are reduced
        leg_reduced_bool = [
            strikes[x_i].size != strikes_r[x_i].size for x_i in range(legs)
        ]
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)

        # only keep combinations where the strikes are in sequential order
        selectBool = cls.screen_strike_sequence(
            strikes_t, strategy=strategy, symmetry=symmetry
        )
        # Only keep the IC possibilities that have a net premium > $0.03
        # If symmetry is enforced, remove the asymmetric possibilities
        selectBool = np.logical_and(
            selectBool, (prices_t[0] + prices_t[1] + prices_t[2] + prices_t[3]) > 0.03
        )
        valid_qty = selectBool.sum()
        if valid_qty == 0:
            return [np.array([[]])] * legs, [np.array([[]])] * legs

        strikes_t = np.concatenate(
            (
                strikes_t[0][selectBool].reshape(valid_qty, 1),
                strikes_t[1][selectBool].reshape(valid_qty, 1),
                strikes_t[2][selectBool].reshape(valid_qty, 1),
                strikes_t[3][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )
        prices_t = np.concatenate(
            (
                prices_t[0][selectBool].reshape(valid_qty, 1),
                prices_t[1][selectBool].reshape(valid_qty, 1),
                prices_t[2][selectBool].reshape(valid_qty, 1),
                prices_t[3][selectBool].reshape(valid_qty, 1),
            ),
            axis=1,
        )

        # strikes have been reduced. Only do enough work to determine the
        #  strike cutoffs for the next round of analysis
        cycles = 100
        chunk_size = 100
        record_cutoff = 10000000
        quantile_cutoff = 75
        total_returns, __, __, strikes_t, __, __ = cls.simulate_monte_carlo(
            positions,
            cashflow_sign,
            quantity,
            strikes_t,
            prices_t,
            dist,
            n_mc=n_mc_step,
            cycles=cycles,
            n_mc_step=n_mc_step,
            chunk_size=chunk_size,
            record_cutoff=record_cutoff,
            quantile_cutoff=quantile_cutoff,
            max_risk=max_risk,
            lifetime=1,
            tckr=tckr,
            sellDate=sellDate,
            exDate=exDate,
            sigma=sigma,
            rate=rate,
            optionDB=optionDB,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
            source=source,
        )
        pf_mean = total_returns[1]
        # quadratic fit for each leg was too complicated. Find the
        #  max return over the option leg by convolving it and
        #  proceeding with the limits set the prices within 25% of the max
        unique_strikes = [
            strikes_r[j].ravel()[np.isin(strikes_r[j].ravel(), strikes_t[:, j])]
            for j in range(legs)
        ]
        max_returns = [
            np.array([pf_mean[strikes_t[:, j] == x].max() for x in unique_strikes[j]])
            for j in range(legs)
        ]
        selectBool = [
            max_returns[j]
            > max_returns[j].max()
            - (0.25 * (max_returns[j].max() - max_returns[j].min()))
            for j in range(legs)
        ]
        i_leg = 0
        next_strikes = []
        next_prices = []
        while True:
            if not leg_reduced_bool[i_leg]:
                next_strikes.append(strikes[i_leg])
                next_prices.append(prices[i_leg])
                i_leg += 1
                if i_leg >= legs:
                    break
                else:
                    continue
            unique_strikes = strikes_r[i_leg].ravel()[
                np.isin(strikes_r[i_leg].ravel(), strikes_t[:, i_leg])
            ]
            max_returns = np.array(
                [pf_mean[strikes_t[:, i_leg] == x].max() for x in unique_strikes]
            )
            selectBool = max_returns > max_returns.max() - (
                0.25 * (max_returns.max() - max_returns.min())
            )
            max_return_strike = unique_strikes[max_returns.argmax()]
            #                pl.plot(unique_strikes, max_returns, 'b.')
            #                max_index = max_returns.argmax()
            #                pl.plot(unique_strikes[[max_index]],
            #                        max_returns[[max_index]], 'k.')
            #                pl.plot(unique_strikes[selectBool],
            #                        max_returns[selectBool], 'r.')
            #                pl.show()
            if selectBool.any():
                low_limit = unique_strikes[selectBool].min()
                high_limit = unique_strikes[selectBool].max()
            else:
                low_limit = unique_strikes.min()
                high_limit = unique_strikes.max()
            # adjust limits for special conditions when they may
            #  be the max or on the edge of the array
            if low_limit == max_return_strike:
                # set the limit at the next lowest limit
                if (unique_strikes < low_limit).any():
                    low_limit = unique_strikes[unique_strikes < low_limit].max()
                else:
                    low_limit = None
            if low_limit is not None and low_limit == unique_strikes.min():
                low_limit = None
            if high_limit == max_return_strike:
                # set the limit at the next higher limit
                if (unique_strikes > high_limit).any():
                    high_limit = unique_strikes[unique_strikes > high_limit].min()
                else:
                    high_limit = None
            if high_limit is not None and high_limit == unique_strikes.max():
                high_limit = None
            # determine which strikes to use for the next round. Limits
            #  with None values will not have a limit applied
            if low_limit is None:
                # if the limit is the strike with the highest return or the
                #  first strike, don't set a lower limit
                if high_limit is None:
                    # if the limit is the strike with the highest return or
                    #  the last strike, don't set an upper limit
                    selectBool = np.array([True] * strikes[i_leg].size)
                else:
                    selectBool = strikes[i_leg] < high_limit
            else:
                if high_limit is None:
                    # if the limit is the strike with the highest return or
                    #  the last strike, don't set an upper limit
                    selectBool = strikes[i_leg] > low_limit
                else:
                    selectBool = np.logical_and(
                        strikes[i_leg] < high_limit, strikes[i_leg] > low_limit
                    )
            next_strikes.append(strikes[i_leg][selectBool])
            next_prices.append(prices[i_leg][selectBool])
            i_leg += 1
            if i_leg >= legs:
                break

        return next_strikes, next_prices

    # @jit(nogil=True) - JIT is giving me problems at the moment 4/23/19
    @classmethod
    def mc_calc_expiration(
        cls,
        positions,
        cashflow_sign,
        quantity,
        strikes,
        premium,
        init_invest,
        fees,
        rand_price_block,
        max_risk,
        record_cutoff=1e7,
        return_all_data=False,
    ):
        """
        strikes are a 2d array with each column representing the x0, x1, x2, x3
            strikes
        init_investment is the abs(max_loss) or the amount that could be lost
            in the transaction
        rand_price_block is the 2d array of random numbers in the shape
            (# of cycles to test the return, # of pf's to test per option)

        returns a tuple of the median, mean, and stdev of the returns for each
            option combination
        """
        start_time = time.clock()
        cycles, pf_cnt = rand_price_block.shape
        valid_qty, legs = strikes.shape
        record_cnt = cycles * pf_cnt * valid_qty
        if record_cnt < record_cutoff:
            premium = premium.reshape((valid_qty, 1, 1))
            init_invest = init_invest.reshape((valid_qty, 1, 1))
            fees = init_invest.reshape((valid_qty, 1, 1))
            strikes = strikes.reshape((valid_qty, 4, 1))
            rand_price_block = rand_price_block.T.reshape((1, pf_cnt, cycles))
            # minimum of the profit from the low side and the high side spreads
            # two methods
            profit_low_spread = (
                np.clip(rand_price_block, strikes[:, [0], :], strikes[:, [1], :])
                - strikes[:, [1], :]
            )
            profit_high_spread = strikes[:, [2]] - np.clip(
                rand_price_block, strikes[:, [2], :], strikes[:, [3], :]
            )
            profit = premium - fees + np.minimum(profit_low_spread, profit_high_spread)
            qty_win = (profit > 0).sum(axis=-1).sum(axis=-1).reshape(valid_qty, 1)

            portfolio_return = 1 + max_risk * (profit / init_invest)
            raw_volatility = portfolio_return.std(axis=-1)
            portfolio_return = portfolio_return.prod(axis=-1)
        else:
            premium = premium.reshape((valid_qty, 1))
            init_invest = init_invest.reshape((valid_qty, 1))
            fees = init_invest.reshape((valid_qty, 1))
            portfolio_return = np.empty((valid_qty, pf_cnt))
            portfolio_return.fill(1)
            qty_win = np.zeros_like(init_invest)
            raw_volatility = []
            loop_cnt = 0
            while True:
                rand_price = rand_price_block[loop_cnt, :]
                # minimum of the profit from the low side and the high
                #  side spreads two methods
                profit_low_spread = (
                    np.clip(rand_price, strikes[:, [0]], strikes[:, [1]])
                    - strikes[:, [1]]
                )
                profit_high_spread = strikes[:, [2]] - np.clip(
                    rand_price, strikes[:, [2]], strikes[:, [3]]
                )
                profit = (
                    premium - fees + np.minimum(profit_low_spread, profit_high_spread)
                )
                qty_win += (profit > 0).sum(axis=-1).reshape(valid_qty, 1)
                loop_return = 1 + max_risk * (profit / init_invest)
                portfolio_return = portfolio_return * loop_return
                raw_volatility.append(loop_return)
                loop_cnt += 1
                if loop_cnt >= cycles:
                    break
            raw_volatility = np.std(raw_volatility, axis=0)

        portfolio_return = portfolio_return ** (1.0 / cycles)
        print(f"IC.mc_calc_expiration() loop time: {time.clock()-start_time}")
        if return_all_data:
            return (portfolio_return, raw_volatility, qty_win / float(cycles * pf_cnt))
        else:
            raw_volatility = np.sqrt((raw_volatility * raw_volatility).sum(axis=-1))
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
                raw_volatility.reshape(valid_qty, 1),
                qty_win / float(cycles * pf_cnt),
            )

    @classmethod
    def mc_calc_sell(
        cls,
        strikes,
        premium,
        init_invest,
        rand_price_block,
        max_risk,
        tckr,
        sellDate,
        exDate,
        sigma,
        rate,
        optionDB,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        """
        strikes are a 2d array with each column representing the x0, x1, x2, x3
            strikes
        init_investment is the abs(max_loss) or the amount that could be lost
            in the transaction
        rand_price_block is the 2d array of random numbers in the shape
            (# of cycles to test the return, # of pf's to test per option)

        returns a tuple of the median, mean, and stdev of the returns for each
            option combination
        """
        cycles, pf_cnt = rand_price_block.shape
        valid_qty = strikes.shape[0]
        premium_sq = np.tile(premium, pf_cnt).reshape((valid_qty, pf_cnt))
        init_invest_sq = np.tile(init_invest, pf_cnt).reshape((valid_qty, pf_cnt))
        portfolio_return = np.empty((valid_qty, pf_cnt))
        portfolio_return.fill(1)

        loop_cnt = 0
        while True:
            rand_price = rand_price_block[loop_cnt, :]
            # estimate of selling the put half (sell sx0, buy sx1)
            s_new_sq = optionDB.PriceEst_BS(
                tckr,
                "Put",
                sellDate,
                np.concatenate((strikes[:, 0], strikes[:, 1]), axis=0).reshape(
                    2 * valid_qty, 1
                ),
                exDate,
                S0=rand_price,
                sigma=sigma,
                rate=rate,
                source=source,
            )
            sell_premium_sq = s_new_sq[:valid_qty, :] - s_new_sq[valid_qty:, :]
            # estimate of selling the call half (buy x2, sell x3)
            s_new_sq = optionDB.PriceEst_BS(
                tckr,
                "Call",
                sellDate,
                np.concatenate((strikes[:, 2], strikes[:, 3]), axis=0).reshape(
                    2 * valid_qty, 1
                ),
                exDate,
                S0=rand_price,
                sigma=sigma,
                rate=rate,
                source=source,
            )
            # s_premium = premium_put_half + premium_sellx3 - premium_buyx2
            sell_premium_sq = sell_premium_sq + (
                s_new_sq[valid_qty:, :] - s_new_sq[:valid_qty, :]
            )
            net_profit_sq = premium_sq + sell_premium_sq
            portfolio_return = portfolio_return * (
                1 + max_risk * (net_profit_sq / init_invest_sq)
            )
            loop_cnt += 1
            if loop_cnt >= cycles:
                break

        if return_all_data:
            return portfolio_return
        else:
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
            )

    @classmethod
    def expected_gain(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        max_risk,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        premium = prices.sum(axis=1).reshape(strikes.shape[0], 1)
        profit = np.concatenate(
            (
                (strikes[:, [0]] - strikes[:, [1]]) + premium,
                premium.copy(),
                (strikes[:, [2]] - strikes[:, [3]]) + premium,
            ),
            axis=1,
        )

        max_loss = np.minimum(profit[:, [0]], profit[:, [2]]).astype(float)
        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_x = dist.cdf(strikes)
        pdf_x = dist.pdf(strikes)
        # exp gain = gain*P($<x0) + gain(x0<$<x1)*P(x0<$<x1)
        #           + profit(x1<$<x2)*P(x1<$<x2) + profit(x2<$<x3)*P(x2<$<x3)
        #           + profit(x3<$)*P(x3<$)
        assert np.all(max_loss != 0), (
            "max_loss should never contain a zero"
            + "\nPositions:\n"
            + str(positions)
            + "\nQuantity:\n"
            + str(quantity)
            + "\nStrike Array:\n"
            + str(strikes)
            + "\nPrice_Array:\n"
            + str(prices)
        )

        exp_gain_ltx0 = (1 + (max_risk * profit[:, [0]] / np.abs(max_loss))) * cdf_x[
            :, [0]
        ]
        exp_gain_x1x2 = (1 + (max_risk * profit[:, [1]] / np.abs(max_loss))) * (
            cdf_x[:, [2]] - cdf_x[:, [1]]
        )
        exp_gain_gtx3 = (1 + (max_risk * profit[:, [2]] / np.abs(max_loss))) * (
            1 - cdf_x[:, [3]]
        )
        # calculate the expected price between x0 and x1
        # see block comments at start of function for deriv. of the eq. below.
        #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
        # = (1 + (max_risk/max_loss)*((m*(u-x1)+max profit)*(CDF(X1)-CDF(X0)))
        #        - std**2*(pdf(X1)-pdf(X0)) * (max_risk/max_loss))
        risk_over_invest = max_risk / np.abs(max_loss)
        # slope = 1
        exp_gain_x0x1 = (
            1 + risk_over_invest * (mean - strikes[:, [1]] + profit[:, [1]])
        ) * (cdf_x[:, [1]] - cdf_x[:, [0]]) - (
            std ** 2 * (pdf_x[:, [1]] - pdf_x[:, [0]]) * risk_over_invest
        )

        # calculate the expected price between x2 and x3
        # slope = -1
        exp_gain_x2x3 = (
            1 + risk_over_invest * (strikes[:, [2]] - mean + profit[:, [1]])
        ) * (cdf_x[:, [3]] - cdf_x[:, [2]]) - (
            std ** 2 * (pdf_x[:, [3]] - pdf_x[:, [2]]) * risk_over_invest
        )

        # combine segments
        exp_gain = (
            exp_gain_ltx0
            + exp_gain_x0x1
            + exp_gain_x1x2
            + exp_gain_x2x3
            + exp_gain_gtx3
        )

        return exp_gain

    @staticmethod
    def expected_profit(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        premium = prices.sum(axis=1).reshape(strikes.shape[0], 1)
        profit = np.concatenate(
            (
                (strikes[:, [0]] - strikes[:, [1]]) + premium,
                premium.copy(),
                (strikes[:, [2]] - strikes[:, [3]]) + premium,
            ),
            axis=1,
        )
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        max_loss = np.minimum(profit[:, [0]], profit[:, [2]])
        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_x = dist.cdf(strikes)
        pdf_x = dist.pdf(strikes)
        # exp profit = profit($<x0)*P($<x0) + profit(x0<$<x1)*P(x0<$<x1)
        #           + profit(x1<$<x2)*P(x1<$<x2) + profit(x2<$<x3)*P(x2<$<x3)
        #           + profit(x3<$)*P(x3<$)

        exp_profit_ltx0 = profit[:, [0]] * cdf_x[:, [0]]
        exp_profit_x1x2 = profit[:, [1]] * (cdf_x[:, [2]] - cdf_x[:, [1]])
        exp_profit_gtx3 = profit[:, [2]] * (1 - cdf_x[:, [3]])
        # calculate the expected price between x0 and x1
        # see block comments at start of function for deriv. of the eq. below.
        #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
        slope = 1
        exp_profit_x0x1 = (slope * (mean - strikes[:, [1]]) + profit[:, [1]]) * (
            cdf_x[:, [1]] - cdf_x[:, [0]]
        ) - slope * (std ** 2) * (pdf_x[:, [1]] - pdf_x[:, [0]])
        # calculate the expected price between x2 and x3
        slope = -1
        exp_profit_x2x3 = (slope * (mean - strikes[:, [2]]) + profit[:, [2]]) * (
            cdf_x[:, [3]] - cdf_x[:, [2]]
        ) - slope * (std ** 2) * (pdf_x[:, [3]] - pdf_x[:, [2]])

        # combine segments
        exp_profit = (
            exp_profit_ltx0
            + exp_profit_x0x1
            + exp_profit_x1x2
            + exp_profit_x2x3
            + exp_profit_gtx3
        )

        return exp_profit / (-1 * max_loss)

    @classmethod
    def calc_max_profit(
        cls, positions, quantity, strikes, prices, current_value=np.inf
    ):
        return cls.calc_premium(prices)

    @classmethod
    def calc_init_invest(cls, positions, quantity, strikes, prices):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        init_invest = np.abs(
            np.minimum(
                (strikes[:, [0]] - strikes[:, [1]]), (strikes[:, [2]] - strikes[:, [3]])
            )
            + cls.calc_premium(prices)
        )
        return init_invest

    @classmethod
    def calc_cashflow_sign(cls, prices):
        return [-1, 1, 1, -1]


def createIronCondor_4Prob(
    tckr,
    probabilityLevels,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = [0, 0, 0, 0]
    for iPrice in range(len(estimatedStrikes)):
        closestStrikes[iPrice] = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes[iPrice],
            optionRange,
            "=",
            currentPrice,
        )
    closestStrikes.sort()
    return IronCondor(
        tckr,
        closestStrikes,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def createIronCondor_2InnerProb(
    tckr,
    probabilityLevels,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    estimatedStrikes.sort()
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = []
    # find strike of the put to sell (2nd lowest)
    insideStrike = optionDB.findClosestStrikePrice(
        currentDate, exDate, estimatedStrikes[0], optionRange, "=", currentPrice
    )
    closestStrikes.append(insideStrike)
    # find strike of the put to buy (lowest)
    closestStrikes.append(
        optionDB.findClosestStrikePrice(
            currentDate, exDate, insideStrike, optionRange, "<", currentPrice
        )
    )
    # find strike of the call to sell (2nd highest)
    insideStrike = optionDB.findClosestStrikePrice(
        currentDate, exDate, estimatedStrikes[1], optionRange, "=", currentPrice
    )
    closestStrikes.append(insideStrike)
    # find strike of the put to buy (lowest)
    closestStrikes.append(
        optionDB.findClosestStrikePrice(
            currentDate, exDate, insideStrike, optionRange, ">", currentPrice
        )
    )
    closestStrikes.sort()
    return IronCondor(
        tckr,
        closestStrikes,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def find_IC_HEV(
    tckr,
    purchaseDate,
    exDate,
    offeringCode,
    predictionModel,
    symmetry=False,
    expValStpCnt=500,
    return_all_data=False,
    source="storage",
):
    """
    Uses the known shape of the value function at expiration to
    calculate the expected value of the option at expiration based on the
    predictions of the predictionModel provided.

    Args:
        tckr - underlying security
        purchaseDate - Date of the hypothetical purchase
        exDate - Date that the option is supposed to expire
        offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
            whether to pull strikes for a weekly, monthly, quarterly,
            or yearly offering. The offering code is returned from the
            optionDB.findClosestExDate function call.
        predictionModel - if None then function will use the default
            prediction model in the MarketPredictions module
        expValStpCnt - Used for the approximation method only
        return_all_data - Default is False. If true then return a tuple of
            the best option and a pd.DataFrame of the info for every option
        source - where to pull the stock data. options are 'storage' or
            'temp_db'

    Return:
        pd.Series or (pd.Series, pd.DataFrame) dependent on return_all_data

    ***************************** Iron Condor ***************************
    Provides a credit upfront (positive premium) but has future liabilities
                    Sell P.-0                   Sell C.-0
                             X1-----------------X2
                            /                    \
                Buy P.     /                      \ Buy C.
                ---------X0                       X3---------

    max loss = min((X0-X1), (X2-X3)) + prem
    max profit = premium
    profit = { $ <= X0:         (X0-X1) + prem
               X0 <= $ <= X1:   ($-X1) + prem
               X1 <= $ <= X2:   prem
               X2 <= $ <= X1:   (X2-$) + prem
               $ <= X0:         (X3-X2) + prem}

    signs will indicate case flow
    max loss will be negative signifying negative cash flow
    map profit will be positive signifying positive cash flow
    ---------------------------------------------------------------------
    exp profit = max loss * P(maxloss) + max profit * P(max profit)
                 + integral(profit between X0 and X1 * dist.pdf)

    profit for a price $ between strikes X0 and X1 =
        profit bull call = $ - X0 + prem = VBuC = m * ($ - X1) + max profit
        profit bear call = X0 - $ + prem = VBeC = m * ($ - X1) + max loss
        profit bull put = $ - X1 + prem = VBuP = m * ($ - X1) + max profit
        profit bear put = X1 - $ + prem = VBeP = m * ($ - X1) + max loss
            m = (max profit - max loss) / (X1 - X0) * (-1 if bear else 1)

    from wiki:
    The expectation of X conditioned on the event that X lies in an
    interval [a,b] is given by:
            E(X given a<X<b) = mean - std^2 * (f(b) - f(a)) / (F(b) - F(a))
    where f and F respectively are the density and the cumulative
    distribution function of X

    therefore E(X) bet. a and b but not guaranteed to be be between a and b
    E(x) = E(X given a<X<b) * P(a<X<b)
    E(x) = u*(F(b) - F(a)) + std^2 * (f(b) - f(a))


    integral(P(x)*dx from a to b) = integral(P(x')*dx' from a-u to b-u)
        =  CDF(b) - CDF(a)

    integral(x*P(x)*dx from a to b)
        = u * (CDF(b) - CDF(a)) - std**2 * (pdf(b) - pdf(a))

    example case of bull put option below
    profit($) = m * ($ - X1) + max profit
    gain($) = 1 + max_risk * (m * ($ - X1) + max profit) / max_loss

    integral(profit($) * dist.pdf * dx from X0 to X1)
        = integral(profit($) * P($) from X0 to X1)
        = integral((m * ($ - X1) + max profit) * P($) from X0 to X1)
        = integral((m*$*P($) - m*X1*P($) + max profit*P($))*d$ from X0 to X1)
        = m*u*(CDF(X1)-CDF(X0)) - std**2*(pdf(X1)-pdf(X0))
            - m*X1*(CDF(X1)-CDF(X0)) + max profit*(CDF(X1)-CDF(X0))
        = (m*(u-x1)+max profit)*(CDF(X1)-CDF(X0)) - std**2*(pdf(X1)-pdf(X0))

    integral(gain($) * dist.pdf * dx from X0 to X1)
        = integral(gain($) * P($) from X0 to X1)
        = integral((1 + max_risk * (m * ($ - X1) + max profit) / max_loss)
                        * P($) from X0 to X1)
        = integral(1*P($)+ (max_risk/max_loss)*(
                    m*$*P($) - m*X1*P($) + max profit*P($))*d$) from X0 to X1
        = (CDF(x1)-CDF(x0)) + (max_risk/max_loss)*(
                (m*(u-x1)+max profit)*(CDF(X1)-CDF(X0))
                - std**2*(pdf(X1)-pdf(X0)))
        = ((1 + (max_risk/max_loss)*((m*(u-x1)+max profit)*(CDF(X1)-CDF(X0)))
            - std**2*(pdf(X1)-pdf(X0)) * (max_risk/max_loss)))

    debug:
    downward trending date - pd.datetime(2015, 6, 20)
    tckr = '.SPX'
    spreadType = 'PUT'
    outlook = 'BULL'
    predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
    optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
    purchaseDate = pd.datetime(2006, 6, 20)
    exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                        purchaseDate + pd.Timedelta(days=90))
    currentPrice = None
    contractQty = 1
    expValStpCnt = 500
    modelingType = 'BS'
    contractSize = 100
    positionName = None
    source = 'storage'
    """
    if predictionModel is None:
        predictionModel = getDefaultPredictionModel_HDF5()
    optionDB = OptionData(stockDB=predictionModel.stockDB)
    currentPrice = predictionModel.get_current_price(purchaseDate, tckr)
    # gather all possible strikes
    daysOut = (exDate - purchaseDate).days
    strikeArray = np.array(
        optionDB.estimateSPXStrikesOffered(
            currentPrice, daysOut, offeringCode=offeringCode
        )
    )
    call_price_est = optionDB.PriceEst_BS(
        tckr, "Call", purchaseDate, strikeArray, exDate, S0=currentPrice, source=source
    )
    put_price_est = optionDB.PriceEst_BS(
        tckr, "Put", purchaseDate, strikeArray, exDate, S0=currentPrice, source=source
    )
    # Remove prices less than $0.01
    selectBool = call_price_est > 0.01
    call_strikes = strikeArray[selectBool]
    call_price_est = call_price_est[selectBool]
    selectBool = put_price_est > 0.01
    put_strikes = strikeArray[selectBool]
    put_price_est = put_price_est[selectBool]
    strikes = np.broadcast_arrays(
        put_strikes.reshape((put_strikes.size, 1, 1, 1)),
        put_strikes.reshape((1, put_strikes.size, 1, 1)),
        call_strikes.reshape((1, 1, call_strikes.size, 1)),
        call_strikes.reshape((1, 1, 1, call_strikes.size)),
    )
    # price signs determine which positions are sold and which are bought
    prices = np.broadcast_arrays(
        -1 * put_price_est.reshape((put_price_est.size, 1, 1, 1)),
        1 * put_price_est.reshape((1, put_price_est.size, 1, 1)),
        1 * call_price_est.reshape((1, 1, call_price_est.size, 1)),
        -1 * call_price_est.reshape((1, 1, 1, call_price_est.size)),
    )
    # only keep strike combinations where the strikes are in sequential order
    selectBool = np.logical_and(strikes[1] > strikes[0], strikes[2] > strikes[1])
    selectBool = np.logical_and(selectBool, strikes[3] > strikes[2])
    # Only keep the IC possibilities that have a net premium > $0.03
    # If symmetry is enforced, remove the asymmetric possibilities
    selectBool = np.logical_and(
        selectBool, (prices[0] + prices[1] + prices[2] + prices[3]) > 0.03
    )
    if symmetry:
        selectBool = np.logical_and(
            selectBool, strikes[1] - strikes[0] == strikes[3] - strikes[2]
        )
    valid_qty = selectBool.sum()
    x0 = strikes[0][selectBool]
    x1 = strikes[1][selectBool]
    x2 = strikes[2][selectBool]
    x3 = strikes[3][selectBool]
    p0 = prices[0][selectBool]
    p1 = prices[1][selectBool]
    p2 = prices[2][selectBool]
    p3 = prices[3][selectBool]
    premium = p0 + p1 + p2 + p3
    del strikes, prices, selectBool

    max_loss = np.minimum((x0 - x1), (x2 - x3)) + premium
    max_profit = premium

    if symmetry:
        profit_x0 = profit_x3 = max_loss
    else:
        profit_x0 = x0 - x1 + premium
        profit_x3 = x2 - x3 + premium
    profit_x1 = profit_x2 = premium

    # get price prediction info
    dist = predictionModel.get_prob_dist(purchaseDate, exDate, tckr)
    mean = dist.mean()
    std = dist.std()
    cdf_x0, cdf_x1, cdf_x2, cdf_x3 = np.hsplit(
        dist.cdf(np.r_[x0, x1, x2, x3]).reshape(4, valid_qty).T, 4
    )
    cdf_x0 = cdf_x0.squeeze()
    cdf_x1 = cdf_x1.squeeze()
    cdf_x2 = cdf_x2.squeeze()
    cdf_x3 = cdf_x3.squeeze()
    # exp profit = profit($<x0)*P($<x0) + profit(x0<$<x1)*P(x0<$<x1)
    #              + profit(x1<$<x2)*P(x1<$<x2) + profit(x2<$<x3)*P(x2<$<x3)
    #              + profit(x3<$)*P(x3<$)
    exp_profit_ltx0 = profit_x0 * cdf_x0
    exp_profit_x1x2 = profit_x1 * (cdf_x2 - cdf_x1)
    exp_profit_gtx3 = profit_x3 * (1 - cdf_x3)
    # calculate the expected price between x0 and x1
    # see block comments at start of function for derivation of the eq. below.
    #  exp_profit_between = integral(profit($) * P($) * d$ from X0 to X1)
    slope = 1
    exp_profit_x0x1 = slope * std / np.sqrt(2 * np.pi) * (
        np.exp(-((x0 - mean) ** 2) / (2 * (std ** 2)))
        - np.exp(-((x1 - mean) ** 2) / (2 * (std ** 2)))
    ) + (slope * mean - slope * x1 + profit_x1) * (cdf_x1 - cdf_x0)
    # calculate the expected price between x2 and x3
    slope = -1
    exp_profit_x2x3 = slope * std / np.sqrt(2 * np.pi) * (
        np.exp(-((x2 - mean) ** 2) / (2 * (std ** 2)))
        - np.exp(-((x3 - mean) ** 2) / (2 * (std ** 2)))
    ) + (slope * mean - slope * x2 + profit_x2) * (cdf_x3 - cdf_x2)
    # combine segments
    exp_profit = (
        exp_profit_ltx0
        + exp_profit_x0x1
        + exp_profit_x1x2
        + exp_profit_x2x3
        + exp_profit_gtx3
    )

    exp_return = exp_profit / (-1 * max_loss)
    column_order = [
        "Strategy",
        "Category",
        "Data_Source",
        "PurchaseDate",
        "ExpirationDate",
        "Strike_0",
        "Strike_1",
        "Strike_2",
        "Strike_3",
        "Price_0",
        "Price_1",
        "Price_2",
        "Price_3",
        "Premium",
        "Max_Loss",
        "Max_Profit",
        "Expected_Profit",
        "Expected_Return",
        "Expected_Return_PerDay",
        "ReturnVsStock",
    ]
    bestIndex = exp_return.argmax()
    return_of_stock = predictionModel.expected_return_of_stock(
        tckr, purchaseDate, exDate, source=source
    )
    bestOption = {
        "Strategy": "Iron Condor",
        "Category": "Iron Condor",
        "Data_Source": "BS",
        "PurchaseDate": purchaseDate,
        "ExpirationDate": exDate,
        "Strike_0": x0[bestIndex],
        "Strike_1": x1[bestIndex],
        "Strike_2": x2[bestIndex],
        "Strike_3": x3[bestIndex],
        "Price_0": p0[bestIndex],
        "Price_1": p1[bestIndex],
        "Price_2": p2[bestIndex],
        "Price_3": p3[bestIndex],
        "Premium": premium[bestIndex],
        "Max_Loss": max_loss[bestIndex],
        "Max_Profit": max_profit[bestIndex],
        "Expected_Profit": exp_profit[bestIndex],
        "Expected_Return": exp_return[bestIndex],
        "Expected_Return_PerDay": (exp_return[bestIndex] / daysOut),
        "ReturnVsStock": (exp_return[bestIndex] / return_of_stock),
    }
    bestOption = pd.Series(data=bestOption)[column_order]
    bestOption.name = "Best " + bestOption.Category
    output = bestOption

    if return_all_data:
        # create the results pd.DF for comparison
        column_order = [
            "Strategy",
            "Category",
            "Data_Source",
            "PurchaseDate",
            "ExpirationDate",
            "Strike_0",
            "Strike_1",
            "Strike_2",
            "Strike_3",
            "Price_0",
            "Price_1",
            "Price_2",
            "Price_3",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Expected_Profit",
            "Expected_Return",
            "Expected_Return_PerDay",
            "ReturnVsStock",
        ]
        options = pd.DataFrame(index=range(valid_qty), columns=column_order)
        options["Strategy"] = "Iron Condor"
        options["Category"] = "Iron Condor"
        options["Data_Source"] = "BS"
        options["PurchaseDate"] = purchaseDate
        options["ExpirationDate"] = exDate
        options["Strike_0"] = x0
        options["Strike_1"] = x1
        options["Strike_2"] = x2
        options["Strike_3"] = x3
        options["Price_0"] = p0
        options["Price_1"] = p1
        options["Price_2"] = p2
        options["Price_3"] = p3
        options["Premium"] = premium
        options["Max_Loss"] = max_loss
        options["Max_Profit"] = max_profit
        options["Expected_Profit"] = exp_profit
        options["Expected_Return"] = exp_return
        options["Expected_Return_PerDay"] = exp_return / daysOut
        options["ReturnVsStock"] = exp_return / return_of_stock
        options = options[column_order]
        output = bestOption, options

    return output
