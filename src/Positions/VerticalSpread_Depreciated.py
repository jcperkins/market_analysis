"""
-------------------------------------------------------------------------------
 Name:        VerticalSpread
 Purpose:      File dedicated to the IronCondor class and functions that involve
                 the IC strategy

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""

import numpy as np

# import pandas as pd
# from scipy import stats
from src.DataSource import OptionData
from Positions import SimpleOption
from Positions.ComplexOption import ComplexOption


class VerticalSpread(ComplexOption):
    def __init__(
        self,
        tckr,
        strikes,
        optionType,
        purchaseDate,
        exDate,
        contractQty,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        """******************************Vertical spreads******************************
                Bullish Put/Call Spread        #       Bearish Put/Call Spread
                                Buy P.-0       #  Buy P.
                            X1---------        #  --------X0
                           /  Sell C.          #  Sell C.-0 \
                Sell P.   /                    #             \ Sell P.-0
                ---------X0                    #             X1---------
                Buy C.-0                       #                   Buy C.
                                               #
            $ = np.clip( price, X0, X1)        # $ = np.clip( price, X0, X1)
            Value call = $ - X0 + prem = VBuC  # Value call = X0 - $ + prem = VBeC
            Value put = $ - X1 + prem = VBuP   # Value put = X1 - $ + prem = VBeP
                            VBuC = -1*VBeC; VBuP = -1*VBeP

            o  strikes are the two strike amounts for the vertical spread
            o  optionType is one of ['Bullish Put Spread', 'Bearish Put Spread', 'Bullish Call Spread', 'Bearish Call Spread']
            o  contractQty is the number of Vertical Spread contracts to buy and not neccessarily
                the number of calls or puts that you have in each posisition
            o  everything else should be self explanitory
        """
        self.strikes = strikes
        self.strikes.sort()
        self.exDate = exDate
        self.optionType = optionType.title()
        self.optionDB = optionDB if optionDB is not None else OptionData()
        self.optionInit = (
            SimpleOption.Call if "Call" in self.optionType else SimpleOption.Put
        )
        lowStrikeQty, highStrikeQty = {
            "Bullish Put Spread": (-1, 1),
            "Bearish Put Spread": (1, -1),
            "Bullish Call Spread": (1, -1),
            "Bearish Call Spread": (-1, 1),
        }[optionType.title()]
        self.positions = []
        if "Call" in self.optionType:
            calls, puts = self.positions, []
        else:
            calls, puts = [], self.positions
        self.positions.append(
            self.optionInit(
                tckr,
                strikes[0],
                exDate,
                lowStrikeQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="VS_%s_1" % optionType.title(),
                optionDB=self.optionDB,
                source=source,
            )
        )
        self.positions.append(
            self.optionInit(
                tckr,
                strikes[1],
                exDate,
                highStrikeQty,
                modelingType=modelingType,
                contractSize=contractSize,
                positionName="VS_%s_1" % optionType.title(),
                optionDB=self.optionDB,
                source=source,
            )
        )

        if positionName is None:
            positionName = tckr + "_%s" % self.optionType
        ComplexOption.__init__(
            self,
            tckr,
            calls,
            puts,
            contractQty,
            self.optionType,
            modelingType=modelingType,
            contractSize=contractSize,
            positionName=positionName,
            optionDB=optionDB,
            source=source,
        )
        self.positions = self.calls if "Call" in self.optionType else self.puts

    def calculateValue_Excised(self, date, price=None):
        price = price if price is not None else self.getUnderlyingPrice(date)
        limitedPrice = np.clip(price, self.strikes[0], self.strikes[1])
        if self.optionType == "Bullish Put Spread":
            value = limitedPrice - self.strikes[1]
        elif self.optionType == "Bearish Put Spread":
            value = self.strikes[1] - limitedPrice
        elif self.optionType == "Bullish Call Spread":
            value = limitedPrice - self.strikes[0]
        elif self.optionType == "Bearish Call Spread":
            value = self.strikes[0] - limitedPrice
        return value * self.qty

    def MaxRisk_NoPremiums(self, currentDate=None, price=None):
        if self.optionType in ["Bullish Put Spread", "Bearish Call Spread"]:
            maxRiskBeforePremium = np.abs(self.strikes[1] - self.strikes[0])
        else:  # (optionType in ['Bearish Put Spread', 'Bullish Call Spread'):
            maxRiskBeforePremium = 0
        return maxRiskBeforePremium * self.qty


def createVerticalSpread_2Prob(
    tckr,
    probabilityLevels,
    spreadType,
    outLook,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    """Initiliazes a vertical spread option position based on the two probabilities indicated
    """
    assert len(probabilityLevels) >= 2, "Not enough probabilities provided"
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)

    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = [0, 0]
    for iPrice in range(len(estimatedStrikes)):
        closestStrikes[iPrice] = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes[iPrice],
            optionRange,
            "=",
            currentPrice,
        )
    closestStrikes.sort()
    return VerticalSpread(
        tckr,
        closestStrikes,
        spreadType,
        outLook,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def createVerticalSpread_1Prob(
    tckr,
    probabilityLevels,
    applyProb,
    spreadType,
    outLook,
    currentDate,
    exDate,
    contractQty,
    predictionModel,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    """Initiliazes a vertical spread option position with the upper or lower strike (depending on applyProb) and the remaining strike selected to be the next available strike
        applyProb is either 'High' or 'Low' and the larger or lower strike value will be assigned the strike
            value associtated with the probability respectively
    """
    assert len(probabilityLevels) >= 1, "Not enough probabilities provided"
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    estimatedStrikes = predictionModel.get_price_from_cum_prob(
        probabilityLevels,
        current_date=currentDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
    )
    exDate, optionRange = optionDB.findClosestExDate(
        currentDate, exDate, expirationCondition="="
    )
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    closestStrikes = []
    # find strike price assotiated with the probability
    strikeCondition = "="
    try:
        probabilityStrike = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes,
            optionRange,
            strikeCondition,
            currentPrice,
        )
    except:
        probabilityStrike = optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes.values[0],
            optionRange,
            strikeCondition,
            currentPrice,
        )
        estimatedStrikes = estimatedStrikes.values[0]
    closestStrikes.append(probabilityStrike)
    strikeCondition = "<" if applyProb.upper() == "HIGH" else ">"
    closestStrikes.append(
        optionDB.findClosestStrikePrice(
            currentDate,
            exDate,
            estimatedStrikes,
            optionRange,
            strikeCondition,
            currentPrice,
        )
    )
    closestStrikes.sort()
    return VerticalSpread(
        tckr,
        closestStrikes,
        spreadType,
        outLook,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def createVerticalSpread_HEVForDate(
    tckr,
    spreadType,
    currentDate,
    exDate,
    offeringCode,
    contractQty,
    predictionModel,
    expValStpCnt=500,
    modelingType="BS",
    contractSize=100,
    positionName=None,
    optionDB=None,
    source="storage",
):
    """Finds the strike values for the highest expected value of a verticle spread if excised on the exDate predicted by the prediction model
        offeringCode is the optionRange returned by OptionData.getClosestSPXDate
    """
    if optionDB is None:
        optionDB = OptionData(stockDB=predictionModel.stockDB)
    #    exDateDF = optionDB.estimateSPXOptionDates(current_date,mon=False,wed=False)
    currentPrice = predictionModel.get_current_price(currentDate, tckr)
    # find the optimized spread on the low side of the IC

    maxReturnPerRisk, maxReturnPerRiskPerDay, lowStrike, highStrike, netPremium, breakEvenPrice, profitProbability = find_HEV_VerticalSpread_SingleDate(
        tckr,
        spreadType,
        currentDate,
        exDate,
        offeringCode,
        predictionModel,
        expValStpCnt=expValStpCnt,
        currentPrice=currentPrice,
        optionDB=optionDB,
        source=source,
    )

    return VerticalSpread(
        tckr,
        [lowStrike, highStrike],
        spreadType,
        currentDate,
        exDate,
        contractQty,
        modelingType=modelingType,
        contractSize=contractSize,
        positionName=positionName,
        optionDB=optionDB,
        source=source,
    )


def find_HEV_VerticalSpread_SingleDate(
    tckr,
    spreadType,
    purchaseDate,
    exDate,
    offeringCode,
    predictionModel,
    probabilityCutOff=0.5,
    expValStpCnt=500,
    currentPrice=None,
    optionDB=None,
    source="strorage",
):
    """Finds the strike values for the highest expected value of a verticle spread if excised on the exDate predicted by the prediction model
        o  spreadType is one of ['Bullish Put Spread', 'Bearish Put Spread', 'Bullish Call Spread', 'Bearish Call Spread']
            Bullish Put/Call Spread   #  Bearish Put/Call Spread
                          Sell P.-0   #  Sell P.
                         X1---------  #  --------X0
                        /  Sell C.    #  Sell C.-0 \
            Buy P.     /              #             \ Buy P.-0
            ---------X0               #             X1---------
            Buy C.-0                  #                  Buy C.

    """
    optionDB = (
        optionDB
        if optionDB is not None
        else OptionData(stockDB=predictionModel.stockDB)
    )
    currentPrice = (
        currentPrice
        if currentPrice is not None
        else predictionModel.get_current_price(purchaseDate, tckr, source=source)
    )
    spreadType = spreadType.title()
    assert spreadType in [
        "Bullish Put Spread",
        "Bearish Put Spread",
        "Bullish Call Spread",
        "Bearish Call Spread",
    ], (
        "the variable 'spreadType' is '%s', but must be one of ['Bullish Put Spread', 'Bearish Put Spread', 'Bullish Call Spread', 'Bearish Call Spread']"
        % spreadType
    )
    # gather all possible strikes
    daysOut = (exDate - purchaseDate).days
    strikeArray = np.array(
        optionDB.estimateSPXStrikesOffered(
            currentPrice, daysOut, offeringCode=offeringCode
        )
    )

    # optimize the lower verticle spread
    # ------------------------------------
    # define the 1st and 2nd strike and shape in such a way that the coordinates represent
    # (strikes[0],strikes[1],futurePrices). The third coordinate won't be used until the
    # expected value portion of code later on. strike[1] should always be greater than strike[0]
    strikes_0 = strikeArray.reshape(strikeArray.size, 1, 1)
    strikes_1 = strikeArray.reshape(1, strikeArray.size, 1)
    # maxRiskBeforePremiums = strikes_0 - strikes_1        -- Negative value
    # get the price of each option and then define arrays that will represent every combination corresponding to the strikes
    # These can be subtracted to calculate the premium recieved for each spread combination
    optionType = "C" if "Call" in spreadType else "P"
    sign_0, sign_1 = {
        "Bullish Put Spread": (-1, 1),
        "Bearish Put Spread": (1, -1),
        "Bullish Call Spread": (-1, 1),
        "Bearish Call Spread": (1, -1),
    }[spreadType.title()]
    priceEst_BS = optionDB.PriceEst_BS(
        tckr,
        optionType,
        purchaseDate,
        strikeArray,
        exDate,
        S0=currentPrice,
        source=source,
    )
    # prices_0 = sign_0*priceEst_BS.reshape(priceEst_BS.size,1,1)
    # prices_1 = sign_1*priceEst_BS.reshape(1,priceEst_BS.size,1)
    # netPremium = prices_1 + prices_0
    netPremium = sign_1 * priceEst_BS.reshape(
        1, priceEst_BS.size, 1
    ) + sign_0 * priceEst_BS.reshape(priceEst_BS.size, 1, 1)
    # calculate the expectedValue of the Option
    # get an array of possible prices and calculate the probability of the price based on the prediction model
    minPrice, maxPrice = predictionModel.get_price_from_cum_prob(
        [0.001, 0.999],
        current_date=purchaseDate,
        predicted_date=exDate,
        tckr=tckr,
        direction="Below",
        source=source,
    )
    priceStep = (maxPrice - minPrice) / expValStpCnt
    futurePriceArray = np.arange(minPrice, maxPrice, priceStep)
    probOfPrice = predictionModel.get_prob_of_price(
        futurePriceArray,
        current_date=purchaseDate,
        predicted_date=exDate,
        tckr=tckr,
        source=source,
    ).reshape(1, 1, futurePriceArray.size)
    # calculate the value of the spread if the options were excised at the prices in priceArray
    # take advantage of broadcasting used behind the curtain in numpy instead of np.broadcast_to() each for each array
    # only variation in value comes between the strike prices
    limitedPrice = np.clip(
        futurePriceArray.reshape(1, 1, futurePriceArray.size), strikes_0, strikes_1
    )
    # value is the amount if excised for the future price plus the premium
    if spreadType == "Bullish Put Spread":
        value = limitedPrice - strikes_1 + netPremium
        breakEvenPrice = strikes_1 - netPremium
        profitableProbability = predictionModel.get_cum_prob_from_price(
            breakEvenPrice,
            current_date=purchaseDate,
            predicted_date=exDate,
            tckr=tckr,
            direction="Above",
            source=source,
        )
    elif spreadType == "Bearish Put Spread":
        value = strikes_1 - limitedPrice + netPremium
        breakEvenPrice = strikes_1 + netPremium
        profitableProbability = predictionModel.get_cum_prob_from_price(
            breakEvenPrice,
            current_date=purchaseDate,
            predicted_date=exDate,
            tckr=tckr,
            direction="Below",
            source=source,
        )
    elif spreadType == "Bullish Call Spread":
        value = limitedPrice - strikes_0 + netPremium
        breakEvenPrice = strikes_0 - netPremium
        profitableProbability = predictionModel.get_cum_prob_from_price(
            breakEvenPrice,
            current_date=purchaseDate,
            predicted_date=exDate,
            tckr=tckr,
            direction="Above",
            source=source,
        )
    else:  # (spreadType== 'Bearish Call Spread'):
        value = strikes_0 - limitedPrice + netPremium
        breakEvenPrice = strikes_0 + netPremium
        profitableProbability = predictionModel.get_cum_prob_from_price(
            breakEvenPrice,
            current_date=purchaseDate,
            predicted_date=exDate,
            tckr=tckr,
            direction="Below",
            source=source,
        )

    # calculate expected value which is the sum( (probability of the price)*(value at that price)*(priceStep) )
    expValue = (value * probOfPrice * priceStep).sum(axis=2)

    # find the greatest expValu per dollar that can be lost and start by converting all strikes and prices to 2D
    strikes_0 = strikes_0.reshape((strikes_0.shape[0], strikes_0.shape[1]))
    strikes_1 = strikes_1.reshape((strikes_1.shape[0], strikes_1.shape[1]))
    netPremium = netPremium.reshape((netPremium.shape[0], netPremium.shape[1]))
    breakEvenPrice = breakEvenPrice.reshape(
        (breakEvenPrice.shape[0], breakEvenPrice.shape[1])
    )
    profitableProbability = profitableProbability.reshape(
        (profitableProbability.shape[0], profitableProbability.shape[1])
    )
    # deterimine which conditions are valid and which are false and create condition to value key
    validConditions = np.logical_and(
        strikes_0 < strikes_1, profitableProbability > probabilityCutOff
    )
    indexKey, columnKey = np.broadcast_arrays(strikes_0, strikes_1)
    strikeConditionKey = np.stack([indexKey.ravel(), columnKey.ravel()]).T
    # maxLoss = maskRiskBeforePremium -spreadPremium (positive premium indicates less max loss)
    if spreadType in ["Bullish Put Spread", "Bearish Call Spread"]:
        maskRiskBeforePremium = np.abs(strikes_1 - strikes_0)
    else:  # (spreadType in ['Bearish Put Spread', 'Bullish Call Spread'):
        maskRiskBeforePremium = 0
    maxLoss = maskRiskBeforePremium - netPremium
    expValuePerRisk = expValue[validConditions] / maxLoss[validConditions]
    strikeConditionKey = strikeConditionKey[validConditions.ravel(), :]
    #    expValuePerRiskPerDay = expValuePerRisk / daysOut
    #    strikeConditionExpReturns = np.stack([indexKey[validConditions], columnKey[validConditions], maxLoss[validConditions],expValue[validConditions],expValue[validConditions]/maxLoss[validConditions]]).T; expValueData = pd.DataFrame(data=strikeConditionExpReturns,columns=['X0','X1','MaxLoss','ExpValue','ExpValuePerRisk'])
    optimizedIndex = expValuePerRisk.argmax()
    optimizedExpReturnPerDollarRisked = expValuePerRisk[optimizedIndex]
    lowStrike = strikeConditionKey[optimizedIndex, 0]
    highStrike = strikeConditionKey[optimizedIndex, 1]
    optimizedNetPremium = netPremium[validConditions][optimizedIndex]
    optimizedBreakEvenPrice = breakEvenPrice[validConditions][optimizedIndex]
    optimizedProbability = profitableProbability[validConditions][optimizedIndex]

    del (
        limitedPrice,
        value,
        indexKey,
        columnKey,
        maxLoss,
        maskRiskBeforePremium,
        netPremium,
        strikeConditionKey,
        profitableProbability,
        breakEvenPrice,
        expValuePerRisk,
        validConditions,
        strikes_0,
        strikes_1,
    )

    return (
        optimizedExpReturnPerDollarRisked,
        optimizedExpReturnPerDollarRisked / daysOut,
        lowStrike,
        highStrike,
        optimizedNetPremium,
        optimizedBreakEvenPrice,
        optimizedProbability,
    )
