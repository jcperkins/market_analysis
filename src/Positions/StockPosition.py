"""
#-------------------------------------------------------------------------------
# Name:        StockPositions
# Purpose:     File for gathering and organizing classes of stock position
#              position types
#
# Author:      Cory Perkins
#
# Created:     19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
"""
import pandas as pd
from MarketAnalysis.DataSource import StockData
import src.Utility as util


class StockPosition:
    def __init__(self, tckr, qty, stockDB=None, source="storage"):
        self.tckr = util.convertTckrToUniversalFormat(tckr)
        self.qty = qty
        # self.price = self.getPrice()
        self.lastPriceDate = None
        self.lastPrice = None
        self.stockDB = stockDB
        self.source = source.upper()
        if stockDB is not None:
            self.stockDB = StockData()

    def calculateValue(self, date, reInvestDiv=False):
        """
        Input:
                o  date can be a single date or a list of dates. Behaviour
                    mimic the stockDB's getPrice behavior
                o  reInvestDiv is a flag to indicate whether to calculate
                    as if dividend returns are absorbed into the value of
                    the position as additional qty or calculated as
                    additional cash
            Returns:
                o  the value of the position for each date, the cash value
                    of the dividend returns ()
                o  resulting cash for each date (cumulative representation
                    from the first date given)
                o  the actual qty in the position for each date adjusted
                    for dividend reinvestment
        """
        date = util.formatDateInput(date)
        close = self.getPrice(date)
        if reInvestDiv:
            if type(date) == pd.DatetimeIndex:
                adjClose = self.getPrice(date, adjusted=True)
                startDiv = self.getDividendPerShare(date.min())
                startQty = self.qty * (1 + startDiv / close[date.min()])
                newQty = (adjClose / close) / (adjClose[0] / close[0]) * startQty
                value = close * newQty
                divReturn = adjClose * 0
            else:
                divReturn = self.getDividendPerShare(date)
                newQty = self.qty * (1 + divReturn / close)
                value = close * newQty
                divReturn = 0
        else:
            value = self.getPrice(date) * self.qty
            divReturn = (self.getDividendPerShare(date) * self.qty).cumsum()
            newQty = close * 0 + self.qty
        return value, divReturn, newQty

    def addQty(self, qty):
        self.qty += qty
        return self.qty

    def getPrice(self, date, adjusted=False):
        return self.stockDB.getPrice(
            self.tckr, date, adjusted=adjusted, source=self.source
        )

    def getDividendPerShare(self, date):
        return self.stockDB.getColumnValueForEachDate(
            self.tckr, date, "Dividends", source=self.source
        )

    def getValidRows(self, date):
        """
        if self.source is set to storage' then return the last row from
        the db storage table, if not, then return the last date in the
        index <= date
        """
        return self.stockDB.getValidRows(self.tckr, date, source=self.source)
