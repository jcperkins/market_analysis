"""
-------------------------------------------------------------------------------
 Name:        ComplexOption
 Purpose:      File for gathering and organizing classes of option position
                 types
 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import time
from numba import jit
from src.DataSource import OptionData
from src.Model import getDefaultPredictionModel_HDF5
import src.Utility as Util


class ComplexOption(object):
    def __init__(
        self,
        tckr,
        calls,
        puts,
        contractQty,
        optionType,
        modelingType="BS",
        contractSize=100,
        positionName=None,
        optionDB=None,
        source="storage",
    ):
        self.optionType = optionType.title()
        self.calls = calls
        self.puts = puts
        self.tckr = Util.convertTckrToUniversalFormat(tckr)
        self.qty = contractQty * contractSize
        self.contractSize = contractSize
        self.contracts = contractQty
        self.modelingType = modelingType
        self.source = source.upper()
        if positionName is None:
            self.name = tckr.replace("ix", "") + "_%s" % self.optionType
        else:
            self.name = positionName
        if optionDB is None:
            self.optionDB = OptionData()
        else:
            self.optionDB = optionDB
        self.stockDB = self.optionDB.stockDB
        self.liborDB = self.optionDB.liborDB

    def calculateValue(self, date, price=None):
        if self.modelingType == "BS":
            method = "BS"
        else:
            method = "EXCISE"
        return self.calculateValue_Single(date, method, price=price) * self.contracts

    def calculateValue_Single(self, date, method, price=None):
        value = 0
        for subPosition in self.calls + self.puts:
            if method == "BS":
                value += subPosition.calculateValue_BS(date)
            else:
                value += subPosition.calculateValue_Excised(date, price=price)
        return value

    def calculateValue_Excised(self, date, price=None):
        return self.calculateValue_Single(date, "EXCISE", price=price) * self.contracts

    def calculateValueOfContract_Excised(self, date, price=None):
        return (
            self.calculateValue_Single(date, "EXCISE", price=price) * self.contractSize
        )

    def calculateValue_BS(self, date):
        return self.calculateValue_Single(date, "BS") * self.contracts

    def calculateValueOfContract_BS(self, date):
        return self.calculateValue_Single(date, "BS") * self.contractSize

    def addQty(self, qty):
        self.qty += qty
        self.contracts += qty / self.contractSize
        return self.qty

    def addContracts(self, contracts):
        self.qty += contracts * self.contractSize
        self.contracts += contracts
        return self.qty

    def getUnderlyingPrice(self, date, adjusted=False):
        return self.stockDB.getPrice(
            self.tckr, date, adjusted=adjusted, source=self.source
        )

    def getValidStockRow(self, date):
        """
        if self.source is set to 'STORAGE' then return the last row from the
        db storage table, if not, then return the last date in
        temp_db.index <= date
        """
        return self.stockDB.getValidRows(self.tckr, date, source=self.source)

    def MaxRisk_NoPremiums(self, currentDate=None, price=None):
        risk = 0
        for subPosition in self.calls + self.puts:
            risk += subPosition.MaxRisk_NoPremiums(currentDate=currentDate, price=price)
        return risk

    def expectedValue_AtExpiration(self, currentDate, predictionModel, stepCnt=500):
        minPrice, maxPrice = predictionModel.get_price_from_cum_prob(
            [0.001, 0.999],
            current_date=currentDate,
            predicted_date=self.exDate,
            tckr=self.tckr,
            direction="Below",
            source=self.source,
        )
        priceStep = (maxPrice - minPrice) / stepCnt
        priceArray = np.arange(minPrice, maxPrice, priceStep)
        probabilityOfPrice = predictionModel.get_prob_of_price(
            priceArray,
            current_date=currentDate,
            predicted_date=self.exDate,
            tckr=self.tckr,
            source=self.source,
        )
        profit = self.calculateValue_Excised(None, price=priceArray)
        expectedValue = (profit * probabilityOfPrice * priceStep).sum()
        return expectedValue * self.qty

    @classmethod
    def monte_carlo(
        cls,
        tckr,
        purchaseDate,
        exDate,
        offeringCode,
        predictionModel,
        strategy="+P -P",
        symmetry=False,
        method="Estimate",
        lifetime=1,
        n_mc=1000,
        cum_cycles=20,
        max_risk=0.2,
        strikes=None,
        prices=None,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        """
        Parent monte_carlo function that will set up and execute the other
        functions

        Args:
            tckr - the name of the underlying security
            spreadtype - 'Call' or 'Put'
            outlook - 'Bull' (positive) or 'Bear' (negative)
            purchaseDate - the last date that the functions will pull
                historical data for
            exDate - If a single date then call the appropriate function for
                        the date.
                     If a range of dates, then find the optimal
                        option for each date.
                     If 'Estimate' then estimate based on the range determined
                        by the offering code
                     If 'Fidelity' then pull all the exDates available in the
                        fidelity options DB for the give purchase date. If no
                        date matches then default to 'Estimate'
            offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
                whether to pull strikes for a weekly, monthly, quarterly,
                or yearly offering. The offering code is returned from the
                optionDB.findClosestExDate function call.
            predictionModel - if None then function will use the default
                prediction model in the MarketPredictions module
            lifetime - the fraction of the option lifetime that the option will
                be held before it is sold. If 'expire' or 1 then hold the
                option until it expires
            n_mc - the number of times each option combination is modeled
            cum_cycles - the number of loops to test the option combination
                so that the true return can be approximated if you have both
                good and bad prices
            max_risk - the maximum amount of the portfolio that will be risked
                in the option strategies
            total_pf - value of the total portfolio that the strategy will be
                applied too
            broker_fee - the price of the first option contract
                (or stock purchase) charged by the broker for the transaction
            addtl_option_fee - the additional fee charged by the broker
                for each contract after the initial contract
            return_all_data - Default is False. If true then return a tuple of
                the best option and a pd.DataFrame of the info for every option
            source - where to pull the stock data. options are 'storage' or
                'temp_db'

        debug code:
        tckr = '.SPX'
        spreadType = 'PUT'
        method='Fidelity'
        outlook = 'BULL'
        predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
        optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
        purchaseDate = pd.datetime(2006, 6, 20)
        purchaseDate = pd.datetime(2018, 1, 9)
        exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                purchaseDate + pd.Timedelta(days=90))
        exDate = offeringCode = None
        currentPrice = None
        contractQty = 1
        expValStpCnt = 500
        modelingType = 'BS'
        contractSize = 100
        positionName = None
        source = 'storage'
        symmetry=False
        max_risk=0.2
        n_mc = 100
        cum_cycles = 10
        lifetime = 1
        """
        if predictionModel is None:
            predictionModel = getDefaultPredictionModel_HDF5()
        optionDB = OptionData(stockDB=predictionModel.stockDB)
        purchaseDate = Util.formatDateInput(purchaseDate)
        positions, cashflow_sign, quantity = cls.interpret_strategy(strategy)
        legs = len(positions)
        if offeringCode is not None:
            offeringCode = offeringCode.upper()
        best_list = []
        if "FID" in method.upper():
            # query the fidelity options database and find the closest
            #  query time to the purchase date.
            fid_data = optionDB.options_for_date(tckr, purchaseDate, method="closest")
            # remove exDate less than 3 days away
            fid_data = fid_data.loc[fid_data.DaysToExpiration > 2, :]
            fid_data = optionDB.summarize_option_data(fid_data, price_calc="weighted")

            if exDate is None or type(exDate) == str:
                all_exdates = pd.DatetimeIndex(list(set(fid_data.ExDate)))
            else:
                all_exdates = pd.DatetimeIndex(list(set(fid_data.ExDate)))
                all_exdates = Util.formatDateArray(
                    [
                        all_exdates[np.abs(all_exdates - date).argmin()]
                        for date in Util.formatDateArray(exDate)
                    ]
                )

            for i_date in range(all_exdates.size):
                loop_start = time.time()
                # debug: i_date = 7
                exDate_i = all_exdates[i_date]
                code_i = None
                days_out = (exDate_i - purchaseDate).days
                info_call = fid_data.loc[
                    np.logical_and(
                        fid_data.ExDate == exDate_i, fid_data.Type == "Call"
                    ),
                    ["Strike", "Ask", "Bid"],
                ].drop_duplicates()
                info_put = fid_data.loc[
                    np.logical_and(fid_data.ExDate == exDate_i, fid_data.Type == "Put"),
                    ["Strike", "Ask", "Bid"],
                ].drop_duplicates()
                strikes = []
                prices = []
                for i in range(legs):
                    if positions[i] == "Call":
                        strikes.append(info_call.Strike.values)
                        if cashflow_sign[i] == -1:
                            prices.append(-1 * info_call.Ask.values)
                        else:
                            prices.append(info_call.Bid.values)
                    else:
                        strikes.append(info_put.Strike.values)
                        if cashflow_sign[i] == -1:
                            prices.append(-1 * info_put.Ask.values)
                        else:
                            prices.append(info_put.Bid.values)
                print(
                    "Analyzing expiration date %1i of %1i - %s "
                    % (i_date + 1, all_exdates.size, str(exDate_i))
                    + "(%1i days, %1i strikes)" % (days_out, info_call.Strike.size)
                )
                best_in_date = cls.monte_carlo_singe_date(
                    tckr,
                    purchaseDate,
                    exDate_i,
                    code_i,
                    predictionModel,
                    strategy,
                    lifetime=lifetime,
                    n_mc=n_mc,
                    cum_cycles=cum_cycles,
                    max_risk=max_risk,
                    strikes=strikes,
                    prices=prices,
                    total_pf=total_pf,
                    broker_fee=broker_fee,
                    addtl_option_fee=addtl_option_fee,
                    return_all_data=False,
                    source=source,
                )
                best_list.append(best_in_date)
                print("Completed in %s seconds" % (str(time.time() - loop_start)))
            if all_exdates.size == 1:
                output = best_list[0]
            else:
                output = pd.concat(best_list, axis=1, ignore_index=True).T
                output = output.sort_values("Avg_Ann_Return", ascending=False).reindex(
                    range(output.index.size)
                )
        else:
            # elif 'EST' in method.upper():
            if exDate is None or type(exDate) == str:
                # Estimate all options that would be available at that time
                if offeringCode is None or offeringCode.upper() == "ALL":
                    weekly = monthly = quarterly = yearly = True
                else:
                    weekly = "W" in offeringCode
                    monthly = "M" in offeringCode
                    quarterly = "Q" in offeringCode
                    yearly = "Y" in offeringCode
                all_exdates = optionDB.estimateSPXOptionDates(
                    purchaseDate,
                    weekly=weekly,
                    mon=False,
                    wed=False,
                    fri=True,
                    monthly=monthly,
                    quarterly=quarterly,
                    yearly=yearly,
                )
            else:
                # assume a single date or a list of dates and force into a seq.
                all_exdates = Util.formatDateArray(exDate)

            if type(all_exdates) == pd.DataFrame:
                offeringCode = all_exdates.values.ravel()
                all_exdates = all_exdates.index.ravel()
            elif type(offeringCode) in [pd.Index, list, set, np.ndarray, pd.Series]:
                if len(offeringCode) != len(all_exdates):
                    offeringCode = [offeringCode[0]] * len(all_exdates)
            else:
                offeringCode = [offeringCode] * len(all_exdates)

            best_list = []
            for i_date in range(all_exdates.size):
                print(
                    "Analyzing expiration date %1i of %1i - %s "
                    % (i_date + 1, all_exdates.size, str(all_exdates[i_date]))
                    + "(%1i days)" % (int((purchaseDate - all_exdates[i_date]).days))
                )
                best_in_date = cls.monte_carlo_singe_date(
                    tckr,
                    purchaseDate,
                    all_exdates[i_date],
                    offeringCode[i_date],
                    predictionModel,
                    strategy,
                    lifetime=lifetime,
                    n_mc=n_mc,
                    cum_cycles=cum_cycles,
                    max_risk=max_risk,
                    strikes=strikes,
                    prices=prices,
                    total_pf=total_pf,
                    broker_fee=broker_fee,
                    addtl_option_fee=addtl_option_fee,
                    return_all_data=False,
                    source=source,
                )
                best_list.append(best_in_date)

            if all_exdates.size == 1:
                output = best_list[0]
            else:
                output = pd.concat(best_list, axis=1, ignore_index=True).T
                output = output.sort_values("Avg_Ann_Return", ascending=False).reindex(
                    range(output.index.size)
                )
        return output

    @classmethod
    def monte_carlo_single_date(
        cls,
        tckr,
        purchaseDate,
        exDate,
        offeringCode,
        predictionModel,
        strategy="+P -P",
        current_price=None,
        symmetry=False,
        n_mc=100,
        cum_cycles=20,
        max_risk=0.2,
        lifetime=1,
        strikes=None,
        prices=None,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        optimize=True,
        test_arbitrage=True,
        source="storage",
    ):
        """
        run full monte carlo simulation for the given option strategy and
        expiration date.

        Args:
            tckr - the name of the underlying security
            spreadtype - 'Call' or 'Put'
            outlook - 'Bull' (positive) or 'Bear' (negative)
            purchaseDate - the last date that the functions will pull
                historical data for
            exDate - If a single date then call the appropriate function for
                        the date.
                     If a range of dates, then find the optimal
                        option for each date.
                     If 'Estimate' then estimate based on the range determined
                        by the offering code
                     If 'Fidelity' then pull all the exDates available in the
                        fidelity options DB for the give purchase date. If no
                        date matches then default to 'Estimate'
            offeringCode - String containing 'W', 'M', 'Q', or 'Y' to indicate
                whether to pull strikes for a weekly, monthly, quarterly,
                or yearly offering. The offering code is returned from the
                optionDB.findClosestExDate function call.
            predictionModel - if None then function will use the default
                prediction model in the MarketPredictions module
            lifetime - the fraction of the option lifetime that the option will
                be held before it is sold. If 'expire' or 1 then hold the
                option until it expires
            n_mc - the number of times each option combination is modeled
            cum_cycles - the number of loops to test the option combination
                so that the true return can be approximated if you have both
                good and bad prices
            max_risk - the maximum amount of the portfolio that will be risked
                in the option strategies
            total_pf - value of the total portfolio that the strategy will be
                applied too
            broker_fee - the price of the first option contract
                (or stock purchase) charged by the broker for the transaction
            addtl_option_fee - the additional fee charged by the broker
                for each contract after the initial contract
            return_all_data - Default is False. If true then return a tuple of
                the best option and a pd.DataFrame of the info for every option
            source - where to pull the stock data. options are 'storage' or
                'temp_db'

        debug:
        tckr = '.SPX'
        spreadType = 'PUT'
        outlook = 'BULL'
        predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
        optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
        purchaseDate = pd.datetime(2006, 6, 20)
        exDate, offeringCode = optionDB.findClosestExDate(purchaseDate,
                                        purchaseDate + pd.Timedelta(days=30))
        current_price = None
        contractQty = 1
        expValStpCnt = 500
        modelingType = 'BS'
        contractSize = 100
        positionName = None
        source = 'storage'
        symmetry=False
        n_mc=100
        cum_cycles=10
        max_risk=0.2
        lifetime=1
        symmetry=False
        return_all_data=False
        strikes = None
        prices = None
        strategy='+P -P'
        total_pf = 1e5
        broker_fee = 0
        addtl_option_fee = 0
        """
        if predictionModel is None:
            predictionModel = getDefaultPredictionModel_HDF5()
        purchaseDate = Util.formatDateInput(purchaseDate)
        exDate = Util.formatDateInput(exDate)
        optionDB = OptionData(stockDB=predictionModel.stockDB)
        if current_price is None:
            current_price = predictionModel.get_current_price(purchaseDate, tckr)
        positions, cashflow_sign, quantity = cls.interpret_strategy(strategy)
        # gather all possible strikes
        daysOut = (exDate - purchaseDate).days
        if type(lifetime) == str or lifetime >= 1:
            sellDate = exDate
            sigma = rate = None
        else:
            sellDate = purchaseDate + pd.Timedelta(days=int(daysOut * lifetime))
            sigma = (
                optionDB.stockDB.PredictSPXVol_1m3mVIX(
                    purchaseDate, exDate, source=source
                )
                / 100.0
            )
            rate = (
                optionDB.liborDB.getLIBOR_Rate(purchaseDate, daysOut, source=source)
                / 100.0
            )

        days_invested = (sellDate - purchaseDate).days
        # get price prediction
        dist = predictionModel.get_prob_dist(purchaseDate, sellDate, tckr)
        predicted_price = dist.mean()
        predicted_std = dist.std()

        strikes, prices = cls.format_strikes_prices(
            positions,
            cashflow_sign,
            quantity,
            strike_list=strikes,
            price_list=prices,
            optionDB=optionDB,
            purchaseDate=purchaseDate,
            exDate=exDate,
            current_price=current_price,
            offeringCode=offeringCode,
            sigma=sigma,
            rate=rate,
            tckr=tckr,
            source=source,
        )

        legs = len(strikes)
        if test_arbitrage:
            # strikes_r is the reduced strike lists
            strikes_r = [
                strikes[x_i].reshape(
                    tuple([1] * x_i + [strikes[x_i].size] + [1] * (legs - x_i - 1))
                )
                for x_i in range(legs)
            ]
            prices_r = [
                prices[x_i].reshape(
                    tuple([1] * x_i + [prices[x_i].size] + [1] * (legs - x_i - 1))
                )
                for x_i in range(legs)
            ]
            valid_qty = np.array([x.size for x in strikes_r], dtype=np.int64).prod()
            strikes_t = np.broadcast_arrays(*strikes_r)
            prices_t = np.broadcast_arrays(*prices_r)
            selectBool = cls.screen_strike_sequence(
                strikes_t, strategy=strategy, symmetry=symmetry
            )
            strikes_t = np.concatenate(
                tuple([x.reshape(valid_qty, 1) for x in strikes_t]), axis=1
            )
            prices_t = np.concatenate(
                tuple([x[selectBool].reshape(valid_qty, 1) for x in prices_t]), axis=1
            )
        # Remove buy prices greater than -$0.01 and sell prices less than $0.01
        i_leg = 0
        while True:
            if prices[i_leg].sum() <= 0:
                selectBool = prices[i_leg] < -0.01
            else:
                selectBool = prices[i_leg] > 0.01
            strikes[i_leg] = strikes[i_leg][selectBool]
            prices[i_leg] = prices[i_leg][selectBool]
            i_leg += 1
            if i_leg >= legs:
                break

        # loop will reduce the strike combination counts until all of them are
        #  under the threshold
        strikes_prev = strikes
        prices_prev = prices
        strike_limit = 10000000 ** (1.0 / legs)
        while True:
            strikes_new, prices_new = cls.reduce_combinations_mc(
                positions,
                quantity,
                strikes_prev,
                prices_prev,
                strike_limit,
                dist,
                strategy=strategy,
                n_mc=n_mc,
                cum_cycles=cum_cycles,
                max_risk=max_risk,
                lifetime=1,
                symmetry=symmetry,
                tckr=tckr,
                sellDate=sellDate,
                exDate=exDate,
                sigma=sigma,
                rate=rate,
                optionDB=optionDB,
                total_pf=total_pf,
                broker_fee=broker_fee,
                addtl_option_fee=addtl_option_fee,
                return_all_data=False,
                source=source,
            )
            if np.any([x.size == 0 for x in strikes_new]):
                break
            elif np.all(
                [
                    np.all(strikes_new[j].size == strikes_prev[j].size)
                    and np.all(prices_new[j].size == prices_prev[j].size)
                    for j in range(legs)
                ]
            ):
                break
            else:
                strikes_prev, prices_prev = strikes_new, prices_new
        # strikes_r is the reduced strike lists and needs to have specific
        #  shapes based on the index position
        strikes_r = [
            strikes_prev[x_i].reshape(
                tuple([1] * x_i + [strikes_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        prices_r = [
            prices_prev[x_i].reshape(
                tuple([1] * x_i + [prices_prev[x_i].size] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)

        # only keep combinations where the strikes are in sequential order
        selectBool = cls.screen_strike_sequence(
            strikes_t, strategy=strategy, symmetry=symmetry
        )
        # Only keep the strategy combos that have a net premium > $0.03
        selectBool = np.logical_and(selectBool, np.abs(np.sum(prices_t, 0)) > 0.03)
        valid_qty = selectBool.sum()

        strikes_t = np.concatenate(
            tuple([x[selectBool].reshape(valid_qty, 1) for x in strikes_t]), axis=1
        )
        prices_t = np.concatenate(
            tuple([x[selectBool].reshape(valid_qty, 1) for x in prices_t]), axis=1
        )

        #        print('\tRunning full MC simulation')
        (
            total_returns,
            raw_volatility,
            win_perc,
            strikes_t,
            prices_t,
            selection_bridge,
        ) = cls.simulate_monte_carlo(
            positions,
            cashflow_sign,
            quantity,
            strikes_t,
            prices_t,
            dist,
            n_mc=n_mc,
            cycles=cum_cycles,
            max_risk=max_risk,
            lifetime=lifetime,
            tckr=tckr,
            sellDate=sellDate,
            exDate=exDate,
            sigma=sigma,
            rate=rate,
            optionDB=optionDB,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
            source=source,
        )
        valid_qty = strikes_t.shape[0]
        column_order = (
            [
                "Strategy",
                "Category",
                "Outcome_Qty",
                "Yield_Cycles",
                "PurchaseDate",
                "ExpirationDate",
                "DaysToExp",
                "DaysInvested",
                "Max_Risk",
                "CurrentPrice",
                "Prediction_Price",
                "Prediction_Std",
                "Total_PF",
                "Trade_Fee",
                "Addtl_Contract_Fee",
            ]
            + ["Strike_%1i" % j for j in range(legs)]
            + ["Price_%1i" % j for j in range(legs)]
            + [
                "Premium",
                "Max_Loss",
                "Max_Profit",
                "Median_PF_Return",
                "Mean_PF_Return",
                "Std_PF_Return",
                "Avg_PF_Return_PerDay",
                "Avg_Ann_Return",
                "Expected_Stock_Return",
                "AvgReturnVsStock",
                "Expected_Gain_Return",
                "Win_Percentage",
                "Ann_Volatility",
            ]
        )

        if strikes_t.size == 0:
            bestOption = pd.Series(index=column_order, name="Best " + strategy)
            bestOption["Strategy"] = strategy
            bestOption["Category"] = "Monte Carlo"
            bestOption["Outcome_Qty"] = n_mc
            bestOption["Yield_Cycles"] = cum_cycles
            bestOption["PurchaseDate"] = purchaseDate
            bestOption["ExpirationDate"] = exDate
            bestOption["DaysToExp"] = daysOut
            bestOption["DaysInvested"] = daysOut
            bestOption["Max_Risk"] = max_risk
            bestOption["CurrentPrice"] = current_price
            bestOption["Prediction_Price"] = dist.mean()
            bestOption["Prediction_Std"] = dist.std()
            bestOption["Total_PF"] = total_pf
            bestOption["Trade_Fee"] = broker_fee
            bestOption["Addtl_Contract_Fee"] = addtl_option_fee
            if return_all_data:
                options = pd.DataFrame(bestOption).T
                options.index = [0]
                return bestOption, options
            else:
                return bestOption
        # calculate the expected return of the option
        exp_return = cls.expected_gain(
            positions,
            quantity,
            strikes_t,
            prices_t,
            max_risk,
            dist,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
        )
        premium = cls.calc_premium(prices_t)
        init_invest = cls.calc_init_invest(positions, quantity, strikes_t, prices_t)
        max_profit = cls.calc_max_profit(positions, quantity, strikes_t, prices_t)

        pf_median, pf_mean, pf_stdev = total_returns
        ann_volatility = np.sqrt(
            (raw_volatility * raw_volatility).sum(-1) / raw_volatility.shape[-1]
        ) * np.sqrt(365 / float(daysOut))
        best_index = pf_mean.argmax()
        return_of_stock = predictionModel.expected_return_of_stock(
            tckr, purchaseDate, sellDate, source=source
        )
        bestOption = {
            "Strategy": strategy,
            "Category": "Monte Carlo",
            "Outcome_Qty": n_mc,
            "Yield_Cycles": cum_cycles,
            "PurchaseDate": purchaseDate,
            "ExpirationDate": exDate,
            "DaysToExp": daysOut,
            "DaysInvested": days_invested,
            "Max_Risk": max_risk,
            "CurrentPrice": current_price,
            "Prediction_Price": predicted_price,
            "Prediction_Std": predicted_std,
            "Total_PF": total_pf,
            "Trade_Fee": broker_fee,
            "Addtl_Contract_Fee": addtl_option_fee,
            "Premium": premium[best_index, 0],
            "Max_Loss": -1 * init_invest[best_index, 0],
            "Max_Profit": max_profit[best_index, 0],
            "Median_PF_Return": pf_median[best_index],
            "Mean_PF_Return": pf_mean[best_index],
            "Std_PF_Return": pf_stdev[best_index],
            "Avg_PF_Return_PerDay": pf_mean[best_index] ** (1.0 / days_invested),
            "Avg_Ann_Return": (pf_mean[best_index] ** (365.0 / days_invested)),
            "Expected_Stock_Return": return_of_stock + 1,
            "AvgReturnVsStock": (pf_mean[best_index] - 1) / (return_of_stock),
            "Expected_Gain_Return": exp_return[best_index, 0],
            "Win_Percentage": win_perc[best_index, 0],
            "Ann_Volatility": ann_volatility[best_index],
        }
        for i in range(legs):
            bestOption["Strike_%1i" % i] = strikes_t[best_index, i]
            bestOption["Price_%1i" % i] = prices_t[best_index, i]
        bestOption = pd.Series(data=bestOption)[column_order]
        bestOption.name = "Best " + strategy
        output = bestOption

        if return_all_data:
            # create the results pd.DF for comparison
            options = pd.DataFrame(index=range(valid_qty), columns=column_order)
            options["Strategy"] = strategy
            options["Category"] = "Monte Carlo"
            options["Outcome_Qty"] = n_mc
            options["Yield_Cycles"] = cum_cycles
            options["PurchaseDate"] = purchaseDate
            options["ExpirationDate"] = exDate
            options["DaysToExp"] = daysOut
            options["DaysInvested"] = daysOut
            options["Max_Risk"] = max_risk
            options["CurrentPrice"] = current_price
            options["Prediction_Price"] = dist.mean()
            options["Prediction_Std"] = dist.std()
            options["Total_PF"] = total_pf
            options["Trade_Fee"] = broker_fee
            options["Addtl_Contract_Fee"] = addtl_option_fee
            options["Premium"] = premium
            options["Max_Loss"] = -1 * init_invest
            options["Max_Profit"] = max_profit
            options["Median_PF_Return"] = pf_median
            options["Mean_PF_Return"] = pf_mean
            options["Std_PF_Return"] = pf_stdev
            options["Avg_PF_Return_PerDay"] = pf_mean ** (1.0 / days_invested)
            options["Avg_Ann_Return"] = pf_mean ** (365.0 / daysOut)
            options["Expected_Stock_Return"] = return_of_stock + 1
            options["AvgReturnVsStock"] = (pf_mean - 1) / return_of_stock
            options["Expected_Gain_Return"] = exp_return
            options["Win_Percentage"] = win_perc
            options["Ann_Volatility"] = ann_volatility
            for i in range(legs):
                options["Strike_%1i" % i] = strikes_t[:, i]
                options["Price_%1i" % i] = prices_t[:, i]
            options = options[column_order]
            output = bestOption, options

        return output

    @classmethod
    def interpret_strategy(cls, strategy):
        """
        Strategy can be a name of a pre-programed strategy or it can
        be a list of positions separated by a space such as '+P_1 -P_1' for
        a Bullish Put Spread. The format of each position is:
            o  '+/-' to indicate whether the position is bought or
                sold (+ = bought, - = sold). No sign defaults to bought
            o  'P/C' to indicate whether the position is a Put or Call
            o  '_X' indicates the quantity to purchase. If this is left off,
                assume 1
        """
        strategy = strategy.upper()
        if "IRON" in strategy or strategy in ["IC", "IB"]:
            # catch all Iron Condor and Iron Butterfly strategies
            positions = ["Put", "Put", "Call", "Call"]
            cashflow_sign = [-1, 1, 1, -1]
            quantity = [1, 1, 1, 1]
        elif "SPREAD" in strategy:
            quantity = [1, 1]
            if "CAll" in strategy:
                positions = ["Call", "Call"]
                if "BEAR" in strategy:
                    cashflow_sign = [1, -1]
                else:
                    cashflow_sign = [-1, 1]
            else:
                positions = ["Put", "Put"]
                if "BEAR" in strategy:
                    cashflow_sign = [1, -1]
                else:
                    cashflow_sign = [-1, 1]
        else:
            positions = [
                "Call" if "C" in x else "Put" for x in strategy.upper().split(" ")
            ]
            cashflow_sign = [1 if "-" in x else -1 for x in strategy.split(" ")]
            quantity = [
                1 if x.find("_") == -1 else int(x[x.find("_") + 1 :])
                for x in strategy.split(" ")
            ]
        return positions, cashflow_sign, quantity

    @classmethod
    def screen_strike_sequence(cls, strikes, strategy=None, symmetry=False):
        # only keep combinations where the strikes are in sequential order
        legs = len(strikes)
        i = 0
        while True:
            if i == 0:
                selectBool = np.isfinite(strikes[i])
            else:
                selectBool = np.logical_and(selectBool, strikes[i] > strikes[i - 1])
            i += 1
            if i >= legs:
                break
        return selectBool

    @classmethod
    def highlight_arbitrage(
        cls,
        call_symbol,
        call_strike,
        call_sell,
        call_buy,
        put_symbol,
        put_strike,
        put_sell,
        put_buy,
    ):
        opportunities = []
        # BuCS
        symbols_r = [
            call_symbol.reshape(call_symbol.size, 1),
            call_symbol.reshape(1, call_symbol.size),
        ]
        strikes_r = [
            call_strike.reshape(call_strike.size, 1),
            call_strike.reshape(1, call_strike.size),
        ]
        prices_r = [
            call_buy.reshape(call_buy.size, 1),
            call_sell.reshape(1, call_buy.size),
        ]
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)
        symbols_t = np.broadcast_arrays(*symbols_r)
        selectBool = strikes_t[1] > strikes_t[0]
        valid_qty = selectBool.sum()
        symbol0 = symbols_t[0][selectBool].reshape(valid_qty, 1)
        symbol1 = symbols_t[1][selectBool].reshape(valid_qty, 1)
        x0 = strikes_t[0][selectBool].reshape(valid_qty, 1)
        x1 = strikes_t[1][selectBool].reshape(valid_qty, 1)
        p0 = prices_t[0][selectBool].reshape(valid_qty, 1)
        p1 = prices_t[1][selectBool].reshape(valid_qty, 1)
        strikegap_sprd = x1 - x0
        premium_sprd = p0 + p1
        ml = premium_sprd
        #        info_BuCS = np.concatenate([x0, x1, p0, p1, strikegap_sprd,
        #                                    premium_sprd, ml], axis=1)
        arbitrage = ml > 0
        if arbitrage.any():
            print("Bullish Call Spread Abitrage Opportunities Exist!")
            arbitrage_rows = np.arange(valid_qty)[arbitrage.flatten()]
            spread_opp = [
                "Bull Call Spread: Buy "
                + symbol0[i, 0]
                + " -- Sell "
                + symbol1[i, 0]
                + " for a %1.2f gain / contract" % ml[i, 0]
                for i in arbitrage_rows
            ]
            print(spread_opp)
            opportunities += spread_opp

        # BeCS
        symbols_r = [
            call_symbol.reshape(call_symbol.size, 1),
            call_symbol.reshape(1, call_symbol.size),
        ]
        strikes_r = [
            call_strike.reshape(call_strike.size, 1),
            call_strike.reshape(1, call_strike.size),
        ]
        prices_r = [
            call_sell.reshape(call_sell.size, 1),
            call_buy.reshape(1, call_buy.size),
        ]
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)
        symbols_t = np.broadcast_arrays(*symbols_r)
        selectBool = strikes_t[1] > strikes_t[0]
        valid_qty = selectBool.sum()
        symbol0 = symbols_t[0][selectBool].reshape(valid_qty, 1)
        symbol1 = symbols_t[1][selectBool].reshape(valid_qty, 1)
        x0 = strikes_t[0][selectBool].reshape(valid_qty, 1)
        x1 = strikes_t[1][selectBool].reshape(valid_qty, 1)
        p0 = prices_t[0][selectBool].reshape(valid_qty, 1)
        p1 = prices_t[1][selectBool].reshape(valid_qty, 1)
        strikegap_sprd = x1 - x0
        premium_sprd = p0 + p1
        ml = premium_sprd - strikegap_sprd
        info_BeCS = np.concatenate(
            [x0, x1, p0, p1, strikegap_sprd, premium_sprd, ml], axis=1
        )
        sym_BeCS = np.concatenate([symbol0, symbol1], axis=1)
        arbitrage = ml > 0
        if arbitrage.any():
            print("Bearish Call Spread Abitrage Opportunities Exist!")
            arbitrage_rows = np.arange(valid_qty)[arbitrage.flatten()]
            spread_opp = [
                "Bear Call Spread: Sell "
                + symbol0[i, 0]
                + " -- Buy "
                + symbol1[i, 0]
                + " for a %1.2f gain / contract" % ml[i, 0]
                for i in arbitrage_rows
            ]
            print(spread_opp)
            opportunities += spread_opp

        # BuPS
        symbols_r = [
            put_symbol.reshape(put_symbol.size, 1),
            put_symbol.reshape(1, put_symbol.size),
        ]
        strikes_r = [
            put_strike.reshape(put_strike.size, 1),
            put_strike.reshape(1, put_strike.size),
        ]
        prices_r = [
            put_buy.reshape(put_buy.size, 1),
            put_sell.reshape(1, put_sell.size),
        ]
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)
        symbols_t = np.broadcast_arrays(*symbols_r)
        selectBool = strikes_t[1] > strikes_t[0]
        valid_qty = selectBool.sum()
        symbol0 = symbols_t[0][selectBool].reshape(valid_qty, 1)
        symbol1 = symbols_t[1][selectBool].reshape(valid_qty, 1)
        x0 = strikes_t[0][selectBool].reshape(valid_qty, 1)
        x1 = strikes_t[1][selectBool].reshape(valid_qty, 1)
        p0 = prices_t[0][selectBool].reshape(valid_qty, 1)
        p1 = prices_t[1][selectBool].reshape(valid_qty, 1)
        strikegap_sprd = x1 - x0
        premium_sprd = p0 + p1
        ml = premium_sprd - strikegap_sprd
        info_BuPS = np.concatenate(
            [x0, x1, p0, p1, strikegap_sprd, premium_sprd, ml], axis=1
        )
        sym_BuPS = np.concatenate([symbol0, symbol1], axis=1)
        arbitrage = ml > 0
        if arbitrage.any():
            print("Bullish Put Spread Abitrage Opportunities Exist!")
            arbitrage_rows = np.arange(valid_qty)[arbitrage.flatten()]
            spread_opp = [
                "Bull Put Spread: Buy "
                + symbol0[i, 0]
                + " -- Sell "
                + symbol1[i, 0]
                + " for a %1.2f gain / contract" % ml[i, 0]
                for i in arbitrage_rows
            ]
            print(spread_opp)
            opportunities += spread_opp

        # BePS
        symbols_r = [
            put_symbol.reshape(put_symbol.size, 1),
            put_symbol.reshape(1, put_symbol.size),
        ]
        strikes_r = [
            put_strike.reshape(put_strike.size, 1),
            put_strike.reshape(1, put_strike.size),
        ]
        prices_r = [
            put_sell.reshape(put_sell.size, 1),
            put_buy.reshape(1, put_buy.size),
        ]
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)
        symbols_t = np.broadcast_arrays(*symbols_r)
        selectBool = strikes_t[1] > strikes_t[0]
        valid_qty = selectBool.sum()
        symbol0 = symbols_t[0][selectBool].reshape(valid_qty, 1)
        symbol1 = symbols_t[1][selectBool].reshape(valid_qty, 1)
        x0 = strikes_t[0][selectBool].reshape(valid_qty, 1)
        x1 = strikes_t[1][selectBool].reshape(valid_qty, 1)
        p0 = prices_t[0][selectBool].reshape(valid_qty, 1)
        p1 = prices_t[1][selectBool].reshape(valid_qty, 1)
        strikegap_sprd = x1 - x0
        premium_sprd = p0 + p1
        ml = premium_sprd  # ml = max loss
        #        info_BePS = np.concatenate([x0, x1, p0, p1, strikegap_sprd,
        #                                    premium_sprd, ml], axis=1)
        arbitrage = ml > 0
        if arbitrage.any():
            print("Bearish Put Spread Abitrage Opportunities Exist!")
            arbitrage_rows = np.arange(valid_qty)[arbitrage]
            spread_opp = [
                "Bear Put Spread: Sell "
                + symbol0[i, 0]
                + " -- Buy "
                + symbol1[i, 0]
                + " for a %1.2f gain / contract" % ml[i, 0]
                for i in arbitrage_rows
            ]
            print(spread_opp)
            opportunities += spread_opp

        # IC = BuPS + BeCS
        s1 = info_BuPS[:, 5] > 0
        s2 = info_BeCS[:, 5] > 0
        # print((s1.sum(), s2.sum()))
        ic_opp = []
        if s1.any() and s2.any():

            @jit(nopython=True)
            def loop_arbitrage_IC(sp1_x0, sp1_x1, sp1_prem, sp2_x0, sp2_x1, sp2_prem):
                best = -100000
                best_index = (-1, -1)
                k = 0
                while k < sp1_prem.size:
                    m = 0
                    while m < sp1_prem.size:
                        if sp1_x1[k] <= sp2_x0[m]:
                            value = (
                                sp1_prem[k]
                                + sp2_prem[m]
                                - max(sp1_x1[k] - sp1_x0[k], sp2_x1[m] - sp2_x0[m])
                            )
                            if value > best:
                                best = value
                                best_index = (k, m)
                        m += 1
                    k += 1
                return best, best_index

            value, index = loop_arbitrage_IC(
                info_BuPS[s1, 0],
                info_BuPS[s1, 1],
                info_BuPS[s1, 5],
                info_BeCS[s2, 0],
                info_BeCS[s2, 1],
                info_BeCS[s2, 5],
            )
            if value > 0:
                symbol0 = sym_BuPS[s1, 0][index[0]]
                symbol1 = sym_BuPS[s1, 1][index[0]]
                symbol2 = sym_BuPS[s1, 0][index[1]]
                symbol3 = sym_BuPS[s1, 1][index[1]]
                print("IC Abitrage Opportunities Exist for $%s / option" % str(value))
                ic_opp = [
                    f"Icon Condor Arbitrage: Buy {symbol0} -- Sell {symbol1} -- Sell {symbol2}"
                    f"-- Buy {symbol3} for a {value} gain / contract"
                ]
                print(ic_opp)
                opportunities += ic_opp

        return opportunities

    @classmethod
    def reduce_combinations_mc(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        strike_limit,
        dist,
        strategy=None,
        n_mc=100,
        cum_cycles=20,
        max_risk=0.2,
        lifetime=1,
        symmetry=False,
        tckr=None,
        sellDate=None,
        exDate=None,
        sigma=None,
        rate=None,
        optionDB=None,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        if np.any([arr.size == 0 for arr in strikes]):
            return strikes, prices
        elif np.all([arr.size < strike_limit for arr in strikes]):
            return strikes, prices
        # Limit strike price to 25-50 strikes and calculate the max return
        #  then return a reduced version of the strike and price combinations
        legs = len(strikes)
        n_mc_step = 3
        cashflow_sign = cls.calc_cashflow_sign(prices)
        selectBool = [
            np.arange(x.size) % max(1, x.size / (strike_limit / 2)) == 0
            for x in strikes
        ]
        # strikes_r is the reduced strike lists
        strikes_r = [
            strikes[x_i][selectBool[x_i]].reshape(
                tuple([1] * x_i + [selectBool[x_i].sum()] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        prices_r = [
            prices[x_i][selectBool[x_i]].reshape(
                tuple([1] * x_i + [selectBool[x_i].sum()] + [1] * (legs - x_i - 1))
            )
            for x_i in range(legs)
        ]
        # create a boolean indicator to signal which legs are reduced
        leg_reduced_bool = [
            strikes[x_i].size != strikes_r[x_i].size for x_i in range(legs)
        ]
        # broadcast the reduced arrays and downselect so that they
        #  can be quicklysolved and provide
        # strikes_t will become the tall strike lists
        strikes_t = np.broadcast_arrays(*strikes_r)
        prices_t = np.broadcast_arrays(*prices_r)

        # only keep combinations where the strikes are in sequential order
        selectBool = cls.screen_strike_sequence(
            strikes_t, strategy=strategy, symmetry=symmetry
        )
        # Only keep the strategy combos that have a net premium > $0.03
        selectBool = np.logical_and(selectBool, np.abs(np.sum(prices_t, 0)) > 0.03)
        valid_qty = selectBool.sum()
        if valid_qty == 0:
            return [np.array([[]])] * legs, [np.array([[]])] * legs

        strikes_t = np.concatenate(
            tuple([x[selectBool].reshape(valid_qty, 1) for x in strikes_t]), axis=1
        )
        prices_t = np.concatenate(
            tuple([x[selectBool].reshape(valid_qty, 1) for x in prices_t]), axis=1
        )

        # strikes have been reduced. Only do enough work to determine the
        #  strike cutoffs for the next round of analysis
        cycles = 100
        chunk_size = 100
        record_cutoff = 10000000
        quantile_cutoff = 75
        total_returns, __, __, strikes_t, __, __ = cls.simulate_monte_carlo(
            positions,
            cashflow_sign,
            quantity,
            strikes_t,
            prices_t,
            dist,
            n_mc=n_mc_step,
            cycles=cycles,
            n_mc_step=n_mc_step,
            chunk_size=chunk_size,
            record_cutoff=record_cutoff,
            quantile_cutoff=quantile_cutoff,
            max_risk=max_risk,
            lifetime=1,
            tckr=tckr,
            sellDate=sellDate,
            exDate=exDate,
            sigma=sigma,
            rate=rate,
            optionDB=optionDB,
            total_pf=total_pf,
            broker_fee=broker_fee,
            addtl_option_fee=addtl_option_fee,
            source=source,
        )
        if strikes_t.size == 0:
            return np.array([]), np.array([])
        pf_mean = total_returns[1]

        unique_strikes = [
            strikes_r[j].ravel()[np.isin(strikes_r[j].ravel(), strikes_t[:, j])]
            for j in range(legs)
        ]
        max_returns = [
            np.array([pf_mean[strikes_t[:, j] == x].max() for x in unique_strikes[j]])
            for j in range(legs)
        ]
        selectBool = [
            max_returns[j]
            > max_returns[j].max()
            - (0.25 * (max_returns[j].max() - max_returns[j].min()))
            for j in range(legs)
        ]
        i_leg = 0
        next_strikes = []
        next_prices = []
        while True:
            if not leg_reduced_bool[i_leg]:
                next_strikes.append(strikes[i_leg])
                next_prices.append(prices[i_leg])
                i_leg += 1
                if i_leg >= legs:
                    break
                else:
                    continue
            unique_strikes = strikes_r[i_leg].ravel()[
                np.isin(strikes_r[i_leg].ravel(), strikes_t[:, i_leg])
            ]
            max_returns = np.array(
                [pf_mean[strikes_t[:, i_leg] == x].max() for x in unique_strikes]
            )
            selectBool = max_returns > max_returns.max() - (
                0.25 * (max_returns.max() - max_returns.min())
            )
            max_return_strike = unique_strikes[max_returns.argmax()]
            #                pl.plot(unique_strikes, max_returns, 'b.')
            #                max_index = max_returns.argmax()
            #                pl.plot(unique_strikes[[max_index]],
            #                        max_returns[[max_index]], 'k.')
            #                pl.plot(unique_strikes[selectBool],
            #                        max_returns[selectBool], 'r.')
            #                pl.show()
            if selectBool.any():
                low_limit = unique_strikes[selectBool].min()
                high_limit = unique_strikes[selectBool].max()
            else:
                low_limit = unique_strikes.min()
                high_limit = unique_strikes.max()
            # adjust limits for special conditions when they may
            #  be the max or on the edge of the array
            if low_limit == max_return_strike:
                # set the limit at the next lowest limit
                if (unique_strikes < low_limit).any():
                    low_limit = unique_strikes[unique_strikes < low_limit].max()
                else:
                    low_limit = None
            if low_limit is not None and low_limit == unique_strikes.min():
                low_limit = None
            if high_limit == max_return_strike:
                # set the limit at the next higher limit
                if (unique_strikes > high_limit).any():
                    high_limit = unique_strikes[unique_strikes > high_limit].min()
                else:
                    high_limit = None
            if high_limit is not None and high_limit == unique_strikes.max():
                high_limit = None
            # determine which strikes to use for the next round. Limits
            #  with None values will not have a limit applied
            if low_limit is None:
                # if the limit is the strike with the highest return or the
                #  first strike, don't set a lower limit
                if high_limit is None:
                    # if the limit is the strike with the highest return or
                    #  the last strike, don't set an upper limit
                    selectBool = np.array([True] * strikes[i_leg].size)
                else:
                    selectBool = strikes[i_leg] < high_limit
            else:
                if high_limit is None:
                    # if the limit is the strike with the highest return or
                    #  the last strike, don't set an upper limit
                    selectBool = strikes[i_leg] > low_limit
                else:
                    selectBool = np.logical_and(
                        strikes[i_leg] < high_limit, strikes[i_leg] > low_limit
                    )
            next_strikes.append(strikes[i_leg][selectBool])
            next_prices.append(prices[i_leg][selectBool])
            i_leg += 1
            if i_leg >= legs:
                break
        return next_strikes, next_prices

    @classmethod
    def simulate_monte_carlo(
        cls,
        positions,
        cashflow_sign,
        quantity,
        strike_array,
        price_array,
        dist,
        n_mc=20,
        cycles=100,
        n_mc_step=3,
        chunk_size=100,
        record_cutoff=10000000,
        quantile_cutoff=75,
        max_risk=0.2,
        lifetime=1,
        tckr=None,
        sellDate=None,
        exDate=None,
        sigma=None,
        rate=None,
        optionDB=None,
        total_pf=100000,
        broker_fee=0,
        addtl_option_fee=0,
        source="storage",
    ):
        if strike_array.size == 0 or price_array.size == 0:
            total_returns = [np.array([[]])] * 3
            stdev_returns = [np.array([[]])] * 3
            strikes_temp = np.array([[]])
            prices_temp = np.array([[]])
            final_selection = np.array([[]])
            win_percentage = np.array([[]])
            return (
                total_returns,
                stdev_returns,
                win_percentage,
                strikes_temp,
                prices_temp,
                final_selection,
            )
        premium = cls.calc_premium(price_array)
        # this function and functions that it cals will divide by init_invest
        #  so add an incremental amount to avoid divide by 0 errors
        init_invest = (
            cls.calc_init_invest(positions, quantity, strike_array, price_array) + 1e-5
        )
        if broker_fee + addtl_option_fee > 0:
            assert np.all(init_invest != 0), (
                "Init_Invest should not contain a zero"
                + "\nPositions:\n"
                + str(positions)
                + "\nQuantity:\n"
                + str(quantity)
                + "\nStrike Array:\n"
                + str(strike_array)
                + "\nPrice_Array:\n"
                + str(price_array)
            )
            contract_qty = np.ceil(
                total_pf * max_risk / (init_invest * 100).astype(int)
            )
            fees = broker_fee + addtl_option_fee * (contract_qty - 1)
            # adjust fees as to the 'effective fee per contract' so that
            #  calc_expiration doesn't have to bother with multiple contracts
            fees = (fees / contract_qty).reshape(init_invest.shape)
        else:
            fees = np.zeros_like(init_invest)
        n_mc = n_mc_step if n_mc is None else n_mc
        strikes_temp = strike_array.copy()
        prices_temp = price_array.copy()
        final_selection = np.isfinite(premium)
        valid_qty = premium.size
        total_returns = None
        total_win_perc = None
        stdev_returns = None
        final_selection = np.isfinite(premium)
        while True:
            split_indices = np.arange(
                chunk_size, valid_qty + chunk_size, chunk_size
            ).astype(int)
            data_block = zip(
                np.split(strikes_temp, split_indices, axis=0),
                np.split(premium, split_indices, axis=0),
                np.split(init_invest, split_indices, axis=0),
                np.split(fees, split_indices, axis=0),
            )
            rand_price_block = dist.rvs(cycles * n_mc_step).reshape(cycles, n_mc_step)
            print("data_block (line 1461):")
            print(f"data_block len : {len(split_indices)}")

            loop_output = np.array(
                [
                    cls.mc_calc_expiration(
                        positions,
                        cashflow_sign,
                        quantity,
                        chunk[0],
                        chunk[1],
                        chunk[2],
                        chunk[3],
                        rand_price_block,
                        max_risk,
                        record_cutoff=record_cutoff,
                        return_all_data=True,
                    )
                    for chunk in data_block
                ]
            )
            loop_gains = loop_output[:, 0]
            loop_gains = np.concatenate(loop_gains, axis=0)
            loop_stdev = loop_output[:, 1]
            loop_stdev = np.concatenate(loop_stdev, axis=0)
            loop_win_perc = loop_output[:, 2]
            loop_win_perc = np.concatenate(loop_win_perc, axis=0)

            if total_win_perc is None:
                total_win_perc = loop_win_perc
            else:
                # update win_perc with the new data (weighted avg)
                previous_sim_qty = total_returns.shape[1] * cycles
                new_sim_qty = n_mc_step * cycles
                total_win_perc = (
                    total_win_perc * previous_sim_qty + loop_win_perc * new_sim_qty
                ) / (previous_sim_qty + new_sim_qty)
            if stdev_returns is None:
                stdev_returns = loop_stdev
            else:
                stdev_returns = np.concatenate([stdev_returns, loop_stdev], axis=1)
            if total_returns is None:
                total_returns = loop_gains
            else:
                total_returns = np.concatenate([total_returns, loop_gains], axis=1)
            mean_gains = total_returns.mean(axis=1)
            selectBool = mean_gains >= np.percentile(mean_gains, quantile_cutoff)
            valid_qty = selectBool.sum()

            final_selection[final_selection] = selectBool
            strikes_temp = strikes_temp[selectBool, :]
            prices_temp = prices_temp[selectBool, :]
            premium = premium[selectBool, :]
            init_invest = init_invest[selectBool, :]
            fees = fees[selectBool, :]
            total_returns = total_returns[selectBool, :]
            stdev_returns = stdev_returns[selectBool, :]
            total_win_perc = total_win_perc[selectBool, :]
            #                x_axis = np.arange(selectBool.size)
            #                pl.plot(x_axis[selectBool], mean_gains[selectBool], 'b.')
            #                pl.plot(x_axis[~selectBool], mean_gains[~selectBool], 'r.')
            #                pl.show()
            #                print (time.time()-t1)
            if type(lifetime) == str or lifetime >= 1:
                # if predicting the best return at the expiration date then
                #  reduce the options to 100 or fewer before releasing
                if valid_qty <= 100 or total_returns.shape[1] >= n_mc:
                    n_mc_remainder = n_mc - total_returns.shape[1]
                    break
            else:
                # If predicting the value if sold then stop before you
                #  cross 100 options so the correct final computation can
                #  be used
                if (
                    valid_qty * quantile_cutoff / 100.0 <= 100
                    or total_returns.shape[1] >= n_mc - 1
                ):
                    n_mc_remainder = n_mc
                    break
        # now add enough trials to get to the required n_mc level
        if n_mc_remainder > 0:
            split_indices = np.arange(
                chunk_size, valid_qty + chunk_size, chunk_size
            ).astype(int)
            data_block = zip(
                np.split(strikes_temp, split_indices, axis=0),
                np.split(premium, split_indices, axis=0),
                np.split(init_invest, split_indices, axis=0),
                np.split(fees, split_indices, axis=0),
            )
            rand_price_block = dist.rvs(cycles * n_mc_remainder).reshape(
                cycles, n_mc_remainder
            )
            print("data_block (line 1553):")
            # for chunk in data_block:
            #     print(chunk)
            if type(lifetime) == str or lifetime >= 1:
                loop_output = np.array(
                    [
                        cls.mc_calc_expiration(
                            positions,
                            cashflow_sign,
                            quantity,
                            chunk[0],
                            chunk[1],
                            chunk[2],
                            chunk[3],
                            rand_price_block,
                            max_risk,
                            record_cutoff=record_cutoff,
                            return_all_data=True,
                        )
                        for chunk in data_block
                    ]
                )
                loop_gains = loop_output[:, 0]
                loop_gains = np.concatenate(loop_gains, axis=0)
                loop_stdev = loop_output[:, 1]
                loop_stdev = np.concatenate(loop_stdev, axis=0)
                loop_win_perc = loop_output[:, 2]
                loop_win_perc = np.concatenate(loop_win_perc, axis=0)

                if total_win_perc is None:
                    total_win_perc = loop_win_perc
                else:
                    # update win_perc with the new data (weighted avg)
                    previous_sim_qty = total_returns.shape[1] * cycles
                    new_sim_qty = n_mc_step * cycles
                    total_win_perc = (
                        total_win_perc * previous_sim_qty + loop_win_perc * new_sim_qty
                    ) / (previous_sim_qty + new_sim_qty)
                if stdev_returns is None:
                    stdev_returns = loop_stdev
                else:
                    stdev_returns = np.concatenate([stdev_returns, loop_stdev], axis=1)
                if total_returns is None:
                    total_returns = loop_gains
                else:
                    total_returns = np.concatenate([total_returns, loop_gains], axis=1)
            else:
                # print('data_block (line 1553):')
                # for chunk in data_block:
                #     print(chunk)

                total_returns = np.array(
                    [
                        cls.mc_calc_sell(
                            chunk[0],
                            chunk[1],
                            chunk[2],
                            rand_price_block,
                            max_risk,
                            tckr,
                            sellDate,
                            exDate,
                            sigma,
                            rate,
                            optionDB,
                            source=source,
                        )
                        for chunk in data_block
                    ]
                )
                total_returns = np.concatenate(total_returns, axis=0)
        total_returns = [
            np.percentile(total_returns, 50, axis=1),
            total_returns.mean(axis=-1),
            total_returns.std(axis=-1),
        ]
        return (
            total_returns,
            stdev_returns,
            total_win_perc,
            strikes_temp,
            prices_temp,
            final_selection,
        )

    @classmethod
    def mc_calc_expiration(
        cls,
        positions,
        cashflow_sign,
        quantity,
        strikes,
        premium,
        init_invest,
        fees,
        rand_price_block,
        max_risk,
        record_cutoff=1e7,
        return_all_data=False,
    ):
        """
        total_pf, broker_fee, addtl_options_fee all try to capture the impact
        of fees on the strategy effectiveness
        args:
            strikes -  a 2d array with each column representing the
                x0, x1, x2, x3 strikes
            init_investment - the abs(max_loss) or the amount that could be
                lost in the transaction
            rand_price_block is the 2d array of random numbers in the shape
                (# of cycles to test the return, # of pf's to test per option)
            total_pf - value of the total portfolio that the strategy will be
                applied too
            broker_fee - the price of the first option contract
                (or stock purchase) charged by the broker for the transaction
            addtl_option_fee - the additional fee charged by the broker
                for each contract after the initial contract

        returns a tuple of the median, mean, stdev and win_percentage of the
            returns for each option combination
        """
        cycles, pf_cnt = rand_price_block.shape
        valid_qty, legs = strikes.shape
        record_cnt = cycles * pf_cnt * valid_qty
        slope = {("Put", 1): 1, ("Put", -1): -1, ("Call", 1): -1, ("Call", -1): 1}
        if record_cnt < record_cutoff:
            # slope_list is the slope of each position relative to the strike
            #  and execution price. It determines if the value is
            #  (strike -  price) or (price -  strike)
            slope_list = np.array(
                [
                    slope[(positions[i_x], cashflow_sign[i_x])]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).reshape(1, legs, 1, 1)
            premium = premium.reshape((valid_qty, 1, 1))
            init_invest = init_invest.reshape((valid_qty, 1, 1))
            fees = fees.reshape((valid_qty, 1, 1))
            strikes = strikes.reshape((valid_qty, legs, 1, 1))
            rand_price_block_Transpose = rand_price_block.T.reshape(
                (1, 1, pf_cnt, cycles)
            )
            profit = slope_list * (rand_price_block_Transpose - strikes)
            # if the position was sold (sign = 1), then you can only lose
            #  from the execution of the option. But if the position was
            #  bought (sign = -1) then it can only make money with the
            #  of the option
            profit = (
                np.concatenate(
                    [
                        np.maximum(0, profit[:, [i], :, :])
                        if cashflow_sign[i] == -1
                        else np.minimum(0, profit[:, [i], :, :])
                        for i in range(legs)
                    ],
                    axis=1,
                ).sum(axis=1)
                + premium
                - fees
            )
            win_qty = (profit > 0).sum(axis=-1).sum(axis=-1).reshape(valid_qty, 1)
            portfolio_return = 1 + max_risk * (profit / init_invest)
            raw_volatility = portfolio_return.std(axis=-1)
            portfolio_return = portfolio_return.prod(axis=-1)

        else:
            slope_list = np.array(
                [
                    slope[(positions[i_x], cashflow_sign[i_x])]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).reshape(1, legs, 1)
            premium = premium.reshape((valid_qty, 1))
            init_invest = init_invest.reshape((valid_qty, 1))
            fees = fees.reshape((valid_qty, 1))
            portfolio_return = np.empty((valid_qty, pf_cnt))
            portfolio_return.fill(1)
            win_qty = np.zeros_like(init_invest)
            strikes = strikes.reshape((valid_qty, legs, 1))
            raw_volatility = []
            loop_cnt = 0
            while True:
                rand_price = rand_price_block[loop_cnt, :].reshape((1, 1, pf_cnt))
                profit = slope_list * (rand_price - strikes)
                # if the position was sold (sign = 1), then you can only lose
                #  from the execution of the option. But if the position was
                #  bought (sign = -1) then it can only make money with the
                #  of the option
                profit = (
                    np.concatenate(
                        [
                            np.maximum(0, profit[:, [i], :])
                            if cashflow_sign[i] == -1
                            else np.minimum(0, profit[:, [i], :])
                            for i in range(legs)
                        ],
                        axis=1,
                    ).sum(axis=1)
                    + premium
                    - fees
                )
                win_qty += (profit > 0).sum(axis=-1).reshape(valid_qty, 1)
                loop_return = 1 + max_risk * (profit / init_invest)
                portfolio_return = portfolio_return * loop_return
                raw_volatility.append(loop_return)
                loop_cnt += 1
                if loop_cnt >= cycles:
                    break
            raw_volatility = np.std(raw_volatility, axis=0)

        portfolio_return = portfolio_return ** (1.0 / cycles)
        if return_all_data:
            return (portfolio_return, raw_volatility, win_qty / float(cycles * pf_cnt))
        else:
            raw_volatility = np.sqrt((raw_volatility * raw_volatility).sum(axis=-1))
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
                raw_volatility.reshape(valid_qty, 1),
                win_qty / float(cycles * pf_cnt),
            )

    @classmethod
    def mc_calc_sell(
        cls,
        strikes,
        premium,
        init_invest,
        rand_price_block,
        max_risk,
        tckr,
        sellDate,
        exDate,
        sigma,
        rate,
        optionDB,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
        return_all_data=False,
        source="storage",
    ):
        """
        strikes are a 2d array with each column representing the x0, x1, x2, x3
            strikes
        init_investment is the abs(max_loss) or the amount that could be lost
            in the transaction
        rand_price_block is the 2d array of random numbers in the shape
            (# of cycles to test the return, # of pf's to test per option)
        total_pf - value of the total portfolio that the strategy will be
            applied too
        broker_fee - the price of the first option contract
            (or stock purchase) charged by the broker for the transaction
        addtl_option_fee - the additional fee charged by the broker
            for each contract after the initial contract

        returns a tuple of the median, mean, and stdev of the returns for each
            option combination
        """
        cycles, pf_cnt = rand_price_block.shape
        valid_qty = strikes.shape[0]
        premium_sq = np.tile(premium, pf_cnt).reshape((valid_qty, pf_cnt))
        init_invest_sq = np.tile(init_invest, pf_cnt).reshape((valid_qty, pf_cnt))
        portfolio_return = np.empty((valid_qty, pf_cnt))
        portfolio_return.fill(1)

        loop_cnt = 0
        while True:
            rand_price = rand_price_block[loop_cnt, :]
            # estimate of selling the put half (sell sx0, buy sx1)
            s_new_sq = optionDB.PriceEst_BS(
                tckr,
                "Put",
                sellDate,
                np.concatenate((strikes[:, 0], strikes[:, 1]), axis=0).reshape(
                    2 * valid_qty, 1
                ),
                exDate,
                S0=rand_price,
                sigma=sigma,
                rate=rate,
                source=source,
            )
            sell_premium_sq = s_new_sq[:valid_qty, :] - s_new_sq[valid_qty:, :]
            # estimate of selling the call half (buy x2, sell x3)
            s_new_sq = optionDB.PriceEst_BS(
                tckr,
                "Call",
                sellDate,
                np.concatenate((strikes[:, 2], strikes[:, 3]), axis=0).reshape(
                    2 * valid_qty, 1
                ),
                exDate,
                S0=rand_price,
                sigma=sigma,
                rate=rate,
                source=source,
            )
            # s_premium = premium_put_half + premium_sellx3 - premium_buyx2
            sell_premium_sq = sell_premium_sq + (
                s_new_sq[valid_qty:, :] - s_new_sq[:valid_qty, :]
            )
            net_profit_sq = premium_sq + sell_premium_sq
            portfolio_return = portfolio_return * (
                1 + max_risk * (net_profit_sq / init_invest_sq)
            )
            loop_cnt += 1
            if loop_cnt >= cycles:
                break

        if return_all_data:
            return portfolio_return
        else:
            return (
                np.percentile(portfolio_return, 50, axis=1).reshape(valid_qty, 1),
                portfolio_return.mean(axis=1).reshape(valid_qty, 1),
                portfolio_return.std(axis=1).reshape(valid_qty, 1),
            )

    @classmethod
    def expected_gain(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        max_risk,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        legs = len(positions)
        premium = cls.calc_premium(prices)
        init_invest = cls.calc_init_invest(positions, quantity, strikes, prices)

        cashflow_sign = cls.calc_cashflow_sign(prices)
        forward_slope = {
            ("Put", 1): 0,
            ("Put", -1): 0,
            ("Call", 1): -1,
            ("Call", -1): 1,
        }
        reverse_slope = {
            ("Put", 1): 1,
            ("Put", -1): -1,
            ("Call", 1): 0,
            ("Call", -1): 0,
        }
        cum_slope_forward = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).cumsum()
        cum_slope_reverse = np.flip(
            np.array(
                [
                    reverse_slope[
                        (positions[(legs - 1) - i_x], cashflow_sign[(legs - 1) - i_x])
                    ]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).cumsum(),
            0,
        )
        segment_slope = np.r_[
            cum_slope_reverse[0],
            (cum_slope_reverse[1:] + cum_slope_forward[:-1]),
            cum_slope_forward[-1],
        ]
        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_keypts = np.concatenate(
            (np.zeros_like(premium), dist.cdf(strikes), np.ones_like(premium)), axis=1
        )
        pdf_keypts = np.concatenate(
            (np.zeros_like(premium), dist.pdf(strikes), np.zeros_like(premium)), axis=1
        )
        keypts = np.concatenate(
            (
                np.zeros_like(premium),
                strikes,
                np.array([mean + 6 * std] * premium.size).reshape(premium.shape),
            ),
            axis=1,
        )
        # calculate the profit at each key pt
        # slope_array is the slope of each position relative to the strike
        #  and execution price. It determines if the value is
        #  (strike -  price) or (price -  strike)
        slope_array = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                + reverse_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).reshape(1, legs, 1)
        # use dimensional convention (combinations, strikes, prices)
        execution_price = strikes.copy().reshape(premium.size, 1, legs)
        profit = slope_array * (
            execution_price - strikes.reshape(strikes.shape + tuple([1]))
        )
        # if the position was sold (sign = 1), then you can only lose
        #  from the execution of the option. But if the position was
        #  bought (sign = -1) then it can only make money with the
        #  of the option
        profit = np.concatenate(
            [
                np.maximum(0, profit[:, [i], :])
                if cashflow_sign[i] == -1
                else np.minimum(0, profit[:, [i], :])
                for i in range(legs)
            ],
            axis=1,
        ).sum(axis=1)
        # just summed over the strike index so the resulting profit array
        #  is the profit calculated at each key pt (i.e. strike price)
        profit = (
            np.concatenate(
                (
                    segment_slope[0] * keypts[:, [1]],
                    profit,
                    segment_slope[-1] * (keypts[:, [-1]] - keypts[:, [-2]]),
                ),
                axis=1,
            )
            + premium
        )
        risk_over_invest = max_risk / init_invest
        exp_gain = np.zeros_like(premium)
        for i in range(legs + 1):
            if segment_slope[i] == 0:
                exp_gain += (1 + risk_over_invest * profit[:, [i]]) * (
                    cdf_keypts[:, [i + 1]] - cdf_keypts[:, [i]]
                )
            else:
                # calculate the expected price between x0 and x1
                # see block comments at start of function for
                #   deriv. of the eq. below.
                #      =integral(profit($)*P($)*d$ from X0 to X1)
                #      = (1 + (max_risk/max_loss)*((m*(u-x1)+max profit)*(
                #           CDF(X1)-CDF(X0))) - std**2*(pdf(X1)-pdf(X0)) * (
                #                max_risk/max_loss))
                exp_gain += (
                    1
                    + risk_over_invest
                    * (
                        segment_slope[i] * (mean - keypts[:, [i + 1]])
                        + profit[:, [i + 1]]
                    )
                ) * (
                    cdf_keypts[:, [i + 1]] - cdf_keypts[:, [i]]
                ) - risk_over_invest * segment_slope[
                    i
                ] * (
                    std ** 2
                ) * (
                    pdf_keypts[:, [i + 1]] - pdf_keypts[:, [i]]
                )
        return exp_gain

    @classmethod
    def expected_profit(
        cls,
        positions,
        quantity,
        strikes,
        prices,
        dist,
        total_pf=1e5,
        broker_fee=0,
        addtl_option_fee=0,
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        legs = len(positions)
        premium = cls.calc_premium(prices)
        cashflow_sign = cls.calc_cashflow_sign(prices)
        forward_slope = {
            ("Put", 1): 0,
            ("Put", -1): 0,
            ("Call", 1): -1,
            ("Call", -1): 1,
        }
        reverse_slope = {
            ("Put", 1): 1,
            ("Put", -1): -1,
            ("Call", 1): 0,
            ("Call", -1): 0,
        }
        cum_slope_forward = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).cumsum()
        cum_slope_reverse = np.flip(
            np.array(
                [
                    reverse_slope[
                        (positions[(legs - 1) - i_x], cashflow_sign[(legs - 1) - i_x])
                    ]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).cumsum(),
            0,
        )
        segment_slope = np.r_[
            cum_slope_reverse[0],
            (cum_slope_reverse[1:] + cum_slope_forward[:-1]),
            cum_slope_forward[-1],
        ]
        # get price prediction info
        mean = dist.mean()
        std = dist.std()
        cdf_keypts = np.concatenate(
            (np.zeros_like(premium), dist.cdf(strikes), np.ones_like(premium)), axis=1
        )
        pdf_keypts = np.concatenate(
            (np.zeros_like(premium), dist.pdf(strikes), np.zeros_like(premium)), axis=1
        )
        keypts = np.concatenate(
            (
                np.zeros_like(premium),
                strikes,
                np.array([mean + 6 * std] * premium.size).reshape(premium.shape),
            ),
            axis=1,
        )
        # calculate the profit at each key pt
        # slope_array is the slope of each position relative to the strike
        #  and execution price. It determine if the value is
        #  (strike -  price) or (price -  strike)
        slope_array = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                + reverse_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).reshape(1, legs, 1)
        # use dimensional convention (combinations, strikes, prices)
        execution_price = strikes.copy().reshape(premium.size, 1, legs)
        profit = slope_array * (
            execution_price - strikes.reshape(strikes.shape + tuple([1]))
        )
        # if the position was sold (sign = 1), then you can only lose
        #  from the execution of the option. But if the position was
        #  bought (sign = -1) then it can only make money with the
        #  of the option
        profit = np.concatenate(
            [
                np.maximum(0, profit[:, [i], :])
                if cashflow_sign[i] == -1
                else np.minimum(0, profit[:, [i], :])
                for i in range(legs)
            ],
            axis=1,
        ).sum(axis=1)
        # just summed over the strike index so the resulting profit array
        #  is the profit calculated at each key pt (i.e. strike price)
        profit = (
            np.concatenate(
                (
                    segment_slope[0] * keypts[:, [1]],
                    profit,
                    segment_slope[-1] * (keypts[:, [-1]] - keypts[:, [-2]]),
                ),
                axis=1,
            )
            + premium
        )
        exp_profit = np.zeros_like(premium)
        for i in range(legs + 1):
            if segment_slope[i] == 0:
                exp_profit += profit[:, [i]] * (
                    cdf_keypts[:, [i + 1]] - cdf_keypts[:, [i]]
                )
            else:
                # calculate the expected price between x0 and x1
                # see block comments at start of function for
                #   deriv. of the eq. below.
                #  exp_profit_between=integral(profit($)*P($)*d$ from X0 to X1)
                exp_profit += (
                    segment_slope[i] * (mean - keypts[:, [i + 1]]) + profit[:, [i + 1]]
                ) * (cdf_keypts[:, [i + 1]] - cdf_keypts[:, [i]]) - segment_slope[i] * (
                    std ** 2
                ) * (
                    pdf_keypts[:, [i + 1]] - pdf_keypts[:, [i]]
                )
        return exp_profit

    @classmethod
    def format_strikes_prices(
        cls,
        positions,
        cashflow_sign,
        quantity,
        strike_list=None,
        price_list=None,
        optionDB=None,
        purchaseDate=None,
        exDate=None,
        current_price=None,
        offeringCode=None,
        sigma=None,
        rate=None,
        tckr=None,
        source="storage",
    ):
        # format strikes and prices that may be passed in and provide defaults
        #  if none are passed
        if purchaseDate is not None and exDate is not None:
            days_out = (purchaseDate - exDate).days
        else:
            days_out = None
        # Step 1: Check if completed lists has been passed into function
        if strike_list is not None:
            assert type(strike_list) == list, (
                "Variable 'strike_list' is type "
                + str(type(strike_list))
                + ", but needs to be 'list'"
            )
            assert len(strike_list) == len(positions), (
                "list 'strike_list' has length "
                + str(len(strike_list))
                + ", but must match the length of the positions (%1i)"
                % (len(positions))
            )
        else:
            strike_list = [None] * len(positions)
        if price_list is not None:
            assert type(price_list) == list, (
                "Variable 'strike_list' is type "
                + str(type(price_list))
                + ", but needs to be 'list'"
            )
            assert len(price_list) == len(positions), (
                "list 'price_list' has length "
                + str(len(price_list))
                + ", but must match the length of the positions (%1i)"
                % (len(positions))
            )
        else:
            price_list = [None] * len(positions)
        # Step 2: format and 'interpret' the input
        valid_strikes = [x is not None for x in strike_list]
        if not np.all(valid_strikes):
            assert optionDB is not None, (
                "variable 'optionDB' cannot be"
                + "'None' while one of the strike arrays is also 'None'"
            )
            assert days_out is not None, (
                "variable 'days_out' cannot be"
                + "'None' while one of the strike arrays is also 'None'"
            )
            assert current_price is not None, (
                "variable 'current_price' cannot be"
                + "'None' while one of the strike arrays is also 'None'"
            )
            #            assert offeringCode is not None, (
            #                    "variable 'offeringCode' cannot be"
            #                    + "'None' while one of the strike arrays is also 'None'")
            strikeArray = np.array(
                optionDB.estimateSPXStrikesOffered(
                    current_price, days_out, offeringCode=offeringCode
                ),
                dtype=int,
            )
            strike_list = [strikeArray.copy() if x is None else x for x in strike_list]
        else:
            strikeArray = np.array([np.nan])
        # Step 3: ensure price_list is compatible with strike_list
        for i_x in range(len(positions)):
            if price_list[i_x] is None or price_list[i_x].size != strike_list[i_x].size:
                price_list[i_x] = cashflow_sign[i_x] * optionDB.PriceEst_BS(
                    tckr,
                    positions[i_x],
                    purchaseDate,
                    strike_list[i_x],
                    exDate,
                    S0=current_price,
                    sigma=sigma,
                    rate=rate,
                    source=source,
                )
            if np.sign(price_list[i_x].sum()) != np.sign(cashflow_sign[i_x]):
                price_list[i_x] = -1 * price_list[i_x]
            # now remove any duplicates
            option_pair = list(
                set(zip(strike_list[i_x].ravel(), price_list[i_x].ravel()))
            )
            x, p = zip(*option_pair)
            x = np.array(x, dtype=float)
            sort_index = x.argsort()
            strike_list[i_x] = x[sort_index]
            price_list[i_x] = np.array(p, dtype=float)[sort_index]
            assert price_list[i_x].size == strike_list[i_x].size, (
                "Internal Logic Error - price and strike array size "
                + "mismatch. \nStrike List:\n%s\nPrice List\n%s"
                % (str(strike_list), str(price_list))
            )

        return strike_list, price_list

    @classmethod
    def calc_max_profit(
        cls, positions, quantity, strikes, prices, current_value=np.inf
    ):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        legs = len(positions)
        premium = cls.calc_premium(prices)
        cashflow_sign = cls.calc_cashflow_sign(prices)
        slope = {("Put", 1): 1, ("Put", -1): -1, ("Call", 1): -1, ("Call", -1): 1}
        forward_slope = {
            ("Put", 1): 0,
            ("Put", -1): 0,
            ("Call", 1): -1,
            ("Call", -1): 1,
        }
        reverse_slope = {
            ("Put", 1): 1,
            ("Put", -1): -1,
            ("Call", 1): 0,
            ("Call", -1): 0,
        }
        cum_slope_forward = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).cumsum()
        cum_slope_reverse = np.flip(
            np.array(
                [
                    reverse_slope[
                        (positions[(legs - 1) - i_x], cashflow_sign[(legs - 1) - i_x])
                    ]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).cumsum(),
            0,
        )
        segment_slope = np.r_[
            cum_slope_reverse[0],
            cum_slope_reverse[1:] + cum_slope_forward[:-1],
            cum_slope_forward[-1],
        ]
        if segment_slope[-1] > 0:
            # max loss is the infinite amount that the stock can rise
            profit = np.empty_like(premium)
            profit.fill(np.inf)
        elif segment_slope[0] < 0:
            # max loss is the amount that the stock can fall + premium
            profit = np.empty_like(premium)
            profit.fill(current_value + premium)
        else:
            # trace the value at each strike price and choose the minimum
            #  value at strike price will be the sum of the value of each
            #  option in the strategy at that excise price
            slope_list = np.array(
                [
                    slope[(positions[i_x], cashflow_sign[i_x])]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).reshape(1, legs, 1)
            execution_price = strikes.copy().reshape((premium.size, 1, legs))
            profit = slope_list * (
                execution_price - strikes.reshape(strikes.shape + (1,))
            )
            profit = (
                np.concatenate(
                    [
                        np.maximum(0, profit[:, [i], :])
                        if cashflow_sign[i] == -1
                        else np.minimum(0, profit[:, [i], :])
                        for i in range(legs)
                    ],
                    axis=1,
                ).sum(axis=1)
                + premium
            )
            profit = profit.max(-1).reshape(premium.shape)

        return profit

    @classmethod
    def calc_max_loss(cls, positions, quantity, strikes, prices, current_value=np.inf):
        if strikes.size == 0 or prices.size == 0:
            return np.array([[]])
        legs = len(positions)
        premium = prices.sum(axis=1).reshape(prices.shape[0], 1)
        cashflow_sign = cls.calc_cashflow_sign(prices)
        slope = {("Put", 1): 1, ("Put", -1): -1, ("Call", 1): -1, ("Call", -1): 1}
        forward_slope = {
            ("Put", 1): 0,
            ("Put", -1): 0,
            ("Call", 1): -1,
            ("Call", -1): 1,
        }
        reverse_slope = {
            ("Put", 1): 1,
            ("Put", -1): -1,
            ("Call", 1): 0,
            ("Call", -1): 0,
        }
        cum_slope_forward = np.array(
            [
                forward_slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).cumsum()
        cum_slope_reverse = np.flip(
            np.array(
                [
                    reverse_slope[
                        (positions[(legs - 1) - i_x], cashflow_sign[(legs - 1) - i_x])
                    ]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).cumsum(),
            0,
        )
        segment_slope = np.r_[
            cum_slope_reverse[0],
            cum_slope_reverse[1:] + cum_slope_forward[:-1],
            cum_slope_forward[-1],
        ]
        if segment_slope[-1] < 0:
            # max loss is the infinite amount that the stock can rise
            init_invest = np.empty_like(premium)
            init_invest.fill(np.inf)
        elif segment_slope[0] > 0:
            # max loss is the amount that the stock can fall + premium
            init_invest = current_value + premium
        else:
            # trace the value at each strike price and choose the minimum
            #  value at strike price will be the sum of the value of each
            #  option in the strategy at that excise price
            slope_list = np.array(
                [
                    slope[(positions[i_x], cashflow_sign[i_x])]
                    for i_x in range(len(positions))
                ],
                dtype=int,
            ).reshape(1, legs, 1)
            execution_price = strikes.copy().reshape((premium.size, 1, legs))
            profit = slope_list * (
                execution_price - strikes.reshape(strikes.shape + (1,))
            )
            profit = (
                np.concatenate(
                    [
                        np.maximum(0, profit[:, [i], :])
                        if cashflow_sign[i] == -1
                        else np.minimum(0, profit[:, [i], :])
                        for i in range(legs)
                    ],
                    axis=1,
                ).sum(axis=1)
                + premium
            )
            init_invest = profit.min(-1).reshape(premium.shape)

        return init_invest

    @classmethod
    def calc_init_invest(
        cls, positions, quantity, strikes, prices, current_value=np.inf
    ):
        return np.abs(
            cls.calc_max_loss(
                positions, quantity, strikes, prices, current_value=current_value
            )
        )

    @classmethod
    def calc_premium(cls, prices):
        return prices.sum(axis=1).reshape(prices.shape[0], 1)

    @classmethod
    def calc_cashflow_sign(cls, prices):
        if type(prices) == list:
            return [int(np.sign(p.sum())) for p in prices]
        else:
            return np.sign(prices.sum(0)).astype(int)

    @classmethod
    def calc_value_expiration(
        cls,
        expiration_price,
        strikes,
        premium=None,
        strategy=None,
        prices=None,
        positions=None,
        cashflow_sign=None,
        quantity=None,
        source="storage",
    ):
        """
        calculates the expected value of a group of option strategies based
        on the price

        args:
            expiration_price is the price of the underlying tckr to evaluate
                the strategy on
            strikes can be a list of strikes or a 2d array of strikes
                similar to strikes_t in the functions above
            prices or premium must be provided. If both are provided, then
                the function will use premium
            strategy or positions, cashflow_sign and quantity must be provided
                if all are provided, then the three are used

        returns:
            the calculated value of the

        debug:
        source = 'storage'
        if final_df is not None:
            positions = None
            cashflow_sign = None
            expiration_price = None
            quantity = None
            eval_row = 0
            exDate, strategy, premium = final_df.loc[eval_row,
                    ['ExpirationDate', 'Strategy', 'Premium']]
            strikes = final_df.loc[eval_row, ['Strike_0', 'Strike_1',
                    'Strike_2', 'Strike_3']].astype(float)
            strikes = strikes[np.isfinite(strikes)]
            if expiration_price is None:
                if predictionModel is None:
                    predictionModel = (
                                MarketPredictions.getDefaultPredictionModel_HDF5())
                expiration_price = predictionModel.get_current_price(
                        exDate, tckr, source=source)

        """
        if positions is None or cashflow_sign is None or quantity is None:
            assert strategy is not None, (
                "if positions, cashflow_sign, or quantity is None, then"
                + " strategy must be provided"
            )
            positions, cashflow_sign, quantity = cls.interpret_strategy(strategy)
        legs = len(positions)
        # strikes
        if type(strikes) == np.ndarray:
            strikes = strikes.reshape(strikes.size / legs, legs).astype(float)
        elif type(strikes) in [
            int,
            float,
            np.int8,
            np.int16,
            np.int32,
            np.int64,
            np.float16,
            np.float32,
            np.float64,
        ]:
            strikes = np.array(strikes, dtype=float).reshape(1, 1)
        else:
            strikes = np.array(strikes, dtype=float).reshape(len(strikes), legs)
        row_cnt = strikes.shape[0]
        # premium/price
        if premium is None:
            assert prices is not None, "Prices or Premium must be provided"
            if type(prices) == np.ndarray:
                prices = prices.reshape(prices.size / legs, legs)
            elif type(prices) in [
                int,
                float,
                np.int8,
                np.int16,
                np.int32,
                np.int64,
                np.float16,
                np.float32,
                np.float64,
            ]:
                prices = np.array(prices).reshape(1, 1).astype(float)
            else:
                prices = np.array(prices, dtype=float).reshape(len(prices) / legs, legs)
            premium = cls.calc_premium(prices)
        else:
            if type(premium) == np.ndarray:
                premium = premium.reshape(premium.size, 1).astype(float)
            elif type(premium) in [
                int,
                float,
                np.int8,
                np.int16,
                np.int32,
                np.int64,
                np.float16,
                np.float32,
                np.float64,
            ]:
                premium = np.array(premium, dtype=float).reshape(1, 1)
            else:
                premium = np.array(premium, dtype=float).reshape(len(premium), 1)
        # expiration price
        if type(expiration_price) == np.ndarray:
            expiration_price = expiration_price.reshape(
                expiration_price.size, 1
            ).astype(float)
        elif type(expiration_price) in [
            int,
            float,
            np.int8,
            np.int16,
            np.int32,
            np.int64,
            np.float16,
            np.float32,
            np.float64,
        ]:
            expiration_price = np.array(expiration_price, dtype=float).reshape(1, 1)
        else:
            expiration_price = np.array(expiration_price, dtype=float).reshape(
                len(expiration_price), 1
            )
        assert premium.shape[0] == strikes.shape[0], (
            "Premium/Price and Strike shapes don't match:"
            + "\n\tPremium: "
            + str(premium.shape)
            + "\n\tStrikes: "
            + str(strikes.shape)
        )
        assert (
            expiration_price.size == 1 or expiration_price.shape[0] == strikes.shape[0]
        ), (
            "Expiration Price and Strike shapes don't match:"
            + "\n\tExpiration Price: "
            + str(expiration_price.shape)
            + "\n\tStrikes: "
            + str(strikes.shape)
        )
        slope = {("Put", 1): 1, ("Put", -1): -1, ("Call", 1): -1, ("Call", -1): 1}
        # slope_list is the slope of each position relative to the strike
        #  and execution price. It determines if the value is
        #  (strike -  price) or (price -  strike)
        slope_list = np.array(
            [
                slope[(positions[i_x], cashflow_sign[i_x])]
                for i_x in range(len(positions))
            ],
            dtype=int,
        ).reshape(1, legs)
        profit = slope_list * (expiration_price - strikes)
        # if the position was sold (sign = 1), then you can only lose
        #  from the execution of the option. But if the position was
        #  bought (sign = -1) then it can only make money with the
        #  of the option
        profit = (
            np.concatenate(
                [
                    np.maximum(0, profit[:, [i]])
                    if cashflow_sign[i] == -1
                    else np.minimum(0, profit[:, [i]])
                    for i in range(legs)
                ],
                axis=1,
            )
            .sum(axis=1)
            .reshape(row_cnt, 1)
            + premium
        )
        if profit.size == 1:
            return float(profit[0, 0])
        else:
            return profit.astype(float)

    @classmethod
    def monte_carlo_singe_date(
        cls,
        tckr,
        purchaseDate,
        exDate_i,
        code_i,
        predictionModel,
        strategy,
        lifetime,
        n_mc,
        cum_cycles,
        max_risk,
        strikes,
        prices,
        total_pf,
        broker_fee,
        addtl_option_fee,
        return_all_data,
        source,
    ):
        pass


def run_all_strategies_single(
    tckr,
    purchaseDate,
    exDate,
    call_symbol,
    call_strike,
    call_sell,
    call_buy,
    put_symbol,
    put_strike,
    put_sell,
    put_buy,
    predictionModel=None,
    optionDB=None,
    current_price=None,
    n_mc=100,
    cum_cycles=20,
    max_risk=0.2,
    total_pf=100000,
    broker_fee=4.95,
    addtl_option_fee=0.12,
    return_all_data=True,
    source="storage",
):
    """
    debug:
    tckr = 'ixSPX'
    purchaseDate = pd.Timestamp(2018, 2, 1, 10)
    predictionModel = MarketPredictions.getDefaultPredictionModel_HDF5()
    current_price = predictionModel.get_current_price(purchaseDate, tckr)
    optionDB = MarketData.OptionData(stockDB=predictionModel.stockDB)
    n_mc = 100
    cum_cycles = 20
    max_risk = 0.2
    total_pf = 100000
    broker_fee = 4.95
    addtl_option_fee = 0.12,
    return_all_data = True
    source = 'storage'
    purchaseDate = Util.formatDateInput(purchaseDate)
    fid_data = optionDB.options_for_date(tckr, purchaseDate,
                                         method='closest')
    # remove exDate less than 3 days away
    fid_data = fid_data.loc[fid_data.DaysToExpiration > 2, :]
    fid_data = optionDB.summarize_option_data(fid_data,
                                          price_calc='weighted')
    current_price = fid_data.UnderlyingPrice.iloc[0]
    all_exdates = pd.DatetimeIndex(list(set(fid_data.ExDate)))
    exDate = all_exdates[0]
    purchaseDate = fid_data.QueryTimeStamp.iloc[0]
    sub_DB = fid_data.loc[fid_data.ExDate == exDate,
                          ['Type', 'Strike', 'Bid', 'Ask']]
    select_call = sub_DB.Type == 'Call'
    call_strike = sub_DB.loc[select_call, 'Strike'].values
    call_sell = sub_DB.loc[select_call, 'Bid'].values
    call_buy = sub_DB.loc[select_call, 'Ask'].values
    put_strike = sub_DB.loc[~select_call, 'Strike'].values
    put_sell = sub_DB.loc[~select_call, 'Bid'].values
    put_buy = sub_DB.loc[~select_call, 'Ask'].values

    """
    from src.Positions import IronCondor
    from src.Positions import VerticalSpread

    ic_cls = IronCondor.IronCondor
    spread_cls = VerticalSpread.VerticalSpread
    base_cls = ComplexOption
    tckr = Util.convertTckrToUniversalFormat(tckr)
    if predictionModel is None:
        predictionModel = getDefaultPredictionModel_HDF5()
    if current_price is None:
        current_price = predictionModel.get_current_price(purchaseDate, tckr)
    purchaseDate = Util.formatDateInput(purchaseDate)
    exDate = Util.formatDateInput(exDate)
    # ensure call and put data are in the correct format
    if type(call_strike) != np.ndarray:
        call_strike = np.array(call_strike)
    call_strike = call_strike.reshape(call_strike.size, 1)
    if type(call_sell) != np.ndarray:
        call_sell = np.array(call_sell)
    call_sell = call_sell.reshape(call_sell.size, 1)
    if type(call_buy) != np.ndarray:
        call_buy = np.array(call_buy)
    if call_buy.sum() > 0:
        call_buy *= -1
    call_buy = call_buy.reshape(call_buy.size, 1)
    if type(put_strike) != np.ndarray:
        put_strike = np.array(put_strike)
    put_strike = put_strike.reshape(put_strike.size, 1)
    if type(put_sell) != np.ndarray:
        put_sell = np.array(put_sell)
    put_sell = put_sell.reshape(put_sell.size, 1)
    if type(put_buy) != np.ndarray:
        put_buy = np.array(put_buy)
    if put_buy.sum() > 0:
        put_buy *= -1
    put_buy = put_buy.reshape(put_buy.size, 1)

    arbitrage_op = ComplexOption.highlight_arbitrage(
        call_symbol,
        call_strike,
        call_sell,
        call_buy,
        put_symbol,
        put_strike,
        put_sell,
        put_buy,
    )

    # iron condor
    print("Evaluating Iron Condor/Butterfly Strategies")
    strikes = [put_strike, put_strike, call_strike, call_strike]
    prices = [put_buy, put_sell, call_sell, call_buy]
    best_ic, ic_results = ic_cls.monte_carlo_single_date(
        tckr,
        purchaseDate,
        exDate,
        None,
        predictionModel,
        strategy="Iron Condor_Butterfly",
        current_price=current_price,
        n_mc=n_mc,
        cum_cycles=cum_cycles,
        max_risk=max_risk,
        strikes=strikes,
        prices=prices,
        total_pf=total_pf,
        broker_fee=broker_fee,
        addtl_option_fee=addtl_option_fee,
        return_all_data=True,
        optimize=True,
        source=source,
    )
    # bullish put spread
    print("Evaluating Bullish Put Spread Strategies")
    strikes = [put_strike, put_strike]
    prices = [put_buy, put_sell]
    best_BuPS, BuPS_results = spread_cls.monte_carlo_single_date(
        tckr,
        purchaseDate,
        exDate,
        None,
        predictionModel,
        strategy="Bullish Put Spread",
        current_price=current_price,
        n_mc=n_mc,
        cum_cycles=cum_cycles,
        max_risk=max_risk,
        strikes=strikes,
        prices=prices,
        total_pf=total_pf,
        broker_fee=broker_fee,
        addtl_option_fee=addtl_option_fee,
        return_all_data=True,
        optimize=True,
        source=source,
    )
    # bullish call spread
    print("Evaluating Bullish Call Spread Strategies")
    strikes = [call_strike, call_strike]
    prices = [call_buy, call_sell]
    best_BuCS, BuCS_results = spread_cls.monte_carlo_single_date(
        tckr,
        purchaseDate,
        exDate,
        None,
        predictionModel,
        strategy="Bullish Call Spread",
        current_price=current_price,
        n_mc=n_mc,
        cum_cycles=cum_cycles,
        max_risk=max_risk,
        strikes=strikes,
        prices=prices,
        total_pf=total_pf,
        broker_fee=broker_fee,
        addtl_option_fee=addtl_option_fee,
        return_all_data=True,
        optimize=True,
        source=source,
    )
    # call
    print("Evaluating Long Call Strategies")
    strikes = [call_strike]
    prices = [call_buy]
    best_call, call_results = base_cls.monte_carlo_single_date(
        tckr,
        purchaseDate,
        exDate,
        None,
        predictionModel,
        strategy="+C",
        current_price=current_price,
        n_mc=n_mc,
        cum_cycles=cum_cycles,
        max_risk=max_risk,
        strikes=strikes,
        prices=prices,
        total_pf=total_pf,
        broker_fee=broker_fee,
        addtl_option_fee=addtl_option_fee,
        return_all_data=True,
        optimize=True,
        source=source,
    )

    final_DF = pd.concat(
        [ic_results, BuPS_results, BuCS_results, call_results],
        axis=0,
        ignore_index=True,
        sort=True,
    )
    final_DF.sort_values("Median_PF_Return", ascending=False, inplace=True)
    final_DF.index = range(final_DF.index.size)
    column_order = [
        "Strategy",
        "Category",
        "Outcome_Qty",
        "Yield_Cycles",
        "PurchaseDate",
        "ExpirationDate",
        "DaysToExp",
        "DaysInvested",
        "Max_Risk",
        "CurrentPrice",
        "Prediction_Price",
        "Prediction_Std",
        "Total_PF",
        "Trade_Fee",
        "Addtl_Contract_Fee",
        "Strike_0",
        "Strike_1",
        "Strike_2",
        "Strike_3",
        "Price_0",
        "Price_1",
        "Price_2",
        "Price_3",
        "Premium",
        "Max_Loss",
        "Max_Profit",
        "Median_PF_Return",
        "Mean_PF_Return",
        "Std_PF_Return",
        "Avg_PF_Return_PerDay",
        "Avg_Ann_Return",
        "Expected_Stock_Return",
        "AvgReturnVsStock",
        "Expected_Gain_Return",
        "Win_Percentage",
        "Ann_Volatility",
    ]
    final_DF = final_DF.loc[:, column_order]

    return final_DF, arbitrage_op
