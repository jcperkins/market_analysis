# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 14:16:33 2018

@author: Cory Perkins

gdax sandbox gui - https://public.sandbox.gdax.com
gemini sandbox gui - https://exchange.sandbox.gemini.com/

gdax_sandbox_key = 'hSLw2bzU26TLMkM1'
gdax_sandbox_secret = 'kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK'
gemini_sandbox_key = 'fkr2M8jvlsvvCcZj0S6C'
gemini_sandbox_secret = '2EM2eu4jxqFbRVEe1YMPzGXT5fws'
"""

import ccxt
import pandas as pd
import numba as np
from numba import jit
from config import default_settings


class CyptoExchange(object):
    """
    gdax sandbox gui - https://public.sandbox.gdax.com
    gemini sandbox gui - https://exchange.sandbox.gemini.com/

    gdax_sandbox_key = 'hSLw2bzU26TLMkM1'
    gdax_sandbox_secret = 'kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK'
    gemini_sandbox_key = 'fkr2M8jvlsvvCcZj0S6C'
    gemini_sandbox_secret = '2EM2eu4jxqFbRVEe1YMPzGXT5fws'
    """

    def __init__(self, work_proxy=True, storage_file=None):
        if storage_file is None:
            storage_file = os.path.join(default_settings.data_dir, "CryptoCurrency.h5")
        # set up configuration data
        self.storage = pd.HDFStore(storage_file)
        self.storage.close()
        self.exch_config = {
            "gdax_sandbox": {
                "id": "gdax_sandbox",
                "apiKey": "hSLw2bzU26TLMkM1",
                "secret": "kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK",
            },
            "gemini_sandbox": {
                "id": "gemini_sandbox",
                "apiKey": "fkr2M8jvlsvvCcZj0S6C",
                "secret": "2EM2eu4jxqFbRVEe1YMPzGXT5fws",
            },
            "bitmex_sandbox": {
                "id": "bitmex_sandbox",
                "apiKey": "KMH5HGa9Z5KMUTsT5gBVRkn9",
                "secret": "8UJ6bxo6V5zx-9sRPMMMSMmger3kwres-BqNkm5mh0PbdIpS",
            },
        }
        self.work_proxy = work_proxy
        if work_proxy:
            for exchange in self.exch_config.keys():
                self.exch_config[exchange]["proxies"] = {
                    "http": "http://webproxy.ext.ti.com:80",
                    "https": "http://webproxy.ext.ti.com:80",
                }
            self.default_config = {
                "proxies": {
                    "http": "http://webproxy.ext.ti.com:80",
                    "https": "http://webproxy.ext.ti.com:80",
                }
            }
        else:
            self.default_config = {}

        self.exch_dict = {}
        self.market_dict = {}
        self.symbols_by_market = {}
        self.markets_by_symbol = {}

    def explore_gdax(self):
        gdax = ccxt.gdax(self.exch_config["gdax_sandbox"])
        gdax_market = gdax.load_markets()
        pair = "BTC/USD"
        print("Market Data for " + pair)
        text = [
            key + ": " + str(gdax_market[pair][key]) for key in gdax_market[pair].keys()
        ]
        print("\n".join(text))
        print("\n\nMarket Functions")
        text = [key + ": " + str(gdax.has[key]) for key in gdax.has.keys()]
        print("\n".join(text))
        print("Exchange Methods")
        print(dir(gdax))

        gem = ccxt.gemini(self.exch_config["gemini_sandbox"])
        gem_market = gem.load_markets()
        pair = "BTC/USD"
        print("Market Data for " + pair)
        text = [
            key + ": " + str(gem_market[pair][key]) for key in gem_market[pair].keys()
        ]
        print("\n".join(text))
        print("\n\nMarket Functions")
        text = [key + ": " + str(gem.has[key]) for key in gem.has.keys()]
        print("\n".join(text))
        print("Exchange Methods")
        print(dir(gem))

    def init_exchanges(self):
        exch = [
            ccxt.gdax(self.default_config),
            ccxt.gemini(self.default_config),
            ccxt.kraken(self.default_config),
            ccxt.bitmex(self.default_config),
            ccxt.btcx(self.default_config),
        ]
        self.exch_dict = {}
        self.market_dict = {}
        self.symbols_by_market = {}
        self.markets_by_symbol = {}
        for i_exch in exch:
            print("\n" + i_exch.id)
            i_market = exch_dict[i_exch.id] = i_exch
            market_dict[i_exch.id] = i_exch.load_markets()
            unique_symbols = market_dict[i_exch.id].keys()
            temp_sym = []
            for sym in unique_symbols:
                if (
                    market_dict[i_exch.id][sym]["info"]["active"]
                    and market_dict[i_exch.id][sym]["info"]["type"] == "swap"
                ):
                    temp_sym.append(sym)
            print(temp_sym)
            market_symbols[i_exch.id] = temp_sym
        unique_symbols = list(set(sum(market_symbols.values())))

    @classmethod
    def process_market(cls, m_id, m_json):
        symbols = m_json.keys()
        if m_id in ["gdax", "gdax_sandbos"]:
            for sym in symbols:
                pass


print("\n\nMarket Functions")
t = [key + ": " + str(test.has[key]) for key in test.has.keys()]
print("\n".join(text))
tm = test.load_markets()
print(tm.keys())
