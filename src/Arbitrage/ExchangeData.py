# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 14:16:33 2018

@author: Cory Perkins

gdax sandbox gui - https://public.sandbox.gdax.com
gemini sandbox gui - https://exchange.sandbox.gemini.com/

gdax_sandbox_key = 'hSLw2bzU26TLMkM1'
gdax_sandbox_secret = 'kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK'
gemini_sandbox_key = 'fkr2M8jvlsvvCcZj0S6C'
gemini_sandbox_secret = '2EM2eu4jxqFbRVEe1YMPzGXT5fws'
"""

import ccxt
import pandas as pd
import numpy as np
from numba import jit
from config import default_settings


class CryptoExchange(object):
    """
    gdax sandbox gui - https://public.sandbox.gdax.com
    gemini sandbox gui - https://exchange.sandbox.gemini.com/

    gdax_sandbox_key = 'hSLw2bzU26TLMkM1'
    gdax_sandbox_secret = 'kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK'
    gemini_sandbox_key = 'fkr2M8jvlsvvCcZj0S6C'
    gemini_sandbox_secret = '2EM2eu4jxqFbRVEe1YMPzGXT5fws'
    """

    def __init__(self, work_proxy=True, storage_file=None):
        # set up configuration data
        if storage_file is None:
            storage_file = os.path.join(default_settings.data_dir, "CryptoCurrency.h5")

        self.storage = pd.HDFStore(storage_file)
        self.storage.close()
        self.exch_config = {
            "gdax_sandbox": {
                "id": "gdax_sandbox",
                "apiKey": "hSLw2bzU26TLMkM1",
                "secret": "kmWBLNw7FT39rzPUeAqglx5iiVFGEsAK",
            },
            "gemini_sandbox": {
                "id": "gemini_sandbox",
                "apiKey": "fkr2M8jvlsvvCcZj0S6C",
                "secret": "2EM2eu4jxqFbRVEe1YMPzGXT5fws",
            },
            "bitmex_sandbox": {
                "id": "bitmex_sandbox",
                "apiKey": "KMH5HGa9Z5KMUTsT5gBVRkn9",
                "secret": "8UJ6bxo6V5zx-9sRPMMMSMmger3kwres-BqNkm5mh0PbdIpS",
            },
        }
        self.work_proxy = work_proxy
        if work_proxy:
            for exchange in self.exch_config.keys():
                self.exch_config[exchange]["proxies"] = {
                    "http": "http://webproxy.ext.ti.com:80",
                    "https": "http://webproxy.ext.ti.com:80",
                }
            self.default_config = {
                "proxies": {
                    "http": "http://webproxy.ext.ti.com:80",
                    "https": "http://webproxy.ext.ti.com:80",
                }
            }
        else:
            self.default_config = {
                "http": "http://proxyconfig.itg.ti.com/proxy.pac",
                "https": "http://proxyconfig.itg.ti.com/proxy.pac",
            }

        self.fetch_method = {
            "multiple": ["kraken", "poloniex"],
            "single": [
                "gdax",
                "gdax_sandbox",
                "gemini",
                "gemini_sandbox",
                "bitmex",
                "bitmex_sandbox",
                "kraken",
            ],
        }
        self.exch_dict = {}
        self.market_dict = {}
        self.symbols_by_market = {}
        self.markets_by_symbol = {}
        self.taker_fees = {}
        self.maker_fees = {}
        self.margin_bool = {}
        self.limits = {}
        self.precision = {}

    def explore_gdax_gemini(self):
        gdax = ccxt.gdax(self.exch_config["gdax_sandbox"])
        gdax_market = gdax.load_markets()
        pair = "BTC/USD"
        print("Market Data for " + pair)
        text = [
            key + ": " + str(gdax_market[pair][key]) for key in gdax_market[pair].keys()
        ]
        print("\n".join(text))
        print("\n\nMarket Functions")
        text = [key + ": " + str(gdax.has[key]) for key in gdax.has.keys()]
        print("\n".join(text))
        print("Exchange Methods")
        print(dir(gdax))
        gem = ccxt.gemini(self.exch_config["gemini_sandbox"])
        gem_market = gem.load_markets()
        pair = "BTC/USD"
        print("Market Data for " + pair)
        text = [
            key + ": " + str(gem_market[pair][key]) for key in gem_market[pair].keys()
        ]
        print("\n".join(text))
        print("\n\nMarket Functions")
        text = [key + ": " + str(gem.has[key]) for key in gem.has.keys()]
        print("\n".join(text))
        print("Exchange Methods")
        print(dir(gem))

    def init_exchanges(self):
        exch = [
            ccxt.gdax(self.default_config),
            ccxt.gemini(self.default_config),
            ccxt.kraken(self.default_config),
            ccxt.bitmex(self.default_config),
            ccxt.btcx(self.default_config),
            ccxt.poloniex(self.default_config),
        ]
        self.exch_dict = {}
        self.market_dict = {}
        self.symbols_by_market = {}
        self.markets_by_symbol = {}
        self.taker_fees = {}
        self.maker_fees = {}
        self.margin_bool = {}
        self.limits = {}
        self.precision = {}
        # process loading the markets first and then loop again
        #  for fees to avoid querying too fast
        for i_exch in exch:
            name = i_exch.id
            print("\n" + name)
            self.exch_dict[name] = i_exch
            i_market = self.market_dict[name] = i_exch.load_markets()
            self.process_market_json(name, i_market)
        self.unique_symbols = self.markets_by_symbol.keys()

    def process_market_json(self, m_id, m_json):
        """
        populates the config information from each exchange
        """
        symbols = m_json.keys()
        self.limits[m_id] = {}

        if m_id in ["gdax", "gdax_sandbos"]:
            self.margin_bool[m_id] = m_json[symbols[0]]["info"]["margin_enabled"]
            self.maker_fees[m_id] = m_json[symbols[0]]["maker"]
            self.taker_fees[m_id] = min(0.0025, m_json[symbols[0]]["taker"])
            valid_sym = []
            for sym in symbols:
                data = m_json[sym]
                if data["active"]:
                    self.limits[m_id][sym] = data["limits"]
                    valid_sym.append(sym)
                    if sym in self.markets_by_symbol.keys():
                        self.markets_by_symbol[sym].append(m_id)
                    else:
                        self.markets_by_symbol[sym] = [m_id]
            self.symbols_by_market[m_id] = valid_sym
        elif m_id in ["gemini", "gemini_sandbos"]:
            self.margin_bool[m_id] = False
            self.maker_fees[m_id] = min(0.0025, m_json[symbols[0]]["taker"])
            self.taker_fees[m_id] = min(0.0025, m_json[symbols[0]]["taker"])
            valid_sym = []
            for sym in symbols:
                valid_sym.append(sym)
                self.limits[m_id][sym] = {"max": None, "min": None}
                if sym in self.markets_by_symbol.keys():
                    self.markets_by_symbol[sym].append(m_id)
                else:
                    self.markets_by_symbol[sym] = [m_id]
            self.symbols_by_market[m_id] = valid_sym
        elif m_id == "kraken":
            self.margin_bool[m_id] = False
            self.precision[m_id] = m_json[symbols[0]]["precision"]
            self.maker_fees[m_id] = m_json[symbols[0]]["tiers"]["maker"][0][1]
            self.taker_fees[m_id] = m_json[symbols[0]]["tiers"]["taker"][0][1]
            valid_sym = []
            for sym in symbols:
                data = m_json[sym]
                if data["active"] and not data["darkpool"]:
                    valid_sym.append(sym)
                    self.limits[m_id][sym] = data["limits"]
                    if sym in self.markets_by_symbol.keys():
                        self.markets_by_symbol[sym].append(m_id)
                    else:
                        self.markets_by_symbol[sym] = [m_id]
            self.symbols_by_market[m_id] = valid_sym
        elif m_id in ["bitmex", "bitmex_sandbox"]:
            self.margin_bool[m_id] = False
            self.precision[m_id] = m_json[symbols[0]]["precision"]
            self.maker_fees[m_id] = abs(m_json[symbols[0]]["maker"])
            self.taker_fees[m_id] = m_json[symbols[0]]["taker"]
            valid_sym = []
            for sym in symbols:
                data = m_json[sym]
                if data["active"] and not data["future"]:
                    valid_sym.append(sym)
                    self.limits[m_id][sym] = data["limits"]
                    if sym in self.markets_by_symbol.keys():
                        self.markets_by_symbol[sym].append(m_id)
                    else:
                        self.markets_by_symbol[sym] = [m_id]
            self.symbols_by_market[m_id] = valid_sym
        elif m_id in ["poloniex"]:
            self.margin_bool[m_id] = False
            self.precision[m_id] = m_json[symbols[0]]["precision"]
            self.maker_fees[m_id] = abs(m_json[symbols[0]]["maker"])
            self.taker_fees[m_id] = m_json[symbols[0]]["taker"]
            valid_sym = []
            for sym in symbols:
                data = m_json[sym]
                if data["active"]:
                    valid_sym.append(sym)
                    self.limits[m_id][sym] = data["limits"]
                    if sym in self.markets_by_symbol.keys():
                        self.markets_by_symbol[sym].append(m_id)
                    else:
                        self.markets_by_symbol[sym] = [m_id]
            self.symbols_by_market[m_id] = valid_sym

    def get_multiple_tckrs(self, symbol="kraken"):
        assert symbol in self.fetch_method["multiple"], (
            "%s exchange not supported" % symbol
        )
        data = pd.DataFrame(
            self.exch_dict[symbol].fetch_tickers(symbols=self.symbols_by_market[symbol])
        ).T
        query_time = pd.Timestamp(data.loc[data.index[0], "datetime"])
        data = data[["ask", "bid", "info"]]
        data["Query_Time"] = query_time
        return data

    def interaction_table(self, priceDF):
        """
        need a pd.DataFrame of ask bid prices with the smbols as the index
        debug:
        sym_full = ['USD/EUR', 'USD/GDP', 'USD/CHF', 'USD/CAD', 'EUR/GDP',
                    'EUR/CHF', 'EUR/CAD', 'GDP/CHF', 'GDP/CAD', 'CHF/CAD']
        temp = np.array([[0.740, 0.741], [0.657, 0.657], [1.060, 1.061],
                      [1.047, 1.011], [0.888, 0.889], [1.432, 1.433],
                      [1.366, 1.366], [1.612, 1.614], [1.538, 1.538],
                      [0.953, 0.953]], dtype=float)
        priceDF = pd.DataFrame(data=temp, index=sym_full,
                               columns=['bid', 'ask'])
        """
        #        invalid = priceDF.bid > priceDF.ask
        #        if invalid.any():
        #            priceDF = priceDF.loc[invalid, :]
        temp = np.array([sym.split("/") for sym in priceDF.index])
        sym_ind = list(set(temp.ravel()))
        sym_ind.sort()
        base_index = [sym_ind.index(s) for s in temp[:, 0]]
        quote_index = [sym_ind.index(s) for s in temp[:, 1]]
        table = np.empty((len(sym_ind), len(sym_ind)))
        table.fill(np.inf)
        table[range(len(sym_ind)), range(len(sym_ind))] = 1
        table[base_index, quote_index] = priceDF.bid
        table[quote_index, base_index] = 1.0 / priceDF.ask
        return sym_ind, table

    def bellman_ford(self, sym_ind, table, fee=0.0026):
        """
        takes the symbol index and interaction table from
        self.interaction_table and returns false if no arbitrage loop and
        the loop if there is one

        fee is the est. fee per trade as a percentage and is the
            kraken default

        -------------- readable version of the algo is----------------------

        # find minimum dist from source for each symbol
        # nested loops runs 14 ms for 22 symbols but array based logic runs
        #  in 47 us for 22 symbols
        for k in range(N):
            for base in range(N):
                for quote in range(N):
                    if (np.isfinite(w[base, quote])
                            and dist[base] + w[base, quote] < dist[quote]):
                        dist[quote] = dist[base] + w[base, quote]
                        prev[quote] = base

        # check for negative cycles
        # nested loops ran in 158 us for 22 symbols but the array version
        #  runs in 7 us
        arbitrage = False
        for base in range(N):
            for quote in range(N):
                if (dist[base] + w[base, quote] < dist[quote]):
                    # Node j is part of a negative cycle
                    arbitrage = quote
                    break
            if arbitrage != False:
                break

        --------------------------------------------------------------------
        debug:
        sym_ind = np.array(['CAD', 'CHF', 'EUR', 'GBP', 'USD'])
        table = np.array([[1.   , 1.049, 0.732, 0.650, 0.955],
                          [0.953, 1.   , 0.698, 0.620, 0.943],
                          [1.366, 1.433, 1.   , 0.889, 1.351],
                          [1.538, 1.614, 1.126, 1.   , 1.522],
                          [1.011, 1.061, 0.741, 0.657, 1.   ]])

        output should be:
            (['EUR/GBP', 'GBP/USD', 'USD/EUR'], [0.889, 1.522, 0.741])
        """
        # clean table to remove rows/cols with only 2 populated values
        #  if the only connections are to itself and 1 other symbol, then it
        #  is a trianglular arbitration dead end
        # valid_rows = np.arange(len(sym_ind))[np.isfinite(table).sum(1) > 2]
        valid_rows = np.isfinite(table).sum(1) > 2
        if not valid_rows.all():
            sym_ind = sym_ind[valid_rows]
            table = table[valid_rows, :][:, valid_rows]
        N = len(sym_ind)
        indices = np.arange(N)
        w = -np.log(table * (1 - fee))
        w[indices, indices] = 0
        # set the dist from source and previous point arrays
        dist = np.array([np.inf] * N, dtype=float)
        prev = np.array([0] * N, dtype=int)
        source = 0
        dist[source] = 0

        # find minimum dist from source for each symbol
        loopcnt = 0
        while True:
            new_dist = dist.reshape(N, 1) + w
            min_base = new_dist.argmin(0)
            new_dist = new_dist[min_base, indices]
            shorter_i = indices[new_dist < dist]
            if shorter_i.size > 0:
                dist[shorter_i] = new_dist[shorter_i]
                prev[shorter_i] = min_base[shorter_i]
                if loopcnt < N - 1:
                    loopcnt += 1
                else:
                    break
            else:
                break

        # check for negative cycles
        neg_cycle = (dist.reshape(N, 1) + w).min(0) < dist
        if neg_cycle.any():
            arbitrage = indices[neg_cycle].min()
        else:
            arbitrage = False

        # runs 4 us without the print statement
        if arbitrage is not False:
            trade_list = []
            i_path = []
            trade_gains = []
            i = arbitrage
            while True:
                trade_list.append(sym_ind[prev[i]] + "/" + sym_ind[i])
                trade_gains.append(table[prev[i], i])
                i_path.append(i)
                i = prev[i]
                if i in i_path:
                    trade_start = i_path.index(i)
                    trade_list = trade_list[trade_start:]
                    trade_list.reverse()
                    trade_gains = trade_gains[trade_start:]
                    trade_gains.reverse()
                    #                    print('Trade %s for a gain of %1.4f' % (
                    #                            str(trade_list), np.prod(trade_gains)))
                    break
            return trade_list, trade_gains
        else:
            return None


def main():
    pass


if __name__ == "__main__":
    main()

# print('\n\nMarket Functions')
# t = [key + ": " + str(test.has[key]) for key in test.has.keys()]
# print('\n'.join(text))
# tm = test.load_markets()
# print tm.keys()
