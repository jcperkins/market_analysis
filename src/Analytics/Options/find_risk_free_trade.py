"""
-------------------------------------------------------------------------------
Name:        find_risk_free_trade.py
Project:     MarketAnalysis_Dev
Module:      MarketAnalysis_Dev
Purpose:

Author:      Cory Perkins

Created:     1/28/20, 11:47 AM
Modified:    1/28/20, 11:47 AM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
from typing import List

risk_free_trade_columns = [
    'tckr',
    'quote_ts',
    'expiration',
    "option_type",
    "stock_price",
    "sell_symbol",
    "sell_strike",
    "sell_price",
    "buy_symbol",
    "buy_strike",
    "buy_price",
    'max_loss',
    'max_profit',
    'initial_investment',
]


def find_risk_free_trade(data: pd.DataFrame):
    """
    looks for any call/put combinations that are priced such that all risk is covered by the premiums
    So far this function look for:
    * Bullish/Bearish Call/Put vertical Spreads
    * Protective Collars ('protective_collar')

    ************************* Vertical spreads **************************
    * bullish vertical call spread is risk free if the price to sell the
      call option with the higher strike is greater than or equal to the
      price of the lower call option price
      * Sell(X1) >= Buy(X0)
    * bearish vertical call spread is risk free if the price sell the lower
      call option is greater than the price to buy the higher call + the
      differences in the strike prices
      * Sell(X0) >= Buy(X1) + (X1- X0)
    * bullish vertical put spread is risk free if the price to sell the put
      option with the higher strike is greater than or equal to the price
      of the lower put option price + the difference in the strike prices
      * Sell(X1) >= Buy(X0) + (X1- X0)
    * bearish vertical put spread is risk free if the price to sell the
      lower put option is greater than the price to buy the higher put
      * Sell(X0) >= Buy(X1)

    Bullish Put/Call Spread     #  Bearish Put/Call Spread
                  Sell P.-0     #  Sell P.
                 X1---------    #  --------X0
                /  Sell C.      #  Sell C.-0 \\
    Buy P.     /                #             \\ Buy P.-0
    ---------X0                 #             X1---------
    Buy C.-0                    #                  Buy C.

    _-_-_-_-_-_-_-_-_-_-_-_- Protective Collar -_-_-_-_-_-_-_-_-_-_-_-_-_-

    $_s = Price of Stock and in the charts this represents a profit = 0 + prem
    $_c = Sell price of the call
    $_p = Buy price of the put             Sell C.
    X_p is put strike price            X_c---------
    X_c is call strike price          /
    $ is $_s at expiration           $_s - Purchase price of the asset
                        Buy P.     /
                        ---------X_p

    Because the slope is zero to the left of the put on a bullish play and 0 to the right
    of the put on a bearish play with the put always being the lower price, the max_loss will
    always be the value of the underlying position + put option value + call option value, all
    calculated at the put strike price. Max profit will be similar but at the call price

    Define:
    * Premium = $_c - $_p
    * Put_Value@X_p = 0
    * Put_Value@X_c = max(0, X_p - X_c)
    * Call_Value@X_p = min(0, X_c - X_p)
    * Call_value@X_c = 0
    * Stock_value@x = $_s - x
    Max Loss($=X_p) = Premium + Put_Value@X_p + Call_Value@X_p + Stock_value@X_p
    Max Profit($=X_c) = Premium + Put_Value@X_c + Call_Value@X_c + Stock_value@X_c
    Investment to open  = $_s + Premium
    _-_-_-_-_-_-_-_-_-_-_-_-_- Alternatives  -_-_-_-_-_-_-_-_-_-_-_-_-_-_
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #       #   X_p < $_s < X_c   #   $_s < X_p < X_c   #   X_p < X_c < $_s   #
    #   B   #            Sell C.  #            Sell C.  #            Sell C.  #
    #   U   #            X_c----  #            X_c----  #            X_c--$_s #
    #   L   #           /         #           /         #           /         #
    #   L   #          $_s        #          /          #          /          #
    #   I   # Buy P.  /           # Buy P.  /           # Buy P.  /           #
    #   S   # ------X_p           # $_s----X_p          # ------X_p           #
    #   H   # M_L =prem-($_s-X_p) # M_L =prem           # M_L =prem-(X_c-X_p) #
    #       # M_P =prem+(X_c-$_s) # M_P =prem+(X_c-X_p) # M_P =prem           #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #       #   X_c < $_s < X_p   #   $_s < X_c < X_p   #   X_c < X_p < $_s   #
    #   B   # Sell C.             # Sell C.             # Sell C.             #
    #   E   # -----X_c            # $_s--X_c            # -----X_c            #
    #   A   #        \\           #        \\           #        \\           #
    #   R   #         $_s         #         \\          #         \\          #
    #   I   #          \\ Buy P.  #          \\ Buy P.  #          \\ Buy P.  #
    #   S   #           X_p-----  #           X_p------ #           X_p---$_s #
    #   H   # M_L =prem-(X_c-$_s) # M_L =prem-(X_c-X_p) # M_L =prem           #
    #       # M_P =prem+($_s-X_p) # M_P =prem           # M_P =prem+(X_c-X_p) #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


    :param data: data must be a dataframe in the same format as the SQLite database with
    all records having the same query_ts, underlying_symbol, and expiration_date
    :return:
    """
    risk_free_list = (
        _find_risk_free_vertical_call(
            data.loc[
                data.option_type == "Call",
                ['tckr', "quote_ts", "expiration", "symbol", "stock_price", "strike", "bid", "ask"],
            ]
        )
        + _find_risk_free_vertical_put(
            data.loc[
                data.option_type == "Put",
                ['tckr', "quote_ts", "expiration", "symbol", "stock_price", "strike", "bid", "ask"],
            ]
        )
        + _find_risk_free_protective_collar(data)
    )
    if len(risk_free_list) > 0:
        return pd.concat(risk_free_list, axis=0, ignore_index=True)[risk_free_trade_columns]
    else:
        return None


def _find_risk_free_vertical_call(data: pd.DataFrame) -> List[pd.DataFrame]:
    """
    looks for any combinations of call options that are priced such that all risk is covered by the premiums

    Investopedia:
        ```
        __Bull call spread__: (premiums result in a net debit)
        * Max profit = the spread between the strike prices - net premium paid.
        * Max loss = net premium paid.
        * Breakeven point = Long Call's strike price + net premium paid.

        __Bear call spread__: (premiums result in a net credit)
        * Max profit = net premium received.
        * Max loss = the spread between the strike prices - net premium received.
        * Breakeven point = Short Call's strike price + net premium received.
        ```


    * bullish vertical call spread is risk free if the price to sell the call option with the higher strike is
      greater than or equal to the price of the lower call option price
      * Sell(X1) >= Buy(X0)
    * bearish vertical call spread is risk free if the price sell the lower call option is greater than the price
      to buy the higher call + the differences in the strike prices
      * Sell(X0) >= Buy(X1) + (X1- X0)

    sell prices will be based off of the bid price and buy prices will be based off the ask prices as I will have
    to take whatever is on the market and cross the spread

    Bullish Call Spread         #  Bearish Call Spread
                     Sell C.    #  Sell C.-0
                 X1---------    #  --------X0
                /               #            \\
               /                #             \\
    ---------X0                 #             X1---------
    Buy C.-0                    #               Buy C.

    :param data: data must be a dataframe in the same format as the SQLite database with all records having the same
    query_ts, underlying_symbol, and expiration_date
    :return:
    """
    risk_free_trades = []
    symbol = data["symbol"].values
    strike_prices = data["strike"].values
    # define X0 (lower strike) to be the first index and X1 (higher strike) to be the second:
    #   * -1 indicates 'rest of qty' in the array.reshape() function below
    x0_shape = (-1, 1)
    x1_shape = (1, -1)
    x0 = strike_prices.reshape(x0_shape)
    x1 = strike_prices.reshape(x1_shape)
    valid_solution_space = x1 > x0
    # bullish case - sell X1 (high strike) and buy X0 (low strike)
    sell_price = data["bid"].values.reshape(x1_shape)
    buy_price = data["ask"].values.reshape(x0_shape)
    premium = sell_price - buy_price
    # look for (SellPrice_X1  - BuyPrice_X0) >= 0
    is_risk_free = np.logical_and(valid_solution_space, premium >= 0)

    if np.any(is_risk_free):
        # i_x* variables are the index values to select the risk free data
        i_x0, i_x1 = np.indices(is_risk_free.shape)
        i_x0 = i_x0[is_risk_free]
        i_x1 = i_x1[is_risk_free]
        risk_free_df = pd.DataFrame(columns=risk_free_trade_columns)
        risk_free_df["sell_symbol"] = symbol.flat[i_x1]
        risk_free_df["buy_symbol"] = symbol.flat[i_x0]
        risk_free_df["sell_strike"] = x1.flat[i_x1]
        risk_free_df["buy_strike"] = x0.flat[i_x0]
        risk_free_df["sell_price"] = sell_price.flat[i_x1]
        risk_free_df["buy_price"] = buy_price.flat[i_x0]

        risk_free_df["option_type"] = "bullish_vertical_call_spread"
        risk_free_df["stock_price"] = data["stock_price"].values[0]
        risk_free_df['tckr'] = data["tckr"].values[0]
        risk_free_df['quote_ts'] = data["quote_ts"].values[0]
        risk_free_df['expiration'] = data["expiration"].values[0]

        # define several temp variables to help with the max_loss/profit equations
        x_delta = np.abs(risk_free_df["buy_strike"] - risk_free_df["sell_strike"])
        temp_premium = risk_free_df["sell_price"] - risk_free_df["buy_price"]

        risk_free_df['max_loss'] = -1* temp_premium
        risk_free_df['max_profit'] = temp_premium + x_delta
        risk_free_df['initial_investment'] = -1 * temp_premium
        assert np.all(pd.notna(risk_free_df))
        risk_free_trades.append(risk_free_df)

    # bearish conditions - sell X0 (lower strike) and buy X1 so transpose pricing
    sell_price = sell_price.T
    buy_price = buy_price.T
    premium = premium.T
    # look for (SellPrice_X0  - BuyPrice_X1) >= (X1 - X0)
    is_risk_free = np.logical_and(valid_solution_space, premium >= (x1 - x0))
    if np.any(is_risk_free):
        # i_x* variables are the index values to select the risk free data
        i_x0, i_x1 = np.indices(is_risk_free.shape)
        i_x0 = i_x0[is_risk_free]
        i_x1 = i_x1[is_risk_free]
        risk_free_df = pd.DataFrame(columns=risk_free_trade_columns)
        risk_free_df["sell_symbol"] = symbol.flat[i_x0]
        risk_free_df["buy_symbol"] = symbol.flat[i_x1]
        risk_free_df["sell_strike"] = x0.flat[i_x0]
        risk_free_df["buy_strike"] = x1.flat[i_x1]
        risk_free_df["sell_price"] = sell_price.flat[i_x0]
        risk_free_df["buy_price"] = buy_price.flat[i_x1]

        risk_free_df["option_type"] = "bearish_vertical_call_spread"
        risk_free_df["stock_price"] = data["stock_price"].values[0]
        risk_free_df['tckr'] = data["tckr"].values[0]
        risk_free_df['quote_ts'] = data["quote_ts"].values[0]
        risk_free_df['expiration'] = data["expiration"].values[0]


        # define several temp variables to help with the max_loss/profit equations
        x_delta = np.abs(risk_free_df["buy_strike"] - risk_free_df["sell_strike"])
        temp_premium = risk_free_df["sell_price"] - risk_free_df["buy_price"]

        risk_free_df['max_loss'] = -1*(temp_premium - x_delta)
        risk_free_df['max_profit'] = temp_premium
        risk_free_df['initial_investment'] = -1 * temp_premium
        assert np.all(pd.notna(risk_free_df))
        risk_free_trades.append(risk_free_df)

    return risk_free_trades


def _find_risk_free_vertical_put(data: pd.DataFrame) -> List[pd.DataFrame]:
    """
    looks for any combinations of put options that are priced such that all risk is covered by the premiums

    Investopedia:
        ```
        __Bull put spread__: (premiums result in a net credit)
        * Max profit = net premium received.
        * Max loss = the spread between the strike prices - net premium received.
        * Breakeven point = Short Put's strike price - net premium received.

        __Bear put spread__: (premiums result in a net debit)
        * Max profit = the spread between the strike prices - net premium paid.
        * Max loss = net premium paid.
        * Breakeven point = Long Put's strike price - net premium paid.
        ```

    bullish vertical put spread is risk free if the price to sell the put option with the higher strike is
    greater than or equal to the price of the lower put option price + the difference in the strike prices
    * Sell(X1) >= Buy(X0) + (X1- X0)
    bearish vertical put spread is risk free if the price to sell the lower put option is greater than the price
    to buy the higher put
    * Sell(X0) >= Buy(X1)

    sell prices will be based off of the bid price and buy prices will be based off the ask prices as I will have
    to take whatever is on the market and cross the spread

    Bullish Put Spread          #  Bearish Put Spread
                  Sell P.-0     #  Sell P.
                 X1---------    #  --------X0
                /               #            \\
    Buy P.     /                #             \\ Buy P.-0
    ---------X0                 #             X1---------
                                #

    :param data: data must be a dataframe in the same format as the SQLite database with all records having the same
    query_ts, underlying_symbol, and expiration_date
    :return:
    """
    risk_free_trades = []
    symbol = data["symbol"].values
    strike_prices = data["strike"].values
    # define X0 (lower strike) to be the first index and X1 (higher strike) to be the second:
    #   * -1 indicates 'rest of qty' in the array.reshape() function below
    x0_shape = (-1, 1)
    x1_shape = (1, -1)
    x0 = strike_prices.reshape(x0_shape)
    x1 = strike_prices.reshape(x1_shape)
    valid_solution_space = x1 > x0
    # bullish case - sell X1 (high strike) and buy X0 (low strike)
    sell_price = data["bid"].values.reshape(x1_shape)
    buy_price = data["ask"].values.reshape(x0_shape)
    premium = sell_price - buy_price
    # look for (SellPrice_X1  - BuyPrice_X0) >= (X1- X0)
    is_risk_free = np.logical_and(valid_solution_space, premium >= (x1 - x0))

    if np.any(is_risk_free):
        # i_x* variables are the index values to select the risk free data
        i_x0, i_x1 = np.indices(is_risk_free.shape)
        i_x0 = i_x0[is_risk_free]
        i_x1 = i_x1[is_risk_free]
        risk_free_df = pd.DataFrame(columns=risk_free_trade_columns)
        risk_free_df["sell_symbol"] = symbol.flat[i_x1]
        risk_free_df["buy_symbol"] = symbol.flat[i_x0]
        risk_free_df["sell_strike"] = x1.flat[i_x1]
        risk_free_df["buy_strike"] = x0.flat[i_x0]
        risk_free_df["sell_price"] = sell_price.flat[i_x1]
        risk_free_df["buy_price"] = buy_price.flat[i_x0]

        risk_free_df["option_type"] = "bullish_vertical_put_spread"
        risk_free_df["stock_price"] = data["stock_price"].values[0]
        risk_free_df['tckr'] = data["tckr"].values[0]
        risk_free_df['quote_ts'] = data["quote_ts"].values[0]
        risk_free_df['expiration'] = data["expiration"].values[0]

        # define several temp variables to help with the max_loss/profit equations
        x_delta = np.abs(risk_free_df["buy_strike"] - risk_free_df["sell_strike"])
        temp_premium = risk_free_df["sell_price"] - risk_free_df["buy_price"]

        risk_free_df['max_loss'] = -1 * (temp_premium - x_delta)
        risk_free_df['max_profit'] = temp_premium
        risk_free_df['initial_investment'] = -1 * temp_premium
        assert np.all(pd.notna(risk_free_df))
        risk_free_trades.append(risk_free_df)

    # bearish conditions - sell X0 (lower strike) and buy X1 so transpose pricing
    sell_price = sell_price.T
    buy_price = buy_price.T
    # look for (SellPrice_X0  - BuyPrice_X1) >= 0
    is_risk_free = np.logical_and(valid_solution_space, sell_price >= buy_price)
    if np.any(is_risk_free):
        # i_x* variables are the index values to select the risk free data
        i_x0, i_x1 = np.indices(is_risk_free.shape)
        i_x0 = i_x0[is_risk_free]
        i_x1 = i_x1[is_risk_free]

        risk_free_df = pd.DataFrame(columns=risk_free_trade_columns)
        risk_free_df["sell_symbol"] = symbol.flat[i_x0]
        risk_free_df["buy_symbol"] = symbol.flat[i_x1]
        risk_free_df["sell_strike"] = x0.flat[i_x0]
        risk_free_df["buy_strike"] = x1.flat[i_x1]
        risk_free_df["sell_price"] = sell_price.flat[i_x0]
        risk_free_df["buy_price"] = buy_price.flat[i_x1]

        risk_free_df["option_type"] = "bearish_vertical_put_spread"
        risk_free_df["stock_price"] = data["stock_price"].values[0]
        risk_free_df['tckr'] = data["tckr"].values[0]
        risk_free_df['quote_ts'] = data["quote_ts"].values[0]
        risk_free_df['expiration'] = data["expiration"].values[0]

        # define several temp variables to help with the max_loss/profit equations
        x_delta = np.abs(risk_free_df["buy_strike"] - risk_free_df["sell_strike"])
        temp_premium = risk_free_df["sell_price"] - risk_free_df["buy_price"]

        risk_free_df['max_loss'] = -1 * temp_premium
        risk_free_df['max_profit'] = temp_premium + x_delta
        risk_free_df['initial_investment'] = -1 * temp_premium
        assert np.all(pd.notna(risk_free_df))
        risk_free_trades.append(risk_free_df)

    return risk_free_trades


def _find_risk_free_protective_collar(data: pd.DataFrame) -> List[pd.DataFrame]:
    """
    looks for any combinations of call/put options and long positions in a stock
    such that all risk is covered by the premiums

    _-_-_-_-_-_-_-_-_-_-_-_- Protective Collar -_-_-_-_-_-_-_-_-_-_-_-_-_-

    $_s = Price of Stock and in the charts this represents a profit = 0 + prem
    $_c = Sell price of the call
    $_p = Buy price of the put             Sell C.
    X_p is put strike price            X_c---------
    X_c is call strike price          /
    $ is $_s at expiration           $_s - Purchase price of the asset
                        Buy P.     /
                        ---------X_p

    Because the slope is zero to the left of the put on a bullish play and 0 to the right
    of the put on a bearish play with the put always being the lower price, the max_loss will
    always be the value of the underlying position + put option value + call option value, all
    calculated at the put strike price. Max profit will be similar but at the call price

    Define:
    * Premium = $_c - $_p
    * Put_Value@X_p = 0
    * Put_Value@X_c = max(0, X_p - X_c)
    * Call_Value@X_p = min(0, X_c - X_p)
    * Call_value@X_c = 0
    * Stock_value@x = $_s - x
    Max Loss($=X_p) = Premium + Put_Value@X_p + Call_Value@X_p + Stock_value@X_p
    Max Profit($=X_c) = Premium + Put_Value@X_c + Call_Value@X_c + Stock_value@X_c
    Investment to open  = $_s + Premium
    _-_-_-_-_-_-_-_-_-_-_-_-_- Alternatives  -_-_-_-_-_-_-_-_-_-_-_-_-_-_
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #       #   X_p < $_s < X_c   #   $_s < X_p < X_c   #   X_p < X_c < $_s   #
    #   B   #            Sell C.  #            Sell C.  #            Sell C.  #
    #   U   #            X_c----  #            X_c----  #            X_c--$_s #
    #   L   #           /         #           /         #           /         #
    #   L   #          $_s        #          /          #          /          #
    #   I   # Buy P.  /           # Buy P.  /           # Buy P.  /           #
    #   S   # ------X_p           # $_s----X_p          # ------X_p           #
    #   H   # M_L =prem-($_s-X_p) # M_L =prem           # M_L =prem-(X_c-X_p) #
    #       # M_P =prem+(X_c-$_s) # M_P =prem+(X_c-X_p) # M_P =prem           #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #       #   X_c < $_s < X_p   #   $_s < X_c < X_p   #   X_c < X_p < $_s   #
    #   B   # Sell C.             # Sell C.             # Sell C.             #
    #   E   # -----X_c            # $_s--X_c            # -----X_c            #
    #   A   #        \\           #        \\           #        \\           #
    #   R   #         $_s         #         \\          #         \\          #
    #   I   #          \\ Buy P.  #          \\ Buy P.  #          \\ Buy P.  #
    #   S   #           X_p-----  #           X_p------ #           X_p---$_s #
    #   H   # M_L =prem-(X_c-$_s) # M_L =prem-(X_c-X_p) # M_L =prem           #
    #       # M_P =prem+($_s-X_p) # M_P =prem           # M_P =prem+(X_c-X_p) #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    sell prices will be based off of the bid price and buy prices will be based off the ask prices as I will have
    to take whatever is on the market and cross the spread

    :param data: data must be a dataframe in the same format as the SQLite database with all records having the same
    query_ts, underlying_symbol, and expiration_date. Both call and put data must be present
    :return:
    """
    risk_free_trades = []
    is_call_option = data.option_type == 'Call'
    current_price = data.stock_price.values[0]
    # define Xp (put strike) to be the first index and Xc (call strike) to be the second:
    #   * -1 indicates 'rest of qty' in the array.reshape() function below
    xp_shape = (-1, 1)
    xc_shape = (1, -1)
    xp = data.loc[~is_call_option, 'strike'].values.reshape(xp_shape)
    xc = data.loc[is_call_option, 'strike'].values.reshape(xc_shape)
    sell_price = data.loc[is_call_option, "bid"].values.reshape(xc_shape)
    buy_price = data.loc[~is_call_option, "ask"].values.reshape(xp_shape)

    # if the price of the stock = xp at expiration...
    # premium = sell_price - buy_price
    # value_put = 0
    # value_call = np.minimum(xc - xp, 0)
    # value_stock = xp - current_price
    # max_loss = premium + value_put + value_call + value_stock
    max_loss = (sell_price - buy_price) + np.minimum(xc - xp, 0) + xp - current_price
    is_risk_free = max_loss >= 0

    if np.any(is_risk_free):
        c_symbols = data.loc[is_call_option, "symbol"].values
        p_symbols = data.loc[~is_call_option, "symbol"].values
        # i_x* variables are the index values to select the risk free data
        i_xp, i_xc = np.indices(is_risk_free.shape)
        i_xp = i_xp[is_risk_free]
        i_xc = i_xc[is_risk_free]

        risk_free_df = pd.DataFrame(columns=risk_free_trade_columns)
        risk_free_df["sell_symbol"] = c_symbols.flat[i_xc]
        risk_free_df["buy_symbol"] = p_symbols.flat[i_xp]
        risk_free_df["sell_strike"] = xc.flat[i_xc]
        risk_free_df["buy_strike"] = xp.flat[i_xp]
        risk_free_df["sell_price"] = sell_price.flat[i_xc]
        risk_free_df["buy_price"] = buy_price.flat[i_xp]

        risk_free_df["option_type"] = "protective_collar"
        risk_free_df["stock_price"] = current_price
        risk_free_df['tckr'] = data["tckr"].values[0]
        risk_free_df['quote_ts'] = data["quote_ts"].values[0]
        risk_free_df['expiration'] = data["expiration"].values[0]

        risk_free_df['max_loss'] = max_loss[is_risk_free]
        # max profit is the max_loss + the strike_delta
        risk_free_df['max_profit'] = risk_free_df.max_loss + np.abs(risk_free_df.buy_strike - risk_free_df.sell_strike)
        # initial investment is the premium + cost of stock
        risk_free_df['initial_investment'] = current_price - (risk_free_df.sell_price - risk_free_df.buy_price)
        assert np.all(pd.notna(risk_free_df))
        risk_free_trades.append(risk_free_df)

    return risk_free_trades
