import datetime
import numpy as np
import pandas as pd
from typing import Union, Sequence, TypeVar, List

# types that are 'date-like'
RawDateSingle = Union[
    str, pd.Timestamp, np.datetime64, datetime.date, datetime.datetime
]
RawDateCollection = Sequence[RawDateSingle]
RawDateType = Union[RawDateSingle, RawDateCollection]
# types that are 'timedelta-like'
RawTimeDeltaSingle = Union[pd.Timedelta, np.timedelta64, str, int, float]
RawTimeDeltaCollection = Sequence[RawTimeDeltaSingle]
RawTimeDeltaType = Union[RawTimeDeltaSingle, RawTimeDeltaCollection]
# "clean" date types are supposed to be pd.Timestamp and pd.DatetimeIndex
PdDate = Union[pd.Timestamp, pd.DatetimeIndex]

Scalar = Union[str, float, int, pd.Timestamp, np.datetime64, bool]
ArrayLike = Union[np.ndarray, pd.Series]
T = TypeVar("T")
# flexible data types that can be a simple type or a construction based on the type
TorList = Union[T, List[T]]
TorArray = Union[T, ArrayLike]
