#!/usr/bin/env python
import pandas as pd
import numpy as np
import datetime
from collections import Counter
from typing import Any, List, Union, Set, Tuple, Dict


def is_same(a: Any, b: Any) -> bool:
    return id(a) == id(b)


def assert_same(a: Any, b: Any) -> bool:
    assert id(a) == id(b)
    return True


def is_iterable_equal(
    a: Union[List, Set, Tuple], b: Union[List, Set, Tuple], order_matters: bool = True
) -> bool:
    """
    returns true if the values are all equal based on the criterion of is_equal.
    Order is considered based on the value of the argument 'order_matters' and defaults to True

    :param a: Union[List, Set]
    :param b: Union[List, Set]
    :param order_matters: bool
    :return: bool
    """
    if len(a) != len(b):
        return False
    if order_matters:
        if isinstance(a, set) or isinstance(b, set):
            return is_iterable_equal(list(a), list(b), order_matters=False)
        else:
            for i in range(len(a)):
                if is_equal(a[i], b[i], order_matters=order_matters) is False:
                    return False
            return True
    else:
        # logic/code adapted from https://stackoverflow.com/questions/7828867/ ...
        #       how-to-efficiently-compare-two-unordered-lists-not-sets-in-python
        try:
            # if all objects are hashable, then this is O(n)
            return Counter(a) == Counter(b)
        except TypeError:
            try:
                # the sorted built-in method is less strict than Counter and has O(n*log(n))
                return is_iterable_equal(sorted(a), sorted(b), order_matters=True)
            except (ValueError, TypeError):
                # try to find each record of a in b - much less efficient but complete
                b_copy = list(b)  # make a mutable copy
                try:
                    for elem in a:
                        b_copy.remove(
                            next(
                                x
                                for x in b_copy
                                if is_equal(elem, x, order_matters=order_matters)
                            )
                        )
                except StopIteration:
                    return False
                return not b_copy


def assert_iterable_equal(
    a: Union[List, Set, Tuple], b: Union[List, Set, Tuple], order_matters: bool = True
) -> bool:
    """
    returns true if the values are all equal based on the criterion of is_equal otherwise throws an error.
    Order is considered based on the value of the argument 'order_matters' and defaults to True

    :param a: Union[List, Set]
    :param b: Union[List, Set]
    :param order_matters: bool
    :return: bool
    """
    assert len(a) == len(b)
    if order_matters:
        if isinstance(a, set) or isinstance(b, set):
            return assert_iterable_equal(list(a), list(b), order_matters=False)
        else:
            for i in range(len(a)):
                assert_equal(a[i], b[i], order_matters=order_matters)
            return True
    else:
        # logic/code adapted from https://stackoverflow.com/questions/7828867/ ...
        #       how-to-efficiently-compare-two-unordered-lists-not-sets-in-python
        try:
            # if all objects are hashable, then this is O(n) to convert to Counter and compare
            a_cntr = Counter(a)
            b_cntr = Counter(b)
        except TypeError:
            try:
                # the sorted built-in method is less strict than Counter and has O(n*log(n))
                a_sort = sorted(a)
                b_sort = sorted(b)
            except (ValueError, TypeError):
                # try to find each record of a in b - much less efficient but complete
                b_copy = list(b)  # make a mutable copy
                missing_elem = None
                try:
                    for elem in a:
                        missing_elem = elem
                        b_copy.remove(
                            next(
                                x
                                for x in b_copy
                                if is_equal(elem, x, order_matters=order_matters)
                            )
                        )
                except StopIteration:
                    assert missing_elem in b_copy
                    return True
                else:
                    assert len(b_copy) == 0
                    return True
            else:
                return assert_iterable_equal(a_sort, b_sort, order_matters=True)
        else:
            assert a_cntr == b_cntr
            return True


def is_equal(
    a: Any, b: Any, order_matters: bool = False, index_matters: bool = False
) -> bool:
    if a is None:
        return b is None
    elif type(a) == float:
        return a == b or (np.isnan(a) and type(b) == float and np.isnan(b))
    elif isinstance(a, datetime.date):
        return a == b or (pd.isna(a) and isinstance(b, datetime.date) and pd.isna(b))
    elif type(a) in [int, complex, str, np.datetime64]:
        return a == b
    else:
        if type(a) != type(b):
            return False
        elif isinstance(a, list) or isinstance(a, set) or isinstance(a, tuple):
            return is_iterable_equal(a, b, order_matters=order_matters)
        elif isinstance(a, dict):
            a_k = [x for x in a.keys()]
            b_k = [x for x in b.keys()]
            if len(a_k) != len(b_k):
                return False
            all_k = list(set(a_k + b_k))
            for k in all_k:
                if is_equal(a[k], b[k]) is False:
                    return False
            return True
        elif isinstance(a, np.ndarray) or isinstance(a, pd.Index):
            return a.shape == b.shape and np.all(
                np.logical_or(a == b, np.logical_and(pd.isna(a), pd.isna(b)))
            )
        elif isinstance(a, pd.Series):
            return a.name == b.name and a.equals(b)
        elif isinstance(a, pd.DataFrame):
            if a.shape != b.shape:
                return False
            if order_matters:
                return a.equals(b)
            else:
                return (
                    Counter(a.columns) == Counter(b.columns)
                    and Counter(a.index) == Counter(b.index)
                    and a.equals(b.loc[a.index, a.columns])
                )
        else:
            return a == b


def assert_equal(a: Any, b: Any, order_matters: bool = False) -> bool:
    if a is None:
        assert b is None
    elif type(a) == float:
        assert a == b or (np.isnan(a) and type(b) == float and np.isnan(b))
    elif isinstance(a, datetime.date):
        assert a == b or (pd.isna(a) and isinstance(b, datetime.date) and pd.isna(b))
    elif type(a) in [int, complex, str, np.datetime64]:
        assert a == b
    else:
        assert type(a) == type(b)
        if isinstance(a, list) or isinstance(a, set) or isinstance(a, tuple):
            assert_iterable_equal(a, b, order_matters=order_matters)
        elif isinstance(a, dict):
            a_k = [x for x in a.keys()]
            b_k = [x for x in b.keys()]
            assert len(a_k) == len(b_k)
            all_k = list(set(a_k + b_k))
            for k in all_k:
                assert_equal(a[k], b[k])
        elif isinstance(a, np.ndarray) or isinstance(a, pd.Index):
            assert a.shape == b.shape and np.all(
                np.logical_or(a == b, np.logical_and(pd.isna(a), pd.isna(b)))
            )
        elif isinstance(a, pd.Series):
            assert a.name == b.name and a.equals(b)
        elif isinstance(a, pd.DataFrame):
            if order_matters:
                assert a.equals(b)
            else:
                assert Counter(a.columns) == Counter(b.columns)
                assert Counter(a.index) == Counter(b.index)
                assert a.equals(b.loc[a.index, a.columns])
        else:
            assert a == b
    return True


def is_subset(
    a: Union[List, Set, Dict, Tuple, pd.Series, pd.DataFrame, pd.Index, np.ndarray],
    b: Union[List, Set, Dict, Tuple, pd.Series, pd.DataFrame, pd.Index, np.ndarray],
) -> bool:
    """
    test if each element of A is represented in B.
    * If 'a' is a set, then it uses the built in set.issubset() function is used.
    * If 'a' is a dataframe or series then use the internal isin() function to determine if the
        argument 'a' is a subset of 'b'
    * If 'a' is a ndarray then flatten the array and use the counter logic
    * otherwise pass arguments directly to the Counter constructor

    :param a: 
    :param b:
    :return:
    """
    if isinstance(a, set):
        return a.issubset(b)
    elif isinstance(a, pd.Series) or isinstance(a, pd.DataFrame):
        return a.isin(b)
    # after this point the if conditions will just set up the a_c, b_c counter objects for the final return
    elif isinstance(a, np.ndarray):
        a_c = Counter(a.flatten())
        b_c = Counter(
            b.flatten() if isinstance(b, np.ndarray) else np.array(b).flatten()
        )
    else:
        a_c = Counter(a)
        b_c = Counter(b)
    return np.all([a_c[k] <= b_c[k] for k in a_c.keys()])


def assert_subset(
    a: Union[List, Set, Dict, Tuple, pd.Series, pd.DataFrame, pd.Index, np.ndarray],
    b: Union[List, Set, Dict, Tuple, pd.Series, pd.DataFrame, pd.Index, np.ndarray],
) -> bool:
    """
    test if each element of A is represented in B and throw an assertion error if not
    * If 'a' is a set, then it uses the built in set.issubset() function is used.
    * If 'a' is a dataframe or series then use the internal isin() function to determine if the
        argument 'a' is a subset of 'b'
    * If 'a' is a ndarray then flatten the array and use the counter logic
    * otherwise pass arguments directly to the Counter constructor

    :param a:
    :param b:
    :return:
    """
    if isinstance(a, set):
        assert a.issubset(b)
        return True
    elif isinstance(a, pd.Series) or isinstance(a, pd.DataFrame):
        assert a.isin(b)
        return True
    # after this point the if conditions will just set up the a_c, b_c counter objects for the final return
    elif isinstance(a, np.ndarray):
        a_c = Counter(a.flatten())
        b_c = Counter(
            b.flatten() if isinstance(b, np.ndarray) else np.array(b).flatten()
        )
    else:
        a_c = Counter(a)
        b_c = Counter(b)
    for k in a_c.keys():
        assert a_c[k] <= b_c[k]
    return True
