import datetime


def month2int(month: str) -> int:
    return int(
        [
            "jan",
            "feb",
            "mar",
            "apr",
            "may",
            "jun",
            "jul",
            "aug",
            "sep",
            "oct",
            "nov",
            "dec",
        ].index(month.strip()[:3].lower())
        + 1
    )


def int2month(month: int, capitalization: str = "FIRSTLETTER") -> str:
    cap_options = {"FIRSTLETTER": str.title, "UPPER": str.upper, "LOWER": str.lower}
    month_names = [
        "jan",
        "feb",
        "mar",
        "apr",
        "may",
        "jun",
        "jul",
        "aug",
        "sep",
        "oct",
        "nov",
        "dec",
    ]
    return cap_options[capitalization.upper()](month_names[month - 1])


def interp_span(spantext: str, unit: str = "d") -> float:
    """
    converts the typical spans that I may use throughout the program
    into float(qty of time) of the time unit indicated spantext can be in
    the format 'XS' where X is a number and S can be yr,m,wks,d unit is
    the unit of time and can be ['yr','m','wk','d']
    """
    year_variations = ["years", "year", "yrs", "yr", "ys", "y"]
    month_variations = ["months", "month", "mths", "mth", "ms", "m"]
    week_variations = ["weeks", "week", "wks", "wk", "ws", "w"]
    day_variations = ["days", "day", "ds", "d"]
    days_estimate = {"y": 365.0, "m": 30.0, "w": 7.0, "d": 1.0}
    conversions = {"y_m": 12, "m_y": 1.0 / 12}
    if unit in year_variations:
        unit = "y"
    elif unit in month_variations:
        unit = "m"
    elif unit in week_variations:
        unit = "w"
    elif unit in day_variations:
        unit = "d"
    else:
        raise ValueError("Unit is not recognizable")
    input_unit = ""
    for indicator in ["y", "m", "w", "d"]:
        if indicator in spantext:
            input_unit = indicator
            break
    qty = float(spantext.split(input_unit)[0].replace(",", ""))
    if "%s_%s" % (input_unit, unit) in conversions.keys():
        return qty * conversions["%s_%s" % (input_unit, unit)]
    else:
        return qty * (days_estimate[input_unit] / days_estimate[unit])


def est_businessdays_span(spantext: str) -> float:
    return interp_span(spantext, unit="d") * 0.7142857142857143  # 5.0/7.0


def middle_friday_of_month(year, month):
    """returns the Friday following the 15th day of the given year and month"""
    middle_friday = datetime.datetime(year, month, 15)
    offset_days = 5 - (middle_friday.isoweekday() % 7)
    if offset_days < 0:
        # if saturday (i.e. offset_days = -1, then go to the following friday by adding 7
        offset_days += 7
    middle_friday = middle_friday + datetime.timedelta(days=offset_days)
    return middle_friday


def last_friday_of_month(year, month):
    """returns the last Friday of the given year and month"""
    if month == 12:
        last_friday = datetime.datetime(year, month, 31)
    else:
        last_friday = datetime.datetime(year, month + 1, 1) - datetime.timedelta(days=1)
    while last_friday.isoweekday() % 7 != 5:
        last_friday = last_friday - datetime.timedelta(days=1)
    return last_friday


def last_businessday_of_month(year, month):
    """returns the last weekday of the given year and month"""
    if month == 12:
        last_day = datetime.datetime(year, month, 31)
    else:
        last_day = datetime.datetime(year, month + 1, 1) - datetime.timedelta(days=1)
    while last_day.isoweekday() > 5:
        last_day = last_day - datetime.timedelta(days=1)
    return last_day
