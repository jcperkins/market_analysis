import pandas as pd
from typing import Tuple
from .NavigateCalendar import interp_span


def calculateMaxPositionQtySPX(
    purchase_span: str, roll_span: str
) -> Tuple[int, pd.Timedelta]:
    """ input: the purchase span text and the roll span text
        output:
            o  max qty of uniformly spaced positions that can fit in the span
            o  datetime.timedelta value required for uniformly spaced positions
    """
    days_to_exdate = interp_span(purchase_span, unit="d")
    days_to_sell_date = interp_span(roll_span, unit="d")
    if days_to_exdate > 731:
        # default to Jan 15th purchase of furthest option date
        # days_to_exdate = 1095  # defaults to 3 years
        days_between_positions = 365
    elif days_to_exdate > 365:
        # quarterly options stop at 1yr and yearly options have options
        #  every 6m
        days_between_positions = 180
    elif days_to_exdate > 180:
        # Monthly options stop at 6m and quarterly options have options
        #  every quarter
        days_between_positions = 90
    elif days_to_exdate > 56:
        # weekly options stop at 8wks and monthly options offer every month
        days_between_positions = 30
    else:
        # weekly options
        days_between_positions = 7
    # cyclesBeforeExDate = int(days_to_exdate / max(days_to_sell_date, days_between_positions))
    cycles_before_sell = int(days_to_sell_date / float(days_between_positions))
    return cycles_before_sell, pd.Timedelta(days=days_between_positions)
