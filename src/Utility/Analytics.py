from typing import Optional, Tuple

import numpy as np
import pandas as pd
from scipy import stats
from src.types import TorArray, ArrayLike


def r2(data: TorArray[float], fit: TorArray[float]) -> float:
    # R2 = 1-SSres/SStot
    # SSres = Sum((data-fit)**2)
    # SStot = Sum((data-data.mean())**2)
    return 1 - ((data - fit) ** 2).sum() / ((data - data.mean()) ** 2).sum()


def gcd(a: float, b: float) -> float:
    """Return greatest common divisor using Euclid's Algorithm."""
    while b:
        a, b = b, a % b
    return a


def integrate_line_times_normal_dist(
        x0: float,
        x1: float,
        y0: float,
        y1: float,
        mean: float,
        std: float,
        slope: Optional[float] = None,
        cdf_x0: Optional[float] = None,
        cdf_x1: Optional[float] = None,
) -> float:
    """
    integral of x * e^(-(x-u)^2) is difficult, but integral of x * e(-x^2)
        is more simple (u is the mean and rho is the std dev)
    so make the transform x' = x - u; x = x' + u;  dx=dx'
    P($) = (1/sqrt(2*pi*rho)*e^(-($-u)^2/(2*rho)))
    P($') = (1/sqrt(2*pi*rho)*e^(-($')^2/(2*rho)))

    integral(P(x)*dx from a to b) = integral(P(x')*dx' from a-u to b-u)
        =  CDF(b) - CDF(a)

    integral(x'*P(x')*dx' from a to b)
        = -(r*e^(-(x')^2/(2*r^2)))/sqrt(2*pi)  a to b
        = (-r/sqrt(2*pi)) * (e^(-b^2/(2*r^2)) - e^(-a^2/(2*r^2)))
        = (r/sqrt(2*pi)) * (e^(-a^2/(2*r^2)) - e^(-b^2/(2*r^2)))

    integral(x*P(x)*dx from a to b)
        = integral((x'+u)*P(x')*dx' from a-u to b-u)
        = integral(x'*P(x')*dx') + integral(u*P(x')*dx') from a-u to b-u)
        = integral(x'*P(x')*dx'  from a-u to b-u)
            + u*(integral(P(x) from a to b))
        = integral(x'*P(x')*dx' from a-u to b-u) + u*(CDF(b) - CDF(a))
        = (r/sqrt(2*pi)) * (e^(-(a-u)^2/(2*r^2)) - e^(-(b-u)^2/(2*r^2)))
            + u*(CDF(b) - CDF(a))

    example case of bull put vertical spread below
    profit($) = m * ($ - X1) + max profit
    profit($') = m * ($' + u - X1) + max profit

    integral(profit($) * dist.pdf * dx from X0 to X1)
        = integral(profit($) * P($) from X0 to X1)
        = integral((m * ($ - X1) + max profit) * P($) from X0 to X1)
        = integral((m*$*P($) - m*X1*P($) + max profit*P($))*d$ from X0 to X1)
        = m*integral($ *P($)) - m*X1*integral(P($))
            + max profit*integral(P($)) from X0 to X1
        = integral($' * P($') * d$' from X0-u to X1-u)
            + u * (CDF(b) - CDF(a))
            - m * X1 * (CDF(b) - CDF(a))
            + max profit * (CDF(b) - CDF(a))
        = (r/sqrt(2*pi)) * (e^(-a^2/(2*r^2)) - e^(-b^2/(2*r^2)))
            + (u - m*X1 + max profit) * (CDF(b) - CDF(a))
    """
    if x0 == x1:
        return 0
    elif x0 > x1:
        x0, x1 = x1, x0
        y0, y1 = y1, y0
    if slope is None:
        slope = (y1 - y0) / (x1 - x0)
    if cdf_x0 is None or cdf_x1 is None:
        dist = stats.norm(loc=mean, scale=std)
        cdf_x0 = dist.cdf(x0)
        cdf_x1 = dist.cdf(x1)
    if y0 == y1:
        return y0 * (cdf_x1 - cdf_x0)
    exp_value = slope * std / np.sqrt(2 * np.pi) * (
            np.exp(-((x0 - mean) ** 2) / (2 * (std ** 2)))
            - np.exp(-((x1 - mean) ** 2) / (2 * (std ** 2)))
    ) + (slope * mean - slope * x1 + y1) * (cdf_x1 - cdf_x0)
    return exp_value


# rolling 2D window for array
def rolling_window_2d(a: ArrayLike, shape: Tuple[int, int]) -> ArrayLike:
    s = (a.shape[0] - shape[0] + 1,) + (a.shape[1] - shape[1] + 1,) + shape
    strides = a.strides + a.strides
    return np.lib.stride_tricks.as_strided(a, shape=s, strides=strides)


# rolling 1D window for array
def rolling_window_1d(
        a: ArrayLike, window: int, axis: int = -1, loc: str = "right"
) -> ArrayLike:
    axis = axis if axis != -1 else len(a.shape) - 1
    shape = (
            a.shape[:axis] + (a.shape[axis] - window + 1,) + a.shape[axis + 1:] + (window,)
    )
    strides = a.strides + (a.strides[-1],)
    if loc is None:
        return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    elif loc.lower() == "right":
        buffer_shape = a.shape[:axis] + (window - 1,) + a.shape[axis + 1:] + (window,)
        nan_buffer = np.empty(buffer_shape)
        nan_buffer.fill(np.nan)
        return np.concatenate(
            (
                nan_buffer,
                np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides),
            ),
            axis=axis,
        )
    elif loc.lower() == "left":
        buffer_shape = a.shape[:axis] + (window - 1,) + a.shape[axis + 1:] + (window,)
        nan_buffer = np.empty(buffer_shape)
        nan_buffer.fill(np.nan)
        return np.concatenate(
            (
                np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides),
                nan_buffer,
            ),
            axis=axis,
        )
    elif loc.lower() == "center":
        left_buffer_len = int((window - 1) / 2)
        right_buffer_len = window - 1 - left_buffer_len
        nan_buffer_left = np.empty(
            a.shape[:axis] + (left_buffer_len,) + a.shape[axis + 1:] + (window,)
        )
        nan_buffer_left.fill(np.nan)
        nan_buffer_right = np.empty(
            a.shape[:axis] + (right_buffer_len,) + a.shape[axis + 1:] + (window,)
        )
        nan_buffer_right.fill(np.nan)
        return np.concatenate(
            (
                nan_buffer_right,
                np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides),
                nan_buffer_left,
            ),
            axis=axis,
        )
    else:
        return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


def daily_volatility(prices: pd.Series, trailing_days: int):
    """
    completes the 'x-Day' annualized volatility for all dates in `prices`
    * first calculates the pct_change() to get the daily change
    * then performs an 'x-Day' rolling window standard deviation on the data to get the daily volatility

    :param prices: the price data used in the volatility calculation can be a series or dataframe but
    the calculation will be applied to all cells. Should be recorded at daily intervals
    :param trailing_days: a the window for the calculation in days
    :return:
    """
    return prices.pct_change().rolling("%01id" % trailing_days).std()


def annualized_volatility(prices: pd.Series, trailing_days: int):
    """
    completes the 'x-Day' annualized volatility for all dates in `prices`
    * first calculates the pct_change() to get the daily change
    * then performs an 'x-Day' rolling window standard deviation on the data to get the daily volatility
    * then multiply by sqrt(252) to convert from daily volatility to annual volatility

    :param prices: the price data used in the volatility calculation can be a series or dataframe but
    the calculation will be applied to all cells. Should be recorded at daily intervals
    :param trailing_days: a the window for the calculation in days
    :return:
    """
    # dailyChange = prices[np.logical_and(prices.index >= start_date, prices.index <= end_date)].pct_change()
    # dailyVolatility = dailyChange.rolling('%01id'%trailing_timedelta.days).std()
    # annualVolatility = np.sqrt(252)*dailyVolatility
    return prices.pct_change().rolling("%01id" % trailing_days).std() * np.sqrt(252)
