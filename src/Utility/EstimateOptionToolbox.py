import pandas as pd
import numpy as np
import scipy
from .NavigateCalendar import middle_friday_of_month, last_businessday_of_month
from src.types import RawDateType
from typing import Union, List, Dict


def estimateSPXOptionDates(
    currentDate: pd.Timestamp,
    weekly: bool = True,
    mon: bool = True,
    wed: bool = True,
    fri: bool = True,
    monthly: bool = True,
    quarterly: bool = True,
    yearly: bool = True,
) -> pd.DataFrame:
    """
    Assumes:
        o  weekly options for 8 weeks including the current week
        o  monthly options on the last business day and the Friday after the 15th day of the month for 6 months
        o  quarterly options on the Friday after the 15th day of the last month of the quarter for 4 quarters
        o  yearly options on the Friday after the 15th day of june and dec for the next 2 years
    """
    exdates: Dict[str, str] = {}
    # weekly option dates
    if weekly:
        # current_date.isoweekday()%7 will give the the day of the week starting with sunday as the first day
        #   - default is monday as the first day)
        end_of_weeklies = currentDate + pd.Timedelta(
            days=(8 * 7) - currentDate.isoweekday() % 7
        )  # points to the sunday of the 9th week
        for wdate in pd.bdate_range(start=currentDate, end=end_of_weeklies, freq="B"):
            if mon and wdate.isoweekday() % 7 == 1:
                if wdate in exdates.keys():
                    exdates[wdate] += "Wm"
                else:
                    exdates[wdate] = "Wm"
            if wed and wdate.isoweekday() % 7 == 3:
                if wdate in exdates.keys():
                    exdates[wdate] += "Ww"
                else:
                    exdates[wdate] = "Ww"
            if fri and wdate.isoweekday() % 7 == 5:
                if wdate in exdates.keys():
                    exdates[wdate] += "Wf"
                else:
                    exdates[wdate] = "Wf"

    # monthly option dates
    if monthly:
        plus6months = currentDate + pd.Timedelta(days=180)
        final_year, finalMonth = plus6months.year, plus6months.month
        iterate_year, iterate_month = currentDate.year, currentDate.month
        while final_year > iterate_year or (
            final_year == iterate_year and finalMonth >= iterate_month
        ):
            middle_of_month = middle_friday_of_month(iterate_year, iterate_month)
            end_of_month = last_businessday_of_month(iterate_year, iterate_month)
            if middle_of_month in exdates.keys():
                exdates[middle_of_month] += "M"
            else:
                exdates[middle_of_month] = "M"
            if end_of_month in exdates.keys():
                exdates[end_of_month] += "M"
            else:
                exdates[end_of_month] = "M"
            iterate_month += 1
            iterate_year += int(iterate_month / 13)
            iterate_month = iterate_month % 12 if iterate_month != 12 else 12

    # quarterly option dates
    if quarterly:
        last_month_in_quarter_list = np.array([3, 6, 9, 12])
        start_month = min(
            last_month_in_quarter_list[last_month_in_quarter_list >= currentDate.month]
        )
        start_year = currentDate.year
        for i in range(4):
            month = start_month + i * 3
            year = start_year + int(month / 12)
            month %= 12
            if month == 0:
                month = 12
                year -= 1
            middle_of_month = middle_friday_of_month(year, month)
            end_of_month = last_businessday_of_month(year, month)
            if middle_of_month in exdates.keys():
                exdates[middle_of_month] += "Q"
            else:
                exdates[middle_of_month] = "Q"
            if end_of_month in exdates.keys():
                exdates[end_of_month] += "Q"
            else:
                exdates[end_of_month] = "Q"

    if yearly:
        # next year's yearly options
        year = currentDate.year
        june_date = middle_friday_of_month(year + 1, 6)
        dec_date = middle_friday_of_month(year + 1, 12)
        if june_date in exdates.keys():
            exdates[june_date] += "Y"
        else:
            exdates[june_date] = "Y"
        if dec_date in exdates.keys():
            exdates[dec_date] += "Y"
        else:
            exdates[dec_date] = "Y"
        # following year's yearly options
        june_date = middle_friday_of_month(year + 2, 6)
        dec_date = middle_friday_of_month(year + 2, 12)
        if june_date in exdates.keys():
            exdates[june_date] += "Y"
        else:
            exdates[june_date] = "Y"
        if dec_date in exdates.keys():
            exdates[dec_date] += "Y"
        else:
            exdates[dec_date] = "Y"
    df: pd.DataFrame = pd.DataFrame(data=exdates, index=["OptionRange"]).transpose()

    df = df.sort_index()
    return df


def createGenericStrikePriceRange(
    current_price: float,
    strike_deltas: List[float],
    lower_bounds: List[float],
    upper_bounds: List[float],
) -> List[float]:
    strikes: List[float] = []
    for i in range(len(strike_deltas)):
        delta = strike_deltas[i]
        start = round(
            lower_bounds[i] * current_price
            + (delta - lower_bounds[i] * current_price % delta),
            1,
        )
        end = round(
            upper_bounds[i] * current_price - upper_bounds[i] * current_price % delta, 1
        )
        num = int((end - start) / delta) + 1
        strikes += np.linspace(start, end, num=num, endpoint=True).tolist()
    strikes = list(set(strikes))
    strikes.sort()
    return strikes


def estimateWeeklyExpirationStrikes(current_price: float) -> List[float]:
    """ 5 deltas to model
        largest delta: start at 45% and ends at 120%
        max-1: start at 50% of the currentprice and end at 115%
        max-2: start at 72% and end at 112%
        max-3: start at 80% and end at 109%
        smallest: start at 85% and end at 107%
    """
    lower_bounds: List[float] = [0.85, 0.80, 0.72, 0.5, 0.45]
    upper_bounds: List[float] = [1.07, 1.09, 1.12, 1.15, 1.2]
    if current_price > 1000:
        strike_deltas: List[float] = [5, 10, 25, 50, 100]
    elif current_price > 500:
        strike_deltas = [2.5, 5, 10, 25, 50]
    elif current_price > 200:
        strike_deltas = [1, 2.5, 5, 10, 25]
    elif current_price > 100:
        strike_deltas = [0.5, 1, 2.5, 5, 10]
    elif current_price > 50:
        strike_deltas = [0.25, 0.5, 1, 2.5, 5]
    else:
        strike_deltas = [0.25, 0.5, 1, 2.5]
    return createGenericStrikePriceRange(
        current_price, strike_deltas, lower_bounds, upper_bounds
    )


def estimateMonthlyExpirationStrikes(current_price: float) -> List[float]:
    """ 3 delta's
        largest delta: starts at 40% ends at  120%
        n-1: starts at 55% and ends at 115%
        n-2: starts at 70% and ends at 110%"""
    lower_bounds: List[float] = [0.70, 0.55, 0.40]
    upper_bounds: List[float] = [1.10, 1.15, 1.20]
    if current_price > 1000:
        strike_deltas: List[float] = [25, 50, 100]
    elif current_price > 500:
        strike_deltas = [10, 25, 50]
    elif current_price > 200:
        strike_deltas = [5, 10, 25]
    elif current_price > 100:
        strike_deltas = [2.5, 5, 10]
    elif current_price > 50:
        strike_deltas = [1, 2.5, 5]
    else:
        strike_deltas = [1, 2.5]
    return createGenericStrikePriceRange(
        current_price, strike_deltas, lower_bounds, upper_bounds
    )


def estimateQuarterlyExpirationStrikes(
    current_price: float, days_out: Union[float, int]
) -> List[float]:
    """ 3 deltas to model
        largest delta: start at  (19.655ln(x) - 66.444)/100 percent of the currentprice and ends at 120%
        middle delta: start at (18.373ln(days out) - 42.749)/100 percent of the currentprice and end at 115%
        smallest delta: start at (19.752ln(x) - 37.118)/100 percent of the currentprice and end at 110%
    """
    lower_bounds: List[float] = [
        (19.752 * np.log(days_out) - 37.118) / 100.0,
        (18.373 * np.log(days_out) - 42.749) / 100.0,
        (19.655 * np.log(days_out) - 66.444) / 100.0,
    ]
    upper_bounds: List[float] = [1.10, 1.15, 1.20]
    if current_price > 1000:
        strike_deltas: List[float] = [25, 50, 100]
    elif current_price > 500:
        strike_deltas = [10, 25, 50]
    elif current_price > 200:
        strike_deltas = [5, 10, 25]
    elif current_price > 100:
        strike_deltas = [2.5, 5, 10]
    elif current_price > 50:
        strike_deltas = [1, 2.5, 5]
    else:
        strike_deltas = [1, 2.5]

    return createGenericStrikePriceRange(
        current_price, strike_deltas, lower_bounds, upper_bounds
    )


def estimateYearlyExpirationStrikes(current_price: float) -> List[float]:
    """ 3 delta's
        largest delta: starts at 0+delta ends at  140%
        n-1: starts at 25% and ends at 115%
        n-2: starts at 35% and ends at 110%"""
    assert np.isfinite(current_price), (
        "Invalid price passed to MarketHelper.estimateYearlyExpirationStrikes, "
        + str(current_price)
    )
    lower_bounds: List[float] = [0.35, 0.25, 0.01]
    upper_bounds: List[float] = [1.10, 1.15, 1.40]
    if current_price > 1000:
        strike_deltas: List[float] = [25, 50, 100]
    elif current_price > 500:
        strike_deltas = [10, 25, 50]
    elif current_price > 200:
        strike_deltas = [5, 10, 25]
    elif current_price > 100:
        strike_deltas = [2.5, 5, 10]
    elif current_price > 50:
        strike_deltas = [1, 2.5, 5]
    else:
        strike_deltas = [0.25, 0.5, 1]

    return createGenericStrikePriceRange(
        current_price, strike_deltas, lower_bounds, upper_bounds
    )


def estimateSPXStrikesOffered(
    currentPrice: float, daysOut: int, offeringType: List[str] = None
) -> List[float]:
    """offeringType can be one or more of ['weekly','monthly','quarterly','yearly']"""
    if offeringType is None:
        offeringType = ["weekly", "monthly", "quarterly", "yearly"]
    if isinstance(offeringType, str):
        offeringType = [offeringType]
    strikes: List[float] = []
    for offering in offeringType:
        indicator = offering[0].upper()
        if indicator in ["W", "M", "Q", "Y"]:
            if indicator == "W":
                strikes += estimateWeeklyExpirationStrikes(currentPrice)
            elif indicator == "M":
                strikes += estimateMonthlyExpirationStrikes(currentPrice)
            elif indicator == "Q":
                strikes += estimateQuarterlyExpirationStrikes(currentPrice, daysOut)
            elif indicator == "Y":
                strikes += estimateYearlyExpirationStrikes(currentPrice)
    strikes = list(set(strikes))
    strikes.sort()
    return strikes


def calculateValue_Excised(optionType: str, strike: float, price: float) -> float:
    if price is None or np.isnan(price):
        # if still none then return nan
        return np.nan
    if optionType[0].upper() == "C":
        if price <= strike:
            return 0.0
        else:
            return price - strike
    else:
        if price >= strike:
            return 0.0
        else:
            return strike - price


def PotentialOptionValue_AtExpiration(
    optionType: str,
    strike: float,
    predictionMean: float,
    predictionStd: float,
    stepCnt: int = 500,
) -> float:
    prediction = scipy.stats.norm(loc=predictionMean, scale=predictionStd)
    cumProbArray = np.linspace(0.001, 0.999, num=stepCnt)
    priceArray = prediction.ppf(cumProbArray)
    probabilityArray = prediction.pdf(priceArray)
    profitLoss = np.array(
        map(lambda price: calculateValue_Excised(optionType, strike, price), priceArray)
    )
    expectedValue = (profitLoss * probabilityArray).sum()
    return expectedValue
