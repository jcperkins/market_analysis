import numpy as np
import pandas as pd
from typing import Union, overload, Optional
from src.types import RawDateType, RawDateSingle, RawDateCollection, RawTimeDeltaType, RawTimeDeltaSingle, RawTimeDeltaCollection


@overload
def formatDateArray(date_array: None) -> None:
    ...


@overload
def formatDateArray(date_array: RawDateType) -> pd.DatetimeIndex:
    ...


def formatDateArray(date_array: Optional[RawDateType]) -> Union[None, pd.DatetimeIndex]:
    """default to the pd.DatetimeIndex; pandas will do all the work"""
    if date_array is None:
        return None
    else:
        try:
            output = pd.DatetimeIndex(date_array)
        except (ValueError, TypeError):
            output = pd.DatetimeIndex([date_array])
        return output



@overload
def formatDateInput(date: None) -> None:
    ...


@overload
def formatDateInput(date: RawDateCollection) -> pd.DatetimeIndex:
    ...


@overload
def formatDateInput(date: RawDateSingle) -> pd.Timestamp:
    ...


def formatDateInput(
    date: Optional[RawDateType]
) -> Union[None, pd.DatetimeIndex, pd.Timestamp]:
    """
    o  Accepts str, pd.Timestamp, np.datetime64, datetime.date,
        datetime.datetime
      o  default format if the date is a txt format is 'YYYY-MM-DD' but
          can accommodate 'MM-DD-YYYY'
    o  Output date in pd.DatetimeIndex if the input is a collection or pd.Timestamp if not
    """
    if date is None:
        return None
    elif isinstance(date, pd.Timestamp) or isinstance(date, pd.DatetimeIndex):
        return date
    elif type(date) in [pd.DatetimeIndex, pd.Index, list, set, np.ndarray, pd.Series]:
        return formatDateArray(date)
    else:
        return pd.Timestamp(date)


@overload
def format_timedeltaindex(value: None) -> None:
    ...


@overload
def format_timedeltaindex(value: RawTimeDeltaType) -> pd.TimedeltaIndex:
    ...


def format_timedeltaindex(value: Union[None, RawTimeDeltaType], unit=None) -> Union[None, pd.TimedeltaIndex]:
    """default to the pd.TimedeltaIndex; pandas will do all the work"""
    if value is None:
        return None
    else:
        try:
            output = pd.TimedeltaIndex(value, unit=unit)
        except (ValueError, TypeError):
            output = pd.TimedeltaIndex([value], unit=unit)
        return output


@overload
def format_timedelta(value: None) -> None:
    ...


@overload
def format_timedelta(value: RawTimeDeltaSingle) -> pd.Timedelta:
    ...


@overload
def format_timedelta(value: RawTimeDeltaCollection) -> pd.TimedeltaIndex:
    ...


def format_timedelta(value: RawTimeDeltaType, unit: Optional[str]=None) -> Union[None, pd.Timedelta, pd.TimedeltaIndex]:
    """
    Values of None pass through otherwise return a pandas Timedelta or TimedeltaIndex (whichever
    best matches the structure of `value`) and let pandas do the work.

    :param value: Timedelta, np.timedelta64, string, or integer value for the time delta
    :param unit: parameter that is passed to pd.Timedelta, None is the pandas default to unit which is 'ns'
    :return:
    """
    if value is None:
        return None
    elif isinstance(value, pd.Timedelta) or isinstance(value, pd.TimedeltaIndex):
        return value
    elif type(value) in [pd.TimedeltaIndex, pd.Index, list, set, np.ndarray, pd.Series]:
        return pd.TimedeltaIndex(value)
    else:
        return pd.Timedelta(value, unit=unit)