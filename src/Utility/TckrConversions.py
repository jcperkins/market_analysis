def convertTckrToYahooFmt(tckr: str) -> str:
    if tckr.upper() in [
        "SPX",
        ".SPX",
        "^SPX",
        "$SPY",
        "IXSPX",
        "IXGSPC",
        ".GSPC",
        "$GSPC",
    ]:
        return "^GSPC"
    elif tckr.upper() in ["VIX", ".VIX", "^VIX", "$VIX", "IXVIX"]:
        # new VIX formula S&P500(1M)
        return "^VIX"
    elif tckr.upper() in ["VXV", ".VXV", "^VXV", "$VXV", "IXVXV"]:
        # 3m S&P500 volatility
        return "^VXV"
    elif tckr.upper() in ["VXO", ".VXO", "^VXO", "$VXO", "IXVXO"]:
        # original formula volatility
        return "^VXO"
    else:
        return tckr.upper()


def convertTckrToUniversalFormat(tckr: str) -> str:
    return (
        tckr.replace(".", "ix").replace("^", "ix").replace("$", "ix").replace("/", "")
    )
