import numpy as np
from scipy import stats
from typing import Tuple
from src.types import TorArray


# Black and Scholes option price calculation
def d1(
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
    d: TorArray[float] = 0,
) -> TorArray[float]:
    v1 = np.log(S / K)
    v2 = ((r - d) + (sigma * sigma) / 2.0) * T
    v3 = sigma * np.sqrt(T)
    return (v1 + v2) / v3


def d2(
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
    d: TorArray[float] = 0,
) -> TorArray[float]:
    return d1(S, K, sigma, r, T, d=d) - (sigma * np.sqrt(T))


def BlackScholes(
    method: str,
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
    d: TorArray[float] = 0,
) -> TorArray[float]:
    """BlackSholes calculation for option pricing
    C = S*N(d1)-K*e^(-rt)*N(d2)
    d_1 = (Ln(S/K)+(r+0.5*sigma^2)*T)/(sigma*sqrt(T))
    d_2=d1-sigma*sqrt(T)
    o  method is 'C' for call or 'P' for put
    o  N() is the CDF of standard normal distribution
    o  S is price of the underlying asset
    o  K is the strike price
    o  sigma is the volatility (should be < 1)
    o  r is the risk free rate (annual rate, expressed in terms of
                                continuous compounding)
    o  T is the time to maturity expressed in years
    o  d is the dividend yield
    """
    v1 = d1(S, K, sigma, r, T, d=d)
    v2 = d2(S, K, sigma, r, T, d=d)

    if method == "C":
        price = S * np.exp(-d * T) * stats.norm.cdf(v1) - K * np.exp(
            -r * T
        ) * stats.norm.cdf(v2)
    else:
        price = K * np.exp(-r * T) * stats.norm.cdf(-v2) - S * np.exp(
            -d * T
        ) * stats.norm.cdf(-v1)
    if type(T) in [list, set, np.ndarray]:
        price[T < 0] = 0
    elif T < 0:
        price = 0

    return price


def BlackScholes_ImpVol_(
    method: str,
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
    d: TorArray[float] = 0,
) -> TorArray[float]:
    """BlackSholes calculation for option pricing
    C = S*N(d1)-K*e^(-rt)*N(d2)
    d_1 = (Ln(S/K)+(r+0.5*sigma^2)*T)/(sigma*sqrt(T))
    d_2=d1-sigma*sqrt(T)
    o  method is 'C' for call or 'P' for put
    o  P is the price (not needed for this calculation)
    o  N() is the CDF of standard normal distribution
    o  S is price of the underlying asset
    o  K is the strike price
    o  sigma is the volatility (should be < 1)
    o  r is the risk free rate (annual rate, expressed in terms of
                                continuous compounding)
    o  T is the time to maturity expressed in years
    o  d is the dividend yield
    """
    v1 = d1(S, K, sigma, r, T, d=d)
    v2 = d2(S, K, sigma, r, T, d=d)

    if method == "C":
        price = S * np.exp(-d * T) * stats.norm.cdf(v1) - K * np.exp(
            -r * T
        ) * stats.norm.cdf(v2)
    else:
        price = K * np.exp(-r * T) * stats.norm.cdf(-v2) - S * np.exp(
            -d * T
        ) * stats.norm.cdf(-v1)
    if type(T) in [list, set, np.ndarray]:
        price[T < 0] = 0
    elif T < 0:
        price = 0

    return price


def Black_Scholes_Greeks_Call(
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
    d: TorArray[float] = 0,
) -> Tuple[
    TorArray[float], TorArray[float], TorArray[float], TorArray[float], TorArray[float]
]:
    """
    Calculating the partial derivatives for a Black Scholes Option (Call)
    # S - Stock price
    # K - Strike price
    # T - Time to maturity
    # r - Risk free interest rate
    # d - Dividend yield
    # sigma - Volatility
    Return:
    Delta: partial wrt S0
    Gamma: second partial wrt S0
    Theta: partial wrt T
    Vega: partial wrt sigma
    Rho: partial wrt r
    """
    T_sqrt = np.sqrt(T)
    v1 = d1(S, K, sigma, r, T, d=d)
    dv1dT = (r - d + sigma * sigma / 2 - np.log(S / K) / T) * (1 / (2 * sigma * T_sqrt))
    dv1dsigma = (
        -(np.log(S / K) / T_sqrt + T_sqrt * (r - d)) / (sigma * sigma) + T_sqrt / 2.0
    )
    dv1dr = dv2dr = T_sqrt / sigma
    v2 = d2(S, K, sigma, r, T, d=d)
    dv2dT = dv1dT - sigma / (2 * T_sqrt)
    dv2dsigma = dv1dsigma - T_sqrt
    Delta = np.exp(-d * T) * stats.norm.cdf(v1)
    Gamma = np.exp(-d * T) * stats.norm.pdf(v1) / (S * sigma * T_sqrt)
    # Theta = -(S*sigma*stats.norm.pdf(v1))/(2*T_sqrt) - r*K*np.exp( -r*T)*stats.norm.cdf(v2)
    Theta = (
        -(S * d * np.exp(-d * T) * stats.norm.cdf(v1))
        + (S * np.exp(-d * T) * stats.norm.pdf(v1) * dv1dT)
        + (r * K * np.exp(-r * T) * stats.norm.cdf(v2))
        - (K * np.exp(-r * T) * stats.norm.pdf(v2) * dv2dT)
    )
    # Vega = S *np.exp(-d*T)* T_sqrt*stats.norm.pdf(v1)
    Vega = (
        S * np.exp(-d * T) * stats.norm.pdf(v1) * dv1dsigma
        - K * np.exp(-r * T) * stats.norm.pdf(v2) * dv2dsigma
    )
    # Rho = K*T*np.exp(-r*T)*stats.norm.cdf(v2)
    Rho = (
        S * np.exp(-d * T) * stats.norm.pdf(v1) * dv1dr
        + r * K * np.exp(-r * T) * stats.norm.cdf(v2)
        - K * np.exp(-r * T) * stats.norm.pdf(v2) * dv2dr
    )
    return Delta, Gamma, Theta, Vega, Rho


def Black_Scholes_Greeks_Put(
    S: TorArray[float],
    K: TorArray[float],
    sigma: TorArray[float],
    r: TorArray[float],
    T: TorArray[float],
) -> Tuple[
    TorArray[float], TorArray[float], TorArray[float], TorArray[float], TorArray[float]
]:
    """
    Calculating the partial derivatives for a Black Scholes Option (Put)
    # S - Stock price
    # K - Strike price
    # T - Time to maturity
    # r - Risk free interest rate
    # d - Dividend yield (not needed for this calculation)
    # sigma - Volatility
    Return:
    Delta: partial wrt S0
    Gamma: second partial wrt S0
    Theta: partial wrt T
    Vega: partial wrt sigma
    Rho: partial wrt r
    """
    T_sqrt = np.sqrt(T)
    _d1 = (np.log(S / K) + r * T) / (sigma * T_sqrt) + 0.5 * sigma * T_sqrt
    _d2 = _d1 - (sigma * T_sqrt)
    Delta = -stats.norm.cdf(-_d1)
    Gamma = stats.norm.pdf(_d1) / (S * sigma * T_sqrt)
    Theta = -(S * sigma * stats.norm.pdf(_d1)) / (2 * T_sqrt) + r * K * np.exp(
        -r * T
    ) * stats.norm.cdf(-_d2)
    Vega = S * T_sqrt * stats.norm.pdf(_d1)
    Rho = -K * T * np.exp(-r * T) * stats.norm.cdf(-_d2)
    return Delta, Gamma, Theta, Vega, Rho
