import sys
import os
from contextlib import contextmanager


@contextmanager
def HiddenPrints(active: bool = True):
    if active:
        old_stdout = sys.stdout
        sys.stdout = open(os.devnull, "w")
        try:
            yield
        finally:
            sys.stdout = old_stdout
    else:
        yield


# Returns a bool indicator of whether the print statement is currently blocked
def isPrintBlocked() -> bool:
    return not sys.stdout == sys.__stdout__
