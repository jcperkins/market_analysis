# ---------------------------------------------------------------------------
# Name:        PyBridge
# Purpose:     Utility function to navigate the differences between python 2.7
#               and python 3.x
#
# Author:      Cory Perkins
#
# Created:     10/08/2018
# Copyright:   (c) Cory Perkins 2018
# Licence:     <your licence>
# ----------------------------------------------------------------------------
import sys


# set this up as a class so that I can protect the is_py3 variable
# noinspection PyUnboundLocalVariable,PyUnresolvedReferences
class PyBridge(object):
    def __init__(self):
        self.is_py3 = sys.version_info[0] == 3

    # methods to determine which version of python is running and helper functions
    #  to navigate where things differ
    @staticmethod
    def py_version_major():
        return sys.version_info[0]

    @staticmethod
    def py_version_minor():
        return sys.version_info[1]

    @staticmethod
    def py_version():
        return ".".join([str(sys.version_info[0]), str(sys.version_info[1])])

    def is_Py3(self):
        return self.is_py3

    def is_Py2(self):
        return not self.is_py3

    def import_StringIO(self):
        if self.is_py3:
            from io import StringIO
        else:
            from StringIO import StringIO
        return StringIO

    def reload_function(self):
        if self.is_Py3:
            from importlib import reload
        return reload

    def user_input(self, message=""):
        return input(message) if self.is_py3 else raw_input(message)

    def iter_range(self, n):
        return range(n) if self.is_py3 else range(n)
