import pandas as pd
from typing import Union, overload
from src.types import Scalar


@overload
def project_data_to_dates(
    data: pd.Series, to_dates=pd.Timestamp, method="ffill"
) -> Scalar:
    ...


@overload
def project_data_to_dates(
    data: pd.Series, to_dates=pd.DatetimeIndex, method="ffill"
) -> pd.Series:
    ...


@overload
def project_data_to_dates(
    data: pd.DataFrame, to_dates=pd.Timestamp, method="ffill"
) -> pd.Series:
    ...


@overload
def project_data_to_dates(
    data: pd.DataFrame, to_dates=pd.DatetimeIndex, method="ffill"
) -> pd.DataFrame:
    ...


def project_data_to_dates(
    data: Union[pd.Series, pd.DataFrame],
    to_dates=Union[pd.Timestamp, pd.DatetimeIndex],
    method="ffill",
) -> Union[Scalar, pd.Series, pd.DataFrame]:
    full_idx = data.index.union(
        [to_dates] if isinstance(to_dates, pd.Timestamp) else to_dates
    )
    result = (
        data.reindex(full_idx, method=method)
        if len(full_idx) != len(data.index)
        else data.fillna(inplace=False, method=method)
    )
    return (
        result[to_dates] if isinstance(result, pd.Series) else result.loc[to_dates, :]
    )
