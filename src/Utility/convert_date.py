import datetime
from typing import Union, Type, cast
import numpy as np
import pandas as pd
from src.types import RawDateType
from .variable_formatting import formatDateArray


def convert_date(
    date: RawDateType, date_type: Type = pd.Timestamp
) -> Union[
    pd.DatetimeIndex, pd.Timestamp, datetime.date, datetime.datetime, np.datetime64
]:
    """
    o  Accepts str, pd.Timestamp, np.datetime64, datetime.date, datetime.datetime
        o  default format if the date is a txt format is 'YYYY-MM-DD' but
          can accommodate 'MM-DD-YYYY'
    o  Output date in the dateType format specified
        o  all input formats except for str can be output formats
    o  convert all formats to pd.Timestamp unless they are already in the
        correct format and then convert to the proper format at the end
        """
    if isinstance(date, date_type):
        # noinspection PyTypeChecker
        return date
    elif type(date) in [pd.DatetimeIndex, pd.Index, list, set, np.ndarray, pd.Series]:
        return formatDateArray(date)
    else:
        # first let pandas do the work and convert to a Timestamp
        try:
            date = pd.Timestamp(date)
        except ValueError:
            # try using numpy astype() method to catch random variations of datetime64
            return cast(np.datetime64, date).astype(date_type)
        # ensure that the date is in the correct format
        if type(date) == date_type:
            return date
        else:
            assert isinstance(date, pd.Timestamp)
            if date_type == datetime.date:
                return date.date()
            elif date_type == np.datetime64:
                return date.asm8
            elif date_type == datetime.datetime:
                return date.to_pydatetime()
            else:
                # if none of the date_types match try a variation of a np.datetime
                #  format return the date if it fails
                return date.asm8().astype(date_type)
