# pulled from https://stackoverflow.com/questions/11276037/resolving-metaclass-conflicts


def metaclass_resolver(*classes):
    metaclass = tuple(set(type(cls) for cls in classes))
    if len(metaclass) == 1:
        metaclass = metaclass[0]
    else:
        # class M_C
        metaclass = type("_".join(mcls.__name__ for mcls in metaclass), metaclass, {})
    return metaclass("_".join(cls.__name__ for cls in classes), classes, {})
