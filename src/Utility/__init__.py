# analytical toolbox
from .Analytics import (
    r2,
    gcd,
    integrate_line_times_normal_dist,
    rolling_window_1d,
    rolling_window_2d,
    daily_volatility,
    annualized_volatility
)
from .BlackAndScholesCalcs import (
    d1,
    d2,
    BlackScholes,
    BlackScholes_ImpVol_,
    Black_Scholes_Greeks_Call,
    Black_Scholes_Greeks_Put,
)
from .EstimateOptionToolbox import (
    estimateMonthlyExpirationStrikes,
    estimateWeeklyExpirationStrikes,
    estimateMonthlyExpirationStrikes,
    estimateQuarterlyExpirationStrikes,
    estimateYearlyExpirationStrikes,
    estimateSPXStrikesOffered,
    createGenericStrikePriceRange,
    calculateValue_Excised,
    PotentialOptionValue_AtExpiration,
)
from .NavigateCalendar import (
    month2int,
    int2month,
    interp_span,
    est_businessdays_span,
    last_businessday_of_month,
    last_friday_of_month,
    middle_friday_of_month,
)

# stdout toolbox
from .PrintBlocking import HiddenPrints, isPrintBlocked
from .PyBridge import PyBridge

# tckr formatting toolbox
from .TckrConversions import convertTckrToUniversalFormat, convertTckrToYahooFmt

# option calculations toolbox
from .calculateMaxPositionQtySPX import calculateMaxPositionQtySPX

# comparisons
from .comparisons import (
    is_same,
    assert_same,
    is_iterable_equal,
    assert_iterable_equal,
    is_equal,
    assert_equal,
    is_subset,
    assert_subset,
)

# datetime toolbox
from .convert_date import convert_date
from .variable_formatting import (
    formatDateArray,
    formatDateInput,
    format_timedeltaindex,
    format_timedelta,
)

# misc
from .mataclass_resolver import metaclass_resolver
from .project_data_to_dates import project_data_to_dates

# helpful strategy information
uniqueTrades_Options = {
    "Call": 1,
    "Put": 1,
    "Bullish Put Spread": 2,
    "Bullish Call Spread": 2,
    "Bearish Put Spread": 2,
    "Bearish Call Spread": 2,
    "Iron Condor": 4,
    "Put Condor": 4,
    "Call Condor": 4,
    "Iron Butterfly": 4,
    "Put Butterfly": 3,
    "Call Butterfly": 3,
}
totalContracts_Options = {
    "Call": 1,
    "Put": 1,
    "Bullish Put Spread": 2,
    "Bullish Call Spread": 2,
    "Bearish Put Spread": 2,
    "Bearish Call Spread": 2,
    "Iron Condor": 4,
    "Put Condor": 4,
    "Call Condor": 4,
    "Iron Butterfly": 4,
    "Put Butterfly": 4,
    "Call Butterfly": 4,
}

monthAbbrToNumConversion = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12,
}

"""
************************ Fundamental Options ************************
      _-_-_-_ Call _-_-_-       #     _-_-_-_ Put _-_-_-_
                 /  Buy Call    #  \\ Buy put
Start @ 0       /               #   \\        End @ 0
--------------X                 #     X--------------
              \\                #    /
               \\ Sell Call     #   / Sell put
$ = np.clip( price, X, None)    #   $ = np.clip( price, None, X)
Value Buy: $ - X + prem         #   Value sell: $ - X + prem
Value sell:  X - $ + prem       #   Value buy: X - $ + prem
Value Buy ~ -1*value sell       #   Value Buy ~ -1*value sell

************************* Vertical spreads **************************
    Bullish Put/Call Spread     #  Bearish Put/Call Spread
                  Sell P.-0     #  Sell P.
                 X1---------    #  --------X0
                /  Sell C.      #  Sell C.-0\\ 
    Buy P.     /                #            \\ Buy P.-0
    ---------X0                 #             X1---------
    Buy C.-0                    #                  Buy C.
                                #
$ = np.clip(price, X0, X1)      # $ = np.clip(price, X0, X1)
prem        = P(X1) - P(X0)     # prem      = P(X0) - P(X1)
Typically   : prem_call < 0     # Typically : prem_call > 0 (max profit)
            : prem_put > 0      #           : prem_put  < 0 (max loss)
Value Bul C = $-X0+prem = VBuC  # Value Be C= X0 - $ + prem = VBeC
Value Bul P = $-X1+prem = VBuP  # Value Be P= X1 - $ + prem = VBeP
                VBuC = -1*VBeC and VBuP = -1*VBeP

****************************** Collars ******************************
Purchased to protect a long position in the underlying asset 

$_s = Price of Stock and in the charts this represents a profit = 0 + prem
$_c = Sell price of the call
$_p = Buy price of the put             Sell C.
X_p is put strike price            X_c---------
X_c is call strike price          /
$ is $_s at expiration           $_s - Purchase price of the asset
                    Buy P.     /
                    ---------X_p

Because the slope is zero to the left of the put on a bullish play and 0 to the right
of the put on a bearish play with the put always being the lower price, the max_loss will
always be the value of the underlying position + put option value + call option value, all
calculated at the put strike price. Max profit will be similar but at the call price

Define:
* Premium = $_c - $_p
* Put_Value@X_p = 0
* Put_Value@X_c = max(0, X_p - X_c)
* Call_Value@X_p = min(0, X_c - X_p)
* Call_value@X_c = 0
* Stock_value@x = $_s - x
Max Loss($=X_p) = Premium + Put_Value@X_p + Call_Value@X_p + Stock_value@X_p
Max Profit($=X_c) = Premium + Put_Value@X_c + Call_Value@X_c + Stock_value@X_c
Investment to open  = $_s + Premium
_-_-_-_-_-_-_-_-_-_-_-_-_- Alternatives  -_-_-_-_-_-_-_-_-_-_-_-_-_-_
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#       #   X_p < $_s < X_c   #   $_s < X_p < X_c   #   X_p < X_c < $_s   #
#   B   #            Sell C.  #            Sell C.  #            Sell C.  #
#   U   #            X_c----  #            X_c----  #            X_c--$_s #
#   L   #           /         #           /         #           /         #
#   L   #          $_s        #          /          #          /          #
#   I   # Buy P.  /           # Buy P.  /           # Buy P.  /           #
#   S   # ------X_p           # $_s----X_p          # ------X_p           #
#   H   # M_L =prem-($_s-X_p) # M_L =prem           # M_L =prem-(X_c-X_p) #
#       # M_P =prem+(X_c-$_s) # M_P =prem+(X_c-X_p) # M_P =prem           #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#       #   X_c < $_s < X_p   #   $_s < X_c < X_p   #   X_c < X_p < $_s   #
#   B   # Sell C.             # Sell C.             # Sell C.             #
#   E   # -----X_c            # $_s--X_c            # -----X_c            #
#   A   #       \\            #        \\            #       \\           #
#   R   #         $_s         #         \\          #         \\          #
#   I   #          \\ Buy P.  #          \\ Buy P.  #          \\ Buy P.  #
#   S   #           X_p-----  #           X_p------ #           X_p---$_s #
#   H   # M_L =prem-(X_c-$_s) # M_L =prem-(X_c-X_p) # M_L =prem           #
#       # M_P =prem+($_s-X_p) # M_P =prem           # M_P =prem+(X_c-X_p) #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

_-_-_-_-_-_-_-_-_-_-_-_-_-_- Bullish  -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-    
$_s = Price of Stock and in the charts this represents a profit = 0 + prem
$_c = Sell price of the call
$_p = Buy price of the put             Sell C.    
X_p is put strike price            X_c---------    
X_c is call strike price          /.        
$ is $_s at expiration           $_s - Purchase price of the asset 
                    Buy P.     /.                
                    ---------X_p                                    
Define:
* Premium = Prem      = $_c - $_p       
* Elbow_0 = Min(X_c, X_p)
* Elbow_1 = Max(X_c, X_p)
Max Loss = (Min($_s, Elbow_1) - Min($_s,Elbow_0) + prem
Max Profit($>=X_c)   = if X_p<$_s then (X_c - $_s + prem) else Abs(X_c-X_p) + prem
Investment to open  = $_s + prem
_-_-_-_-_-_-_-_-_-_-_-_-_- Alternatives  -_-_-_-_-_-_-_-_-_-_-_-_-_-_
#            Sell C.  #            Sell C.  #            Sell C.  #
#            X_c----  #            X_c----  #            X_c--$_s # 
#           /.        #           /.        #           /.        # 
#          $_s        #          /.         #          /.         #
# Buy P.  /.          # Buy P.  /.          # Buy P.  /.          #       
# ------X_p           # $_s----X_p          # ------X_p           #
# M_L =($_s-X_p)+prem # M_L = prem          # M_L =(X_c-X_p)+prem #
# M_P =(X_c-$_s)+prem # M_P =(X_c-X_p)+prem # M_P = prem          #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  
#            Buy P.   #             Buy P.  #             Buy P.  #
#            X_p----  #            X_p----  #            X_p--$_s # 
#           /.        #           /.        #           /.        # 
#          $_s        #          /.         #          /.         #
# Sell C. /.          # Sell C. /.          # Sell C. /.          #       
# ------X_c           # $_s----X_c          # ------X_c           #    
# M_L =($_s-X_c)+prem # M_L = prem          # M_L =(X_p-X_c)+prem #
# M_P =(X_p-$_s)+prem # M_P =(X_p-X_c)+prem # M_P = prem          #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
Elbow_0 = Min(X_c, X_p)
Elbow_1 = Max(X_c, X_p)
Max Loss = (Min($_s, Elbow_1) - Min($_s,Elbow_0) + prem
                        
Premium = Prem      = $_c - $_p       
Max Loss($<=X_p)     = Max(0, $_s - X_p) + Prem
Max Profit($>=X_c)   = if X_p<$_s then (X_c - $_s + prem) else Abs(X_c-X_p) + prem
Investment to open  = $_s + prem

****************************** Condors ******************************
_-_-_-_-_-_-_-_-_-_-_-_-_-_- Iron Condor  -_-_-_-_-_-_-_-_-_-_-_-_-_-
                        X1------ 0 -----X2. buy C.
                       /  Sell P.         \\
            Buy P.   /                     \\.Sell C.
            -------X0.                       X3----
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = X1 - $ + prem
Else:
  $ = np.clip(price,X2,X3)
  Value = X2 - $ + prem

_-_-_-_-_-_-_-_-_-_-_-_-_-_- Call Condor  -_-_-_-_-_-_-_-_-_-_-_-_-_-
                X1-----------X2. buy C.
                /  Sell C.     \\ 
      Buy C.   /                \\ Sell C.
    -- 0 ---X0.                    X3-- 0 --
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = $ - X0 + prem
Else:
  $ = np.clip(price,X2,X3)
  Value = X3 - $ + prem

 _-_-_- Put Condor  -_-_-_
             X1-----------X2. Sell P.
          /  Sell P.         \\ 
Buy P.   /                    \\ Buy P.
-- 0 --X0.                       X3-- 0 --
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = Price - X0 + prem
Else:
  $ = np.clip(price,X2,X3)
  Value = X3 - $ + prem


****** Butterflies ******
 _-_-_- Iron Butterfly  -_-_-_
                X1 - 0   Sell P, Sell C
             /      \\ 
  Buy P.    /        \\  Buy C.
----------X0.          X2---------
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = X1 - $ + prem
Else:
  $ = np.clip(price,X1,X2)
  Value = $ X2 + prem

 _-_-_- Call Butterfly  -_-_-_
                 X1   Sell C x2
              /      \\ 
   Buy C.    /        \\  Buy C.
---- 0 ----X0.          X2---- 0 ----
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = $ - X0 + prem
Else:
  $ = np.clip(price,X1,X2)
  Value = X2 - $ + prem

 _-_-_- Put Butterfly  -_-_-_
                 X1  Buy P 2x
              /      \\ 
  Sell P.    /        \\ Sell P.
---- 0 ----X0.          X2---- 0 ----
If( price < X1 ):
  $ = np.clip(price,X0,X1)
  Value = Price - X0 + prem
Else:
  $ = np.clip(price,X1,X2)
  Value = X2 - $ + prem

**** Calendar Spread ****
Volatility play.
_-_-_- high now, low later -_-_-_
If volatility is high and you think that it will go lower in the future, then sell
a position with a long date and cover by buying and rolling a position with a shorter date. 
As it rolls you make money as Vol goes down and the option gets cheaper.

_-_-_- low now, high later -_-_-_
Vice-versa above
"""
