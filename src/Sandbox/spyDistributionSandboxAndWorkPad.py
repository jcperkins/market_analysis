import pandas as pd
import pandas_datareader.data as web
import requests
import numpy as np
import datetime
import os
from scipy import stats  # , linspace, polyval, polyfit, sqrt, randn
from math import log
from matplotlib import pyplot as plt
from config import default_settings

spanToDays = {"1yr": 252, "6m": 126, "3m": 63, "1m": 21, "1wk": 5}
spanToProjectedDays = {"1yr": 365, "6m": 180, "3m": 90, "1m": 30, "1wk": 7}
spansInAnalysis = ["1yr", "6m", "3m", "1m"]
tckr = "SPY"
mainDir = default_settings.root_dir
storage = pd.HDFStore(os.path.join(default_settings.data_dir, "TckrData.h5"))
tckrList = storage.keys()
for iName in range(len(tckrList)):
    tckrList[iName] = tckrList[iName].split(".")[0]


# ********************************************************************
# ------------  Begin Actual Price vs. Bid vs. Ask Work -------------
# ********************************************************************
intraDayStorage = pd.HDFStore(
    os.path.join(
        default_settings.data_dir,
        "IntraDayOptions",
        "Data",
        "DailyOptionQuotesFidelity_Sandbox.h5",
    )
)
intraDayStorage.open()
allIntraDayOptions = intraDayStorage["ixSPX"]
intraDayStorage.close()
outputCols = [
    "Symbol",
    "ExDate",
    "DaysToExpiration",
    "Strike",
    "Type",
    "PrevTS",
    "NextTS",
    "QTY",
    "LastPrice",
    "PreviousAsk",
    "PreviousAskSize",
    "PreviousBid",
    "PreviousBidSize",
    "PreviousImpVol",
    "PreviousOpenInt",
]
data = []
# loop through Symbols to loop through ExDates, Strikes, and Type at the same time
uniqueSymbols = allIntraDayOptions.Symbol.unique()
symbolCnt = 0
totalSymbols = len(uniqueSymbols)
for symbol in uniqueSymbols:
    # debug: symbol = '-SPX170818P2415'
    # debug symbolCnt = 2950
    symbolCnt += 1
    if symbolCnt % 100 == 0:
        print("Symbol %01i of %01i" % (symbolCnt, totalSymbols))
    symbolData = allIntraDayOptions.ix[
        allIntraDayOptions.Symbol == symbol,
        [
            "Symbol",
            "ExDate",
            "DaysToExpiration",
            "Strike",
            "Type",
            "QueryTimeStamp",
            "Last",
            "Ask",
            "Ask Size",
            "Bid",
            "Bid Size",
            "Volume",
            "Open Int",
            "Imp Vol",
        ],
    ]
    # get timestamps and loop through to track changes in volume
    symbolTimeStamps = np.sort(symbolData.QueryTimeStamp.unique())
    for tsIndex in range(len(symbolTimeStamps) - 1):
        contractsPurchased = int(
            symbolData.ix[
                symbolData.QueryTimeStamp == symbolTimeStamps[tsIndex + 1], "Volume"
            ]
        ) - int(
            symbolData.ix[
                symbolData.QueryTimeStamp == symbolTimeStamps[tsIndex], "Volume"
            ]
        )
        if contractsPurchased > 0:
            # get info from previous timestamp row
            exDate, strike, optionType, prevTS, ask, askSize, bid, bidSize, impVol, openInt = symbolData.ix[
                symbolData.QueryTimeStamp == symbolTimeStamps[tsIndex],
                [
                    "ExDate",
                    "Strike",
                    "Type",
                    "QueryTimeStamp",
                    "Ask",
                    "Ask Size",
                    "Bid",
                    "Bid Size",
                    "Imp Vol",
                    "Open Int",
                ],
            ].iloc[
                0, :
            ]
            # get info from next timestamp row
            daysToExpiration, nextTS, lastPrice = symbolData.ix[
                symbolData.QueryTimeStamp == symbolTimeStamps[tsIndex + 1],
                ["DaysToExpiration", "QueryTimeStamp", "Last"],
            ].iloc[0, :]

            data.append(
                [
                    symbol,
                    exDate,
                    daysToExpiration,
                    strike,
                    optionType,
                    prevTS,
                    nextTS,
                    contractsPurchased,
                    lastPrice,
                    ask,
                    askSize,
                    bid,
                    bidSize,
                    impVol,
                    openInt,
                ]
            )

allPurchases = pd.DataFrame(data=data, columns=outputCols)


# ********************************************************************
# --------------  Begin SPY distribution modeling work --------------
# ********************************************************************


def interpretSpanToDays(span):
    if "yr" in span:
        unitDays = 252
        if "yrs" in span:
            try:
                qty = int(span.replace("yrs", ""))
            except:
                return np.nan
        else:
            try:
                qty = int(span.replace("yr", ""))
            except:
                return np.nan
    elif "m" in span:
        unitDays = 21
        try:
            qty = int(span.replace("m", ""))
        except:
            return np.nan
    elif "wk" in span:
        unitDays = 5
        if "wks" in span:
            try:
                qty = int(span.replace("wks", ""))
            except:
                return np.nan
        else:
            try:
                qty = int(span.replace("wk", ""))
            except:
                return np.nan
    else:
        return np.nan
    return unitDays * qty


def interpretSpanToProjectedDays(span):
    if "yr" in span:
        unitDays = 365
        if "yrs" in span:
            try:
                qty = int(span.replace("yrs", ""))
            except:
                return np.nan
        else:
            try:
                qty = int(span.replace("yr", ""))
            except:
                return np.nan
    elif "m" in span:
        unitDays = 30
        try:
            qty = int(span.replace("m", ""))
        except:
            return np.nan
    elif "wk" in span:
        unitDays = 7
        if "wks" in span:
            try:
                qty = int(span.replace("wks", ""))
            except:
                return np.nan
        else:
            try:
                qty = int(span.replace("wk", ""))
            except:
                return np.nan
    else:
        return np.nan
    return unitDays * qty

    def exploreStockDistribution(self, tckr):
        spanToDays = {"1yr": 252, "6m": 126, "3m": 63, "1m": 21, "1wk": 5}
        spanToProjectedDays = {"1yr": 365, "6m": 180, "3m": 90, "1m": 30, "1wk": 7}
        spansInAnalysis = ["1yr", "6m", "3m", "1m"]
        tckrData = self.loadTckrData(tckr)
        maxRow = len(tckrData) - 1
        # outColumns = ['Stock','Date','Price','Slope-1yr','Intercept-1yr','Rsq-1yr','AnnualVolatility-1yr','FuturePrice-1yr','PriceDelta-1yr','Slope-6m','Intercept-6m','Rsq-6m','AnnualVolatility-6m','FuturePrice-6m','PriceDelta-6m','Slope-3m','Intercept-3m','Rsq-3m','AnnualVolatility-3m','FuturePrice-3m','PriceDelta-3m','Slope-1m','Intercept-1m','Rsq-1m','AnnualVolatility-1m','FuturePrice-1m','PriceDelta-1m','Slope-1wk','Intercept-1wk','Rsq-1wk','AnnualVolatility-1wk','FuturePrice-1wk','PriceDelta-1wk']
        outColumns = ["Stock", "Date", "Price"]
        for span in spansInAnalysis:
            for dataCategory in [
                "Slope",
                "SlopeLN",
                "Rsq",
                "RsqLN",
                "AnnualVolatility",
            ]:
                outColumns.append("%s-%s" % (dataCategory, span))
        data = np.empty((maxRow + 1, len(outColumns))) * np.nan
        stockDist = pd.DataFrame(data=data, columns=outColumns)
        stockDist.ix[:, "Stock"] = tckr
        stockDist.Date = tckrData.index
        stockDist.Price = tckrData.ix[:, "Adj Close"].tolist()
        # stockDist['PriceLn'] = stockDist.apply(lambda row: log(row['Price']),axis=1)
        yearOfData = 0
        maxYear = tckrData.index[maxRow].year
        minYear = min(tckrData.index).year
        for row in range(len(tckrData.index)):
            rDate = tckrData.index[row]
            if rDate.year > yearOfData:
                yearOfData = rDate.year
                print(
                    "Processing data for the year %02i (%01i of %01i)"
                    % (yearOfData, yearOfData - minYear + 1, maxYear - minYear + 1)
                )
            xAxis = None
            yAxis = None
            for span in ["1yr", "6m", "3m", "1m"]:
                if row >= spanToDays[span]:
                    if xAxis and len(xAxis) > spanToDays[span]:
                        dailyChange = dailyChange[-1 * spanToDays[span] :]
                        xAxis = xAxis[-1 * spanToDays[span] :]
                        yAxis = yAxis[-1 * spanToDays[span] :]
                        yAxis_ln = yAxis_ln[-1 * spanToDays[span] :]
                    else:
                        # calculate daily change data for annulized volatility data
                        dailyChange = [np.NaN] * spanToDays[span]
                        for iClose in range(len(dailyChange)):
                            if row - spanToDays[span] + iClose > 0:
                                dailyChange[iClose] = (
                                    tckrData.ix[
                                        row - spanToDays[span] + iClose, "Adj Close"
                                    ]
                                    / tckrData.ix[
                                        row - spanToDays[span] + iClose - 1, "Adj Close"
                                    ]
                                    - 1
                                )
                        # calculate axis for linear reg for timespan
                        xAxis = tckrData.index[row - spanToDays[span] : row].tolist()
                        yAxis = tckrData[row - spanToDays[span] : row][
                            "Adj Close"
                        ].tolist()
                        yAxis_ln = np.log(np.array(yAxis))
                        for iDate in range(len(xAxis)):
                            xAxis[iDate] = int((xAxis[iDate] - rDate).days)

                    # Calculate Annulized Volatility for span
                    annualVolatility = np.sqrt(252) * np.std(dailyChange)
                    # calculate linear regression stats for span
                    (slope, intercept, rsq, tt, stderr) = stats.linregress(xAxis, yAxis)
                    (
                        slope_ln,
                        intercept_ln,
                        rsq_ln,
                        tt_ln,
                        stderr_ln,
                    ) = stats.linregress(xAxis, yAxis_ln)

                    ##                    #find future price
                    ##                    if((row+spanToDays[span])<=maxRow):
                    ##                        futurePrice = tckrData.ix[row+spanToDays[span],'Adj Close']
                    ##                        priceDelta = futurePrice - tckrData.ix[row,'Adj Close']
                    ##                        priceDeltaPercentile = priceDelta/tckrData.ix[row,'Adj Close']
                    ##                    else:
                    ##                        futurePrice = np.nan
                    ##                        priceDelta = np.nan
                    ##                        priceDeltaPercentile = np.nan
                    # record the data
                    stockDist.ix[
                        row,
                        [
                            "AnnualVolatility-%s" % span,
                            "Slope-%s" % span,
                            "SlopeLN-%s" % span,
                            "Rsq-%s" % span,
                            "RsqLN-%s" % span,
                        ],
                    ] = [annualVolatility, slope, slope_ln, rsq, rsq_ln]

        def findLargestRsqSpanAndValue(stockDist, timeSpanList, logScale=False):
            """
            stockDist is the standard historical stock information and must have
                the Rsq columns corresponding to the spans indicated in the timeSpanList
            timeSpanList is a list of spans to be included
            """
            # create the rsqArray from the stockData and the colToSpanDict from the timeSpanList
            colToSpanDict = {}
            if logScale:
                rsqCols = ["RsqLN-"] * len(timeSpanList)
            else:
                rsqCols = ["Rsq-"] * len(timeSpanList)
            for iCol in range(len(timeSpanList)):
                colToSpanDict[iCol] = timeSpanList[iCol]
                rsqCols[iCol] = rsqCols[iCol] + timeSpanList[iCol]
            rsqArray = np.array(stockDist.ix[:, rsqCols])
            maxRsqSpan = np.array(["------"] * rsqArray.shape[0])
            maxRsqValues = np.max(rsqArray, axis=1)
            # go through the list of spans starting with the end (assumed to be the smallest span) and up to the start (assumed to be the largest span)
            for iCol in range(rsqArray.shape[1] - 1, -1, -1):
                maxRsqSpan[rsqArray[:, iCol] == maxRsqValues] = [
                    colToSpanDict[iCol]
                ] * len(maxRsqSpan[rsqArray[:, iCol] == maxRsqValues])
            return maxRsqSpan, maxRsqValues

        def findMinMaxVolatility(variationArray, colToSpanDict):
            """
            stockDist is the standard historical stock information and must have
                the Vol columns corresponding to the spans indicated in the timeSpanList
            timeSpanList is a list of spans to be included
            """
            # create the rsqArray from the stockData and the colToSpanDict from the timeSpanList
            colToSpanDict = {}
            variationCols = ["AnnualVolatility-"] * len(timeSpanList)
            for iCol in range(len(timeSpanList)):
                colToSpanDict[iCol] = timeSpanList[iCol]
                variationCols[iCol] = variationCols[iCol] + timeSpanList[iCol]
            variationArray = np.array(stockDist.ix[:, variationCols])
            maxVariationSpan = np.array(["------"] * variationArray.shape[0])
            minVariationSpan = np.array(["------"] * variationArray.shape[0])
            maxVariationValues = np.max(variationArray, axis=1)
            minVariationValues = np.min(variationArray, axis=1)
            for iCol in range(variationArray.shape[1] - 1, -1, -1):
                maxVariationSpan[variationArray[:, iCol] == maxVariationValues] = [
                    colToSpanDict[iCol]
                ] * len(maxVariationSpan[variationArray[:, iCol] == maxVariationValues])
                minVariationSpan[variationArray[:, iCol] == minVariationValues] = [
                    colToSpanDict[iCol]
                ] * len(minVariationSpan[variationArray[:, iCol] == minVariationValues])
            return (
                maxVariationSpan,
                maxVariationValues,
                minVariationSpan,
                minVariationValues,
            )

        ##colToSpanDict = {}
        ##variationCols = ['AnnualVolatility-']*len(spansInAnalysis)
        ##rsqCols = ['Rsq-']*len(spansInAnalysis)
        ##rsqLNCols = ['RsqLN-']*len(spansInAnalysis)
        ##for iCol in range(len(spansInAnalysis)):
        ##    colToSpanDict[iCol] = spansInAnalysis[iCol]
        ##    variationCols[iCol] = variationCols[iCol]+spansInAnalysis[iCol]
        ##    rsqCols[iCol] = rsqCols[iCol]+spansInAnalysis[iCol]
        ##    rsqLNCols[iCol] = rsqLNCols[iCol]+spansInAnalysis[iCol]

        maxVariationSpan, maxVariationValues, minVariationSpan, minVariationValues = findMinMaxVolatility(
            stockDist, spansInAnalysis
        )
        stockDist["MaxVolatilitySpan"] = maxVariationSpan
        stockDist["MaxVolatilityValue"] = maxVariationValues
        stockDist["MinVolatilitySpan"] = minVariationSpan
        stockDist["MinVolatilityValue"] = minVariationValues

        maxRsqSpan, maxRsqValues = findLargestRsqSpanAndValue(
            stockDist, spansInAnalysis
        )
        stockDist["MaxRsqSpan"] = maxRsqSpan
        stockDist["MaxRsqValue"] = maxRsqValues
        maxRsqSpan, maxRsqValues = findLargestRsqSpanAndValue(
            stockDist, spansInAnalysis
        )
        stockDist["MaxRsqLNSpan"] = maxRsqSpan
        stockDist["MaxRsqLNValue"] = maxRsqValues

        linearFitBetterCnt = len(
            stockDist.ix[stockDist.MaxRsqValue >= stockDist.MaxRsqLNValue, :].index
        )
        totalCnt = len(stockDist.index)
        meanDiff = (stockDist.MaxRsqValue - stockDist.MaxRsqLNValue).mean()
        stDevDiff = (stockDist.MaxRsqValue - stockDist.MaxRsqLNValue).std()

        def XSpanTrendPredictionYIntervalYIntervalAgo(
            stockDist, span, interval, logScale=False
        ):
            """
            uses the linear trend from the span indicated by span and extrapolates the value out 'interval' number of days
            """
            # earliestDate = stockDist.index.min()
            if logScale:
                col_LN_Suffix = "LN"
            else:
                col_LN_Suffix = ""

            startDate = stockDist.index.min() + datetime.timedelta(days=interval)
            endDate = stockDist.index.max()
            originalPrice_Array = np.array(stockDist[startDate:endDate][["Price"]])
            offset = len(stockDist.index) - len(originalPrice_Array)
            predictionArray = np.zeros((len(stockDist.index), 1))
            predictionArray[:offset] = np.nan
            # if span not valid (i.e. None) then use a slope of 0 to indicate that there is no trend
            if span:
                originalSlope_Array = np.array(
                    stockDist[startDate:endDate][["Slope%s-%s" % (col_LN_Suffix, span)]]
                )
                # if the trend is logscaled, then the slope is already int eh correct scale but the price needs to
                # be transformed to log(Price) and then convert the final prediction back to linear
                if logScale:
                    predictionArray[offset:] = np.exp(
                        np.log(originalPrice_Array) + originalSlope_Array * interval
                    )
                else:
                    predictionArray[offset:] = (
                        originalPrice_Array + originalSlope_Array * interval
                    )
            else:
                predictionArray[offset:] = originalPrice_Array

            return predictionArray

        def bestRsqTrendPredictionYIntervalAgo(
            stockDist,
            interval,
            rsqCutOff=0.5,
            logScale=False,
            defaultToLastPrediction=True,
        ):
            """
            if defaultToLastPrediction = True:
                then carry over the last valid prediction
            else:
                assume no change and predict the same price as the start of the interval
            """
            rowsInFirstInterval = (
                stockDist.index
                < (stockDist.index.min() + datetime.timedelta(days=interval))
            ).sum()
            predictionArray = np.zeros((len(stockDist.index), 1))
            for span in spansInAnalysis:
                predictionArray[
                    np.array(stockDist.MaxRsqSpan == span)
                ] = XSpanTrendPredictionYIntervalAgo(
                    stockDist, span, interval, logScale=logScale
                )[
                    np.array(stockDist.MaxRsqSpan == span)
                ]
            predictionArray[np.array(stockDist.MaxRsqValue < rsqCutOff)] = np.nan
            predictionArray[np.isnan(np.array(stockDist.MaxRsqValue))] = np.nan
            previousNaNCount = -1
            runCount = 0
            while (
                previousNaNCount != np.isnan(predictionArray).sum()
                and runCount < rowsInFirstInterval
            ):
                previousNaNCount = np.isnan(predictionArray).sum()
                assignBoolArray = np.append(
                    np.array([[False]]), np.isnan(predictionArray[1:])
                )
                shiftedBoolArray = np.append(
                    np.isnan(predictionArray[(1):]), np.array([[False]])
                )
                predictionArray[assignBoolArray] = predictionArray[shiftedBoolArray]
                if runCount % 5.0 == 0:
                    print(runCount)
                runCount += 1
            # for remaining NAN values, assume no change when possible
            assignBoolArray = np.append(
                np.array([[False] * rowsInFirstInterval]),
                np.isnan(predictionArray[rowsInFirstInterval:]),
            )
            shiftedBoolArray = np.append(
                np.isnan(predictionArray[rowsInFirstInterval:]),
                np.array([[False] * rowsInFirstInterval]),
            )
            predictionArray[assignBoolArray] = np.array(
                stockDist.Price[shiftedBoolArray]
            ).reshape(predictionArray[assignBoolArray].shape)
            return predictionArray

        stockDist["BestRsqTrend1mAgo"] = bestRsqTrendPredictionYIntervalAgo(
            stockDist, 30
        )
        stockDist["BestRsqLNTrend1mAgo"] = bestRsqTrendPredictionYIntervalAgo(
            stockDist, 30, logScale=True
        )

        return stockDist


# tckrData = loadTckrData(tckr)
tckrData = storage.get(tckr)
storage.close()
maxRow = len(tckrData) - 1
# outColumns = ['Stock','Date','Price','Slope-1yr','Intercept-1yr','Rsq-1yr','AnnualVolatility-1yr','FuturePrice-1yr','PriceDelta-1yr','Slope-6m','Intercept-6m','Rsq-6m','AnnualVolatility-6m','FuturePrice-6m','PriceDelta-6m','Slope-3m','Intercept-3m','Rsq-3m','AnnualVolatility-3m','FuturePrice-3m','PriceDelta-3m','Slope-1m','Intercept-1m','Rsq-1m','AnnualVolatility-1m','FuturePrice-1m','PriceDelta-1m','Slope-1wk','Intercept-1wk','Rsq-1wk','AnnualVolatility-1wk','FuturePrice-1wk','PriceDelta-1wk']
outColumns = ["Stock", "Date", "Price"]
for span in spansInAnalysis:
    for dataCategory in ["Slope", "SlopeLN", "Rsq", "RsqLN", "AnnualVolatility"]:
        outColumns.append("%s-%s" % (dataCategory, span))
data = np.empty((maxRow + 1, len(outColumns))) * np.nan
stockDist = pd.DataFrame(data=data, columns=outColumns)
stockDist.ix[:, "Stock"] = tckr
stockDist.Date = tckrData.index
stockDist.Price = tckrData.ix[:, "Adj Close"].tolist()
# stockDist['PriceLn'] = stockDist.apply(lambda row: log(row['Price']),axis=1)
yearOfData = 0
maxYear = tckrData.index[maxRow].year
minYear = min(tckrData.index).year
for row in range(len(tckrData.index)):
    rDate = tckrData.index[row]
    if rDate.year > yearOfData:
        yearOfData = rDate.year
        print(
            "Processing data for the year %02i (%01i of %01i)"
            % (yearOfData, yearOfData - minYear + 1, maxYear - minYear + 1)
        )
    xAxis = None
    yAxis = None
    for span in spansInAnalysis:
        if row >= spanToDays[span]:
            if xAxis and len(xAxis) > spanToDays[span]:
                dailyChange = dailyChange[-1 * spanToDays[span] :]
                xAxis = xAxis[-1 * spanToDays[span] :]
                yAxis = yAxis[-1 * spanToDays[span] :]
                yAxis_ln = yAxis_ln[-1 * spanToDays[span] :]
            else:
                # calculate daily change data for annulized volatility data
                dailyChange = [np.NaN] * spanToDays[span]
                for iClose in range(len(dailyChange)):
                    if row - spanToDays[span] + iClose > 0:
                        dailyChange[iClose] = (
                            tckrData.ix[row - spanToDays[span] + iClose, "Adj Close"]
                            / tckrData.ix[
                                row - spanToDays[span] + iClose - 1, "Adj Close"
                            ]
                            - 1
                        )
                # calculate axis for linear reg for timespan
                xAxis = tckrData.index[row - spanToDays[span] : row].tolist()
                yAxis = tckrData[row - spanToDays[span] : row]["Adj Close"].tolist()
                yAxis_ln = np.log(np.array(yAxis))
                for iDate in range(len(xAxis)):
                    xAxis[iDate] = int((xAxis[iDate] - rDate).days)

            # Calculate Annulized Volatility for span
            annualVolatility = np.sqrt(252) * np.std(dailyChange)
            # calculate linear regression stats for span
            (slope, intercept, rsq, tt, stderr) = stats.linregress(xAxis, yAxis)
            (slope_ln, intercept_ln, rsq_ln, tt_ln, stderr_ln) = stats.linregress(
                xAxis, yAxis_ln
            )

            ##                    #find future price
            ##                    if((row+spanToDays[span])<=maxRow):
            ##                        futurePrice = tckrData.ix[row+spanToDays[span],'Adj Close']
            ##                        priceDelta = futurePrice - tckrData.ix[row,'Adj Close']
            ##                        priceDeltaPercentile = priceDelta/tckrData.ix[row,'Adj Close']
            ##                    else:
            ##                        futurePrice = np.nan
            ##                        priceDelta = np.nan
            ##                        priceDeltaPercentile = np.nan
            # record the data
            stockDist.ix[
                row,
                [
                    "AnnualVolatility-%s" % span,
                    "Slope-%s" % span,
                    "SlopeLN-%s" % span,
                    "Rsq-%s" % span,
                    "RsqLN-%s" % span,
                ],
            ] = [annualVolatility, slope, slope_ln, rsq, rsq_ln]


def findLargestRsqSpanAndValue(stockDist, timeSpanList, logScale=False):
    """
    stockDist is the standard historical stock information and must have
        the Rsq columns corresponding to the spans indicated in the timeSpanList
    timeSpanList is a list of spans to be included
    """
    # create the rsqArray from the stockData and the colToSpanDict from the timeSpanList
    colToSpanDict = {}
    if logScale:
        rsqCols = ["RsqLN-"] * len(timeSpanList)
    else:
        rsqCols = ["Rsq-"] * len(timeSpanList)
    for iCol in range(len(timeSpanList)):
        colToSpanDict[iCol] = timeSpanList[iCol]
        rsqCols[iCol] = rsqCols[iCol] + timeSpanList[iCol]
    rsqArray = np.array(stockDist.ix[:, rsqCols])
    rsqArray[np.isnan(rsqArray)] = -99
    maxRsqSpan = np.array(["------"] * rsqArray.shape[0])
    maxRsqValues = np.max(rsqArray, axis=1)
    maxRsqValues[maxRsqValues < -98] = np.nan
    # go through the list of spans starting with the end (assumed to be the smallest span) and up to the start (assumed to be the largest span)
    for iCol in range(rsqArray.shape[1] - 1, -1, -1):
        maxRsqSpan[rsqArray[:, iCol] == maxRsqValues] = [colToSpanDict[iCol]] * len(
            maxRsqSpan[rsqArray[:, iCol] == maxRsqValues]
        )
    return maxRsqSpan, maxRsqValues


def findMinMaxVolatility(variationArray, colToSpanDict):
    """
    stockDist is the standard historical stock information and must have
        the Vol columns corresponding to the spans indicated in the timeSpanList
    timeSpanList is a list of spans to be included
    """
    # create the rsqArray from the stockData and the colToSpanDict from the timeSpanList
    colToSpanDict = {}
    variationCols = ["AnnualVolatility-"] * len(timeSpanList)
    for iCol in range(len(timeSpanList)):
        colToSpanDict[iCol] = timeSpanList[iCol]
        variationCols[iCol] = variationCols[iCol] + timeSpanList[iCol]
    variationArray = np.array(stockDist.ix[:, variationCols])
    maxVariationSpan = np.array(["------"] * variationArray.shape[0])
    minVariationSpan = np.array(["------"] * variationArray.shape[0])
    maxVariationValues = np.max(variationArray, axis=1)
    minVariationValues = np.min(variationArray, axis=1)
    for iCol in range(variationArray.shape[1] - 1, -1, -1):
        maxVariationSpan[variationArray[:, iCol] == maxVariationValues] = [
            colToSpanDict[iCol]
        ] * len(maxVariationSpan[variationArray[:, iCol] == maxVariationValues])
        minVariationSpan[variationArray[:, iCol] == minVariationValues] = [
            colToSpanDict[iCol]
        ] * len(minVariationSpan[variationArray[:, iCol] == minVariationValues])
    return maxVariationSpan, maxVariationValues, minVariationSpan, minVariationValues


##colToSpanDict = {}
##variationCols = ['AnnualVolatility-']*len(spansInAnalysis)
##rsqCols = ['Rsq-']*len(spansInAnalysis)
##rsqLNCols = ['RsqLN-']*len(spansInAnalysis)
##for iCol in range(len(spansInAnalysis)):
##    colToSpanDict[iCol] = spansInAnalysis[iCol]
##    variationCols[iCol] = variationCols[iCol]+spansInAnalysis[iCol]
##    rsqCols[iCol] = rsqCols[iCol]+spansInAnalysis[iCol]
##    rsqLNCols[iCol] = rsqLNCols[iCol]+spansInAnalysis[iCol]

maxVariationSpan, maxVariationValues, minVariationSpan, minVariationValues = findMinMaxVolatility(
    stockDist, spansInAnalysis
)
stockDist["MaxVolatilitySpan"] = maxVariationSpan
stockDist["MaxVolatilityValue"] = maxVariationValues
stockDist["MinVolatilitySpan"] = minVariationSpan
stockDist["MinVolatilityValue"] = minVariationValues

maxRsqSpan, maxRsqValues = findLargestRsqSpanAndValue(stockDist, spansInAnalysis)
stockDist["MaxRsqSpan"] = maxRsqSpan
stockDist["MaxRsqValue"] = maxRsqValues
maxRsqSpan, maxRsqValues = findLargestRsqSpanAndValue(stockDist, spansInAnalysis)
stockDist["MaxRsqLNSpan"] = maxRsqSpan
stockDist["MaxRsqLNValue"] = maxRsqValues

linearFitBetterCnt = len(
    stockDist.ix[stockDist.MaxRsqValue >= stockDist.MaxRsqLNValue, :].index
)
totalCnt = len(stockDist.index)
rsqDiff = stockDist.MaxRsqValue - stockDist.MaxRsqLNValue
meanDiff = (stockDist.MaxRsqValue - stockDist.MaxRsqLNValue).mean()
stDevDiff = (stockDist.MaxRsqValue - stockDist.MaxRsqLNValue).std()

stockDist.to_csv(os.path.join(default_settings.data_dir, "SPYHistAndFits_20170703.csv"))

stockDist = pd.read_csv(
    os.path.join(default_settings.data_dir, "SPYHistAndFits_20170703.csv"),
    index_col=0,
    parse_dates=True,
    infer_datetime_format=True,
)

# set up for a 2 week and a 1 month stock prediction
def XSpanTrendPredictionYIntervalAgo(stockDist, span, interval, logScale=False):
    """
Debug Code
interval = 30
span = '1m'
col_LN_Suffix = ''
startDate = stockDist.index.min() + datetime.timedelta(days=interval)
endDate = stockDist.index.max()
originalPrice_Array = np.array(stockDist[startDate:endDate].Price)
originalSlope_Array = np.array(stockDist[startDate:endDate][['Slope%s-%s' % (col_LN_Suffix,span)]])
    """
    # earliestDate = stockDist.index.min()
    if logScale:
        col_LN_Suffix = "LN"
    else:
        col_LN_Suffix = ""

    startDate = stockDist.index.min() + datetime.timedelta(days=interval)
    endDate = stockDist.index.max()
    originalPrice_Array = np.array(stockDist[startDate:endDate][["Price"]])
    offset = len(stockDist.index) - len(originalPrice_Array)
    predictionArray = np.zeros((len(stockDist.index), 1))
    predictionArray[:offset] = np.nan
    # if span not valid (i.e. None) then use a slope of 0 to indicate that there is no trend
    if span:
        originalSlope_Array = np.array(
            stockDist[startDate:endDate][["Slope%s-%s" % (col_LN_Suffix, span)]]
        )
        # if the trend is logscaled, then the slope is already int eh correct scale but the price needs to
        # be transformed to log(Price) and then convert the final prediction back to linear
        if logScale:
            predictionArray[offset:] = np.exp(
                np.log(originalPrice_Array) + originalSlope_Array * interval
            )
        else:
            predictionArray[offset:] = (
                originalPrice_Array + originalSlope_Array * interval
            )
    else:
        predictionArray[offset:] = originalPrice_Array

    return predictionArray


def bestRsqTrendPredictionYIntervalAgo(
    stockDist,
    timeSpans,
    interval,
    rsqCutOff=0.5,
    logScale=False,
    defaultToLastPrediction=True,
):
    """
    if defaultToLastPrediction = True:
        then carry over the last valid prediction
    else:
        assume no change and predict the same price as the start of the interval
    """
    rowsInFirstInterval = (
        stockDist.index < (stockDist.index.min() + datetime.timedelta(days=interval))
    ).sum()
    predictionArray = np.zeros((len(stockDist.index), 1))
    for span in timeSpans:
        predictionArray[
            np.array(stockDist.MaxRsqSpan == span)
        ] = XSpanTrendPredictionYIntervalAgo(
            stockDist, span, interval, logScale=logScale
        )[
            np.array(stockDist.MaxRsqSpan == span)
        ]
    predictionArray[np.array(stockDist.MaxRsqValue < rsqCutOff)] = np.nan
    predictionArray[np.isnan(np.array(stockDist.MaxRsqValue))] = np.nan
    previousNaNCount = -1
    runCount = 0
    while (
        previousNaNCount != np.isnan(predictionArray).sum()
        and runCount < rowsInFirstInterval
    ):
        previousNaNCount = np.isnan(predictionArray).sum()
        assignBoolArray = np.append(np.array([[False]]), np.isnan(predictionArray[1:]))
        shiftedBoolArray = np.append(
            np.isnan(predictionArray[(1):]), np.array([[False]])
        )
        predictionArray[assignBoolArray] = predictionArray[shiftedBoolArray]
        if runCount % 5.0 == 0:
            print(runCount)
        runCount += 1
    # for remaining NAN values, assume no change when possible
    assignBoolArray = np.append(
        np.array([[False] * rowsInFirstInterval]),
        np.isnan(predictionArray[rowsInFirstInterval:]),
    )
    shiftedBoolArray = np.append(
        np.isnan(predictionArray[rowsInFirstInterval:]),
        np.array([[False] * rowsInFirstInterval]),
    )
    predictionArray[assignBoolArray] = np.array(
        stockDist.Price[shiftedBoolArray]
    ).reshape(predictionArray[assignBoolArray].shape)
    return predictionArray


stockDist["BestRsqTrend1mAgo"] = bestRsqTrendPredictionYIntervalAgo(
    stockDist, ["1m"], 30
)
stockDist["BestRsqLNTrend1mAgo"] = bestRsqTrendPredictionYIntervalAgo(
    stockDist, ["1m"], 30, logScale=True
)
stockDist["BestRsqTrend2wAgo"] = bestRsqTrendPredictionYIntervalAgo(
    stockDist, ["2w"], 14
)
stockDist["BestRsqLNTrend2wAgo"] = bestRsqTrendPredictionYIntervalAgo(
    stockDist, ["2w"], 14, logScale=True
)

stockDist.to_csv(os.path.join(default_settings.data_dir, "SPYHistAndFits_20170703.csv"))

# start working the trend of best 5,10 fit for 2w and 1m predictions
r"""debug code
import pandas as pd
import pandas_datareader.data as web
import requests
import numpy as np
import datetime
import os
from scipy import stats #, linspace, polyval, polyfit, sqrt, randn
from math import log
from matplotlib import pyplot as plt
spanToDays = {'1yr':252,'6m':126,'3m':63,'1m':21,'1wk':5}
spanToProjectedDays = {'1yr':365,'6m':180,'3m':90,'1m':30,'1wk':7}
spansInAnalysis = ['1yr','6m','3m','1m']
stockDist = pd.read_csv(
    os.path.join(default_settings.data_dir, "SPYHistAndFits_20170703.csv"),
    index_col=0,
    parse_dates=True,
    infer_datetime_format=True
)"""


def calculateXDayRSME(prediction, actual, interval):
    actual = actual.reshape(prediction.shape)
    mse = np.zeros(prediction.shape)
    mse[:interval] = np.nan
    meanPrice = np.zeros(prediction.shape)
    for iDay in range(interval):
        shiftedActual = actual[(interval - (1 + iDay)) : (-1 - iDay)]
        shiftedPrediction = prediction[(interval - (1 + iDay)) : (-1 - iDay)]
        mse[interval:] = mse[interval:] + (shiftedPrediction - shiftedActual) ** 2
        meanPrice[interval:] = meanPrice[interval:] + shiftedActual
    # normalized RMSE, or coefficient of variation for the RMSE, is Sqrt(Sum(MSE) over interval)/average price of the interval
    meanPrice /= np.float(interval)
    rmse = (mse / np.float(interval)) ** 0.5 / meanPrice
    return rmse


def findBestXDayTrendYDaysAgo(
    stockDist,
    testInterval,
    predictionInterval,
    timeSpans,
    rmseCutoff=0.02,
    logScale=False,
):
    commonShape = (len(stockDist.index), 1)
    lowestRMSE = np.array([100.0] * len(stockDist.index)).reshape(commonShape)
    lowestRMSESpan = np.array(["------"] * len(lowestRMSE)).reshape(commonShape)
    lowestRMSEPrediction = np.zeros(commonShape)
    actual = np.array(stockDist.Price).reshape(commonShape)
    # allSpanArrays[row,span]
    colToSpanDict = {}
    for iSpan in range(len(timeSpans)):
        colToSpanDict[iSpan] = timeSpans[iSpan]
        predictionArray = XSpanTrendPredictionYIntervalAgo(
            stockDist, timeSpans[iSpan], predictionInterval, logScale=logScale
        ).reshape(commonShape)
        spanRMSE = calculateXDayRSME(predictionArray, actual, testInterval)
        changeBool = spanRMSE < lowestRMSE
        lowestRMSE[changeBool] = spanRMSE[changeBool]
        lowestRMSESpan[changeBool] = timeSpans[iSpan]
        lowestRMSEPrediction[changeBool] = predictionArray[changeBool]
    # set the prediction to the original price if the RMSE is above the cutoff
    changeBool = lowestRMSE > rmseCutoff
    rowsInFirstInterval = (
        stockDist.index
        < stockDist.index.min() + datetime.timedelta(days=predictionInterval)
    ).sum()
    shiftedBoolArray = np.append(
        changeBool[rowsInFirstInterval:], np.array([[False]] * rowsInFirstInterval)
    ).reshape((len(stockDist.index),))
    previousPrice = np.array(stockDist.Price[shiftedBoolArray])
    # lowestRMSE[changeBool] = np.nan
    # lowestRMSESpan[changeBool] = 'None'
    assignBoolArray = np.append(
        np.array([[False]] * rowsInFirstInterval), changeBool[rowsInFirstInterval:]
    ).reshape(commonShape)
    lowestRMSEPrediction[assignBoolArray] = previousPrice
    return lowestRMSE, lowestRMSESpan, lowestRMSEPrediction


def calculatePearsonGoF(actual, prediction):
    return (((actual - prediction) ** 2) / prediction).sum()


def calculateLiklihoodRatio(actual, prediction):
    return 2 * ((actual * (np.log(actual / prediction))).sum())


def cycleThroughBestRsqPredictionParameters(
    stockDist,
    rsqCutoff=None,
    trendSpansLargestToSmallest=None,
    predictionInterval=None,
    logScale=None,
):
    # columns are (date,price)+volatility for each trendSpan+prediction for each parameter combination
    if rsqCutoff is None:
        rsqCutoff = [0.2, 0.4, 0.5, 0.6, 0.8, 0.9]
    if trendSpansLargestToSmallest is None:
        trendSpansLargestToSmallest = [
            "1yr",
            "9m",
            "6m",
            "3m",
            "2m",
            "1m",
            "2wk",
            "1wk",
        ]
    if predictionInterval is None:
        predictionInterval = [7, 14, 30, 60, 90]
    if logScale is None:
        logScale = [False, True]
    predictionQty = (
        len(rsqCutoff)
        * len(trendSpansLargestToSmallest)
        * len(predictionInterval)
        * len(logScale)
    )
    allPredictions = (
        np.empty(
            (
                len(stockDist.index),
                (2 + len(trendSpansLargestToSmallest) + predictionQty),
            )
        )
        * np.nan
    )
    colToRsqCutoff = {}
    colToSmallestSpanDict = {}
    colToPredictionInterval = {}
    colToScaleDict = {}
    colNum = 2 + len(trendSpansLargestToSmallest)
    colNames = ["-"] * allPredictions.shape[1]
    for iSpan in trendSpansLargestToSmallest:
        if iSpan == 0:
            trendSpan = trendSpansLargestToSmallest
            smallestSpan = trendSpan[len(trendSpan) - 1]
        else:
            trendSpan = trendSpan[:-1]
            smallestSpan = trendSpan[len(trendSpan) - 1]
        for interval in predictionInterval:
            for cutOff in rsqCutoff:
                for scale in logScale:
                    if scale:
                        axisScale = "LN"
                    else:
                        axisScale = "Linear"
                    allPredictions[:, colNum] = findBestXDayTrendYDaysAgo(
                        stockDist,
                        interval,
                        interpretSpanToProjectedDays(span),
                        trendSpan,
                        rmseCutoff=cutOff,
                        logScale=scale,
                    )
                    colToPredictionInterval[colNum] = interval
                    colToRsqCutoff[colNum] = cutOff
                    colToSmallestSpanDict[colNum] = smallestSpan
                    colToScaleDict[colNum] = axisScale
                    # col_name in the format -  'BestRsq_intervalDays_axisScale_smallestSpan_cutOff_
                    colNames[colNum] = "BestRsq_%01iDays_%s_%s_%0.2f" % (
                        interval,
                        axisScale,
                        smallestSpan,
                        cutOff,
                    )
                    if colNum - 2 + len(trendSpansLargestToSmallest) % 10 == 0:
                        print(
                            "Finished calculating %01i of %01i predictions"
                            % (
                                colNum - 2 + len(trendSpansLargestToSmallest),
                                predictionQty,
                            )
                        )
                    colNum += 1

    # assign known columns at the end
    allPredictions[:, 0] = range(len(stockDist.index))
    colNames[0] = "Date"
    allPredictions[:, 1] = stockDist.Price
    colNames[1] = "Price"
    for iSpan in range(len(trendSpansLargestToSmallest)):
        allPredictions[:, iSpan + 2] = stockDist.ix[
            :, "AnnualVolatility-%s" % trendSpansLargestToSmallest[iSpan]
        ]
        colNames[iSpan + 2] = "AnnualVolatility-%s" % trendSpansLargestToSmallest[iSpan]

    predictionDF = pd.DataFrame(data=allPredictions, index=0, columns=colNames)
    predictionDF.index = stockDist.index
    # save prediction dataframe
    prediction_fPath = os.path.join(
        default_settings.data_dir, "SPYBestRsqTrendFits_20170703.csv"
    )
    predictionDF.to_csv(prediction_fPath)
    del allPredictions
    minYear = min(predictionDF.index)
    maxYear = max(predictionDF.index)
    years = range(minYear, maxYear + 1, 1)

    # process the fit of each year of data for each prediction
    colNames_GoF = [
        "PredictionInterval",
        "AxisScale",
        "SmallestSpan",
        "RsqCutOff",
        "Year",
    ]
    goodnessOfFitData = np.empty((predictionQty, len(colNames_GoF))) * np.nan
    print("\nFinished calculating the predictions... now processing them")
    rowGoF = 0
    for iCol in range(2 + len(trendSpansLargestToSmallest), len(predictionDF.columns)):
        for year in years:
            selectRows = np.logical_and(
                predictionDF.index >= datetime.date(year, 1, 1),
                predictionDF.index < datetime.date(year + 1, 1, 1),
            )
            pearsonGoF = calculatePearsonGoF(
                np.array(predictionDF.ix[selectRows, "Price"]),
                np.array(predictionDF.ix[selectRows, iCol]),
            )
            likelhoodGoF = calculateLiklihoodRatio(
                np.array(predictionDF.ix[selectRows, "Price"]),
                np.array(predictionDF.ix[selectRows, iCol]),
            )
            goodnessOfFitData[rowGoF, :] = [
                colToPredictionInterval[iCol],
                colToScaleDict[iCol],
                colToSmallestSpanDict[iCol],
                colToRsqCutoff[iCol],
                year,
                pearsonGoF,
                liklihoodGof,
            ]
            if rowGoF % 10.0 == 0:
                print(
                    "Finished Processing %01i of %01i predictions"
                    % (rowGoF, predictionQty)
                )
            rowGoF += 1

    processedDF = pd.DataFrame(data=goodnessOfFitData, columns=colNames_GoF)
    # save Prediction processing results
    processed_fPath = os.path.join(
        default_settings.data_dir, "SPYBestRsqTrendGoodnessOfFitByYear_20170703.csv"
    )
    processedDF.to_csv(prediction_fPath)
    del goodnessOfFitData

    return prediction_fPath, processed_fPath


# def calculatePredictionFitByYear(actual,prediciton):
