# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 12:52:02 2017

@author: a0225347
"""
from MarketAnalysis import (
    MarketBackTest,
    MarketData_Depreciated,
    MarketPortfolio_Depreciated,
    OptionPositions,
)
import src.Utility as MH
from src.Strategies import RebalanceStock, RollingSPXOption
import pandas as pd, numpy as np, datetime, os
import ffn

ffn.extend_pandas()
from matplotlib import pyplot as plt

reload(MarketBackTest)
reload(MarketData_Depreciated)
reload(MarketPortfolio_Depreciated)
reload(MH)
reload(RebalanceStock)
reload(RollingSPXOption)
reload(OptionPositions)

ixSPX, SPY = False, True


def runConditions(ixSPX, SPY):
    # startDate=None
    indexOptions = []
    if ixSPX:
        indexOptions.append("ixSPX")
    if SPY:
        indexOptions.append("SPY")
    sDB, oDB, lDB, iDB = MarketData_Depreciated.initiateAllDataBases(workProxy=True)
    self = bt = MarketBackTest.Backtest(
        workProxy=sDB.workProxyBool, stockDB=sDB, optionDB=oDB, liborDB=lDB
    )
    startDate = None
    endDate = None
    initialValue = 100000
    reInvestDiv = True
    brokerageTradeFee = 4.95
    additionalOptionContractFee = 0.15
    width = 50
    fileDir = None
    debug = False
    printStatusBool = True
    minimizePrint = False

    optionType = "Call"
    purchaseSpan = "6m"
    rollAfterSpan = "3m"
    targetStrikeToPriceRatio = 0.8
    maxPfRisk = 0.40
    additionalConditions = []
    positionQty = 4
    targetStockPerc = {"AGG": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {}
    rebalanceThreshold = 0.02
    indexOptions = indexOptions
    pfName = None
    #    optionType= 'Call'
    #    purchaseSpan = '2y'
    #    rollAfterSpan = '1y'
    #    targetStrikeToPriceRatio = 0.8
    #    maxPfRisk = 0.2
    #    additionalConditions=[]
    #    positionQty=4
    #    targetStockPerc = {'AGG':1}
    #    rebalanceFreq = '5y'
    #    defaultTckrs = {}
    #    rebalanceThreshold = 0.02
    #    indexOptions = ['SPY']
    #    pfName=None
    strategy = RollingSPXOption.RollingSPXOption(
        optionType,
        purchaseSpan,
        rollAfterSpan,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        additionalConditions=additionalConditions,
        positionQty=positionQty,
        pfName=pfName,
        indexOptions=indexOptions,
    )
    strategyArray = [strategy]

    # strategyArray = [];strategyArray.append(RollingSPXOption.RollingSPXCalls());strategy = strategyArray[0]
    if startDate is None:
        startDate = self.simulationStartDate
    else:
        startDate = MH.formatDateInput(startDate)

    if endDate is None:
        endDate = MH.formatDateInput(datetime.date.today())
    else:
        endDate = MH.formatDateInput(endDate)

    startDate, endDate = strategy.initilizeStrategy(
        self.stockDB, self.optionDB, self.liborDB, startDate=startDate, endDate=endDate
    )
    if not minimizePrint:
        print(startDate, endDate)

    if fileDir is None:
        fileDir = os.path.join(self.mainDir, "PortfolioResults")

    pfName = strategy.getPortfolioName()

    pf = MarketPortfolio_Depreciated.Portfolio(
        initialValue=initialValue,
        reInvestDiv=reInvestDiv,
        pfName=pfName,
        startDate=startDate,
        fileDir=fileDir,
        optionDB=self.optionDB,
        stockDB=self.stockDB,
        brokerageTradeFee=brokerageTradeFee,
        additionalOptionContractFee=additionalOptionContractFee,
        silent=False,
        source="temp_db",
    )

    currentDate = startDate
    eventFlags = ["Start"]
    width = 50
    valueAfterProcessing = pf.calculateValue()
    while currentDate < endDate:
        valueBeforeAdvance = valueAfterProcessing
        sectionTitle = (
            " Advance To "
            + currentDate.strftime("%Y-%m-%d")
            + " - Cmds: "
            + str(eventFlags)
        )
        if not minimizePrint:
            print("\n" + "*" * width)
            padding = max((width - len(sectionTitle)) / 2, 0)
            print("*" * padding + sectionTitle + "*" * padding)
            print("*" * width)
        else:
            print(sectionTitle)
        pf.advanceToDate(currentDate)
        valueAfterAdvance = pf.calculateValue()
        print(
            "Value changed from $%01.2f to $%01.2f"
            % (valueBeforeAdvance, valueAfterAdvance)
        )
        pf.brokerageTradeFee = self.cpiDB.inflationAdjustedValue(
            currentDate, pf.brokerageTradeFee
        )
        pf.additionalOptionContractFee = self.cpiDB.inflationAdjustedValue(
            currentDate, pf.additionalOptionContractFee
        )
        print("Start Processing for " + str(currentDate))
        eventDate, eventFlags = strategy.process(
            currentDate, eventFlags, pf, self.stockDB, self.optionDB, self.liborDB
        )
        valueAfterProcessing = pf.calculateValue()
        print(
            "Value changed from $%01.2f to $%01.2f because of processing"
            % (valueAfterAdvance, valueAfterProcessing)
        )

        # print("pf.valueHistory: "+str(pf.valueHistory.loc[max(pf.valueHistory.loc[pf.currentDay-pd.Timedelta(days=10):pf.currentDay - pd.Timedelta(days=1),:].index),:]))
        print("pf.cash: " + str(pf.cash))
        print("pf.stockValues: " + str(pf.stockValues))
        print("pf.optionValues: " + str(pf.optionValues))
        print("TotalValue: " + str(pf.calculateValue()))

        assert eventDate >= currentDate, (
            "MarketBacktest.RunStrategy: The next eventDate has been set in the past. \nCurrentDate: %s   eventDate: %s"
            % (str(currentDate), str(eventDate))
        )
        if printStatusBool and not minimizePrint:
            pf.printStatus(statusTitle="Post Processing")
        currentDate = min(endDate, eventDate)
    pf.advanceToDate(endDate)

    totalValue = pf.valueHistory.TotalValue.astype(float)
    statObject = ffn.PerformanceStats(totalValue[np.isfinite(totalValue)], rf=0.0)
    return statObject, pf.valueHistory


spyStats_6m, spypf_6m = runConditions(False, True)
ixSPXStats_6m, ixSPXpf_6m = runConditions(True, False)
ixSPX_SPY_Stats_6m, ixSPX_SPY_pf_6m = runConditions(True, True)

totalRecords_6m = pd.concat(
    [
        ixSPXpf_6m.TotalValue.astype(float),
        spypf_6m.TotalValue.astype(float),
        ixSPX_SPY_pf_6m.TotalValue.astype(float),
    ],
    axis=1,
)
totalRecords_6m.columns = ["ixSPX_6m", "SPY_6m", "ixSPX_SPY_6m"]
totalRecords_6m = totalRecords_6m.loc[np.isfinite(totalRecords_6m.iloc[:, 0]), :]
totalStats = ffn.GroupStats(totalRecords_6m)
totalStats.display()
