# -------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      a0225347
#
# Created:     13/07/2017
# Copyright:   (c) a0225347 2017
# Licence:     <your licence>
# -------------------------------------------------------------------------------
#!/usr/bin/env python
import time, datetime
import pandas as pd
import os

# import pandas_datareader.data as web
import numpy as np
import requests
from StringIO import StringIO
from selenium import webdriver
from lxml import html
from config import default_settings


class FidelitySandbox:
    def __init__(
        self,
        fileDir=None,
        workProxy=False,
        proxies=None,
        cookieValue=None,
        crumbValue=None,
        webDriver=None,
    ):
        if fileDir is None:
            fileDir = default_settings.root_dir

        if proxies is None:
            proxies = {}
        self.mainDir = fileDir
        # initialize the data connection parameters
        self.workProxyBool = workProxy
        if workProxy and not len(proxies):
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        else:
            self.proxies = proxies
        self.session = requests.session()
        self.session.proxies = self.proxies
        if not cookieValue:
            cookieValue = "b7thnptblu4mf&b=3&s=v2"
        if not crumbValue:
            crumbValue = "7i9IPsqlgF6"
        self.cookieValue = cookieValue
        self.crumbValue = crumbValue
        self.chromedriver = (
            r"C:\Users\a0225347\Documents\Python\SeleniumWebDrivers\chromedriver.exe"
        )
        self.phantomJSDriver = r"C:\Users\a0225347\Documents\Python\SeleniumWebDrivers\phantomjs-2.1.1-windows\bin\phantomjs.exe"
        self.finalContextColumns = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "UnderlyingPrice",
            "Type",
            "Strike",
        ]
        self.finalDataColumns = [
            "Symbol",
            "Last",
            "Change",
            "Bid Size",
            "Bid",
            "Ask",
            "Ask Size",
            "Volume",
            "Open Int",
            "Imp Vol",
            "Delta",
            "Gamma",
            "Theta",
            "Vega",
            "Rho",
        ]
        self.hd5QueryCols = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "Type",
            "Strike",
            "Delta",
        ]
        self.finalColumns = self.finalContextColumns + self.finalDataColumns
        self.finalDtypes = [
            "datetime64[ns]",
            "datetime64[ns]",
            np.int,
            np.object,
            np.float,
            np.object,
            np.float,
            np.object,
        ] + [np.float] * 14
        self.dataNameToColNumDict = dict(
            map(
                lambda dataName: (dataName, self.finalColumns.index(dataName)),
                self.finalColumns,
            )
        )
        self.monthAbbrToNumConvertion = {
            "Jan": 1,
            "Feb": 2,
            "Mar": 3,
            "Apr": 4,
            "May": 5,
            "Jun": 6,
            "Jul": 7,
            "Aug": 8,
            "Sep": 9,
            "Oct": 10,
            "Nov": 11,
            "Dec": 12,
        }
        # inititialize the database
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", "DailyOptionQuotesFidelity.h5")
        )
        self.tckrList = self.storage.keys()
        self.storage.close()
        self.driver = webDriver

    def mineFidelityOptions(self, symbol, loadInBatches=True, debug=False):
        symbol = symbol.upper()
        storageSymbol = symbol.replace(".", "^")
        optionData_Total = pd.DataFrame(columns=self.finalColumns)
        optionURL = (
            "https://researchtools.fidelity.com/ftgw/mloptions/goto/optionChain?symbol=%s"
            % symbol
        )
        if not self.driver:
            if debug:
                self.driver = driver = webdriver.Chrome(
                    executable_path=self.chromedriver
                )
            else:
                self.driver = driver = webdriver.PhantomJS(
                    executable_path=self.phantomJSDriver,
                    service_log_path=os.path.devnull,
                )
                driver.set_window_size(1120, 550)
        else:
            driver = self.driver
        driver.get(optionURL)
        print(driver.title + ": " + symbol)
        # check for a login link and click on the login link if it exists
        loginLink = driver.find_elements_by_xpath('//*[@id="chainPeak"]')
        if len(loginLink) > 0:
            print("\tNeed to Login")
            loginLink[0].click()
            # enter username on new page
            driver.find_elements_by_xpath('//*[@id="userId-input"]')[0].send_keys(
                "coryperk"
            )
            driver.find_elements_by_xpath('//*[@id="password"]')[0].send_keys(
                "Hebrews135"
            )
            # press the submit button
            driver.find_elements_by_xpath('//*[@id="fs-login-button"]')[0].click()

        # get the price of the underlying symbol and the time of the query
        queryTimeStamp = datetime.datetime.today()
        print("\tCollecting Underlying Price")
        underlyingPrice = float(
            driver.find_elements_by_xpath('//*[@id="companyDetailsDiv"]/div/span[1]')[0]
            .text.replace("$", "")
            .replace(",", "")
        )
        print("\tLogin Successful")

        # ensure that all dates are unselected
        applyButtonElement = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
        exDateBarElement = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
        )
        selectedElements = exDateBarElement.find_elements_by_class_name(
            "selected-state"
        )
        if len(selectedElements) > 0:
            for expirationElement in selectedElements:
                expirationElement.click()
            # click on the apply settings button
            driver.find_element_by_xpath('//*[@id="Bttn_Apply"]').click()
        # get the total number of expiration dates
        # exDateElementList = driver.find_elements_by_xpath("//*[@id=\"OptionSearchForm\"]/div[2]/div[1]/div[2]/div/ul/li")
        exDateQty = len(
            driver.find_elements_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul/li'
            )
        )
        # cycle through experation dates one at a time and harvest each set of data
        for iExDate in range(exDateQty):
            # select the current expiration date and apply the settings
            driver.find_element_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul/li[%01i]'
                % (iExDate + 1)
            ).click()
            driver.find_element_by_xpath('//*[@id="Bttn_Apply"]').click()

            # Initialize column headers and dataFrames
            if iExDate == 0:
                # get table headers by looping through the TH values until one doesn't exist
                print("\tMining Column Names")
                colHeaderNames = []
                iCol = 1
                colHeaderElementList = driver.find_elements_by_xpath(
                    '//*[@id="fullTable"]/thead[1]/tr/th'
                )
                for iCol in range(len(colHeaderElementList)):
                    colHeaderNames.append(colHeaderElementList[iCol].text)
                callToPutTransistion = colHeaderNames.index("Strike")

            # get the expiration date info from the first expiration date
            exDateInfo = (
                driver.find_elements_by_class_name("mth0")[0]
                .get_attribute("name")
                .split(" ")
            )
            exDate = datetime.date(
                int("20" + exDateInfo[2][1:]),
                self.monthAbbrToNumConvertion[exDateInfo[0]],
                int(exDateInfo[1]),
            )
            if "(" in exDateInfo[3]:
                daysToExpiration = int(exDateInfo[3][(exDateInfo[3].find("(") + 1) :])
            else:
                daysToExpiration = int(exDateInfo[4][(exDateInfo[4].find("(") + 1) :])
            # get number of rows in table for the current exDate
            rowQty = driver.execute_script(
                "return document.getElementById(\"fullTable\").getElementsByClassName('mth0').length"
            )
            # init new dataFrame for the expiration date
            optionData = pd.DataFrame(
                index=range(rowQty * 2), columns=self.finalColumns
            )
            # line prefix is = ['QueryDate','QueryTime','ExDate','DaysToExpiration','UnderlyingSymbol','UnderlyingPrice','Type','Strike']
            optionData["QueryTimeStamp"] = np.array(
                [np.datetime64(queryTimeStamp)] * (rowQty * 2)
            ).astype("datetime64[ns]")
            optionData["ExDate"] = np.array(
                [np.datetime64(exDate)] * (rowQty * 2)
            ).astype("datetime64[ns]")
            optionData["DaysToExpiration"] = np.array(
                [daysToExpiration] * (rowQty * 2)
            ).astype(float)
            optionData["UnderlyingSymbol"] = np.array([symbol] * (rowQty * 2)).astype(
                str
            )
            optionData["UnderlyingPrice"] = np.array(
                [float(str(underlyingPrice).replace("$", "").replace(",", ""))]
                * (rowQty * 2)
            ).astype(float)
            setCallBoolList = [True] * rowQty + [False] * rowQty
            setPutBoolList = [False] * rowQty + [True] * rowQty
            optionData["Type"] = np.array(
                ["Call"] * (rowQty) + ["Put"] * (rowQty)
            ).astype(str)

            print(
                "\tMining option data with an experation data of %s (%01i rows)"
                % (str(exDate), rowQty)
            )
            optionData = self.processSingleExDateOptionData_XMLParser(
                driver,
                optionData,
                colHeaderNames,
                callToPutTransistion,
                setCallBoolList,
                setPutBoolList,
                debug=debug,
            )

            if loadInBatches:
                # if load in batches is true, then append each expiration date data to stored dataset and create a dataset if necessary
                self.storage.open()
                if storageSymbol in self.storage.keys():
                    self.storage.append(
                        storageSymbol,
                        optionData,
                        format="table",
                        data_columns=self.hd5QueryCols,
                    )
                else:
                    self.storage.put(
                        storageSymbol,
                        optionData,
                        format="table",
                        data_columns=self.hd5QueryCols,
                    )
                self.storage.close()
            else:
                # if not then append it to the default total dataframe and load it to the database at the very end
                optionData_Total = optionData_Total.append(optionData)

            # unselect the current expiration date
            driver.find_element_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul/li[%01i]'
                % (iExDate + 1)
            ).click()

        # if load in batches is false, then we need to load the entire dataset at this point and then close the webdriver
        if not loadInBatches:
            self.storage.open()
            if storageSymbol in self.storage.keys():
                self.storage.append(
                    storageSymbol,
                    optionData_Total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            else:
                self.storage.put(
                    storageSymbol,
                    optionData_Total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            self.storage.close()
        else:
            optionData_Total = self.loadFromDB(
                storageSymbol,
                query_FromDate=queryTimeStamp,
                query_ToDate=queryTimeStamp,
            )
        return optionData_Total

    def closeDriver(self):
        return self.driver.quit()

    @staticmethod
    def processSingleExDateOptionData_XMLParser(
        driver,
        optionData,
        tableHeaderNames,
        callToPutTransistionIndex,
        setCallBoolList,
        setPutBoolList,
        debug=False,
    ):
        tableTree = html.fromstring(
            driver.find_element_by_xpath('//*[@id="fullTable"]').get_attribute(
                "innerHTML"
            )
        )
        targetRows = len(optionData.index) / 2
        for iCol in range(len(tableHeaderNames)):
            dataName = tableHeaderNames[iCol]
            # if(True):
            # print("\t\tProcessing column %01i: '%s'" % (iCol,dataName))
            # print("Option Data Length - %01i" % len(optionData.index))

            # different columns have different structurs that need to be navigated past the td html tag
            # those that end in '/a' will also pick up a dummy value from the subheader of the table and so
            # firstValidRow needs to be moved to 1 for only those columns
            xPathSuffix = ""
            if dataName in ["Symbol", "Bid", "Ask"]:
                xPathSuffix = "/a"
            elif dataName in ["Volume", "Open Int"]:
                xPathSuffix = "/div/span"

            # dtype determiniation
            datatype = str
            if dataName in [
                "Strike",
                "Last",
                "Change",
                "Bid",
                "Ask",
                "Imp Vol",
                "Delta",
                "Gamma",
                "Theta",
                "Vega",
                "Rho",
            ]:
                datatype = float
            elif dataName in ["Bid Size", "Ask Size", "Volume", "Open Int"]:
                datatype = int

            # start processing data

            # no valid data in action cell
            if dataName == "Action":
                continue

            # get data as txt from column
            colTextList = np.array(
                map(
                    lambda x: x.replace("$", "").replace(",", ""),
                    tableTree.xpath(
                        "//tbody/tr/td[%01i]%s/text()" % (iCol + 1, xPathSuffix)
                    ),
                ),
                dtype=str,
            )
            firstValidRow = len(colTextList) - targetRows
            colTextList = colTextList[firstValidRow:]
            if dataName == "Strike":
                # Add the strik to the call and put columns along with the other information at the start of the line
                optionData.ix[setCallBoolList, dataName] = colTextList.astype(float)
                optionData.ix[setPutBoolList, dataName] = colTextList.astype(float)
            elif dataName in ["Imp Vol"]:
                # remove the percent sign and return a float
                colTextList = np.array(map(lambda x: x[:-2], colTextList), dtype=str)
                colTextList[colTextList == ""] = np.nan
                if iCol < callToPutTransistionIndex:
                    optionData.ix[setCallBoolList, dataName] = colTextList.astype(float)
                else:
                    optionData.ix[setPutBoolList, dataName] = colTextList.astype(float)
            else:
                # add the data in the predetermined format such that empty cells are nan values for numeric columns
                if datatype in [int, float]:
                    colTextList[colTextList == ""] = np.nan
                if iCol < callToPutTransistionIndex:
                    optionData.ix[setCallBoolList, dataName] = colTextList.astype(
                        datatype
                    )
                else:
                    optionData.ix[setPutBoolList, dataName] = colTextList.astype(
                        datatype
                    )

        # need to convert the numeric columns back to numeric dtypes
        optionData[
            [
                "Strike",
                "Last",
                "Bid",
                "Ask",
                "Change",
                "Bid Size",
                "Ask Size",
                "Volume",
                "Open Int",
                "Imp Vol",
                "Delta",
                "Gamma",
                "Theta",
                "Vega",
                "Rho",
            ]
        ] = optionData[
            [
                "Strike",
                "Last",
                "Bid",
                "Ask",
                "Change",
                "Bid Size",
                "Ask Size",
                "Volume",
                "Open Int",
                "Imp Vol",
                "Delta",
                "Gamma",
                "Theta",
                "Vega",
                "Rho",
            ]
        ].apply(
            pd.to_numeric
        )
        return optionData

    def loadFromDB(
        self,
        underlyingSymbol,
        query_FromDate=None,
        query_ToDate=None,
        strike_Low=None,
        strik_High=None,
        cNames=None,
    ):
        underlyingSymbol = underlyingSymbol.upper().replace(".", "^")
        if query_FromDate:
            query_FromDate = self.formatDateInput(query_FromDate)
        if query_ToDate:
            query_ToDate = self.formatDateInput(query_ToDate)

        whereStr = ""
        if query_FromDate == query_ToDate:
            query_FromDate = datetime.datetime(
                query_FromDate.year, query_FromDate.month, query_FromDate.day
            )
            query_ToDate = datetime.datetime(
                query_FromDate.year, query_FromDate.month, query_FromDate.day
            ) + datetime.timedelta(days=1)
            whereStr = (
                whereStr
                + "QueryTimeStamp>=query_FromDate & QueryTimeStamp<=query_ToDate"
            )
        elif query_FromDate:
            if query_ToDate:
                whereStr = (
                    whereStr
                    + "QueryTimeStamp>=query_FromDate & QueryTimeStamp<=query_ToDate"
                )
            else:
                whereStr += "QueryTimeStamp>=query_FromDate"
        else:
            if query_ToDate:
                whereStr += "QueryTimeStamp<=query_ToDate"

        if cNames is not None:
            if type(cNames) != type([]):
                cNames = [cNames]
            if whereStr == "":
                whereStr += "columns=cNames"
            else:
                whereStr += " & columns=cNames"

        self.storage.open()
        if whereStr == "":
            tckrData = self.storage.get(underlyingSymbol)
        else:
            tckrData = self.storage.select(underlyingSymbol, whereStr)
        self.storage.close()
        return tckrData

    @staticmethod
    def formatDateInput(date):
        if type(date) == str:
            if "/" in date:
                sep = "/"
            elif "-" in date:
                sep = "-"
            elif "." in date:
                sep = "."
            else:
                sep = " "
            month, day, year = date.split(sep)
            return datetime.datetime(int(year), int(month), int(day))
        else:
            return date


##def processSingleExDateOptionData(self,driver,optionData,tableHeaderNames,callToPutTransistionIndex,setCallBoolList,setPutBoolList,debug = 0):
##        for iCol in range(len(tableHeaderNames)):
##            dataName = tableHeaderNames[iCol]
##            #if(True):
##                #print("\t\tProcessing '%s' column" % (dataName))
##                #print("Option Data Length - %01i" % len(optionData.index))
##            #different columns have different structurs that need to be navigated past the td html tag
##            xPathSuffix = ''
##            if(dataName in ['Symbol','Bid','Ask']):
##                xPathSuffix = '/a'
##            elif(dataName in ['Volume','Open Int']):
##                xPathSuffix = '/div/span'
##
##            #the experiation date summary column has 3 columns so we need a first valid row variable to distinguish between
##            #columns that have an extra empty value at the start of the list
##            if(iCol<3):
##                firstValidRow = 1
##            else:
##                firstValidRow = 0
##
##            #start processing data
##            if(dataName == 'Action'):
##            #no valid data in action cell
##                continue
##            elif(dataName == 'Strike'):
##            #Add the strik to the call and put columns along with the other information at the start of the line
##                optionData.ix[setCallBoolList,dataName] = np.array(map(lambda x: float(x.text.replace('$','').replace(',','')),driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ),dtype = float)
##                optionData.ix[setPutBoolList,dataName] = np.array(map(lambda x: float(x.text.replace('$','').replace(',','')),driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ),dtype = float)
##            elif(dataName in ['Last','Bid','Ask','Change','Bid Size','Ask Size','Volume','Open Int','Delta','Gamma','Theta','Vega','Rho']):
##            #process as a float
##                colTextList = np.array(map(lambda x: float(x.text.replace('$','').replace(',','')),driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ), dtype = str)
##                colTextList[colTextList==''] = np.nan
##                if(iCol<callToPutTransistionIndex):
##                    optionData.ix[setCallBoolList,dataName] = colTextList.astype(float)
##                else:
##                    optionData.ix[setPutBoolList,dataName] = colTextList.astype(float)
##            elif(dataName in ['Imp Vol']):
##            # remove the percent sign and return a float
##                colTextList = np.array(map(lambda x: x.text[:-2].replace('$','').replace(',',''),driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ),dtype = str)
##                colTextList[colTextList==''] = np.nan
##                if(iCol<callToPutTransistionIndex):
##                    optionData.ix[setCallBoolList,dataName] = colTextList.astype(float)
##                else:
##                    optionData.ix[setPutBoolList,dataName] = colTextList.astype(float)
##            else:
##            # else just add a string representation of the object
##                if(iCol<callToPutTransistionIndex):
##                    optionData.ix[setCallBoolList,dataName] = np.array(map(lambda x: x.text,driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ),dtype = str)
##                else:
##                    optionData.ix[setPutBoolList,dataName] = np.array(map(lambda x: x.text,driver.find_elements_by_xpath("//*[@id=\"fullTable\"]/tbody/tr/td[%01i]%s" % (iCol+1,xPathSuffix))[firstValidRow:] ), dtype = str)
##
##        #need to convert the numeric columns back to numeric dtypes
##        optionData[['Strike','Last','Bid','Ask','Change','Bid Size','Ask Size','Volume','Open Int','Imp Vol','Delta','Gamma','Theta','Vega','Rho']] = optionData[['Strike','Last','Bid','Ask','Change','Bid Size','Ask Size','Volume','Open Int','Imp Vol','Delta','Gamma','Theta','Vega','Rho']].apply(pd.to_numeric)
##        return optionData
