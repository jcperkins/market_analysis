# Copyright (c) 2011, Mark Chenoweth
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
#   disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from typing import Union
import urllib2, time, datetime
import pandas as pd
import numpy as np


class Quote(object):

    DATE_FMT = "%Y-%m-%d"
    TIME_FMT = "%H:%M:%S"

    def __init__(self):
        self.symbol = ""
        self.date, self.time, self.open_, self.high, self.low, self.close, self.volume = (
            [] for _ in range(7)
        )

    def append(self, dt, open_, high, low, close, volume):
        self.date.append(dt.date())
        self.time.append(dt.time())
        self.open_.append(float(open_))
        self.high.append(float(high))
        self.low.append(float(low))
        self.close.append(float(close))
        self.volume.append(int(volume))

    def to_csv(self):
        return "".join(
            [
                "{0},{1},{2},{3:.2f},{4:.2f},{5:.2f},{6:.2f},{7}\n".format(
                    self.symbol,
                    self.date[bar].strftime("%Y-%m-%d"),
                    self.time[bar].strftime("%H:%M:%S"),
                    self.open_[bar],
                    self.high[bar],
                    self.low[bar],
                    self.close[bar],
                    self.volume[bar],
                )
                for bar in range(len(self.close))
            ]
        )

    def write_csv(self, filename):
        with open(filename, "w") as f:
            f.write(self.to_csv())

    def read_csv(self, filename):
        self.symbol = ""
        self.date, self.time, self.open_, self.high, self.low, self.close, self.volume = (
            [] for _ in range(7)
        )
        for line in open(filename, "r"):
            symbol, ds, ts, open_, high, low, close, volume = line.rstrip().split(",")
            self.symbol = symbol
            dt = datetime.datetime.strptime(
                ds + " " + ts, self.DATE_FMT + " " + self.TIME_FMT
            )
            self.append(dt, open_, high, low, close, volume)
        return True

    def __repr__(self):
        return self.to_csv()


class YahooQuote(Quote):
    """ Daily quotes from Yahoo. Date format='yyyy-mm-dd' """

    def __init__(
        self,
        symbol,
        start_date,
        end_date=datetime.date.today().isoformat(),
        workProxy=True,
    ):
        super(YahooQuote, self).__init__()
        self.symbol = symbol.upper()
        start_year, start_month, start_day = start_date.split("-")
        start_month = str(int(start_month) - 1)
        end_year, end_month, end_day = end_date.split("-")
        end_month = str(int(end_month) - 1)
        url_string = "http://ichart.finance.yahoo.com/table.csv?s={0}".format(symbol)
        url_string += "&a={0}&b={1}&c={2}".format(start_month, start_day, start_year)
        url_string += "&d={0}&e={1}&f={2}".format(end_month, end_day, end_year)
        if workProxy:
            proxy_handler = urllib2.ProxyHandler(
                {"http": "http://webproxy.ext.ti.com:80/"}
            )
            proxy_auth_handler = urllib2.ProxyBasicAuthHandler()
            opener = urllib2.build_opener(proxy_handler, proxy_auth_handler)
            csv = opener.open(url_string).readlines()
        else:
            csv = urllib.urlopen(url_string).readlines()
        csv.reverse()
        for bar in range(0, len(csv) - 1):
            ds, open_, high, low, close, volume, adjc = csv[bar].rstrip().split(",")
            open_, high, low, close, adjc = [
                float(x) for x in [open_, high, low, close, adjc]
            ]
            if close != adjc:
                factor = adjc / close
                open_, high, low, close = [
                    x * factor for x in [open_, high, low, close]
                ]
            dt = datetime.datetime.strptime(ds, "%Y-%m-%d")
            self.append(dt, open_, high, low, close, volume)


class YahooHistoricalQuote:
    """ Daily quotes from Yahoo. Date format='yyyy-mm-dd'
  use chrome developer tools to discover a cookie/crumb combination that will work"""

    def __init__(
        self,
        symbol,
        start_date,
        end_date=datetime.datetime.today(),
        workProxy=True,
        proxies={},
        cookieValue=None,
        crumbValue=None,
    ):
        super(YahooQuote, self).__init__()
        self.workProxyBool = workProxy
        if workProxy and not len(proxies):
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        else:
            self.proxies = proxies
        self.session = requests.session()
        self.session.proxies = self.proxies
        if not cookieValue:
            cookieValue = "b7thnptblu4mf&b=3&s=v2"
        if not crumbValue:
            crumbValue = "7i9IPsqlgF6"
        self.symbol = symbol.upper()
        # date processing
        if type(start_date) == str:
            year, month, day = start_date.split("-")
            start_date = datetime.datetime(year, month, year)
        start_year = start_date.year
        start_month = start_date.month
        start_day = start_date.day
        self.start_date = start_date
        if type(end_date) == str:
            year, month, day = end_date.split("-")
            end_date = datetime.datetime(year, month, day)
        end_year = end_date.year
        end_month = end_date.month
        end_day = end_date.day
        self.end_date = end_date
        start_date_timestamp = int(
            (start_date - datetime.datetime(1970, 1, 1)).total_seconds()
        )
        end_date_timestamp = int(
            (end_date - datetime.datetime(1970, 1, 1)).total_seconds()
        )
        cookies = {B: cookieValue}
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download/%s?period1=%10i&period2=%10i&interval=1d&events=history&crumb=%s"
            % (self.symbol, start_date_timestamp, end_date_timestamp, crumbValue)
        )
        response = self.session.get(url_string, cookies=cookies)
        self.data = pd.read_csv(StringIO.StringIO(response.content), index_col=0)

        def getData(self):
            return self.data


class Options(object):
    def __init__(
        self,
        symbol,
        yahooGenericURLPrefix="http://query1.finance.yahoo.com/v7/finance/options/",
        yahooGenericURLSuffix="?formatted=true&crumb=7i9IPsqlgF6&lang=en-US&region=US&corsDomain=finance.yahoo.com",
        workProxy=True,
    ):
        self.symbol = symbol.upper()
        self.workProxy = workProxy
        self.yahooOptionGenericURL = (
            yahooGenericURLPrefix + self.symbol + yahooGenericURLSuffix
        )
        option = self.getAllData(self.yahooOptionGenericURL)
        expirations = option["optionChain"]["result"][0]["expirationDates"]
        self.expirationDates = {}
        for unicodeDate in expirations:
            self.expirationDates[unicodeDate] = datetime.datetime.fromtimestamp(
                unicodeDate
            )
        self.allDataColumns = [
            "Strike",
            "Expiry",
            "Type",
            "Symbol",
            "Last",
            "Bid",
            "Ask",
            "Chg",
            "PctChng",
            "LastDate",
            "Volume",
            "ImpliedVolatility",
            "OpenInterest",
            "InTheMoney",
        ]
        self.allData = None

    def queryYahooOptionData(self, url_string):
        if self.workProxy:
            proxy_handler = urllib2.ProxyHandler(
                {"http": "http://webproxy.ext.ti.com:80/"}
            )
            proxy_auth_handler = urllib2.ProxyBasicAuthHandler()
            opener = urllib2.build_opener(proxy_handler, proxy_auth_handler)
            option = opener.open(url_string).readlines()[0]
        else:
            option = urllib.urlopen(url_string).readlines()[0]
        option = option.replace("true", "True")
        option = option.replace("false", "False")
        option = option.replace("null", "None")
        option = eval(option)
        return option

    def getAllData(self):
        for expiry in self.expirationDates.keys():
            url_string = (
                "http://query1.finance.yahoo.com/v7/finance/options/%s?formatted=true&crumb=7i9IPsqlgF6&lang=en-US&region=US&straddle=false&date=%s&corsDomain=finance.yahoo.com"
                % (self.symbol, str(expiry))
            )
            option = self.queryYahooOptionData(url_string)
            if self.allData:
                self.allData = self.allData.append(self.processRawOptionData(option))
            else:
                self.allData = self.processRawOptionData(option)
        return self.allData

    def processRawOptionData(self, option):
        """
            depreciated due to discovery of how to use the pandas options class within the work proxy
        """
        records = option["optionChain"]["result"][0]["options"][0]["puts"][0].keys()
        data = np.zeros((len(records) * 2, len(self.allDataColumns)))
        for iRecord in range(len(records)):
            call = option["optionChain"]["result"][0]["options"][0]["calls"][0]
            put = option["optionChain"]["result"][0]["options"][0]["puts"][0]
            # allDataColumns = {'Strike':['strike']['raw'],'Expiry':['expiration']['fmt'],'Type':'Put'/'Call','Symbol':['contractSymbol'],'Last':['lastPrice']['raw'],'Bid':['bid']['raw'],'Ask':['ask']['raw'],'Chg':['change']['raw'],'PctChng':['percentChange']['raw']/100,'LastDate':['lastTradeDate']['fmt'],'Volume':['volume']['raw'],'ImpliedVolatility':['impliedVolatility']['raw'],'OpenInterest':['openInterest']['raw'],'InTheMoney':['inTheMoney']}
            # recordAccess = [
            data[iRecord * 2] = [
                put["strike"]["raw"],
                put["expiration"]["fmt"],
                "Put",
                put["contractSymbol"],
                put["lastPrice"]["raw"],
                put["bid"]["raw"],
                put["ask"]["raw"],
                put["change"]["raw"],
                put["percentChange"]["raw"] / 100,
                put["lastTradeDate"]["fmt"],
                put["volume"]["raw"],
                put["impliedVolatility"]["raw"],
                put["openInterest"]["raw"],
                put["inTheMoney"],
            ]
            data[iRecord * 2 + 1] = [
                call["strike"]["raw"],
                call["expiration"]["fmt"],
                "call",
                call["contractSymbol"],
                call["lastPrice"]["raw"],
                call["bid"]["raw"],
                call["ask"]["raw"],
                call["change"]["raw"],
                call["percentChange"]["raw"] / 100,
                call["lastTradeDate"]["fmt"],
                call["volume"]["raw"],
                call["impliedVolatility"]["raw"],
                call["openInterest"]["raw"],
                call["inTheMoney"],
            ]

    def storeOptionQuote(self):
        pass


if __name__ == "__main__":
    q: Union[YahooQuote, Quote] = YahooQuote(
        "aapl", "2011-01-01"
    )  # download year to date Apple data
    print(q)  # print it out
    q = YahooQuote(
        "orcl", "2011-02-01", "2011-02-28"
    )  # download Oracle data for February 2011
    q.write_csv("orcl.csv")  # save it to disk
    q = Quote()  # create a generic quote object
    q.read_csv("orcl.csv")  # populate it with our previously saved data
    print(q)  # print it out


##Option data is formated:
##Option['optionChain']['result'] -> list of 1 dictionary
##    keys in diction:
##        'underlyingSymbol' -> 'SPY'
##        'expirationDates' -> List of unicode dates (access with datetime.datetime.fromtimestamp('unix timestamp'))
##        'strikes' -> List of strike prices
##        'hasMiniOptions', -> True/False
##        'quote' -> Dictionary with keys:
##             'quoteType',
##             'quoteSourceName',
##             'longName',
##             'messageBoardId',
##             'exchangeTimezoneName',
##             'fiftyTwoWeekLowChange',
##             'fiftyTwoWeekLowChangePercent',
##             'gmtOffSetMilliseconds',
##             'fiftyTwoWeekHighChange',
##             'regularMarketVolume',
##             'regularMarketTime',
##             'regularMarketPrice',
##             'financialCurrency',
##             'fiftyDayAverage',
##             'regularMarketDayLow',
##             'fiftyTwoWeekLow',
##             'bid',
##             'regularMarketChangePercent',
##             'sharesOutstanding',
##             'averageDailyVolume3Month',
##             'trailingThreeMonthNavReturns',
##             'twoHundredDayAverageChangePercent',
##             'averageDailyVolume10Day',
##             'twoHundredDayAverage',
##             'marketState',
##             'regularMarketOpen',
##             'twoHundredDayAverageChange',
##             'priceHint',
##             'fiftyTwoWeekHighChangePercent',
##             'currency',
##             'exchange',
##             'fiftyDayAverageChangePercent',
##             'fullExchangeName',
##             'exchangeTimezoneShortName',
##             'market',
##             'sourceInterval',
##             'fiftyTwoWeekHigh',
##             'regularMarketDayHigh',
##             'askSize',
##             'symbol',
##             'trailingThreeMonthReturns',
##             'fiftyDayAverageChange',
##             'ask',
##             'bidSize',
##             'shortName',
##             'regularMarketPreviousClose',
##             'regularMarketChange',
##             'marketCap'
##        'options' -> list with 1 dictionary with keys:
##            'puts' - list of dictionaries for each strike with keys:
##                 'lastTradeDate',
##                 'contractSize',
##                 'lastPrice',
##                 'contractSymbol',
##                 'bid',
##                 'inTheMoney',
##                 'volume',
##                 'ask',
##                 'percentChange',
##                 'impliedVolatility',
##                 'change',
##                 'currency',
##                 'expiration',
##                 'strike',
##                 'openInterest'
##            'expirationDate' - unicode timestamp
##            'calls' -> same as puts
##            'hasMiniOptions' -> boolean

##Getting the Option data URL and result
##  1. Go to yahoo finance with the google chrome browser
##  2. Input the symbol you want to test for and the select the options tab
##  3. Turn on the developer tools
##      a. Options (3 vertically stacked dots in the upper right corner of the browser and under the window close X box)
##      b. Options -> More Tools -> Developer Tools
##  4. In the developer tools, make sure you are working in the network tab and then under the filter toolbar select XHR
##  5. Select a new date in the option expiration drop down menu
##  6. Find the record in the developer tool that corresponds to the fetched options data and copy the url
