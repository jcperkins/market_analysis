from __future__ import annotations

from typing import Optional, Union, overload

import numpy as np
import pandas as pd
import src.Utility as Util
from src.types import RawDateSingle, RawDateType, RawTimeDeltaSingle, RawDateCollection

from ..ApiClasses import PredictVolAPI


# noinspection PyUnresolvedReferences
class Volatility30DayMixin(PredictVolAPI):
    @overload
    def predict_volatility(
        self,
        from_dates: RawDateSingle,
        time_delta: RawTimeDeltaSingle,
        unit="Percent",
        predicted_mean: Optional[Union[float, pd.Series]] = None,
    ) -> float:
        ...

    @overload
    def predict_volatility(
        self,
        from_dates: RawDateCollection,
        time_delta: RawTimeDeltaSingle,
        unit="Percent",
        predicted_mean: Optional[Union[float, pd.Series]] = None,
    ) -> pd.Series:
        ...

    def predict_volatility(
        self,
        from_dates: RawDateType,
        time_delta: RawTimeDeltaSingle,
        unit="Percent",
        predicted_mean: Optional[Union[float, pd.Series]] = None,
    ) -> Union[float, pd.Series]:
        """
        returns a 'X-day' prediction (i.e. 7-day, 30-day, 90-day, etc.) for each date
        provided in `from_dates`.

        defaults to the 30d annual volatility adjusted to represent
        the expected variation around the mean at the predicted_date

        args:
            from_dates  -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta  -   a pd.TimeDelta object to predict
            unit        -   'PERCENT' if the volatility should be as a percent of the mean or
                            'DOLLAR' if it should be in volatility as +/- dollars
            predicted_mean- can be supplied to provide custom estimates for the mean or to
            prevent calculating the mean twice

        returns:
            the predicted volatility (+/- std) as a:
                float if current_date is a timestamp and time_delta is a Timedelta instance
                series if dates is a DatetimeIndex object with the name set to the 'x-Days'
                where x is the number of days in time_delta
        """
        from_dates = Util.formatDateInput(from_dates)
        if from_dates is pd.NaT or from_dates is None:
            return np.nan
        if not isinstance(time_delta, pd.Timedelta):
            time_delta = pd.Timedelta(time_delta, unit="days")

        volatility_ratio = Util.project_data_to_dates(
            Util.annualized_volatility(self.data, time_delta.days)
        ) * np.sqrt(time_delta / pd.Timedelta(days=365))
        if isinstance(volatility_ratio, pd.Series):
            volatility_ratio.name = time_delta
        if unit.upper() == "PERCENT":
            return volatility_ratio
        else:
            if predicted_mean is None:
                predicted_mean = self.predict_mean(
                    from_dates=from_dates, time_delta=time_delta
                )
            return volatility_ratio * predicted_mean
