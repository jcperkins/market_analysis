from __future__ import annotations

from abc import abstractmethod, ABCMeta, ABC
from typing import Union, Optional, Tuple, List, Dict, Callable

import numpy as np
import pandas as pd
import src.Utility as Util
from scipy import stats
from src.types import (
    RawDateSingle,
    RawDateCollection,
    RawDateType,
    RawTimeDeltaSingle,
    RawTimeDeltaCollection,
    TorArray,
)

TPredictionShape = Tuple[List[float], Dict[str, float]]


class PredictMeanAPI(metaclass=ABCMeta):
    data: pd.Series

    @abstractmethod
    def predict_mean(
        self, from_dates: RawDateType, time_delta: RawTimeDeltaSingle
    ) -> Union[float, pd.Series]:
        """
        returns a 'X-day' prediction (i.e. 7-day, 30-day, 90-day, etc.) for each date
        provided in `from_dates`.

        This base model implementation defaults to the current value but this function
        is expected to be overridden in modules that inherit from the base. If
        there is more than one timedelta, then it will return a dataframe (one column for each
        time_delta)

        args:
            from_dates  -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta  -   a pd.TimedeltaIndex object of intervals to predict

        returns:
            the predicted mean as a:
                float if current_date is a timestamp and time_delta is a Timedelta instance
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object with the name of the series set to the 'x-Days'
                    where x is the number of days in time_delta
        """
        pass

    def populate_mean_df(
        self,
        date_index: RawDateCollection,
        days_out: Optional[RawTimeDeltaCollection] = None,
    ) -> pd.DataFrame:
        """
        return a prediction dataframe for each date in the index with a column
        for each interval with the column name formatted as '%1i-Day'
        """
        date_index = Util.formatDateArray(date_index)
        if days_out is None:
            days_out = pd.TimedeltaIndex(
                [7, 14, 30, 60, 180, 365, 730, 1095, 1825], unit="days"
            ).sort_values()
        else:
            days_out = Util.format_timedeltaindex(days_out, unit='days').sort_values()
        return pd.concat(
            [self.predict_mean(date_index, td) for td in days_out],
            axis=1,
            ignore_index=False,
        )


class PredictVolAPI(metaclass=ABCMeta):
    data: pd.Series

    @abstractmethod
    def predict_volatility(
        self,
        from_dates: RawDateType,
        time_delta: RawTimeDeltaSingle,
        unit="Percent",
        predicted_mean: Optional[Union[float, pd.Series]] = None,
    ) -> Union[float, pd.Series]:
        """
        returns a 'X-day' prediction (i.e. 7-day, 30-day, 90-day, etc.) for each date
        provided in `from_dates`.

        defaults to the 30d annual volatility adjusted to represent
        the expected variation around the mean at the predicted_date

        args:
            from_dates  -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta  -   a pd.TimeDelta object to predict
            unit        -   'PERCENT' if the volatility should be as a percent of the mean or
                            'DOLLAR' if it should be in volatility as +/- dollars
            predicted_mean- can be supplied to provide custom estimates for the mean or to
            prevent calculating the mean twice

        returns:
            the predicted volatility (+/- std) as a:
                float if current_date is a timestamp and time_delta is a Timedelta instance
                series if dates is a DatetimeIndex object with the name set to the 'x-Days'
                where x is the number of days in time_delta
        """
        pass

    def populate_volatility_df(
        self,
        date_index: RawDateCollection,
        days_out: Optional[RawTimeDeltaCollection] = None,
        unit="Percent",
        predicted_mean: Optional[pd.DataFrame] = None,
    ) -> pd.DataFrame:
        """
        return a prediction dataframe for each date in the index with a column
        for each interval with the column name formatted as '%1i-Day'

        :param date_index: the dates that will make up the index of the df
        :param days_out: the qty of days out that the model will predict (prediction_date - from_date)
        :param unit: 'percent' or 'dollar'
        :param predicted_mean: optional dataframe formatted like the result of populate_mean_df() to use
        as custom predictions of the mean
        :return: a dataframe of predicted volatility's
        """
        date_index = Util.formatDateArray(date_index)
        if days_out is None:
            days_out = pd.TimedeltaIndex(
                [7, 14, 30, 60, 180, 365, 730, 1095, 1825], unit="days"
            ).sort_values()
        else:
            days_out = Util.format_timedeltaindex(days_out).sort_values()
        if predicted_mean is None:
            return pd.concat(
                [self.predict_volatility(date_index, td, unit=unit) for td in days_out],
                axis=1,
                ignore_index=False,
            )
        else:
            return pd.concat(
                [
                    self.predict_volatility(
                        date_index,
                        td,
                        unit=unit,
                        predicted_mean=predicted_mean[f"{td.days}-Day"],
                    )
                    for td in days_out
                ],
                axis=1,
                ignore_index=False,
            )


class GenericDistributionModel(metaclass=ABCMeta):
    dist = stats.rv_continuous

    @abstractmethod
    def get_prediction_shape(
        self, current_date: RawDateSingle, predicted_date: RawDateSingle
    ) -> TPredictionShape:
        """
        returns the information needed to build a continuous random variable with the scipy.stats module
         to create a predicted distribution of the price.

        :param current_date: the maximum date allowed to use int he prediction calculation
        :param predicted_date: the date to predict the price
        :return: location, scale, []
        """
        pass

    # noinspection PyUnresolvedReferences
    def get_frozen_dist_with_dates(
        self, current_date: RawDateSingle, predicted_date: RawDateSingle
    ) -> stats.rv_continuous:
        """
        returns a scipy.stats.norm random variable for the prediction of the provided dates

        :param current_date: the maximum date that the model is allowed to use in it's predictions
        :param predicted_date: the future date that the model is trying to get a distribution
        :return:
        """
        return self.get_prob_dist_with_dates(
            self.get_prediction_shape(current_date, predicted_date)
        )

    def _get_frozen_dist_with_shape(
        self, dist_shape: TPredictionShape
    ) -> stats.rv_continuous:
        """
        returns a scipy.stats random variable with the shape returned from a previously called
        get_prediction_shape() fn

        :param dist_shape: object returned by get_prediction_shape() that tells scipy.stats how to
        build the distribution
        :return:
        """
        return self.dist(*dist_shape[0], **dist_shape[1])

    def get_cum_prob_from_price(
        self,
        price: TorArray[float],
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        direction: str = "Below",
    ) -> TorArray[float]:
        """
        returns the predicted probability that the price will be above or
        below the price targets provided in the variable 'prices'. The
        variable 'direction' determines if the probability is the probability
        of the future price being above the target price or below the
        target price.
        """
        return self._get_cum_prob_from_price_with_shape(
            price,
            self.get_prediction_shape(current_date, predicted_date),
            direction=direction,
        )

    def _get_cum_prob_from_price_with_shape(
        self,
        price: TorArray[float],
        dist_shape: TPredictionShape,
        direction: str = "Below",
    ) -> TorArray[float]:
        """
        returns the predicted probability that the price will be above or
        below the price targets provided in the variable 'prices'. The
        variable 'direction' determines if the probability is the probability
        of the future price being above the target price or below the
        target price.

        requires a distribution shape tuple from a previously called get_prediction_shape() fn to be supplied

        """
        args, kwargs = dist_shape
        probability = self.dist.cdf(price, *args, **kwargs)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        return probability

    @staticmethod
    def _get_cum_prob_from_price_with_frozen_rv(
        price: TorArray[float], frozen_rv: stats.rv_continuous, direction: str = "Below"
    ) -> TorArray[float]:
        """
        returns the predicted probability that the price will be above or
        below the price targets provided in the variable 'prices'. The
        variable 'direction' determines if the probability is the probability
        of the future price being above the target price or below the
        target price.

        requires a scipy.stats frozen random variable from a previously called get_frozen_dist_with_dates() fn to
        be supplied

        """
        probability = frozen_rv.cdf(price)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        return probability

    def get_price_from_cum_prob(
        self,
        probability: TorArray[float],
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        direction="Below",
    ) -> TorArray[float]:
        """
        The percent point function is the inverse of the cdf function and
        so this is the inverse of the 'getProbFromPrices function
          o  direction indicates whether the probabilities reflect the
             probability of prices being above or below the returned price
        """
        return self._get_price_from_cum_prob_with_shape(
            probability,
            self.get_prediction_shape(current_date, predicted_date),
            direction=direction,
        )

    def _get_price_from_cum_prob_with_shape(
        self, probability, dist_shape: TPredictionShape, direction="Below"
    ) -> TorArray[float]:
        """
        The percent point function is the inverse of the cdf function and
        so this is the inverse of the 'getProbFromPrices function
          o  direction indicates whether the probabilities reflect the
             probability of prices being above or below the returned price
        """
        probability = np.array(probability)
        args, kwargs = dist_shape
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        price = self.dist.ppf(probability, *args, **kwargs)
        return price

    @staticmethod
    def _get_price_from_cum_prob_with_frozen_rv(
        probability, frozen_rv: stats.rv_continuous, direction="Below"
    ) -> TorArray[float]:
        """
        The percent point function is the inverse of the cdf function and
        so this is the inverse of the 'getProbFromPrices function
          o  direction indicates whether the probabilities reflect the
             probability of prices being above or below the returned price
        """
        probability = np.array(probability)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        price = frozen_rv.ppf(probability)
        return price

    def get_prob_of_price(
        self,
        price: TorArray[float],
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
    ) -> TorArray[float]:
        """returns the probability density function value of the price in the price prediction for `predicted_date`"""
        return self._get_prob_of_price_with_shape(
            price, self.get_prediction_shape(current_date, predicted_date)
        )

    def _get_prob_of_price_with_shape(
        self, price: TorArray[float], dist_shape: TPredictionShape
    ) -> TorArray[float]:
        """
        returns the probability density function value of the price in the price prediction

        requires that the shape of the prediction has been previously calculated and passed as `dist_shape`
        """
        return self.dist.pdf(price, *dist_shape[0], **dist_shape[1])

    @staticmethod
    def _get_prob_of_price_with_frozen_rv(
        price: TorArray[float], frozen_rv: stats.rv_continuous
    ) -> TorArray[float]:
        """
        returns the probability density function value of the price in the price prediction

        requires that the prediction has been previously calculated as a frozen distribution and passed as `frozen_rv
        """
        return frozen_rv.pdf(price)

    def expected_price_change(
        self,
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        current_price: Optional[float] = None,
    ):
        """
        expected price change   = Expected(Future price - current_price)
                                = mean - current price

        Implementing the optimization below
        E(x-C) = E(x) - C*E(1) = mean - C
        """

        return self._expected_price_change_with_frozen_rv(
            self.get_frozen_dist_with_dates(current_date, predicted_date),
            Util.project_data_to_dates(self.data, current_date)
            if current_price is None
            else current_price,
        )

    @staticmethod
    def _expected_price_change_with_frozen_rv(
        frozen_rv: stats.rv_continuous, current_price: float
    ):
        """
        expected price change   = Expected(Future price - current_price)
                                = mean - current price

        The proper way to do this is to get the distribution of the predicted price and calculate an
        expected value such as:
            self.dist.expect(lambda x: x - current_price, loc=loc, scale=scale)

        Implementing the optimization below
        E(x-C) = E(x) - C*E(1) = mean - C
        """
        return frozen_rv.moment(1) - current_price

    def expected_return_of_stock(
        self,
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        current_price: Optional[float] = None,
    ):
        """
        expected return = Expected( Future price/ current_price)
                        = (1/current price) * E(Future price)

        Implementing the optimization below
        E(x/C) = (1/C)E(x) = mean / C
        """
        if current_price is None:
            current_price = Util.project_data_to_dates(self.data, current_date)
        return self._expected_price_return_with_frozen_rv(
            self.get_frozen_dist_with_dates(current_date, predicted_date), current_price
        )

    @staticmethod
    def _expected_price_return_with_frozen_rv(
        frozen_rv: stats.rv_continuous, current_price: float
    ):
        """
        expected price change   = Expected(Future price - current_price)
                                = mean - current price

        The proper way to do this is to get the distribution of the predicted price and calculate an
        expected value such as:
            self.dist.expect(lambda x: x - current_price, loc=loc, scale=scale)

        """
        return frozen_rv.moment(1) / current_price

    def expected_value(
        self,
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        fn: Callable[[float], float],
    ):
        """Calculates the expected value of the fn when calculated against the price prediction at `predicted_date`"""
        return self._expected_value_with_frozen_rv(
            self.get_frozen_dist_with_dates(current_date, predicted_date), fn
        )

    @staticmethod
    def _expected_value_with_frozen_rv(
        frozen_rv: stats.rv_continuous, fn: Callable[[float], float]
    ):
        """Calculates the expected value of the fn when calculated against the price prediction at `predicted_date`"""
        return frozen_rv.expect(fn)


class WholePredictionModelAPI(
    PredictMeanAPI, PredictVolAPI, GenericDistributionModel, ABC
):
    def __init__(self):
        self.tckr: str = ""
        self.data: pd.Series = pd.Series(name="price")

    def initialize(
        self, tckr: str, adj_close: pd.Series, final_close: Optional[float] = None
    ) -> WholePredictionModelAPI:
        """
        initialize the prediction model with the tckr and the price data
        :param tckr: the stock tckr
        :param adj_close: adj_close data as a series with the date set as the index
        :param final_close: the last value of the close price that is used to normalize the adj_price
        :return: a reference to the Model instance
        """
        assert isinstance(adj_close, pd.Series)
        self.tckr = tckr
        # the adjusted price has more reliable day to day changes, but may not be an accurate indicator
        # of the actual final price. Normalize it to the close price to ensure that its last value is the
        # same as the close price
        if final_close is None:
            self.data = adj_close.sort_index(inplace=False)
        else:
            # noinspection PyTypeChecker
            self.data = adj_close.sort_index(inplace=False) * (
                final_close / adj_close[adj_close.index.max()]
            )
        self.data.name = "price"
        return self
