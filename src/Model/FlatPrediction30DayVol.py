"""
-------------------------------------------------------------------------------
 Name:        BaseModel_HDF5
 Purpose:     Parent class for all prediction models

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""

from __future__ import annotations

from .ApiClasses import WholePredictionModelAPI
from .PredictMeanMixins import FlatMeanMixin
from .PredictVolatilityMixins import Volatility30DayMixin
from .PredictionDistributions import NormDistributionModel


class FlatPrediction30DayVol(
    FlatMeanMixin, Volatility30DayMixin, NormDistributionModel, WholePredictionModelAPI
):
    def __init__(self):
        super().__init__()


def main():
    print("Model\\FlatPrediction30DayVol.py")


if __name__ == "__main__":
    main()
