from sqlalchemy.orm import Session as SqlSession
from .BaseModel_HDF5 import BaseModel_HDF5
from .SMADeltaModel_HDF5 import SMADeltaModel_HDF5
from .TrendLineScaledR2 import TrendLineScaledR2
from .FlatPrediction30DayVol import FlatPrediction30DayVol
from src.DataSource import StockData
from typing import Optional


def getDefaultPredictionModel_HDF5(
    fileDir: Optional[str] = None,
    workProxy: Optional[bool] = None,
    stockDB: Optional[StockData] = None,
) -> BaseModel_HDF5:
    return SMADeltaModel_HDF5(fileDir=fileDir, workProxy=workProxy, stockDB=stockDB)


def default_prediction_model(
    session: SqlSession, fileDir: Optional[str] = None, workProxy: Optional[bool] = None
) -> BaseModel_HDF5:
    raise NotImplementedError
