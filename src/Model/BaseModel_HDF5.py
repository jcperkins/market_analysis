"""
-------------------------------------------------------------------------------
 Name:        BaseModel_HDF5
 Purpose:     Parent class for all prediction models

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
import pandas as pd
import numpy as np
from scipy import stats
from src.DataSource import StockData
import src.Utility as Util
from typing import Optional
from src.types import RawDateSingle
from config import default_settings

"""
from MarketAnalysis import MarketPredictions
reload(Prediction)
reload(StockData)
tckr = '.SPX'
self = mp = MarketPredictions.MarketPredictions('tckr',workProxy = True)
data = self.stockDB.loadFromDB(tckr)
"""


class BaseModel_HDF5(object):
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        stockDB: Optional[StockData] = None,
    ):
        if fileDir is None:
            fileDir = default_settings.root_dir
        self.workProxy: bool = workProxy if workProxy is not None else default_settings.work_proxy
        if stockDB is None:
            self.stockDB = StockData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.stockDB = stockDB
        tckr: Optional[str] = None
        currentDate: Optional[RawDateSingle] = None
        predictedDate: Optional[RawDateSingle] = None
        self.distributionInfo = (tckr, currentDate, predictedDate)
        self.distribution = None
        self.meanDB = None
        self.volatilityDB = None
        self.volatilityDB_unit = None

    def get_current_price(self, currentDate, tckr, source="storage") -> float:
        return self.stockDB.getPrice(tckr, currentDate, adjusted=False, source=source)

    def populate_mean_df(
        self, date_index, tckr=".SPX", days_out=None, tckr_source="storage"
    ):
        """
        return a prediction dataframe for each date in the index with a column
        for each interval with the column name formatted as '%1i-Day'
        """
        if days_out is None:
            days_out = [7, 14, 30, 60, 180, 365, 730, 1095, 1825]
        date_index = Util.formatDateArray(date_index)
        time_delta = pd.TimedeltaIndex(days_out, unit="days").sort_values()
        self.meanDB = self.predict_mean(
            date_index,
            time_delta=time_delta,
            tckr=tckr,
            model_source="storage",
            tckr_source=tckr_source,
        )
        return self.meanDB

    def populate_volatility_df(
        self,
        date_index,
        tckr=".SPX",
        days_out=None,
        unit="Percent",
        tckr_source="storage",
    ):
        if days_out is None:
            days_out = [7, 14, 30, 60, 180, 365, 730, 1095, 1825]
        date_index = Util.formatDateArray(date_index)
        time_delta = pd.TimedeltaIndex(days_out, unit="days").sort_values()

        self.volatilityDB = self.predict_volatility(
            date_index,
            time_delta=time_delta,
            tckr=tckr,
            unit=unit,
            model_source="storage",
            tckr_source=tckr_source,
        )
        self.volatilityDB_unit = unit
        return self.volatilityDB

    def predict_mean(
        self,
        dates,
        predicted_date=None,
        time_delta=None,
        tckr="ixSPX",
        model_source="storage",
        tckr_source="storage",
    ):
        if dates is pd.NaT:
            return np.nan
        else:
            dates = Util.formatDateInput(dates)
        if predicted_date is not None:
            predicted_date = Util.formatDateInput(predicted_date)

        assert predicted_date is not None or time_delta is not None, (
            "Not Enough Data Error in MarketPrediction function "
            + "predict_mean(): 'predicted_date' and 'days_out' are"
            + "both None"
        )
        if time_delta is None:
            time_delta = predicted_date - dates
        elif type(time_delta) in [int, float]:
            time_delta = pd.TimedeltaIndex([time_delta], unit="days")
        elif type(time_delta) in [list, set, np.ndarray, pd.Series]:
            time_delta = pd.TimedeltaIndex(time_delta, unit="days")

        if (
            model_source.upper() in ["TEMPDB", "MEANDB", "AVGDB", "AVERAGEDB"]
            and self.meanDB is not None
        ):
            data = self.predict_mean_tempdf(
                dates, time_delta=time_delta, tckr=tckr, tckr_source=tckr_source
            )
        else:
            data = self.predict_mean_storage(
                dates, time_delta=time_delta, tckr=tckr, tckr_source=tckr_source
            )

        return data

    def predict_mean_storage(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """
        Defaults to the current value. If there is more than one
        timedelta, then it will return list of pd.Series (one for each
        time_delta)

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        mean = self.stockDB.getPrice(tckr, dates, adjusted=False, source=tckr_source)
        if type(time_delta) == pd.TimedeltaIndex:
            output = [pd.Series(data=mean, name=td, dtype=float) for td in time_delta]
            mean = pd.concat(output, axis=1)
            if mean.index.size == 1:
                mean = mean.loc[mean.index[0], :]
            else:
                mean = mean[mean.columns.sort_values()]
        else:
            if type(mean) == pd.Series:
                mean.name = time_delta
        return mean

    def predict_mean_tempdf(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """Defaults to the current value. If there is more than one
        timedelta, then it will return list of pd.Series (one for each
        time_delta)

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        dates = Util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the date_time index for predictions
        dates = pd.DatetimeIndex(dates.date)
        if type(time_delta) in [pd.Timedelta, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        prediction_mean = [
            self.interp_from_tempdf(dates, td, self.meanDB) for td in time_delta
        ]
        prediction_mean = pd.concat(prediction_mean, axis=1)
        prediction_mean = prediction_mean[prediction_mean.columns.sort_values()]
        final_cols = time_delta[0] if single_td else time_delta
        final_rows = dates[0] if single_date else dates
        return prediction_mean.loc[final_rows, final_cols]

    @staticmethod
    def interp_from_tempdf(dates, time_delta, temp_db):
        db_cols = temp_db.columns
        if time_delta in db_cols:
            output = temp_db.loc[dates, time_delta]
        elif time_delta < db_cols.min():
            output = temp_db.loc[dates, db_cols.min()]
        else:
            if time_delta < db_cols.max():
                low_col = db_cols[db_cols < time_delta].max()
                high_col = db_cols[db_cols > time_delta].min()
            else:
                low_col = db_cols[db_cols < db_cols.max()].max()
                high_col = db_cols.max()
            slope = (
                temp_db.loc[dates, high_col] - temp_db.loc[dates, low_col]
            ) / float((high_col - low_col).days)
            output = temp_db.loc[dates, low_col] + int(time_delta.days) * slope
            output.name = time_delta
        return output

    def predict_volatility(
        self,
        dates,
        predicted_date=None,
        time_delta=None,
        tckr="ixSPX",
        unit="Percent",
        model_source="storage",
        tckr_source="storage",
    ):
        """
        defaults to the 30d annual volatilty adjusted to represent
        the expected variation around the mean at the preditedDate

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if dates is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        if dates is pd.NaT:
            return np.nan
        else:
            dates = Util.formatDateInput(dates)
        if predicted_date is not None:
            predicted_date = Util.formatDateInput(predicted_date)

        assert predicted_date is not None or time_delta is not None, (
            "Not Enough Data Error in MarketPrediction function "
            + "predict_mean(): 'predicted_date' and 'days_out' are"
            + "both None"
        )
        if time_delta is None:
            time_delta = predicted_date - dates
        elif type(time_delta) in [int, float]:
            time_delta = pd.TimedeltaIndex([time_delta], unit="days")
        elif type(time_delta) in [list, set, np.ndarray, pd.Series]:
            time_delta = pd.TimedeltaIndex(time_delta, unit="days")

        if (
            model_source.upper() in ["TEMPDB", "MEANDB", "AVGDB", "AVERAGEDB"]
            and self.meanDB is not None
        ):
            data = self.predict_volatility_tempdf(
                dates,
                time_delta=time_delta,
                unit=unit,
                tckr=tckr,
                tckr_source=tckr_source,
            )
        else:
            data = self.predict_volatility_storage(
                dates,
                time_delta=time_delta,
                unit=unit,
                tckr=tckr,
                tckr_source=tckr_source,
            )

        return data

    def predict_volatility_storage(
        self,
        current_date,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="PERCENT",
        tckr_source="storage",
    ):
        """
        defaults to the 30d annual volatility adjusted to represent
        the expected variation around the mean at the predicted_date

        args:
            current_date -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if current_date is a DatetimeIndex object and
                    time_delta is a Timdedelta object or current_date is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both current_date and time_delta are structured
                    as indexes
        """
        predicted_mean = self.predict_mean_storage(
            current_date, time_delta=time_delta, tckr=tckr, tckr_source=tckr_source
        )
        current_price = self.stockDB.getPrice(
            tckr, current_date, adjusted=False, source=tckr_source
        )
        volatility = self.annual_volatility(
            tckr, current_date, time_delta, source=tckr_source
        )
        volatility = volatility * np.sqrt(time_delta / pd.Timedelta(days=365))
        if unit.upper() == "PERCENT":
            multiplier = predicted_mean / current_price
        else:
            multiplier = volatility * (predicted_mean / current_price) * predicted_mean
        return volatility * multiplier

    def predict_volatility_tempdf(
        self,
        current_date,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="Percent",
        tckr_source="storage",
    ):
        """
        Interpolates from the current temp_db

        args:
            current_date -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if current_date is a DatetimeIndex object and
                    time_delta is a Timedelta object or current_date is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both current_date and time_delta are structured
                    as indexes
        """
        if type(time_delta) in [pd.Timedelta, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            # noinspection PyUnresolvedReferences
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        pred_volatility = [
            self.interp_from_tempdf(current_date, td, self.volatilityDB)
            for td in time_delta
        ]
        if single_td:
            pred_volatility = pred_volatility[0]
        else:
            pred_volatility = pd.concat(pred_volatility, axis=1)
            pred_volatility = pred_volatility[pred_volatility.columns.sort_values()]
        if unit == self.volatilityDB_unit:
            return pred_volatility
        elif unit.upper() == "PERCENT":
            future_value = self.predict_mean(
                current_date,
                time_delta=time_delta,
                model_source="temp_db",
                tckr_source=tckr_source,
            )
            return future_value * pred_volatility
        else:
            future_value = self.predict_mean(
                current_date,
                time_delta=time_delta,
                model_source="temp_db",
                tckr_source=tckr_source,
            )
            return pred_volatility / future_value

    def set_model_info(self, current_date, predicted_date, tckr):
        info_changed = False
        if tckr is None:
            tckr = self.distributionInfo[0]
        if current_date is None:
            current_date = self.distributionInfo[1]
        if predicted_date is None:
            predicted_date = self.distributionInfo[2]
        if (
            self.distributionInfo != (tckr, current_date, predicted_date)
            or self.distribution is None
        ):
            assert (
                current_date is not None
                and predicted_date is not None
                and tckr is not None
            ), (
                "Not enough information has been provided. "
                + "The probability distribution has not been "
                + "initilized yet and it needs valid values for "
                + "'current_date', 'predicted_date', and 'tckr'... "
                + "current_date = %s, predicted_date = %s, tckr = %s"
                % (str(current_date), str(predicted_date), str(tckr))
            )
            info_changed = True
        return info_changed

    def get_prob_dist(
        self,
        current_date,
        predicted_date,
        tckr,
        model_source="storage",
        source="storage",
    ):
        tckr = self.distributionInfo[0] if tckr is None else tckr
        if current_date is None:
            current_date = self.distributionInfo[1]
        if predicted_date is None:
            predicted_date = self.distributionInfo[2]
        if (
            self.distributionInfo != (tckr, current_date, predicted_date)
            or self.distribution is None
        ):
            assert (
                current_date is not None
                and predicted_date is not None
                and tckr is not None
            ), (
                "Not enough information has been provided. The "
                + "probability distribution has not been "
                + "initilized yet and it needs valid values for "
                + "'current_date', 'predicted_date', and 'tckr'... "
                + "current_date = %s, predicted_date = %s, tckr = %s"
                % (str(current_date), str(predicted_date), str(tckr))
            )
            self.distributionInfo = (tckr, current_date, predicted_date)
            prediction_mean = self.predict_mean(
                current_date,
                predicted_date,
                tckr=tckr,
                model_source=model_source,
                tckr_source=source,
            )
            prediction_stdev = self.predict_volatility(
                current_date,
                predicted_date,
                tckr=tckr,
                model_source=model_source,
                tckr_source=source,
            )
            self.distribution = stats.norm(loc=prediction_mean, scale=prediction_stdev)
        return self.distribution

    def get_cum_prob_from_price(
        self,
        price,
        current_date=None,
        predicted_date=None,
        tckr=None,
        direction="Below",
        source="storage",
    ):
        """
        returns the predicted probability that the price will be above or
        below the price targets provided in the variable 'prices'. The
        variable 'direction' determines if the probability is the probability
        of the future price being above the target price or below the
        target price.
        """
        dist = self.get_prob_dist(current_date, predicted_date, tckr)
        probability = dist.cdf(price)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        return probability

    def get_price_from_cum_prob(
        self,
        probability,
        current_date=None,
        predicted_date=None,
        tckr=None,
        direction="Below",
        source="storage",
    ):
        """
        The percent point function is the inverse of the cdf function and
        so this is the inverse of the 'getProbFromPrices function
          o  direction indicates whether the probabilities reflect the
             probability of prices being above or below the returned price
        """
        dist = self.get_prob_dist(current_date, predicted_date, tckr)
        if type(probability) == list:
            probability = np.array(probability)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        price = dist.ppf(probability)
        return price

    def get_prob_of_price(
        self, price, current_date=None, predicted_date=None, tckr=None, source="storage"
    ):
        return self.get_prob_dist(current_date, predicted_date, tckr).pdf(price)

    def calculate_trend_by_span(
        self, tckr, date, trailing_days, col_name="Close", source="storage"
    ):
        date = Util.formatDateInput(date)
        if type(trailing_days) == str:
            trailing_days = Util.interp_span(trailing_days, unit="d")
        if type(date) == pd.DatetimeIndex:
            start_date = min(date) - pd.Timedelta(days=trailing_days)
            end_date = max(date) + pd.Timedelta(days=1)
            tckr_prices = tckr_prices = self.stockDB.loadFromDB(
                dbKey=tckr,
                start=start_date,
                end=end_date,
                columns=[col_name],
                source=source,
            )
        else:
            start_date = date - pd.Timedelta(days=trailing_days)
            end_date = date + pd.Timedelta(days=1)
            tckr_prices = tckr_prices = self.stockDB.loadFromDB(
                dbKey=tckr,
                start=start_date,
                end=end_date,
                columns=[col_name],
                source=source,
            )
        #            if(date not in tckr_prices.index):
        #                return np.nan,np.nan
        #        start =time.time()
        first_date = tckr_prices.index.min()
        tckr_prices["DeltaTime"] = (tckr_prices.index - first_date).days
        tckr_prices["XY"] = tckr_prices[col_name] * tckr_prices["DeltaTime"]
        tckr_prices["X2"] = tckr_prices["DeltaTime"] ** 2
        tckr_prices["Y2"] = tckr_prices[col_name] ** 2
        r = tckr_prices.rolling("%01id" % trailing_days)
        sum_data = r[["DeltaTime", col_name, "XY", "X2", "Y2"]].aggregate(np.sum)
        num = r[col_name].count()
        # intercept = ((sumY*sumX2) - (sumX*SumXY))/((num*sumX2)-(SumX**2))
        intercept = (
            sum_data[col_name] * sum_data["X2"]
            - (sum_data["DeltaTime"] * sum_data["XY"])
        ) / ((num * sum_data["X2"]) - (sum_data["DeltaTime"] ** 2))
        # slope = (num*sumXY - sumX*sumY)/((num*sumX2)-(SumX**2))
        slope = (num * sum_data["XY"] - sum_data["DeltaTime"] * sum_data[col_name]) / (
            (num * sum_data["X2"]) - (sum_data["DeltaTime"] ** 2)
        )
        slope.name = "Slope"
        # trend = intercept + slope*data['DeltaTime']
        # now for R2 = 1-ss_tot/ss_res
        y_avg = r[col_name].mean()
        # r2 = pd.Series(data = [0]*len(data.index),index=data.index,
        #  dtype=float,name='R2')
        ss_tot = sum_data["Y2"] - 2 * y_avg * sum_data[col_name] + num * (y_avg ** 2)
        ss_res = (
            sum_data["Y2"]
            - 2 * intercept * sum_data[col_name]
            - 2 * slope * sum_data["XY"]
            + (intercept ** 2) * num
            + 2 * intercept * slope * sum_data["DeltaTime"]
            + (slope ** 2) * sum_data["X2"]
        )
        r2values = 1 - ss_res / ss_tot
        r2 = pd.Series(data=r2values, index=tckr_prices.index, dtype=float, name="R2")
        # now ensure that the output has the correct format
        if type(date) == pd.DatetimeIndex:
            output = pd.concat([slope, r2, pd.Series(index=date)], axis=1)[
                ["Slope", "R2"]
            ].sort_index(inplace=False)
        else:
            output = pd.concat([slope, r2, pd.Series(index=[date])], axis=1)[
                ["Slope", "R2"]
            ].sort_index(inplace=False)
        output = (
            output.fillna(method="ffill")
            .fillna(method="bfill")
            .loc[date, ["Slope", "R2"]]
        )
        output = output.Slope, output.R2
        return output

    def annual_volatility(self, tckr, dates, trailing_timedelta, source="strorage"):
        # dailyChange = data['Close'].pct_change()
        # dailyVolatility = dailyChange.rolling('%01id'%trailing_days).std()
        # annualVolatility = np.sqrt(252)*dailyVolatility
        dates = Util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the DatetimeIndex for predictions
        dates = pd.DatetimeIndex(dates.date)

        if type(trailing_timedelta) == pd.Timedelta:
            single_td = True
            trailing_timedelta = pd.TimedeltaIndex([trailing_timedelta]).sort_values()
        else:
            single_td = False
        start_date = min(dates) - trailing_timedelta
        end_date = max(dates) + pd.Timedelta(days=1)
        date_range = pd.bdate_range(start=start_date.min(), end=end_date)
        tckr_change = self.stockDB.getPrice(
            tckr, date_range, source=source
        ).pct_change()
        output = [
            pd.Series(name=td, data=tckr_change.rolling("%1id" % td.days).std())
            for td in trailing_timedelta
        ]
        output = pd.concat(output, axis=1) * np.sqrt(252)

        final_rows = np.array(dates, dtype="datetime64[ns]")
        while True:
            valid_rows = np.isin(final_rows, output.index)
            if valid_rows.all():
                final_rows = pd.DatetimeIndex(final_rows)
                break
            final_rows[~valid_rows] = final_rows[~valid_rows] - pd.Timedelta(days=1)

        final_cols = trailing_timedelta[0] if single_td else trailing_timedelta
        final_rows = final_rows[0] if single_date else final_rows

        return output.loc[final_rows, final_cols]

    def approximate_vix_volatility(self, date, prediction_date, source="storage"):
        """ Sets the volatility based on the 1 month and 3 month VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month VIX
                volatilities"""
        date = Util.formatDateInput(date)
        prediction_date = Util.formatDateInput(prediction_date)
        if type(date) == pd.DatetimeIndex:
            volatility_data = pd.DataFrame(
                index=date, columns=["ixVIX", "ixVXV", "AdjustedVixVolatility"]
            )
        else:
            volatility_data = pd.DataFrame(
                index=[date],
                columns=[
                    "PredictionTimedelta",
                    "ixVIX",
                    "ixVXV",
                    "AdjustedVixVolatility",
                ],
            )

        volatility_data.loc[:, "PredictionTimedelta"] = prediction_date - date
        # load the correct data
        if (volatility_data.PredictionTimedelta > pd.Timedelta(days=30)).any():
            volatility_data.loc[date, "ixVXV"] = self.stockDB.getPrice(
                "ixVXV", date, adjusted=False, source=source
            )
            if (volatility_data.PredictionTimedelta < pd.Timedelta(days=90)).any():
                volatility_data.loc[date, "ixVIX"] = self.stockDB.getPrice(
                    "ixVIX", date, adjusted=False, source=source
                )
                # Assign dates less than or equal to 30 days or have null ixVXV
                select_bool = np.logical_and(
                    volatility_data.PredictionTimedelta <= pd.Timedelta(days=30),
                    pd.isna(volatility_data.ixVXV),
                )
                if select_bool.any():
                    volatility_data.loc[
                        select_bool, "AdjustedVixVolatility"
                    ] = volatility_data.loc[select_bool, "ixVIX"]
                # Assign all dates greater than or equal to 90 days
                select_bool = volatility_data.PredictionTimedelta <= pd.Timedelta(
                    days=30
                )
                if select_bool.any():
                    volatility_data.loc[
                        select_bool, "AdjustedVixVolatility"
                    ] = volatility_data.loc[select_bool, "ixVXV"]
                # assign all remaining values the weighted average of the 1m
                #  and 3m VIX volatility
                select_bool = pd.isna(volatility_data.AdjustedVixVolatility)
                if select_bool.any():
                    calculated_value = volatility_data.ixVIX[select_bool] / 60.0 * (
                        90.0 - volatility_data.PredictionTimedelta[select_bool].days
                    ) + (
                        volatility_data.ixVXV[select_bool]
                        / 60.0
                        * (volatility_data.PredictionTimedelta.days[select_bool] - 30)
                    )
                    volatility_data.loc[
                        select_bool, "AdjustedVixVolatility"
                    ] = calculated_value
            else:
                # nothing is below 90 so assign VXV to everything and
                #  correct for if there are any missing values
                volatility_data.loc[
                    date, "AdjustedVixVolatility"
                ] = self.stockDB.getPrice("ixVXV", date, adjusted=False, source=source)
                select_bool = pd.isna(volatility_data.AdjustedVixVolatility)
                if select_bool.any():
                    missing_dates = volatility_data.index[select_bool]
                    volatility_data.loc[
                        missing_dates, "AdjustedVixVolatility"
                    ] = self.stockDB.getPrice(
                        "ixVIX", missing_dates, adjusted=False, source=source
                    )
        else:
            # nothing is above 30 days so assign VIX to everything
            volatility_data.loc[date, "AdjustedVixVolatility"] = self.stockDB.getPrice(
                "ixVIX", date, adjusted=False, source=source
            )

        return volatility_data.AdjustedVixVolatility[date]

    def expected_profit_of_stock(
        self, tckr, current_date, predicted_date, source="storage"
    ):
        """
        expected profit = expected price - current price
                        = mean of dist - current price
        """
        expected_price = self.get_prob_dist(
            current_date, predicted_date, tckr, source=source
        ).mean()
        current_price = self.get_current_price(current_date, tckr, source=source)
        return expected_price - current_price

    def expected_return_of_stock(
        self, tckr, current_date, predicted_date, source="storage"
    ):
        """return = profit / amount risked = profit / price"""
        expected_price = self.get_prob_dist(
            current_date, predicted_date, tckr, source=source
        ).mean()
        current_price = self.get_current_price(current_date, tckr, source=source)
        return (expected_price / current_price) - 1


def main():
    print("Model\\BaseModel_HDF5.py")


if __name__ == "__main__":
    main()
