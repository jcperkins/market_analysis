"""
-------------------------------------------------------------------------------
 Name:        TrendLineScaledR2
 Purpose:     Parent class for all prediction models

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
import pandas as pd
import numpy as np
from .BaseModel_HDF5 import BaseModel_HDF5
from src.DataSource import StockData
import src.Utility as Util
from typing import Optional, List

"""
from MarketAnalysis import MarketPredictions
reload(Prediction)
reload(StockData)
tckr = '.SPX'
self = mp = MarketPredictions.MarketPredictions('tckr',workProxy = True)
data = self.stockDB.loadFromDB(tckr)
"""


class TrendLineScaledR2(BaseModel_HDF5):
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        stockDB: Optional[StockData] = None,
        maxTrailingDays_Vol: int = 18,
        quadCoef_Vol: Optional[List[float]] = None,
        trendSpan_Mean: Optional[List[float]] = None,
    ):
        BaseModel_HDF5.__init__(
            self, fileDir=fileDir, workProxy=workProxy, stockDB=stockDB
        )
        if quadCoef_Vol is None:
            quadCoef_Vol = [76.506, -1.264, 0.0068]
        if trendSpan_Mean is None:
            trendSpan_Mean = [180, 270, 365]
        self.maxTrailingDays_vol = maxTrailingDays_Vol
        self.quadCoef_Vol = quadCoef_Vol

    #    def PredictMeanColumn_BestRsqTrendScaledByRsq(self,data,daysOut):
    def predict_mean(
        self, current_date, predicted_date, tckr, source="storage", return_extra=False
    ):
        """
        TODO :  Need to update to run with the rest of the inheritance
        Calculates a trend line for several trend_spans and uses the line
        with the best fit. The returned slope is scaled by the R2 value
        """
        current_date = Util.formatDateInput(current_date)
        predicted_date = Util.formatDateInput(predicted_date)
        trend_spans = [180, 270, 365]
        if type(current_date) == pd.DatetimeIndex:
            slopes = pd.DataFrame(index=current_date, columns=trend_spans)
        else:
            slopes = pd.DataFrame(index=[current_date], columns=trend_spans)
        if type(current_date) == pd.DatetimeIndex:
            r2_df = pd.DataFrame(index=current_date, columns=trend_spans)
        else:
            r2_df = pd.DataFrame(index=[current_date], columns=trend_spans)
        for span in trend_spans:
            slopes.loc[current_date, span], r2_df.loc[
                current_date, span
            ] = self.calculate_trend_by_span(
                tckr, current_date, span, col_name="Close", source=source
            )
        # slopeDF = pd.DataFrame(slopes)
        output = pd.DataFrame(
            index=r2_df.index,
            columns=["PredictedMean", "BestSlope", "BestR2", "TrendSpan"],
        )
        output.loc[:, "BestR2"] = r2_df.max(axis=1)
        # now ensure that only one column per row has a True value
        #        output.loc[:, 'BestSlope'] = pd.Series(index=r2_df.index,
        #                                               name = 'BestSlope')
        for span in trend_spans:
            unassigned_rows = pd.isna(output.PredictedMean)
            bool_select = np.logical_and(unassigned_rows, r2_df[span] == output.BestR2)
            output.loc[bool_select, "BestSlope"] = slopes[span][bool_select]
            output.loc[bool_select, "TrendSpan"] = span
        days_out = (predicted_date - current_date).days
        current_price = self.stockDB.getPrice(
            tckr, current_date, adjusted=False, source="storage"
        )
        output.loc[:, "PredictedMean"] = current_price + (
            output.BestSlope * days_out * output.BestR2
        )
        if return_extra:
            output = output.loc[current_date, :]
            return (
                output.PredictedMean,
                output.BestSlope,
                output.BestR2,
                output.TrendSpan,
            )
        else:
            return output.loc[current_date, "PredictedMean"]

    def predict_volatility(
        self, current_date, predicted_date, tckr, unit="Percent", source="storage"
    ):
        """
        observed pattern of best fits following the trend:
            Volatility Calculated Span =
                            0.0068(days_out)^2 - 1.264(days_out) + 76.506
        This was observed when comparing various Rsq trend predictors
            paired with several volatility spans original work can be found at:
        """
        days_out = np.mean(np.clip((predicted_date - current_date).days, 0, 90))
        trailing_days = (
            self.quadCoef_Vol[0]
            + self.quadCoef_Vol[1] * days_out
            + self.quadCoef_Vol[2] * days_out ** 2
        )
        trailing_days = pd.Timedelta(days=np.round(trailing_days))
        predicted_mean = self.predict_mean(
            current_date, predicted_date, tckr, source=source
        )
        current_price = self.stockDB.getPrice(
            tckr, current_date, adjusted=False, source=source
        )
        volatility = self.annual_volatility(
            tckr, current_date, trailing_days, source=source
        )
        if unit.upper() == "PERCENT":
            multiplier = predicted_mean / current_price
        else:
            multiplier = volatility * (predicted_mean / current_price) * predicted_mean
        return np.abs(volatility * multiplier)


def main():
    print("Model\\TrendLineScaledR2.py")


if __name__ == "__main__":
    main()
