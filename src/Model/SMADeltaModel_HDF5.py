"""
-------------------------------------------------------------------------------
 Name:        SMADeltaModel_HDF5
 Purpose:     Calculates teh mean from the difference in 2 simple
              moving averages
                if 2 weeks or more, it will be a scale factor time the SMA
                for a period equal to the predictiono range minus the
                SMA for a period equal to half of the prediction range
                    o  1 week - 0x
                    o  2 weeks - 0.75x
                    o  3 weeks - 1x
                    o  1m - 1.5x
                    o  2m+ - 2.0x
 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
import numpy as np
import pandas as pd

import src.Utility as Util
from src.Model.BaseModel_HDF5 import BaseModel_HDF5
from src.DataSource import StockData
from typing import Optional


"""
from MarketAnalysis import MarketPredictions
reload(Prediction)
reload(StockData)
tckr = '.SPX'
self = mp = MarketPredictions.MarketPredictions('tckr',workProxy = True)
data = self.stockDB.loadFromDB(tckr)
"""


class SMADeltaModel_HDF5(BaseModel_HDF5):
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        stockDB: Optional[StockData] = None,
    ):
        BaseModel_HDF5.__init__(
            self, fileDir=fileDir, workProxy=workProxy, stockDB=stockDB
        )

    def predict_mean_storage(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """
        if 2 weeks or more, it will be a scale factor time the SMA for a
        period equal to the predictiono range minus the SMA for a period equal
        to half of the prediction range
            o  1 week - 0x
            o  2 weeks - 0.75x
            o  3 weeks - 1x
            o  1m - 1.5x
            o  2m+ - 2.0x
        """
        # start with formatting
        dates = Util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the DatetimeIndex for predictions
        dates = pd.DatetimeIndex(dates.date)
        if type(time_delta) in [pd.Timedelta, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        days = [7, 14, 21, 30, 60]
        factors = [0, 0.75, 1.25, 1.6, 2]
        multi_factor = np.interp(time_delta.days, days, factors)

        if multi_factor.size == 1 and multi_factor[0] == 0.0:
            if single_date:
                return self.stockDB.getPrice(
                    tckr, dates[0], adjusted=False, source=tckr_source
                )
            else:
                return self.stockDB.getPrice(
                    tckr, dates, adjusted=False, source=tckr_source
                )
        else:
            start_date = dates.min() - time_delta.max()
            end = dates.max() + pd.Timedelta(days=1)
            tckr_data = pd.bdate_range(start=start_date, end=end)
            tckr_data = self.stockDB.getPrice(tckr, tckr_data, source=tckr_source)
            start_price = tckr_data[dates]
            # sma_diff = sma_short - sma_long
            sma_valid = np.logical_or(
                time_delta >= pd.Timedelta(days=2), multi_factor != 0
            )
            if sma_valid.any():
                sma_diff = [
                    (
                        tckr_data.rolling("%1id" % (d / 2)).mean()
                        - tckr_data.rolling("%1id" % d).mean()
                    )[dates]
                    for d in time_delta[sma_valid].days
                ]
                mean = [
                    pd.Series(
                        name=time_delta[sma_valid][i],
                        data=(sma_diff[i] * multi_factor[sma_valid][i] + start_price),
                    )
                    for i in range(sma_valid.sum())
                ]
            if (~sma_valid).any():
                mean += [
                    pd.Series(start_price, name=td) for td in time_delta[~sma_valid]
                ]
            mean = pd.concat(mean, axis=1)
            mean = mean[mean.columns.sort_values()]
        # account for mult. vs single time_delta formatting requirements
        final_cols = time_delta[0] if single_td else time_delta
        final_rows = dates[0] if single_date else dates
        return mean.loc[final_rows, final_cols]

    def predict_volatility_storage(
        self,
        dates,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="PERCENT",
        tckr_source="storage",
    ):
        """
        The volatility that best matches the variation of the price around the
        predicted mean from the SMA differences is the annual volatility
        calculated from a span of days 1/2 the prediction span with a minimum
        span of 30 days and adjusted for the variation expected in the
        prediction span
            variation % = annual_volatility(priceData,
                                            span=max(30, days_out/2))
                          * np.sqrt(days_out/365.)
        args:
            dates   - The hypothetical date that the prediction is made
                            no information past this date is considered
            predicted_date - The future date for which to predict the
                            volatility
            tckr          - The underlying asset to predict
            span          - Not Used - The span (in days) of historical prices
                            used to estimate the annual volatility. The SMA
                            instance of the predictionModel class overrides
                            the span parameter and recalculates it off of the
                            number of days between predicted_date and
                            dates
            unit          - Indicates the type of return expected. 'Percent'
                            is the percentage of the expected mean that the
                            model expects the price to fluctuate
                            (predicted stdev / predicted mean). 'Dollar' is
                            expected stdev of the price around the new mean
                            in dollars.
        """
        if type(time_delta) in [pd.Timedelta, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        days_out = np.array(time_delta.days, dtype=int)
        time_delta = pd.TimedeltaIndex(np.clip(days_out, 30, 5000), unit="days")
        time_delta = time_delta[0] if single_td else time_delta
        predicted_mean = self.predict_mean(
            dates,
            time_delta=time_delta,
            tckr=tckr,
            model_source="storage",
            tckr_source=tckr_source,
        )
        volatility = self.annual_volatility(tckr, dates, time_delta, source=tckr_source)
        volatility = volatility * np.sqrt(time_delta.days / 365.0)
        if unit.upper() == "PERCENT":
            multiplier = 1
        else:
            multiplier = predicted_mean
        return volatility * multiplier


def main():
    print("Model\\SMADeltaModel_HDF5.py")


if __name__ == "__main__":
    main()
