from __future__ import annotations

from typing import Optional, Tuple, List, Dict

import pandas as pd
import src.Utility as Util
from scipy import stats
from src.types import RawDateSingle

from ..ApiClasses import GenericDistributionModel


class NormDistributionModel(GenericDistributionModel):
    dist = stats.norm

    # noinspection PyUnresolvedReferences
    def get_prediction_shape(
        self, current_date: RawDateSingle, predicted_date: RawDateSingle
    ) -> Tuple[List[float], Dict[str, float]]:
        """
        returns the information needed to build a continuous random variable with the scipy.stats module
         to create a predicted distribution of the price.

        For the Norm distribution, only the location and scale are needed

        :param current_date: the maximum date allowed to use int he prediction calculation
        :param predicted_date: the date to predict the price
        :return: location, scale, []
        """
        current_date = pd.Timestamp(current_date)
        time_delta = pd.Timestamp(predicted_date) - current_date
        args = []
        kw = {}
        mean = self.predict_mean(current_date, time_delta)
        kw["loc"] = mean
        kw["scale"] = (
            self.predict_volatility(current_date, time_delta, unit="percent") * mean
        )
        return args, kw

    # noinspection PyUnresolvedReferences
    def expected_price_change(
        self,
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        current_price: Optional[float] = None,
    ):
        """
        expected price change   = Expected(Future price - current_price)
                                = mean - current price

        The proper way to do this is to get the distribution of the predicted price and calculate an
        expected value such as:
            self.dist.expect(lambda x: x - current_price, loc=loc, scale=scale)

        but is a normal distribution and we know the current priced is a constant, we
        can take some shortcuts and not calculate the variation of the distribution as it will not be needed
        E(x-C) = E(x) - C*E(1) = mean - C
        """
        expected_price = self.predict_mean(current_date, predicted_date)
        if current_price is None:
            current_price = Util.project_data_to_dates(self.data, current_date)
        return expected_price - current_price

    # noinspection PyUnresolvedReferences
    def expected_return_of_stock(
        self,
        current_date: RawDateSingle,
        predicted_date: RawDateSingle,
        current_price: Optional[float] = None,
    ):
        """
        expected return = Expected( Future price/ current_price)
                        = (1/current price) * E(Future price)

        Implementing the optimization below
        E(x/C) = (1/C)E(x) = mean / C
        """
        expected_price = self.predict_mean(current_date, predicted_date)
        if current_price is None:
            current_price = Util.project_data_to_dates(self.data, current_date)

        return expected_price / current_price
