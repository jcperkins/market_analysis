"""
-------------------------------------------------------------------------------
Name:        FlatMeanMixin.py
Project:     MarketAnalysis_Dev
Module:      MarketAnalysis_Dev
Purpose:

Author:      Cory Perkins

Created:     1/28/20, 9:09 AM
Modified:    1/28/20, 9:08 AM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""
from __future__ import annotations

from typing import Union, overload

import numpy as np
import pandas as pd
import src.Utility as Util
from src.types import RawDateSingle, RawDateCollection, RawDateType, RawTimeDeltaSingle

from ..ApiClasses import PredictMeanAPI


class FlatMeanMixin(PredictMeanAPI):
    @overload
    def predict_mean(
        self, from_dates: RawDateSingle, time_delta: RawTimeDeltaSingle
    ) -> float:
        ...

    @overload
    def predict_mean(
        self, from_dates: RawDateCollection, time_delta: RawTimeDeltaSingle
    ) -> pd.Series:
        ...

    def predict_mean(
        self, from_dates: RawDateType, time_delta: RawTimeDeltaSingle
    ) -> Union[float, pd.Series]:
        """
        returns a 'X-day' prediction (i.e. 7-day, 30-day, 90-day, etc.) for each date
        provided in `from_dates`.

        This base model implementation defaults to the current value but this function
        is expected to be overridden in modules that inherit from the base. If
        there is more than one timedelta, then it will return a dataframe (one column for each
        time_delta)

        args:
            from_dates  -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta  -   a pd.TimedeltaIndex object of intervals to predict

        returns:
            the predicted mean as a:
                float if current_date is a timestamp and time_delta is a Timedelta instance
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object with the name of the series set to the 'x-Days'
                    where x is the number of days in time_delta
        """
        from_dates = Util.formatDateInput(from_dates)
        if from_dates is pd.NaT or from_dates is None:
            return np.nan
        if not isinstance(time_delta, pd.Timedelta):
            time_delta = pd.Timedelta(time_delta, unit="days")
        result = Util.project_data_to_dates(self.data, from_dates, method="ffill")
        if isinstance(result, pd.Series):
            result.name = f"{time_delta.days}-Day"
        return result
