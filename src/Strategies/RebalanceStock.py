# -------------------------------------------------------------------------------
# Name:        Rebalance Stock
# Purpose:     Strategy consists of trying to maintain a given portfolio balance
#                within a given threshold
#
# Author:      a0225347
# o
# Created:     2017/08/10
# Copyright:   (c) a0225347 2017
# Licence:     <your licence>
# -------------------------------------------------------------------------------
#!/usr/bin/env python

# Package and modules for importing data; this code may change depending on pandas version import datetime
# import pandas as pd
import numpy as np
import datetime
import src.Utility as MH


class RebalanceStock(object):
    def __init__(
        self,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=None,
        pfName="GenericPortfolio",
    ):
        self.conditionsValid = True
        self.targetStockPerc = targetStockPerc
        self.rebalanceFreq = rebalanceFreq
        self.rebalanceSpan = datetime.timedelta(
            days=MH.interpretSpan(rebalanceFreq, unit="d")
        )
        self.rebalanceThreshold = rebalanceThreshold
        self.pfName = pfName
        self.eventDictionary = {}
        self.earliestTrades = None
        self.lastTrades = None
        self.startDate = None
        self.endDate = None
        # logic to pupulate defaultTckrs
        if defaultTckrs is None:
            self.defaultTckrs = {}
            for tckr in self.targetStockPerc.keys():
                self.defaultTckrs[tckr] = tckr
        elif type(defaultTckrs) == str:
            default = defaultTckrs
            self.defaultTckrs = {}
            for tckr in self.targetStockPerc.keys():
                self.defaultTckrs[tckr] = default
        else:
            self.defaultTckrs = defaultTckrs

        self.name = "RebalanceStock"

    # =============================================================================
    # *****************************************************************************
    # --------------------------- MANDATORY FUNCTIONS -----------------------------
    # *****************************************************************************
    # =============================================================================

    def areConditionsValid(self):
        return self.conditionsValid

    def calculateStartEndDates(self, sDB, oDB, lDB, startDate=None, endDate=None):
        """ shortcut function to avoid duplicating heavy data manipulation if possible.
            if shortcutting to the start/end dates will be difficult, just shortcut to the
                init function and call self.initilizeStrategy()
        """
        # determine start and end dates
        startDate, endDate = self.initilizeStartEndDates(sDB, startDate, endDate)
        self.startDate = startDate
        self.endDate = endDate
        return startDate, endDate

    def initilizeStrategy(self, sDB, oDB, lDB, startDate=None, endDate=None):
        # determine start and end dates
        startDate, endDate = self.initilizeStartEndDates(sDB, startDate, endDate)
        self.startDate = startDate
        self.endDate = endDate
        # load temp DB's with the correct data for backtesting
        self.initilizeData(sDB, startDate, endDate)
        return startDate, endDate

    def getPortfolioName(self):
        if self.pfName is None:
            # create default portfolio name
            itemListStr = ""
            for tckr in self.targetStockPerc.keys():
                itemListStr = (
                    itemListStr
                    + tckr
                    + str(int(self.targetStockPerc[tckr] * 100))
                    + "_"
                )
            self.pfName = (
                "Balanced_"
                + itemListStr
                + "Rebalance"
                + self.rebalanceFreq
                + "_Start%04i%02i%02i"
                % (self.startDate.year, self.startDate.month, self.startDate.day)
            )
        return self.pfName

    def process(self, currentDate, eventFlags, pf, sDB, oDB, lDB):
        pfMix = self.createValidMix(currentDate)
        pf.rebalance(pfMix, self.rebalanceThreshold)
        nextRebalance = currentDate + self.rebalanceSpan
        if nextRebalance in self.eventDictionary.keys():
            self.eventDictionary[nextRebalance].append("Rebalance")
        else:
            self.eventDictionary[nextRebalance] = ["Rebalance"]
        nextEventDate = min(self.eventDictionary.keys())
        nextEventFlags = self.eventDictionary.pop(nextEventDate)
        return nextEventDate, nextEventFlags

    # =============================================================================
    # *****************************************************************************
    # ----------------------------- HELPER FUNCTIONS ------------------------------
    # *****************************************************************************
    # =============================================================================

    def initilizeStartEndDates(self, sDB, startDate, endDate):
        # find all unique tckrs that will be used in the analysis
        uniqueTckrs = list(
            set(self.targetStockPerc.keys() + self.defaultTckrs.values())
        )
        # map tckr to earliest and latest recorded trade
        earliestTrades = {}
        lastTrades = {}
        for tckr in uniqueTckrs:
            earliestTrades[tckr], lastTrades[tckr] = sDB.getMinMaxDate(tckr)
        self.earliestTrades = earliestTrades
        self.lastTrades = lastTrades
        # find the maximum earliest trade date that can't be substituted and this will be the limit to our start date
        # do the inverse for the endDate
        earliestDate = datetime.datetime(1900, 1, 1)
        latestDate = datetime.datetime(2100, 1, 1)
        for tckr in earliestTrades.keys():
            if (not tckr in self.defaultTckrs.keys()) or tckr == self.defaultTckrs[
                tckr
            ]:
                # no default position so this is a limiting factor
                earliestDate = max(earliestDate, earliestTrades[tckr])
                latestDate = min(latestDate, lastTrades[tckr])

        if startDate is None:
            startDate = datetime.datetime(1900, 1, 1)
        startDate = MH.formatDateInput(max(startDate, earliestDate))

        if endDate is None:
            endDate = datetime.datetime(1900, 1, 1)
        endDate = MH.formatDateInput(max(endDate, latestDate))
        return startDate, endDate

    def initilizeData(self, sDB, startDate, endDate):
        # try to always add 'ixSPX' to uniqueTckrs for comparison at the end
        uniqueTckrs = list(
            set(self.targetStockPerc.keys() + self.defaultTckrs.values() + ["ixSPX"])
        )
        sDB.populateTempDB(uniqueTckrs, start=startDate, end=endDate)

    def findValidTckr(self, currentDate, tckr):
        if currentDate < self.earliestTrades[tckr]:
            if tckr in self.defaultTckrs.keys():
                return self.findValidTckr(currentDate, self.defaultTckrs[tckr])
            else:
                raise ValueError(
                    "No valid tckrs or default tckrs available for the date '%s'"
                    % str(currentDate)
                )
        else:
            return tckr

    def createValidMix(self, currentDate):
        realMix = self.targetStockPerc.copy()
        for tckr in self.targetStockPerc.keys():
            validTckr = self.findValidTckr(currentDate, tckr)
            if validTckr != tckr:
                if validTckr in realMix.keys():
                    realMix[validTckr] += realMix.pop(tckr)
                else:
                    realMix[validTckr] = realMix.pop(tckr)
        return realMix


def SPYOnly():
    targetStockPerc = {"IVV": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {"IVV": "SPY"}
    rebalanceThreshold = 0.02
    pfName = "SPXOnly"
    return RebalanceStock(
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        pfName=pfName,
    )


def BalancedEquity():
    targetStockPerc = {
        "IVV": 0.2,
        "IUSG": 0.1,
        "IUSV": 0.1,
        "IWM": 0.2,
        "IWR": 0.2,
        "EFA": 0.2,
    }
    rebalanceFreq = "6m"
    defaultTckrs = {"IVV": "SPY"}
    rebalanceThreshold = 0.02
    pfName = "Balanced"
    return RebalanceStock(
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        pfName=pfName,
    )


def Leveraged():
    bondPortion = 0.65
    stockPortion = 0.35
    targetStockPerc = {
        "SPXL": stockPortion / 2,
        "UPRO": stockPortion / 2,
        "BOND": bondPortion,
    }
    defaultTckrs = {
        "SPXL": "SPXLFAKE",
        "UPRO": "SPXL",
        "BOND": "AGG",
        "HYG": "AGG",
        "TLT": "AGG",
        "SHY": "AGG",
        "ILTB": "AGG",
        "IEI": "AGG",
        "IEF": "AGG",
    }
    rebalanceFreq = "6m"
    rebalanceThreshold = 0.05
    pfName = "Leveraged"
    return RebalanceStock(
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        pfName=pfName,
    )


def cycleFreqAndThreshold_BalancedEquity():
    targetStockPerc = {
        "IVV": 0.2,
        "IUSG": 0.1,
        "IUSV": 0.1,
        "IWM": 0.2,
        "IWR": 0.2,
        "EFA": 0.2,
    }
    #    rebalanceFreq = ['2y','18m','1y','6m','3m']
    rebalanceFreq = ["2y", "1y"]
    defaultTckrs = {"IVV": "SPY"}
    rebalanceThreshold = [0.05, 0.1] + np.arange(0.2, 1, 0.2).tolist()
    strategyArray = []
    pfNameBase = "B"
    for freq in rebalanceFreq:
        for thresh in rebalanceThreshold:
            pfName = pfNameBase + "-" + freq + "-" + str(thresh)
            strategyArray.append(
                RebalanceStock(
                    targetStockPerc,
                    freq,
                    thresh,
                    defaultTckrs=defaultTckrs,
                    pfName=pfName,
                )
            )
    return strategyArray


def cycleFreqAndThreshold_Leveraged():
    bondPortion = 0.65
    stockPortion = 0.35
    targetStockPerc = {
        "SPXL": stockPortion / 2,
        "UPRO": stockPortion / 2,
        "BOND": bondPortion,
    }
    defaultTckrs = {
        "SPXL": "SPXLFAKE",
        "UPRO": "SPXL",
        "BOND": "AGG",
        "HYG": "AGG",
        "TLT": "AGG",
        "SHY": "AGG",
        "ILTB": "AGG",
        "IEI": "AGG",
        "IEF": "AGG",
    }
    rebalanceFreq = ["2y", "18m", "1y", "6m", "3m"]
    #    rebalanceFreq = ['2y','1y']
    rebalanceThreshold = [0.05, 0.1] + np.arange(0.2, 1, 0.2).tolist()
    strategyArray = []
    pfNameBase = "Lev"
    for freq in rebalanceFreq:
        for thresh in rebalanceThreshold:
            pfName = pfNameBase + "-" + freq + "-" + str(thresh)
            strategyArray.append(
                RebalanceStock(
                    targetStockPerc,
                    freq,
                    thresh,
                    defaultTckrs=defaultTckrs,
                    pfName=pfName,
                )
            )
    return strategyArray


# ['5y','3y','2y','1y','9m','6m','3m']
def optimizableLeverage(
    stkPrtn=0.35,
    rblncFrq="5y",
    rblncThrshld=0.02,
    aggPerc=None,
    bondPerc=None,
    hygPerc=None,
    tltPerc=None,
    shyPerc=None,
    iltbPerc=None,
    ieiPerc=None,
    iefPerc=None,
    pfName="Lvrg",
):
    """
    Allows for optimization routines to fill some of the parameters and
    this will fill out the rest and return the function.
            returns None if the input parameters are invalid
quick debug code:
stkPrtn=0.35
rblncFrq='5y'
rblncThrshld=0.02
aggPerc=None
bondPerc=None
hygPerc=None
tltPerc=0.2
shyPerc=None
iltbPerc=None
ieiPerc=None
iefPerc=None
    """
    outputStrategy = None
    defaultBondPerc = {
        "AGG": 0.2,
        "BOND": 0.2,
        "HYG": 0.15,
        "TLT": 0.05,
        "SHY": 0.1,
        "ILTB": 0.1,
        "IEI": 0.1,
        "IEF": 0.1,
    }
    defaultTckrs = {
        "SPXL": "SPXLFAKE",
        "UPRO": "SPXL",
        "BOND": "AGG",
        "HYG": "AGG",
        "TLT": "AGG",
        "SHY": "AGG",
        "ILTB": "AGG",
        "IEI": "AGG",
        "IEF": "AGG",
    }
    stockPortion = float(stkPrtn)
    bondPortion = 1 - stockPortion
    targetStockPerc = {"SPXL": stockPortion * 0.5, "UPRO": stockPortion * 0.5}

    emptyBondPerc = {}
    usedBondPerc = 0
    leftOverDefaultPerc = 0
    bondInputs = [
        aggPerc,
        bondPerc,
        hygPerc,
        tltPerc,
        shyPerc,
        iltbPerc,
        ieiPerc,
        iefPerc,
    ]
    bondTckrs = ["AGG", "BOND", "HYG", "TLT", "SHY", "ILTB", "IEI", "IEF"]
    for iBond in range(len(bondInputs)):
        if bondInputs[iBond] is None:
            leftOverDefaultPerc += defaultBondPerc[bondTckrs[iBond]]
            emptyBondPerc[bondTckrs[iBond]] = defaultBondPerc[bondTckrs[iBond]]
        else:
            usedBondPerc += bondInputs[iBond]
            targetStockPerc[bondTckrs[iBond]] = bondPortion * bondInputs[iBond]

    if usedBondPerc <= 1.01:
        leftOverDefaultPerc = sum(emptyBondPerc.values())
        bondPercRemaining = float(1 - usedBondPerc)
        for emptyBond in emptyBondPerc.keys():
            targetStockPerc[emptyBond] = (
                bondPortion
                * emptyBondPerc[emptyBond]
                * bondPercRemaining
                / leftOverDefaultPerc
            )

        if 1.001 >= sum(targetStockPerc.values()) >= 0.999:
            outputStrategy = RebalanceStock(
                targetStockPerc,
                rblncFrq,
                rblncThrshld,
                defaultTckrs=defaultTckrs,
                pfName=pfName,
            )
    return outputStrategy

    pass


def main():
    pass


if __name__ == "__main__":
    main()
