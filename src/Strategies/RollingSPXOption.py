"""
-------------------------------------------------------------------------------
 Name:        RollingSPXOption
 Purpose:     Contains the logic and code for the strategy that will role
                 different option positions as it's primary method of
                 generating returns

 Author:      a0225347
o
 Created:     2017/08/10
 Copyright:   (c) a0225347 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
# !/usr/bin/env python

# Package and modules for importing data; this code may change depending on
# pandas version import datetime
import pandas as pd
import numpy as np
import datetime
import src.Utility as MH


class RollingSPXOption:
    def __init__(
        self,
        optionType,
        purchaseSpan,
        rollAfterSpanTxt,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=None,
        additionalConditions=None,
        positionQty=1,
        pfName=None,
        indexOptions=None,
    ):
        # default to a call optionType for now
        if additionalConditions is None:
            additionalConditions = []
        if indexOptions is None:
            indexOptions = ["ixSPX", "SPY"]
        self.optionType = "Call"
        #        self.optionType = optionType
        self.conditionsValid = True
        if purchaseSpan == rollAfterSpanTxt:
            self.sellAtExpiration = True
        else:
            self.sellAtExpiration = False
        # If purchase span is > 2yr then default to 3yrs and buy the longest
        #  exDate available in the middle of Jan
        if MH.interpretSpan(purchaseSpan) > 731:
            self.purchaseSpan = "3y"
        else:
            self.purchaseSpan = purchaseSpan
        self.rollAfterSpanTxt = rollAfterSpanTxt
        self.rollAfterSpan = datetime.timedelta(
            days=MH.interpretSpan(rollAfterSpanTxt, unit="d")
        )
        self.targetStrikeToPriceRatio = targetStrikeToPriceRatio
        self.maxPfRisk = maxPfRisk

        self.targetStockPerc = targetStockPerc
        self.rebalanceFreq = rebalanceFreq
        self.rebalanceSpan = datetime.timedelta(
            days=MH.interpretSpan(rebalanceFreq, unit="d")
        )
        self.rebalanceThreshold = rebalanceThreshold
        self.pfName = pfName
        self.additionalConditions = additionalConditions
        self.maxPositionsPossible, self.minPositionSpan = MH.calculateMaxPositionQtySPX(
            self.purchaseSpan, self.rollAfterSpanTxt
        )
        self.positionQty = min(self.maxPositionsPossible, positionQty)
        self.eventDictionary = {}
        self.earliestTrades = None
        self.lastTrades = None
        self.startDate = None
        self.endDate = None
        self.existingExDates = {}
        # logic to pupulate defaultTckrs
        if defaultTckrs is None:
            self.defaultTckrs = {}
            for tckr in self.targetStockPerc.keys():
                self.defaultTckrs[tckr] = tckr
        elif type(defaultTckrs) == str:
            default = defaultTckrs
            self.defaultTckrs = {}
            for tckr in self.targetStockPerc.keys():
                self.defaultTckrs[tckr] = default
        else:
            self.defaultTckrs = defaultTckrs
        if (
            self.rollAfterSpan.days > MH.interpretSpan(self.purchaseSpan, unit="d")
            or self.rollAfterSpan < self.minPositionSpan
            or self.maxPfRisk > 1
            or self.maxPfRisk < 0
        ):
            self.conditionsValid = False
        self.indexOptions = indexOptions
        self.name = "RollingCalls_" + "_".join(indexOptions)

        self.oneOptionAtATime = True

    # =============================================================================
    # *****************************************************************************
    # --------------------------- MANDATORY FUNCTIONS -----------------------------
    # *****************************************************************************
    # =============================================================================

    def areConditionsValid(self):
        return self.conditionsValid

    def calculateStartEndDates(self, sDB, oDB, lDB, startDate=None, endDate=None):
        """
        shortcut function to avoid duplicating heavy data manipulation
        if possible. if shortcutting to the start/end dates will be
        difficult, just shortcut to the init function and call
        self.initilizeStrategy()
        """
        # determine start and end dates
        startDate, endDate = self.initilizeStartEndDates(sDB, startDate, endDate)
        self.startDate = startDate
        self.endDate = endDate
        return startDate, endDate

    def initilizeStrategy(self, sDB, oDB, lDB, startDate=None, endDate=None):
        # determine start and end dates
        startDate, endDate = self.initilizeStartEndDates(sDB, startDate, endDate)
        self.startDate = startDate
        self.endDate = endDate
        # load temp DB's with the correct data for backtesting
        self.initilizeData(sDB, oDB, lDB, startDate, endDate)
        return startDate, endDate

    def getPortfolioName(self):
        if self.pfName is None:
            # create default portfolio name
            self.pfName = (
                "RollingSPX"
                + self.optionType
                + "_Rebalance"
                + self.rebalanceFreq
                + "_Start%04i%02i%02i"
                % (self.startDate.year, self.startDate.month, self.startDate.day)
            )
        return self.pfName

    def process(self, currentDate, eventFlags, pf, sDB, oDB, lDB):
        if "Start" in eventFlags:
            self.initilizePf(currentDate, pf, sDB, oDB, lDB)
        # cycle through flags and look for instructions
        for flag in eventFlags:
            # cycle through the flags
            # o  Option Position Name = 'OptionType-PurchaseCode-SellCode-
            #                                                      StrikeCode'
            #   o  current option types available are ['Call','Put']
            #   o  purchase code is the span and then additional info
            #       follows seperatred by '_'
            #   o  sellcode is the span and then additional info follows
            #       seperatred by '_'
            #   o  strike code is simple for now IXX for in the money and
            #       OXX is out of the money and XX is the percentage of that
            #       the position is in/out of the money
            # interpret flag
            if type(flag) == tuple:
                cmd = flag[0]
                if len(flag) >= 2:
                    position = flag[1]
                #                    if(len(flag)>=3):
                #                        arguments = flag[2:]
                #                    else:
                #                        arguments = None
                else:
                    position = None
            else:
                cmd = flag
            if cmd == "Buy":
                self.buyOption(position, pf, sDB, oDB, lDB)
            elif cmd == "Sell":
                self.sellOption(position, pf, sDB, oDB, lDB)
            elif cmd == "Excise":
                self.exciseOption(position, pf, sDB, oDB, lDB)
            elif cmd == "Roll":
                self.rollOption(position, pf, sDB, oDB, lDB)
        if "Rebalance" in eventFlags:
            self.rebalanceStock(currentDate, pf, sDB, oDB, lDB)

        # determine the date and the flags requried for the next step
        nextEventDate = min(self.eventDictionary.keys())
        nextEventFlags = self.eventDictionary.pop(nextEventDate)
        return nextEventDate, nextEventFlags

    # =============================================================================
    # *****************************************************************************
    # ----------------------------- HELPER FUNCTIONS ------------------------------
    # *****************************************************************************
    # =============================================================================

    def initilizeStartEndDates(self, sDB, startDate, endDate):
        # find all unique tckrs that will be used in the analysis
        uniqueTckrs = (
            list(set(self.targetStockPerc.keys() + self.defaultTckrs.values()))
            + ["ixVIX"]
            + self.indexOptions
        )
        # map tckr to earliest and latest recorded trade
        earliestTrades = {}
        lastTrades = {}
        for tckr in uniqueTckrs:
            earliestTrades[tckr], lastTrades[tckr] = sDB.getMinMaxDate(tckr)
        self.earliestTrades = earliestTrades
        self.lastTrades = lastTrades
        # Find the maximum earliest trade date that can't be substituted
        # and this will be the limit to our start date do the inverse for
        # the endDate
        earliestDate = datetime.datetime(1900, 1, 1)
        latestDate = datetime.datetime(2100, 1, 1)
        for tckr in earliestTrades.keys():
            if tckr not in self.defaultTckrs.keys() or tckr == self.defaultTckrs[tckr]:
                # no default position so this is a limiting factor
                earliestDate = max(earliestDate, earliestTrades[tckr])
                latestDate = min(latestDate, lastTrades[tckr])

        if startDate is None:
            startDate = datetime.datetime(1900, 1, 1)
        startDate = MH.formatDateInput(max(startDate, earliestDate))

        if endDate is None:
            endDate = datetime.datetime(2100, 1, 1)
        endDate = MH.formatDateInput(min(endDate, latestDate))
        return startDate, endDate

    def initilizeData(self, sDB, oDB, lDB, startDate, endDate):
        # try to always add 'ixSPX' to uniqueTckrs for comparison at the end
        uniqueTckrs = list(
            set(
                self.targetStockPerc.keys()
                + self.defaultTckrs.values()
                + ["ixSPX", "ixVIX", "ixVXV", "SPY"]
            )
        )
        sDB.populateTempDB(uniqueTckrs, start=startDate, end=endDate)
        lDB.populateTempDB(start=startDate, end=endDate)

    def saveEvent(self, eventDate, eventFlag):
        eventDate = MH.formatDateInput(eventDate)
        if type(eventFlag) is not list:
            eventFlag = [eventFlag]
        if eventDate in self.eventDictionary.keys():
            self.eventDictionary[eventDate] = (
                self.eventDictionary[eventDate] + eventFlag
            )
        else:
            self.eventDictionary[eventDate] = eventFlag

    # =============================================================================
    # ------------------------ Stock Processing Functions -------------------------
    # =============================================================================
    def findValidTckr(self, currentDate, tckr):
        if currentDate < self.earliestTrades[tckr]:
            if tckr in self.defaultTckrs.keys():
                return self.findValidTckr(currentDate, self.defaultTckrs[tckr])
            else:
                raise ValueError(
                    "No valid tckrs or default tckrs available for the date "
                    + str(currentDate)
                )
        else:
            return tckr

    def createValidMix(self, currentDate):
        realMix = self.targetStockPerc.copy()
        for tckr in self.targetStockPerc.keys():
            validTckr = self.findValidTckr(currentDate, tckr)
            if validTckr != tckr:
                if validTckr in realMix.keys():
                    realMix[validTckr] += realMix.pop(tckr)
                else:
                    realMix[validTckr] = realMix.pop(tckr)
        return realMix

    def rebalanceStock(self, currentDate, pf, sDB, oDB, lDB):
        pfMix = self.createValidMix(currentDate)
        pf.rebalance(pfMix, self.rebalanceThreshold)
        nextRebalance = currentDate + self.rebalanceSpan
        if nextRebalance in self.eventDictionary.keys():
            self.eventDictionary[nextRebalance].append("Rebalance")
        else:
            self.eventDictionary[nextRebalance] = ["Rebalance"]

    # =============================================================================
    # ----------------------- Option Processing Functions -------------------------
    # =============================================================================
    def initilizePf(self, currentDate, pf, sDB, oDB, lDB, purchaseAllAtOnce=True):
        """
        Preload the pf option portfolio with the necessary option data
            o Option Position Name = 'OptionType-PurchaseCode-SellCode-
                StrikeCode'
            o current option types available are ['Call','Put']
            o purchase code is the span and then additional info follows
              seperated by '_'
                o '3yr' purchase span defaults to Jan 15th purchases of the
                  furthest exDate
                o yearly offerings: user can specify Jun ex dates, Dec ex
                  dates or both
                o quarterly offereings: user can specify Q1,Q2,Q3,Q4, or any
                  combination of quarters
            o sellcode is the span and then additional info follows in csv
            o strike code is simple for now DXX for in the money (discount)
              and PXX is out of the money (premium) and XX is the percentage
              of that the position is in/out of the money
        """
        initialCash = pf.cash

        positionNames = []
        if self.purchaseSpan == "3y":
            purchaseDaysOut = MH.interpretSpan("35m")
        else:
            purchaseDaysOut = MH.interpretSpan(self.purchaseSpan)
        rollDays = self.rollAfterSpan.days
        purchaseCodes = []
        conditions = []
        for positionNum in range(self.positionQty):
            if purchaseDaysOut > 731:
                # default to middle of Jan purchase of furthest exDate
                purchaseCodes = self.calculateOptionPositionName(["Jan"], purchaseCodes)
            elif purchaseDaysOut > 365:
                # logic for yearly offerings
                if "Dec" in self.additionalConditions:
                    conditions.append("Dec")
                if "Jun" in self.additionalConditions:
                    conditions.append("Jun")
                else:
                    # Default condition: all option dedidicated for this
                    #  positions are bought in the closest offering to the
                    #  span date
                    conditions = ["Closest"]
            elif purchaseDaysOut > 180:
                # logic for quarterly offerings
                for addtlCond in self.additionalConditions:
                    if addtlCond in ["Q1", "Q2", "Q3", "Q4"]:
                        conditions.append(addtlCond)

            if len(conditions) == 0:
                # Default condition: all options dedidicated for this
                # position are bought in the closest offering to the span date
                conditions = ["Closest"]
            purchaseCodes = self.calculateOptionPositionName(conditions, purchaseCodes)
        # convert rollAfterSpan into sellCode
        if self.sellAtExpiration:
            sellCodes = ["Ex"]
        else:
            sellCodes = [self.rollAfterSpanTxt]
        strikeCodes = []
        if self.targetStrikeToPriceRatio >= 1:
            strikeCodes.append(
                "P%02i" % round((self.targetStrikeToPriceRatio - 1) * 100)
            )
        else:
            strikeCodes.append(
                "D%02i" % round((1 - self.targetStrikeToPriceRatio) * 100)
            )
        for purchase in purchaseCodes:
            for sell in sellCodes:
                for strike in strikeCodes:
                    positionNames.append(
                        self.optionType + "-" + purchase + "-" + sell + "-" + strike
                    )
        # create optionPositions
        weekly = mon = wed = fri = monthly = quarterly = yearly = False
        if purchaseDaysOut > 365:
            yearly = True
        elif purchaseDaysOut > 180:
            quarterly = True
        elif purchaseDaysOut > 57:
            monthly = True
        else:
            weekly = fri = True
        allExDates = np.array(
            MH.estimateSPXOptionDates(
                currentDate,
                weekly=weekly,
                mon=mon,
                wed=wed,
                fri=fri,
                monthly=monthly,
                quarterly=quarterly,
                yearly=yearly,
            ).index
        )
        exDateMenu = []
        for condition in self.additionalConditions:
            if condition.title() in ["Dec", "Jun"]:
                monthMatchIndex = map(
                    lambda x: pd.Timestamp(x).month == MH.month2int(condition.title()),
                    allExDates,
                )
                exDateMenu = exDateMenu + allExDates[monthMatchIndex]
            if condition.upper() in ["Q1", "Q2", "Q3", "Q4"]:
                quarterToMonthDict = {"Q1": 3, "Q2": 6, "Q3": 9, "Q4": 12}
                monthMatchIndex = map(
                    lambda x: (
                        pd.Timestamp(x).month == quarterToMonthDict[condition.upper()]
                    ),
                    allExDates,
                )
                exDateMenu = exDateMenu + allExDates[monthMatchIndex]
        # If menu is empty then use evenly spaced options from all exDates
        #  in range
        if len(exDateMenu) == 0:
            exDateMenu = allExDates
        exDateMenu = MH.formatDateInput(exDateMenu).sort_values()
        actualPositionQty = 0
        # dateShiftTimedelta is the number of days to shift in order to get
        #  to that posisitions next location in the cycle. It is also the
        #  total days in a cycle before the positions start repeating
        dateShiftTimedelta = self.minPositionSpan * self.maxPositionsPossible
        # find the first date that will allow for owning the position for %75
        #  of the rollSpan
        firstExDate = None
        iDate = 0
        menuSeries = pd.Series(data=exDateMenu, name="exDateMenu")
        menuLength = exDateMenu.size

        def estNextDateInMenu(date, menuSeries, dateShiftTimedelta):
            """
            uses the dateShiftTimedelta to shift the date until it is
            no longer within 90% of the minPositionSpan
            """
            nextDate = MH.formatDateInput(date + self.minPositionSpan)
            deltaDays = np.abs((menuSeries - nextDate))
            if any(deltaDays < self.minPositionSpan * 0.4):
                nextDate = estNextDateInMenu(nextDate, menuSeries, dateShiftTimedelta)
            return nextDate

        while firstExDate is None:
            date = menuSeries[iDate]
            purchaseDate = date - pd.Timedelta(days=purchaseDaysOut)
            sellDate = date - pd.Timedelta(days=(purchaseDaysOut - rollDays))
            if (sellDate - currentDate).days >= rollDays * 0.75:
                firstExDate = date
            else:
                menuSeries[iDate] = estNextDateInMenu(
                    date, menuSeries, dateShiftTimedelta
                )
            iDate = (iDate + 1) % menuLength

        # find evenly spaced target exDates for the positions
        positionIndex = np.arange(len(positionNames))
        evenSpacing = (
            self.minPositionSpan * self.maxPositionsPossible / self.positionQty
        )
        positionTargetDate = firstExDate + pd.TimedeltaIndex(
            data=evenSpacing.days * positionIndex, unit="D"
        )
        for iPosition in positionIndex:
            position = positionNames[iPosition]
            (
                optionType,
                purchaseSpan,
                purchaseConditions,
                sellSpan,
                sellConditions,
                strikeCode,
            ) = self.interpretOptionName(position)
            if positionTargetDate[iPosition] in exDateMenu:
                exDate = positionTargetDate[iPosition]
            else:
                # Find the closest date in menuSeries which will have
                #  estimated future options as well
                minDeltaIndex = np.abs(
                    menuSeries - positionTargetDate[iPosition]
                ).idxmin()
                exDate = menuSeries[minDeltaIndex]
            # Remove all values that are within the 90% of the minimumShift
            #  from menuSeries and exDateMenu
            menuSeries = menuSeries[
                np.abs(menuSeries - exDate) > self.minPositionSpan * 0.5
            ]
            exDateMenu = exDateMenu[
                np.abs(exDateMenu - exDate) > self.minPositionSpan * 0.5
            ]

            # validate exDate and if not valid add 1 purchase span to the date
            purchaseDate = exDate - pd.Timedelta(days=purchaseDaysOut)
            if "Ex" in sellConditions:
                sellDate = exDate
            else:
                sellDate = exDate - pd.Timedelta(days=purchaseDaysOut - rollDays)
            # add 1 year and check again if:
            #  the exDate matches the year and month of any existing date
            #  the exDate is too
            exDateValid = False
            while not exDateValid:
                exDateValid = True
                if (
                    purchaseDate < currentDate
                    and (currentDate - purchaseDate).days > rollDays / 2.0
                ):
                    # the option is valid if the option will be in the
                    #  portfolio for at least half of the intended time.
                    # This should be true because the dates were chosen so
                    #  that they would be owned for at least 75% of the time
                    exDateValid = False
                if not exDateValid:
                    # add the pre-determined shift
                    exDate = exDate + dateShiftTimedelta
                    purchaseDate = exDate - pd.Timedelta(days=purchaseDaysOut)
                    if "Ex" in sellConditions:
                        sellDate = exDate - pd.Timedelta(days=1)
                    else:
                        sellDate = exDate - pd.Timedelta(
                            days=(purchaseDaysOut - rollDays)
                        )

            eventFlag = [("Buy", position)]
            if purchaseDate < currentDate:
                if (sellDate - currentDate).days >= rollDays / 2.0:
                    # Buy the option now if the option will be in the
                    #  portfolio for at least half of the intended amount
                    #  of time.
                    self.buyOption(
                        position,
                        pf,
                        sDB,
                        oDB,
                        lDB,
                        contractQty=None,
                        sellDate=sellDate,
                        targetExDate=exDate,
                        targetStrike=None,
                    )
                else:
                    # Just in case...add the purchase span until the
                    #  purchase date becomes reasonable
                    while purchaseDate < currentDate:
                        purchaseDate = MH.formatDateInput(
                            purchaseDate + pd.Timedelta(days=purchaseDaysOut)
                        )
                    self.saveEvent(purchaseDate, eventFlag)
            else:
                # Create a buy event for the day that this posistion
                #  should be purchased
                self.saveEvent(purchaseDate, eventFlag)
            actualPositionQty += 1
        self.positionQty = actualPositionQty
        # call rebalance function which will create it's own flags
        self.rebalanceStock(currentDate, pf, sDB, oDB, lDB)
        # return the net cash flow
        return pf.cash - initialCash

    def calculateOptionPositionName(self, conditions, purchaseCodes):
        loopIteration = 0
        loopCode = self.purchaseSpan + "_" + conditions[0] + "_" + str(0)
        while loopCode in purchaseCodes:
            suffix = str(loopIteration)
            for condition in conditions:
                loopCode = self.purchaseSpan + "_" + condition + "_" + suffix
                if loopCode not in purchaseCodes:
                    break
            loopIteration += 1
        purchaseCodes.append(loopCode)
        return purchaseCodes

    @staticmethod
    def interpretOptionName(positionName):
        optionType, purchaseCode, sellCode, strikeCode = positionName.split("-")
        exDateParms = purchaseCode.split("_")
        sellParms = sellCode.split("_")
        purchaseSpan = ""
        purchaseConditions = []
        if len(exDateParms) >= 1:
            purchaseSpan = exDateParms[0]
        if len(exDateParms) >= 2:
            purchaseConditions = exDateParms[1:]
        sellSpan = ""
        sellConditions = []
        if len(sellParms) >= 1:
            sellSpan = sellParms[0]
        if len(sellParms) >= 2:
            sellConditions = sellParms[1:]
        return (
            optionType,
            purchaseSpan,
            purchaseConditions,
            sellSpan,
            sellConditions,
            strikeCode,
        )

    def buyOption(
        self,
        positionName,
        pf,
        sDB,
        oDB,
        lDB,
        contractQty=None,
        sellDate=None,
        targetExDate=None,
        targetStrike=None,
    ):
        """
        Debug quick code - initialize pf,sDB,oDB,lDB through the backtest
        program debug code
        positionName = 'Call-2y_Closest_0-1y-D20'
        contractQty=None
        sellDate = None
        targetExDate = None
        targetStrike = None
        """
        # need to sell any existing contracts under this position because
        #  the info is about to change
        if (
            positionName in pf.optionPositions.keys()
            and round(pf.optionPositions[positionName].contracts) != 0
        ):
            self.sellOption(positionName, pf, sDB, oDB, lDB)
        initialCash = pf.cash
        # unpack arguments
        (
            optionType,
            purchaseSpan,
            purchaseConditions,
            sellSpan,
            sellConditions,
            strikeCode,
        ) = self.interpretOptionName(positionName)

        # gather the option details
        # logic for exDate - the exDate can't be within 20% of any other
        #  exDate in the portfolio
        if targetExDate is None:
            if self.purchaseSpan == "3y":
                purchaseDaysOut = MH.interpretSpan("35m")
            else:
                purchaseDaysOut = MH.interpretSpan(self.purchaseSpan)

            if purchaseConditions[0].upper() in ["CLOSEST"]:
                targetExDate = pf.currentDay + pd.Timedelta(days=purchaseDaysOut)
            else:
                weekly = mon = wed = fri = monthly = quarterly = yearly = False
                if purchaseDaysOut > 365:
                    yearly = True
                elif purchaseDaysOut > 180:
                    quarterly = True
                elif purchaseDaysOut > 57:
                    monthly = True
                else:
                    weekly = fri = True
                allExDates = MH.formatDateInput(
                    MH.estimateSPXOptionDates(
                        pf.currentDay,
                        weekly=weekly,
                        mon=mon,
                        wed=wed,
                        fri=fri,
                        monthly=monthly,
                        quarterly=quarterly,
                        yearly=yearly,
                    ).index
                )
                # remove the dates that are within 0.5 of the minPositionSpan
                for date in self.existingExDates.values():
                    allExDates = allExDates[
                        np.abs(allExDates - date) > self.minPositionSpan * 0.5
                    ]
                #                    selection = np.abs((allExDates-date)
                #                    allExDates = allExDates[selection]
                if purchaseConditions[0].upper() in ["DEC", "JUN"]:
                    monthMatchIndex = map(
                        lambda x: pd.Timestamp(x).month
                        == MH.month2int(purchaseConditions[0].title()),
                        allExDates,
                    )
                    targetExDate = MH.formatDateInput(max(allExDates[monthMatchIndex]))
                elif purchaseConditions[0].upper() in ["Q1", "Q2", "Q3", "Q4"]:
                    quarterToMonthDict = {"Q1": 3, "Q2": 6, "Q3": 9, "Q4": 12}
                    monthMatchIndex = map(
                        lambda x: pd.Timestamp(x).month
                        == quarterToMonthDict[purchaseConditions[0].upper()],
                        allExDates,
                    )
                    targetExDate = MH.formatDateInput(max(allExDates[monthMatchIndex]))
                else:
                    targetExDate = pf.currentDay + pd.Timedelta(days=purchaseDaysOut)
        assert targetExDate is not None, "targetExDate was never assigned"

        # Start with the first index option in the indexOptions list
        #  and purchase as much as you can. Then move to the next
        #  one. self.indexOptions should be ordered with this in mind
        valueForOptions = (pf.calculateValue() * self.maxPfRisk) / float(
            self.positionQty
        )
        originalTargetStrike = targetStrike
        if strikeCode[0].upper() == "P":  # 'P' for premium
            strikeRatio = 1 + float(strikeCode[1:]) / 100.0
        elif strikeCode[0].upper() == "D":  # 'D' for discount
            strikeRatio = 1 - float(strikeCode[1:]) / 100.0
        else:
            strikeRatio = 1
        for iTckr in range(len(self.indexOptions)):
            underlyingSymbol = self.indexOptions[iTckr]
            # logic for strike price
            if originalTargetStrike is None or iTckr > 0:
                currentPrice = sDB.getPrice(
                    underlyingSymbol, pf.currentDay, source="temp_db"
                )
                assert np.isfinite(currentPrice), (
                    "Invalid Price returned to"
                    + "RollingSPXOption.buyOption."
                    + "Tckr: %s, Date: %s, Price: %s"
                    % (underlyingSymbol, str(pf.currentDay), str(currentPrice))
                )
                targetStrike = currentPrice * strikeRatio

            optionPrice, exDate, strike = oDB.createOption_BS(
                underlyingSymbol,
                pf.currentDay,
                targetExDate,
                targetStrike,
                optionType,
                currentPrice=currentPrice,
                strikeCondition="=",
                expirationCondition="=",
                source="temp_db",
            )

            # can only buy contracts with the portion of the PF available
            assert np.isfinite(pf.calculateValue()), "calculateValue nan"
            assert np.isfinite(self.maxPfRisk), "self.maxPfRisk nan"
            assert np.isfinite(self.positionQty), "self.positionQty nan"
            assert np.isfinite(optionPrice), "optionPrice nan"

            contracts = int(valueForOptions / (optionPrice * 100))
            # only bother to buy if contracts > 0
            if contracts > 0:
                netCashFlow = pf.buyOption(
                    positionName + "_" + underlyingSymbol,
                    contracts,
                    underlyingSymbol,
                    strike,
                    exDate,
                    optionType,
                )
                self.existingExDates[positionName] = exDate
            else:
                netCashFlow = 0

            valueForOptions += netCashFlow

        # set up the next event on the day when this position needs to be sold
        if sellDate is None:
            #            rollParameters = arguments[2].split('-')
            if sellSpan == "Ex":
                eventDate = exDate - pd.Timedelta(days=1)
            else:
                # I lose 5 days a year by setting the event date to
                #  the rollspan + current date need to anchor the event date
                #  to the exDate instead
                # rollSpan = datetime.timedelta(days=MH.interp_span(
                #  sellSpan,unit='d'))
                # eventDate = pf.currentDay+rollSpan
                eventDate = exDate - datetime.timedelta(
                    days=(
                        MH.interpretSpan(purchaseSpan, unit="d")
                        - MH.interpretSpan(sellSpan, unit="d")
                    )
                )
        else:
            if sellDate == targetExDate:
                eventDate = exDate
            else:
                eventDate = MH.formatDateInput(
                    min(sellDate, exDate - pd.Timedelta(days=1))
                )
        eventFlag = [("Roll", positionName)]
        self.saveEvent(eventDate, eventFlag)
        assert (
            eventDate in self.eventDictionary.keys()
        ), "Roll Event was not saved correctly"
        # print self.eventDictionary
        return pf.cash - initialCash

    def exciseOption(self, positionName, pf, sDB, oDB, lDB, contractQty=None):
        assert positionName in pf.optionPositions.keys(), (
            "The option position %s does not exist in the portfolio" % positionName
        )
        netCashFlow = 0
        for tckr in self.indexOptions:
            if positionName + "_" + tckr in pf.optionPositions.keys():
                if contractQty is None:
                    contractQty = pf.optionPositions[
                        positionName + "_" + tckr
                    ].contracts
                netCashFlow += pf.exciseOption(positionName + "_" + tckr, contractQty)
        return netCashFlow

    def sellOption(self, positionName, pf, sDB, oDB, lDB, contractQty=None):
        # sells all options for all tckrs under the position conditions
        netCashFlow = 0
        resetQty = False
        for tckr in self.indexOptions:
            if positionName + "_" + tckr in pf.optionPositions.keys():
                if contractQty is None:
                    contractQty = pf.optionPositions[
                        positionName + "_" + tckr
                    ].contracts
                    resetQty = True
                netCashFlow += pf.sellOption(positionName + "_" + tckr, contractQty)
            if resetQty:
                contractQty = None
                resetQty = False
        return netCashFlow

    def rollOption(
        self,
        positionName,
        pf,
        sDB,
        oDB,
        lDB,
        sellDate=None,
        targetExDate=None,
        targetStrike=None,
    ):
        netCashFlow = 0
        # sell all units of position
        netCashFlow += self.sellOption(positionName, pf, sDB, oDB, lDB)
        # buy new units
        netCashFlow += self.buyOption(positionName, pf, sDB, oDB, lDB)
        return netCashFlow


def RollingSPXCalls():
    #    optionType= 'Call'
    #    purchaseSpan = '2y'
    #    rollAfterSpan = '1y'
    #    targetStrikeToPriceRatio = 0.8
    #    maxPfRisk = 0.40
    #    additionalConditions=[]
    #    positionQty=4
    #    targetStockPerc = {'AGG':1}
    #    rebalanceFreq = '5y'
    #    defaultTckrs = {}
    #    rebalanceThreshold = 0.02
    #    indexOptions = ['ixSPX','SPY']
    #    pfName=None
    optionType = "Call"
    purchaseSpan = "6m"
    rollAfterSpan = "3m"
    targetStrikeToPriceRatio = 0.885
    maxPfRisk = 0.54
    additionalConditions = []
    positionQty = 4
    targetStockPerc = {"AGG": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {}
    rebalanceThreshold = 0.02
    indexOptions = ["SPY"]
    pfName = None

    return RollingSPXOption(
        optionType,
        purchaseSpan,
        rollAfterSpan,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        additionalConditions=additionalConditions,
        positionQty=positionQty,
        pfName=pfName,
        indexOptions=indexOptions,
    )


def OptimizezedRollingSPXCalls_100k():
    optionType = "Call"
    purchaseSpan = "2y"
    rollAfterSpan = "6m"
    targetStrikeToPriceRatio = 0.84
    maxPfRisk = 0.7
    additionalConditions = []
    positionQty = 4
    targetStockPerc = {"AGG": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {}
    rebalanceThreshold = 0.02
    pfName = None
    indexOptions = ["SPY"]
    return RollingSPXOption(
        optionType,
        purchaseSpan,
        rollAfterSpan,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
        defaultTckrs=defaultTckrs,
        additionalConditions=additionalConditions,
        positionQty=positionQty,
        pfName=pfName,
        indexOptions=indexOptions,
    )


def main():
    pass


if __name__ == "__main__":
    main()
