# -------------------------------------------------------------------------------
# Name:        RebalanceStock_Dynamic
# Purpose:     Strategy consists of trying to maintain a portfolio balance that has
#                dynamic ratios and within a given threshold
#
# Author:      a0225347
# o
# Created:     2017/08/10
# Copyright:   (c) a0225347 2017
# Licence:     <your licence>
# -------------------------------------------------------------------------------


import numpy as np
import datetime
import src.Utility as MH
from src.Strategies import RebalanceStock


class RebalanceStock_DynamicSizing(RebalanceStock):
    def __init__(
        self,
        baskestLists,
        rebalanceFreq,
        rebalanceThreshold,
        alarmThreshold,
        defaultTckrs=None,
        pfName="GenericPortfolio",
    ):
        pass
