"""
-------------------------------------------------------------------------------
 Name:        MarketResearch
 Purpose:     Holding place for functions and routines to help me look
                 for market patterns

 Author:      a0225347
o
 Created:     2017/08/10
 Copyright:   (c) a0225347 2017
 Licence:     <your licence>
---------------------------------------------------------------------------
Notes:
    Trend Predictions need to be indexed by the day that it is made and
    contain the value that it predicts the SPX will be after an interval X

    See bottom of file for notes on the results of different predictions
    The best strategy so far is to use the SMA difference multiplied by a
    scaling factor that is dependent on the prediction span (prediction
    span is frequently represented by the variable trendspan in the
    code below). The span of the long SMA is the trend span and the span
    of the short SMA is the trendspan/2. The factors are as follows:
        (trendspans, factors) = [(7, 0),
                                (14, 1),
                                (30, 1.6),
                                (90, 2.0),
                                (180, 2.0),
                                (360, 2.0)]
    The volatility that best matches the variation of the price around the
    predicted mean is:
        variation % = annual_volatility(priceData,max(30, trendSpan/2))
                        * np.sqrt(trendDays/365.)

"""

import pandas as pd
import numpy as np
import datetime, time, itertools  # ,os
from matplotlib import pyplot as plt

# from matplotlib import pyplot as plt
from scipy import stats, optimize, interpolate
from src.Model import getDefaultPredictionModel_HDF5
from src.DataSource import StockData, DataTools
import src.Utility as Util
import ffn
from importlib import reload

ffn.extend_pandas()
reload(StockData)
reload(Util)
# reload(MarketPredictions)
sDB, oDB, lDB, iDB = DataTools.initiateAllDataBases(workProxy=True)
similationStartTime = pd.Timestamp("1990-1-1")
predictionModel = getDefaultPredictionModel_HDF5()
lastPredictionPaths = {
    "Flat": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\FlatResults.csv",
    "SmoothFlat": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothFlatResults.csv",
    "VWT": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\VolumeWeightedTrendResults.csv",
    "SMA_1x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SMA_1xResults.csv",
    "SMA_2x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SMA_2xResults.csv",
    "SMA_1.25x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SMA_1p25xResults.csv",
    "SMA_1.50x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SMA_1p50xResults.csv",
    "SMA_1.75x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SMA_1p75xResults.csv",
    "SmoothSMA_1x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothSMA_1xResults.csv",
    "SmoothSMA_2x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothSMA_2xResults.csv",
    "SmoothSMA_1.25x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothSMA_1p25xResults.csv",
    "SmoothSMA_1.50x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothSMA_1p50xResults.csv",
    "SmoothSMA_1.75x": r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\SmoothSMA_1p75xResults.csv",
}


def loadLastPredictions(indicator="vwt"):
    if indicator in lastPredictionPaths.keys():
        return pd.read_csv(
            lastPredictionPaths[indicator], index_col=0, parse_dates=True
        )
    if indicator.lower() in [
        "vwt",
        "volume weighted",
        "volumeweighted",
        "volume_weighted",
        "volume weighted trend",
        "volumeweightedtrend",
        "volume_weighted_trend",
        "trend",
    ]:
        indicator = "VWT"
    if "flat" in indicator.lower():
        if "smooth" in indicator.lower():
            indicator = "SmoothFlat"
        else:
            indicator = "Flat"
    elif "sma" in indicator.lower():
        if indicator[-1].isdigit():
            scale = int(indicator[indicator.find("_") + 1 :])
            indicator = "SMA_%01ix" % scale
            if "smooth" in indicator.lower():
                indicator = "Smooth" + indicator
        elif indicator[-1] == "x" and indicator[-2].isdigit():
            scale = int(indicator[indicator.find("_") + 1 : -1])
            indicator = "SMA_%01ix" % scale
            if "smooth" in indicator.lower():
                indicator = "Smooth" + indicator

    if indicator not in lastPredictionPaths.keys() and "\\" not in indicator:
        if "csv" in indicator.lower():
            fPath = (
                r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\%s"
                % indicator
            )
        else:
            fPath = (
                r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\%s.csv"
                % indicator
            )
    else:
        fPath = lastPredictionPaths.get(indicator, indicator)
    try:
        prediction = pd.read_csv(fPath, index_col=0, parse_dates=True)
    except:
        prediction = None
        print("No prediction available for " + indicator)
    return prediction


def loadAllSPXData(sDB, columns=None):
    return sDB.loadFromDB(dbKey="ixSPX", columns=columns)


def resetImports():
    reload(MarketData)
    reload(Util)
    reload(MarketPredictions)


#    reload(MarketPortfolio)
#    reload(MarketBackTest)
#    reload(RebalanceStock)
#    reload(RollingSPXOption)
#    reload(OptionPositions)


def createPredictionListFromStorage(columnSuffix="1m", ignoreKeys=None):
    if ignoreKeys is None:
        ignoreKeys = []
    predictionList = []
    predictionNames = []
    for key in lastPredictionPaths.keys():
        if key in ignoreKeys:
            continue
        prediction = loadLastPredictions(key)
        series = prediction["Predict" + columnSuffix]
        series.name = key
        predictionList.append(series)
        predictionNames.append(key)
    return predictionList, predictionNames


def comparePrediction(
    priceData,
    predictionList,
    trendDays=30,
    volatilityDays=30,
    names=None,
    plotProbCharts=False,
    plotHist=False,
    plotTimeline=False,
):
    """ common stats used to compare the accuracy of prediction methods.
        input:
            o  priceData - a pandas series of the real Close price for each day
            o  predictionList - a list of pandas series. Each series is the prediction of a given method
                                prediction series are structured so that the index represents the date
                                that the prediction was made and the value is the guess at what the stock
                                will be worth after a given interval
            o  trendDays - # of days in advance that the predictionList are build on - (used for actual price)
            o  volatilityDays - the number of days with which to caclulate the volatility/stdDev parameters
            o  names - option list of names to call each prediction and corresponds to the series in predictions
    """
    # sanitize/process inputs
    if predictionList is None:
        suffix = {7: "1w", 14: "2w", 30: "1m", 60: "2m", 90: "3m", 180: "6m", 365: "1y"}
        predictionList, names = createPredictionListFromStorage(suffix[trendDays])
    elif type(predictionList) != list:
        predictionList = [predictionList]

    if names is None or type(names) not in [list, np.ndarray]:
        _names = [None] * len(predictionList)
    elif len(names) != len(predictionList):
        print(
            "'names' variable has a different length than the 'predictions' variable. Setting 'names' to None"
        )
        _names = [None] * len(predictionList)
    else:
        _names = list(names)
    names = []
    for i in range(len(predictionList)):
        name_i = _names[i] if _names[i] is not None else predictionList[i].name
        names.append(name_i)

    if priceData is None:
        priceData = sDB.loadFromDB(dbKey="ixSPX", columns="Close")

    # create actual value, recent variation, and flat prediction series
    shiftRows = int(round(trendDays * 252.0 / 365.0))
    endDays = priceData.index[shiftRows:]
    startDays = priceData.index[:-shiftRows]
    actualClose = priceData[endDays]
    actualClose.index = startDays
    actualClose.name = "ActualFutureValue"
    #    actualReturn = 1 - actualClose/priceData
    #    actualReturn.name = 'ActualReturn'
    variation = annual_volatility(priceData, volatilityDays) * np.sqrt(
        trendDays / 365.0
    )
    #    actualReturn_StdDev = actualReturn/variation
    #    actualReturn_StdDev.name = 'ActualReturn_StdDevNormalized'
    flat = priceData[startDays]
    flat.name = "Flat"
    if "Flat" not in names:
        predictionList = [flat] + predictionList
        names = ["Flat"] + names
    methodQty = len(predictionList)

    # error and accuracy calculations
    meanErrorPctStr = "\nMean Error Percent:"
    meanErrorStdDevStr = "\nStd. Dev. Error Percent:"
    stdErrorPctStr = "\nMean Error StdDev Factor:"
    stdErrorStdDevStr = "\nStd. Dev. StdDev Factor:"
    pdfR2toIdeal = "\nPDF R^2 correlation to ideal:"
    cdfR2toIdeal = "\nCDF R^2 correlation to ideal:"

    x = x = np.linspace(-2.5, 2.5, 500)
    idealPDF = pd.Series(stats.norm.pdf(x), index=x, name="Ideal_PDF")
    idealCDF = pd.Series(stats.norm.cdf(x), index=x, name="Ideal_CDF")

    predErrorList = []
    pdfFitList = [idealPDF]
    cdfFitList = [idealCDF]
    #    predictionReturns = [actualReturn]
    #    predictionReturns_StdDev = [actualReturn_StdDev]
    statsDF = pd.DataFrame(
        index=names,
        columns=[
            "MeanErrorPct",
            "StdDevErrorPct",
            "MeanStdDevFctr",
            "StdDevOfStdFctr",
            "R2_PDF",
            "R2_CDF",
        ],
    )
    for i in range(len(predictionList)):
        series = predictionList[i]
        sName = names[i]
        sName = series.name if sName is None else sName
        pctName, stdName = sName + "_ErrorAsPct", sName + "_ErrorAsStdDev"
        errorAsPct = (actualClose - series) / flat
        errorAsPct = errorAsPct[pd.notnull(errorAsPct)]
        errorAsPct.name = pctName
        predErrorList.append(errorAsPct)
        meanErrorPctStr += "\n\t%s = %1.4f" % (sName, errorAsPct.mean())
        meanErrorStdDevStr += "\n\t%s = %1.4f" % (sName, errorAsPct.std())
        statsDF.loc[sName, "MeanErrorPct"] = errorAsPct.mean()
        statsDF.loc[sName, "StdDevErrorPct"] = errorAsPct.std()
        errorAsStdDev = errorAsPct / variation
        errorAsStdDev = errorAsStdDev[pd.notnull(errorAsStdDev)]
        errorAsStdDev.name = stdName
        predErrorList.append(errorAsStdDev)
        stdErrorPctStr += "\n\t%s = %1.4f" % (sName, errorAsStdDev.mean())
        stdErrorStdDevStr += "\n\t%s = %1.4f" % (sName, errorAsStdDev.std())
        statsDF.loc[sName, "MeanStdDevFctr"] = errorAsStdDev.mean()
        statsDF.loc[sName, "StdDevOfStdFctr"] = errorAsStdDev.std()

        pdfFit = pd.Series(
            stats.norm.pdf(x, loc=errorAsStdDev.mean(), scale=errorAsStdDev.std()),
            index=x,
            name=sName + "_PDF",
        )
        pdfFitList.append(pdfFit)
        cdfFit = pd.Series(
            stats.norm.cdf(x, loc=errorAsStdDev.mean(), scale=errorAsStdDev.std()),
            index=x,
            name=sName + "_CDF",
        )
        cdfFitList.append(cdfFit)
        pdfR2, cdfR2 = R2(pdfFit, idealPDF), R2(cdfFit, idealCDF)
        pdfR2toIdeal += "\n\t%s = %1.4f" % (sName, pdfR2)
        cdfR2toIdeal += "\n\t%s = %1.4f" % (sName, cdfR2)
        statsDF.loc[sName, "R2_PDF"] = pdfR2
        statsDF.loc[sName, "R2_CDF"] = cdfR2
    #        seriesReturn = 1 - series/priceData
    #        seriesReturn.name = sName+"_Return"
    #        predictionReturns.append(seriesReturn)
    #        seriesReturnStdDev = seriesReturn/variation
    #        seriesReturnStdDev.name = sName+'_Return_StdDevNormalized'
    #        predictionReturns_StdDev.append(seriesReturnStdDev)

    print(meanErrorPctStr)
    print(meanErrorStdDevStr)
    print(stdErrorPctStr)
    print(stdErrorStdDevStr)
    print(pdfR2toIdeal)
    print(cdfR2toIdeal)

    pdfDF = pd.concat(pdfFitList, axis=1)
    cdfDF = pd.concat(cdfFitList, axis=1)
    if plotProbCharts:
        pdfDF.plot()
        plt.title("Probability Density Function")
        plt.show()
        cdfDF.plot()
        plt.title("Cumlative Density Function")
        plt.show()

    # display results
    #    plt.figure(1,(10,10))
    #    totalDF[['Close','Actual'+trendSuffix,'Trend','Prediction']].plot()
    #    #plotByTimeDelta(totalDF[['Close','Actual'+trendSuffix,'Trend','Prediction']])
    #    plt.show()
    if plotHist:
        figCnt = 0
        plt.figure(2, (8, 3 * methodQty))
        while True:
            series = predErrorList[figCnt]
            series = series[pd.notnull(series)]
            figCnt += 1
            ax = plt.subplot(methodQty, 2, figCnt)
            ax.hist(series)
            plt.sca(ax)
            plt.title(series.name)
            if figCnt >= methodQty * 2:
                break
        plt.tight_layout()
        plt.show()
    if plotTimeline:
        plotByTimeDelta(pd.concat([priceData, actualClose] + predictionList, axis=1))

    # returnsDF = pd.concat(predictionReturns,axis=1)
    # returnsStdDevDF = pd.concat(predictionReturns_StdDev,axis=1)

    return statsDF, pdfDF, cdfDF


def refreshSMAPredictions(smaScaleFactorList=None, startMethodList=None):
    if smaScaleFactorList is None:
        smaScaleFactorList = [0, 1, 1.25, 1.5, 1.75, 2]
    if startMethodList is None:
        startMethodList = ["Raw", "Smooth"]
    for smaScaleFactor in smaScaleFactorList:
        for startMethod in startMethodList:
            print("Method: %s SMA Scale Factor: %1i" % (startMethod, smaScaleFactor))
            runSMAPredictionAgainsData(
                smaScaleFactor=smaScaleFactor, startMethod=startMethod
            )


def runSMAPredictionAgainsData(smaScaleFactor=1, startMethod="Raw"):
    spxData = loadAllSPXData(sDB, columns=["Close", "Volume"])
    #    spxData = spxData.iloc[-510:,:]
    # add blank prediction columns
    newColumnSpans = ["1w", "2w", "1m", "3m", "6m", "1y"]
    predictionRanges = [7, 14, 30, 90, 180, 365]
    columnList = [spxData.Close, spxData.Volume]
    for iSpan in range(len(newColumnSpans)):
        print("Processing " + newColumnSpans[iSpan] + " span")
        trend, actual = SMATrend(
            spxData.Close,
            spxData.Volume,
            predictionRanges[iSpan],
            smaScaleFactor=smaScaleFactor,
            startMethod=startMethod,
            displayStatus=False,
        )
        trend.name = "Predict" + newColumnSpans[iSpan]
        columnList.append(trend)
        actual.name = "Actual" + newColumnSpans[iSpan]
        columnList.append(actual)
    prefix = "" if startMethod.lower() == "raw" else startMethod.title()
    predictionDF = pd.concat(columnList, axis=1)
    if smaScaleFactor == 0:
        predictionDF.to_csv(
            r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\%sFlatResults.csv"
            % (prefix)
        )
    elif smaScaleFactor % 1 == 0:
        predictionDF.to_csv(
            r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\%sSMA_%01ixResults.csv"
            % (prefix, smaScaleFactor)
        )
    else:
        predictionDF.to_csv(
            r"C:\Users\cp035982\Python\Projects\MarketAnalysis\ResearchData\%sSMA_%1ip%02ixResults.csv"
            % (prefix, int(smaScaleFactor), int(100 * (smaScaleFactor % 1)))
        )
    return predictionDF


def SMATrend(
    priceData,
    volumeData,
    predictionRange,
    smaScaleFactor=1,
    startMethod="Smooth",
    displayStatus=True,
):
    # save the actual future value
    colSuffix = "_%03iDays" % predictionRange
    shiftRows = int(round(predictionRange * 252.0 / 365.0))
    endDays = priceData.index[shiftRows:]
    startDays = priceData.index[:-shiftRows]
    actualClose = priceData[endDays].copy()
    actualClose.index = startDays
    actualClose.name = "Actual" + colSuffix

    if startMethod.lower() == "raw":
        trendStart = priceData.copy()
    else:
        # calculate the smoothed termination value for each date
        def apply_PredictionStart(df):
            weightingSeries = df.Volume / df.Volume.max()
            smoothingRange = df.index.size
            finalClose = (
                smooth(df.Close * weightingSeries, smoothingRange / 2 + 1)
                / smooth(weightingSeries, smoothingRange / 2 + 1)
            )[-1]
            return finalClose

        window_length = shiftRows + (shiftRows + 1) % 2
        # trying to shortcut the trend start didn't work
        #    window = np.hanning(2*window_length - 1)
        #    window[window_length+1:] = 0
        #    weightingSeries = volumeData/volumeData.max()
        #    finalValue = smooth(priceData*weightingSeries, 2*window_length - 1, window)/smooth(weightingSeries, 2*window_length - 1, window)
        #    finalValue.name = 'TrendStart'
        smoothedSource = pd.concat([priceData, volumeData], axis=1)
        trendStart = smoothedSource.rollapply(
            2 * window_length - 1, apply_PredictionStart
        )["Close"]

    if smaScaleFactor == 0:
        smaTrend = trendStart
        smaTrend.name = "Flat" % smaScaleFactor
    else:
        sma_short = priceData.rolling("%01id" % (predictionRange / 2)).mean()
        sma_long = priceData.rolling("%01id" % (predictionRange)).mean()
        smaDiff = sma_short - sma_long
        smaTrend = trendStart[startDays] + smaDiff[startDays] * smaScaleFactor
        if smaScaleFactor % 1 == 0:
            smaTrend.name = "%01ixSMADiff" % smaScaleFactor
        else:
            smaTrend.name = "%1.2fxSMADiff" % smaScaleFactor

    if displayStatus:
        trendStart = trendStart.copy()
        trendStart.name = "TrendStart"
        if smaScaleFactor != 0:
            sma_long.name = "SMA_Long"
            sma_short.name = "SMA_Short"
        endDays = trendStart.index[shiftRows:]
        startDays = trendStart.index[:-shiftRows]
        actualEndSmoothed = trendStart[endDays]
        actualEndSmoothed.index = startDays
        actualEndSmoothed.name = "ActualSmoothed" + colSuffix
        flat = trendStart[startDays]
        flat.name = "Flat"
        # sourceSummary = pd.concat([priceData, trendStart, smaTrend],axis=1)
        predictionSummary = pd.concat([priceData, actualClose, flat, smaTrend], axis=1)
        # predictionSummary = pd.concat([priceData,actualClose,actualEndSmoothed],axis=1)
        if smaScaleFactor != 0:
            plotByTimeDelta(predictionSummary, pd.DataFrame(smaDiff))
        else:
            plotByTimeDelta(predictionSummary)
    return smaTrend, actualClose


def smooth(x, window_len=61, window="hanning", extendEnd=True):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
    see also:
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
     """

    if x.ndim != 1:
        raise (ValueError, "smooth only accepts 1 dimension arrays.")
    if type(window) == str:
        if window_len < 3:
            return x
        if type(window_len) != int and np.integer not in type(window_len).mro():
            window_len = int(window_len)
        if window_len % 2 == 0:
            window_len += 1
        if window == "flat":  # moving average
            w = np.ones(window_len, "d")
        else:
            w = eval("np." + window + "(window_len)")
    elif type(window) == np.ndarray:
        w = window
        window_len = w.size
    else:
        raise (
            ValueError,
            "Window needs to be provided as an np.ndarray or one of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'",
        )
    if x.size < window_len:
        raise (ValueError, "Input vector needs to be bigger than window size.")
    if window_len < 3:
        return x
    if type(window_len) != int and np.integer not in type(window_len).mro():
        window_len = int(window_len)
    if window_len % 2 == 0:
        window_len += 1

    def smooth_Array(xArray, w, window_len=11):
        # mathematically convolving the input signal with the weighted windows so that
        #    each point is averaged with the other points in the window according to the window weighting
        s = np.r_[x[window_len - 1 : 0 : -1], x, x[-2 : -window_len - 1 : -1]]
        y = np.convolve(w / w.sum(), s, mode="valid")[
            (window_len / 2) : -(window_len / 2)
        ]
        return y

    if type(x) == np.ndarray:
        y = smooth_Array(x, w, window_len=window_len)
    if type(x) == pd.Series:
        y = pd.Series(
            data=smooth_Array(x, w, window_len=window_len), name=x.name, index=x.index
        )
    elif type(x) == pd.DataFrame:
        y = pd.DataFrame(columns=x.columns, index=x.index)
        for colName in y.columns:
            try:
                y.loc[:, colName] = smooth_Array(x[colName], w, window_len=window_len)
            except:
                y.loc[:, colName] = x[colName]
    else:
        y = smooth_Array(x, w, window_len=window_len)
    assert x.shape == y.shape, "Input/output shapes are not equal"
    return y


def smoothRollingFunc(
    data,
    func,
    smoothWindow=61,
    windowType="hanning",
    extendEnd=True,
    args=None,
    kwargs=None,
):
    if args is None:
        args = []
    if kwargs is None:
        kwargs = {}
    if data.size > smoothWindow:
        sm = smooth(data, window_len=smoothWindow, window=windowType)
    else:
        sm = data
    if type(func) == str and func.lower() in [
        "last",
        "end",
        "final",
        "endpoint",
        "end point",
        "end pt",
    ]:
        return sm[-1]
    return func(sm, *args, **kwargs)


def rollingSmooth_EndPt(data, window_len, windowType="hanning"):
    return smoothRollingFunc(
        data, "last", smoothWindow=window_len, windowType=windowType
    )


def rollingDF_R2(df):
    """ dataframe must have a column named 'Data' and a column named 'Fit'
    the result will be in the dimentions of the original DF, so the end result
    will need to be reduced to a single column (i.e. df.iloc[:,0])"""
    return 1 - ((df.Data - df.Fit) ** 2).sum() / ((df.Data - df.Data.mean()) ** 2).sum()


def R2(data, fit):
    # R2 = 1-SSres/SStot
    # SSres = Sum((data-fit)**2)
    # SStot = Sum((data-data.mean())**2)
    return 1 - ((data - fit) ** 2).sum() / ((data - data.mean()) ** 2).sum()


def researchSMADiffAsPctVsFuturePctChange():
    spxData = loadAllSPXData(sDB, columns=["Close"])
    close = spxData.Close
    for days in [30, 90, 180]:
        rows = int(days * (252 / 365.0))
        pctChng = spxData["PctChng_%01i" % days] = close.pct_change(periods=rows)
        spxData["Future_PctChng_%01i" % days] = (
            close.pct_change(periods=rows)[rows:].tolist() + [np.nan] * rows
        )
        spxData["Volatility_%01i" % days] = np.sqrt(rows) * (
            close.pct_change().rolling(rows).std()
        )
        spxData["Future_PctChng_Volatility_%01i" % days] = (
            spxData["Future_PctChng_%01i" % days] / spxData["Volatility_%01i" % days]
        )
        spxData["SMADiff_Ratio_%01i_%01i" % (days, days * 2)] = (
            close.rolling(rows / 2).mean() - close.rolling(rows).mean()
        ) / close
        spxData["SMADiff_Ratio_Volatility_%01i_%01i" % (days, days * 2)] = (
            spxData["SMADiff_Ratio_%01i_%01i" % (days, days * 2)]
            / spxData["Volatility_%01i" % days]
        )
        spxData["PctChng_Mean_%01i" % days] = pctChng.rolling(rows).mean()
        spxData["PctChng_Std_%01i" % days] = pctChng.rolling(rows).std()
        # a normal distribution can be split into thirds at roughly +/- 0.4 stdevs
        # +1 is positive, -1 is negative, 0 is neutral
        spxData["PctChng_EndPtSmooth_%01i" % days] = pctChng.rolling(rows * 2).apply(
            smoothRollingFunc, args=["final"], kwargs={"smoothWindow": rows / 5}
        )
        spxData["Outlook_%01i" % days] = (
            np.clip(
                (
                    spxData["PctChng_EndPtSmooth_%01i" % days]
                    - spxData["PctChng_Mean_%01i" % days]
                )
                / spxData["PctChng_Std_%01i" % days],
                -0.4,
                0.4,
            )
            / 0.4
        )
        spxData.loc[
            np.logical_and(
                spxData["Outlook_%01i" % days] < 1, spxData["Outlook_%01i" % days] > -1
            ),
            "Outlook_%01i" % days,
        ] = 0.0


def buildTrendFromParms(x, polyTrends, sinusoidalTrends, minSpan=0):
    y = pd.Series(data=np.zeros(x.shape), index=x, name="Trend Fit")
    if len(polyTrends) > 0:
        # need to apply the poly trends in order from largest to least
        #   and weight the following contribution according to the span

        # get a list of spans so that I can figure out what the min/max is
        spans = np.array(zip(*polyTrends)[0])
        # assume that polyTrends is built in the same order that it needs to be processed
        polyPredictions = []
        for span, dayRange, parms in polyTrends:
            if span < minSpan:
                continue
            trendSeries = pd.Series(data=np.zeros(x.shape), index=x, name=span)
            valid = np.logical_and(x >= dayRange[0], x <= dayRange[1])
            trendSeries[valid] = np.polyval(parms, x[valid])
            # for all but the largest span time decay the effect of the trend so that all the trends smoothly merge
            if span < spans.max():
                trendSeries = trendSeries * (
                    1 - (np.clip(x, 0, dayRange[1]) / float(dayRange[1]))
                )
            else:
                # the longest trend needs to time decay to half of it's value
                if (x > 0).any():
                    trendSeries[x > 0] = trendSeries[0] + (
                        trendSeries[x > 0] - trendSeries[0]
                    ) * (
                        1
                        - 0.5 * (np.clip(x[x > 0], 0, dayRange[1]) / float(dayRange[1]))
                    )
                # find the last valid value and assign it to any values outside of the range of the prediction
                if (x > dayRange[1]).any():
                    lastValidIndex = x[valid].max()
                    trendSeries[lastValidIndex:] = trendSeries[lastValidIndex]

            polyPredictions.append(trendSeries)
        polyPredictions.append(pd.Series(data=np.zeros(x.shape), index=x, name="Final"))
        polyPredictions = pd.concat(polyPredictions, axis=1)
        polyPredictions = polyPredictions.cumsum(axis=1)
        y[:] = polyPredictions.Final[:]
    if len(sinusoidalTrends) > 0:
        for span, dayRange, parms in sinusoidalTrends:
            if span < minSpan:
                continue
            weighting = np.zeros(x.shape)
            weighting[np.logical_and(x <= 0, x >= max(dayRange[0], -span * 4))] = 1
            xPos = x[x > 0]
            if xPos.size > 0:
                weighting[x > 0] = np.exp(-xPos / float(span))
            y[:] += (
                parms[0] * np.sin(((x + parms[1]) / parms[2]) * np.pi) + parms[3]
            ) * weighting
    assert pd.notnull(y).all(), "Func buildTrendFromParms: output contains a null value"
    if pd.isna(y).any():
        print(y)
    return y


def createPctChangeDF(spanTxt, spanDays):
    windowDays = int(spanDays * 252 / 365)
    df = pd.DataFrame(
        columns=[
            "Close",
            "PriceVariation",
            "PctChng_%s" % spanTxt,
            "PctChng_Smooth_%s" % spanTxt,
            "PctChng_Mean_%s" % spanTxt,
            "PctChng_Mean0p5x_%s" % spanTxt,
            "PctChng_Mean2x_%s" % spanTxt,
            "PctChng_StdDev_%s" % spanTxt,
            "PctChng_StdDev_Daily",
            "PctChng_%s_RelPriceVar" % spanTxt,
            "PctChng_%s_RelStd" % spanTxt,
        ]
    )
    close = df["Close"] = loadAllSPXData(sDB, "Close")
    df["PriceVariation"] = np.sqrt(windowDays) * (
        close.pct_change().rolling(windowDays).std()
    )
    pctChange = df["PctChng_%s" % spanTxt] = close.pct_change(periods=windowDays)
    # df['PctChng_Smooth_%s' % spanTxt] = smooth(pctChange,window_len=windowDays)
    df["PctChng_EndPtSmooth_%s" % spanTxt] = pctChange.rolling(windowDays * 4).apply(
        smoothRollingFunc, args=["final"], kwargs={"smoothWindow": windowDays}
    )
    df["PctChng_Mean_%s" % spanTxt] = pctChange.rolling(windowDays).mean()
    df["PctChng_Mean2x_%s" % spanTxt] = pctChange.rolling(windowDays * 2).mean()
    df["PctChng_Mean0p5x_%s" % spanTxt] = pctChange.rolling(windowDays / 2).mean()
    df["PctChng_StdDev_%s" % spanTxt] = pctChange.rolling(windowDays).std()
    df["PctChng_StdDev_Daily"] = close.pct_change(1).rolling(windowDays).std()
    df["PctChng_%s_RelPriceVar" % spanTxt] = pctChange / df["PriceVariation"]
    df["PctChng_%s_RelStd" % spanTxt] = pctChange / df["PctChng_StdDev_%s" % spanTxt]

    plotByTimeDelta(
        df[
            [
                "PctChng_%s" % spanTxt,
                "PctChng_Smooth_%s" % spanTxt,
                "PctChng_EndPtSmooth_%s" % spanTxt,
                "PctChng_Mean_%s" % spanTxt,
                "PctChng_Mean0p5x_%s" % spanTxt,
            ]
        ]
    )
    df[["PriceVariation", "PctChng_StdDev_%s" % spanTxt, "PctChng_StdDev_Daily"]].plot()
    df[["PctChng_%s_RelPriceVar" % spanTxt, "PctChng_%s_RelStd" % spanTxt]].plot()

    df[["PctChng_%s_RelPriceVar" % spanTxt]].hist()
    plt.title(
        plt.gca().get_title()
        + "\nMean - %1.4f\nStd Dev - %1.4f"
        % (
            df[["PctChng_%s_RelPriceVar" % spanTxt]].mean(),
            df[["PctChng_%s_RelPriceVar" % spanTxt]].std(),
        )
    )
    plt.show()

    plotHist(df[["PctChng_%s_RelPriceVar" % spanTxt]])
    plotHist(df[["PctChng_%s_RelStd" % spanTxt]])

    return df


def runPredictionAgainstData():
    def apply_VolumeWeightedTrend(
        df, predictFutureRows=np.array([0, 5, 10, 21, 63, 127, 252])
    ):
        polyTrends, sinusoidalTrends = volumeWeightedTrend(
            df.Close, df.Volume, predictFutureRows.max(), displayStatus=False
        )
        newRow = df.iloc[-1, :].copy()
        if df.index[-1].year > df.index[-2].year:
            print(df.index[-1].year)
        weightingSeries = df.Volume / df.Volume.max()
        finalCloseTrend = (
            smooth(df.Close * weightingSeries, 21) / smooth(weightingSeries, 21)
        )[-1]
        singlePredictions = (
            buildTrendFromParms(predictFutureRows, polyTrends, sinusoidalTrends).values
            + finalCloseTrend
        )
        newRow[-predictFutureRows.size + 1 :] = singlePredictions[1:]
        return newRow

    def apply_pctChngeTrend(
        df, predictFutureRows=np.array([0, 5, 11, 21, 63, 127, 253])
    ):
        polyTrends, sinusoidalTrends = pctChngeTrend(
            df.Close, df.Volume, predictFutureRows.max(), displayStatus=False
        )
        newRow = df.iloc[-1, :].copy()
        if df.index[-1].year > df.index[-2].year:
            print(df.index[-1].year)
        weightingSeries = df.Volume / df.Volume.max()
        weightedSmoothClose = smooth(df.Close * weightingSeries, 21) / smooth(
            weightingSeries, 21
        )
        finalSmoothedClose = weightedSmoothClose[-1]
        finalSmoothedPctChange = []
        for rows in predictFutureRows:
            if rows == 0:
                finalSmoothedPctChange.append(0)
            else:
                pctChange = df["Close"].pct_change(periods=rows)
                finalSmoothedPctChange.append(smooth(pctChange, rows)[-1])

        singlePredictions = finalSmoothedClose * (
            1
            + buildTrendFromParms(
                predictFutureRows, polyTrends, sinusoidalTrends
            ).values
            + np.array(finalSmoothedPctChange)
        )
        newRow[-predictFutureRows.size + 1 :] = singlePredictions[1:]
        return newRow

    spxData = loadAllSPXData(sDB, columns=["Close", "Volume"])
    #    spxData = spxData.iloc[-510:,:]
    # add blank prediction columns
    newColumnSpans = ["1w", "2w", "1m", "3m", "6m", "1y"]
    for span in newColumnSpans:
        spxData["Predict" + span] = np.nan
    predictions = spxData.rollapply(504, apply_VolumeWeightedTrend)
    # predictions = spxData.rollapply(504, apply_pctChngeTrend)
    predictions["Close"] = spxData.Close
    predictions["Volume"] = spxData.Volume
    # populate the actual price for the predicted day
    rowAdditions = [5, 10, 21, 63, 127, 252]
    for iCol in range(len(rowAdditions)):
        predictions["Actual" + newColumnSpans[iCol]] = np.nan
        predictions.iloc[: -rowAdditions[iCol], -1] = predictions.iloc[
            rowAdditions[iCol] :, 0
        ].values
        predictions[
            ["Actual" + newColumnSpans[iCol], "Predict" + newColumnSpans[iCol]]
        ].plot()
        plt.legend()
        plt.show()
    return predictions


def applyStaticLimitsToTrend(
    predictions, trendSuffix="1m", volatilityDays=30, limit=0.05, displayResults=True
):
    totalDF = pd.concat(
        [
            predictions["Predict" + trendSuffix],
            pd.Series(index=predictions.index, name="Prediction"),
        ],
        axis=1,
    )
    totalDF.columns = ["Trend", "Prediction"]
    firstValidIndex = np.arange(totalDF.index.size)[pd.notnull(totalDF.Trend)].min()
    predictValue = totalDF.iloc[firstValidIndex, 1] = totalDF.iloc[firstValidIndex, 0]
    for i in np.arange(firstValidIndex + 1, totalDF.index.size):
        predictValue = totalDF.iloc[i, 1] = (
            np.clip(totalDF.iloc[i, 0] / predictValue, 1 - limit, 1 + limit)
            * predictValue
        )
    totalDF["Actual" + trendSuffix] = predictions["Actual" + trendSuffix]
    totalDF["Close"] = predictions.Close
    totalDF["ExpVariation" + trendSuffix] = annual_volatility(
        totalDF.Close, volatilityDays
    ) * np.sqrt(volatilityDays / 365.0)
    trendErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Trend) / totalDF.Close
    trendErrorPerc = trendErrorPerc[pd.notnull(trendErrorPerc)]
    trendErrorAsStd = trendErrorPerc / totalDF["ExpVariation" + trendSuffix]
    trendErrorAsStd = trendErrorAsStd[pd.notnull(trendErrorAsStd)]
    predErrorPerc = (
        totalDF["Actual" + trendSuffix] - totalDF.Prediction
    ) / totalDF.Close
    predErrorPerc = predErrorPerc[pd.notnull(predErrorPerc)]
    predErrorAsStd = predErrorPerc / totalDF["ExpVariation" + trendSuffix]
    predErrorAsStd = predErrorAsStd[pd.notnull(predErrorAsStd)]
    flatErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Close) / totalDF.Close
    flatErrorPerc = flatErrorPerc[pd.notnull(flatErrorPerc)]
    flatErrorStd = flatErrorPerc / totalDF["ExpVariation" + trendSuffix]
    flatErrorStd = flatErrorStd[pd.notnull(flatErrorStd)]
    print(
        "Mean Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.mean(), predErrorPerc.mean(), flatErrorPerc.mean())
    )
    print(
        "\nStd. Dev. Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.std(), predErrorPerc.std(), flatErrorPerc.std())
    )
    print(
        "Mean Error StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.mean(), predErrorAsStd.mean(), flatErrorStd.mean())
    )
    print(
        "\nStd. Dev. StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.std(), predErrorAsStd.std(), flatErrorStd.std())
    )
    if displayResults:
        plt.figure(1, (10, 10))
        totalDF[["Close", "Actual" + trendSuffix, "Trend", "Prediction"]].plot()
        # plotByTimeDelta(totalDF[['Close','Actual'+trendSuffix,'Trend','Prediction']])
        plt.show()
        plt.figure(2, (10, 10))
        ax1 = plt.subplot(321)
        ax1.hist(trendErrorPerc)
        plt.sca(ax1)
        plt.title("Error Percent - Trend")
        ax2 = plt.subplot(323)
        ax2.hist(predErrorPerc)
        plt.sca(ax2)
        plt.title("Error Percent - Prediction")
        ax3 = plt.subplot(325)
        ax3.hist(flatErrorPerc)
        plt.sca(ax3)
        plt.title("Error Percent - Flat")
        ax4 = plt.subplot(322)
        ax4.hist(trendErrorAsStd)
        plt.sca(ax4)
        plt.title("Std Dev Factor - Trend")
        ax5 = plt.subplot(324)
        ax5.hist(predErrorAsStd)
        plt.sca(ax5)
        plt.title("Std Dev Factor - Prediction")
        ax6 = plt.subplot(326)
        ax6.hist(flatErrorStd)
        plt.sca(ax6)
        plt.title("Std Dev Factor - Flat")
        plt.tight_layout()
        plt.show()
    data = [
        [
            "Trend",
            "ErrorPerc",
            trendSuffix,
            volatilityDays,
            limit,
            trendErrorPerc.mean(),
            trendErrorPerc.std(),
        ],
        [
            "Trend",
            "StdDevFactor",
            trendSuffix,
            volatilityDays,
            limit,
            trendErrorAsStd.mean(),
            trendErrorAsStd.std(),
        ],
        [
            "Prediction",
            "ErrorPerc",
            trendSuffix,
            volatilityDays,
            limit,
            predErrorPerc.mean(),
            predErrorPerc.std(),
        ],
        [
            "Prediction",
            "StdDevFactor",
            trendSuffix,
            volatilityDays,
            limit,
            predErrorAsStd.mean(),
            predErrorAsStd.std(),
        ],
        [
            "Flat",
            "ErrorPerc",
            trendSuffix,
            volatilityDays,
            limit,
            flatErrorPerc.mean(),
            flatErrorPerc.std(),
        ],
        [
            "Flat",
            "StdDevFactor",
            trendSuffix,
            volatilityDays,
            limit,
            flatErrorStd.mean(),
            flatErrorStd.std(),
        ],
    ]
    columns = [
        "Projection",
        "ParmType",
        "TrendSuffix",
        "VolatilityDays",
        "Limit",
        "Mean",
        "StdDev",
    ]
    return pd.DataFrame(data=data, columns=columns)


def applyEMASMALimitsToTrend(
    predictions, trendSuffix="1m", volatilityDays=30, limit=0.05
):
    ema_sma = (
        predictions.Close.ewm(span=45).mean() - predictions.Close.rolling(45).mean()
    )
    #    ema_sma_mean = ema_sma.rolling(45).mean()
    #    ema_sma_std = ema_sma.rolling(45).std()
    #    ema_sma_StdFctr = (ema_sma-ema_sma_mean)/ema_sma_std
    ema_sma_StdFctr = (ema_sma - (ema_sma.rolling(45).mean())) / (
        ema_sma.rolling(45).std()
    )
    ema_sma_StdFctr.name = "EMA_SMA_StdFactor"
    lowLimit = 1 - limit * (1 - ema_sma_StdFctr / 3.0)
    lowLimit.name = "Low Limit"
    highLimit = 1 + limit * (1 + ema_sma_StdFctr / 3.0)
    highLimit.name = "High Limit"
    totalDF = pd.concat(
        [
            predictions["Predict" + trendSuffix],
            pd.Series(index=predictions.index, name="Prediction"),
        ],
        axis=1,
    )
    totalDF.columns = ["Trend", "Prediction"]
    firstValidIndex = np.arange(totalDF.index.size)[pd.notnull(totalDF.Trend)].min()
    predictValue = totalDF.iloc[firstValidIndex, 1] = totalDF.iloc[firstValidIndex, 0]
    for i in np.arange(firstValidIndex + 1, totalDF.index.size):
        predictValue = totalDF.iloc[i, 1] = (
            np.clip(totalDF.iloc[i, 0] / predictValue, lowLimit[i], highLimit[i])
            * predictValue
        )
    totalDF["Actual" + trendSuffix] = predictions["Actual" + trendSuffix]
    totalDF["Close"] = predictions.Close
    # plotByTimeDelta(totalDF.iloc[:,:-1])
    totalDF["ExpVariation" + trendSuffix] = annual_volatility(
        totalDF.Close, volatilityDays
    ) * np.sqrt(volatilityDays / 365.0)
    trendErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Trend) / totalDF.Close
    trendErrorPerc.name = "trendErrorPerc"
    trendErrorAsStd = trendErrorPerc / totalDF["ExpVariation" + trendSuffix]
    predErrorPerc = (
        totalDF["Actual" + trendSuffix] - totalDF.Prediction
    ) / totalDF.Close
    predErrorPerc.name = "predErrorPerc"
    predErrorAsStd = predErrorPerc / totalDF["ExpVariation" + trendSuffix]
    flatErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Close) / totalDF.Close
    flatErrorPerc.name = "flatErrorPerc"
    flatErrorStd = flatErrorPerc / totalDF["ExpVariation" + trendSuffix]
    print(
        "Mean Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.mean(), predErrorPerc.mean(), flatErrorPerc.mean())
    )
    print(
        "\nStd. Dev. Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.std(), predErrorPerc.std(), flatErrorPerc.std())
    )
    print(
        "Mean Error StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.mean(), predErrorAsStd.mean(), flatErrorStd.mean())
    )
    print(
        "\nStd. Dev. StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.std(), predErrorAsStd.std(), flatErrorStd.std())
    )
    fig, axs = plt.subplots(3, 2, sharex=True)
    fig.set_figheight(9)
    fig.set_figwidth(10)
    fig.sca(axs[0, 0])
    plt.hist(trendErrorPerc)
    plt.legend()
    fig.sca(axs[0, 1])
    plt.hist(trendErrorAsStd)
    plt.legend()
    fig.sca(axs[1, 0])
    plt.hist(predErrorPerc)
    plt.legend()
    fig.sca(axs[1, 1])
    plt.hist(predErrorAsStd)
    plt.legend()
    fig.sca(axs[2, 0])
    plt.hist(flatErrorPerc)
    plt.legend()
    fig.sca(axs[2, 1])
    plt.hist(flatErrorStd)
    plt.legend()
    plt.show()
    fig, axs = plt.subplots(2, 2, sharex=True)
    fig.set_figheight(9)
    fig.set_figwidth(10)
    fig.sca(axs[0, 0])
    plt.plot(totalDF[["Close", "Actual" + trendSuffix, "Trend", "Prediction"]])
    plt.legend()
    fig.sca(axs[0, 1])
    ema_sma_StdFctr.plot()
    plt.legend()
    fig.sca(axs[1, 0])
    trendErrorPerc.plot()
    predErrorPerc.plot()
    flatErrorPerc.plot()
    plt.legend()
    fig.sca(axs[1, 1])
    lowLimit.plot()
    highLimit.plot()
    plt.legend()
    plt.show()


def applyDampeningToTrend(predictions, trendSuffix="1m", volatilityDays=30, TC=30):
    totalDF = pd.concat(
        [
            predictions["Predict" + trendSuffix],
            pd.Series(index=predictions.index, name="Prediction"),
        ],
        axis=1,
    )
    totalDF.columns = ["Trend", "Prediction"]
    firstValidIndex = np.arange(totalDF.index.size)[pd.notnull(totalDF.Trend)].min()
    predictValue = totalDF.iloc[firstValidIndex, 1] = totalDF.iloc[firstValidIndex, 0]
    for i in np.arange(firstValidIndex + 1, totalDF.index.size):
        predictValue = totalDF.iloc[i, 1] = predictValue * (
            1
            + np.sign(totalDF.iloc[i, 0] - predictValue)
            * np.exp(
                -np.abs((totalDF.iloc[i, 0] - predictValue) / predictValue) * float(TC)
            )
        )
    totalDF["Actual" + trendSuffix] = predictions["Actual" + trendSuffix]
    totalDF["Close"] = predictions.Close
    # plotByTimeDelta(totalDF.iloc[:,:-1])
    totalDF["ExpVariation" + trendSuffix] = annual_volatility(
        totalDF.Close, volatilityDays
    ) * np.sqrt(volatilityDays / 365.0)
    trendErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Trend) / totalDF.Close
    trendErrorAsStd = trendErrorPerc / totalDF["ExpVariation" + trendSuffix]
    predErrorPerc = (
        totalDF["Actual" + trendSuffix] - totalDF.Prediction
    ) / totalDF.Close
    predErrorAsStd = predErrorPerc / totalDF["ExpVariation" + trendSuffix]
    flatErrorPerc = (totalDF["Actual" + trendSuffix] - totalDF.Close) / totalDF.Close
    flatErrorStd = flatErrorPerc / totalDF["ExpVariation" + trendSuffix]
    print(
        "Mean Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.mean(), predErrorPerc.mean(), flatErrorPerc.mean())
    )
    print(
        "\nStd. Dev. Error Percent:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorPerc.std(), predErrorPerc.std(), flatErrorPerc.std())
    )
    print(
        "Mean Error StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.mean(), predErrorAsStd.mean(), flatErrorStd.mean())
    )
    print(
        "\nStd. Dev. StdDev Factor:\n\tTrend = %1.4f\n\tPrediction = %1.4f\n\tFlat = %1.4f"
        % (trendErrorAsStd.std(), predErrorAsStd.std(), flatErrorStd.std())
    )
    totalDF[["Close", "Actual" + trendSuffix, "Trend", "Prediction"]].plot()


def applyStaticLimitsToTrend_Vector(priceData, trend, limit=0.05):
    firstValidIndex = np.arange(trend.index.size)[pd.notnull(trend)].min()
    prediction = trend.copy()
    prevPrediction = trend.copy()
    prevPrediction[firstValidIndex + 1 :] = prevPrediction[firstValidIndex:-1]
    delta = np.clip((trend - prevPrediction) / prevPrediction, -limit, limit) + 1
    delta[firstValidIndex] = prevPrediction[firstValidIndex]
    loop = 0
    while True:
        loop += 1
        prevPrediction = delta.cumprod()
        prevPrediction.name = "PrevPrediction"
        pd.concat([priceData, trend, prevPrediction], axis=1).plot()
        plt.title("Loop %01i" % loop)
        plt.show()
        newDelta = np.clip((trend - prevPrediction) / prevPrediction, -limit, limit) + 1
        newDelta[firstValidIndex] = trend[firstValidIndex]
        if (newDelta[firstValidIndex:] == delta[firstValidIndex:]).all() or loop > 10:
            break
        else:
            delta = newDelta

    totalDF = pd.concat(
        [trend, pd.Series(index=trend.index, name="Prediction")], axis=1
    )
    totalDF.columns = ["Trend", "Prediction"]
    totalDF.iloc[firstValidIndex, 1] = totalDF.iloc[firstValidIndex, 0]
    for row in totalDF.index[firstValidIndex + 1 :]:
        pass


def volumeWeightedTrend(priceData, volumeData, predictionRange, displayStatus=False):
    """Predict where the current trends will be at x days out.
        o  processingSpan will determine the window of the smoothing function and will be the long
            period of the SMA pairs that determine where the trends will end.
        o  polyTrends will be a list of tuples representing a polynomial trend line
            the trendlines represent delta values from the last known price. It should be common when
            plotting the trend vs. the price to add the trend to the last known value to get the
            fit of the trend to the data. I will try to use the nomenclature of 'trend' to indicate the
            expected rise/fall and 'fit' to indicate the predicted close as compared to the actual close
            - The first valule will be the processing span that caclulated the trend
            - The second value will be a tuple of the upper and lower limits (min,max)
            - The third value is a list of 2nd deg polyfit parms adjusted so that the final date is the origin
                and the x-axis of the fit is the number of days relative to the final
        o  sinusoidal trends will be a list of tuples representing a sinusoidal trend line
            - The first valule will be the processing span that caclulated the trend
            - The second value will be a tuple of the upper and lower limits (min,max)
            - The third value is a list of parms for the sinusoidal function
                ~ [est_std, est_phase, est_stretch, est_mean] put in the equation:

            -
    Start with 2 years of data and a 180 day span
        1.  Create a smoothed price data set with the 180 day span
        2.  Classify by looking at 1st and 2nd derivatives
            o  is there a second derivative (i.e. the mean is outside 1 std of zero )
            o  is there a place where the trend changes?
                o  d1, d2 distributions are not normal (scipy.stats.normaltest(series)[1]>0.05)
                o  SMA's cross or diverge
            o  if trend changes, change data range to the range of the most recent trend
        3.  fit to trend and calculate the residual
        4.  is there a new trend or a sinusoidal in the residual
        5.  reduce down to the next lower processing span and repeat

debug:
spxFull = loadAllSPXData(sDB,columns=['Close','Volume'])
spxData = spxFull.Close
priceData = spxData[-504:]
volumeData = spxFull.iloc[-504:,-1]
polyTrends=[]
sinusoidalTrends=[]
displayStatus=True
predictionRange = 30
polyParms, sinusiodalParms = volumeWeightedTrend(priceData,volumeData,predictionRange,displayStatus=displayStatus)

to-do:
    adapt so that the functions roll off according to the strength of the R2
    """
    polyTrends = []
    sinusoidalTrends = []
    processingSpanOptions = {700: 180, 350: 90, 170: 44, 0: 22, -1: -1}
    processingCutoffs = np.array(processingSpanOptions.keys()).astype(int)
    historicalDays = (priceData.index.max() - priceData.index.min()).days
    startDate = priceData.index.min()
    processingSpan = processingSpanOptions[
        processingCutoffs[processingCutoffs <= historicalDays].max()
    ]
    weightingSeries = volumeData / volumeData.max()
    weightedSmooth = smooth(
        priceData[startDate:] * weightingSeries, processingSpan / 2 + 1
    ) / smooth(weightingSeries, processingSpan / 2 + 1)
    finalValue = weightedSmooth[-1]

    while processingSpan != -1 and predictionRange <= 2 * processingSpan:

        polyTrends, sinusoidalTrends = findTrendsWithinSpan(
            processingSpan,
            historicalDays,
            priceData[startDate:],
            volumeData[startDate:],
            predictionRange,
            polyTrends,
            sinusoidalTrends,
            finalValue,
            displayStatus=displayStatus,
        )

        historicalDays = {180: 365, 90: 180, 44: 90, 22: -1}[processingSpan]
        processingSpan = processingSpanOptions[
            processingCutoffs[processingCutoffs <= historicalDays].max()
        ]
        startDate = (
            priceData.index.max() - pd.Timedelta(days=historicalDays)
            if processingSpan > -1
            else priceData.index.max()
        )

    if displayStatus:
        newIndex = priceData.index[pd.notnull(priceData)]
        priceData[newIndex].plot()
        weightingSeries = volumeData[newIndex] / volumeData[newIndex].max()
        weightedSmooth = smooth(
            priceData[newIndex] * weightingSeries, predictionRange * 2
        ) / smooth(weightingSeries, predictionRange * 2)
        weightedSmooth.name = "Smoothed priceData"
        weightedSmooth.plot()
        finalValue = weightedSmooth[-1]
        xIndex = pd.date_range(
            newIndex.min(), newIndex.max() + pd.Timedelta(days=2 * predictionRange)
        )
        existingFit = finalValue + buildTrendFromParms(
            (xIndex - newIndex.max()).days, polyTrends, sinusoidalTrends
        )
        existingFit.index = xIndex
        existingFit.plot()
        plt.legend()
        plt.show()

    return polyTrends, sinusoidalTrends


def pctChngeTrend(priceData, volumeData, predictionRange, displayStatus=False):
    """Predict where the current trends will be at 'x'predictionRange days out.
        o  processingSpan will determine the window of the smoothing function and will be the long
            period of the SMA pairs that determine where the trends will end.
        o  polyTrends will be a list of tuples representing a polynomial trend line
            the trendlines represent delta values from the last known price. It should be common when
            plotting the trend vs. the price to add the trend to the last known value to get the
            fit of the trend to the data. I will try to use the nomenclature of 'trend' to indicate the
            expected rise/fall and 'fit' to indicate the predicted close as compared to the actual close
            - The first valule will be the processing span that caclulated the trend
            - The second value will be a tuple of the upper and lower limits (min,max)
            - The third value is a list of 2nd deg polyfit parms adjusted so that the final date is the origin
                and the x-axis of the fit is the number of days relative to the final
        o  sinusoidal trends will be a list of tuples representing a sinusoidal trend line
            - The first valule will be the processing span that caclulated the trend
            - The second value will be a tuple of the upper and lower limits (min,max)
            - The third value is a list of parms for the sinusoidal function
                ~ [est_std, est_phase, est_stretch, est_mean] put in the equation:

            -
    Start with 2 years of data and a 180 day span
        1.  calculate the pct change and smoothed dataset with the 180 day span
        2.  Classify by looking at 1st and 2nd derivatives
            o  is there a second derivative (i.e. the mean is outside 1 std of zero )
            o  is there a place where the trend changes?
                o  d1, d2 distributions are not normal (scipy.stats.normaltest(series)[1]>0.05)
                o  SMA's cross or diverge
            o  if trend changes, change data range to the range of the most recent trend
        3.  fit to trend and calculate the residual
        4.  is there a new trend or a sinusoidal in the residual
        5.  reduce down to the next lower processing span and repeat

debug:
current_date = pd.Timestamp()
spxData = loadAllSPXData(sDB)
priceData = spxData.iloc[-504:,spxData.columns.get_loc('Close')]
volumeData = spxData.iloc[-504:,spxData.columns.get_loc('Volume')]

selectDateRow = -504
date = spxData.index[selectDateRow]
priceData = spxData.iloc[selectDateRow-504:selectDateRow+1,spxData.columns.get_loc('Close')]
volumeData = spxData.iloc[selectDateRow-504:selectDateRow+1,spxData.columns.get_loc('Volume')]

polyTrends=[]
sinusoidalTrends=[]
displayStatus=True
predictionRange = 30
polyParms, sinusiodalParms = pctChngeTrend(priceData,volumeData,predictionRange,displayStatus=displayStatus)
    """
    polyTrends = []
    sinusoidalTrends = []
    processingSpanOptions = {700: 180, 350: 90, 170: 44, 0: 22, -1: -1}
    processingCutoffs = np.array(processingSpanOptions.keys()).astype(int)
    historicalDays = (priceData.index.max() - priceData.index.min()).days
    startDate = priceData.index.min()
    processingSpan = processingSpanOptions[
        processingCutoffs[processingCutoffs <= historicalDays].max()
    ]
    pctChange = priceData.pct_change(periods=processingSpan)
    finalValue = pctChange[-1]

    while processingSpan != -1 and predictionRange <= 2 * processingSpan:

        polyTrends, sinusoidalTrends = findTrendsWithinSpan(
            processingSpan,
            historicalDays,
            pctChange[startDate:],
            volumeData[startDate:],
            predictionRange,
            polyTrends,
            sinusoidalTrends,
            displayStatus=displayStatus,
        )

        historicalDays = {180: 365, 90: 180, 44: 90, 22: -1}[processingSpan]
        processingSpan = processingSpanOptions[
            processingCutoffs[processingCutoffs <= historicalDays].max()
        ]
        startDate = (
            pctChange.index.max() - pd.Timedelta(days=historicalDays)
            if processingSpan > -1
            else pctChange.index.max()
        )

    if displayStatus:
        newIndex = pctChange.index[pd.notnull(pctChange)]
        pctChange[newIndex].plot()
        weightingSeries = volumeData[newIndex] / volumeData[newIndex].max()
        weightedSmooth = smooth(
            pctChange[newIndex] * weightingSeries, predictionRange * 2
        ) / smooth(weightingSeries, predictionRange * 2)
        weightedSmooth.name = "Smoothed PctChange"
        weightedSmooth.plot()
        finalValue = weightedSmooth[-1]
        xIndex = pd.date_range(
            newIndex.min(), newIndex.max() + pd.Timedelta(days=2 * predictionRange)
        )
        existingFit = finalValue + buildTrendFromParms(
            (xIndex - newIndex.max()).days, polyTrends, sinusoidalTrends
        )
        existingFit.index = xIndex
        existingFit.plot()
        plt.legend()
        plt.show()

    return polyTrends, sinusoidalTrends


def findTrendsWithinSpan(
    processingSpan,
    historicalDays,
    timeSeries,
    seriesWeight,
    predictionRange,
    polyTrends,
    sinusoidalTrends,
    finalValue,
    displayStatus=False,
):
    # Smoothing a weighted list is done by convolving the smoothing window with the value/weighting product and the weights seperately and then dividing the convolved prod(value, weights) by the convolved weights
    # see an example here: https://stackoverflow.com/questions/46230222/smoothing-a-series-of-weighted-values-in-numpy-pandas
    if processingSpan <= 0:
        return polyTrends, sinusoidalTrends

    startDate = timeSeries.index.max() - pd.Timedelta(days=historicalDays)
    if displayStatus:
        print("Start processing with a window of %01i" % processingSpan)
    weightingSeries = seriesWeight[startDate:] / seriesWeight[startDate:].max()
    weightedSmooth = smooth(
        timeSeries[startDate:] * weightingSeries, processingSpan / 2 + 1
    ) / smooth(weightingSeries, processingSpan / 2 + 1)
    # finalValue = weightedSmooth[-1]
    weightedSmooth.name = "WeightedSmooth"

    resetDate, trendResets, sma_short, sma_long = searchForTrendResets(
        processingSpan, weightedSmooth
    )

    trendValid = False
    #   -----New Code - Try to level the entire field first ---------------------
    newIndex = weightedSmooth.index[pd.notnull(weightedSmooth)]
    xAxis = (newIndex - newIndex.max()).days
    existingFit = finalValue + buildTrendFromParms(xAxis, polyTrends, sinusoidalTrends)
    existingFit.index = newIndex
    assert np.isfinite(
        existingFit
    ).all(), "Fit previous trends: ExistingFit has a null value"
    residual = weightedSmooth[newIndex] - existingFit
    residual.name = "Residual"
    assert pd.notnull(residual).all(), "Post fit subtraction: residual has a null value"
    # fit polynomial trend of the residual
    parms = np.polyfit(xAxis, residual, 1)
    polyFit = pd.Series(data=np.polyval(parms, xAxis), index=newIndex, name="TrendFit")
    r2 = R2(residual, polyFit)
    if r2 > 0.25:
        trendValid = True
        parms = np.array([0] + parms.tolist())
        xMin, xMax = (
            max((newIndex.min() - newIndex.max()).days, -4.0 * processingSpan),
            processingSpan * 2,
        )
        # store the trend
        polyTrends.append((processingSpan, (xMin, xMax), parms))

    #   --------------- End New Code ---------------------------------------------
    # now focus on the relevant portion of trading that is the most recent
    newIndex = weightedSmooth[resetDate:][pd.notnull(weightedSmooth[resetDate:])].index

    if newIndex.size > processingSpan / 2:
        # find an estimation for the end of the trend (i.e. when the SMAs are expected to cross)
        xAxis = (newIndex - newIndex.max()).days
        smaDiffParms = np.polyfit(
            xAxis[-processingSpan / 2 :],
            (sma_short - sma_long)[-processingSpan / 2 :],
            1,
        )
        smaNextCross = int(-smaDiffParms[1] / smaDiffParms[0])
        if smaNextCross <= 0:
            smaNextCross = 99999
        if smaNextCross > processingSpan / 4:
            # subtract the existing trends from the trendData and assign the residuals to the trend data
            existingFit = finalValue + buildTrendFromParms(
                xAxis, polyTrends, sinusoidalTrends
            )
            existingFit.index = newIndex
            assert np.isfinite(
                existingFit
            ).all(), "Fit previous trends: ExistingFit has a null value"
            residual = weightedSmooth[newIndex] - existingFit
            assert pd.notnull(
                residual
            ).all(), "Post fit subtraction: residual has a null value"

            # fit polynomial trend of the residual
            parms = np.polyfit(xAxis, residual, 2)
            polyFit = pd.Series(
                data=np.polyval(parms, xAxis), index=newIndex, name="TrendFit"
            )
            # weightedSmooth[newIndex].plot();existingFit[newIndex].plot();residual[newIndex].plot();polyFit[newIndex].plot();plt.legend()
            r2Poly = R2(residual[-residual.size / 2 :], polyFit[-residual.size / 2 :])
            if displayStatus:
                print("Polynomial 2nd half R2 = %1.2f" % r2Poly)
            if r2Poly > 0.5:
                # find an estimation for the end of the trend (i.e. when the SMAs are expected to cross)
                smaDiffParms = np.polyfit(
                    xAxis[-processingSpan / 2 :],
                    (sma_short - sma_long)[-processingSpan / 2 :],
                    1,
                )
                xMin, xMax = (
                    max((newIndex.min() - newIndex.max()).days, -4.0 * processingSpan),
                    min(smaNextCross, processingSpan * 2),
                )
                # store the trend
                polyTrends.append((processingSpan, (xMin, xMax), parms))
                residual -= polyFit
                existingFit[newIndex] += polyFit
                # weightedSmooth[newIndex].plot();existingFit[newIndex].plot();plt.legend()
            assert pd.notnull(
                residual
            ).all(), "Post PolyTrend: TrendData has a null value"

    # attempt to optimize with a sinusoidal function
    newIndex = weightedSmooth[pd.notnull(weightedSmooth)][startDate:].index
    xAxis = (newIndex - newIndex.max()).days
    existingFit = finalValue + buildTrendFromParms(xAxis, polyTrends, sinusoidalTrends)
    existingFit.index = newIndex
    residual = weightedSmooth[newIndex] - existingFit
    sineParms, sineFit = fitWithSinusoidal(residual, displayStatus)
    r2Sine = R2(residual[-residual.size / 2 :], sineFit[-residual.size / 2 :])
    if displayStatus:
        print("Sinusoidal 2nd half R2 = %1.2f" % r2Sine)
    if r2Sine > 0.5:
        xMin, xMax = (
            max((newIndex.min() - newIndex.max()).days, -4.0 * processingSpan),
            processingSpan * 2,
        )
        sinusoidalTrends.append((processingSpan, (xMin, xMax), sineParms))
        residual = residual - sineFit
        existingFit[newIndex] += sineFit
        trendValid = True
    assert pd.notnull(
        residual
    ).all(), "Post Sinusoidal Trend: TrendData has a null value"

    if displayStatus and not trendValid:
        print(
            "SMA Signals crossed too close to the end for a %01i trend and no sinusoidal trend emerged"
            % processingSpan
        )
        # calculate an existing fit for the plotting
        xAxis = (newIndex - newIndex.max()).days
        existingFit = finalValue + buildTrendFromParms(
            xAxis, polyTrends, sinusoidalTrends
        )
        existingFit.index = newIndex

    if displayStatus:
        fig, axs = plt.subplots(3, 1, sharex=False)
        fig.set_figheight(9)
        fig.set_figwidth(10)
        fig.sca(axs[0])
        timeSeries[startDate:].plot()
        weightedSmooth.plot()
        sma_short.plot()
        sma_long.plot()
        plt.plot(existingFit.index, existingFit.values, "--", lw=3, label="TrendFit")
        plt.plot(trendResets, sma_short[trendResets], "ro", lw=5, label="TrendReset")
        axs[0].set_xlim(startDate, timeSeries.index.max())
        plt.legend()

        fig.sca(axs[1])
        residual = timeSeries[existingFit[startDate:].index] - existingFit[startDate:]
        residual.name = "Residual"
        residual.plot()
        axs[1].set_xlim(startDate, existingFit.index.max())

        fig.sca(axs[2])
        residual.hist()
        plt.tight_layout()
        plt.show()

    return polyTrends, sinusoidalTrends


def searchForTrendResets(processingSpan, weightedSmooth):
    """ Uses sma long and short periods to determine when the current trend will end
        o  Assumes the current trend will end when the SMA lines cross and stay crossed for
            at least a week
        returns the most recent trend Reset date, the list of all trend resets, the sma_short line and the sma_long line"""
    sma_short = weightedSmooth.rolling("%01id" % (processingSpan / 2)).mean()
    sma_short.name = "SMA_Short"
    sma_long = weightedSmooth.rolling("%01id" % (processingSpan)).mean()
    sma_long.name = "SMA_Long"
    validRows = np.logical_and(np.isfinite(sma_short), np.isfinite(sma_long))
    # find where lines cross
    shortOverLong = sma_short > sma_long
    longOverShort = sma_short <= sma_long
    # shift one so that the crossover points are rows wehre one is true but not the other
    longOverShort[:-1] = longOverShort[1:]
    trendResets = np.logical_and(
        validRows,
        np.logical_or(
            np.logical_and(shortOverLong, longOverShort),
            np.logical_and(~shortOverLong, ~longOverShort),
        ),
    )
    trendResets = sma_short.index[trendResets]
    # ensure that at trend resets are valid
    if trendResets.size > 2:
        # valid trendResets last for more than 1 week and are not within a month of the final day in the dataset
        trendResets = trendResets[
            trendResets < shortOverLong.index.max() - pd.Timedelta(days=30)
        ]
        validItems = list((trendResets[1:] - trendResets[:-1]) > pd.Timedelta(days=7))
        # logical and with the shifted valid list provides that both of the invalid items are removed
        validItems = np.logical_and(
            validItems + validItems[-1:], validItems[:1] + validItems
        )
        trendResets = trendResets[validItems]
        resetDate = trendResets.max()
    elif trendResets.size == 1:
        trendResets = trendResets[
            trendResets < shortOverLong.index.max() - pd.Timedelta(days=30)
        ]
        resetDate = trendResets.max()
    else:
        resetDate = weightedSmooth.index.min()

    return resetDate, trendResets, sma_short, sma_long


def fitWithSinusoidal_0(series, showFitSummaryPlot=False):
    validIndices = series.index[pd.notnull(series)]
    xAxis = (validIndices - validIndices.max()).days
    guess_mean = np.mean(series)
    guess_std = 3 * np.std(series) / (2 ** 0.5)
    # the best guess at where then phase should be is the first time the function crosses the mean from low to high
    crossMean = np.array(
        xAxis[
            list(
                np.logical_and(
                    series[validIndices][:-1] <= guess_mean,
                    series[validIndices][1:] >= guess_mean,
                )
            )
            + [False]
        ]
    )
    # the next best guess for strech can be estimated by half of the average time between records of 'crossMean'
    if crossMean.size > 1:
        guess_phase = crossMean.max()
        guess_stretch = (crossMean[1:] - crossMean[:-1]).mean() / 2.0
    else:
        return (
            [0, 0, 0, 0],
            pd.Series(data=np.zeros(series.shape), index=series.index, name="Sine Fit"),
        )
    # fit the data
    optimize_func = (
        lambda x: x[0] * np.sin(((xAxis + x[1]) / x[2]) * np.pi)
        + x[3]
        - series[validIndices].values
    )
    est_std, est_phase, est_stretch, est_mean = optimize.leastsq(
        optimize_func, [[guess_std, guess_phase, guess_stretch, guess_mean]]
    )[0]
    xAxis = (series.index - series.index.max()).days
    series_fit = (
        est_std * np.sin(((xAxis + est_phase) / est_stretch) * np.pi) + est_mean
    )

    if showFitSummaryPlot:
        series_first_guess = (
            guess_std * np.sin(((xAxis + guess_phase) / guess_stretch) * np.pi)
            + guess_mean
        )
        plt.plot(xAxis, series, ".")
        plt.plot(xAxis, series_first_guess, label="first guess")
        plt.plot(xAxis, series_fit, label="after fitting")
        plt.legend()
        plt.show()
    return (
        [est_std, est_phase, est_stretch, est_mean],
        pd.Series(data=series_fit, index=series.index, name="Sine Fit"),
        R2(series.values, series_fit.values),
    )


def fitAndPlotTrendForDate(
    date,
    predictionRange=30,
    plotForwardRange=180,
    trendType="pctChng",
    polyTrends=None,
    sinusoidalTrends=None,
    displayStatus=True,
):
    date = Util.formatDateInput(date)
    spxClose = loadAllSPXData(sDB, columns="Close")
    selectDate = spxClose.index.get_loc(date)
    priceData = spxClose[max(0, selectDate - 504) : selectDate + 1]
    volumeData = loadAllSPXData(sDB, columns="Volume")[priceData.index]
    if polyTrends is None:
        if trendType.upper() == "PCTCHNG":
            polyTrends, sinusoidalTrends = pctChngeTrend(
                priceData, volumeData, predictionRange, displayStatus=displayStatus
            )
        else:
            polyTrends, sinusoidalTrends = volumeWeightedTrend(
                priceData, volumeData, predictionRange, displayStatus=displayStatus
            )

    # xIndex = pd.bdate_range(priceData.index.min(),date+pd.Timedelta(days=plotForwardRange))
    xIndex = pd.date_range(
        priceData.index.min(), date + pd.Timedelta(days=plotForwardRange)
    )
    rows = (predictionRange * 252) / 365 + (1 - (predictionRange * 252) / 365 % 2)
    if trendType.upper() == "PCTCHNG":
        weightingSeries = volumeData / volumeData.max()
        weightedSmoothClose = smooth(priceData * weightingSeries, rows) / smooth(
            weightingSeries, rows
        )
        pctChange = priceData.pct_change(periods=rows)
        finalSmoothedPctChange = smooth(pctChange, rows)[-1]
        trendPctChange = (
            1
            + buildTrendFromParms((xIndex - date).days, polyTrends, sinusoidalTrends)
            + finalSmoothedPctChange
        )
        smoothClose_Prev = weightedSmoothClose.copy()
        smoothClose_Prev.index = (weightingSeries.index - date).days + predictionRange
        trend = pd.Series(index=trendPctChange.index, name=trendPctChange.name)
        trend[smoothClose_Prev.index] = (
            smoothClose_Prev * trendPctChange[smoothClose_Prev.index]
        )
        lastValidIndex = trend.index[pd.notnull(trend)].max()
        while True:
            setIndices = np.arange(
                lastValidIndex + 1,
                min(lastValidIndex + 1 + predictionRange, trend.index.max()),
            )
            if setIndices.size == 0:
                break
            trend.loc[setIndices] = trend.loc[setIndices - predictionRange].values * (
                trendPctChange.loc[setIndices].values
            )
            lastValidIndex = setIndices.max()
            if lastValidIndex == trend.index.max():
                break
    else:
        weightingSeries = volumeData / volumeData.max()
        finalCloseTrend = (
            smooth(priceData * weightingSeries, rows)
            / smooth(weightingSeries, rows)[-1]
        )
        trend = (
            buildTrendFromParms((xIndex - date).days, polyTrends, sinusoidalTrends)
            + finalCloseTrend
        )

    trend.index = xIndex
    trend = trend[trend > 0.001]
    fig, axs = plt.subplots(1, 1, sharex=False)
    fig.set_figheight(8)
    fig.set_figwidth(8)
    # fig.sca(axs[0])
    trend.plot()
    spxClose.loc[xIndex.min() : xIndex.max()].plot()


def fitAndPlotTrend(
    priceData,
    predictionRange,
    polyTrends=None,
    sinusoidalTrends=None,
    volumeData=None,
    displayStatus=True,
):
    if polyTrends is None or sinusoidalTrends is None:
        if priceData is None:
            priceData = loadAllSPXData(sDB, columns="Close")[-504:]
        if volumeData is None:
            volumeData = loadAllSPXData(sDB, columns="Volume")[priceData.index]
        polyTrends, sinusoidalTrends = volumeWeightedTrend(
            priceData, volumeData, predictionRange, displayStatus=displayStatus
        )
        predictionRange *= 2
    else:
        xIndex = pd.bdate_range(
            priceData.index.min(),
            priceData.index.max() + pd.Timedelta(days=2 * predictionRange),
        )
        trend = (
            buildTrendFromParms(
                (xIndex - priceData.index.max()).days, polyTrends, sinusoidalTrends
            )
            + priceData[-1]
        )
        trend.index = xIndex
        trend = trend[trend > 0.001]
        fig, axs = plt.subplots(1, 1, sharex=False)
        fig.set_figheight(8)
        fig.set_figwidth(8)
        # fig.sca(axs[0])
        trend.plot()
        priceData.plot()


def fitWithSinusoidal(series, showFitSummaryPlot=False):
    validIndices = series.index[pd.notnull(series)]
    xAxis = (validIndices - validIndices.max()).days
    guess_mean = np.mean(series)
    guess_std = 3 * np.std(series) / (2 ** 0.5)
    # the best guess at where then phase should be is the first time the function crosses the mean from low to high
    crossMean = np.array(
        xAxis[
            list(
                np.logical_and(
                    series[validIndices][:-1] <= guess_mean,
                    series[validIndices][1:] >= guess_mean,
                )
            )
            + [False]
        ]
    )
    # the next best guess for strech can be estimated by half of the average time between records of 'crossMean'
    if crossMean.size > 1:
        guess_phase = crossMean.max()
        guess_stretch = (crossMean[1:] - crossMean[:-1]).mean() / 2.0
    else:
        return (
            [0, 0, 0, 0],
            pd.Series(data=np.zeros(series.shape), index=series.index, name="Sine Fit"),
        )
    # fit the data
    optimize_func = (
        lambda x: x[0] * np.sin(((xAxis + x[1]) / x[2]) * np.pi)
        + x[3]
        - series[validIndices].values
    )
    est_std, est_phase, est_stretch, est_mean = optimize.leastsq(
        optimize_func, [[guess_std, guess_phase, guess_stretch, guess_mean]]
    )[0]
    xAxis = (series.index - series.index.max()).days
    series_fit = (
        est_std * np.sin(((xAxis + est_phase) / est_stretch) * np.pi) + est_mean
    )

    if showFitSummaryPlot:
        series_first_guess = (
            guess_std * np.sin(((xAxis + guess_phase) / guess_stretch) * np.pi)
            + guess_mean
        )
        plt.plot(xAxis, series, ".")
        plt.plot(xAxis, series_first_guess, label="first guess")
        plt.plot(xAxis, series_fit, label="after fitting")
        plt.legend()
        plt.show()
    return (
        [est_std, est_phase, est_stretch, est_mean],
        pd.Series(data=series_fit, index=series.index, name="Sine Fit"),
    )


def exploreConvolvingWindows():
    spxData = loadAllSPXData(predictionModel.stockDB, columns="Close")
    spxData.name = "SPX_Close"
    convolvedList = [spxData]
    smaList = [spxData]
    trendSpans = [15, 30, 45, 60, 90, 180, 365]
    for span in trendSpans:
        convolved = smooth(spxData, window_len=span * 252 / 365)
        convolved.name = "Window_%03id" % span
        convolvedList.append(convolved)
        sma = spxData.rolling("%01id" % span).mean()
        sma.name = "SMA_%03id" % span
        smaList.append(sma)
        print("*" * 15 + "Close vs. Convolved vs. SMA for Span = %01i" % span)
        pd.concat([spxData, convolved, sma], axis=1).iloc[-252:, :].plot()
        plt.show()
    plotByTimeDelta(pd.concat(convolvedList, axis=1))


def PredictMean_SMA_Convolved(days=30):
    """for 30 days, the weighted average of the delta between the 90-180 day SMA and the 30-90 day SMA is working well
    I want to try the 45-90 instead of the 30-90
        o  This method is a good way to get the general trend, but it is also reproducing the features that have already happened
        o  need to "filter the data so that the trend can avoid blindly following the ant path on the correct trajectory
            o  Basically if it is in the past, the trend already missed it and it is water under the bridge. What is
                needed is the logic to determine if it was transient or a real trend
            o  determine a "window" where effects that happen within that rolling window are considered transient
                and are smoothed out.
    """
    trendSpans = [45, 90, 180]
    localSpans = [7, 14]
    window = 61
    spxData = loadAllSPXData(predictionModel.stockDB, columns="Close")
    spxData.name = "SPX_Close"
    vixData = predictionModel.stockDB.loadFromDB(dbKey="ixVIX", columns="Close")
    vixData.name = "IV30"
    vxvData = predictionModel.stockDB.loadFromDB(dbKey="ixVXV", columns="Close")
    vxvData.name = "IV90"
    # save the volatility data for the span
    vol30Day = annual_volatility(spxData, 30)
    vol30Day.name = "Ann_Volatility_030Days"
    adjVol = vol30Day * np.sqrt(days / 365.0)
    adjVol.name = "PeriodAdjVolatility"

    columnList = [spxData, vixData, vxvData, vol30Day, adjVol]
    # save the actual future value
    colSuffix = "_%03iDays" % days
    shiftRows = int(round(days * 252.0 / 365.0))
    endDays = spxData.index[shiftRows:]
    startDays = spxData.index[:-shiftRows]
    actualClose = spxData[endDays].copy()
    actualClose.index = startDays
    colName = "FuturePrice" + colSuffix
    actualClose.name = colName
    columnList.append(actualClose)
    # future price to start price delta
    futureDelta = actualClose[startDays] - spxData[startDays]
    colName = "FutureDelta" + colSuffix
    futureDelta.name = colName
    columnList.append(futureDelta)
    colName = "FutureDeltaPerc" + colSuffix
    futureDeltaPerc = futureDelta / spxData
    futureDeltaPerc.name = colName
    columnList.append(futureDeltaPerc)
    colName = "FutureDeltaStd" + colSuffix
    futureDeltaStd = futureDeltaPerc / adjVol
    futureDeltaStd.name = colName
    columnList.append(futureDeltaStd)

    for span in trendSpans:
        # save the moving average for the span
        colName = "SMA%03i" % span
        sma = spxData.rolling("%01id" % span).mean()
        sma.name = colName
        columnList.append(sma)
        for localSlopeDays in localSpans:
            smaLocalSlope, smaLocalR2 = calculateTrend_BySpan(sma, localSlopeDays)
            # slope of the SMA
            colName = "SMA%03i" % span + "_%02iD_Slope" % localSlopeDays
            smaLocalSlope.name = colName
            columnList.append(smaLocalSlope)
            # R2 of the SMA trend
            colName = "SMA%03i" % span + "_%02iD_R2" % localSlopeDays
            smaLocalR2.name = colName
            columnList.append(smaLocalR2)

    researchData = pd.concat(columnList, axis=1)
    # calculate the SMA interaction columns
    spanCombinations = [(45, 90), (90, 180)]
    for shortSpan, longSpan in spanCombinations:
        print(
            "Calculating Indicators for SMA combination (%03i, %03i)"
            % (shortSpan, longSpan)
        )
        colNameDiff = "SMADiff_%03i_%03i" % (shortSpan, longSpan)
        smaDiff = (
            researchData["SMA%03i" % shortSpan] - researchData["SMA%03i" % longSpan]
        )
        researchData[colNameDiff] = smaDiff
        for localSlopeDays in localSpans:
            # get the 1st and second derivative of the SMADiff and the diff,sum, and mean of the sma slopes
            # 1st derivative of the SMAdiff
            d1_smaDiff, smaLocalR2 = calculateTrend_BySpan(smaDiff, localSlopeDays)
            # slope of the SMADiff
            d1_smaDiffName = colNameDiff + "_%02iD_d1" % localSlopeDays
            smaLocalSlope.name = d1_smaDiffName
            researchData[d1_smaDiffName] = d1_smaDiff
            # 2nd derivative - only use the largest span
            d2_smaDiff, smaLocalR2 = calculateTrend_BySpan(d1_smaDiff, localSpans[-1])
            # slope of the 1st derivative
            d2_smaDiffName = colNameDiff + "_%02iD_d2" % localSlopeDays
            smaLocalSlope.name = d2_smaDiffName
            researchData[d2_smaDiffName] = d2_smaDiff

            # difference, sum, mean, and weighted mean of the slopes
            shortSMA_Slope = researchData[
                "SMA%03i" % shortSpan + "_%02iD_Slope" % localSlopeDays
            ]
            longSMA_Slope = researchData[
                "SMA%03i" % longSpan + "_%02iD_Slope" % localSlopeDays
            ]
            colName = "SMA_SlopeDiff_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = shortSMA_Slope - longSMA_Slope

            colName = "SMA_SlopeSum_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = shortSMA_Slope + longSMA_Slope

            colName = "SMA_SlopeMean_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = (shortSMA_Slope + longSMA_Slope) / 2

            colName = "SMA_SlopeWeightR2_%03i_%03i" % (shortSpan, longSpan)
            shortSMA_R2 = researchData[
                "SMA%03i" % shortSpan + "_%02iD_R2" % localSlopeDays
            ]
            longSMA_R2 = researchData[
                "SMA%03i" % longSpan + "_%02iD_R2" % localSlopeDays
            ]
            researchData[colName] = (
                shortSMA_Slope * shortSMA_R2 - longSMA_Slope * longSMA_R2
            ) / (shortSMA_R2 + longSMA_R2)
    window = 21
    researchData["ShortPrediction"] = researchData.SMADiff_045_090 * days / 45
    researchData["LongPrediction"] = researchData.SMADiff_090_180 * days / 90
    researchData["WeightedAvgSMADiff"] = predictedDiff = (
        researchData.ShortPrediction * 45 + researchData.LongPrediction * 90
    ) / 135
    reducedResearchData = {
        "Prediction": pd.concat(
            [
                researchData["FutureDelta_030Days"],
                (researchData.ShortPrediction * 45 + researchData.LongPrediction * 90)
                / 135,
            ],
            axis=1,
        ),
        "Delta": pd.concat(
            [
                researchData["FutureDelta_030Days"],
                researchData["ShortPrediction"],
                researchData["LongPrediction"],
                researchData["WeightedAvgSMADiff"],
            ],
            axis=1,
        ),
        "Percent": pd.concat(
            [
                researchData["FutureDeltaPerc_030Days"],
                researchData["ShortPrediction"] / researchData.SPX_Close,
                researchData["LongPrediction"] / researchData.SPX_Close,
                (researchData["WeightedAvgSMADiff"]) / researchData.SPX_Close,
            ],
            axis=1,
        ),
        "Std Dev": pd.concat(
            [
                researchData["FutureDeltaStd_030Days"],
                researchData["ShortPrediction"] / researchData.SPX_Close / adjVol,
                researchData["LongPrediction"] / researchData.SPX_Close / adjVol,
                researchData["WeightedAvgSMADiff"] / researchData.SPX_Close / adjVol,
            ],
            axis=1,
        ),
    }

    reducedResearchData["Prediction"].columns = [
        "FutureDelta_030Days",
        "WeightedAvgSMADiff",
    ]
    reducedResearchData["Delta"].columns = [
        "FutureDelta_030Days",
        "ShortPrediction_Delta",
        "LongPrediction_Delta",
        "WeightedAvgSMADiff_Delta",
    ]
    reducedResearchData["Percent"].columns = [
        "FutureDeltaPerc_030Days",
        "ShortPrediction_Percent",
        "LongPrediction_Percent",
        "WeightedAvgSMADiff_Percent",
    ]
    reducedResearchData["Std Dev"].columns = [
        "FutureDeltaStd_030Days",
        "ShortPrediction_Std",
        "LongPrediction_Std",
        "WeightedAvgSMADiff_Std",
    ]

    plotByTimeDelta(reducedResearchData["Percent"])

    plotXYWithMeanAndStd(
        reducedResearchData["Percent"].FutureDeltaPerc_030Days,
        reducedResearchData["Percent"].WeightedAvgSMADiff_Percent,
    )
    plotXYWithMeanAndStd(
        reducedResearchData["Std Dev"].FutureDeltaStd_030Days,
        reducedResearchData["Std Dev"].WeightedAvgSMADiff_Std,
    )

    print("Prediction And Actual Vs Time")
    prediction = spxData + predictedDiff
    prediction.name = "Predicted_Close"

    stdDevFactor = 1
    resultsDF = pd.concat(
        [
            actualClose,
            (smooth(spxData, window) + predictedDiff),
            actualClose * (1 + adjVol * stdDevFactor),
            actualClose * (1 - adjVol * stdDevFactor),
        ],
        axis=1,
    )
    resultsDF.columns = [
        "Actual_%01iDay" % days,
        "Prediction_%01iDay" % days,
        "Plus_%01i_StdDev" % stdDevFactor,
        "Minus_%01i_StdDev" % stdDevFactor,
    ]
    plotByTimeDelta(resultsDF)

    print("Prediction Offset From Actual / Period Adj Volatility")
    offset = (
        ((smooth(spxData, window) + predictedDiff) - actualClose) / spxData
    ) / adjVol
    offset = offset[pd.notnull(offset)]
    offset.name = "Offset"
    #    validDates = np.inter
    plotXYWithMeanAndStd(futureDelta[offset.index], offset)
    fitStats = stats.norm.fit(offset)
    print("Offset Stats are: " + str(fitStats))
    normFitObject = stats.norm(loc=fitStats[0], scale=fitStats[1])
    xHist = np.linspace(offset.min(), offset.max(), num=100)
    yHist = normFitObject.pdf(xHist) * offset.size
    plt.hist(offset)
    plt.plot(xHist, yHist)
    plt.show()

    return reducedResearchData, researchData


def PredictMean_SMA(days=30):
    """for 30 days, the weighted average of the delta between the 90-180 day SMA and the 30-90 day SMA is working well
    I want to try the 45-90 instead of the 30-90
        o  This method is a good way to get the general trend, but it is also reproducing the features that have already happened
        o  need to "filter the data so that the trend can avoid blindly following the ant path on the correct trajectory
            o  Basically if it is in the past, the trend already missed it and it is water under the bridge. What is
                needed is the logic to determine if it was transient or a real trend
            o  determine a "window" where effects that happen within that rolling window are considered transient
                and are smoothed out.
    """
    trendSpans = [45, 90, 180]
    localSpans = [7, 14]
    spxData = loadAllSPXData(predictionModel.stockDB, columns="Close")
    spxData.name = "SPX_Close"
    vixData = predictionModel.stockDB.loadFromDB(dbKey="ixVIX", columns="Close")
    vixData.name = "IV30"
    vxvData = predictionModel.stockDB.loadFromDB(dbKey="ixVXV", columns="Close")
    vxvData.name = "IV90"
    # save the volatility data for the span
    vol30Day = annual_volatility(spxData, 30)
    vol30Day.name = "Ann_Volatility_030Days"
    adjVol = vol30Day * np.sqrt(days / 365.0)
    adjVol.name = "PeriodAdjVolatility"

    columnList = [spxData, vixData, vxvData, vol30Day, adjVol]
    # save the actual future value
    colSuffix = "_%03iDays" % days
    shiftRows = int(round(days * 252.0 / 365.0))
    endDays = spxData.index[shiftRows:]
    startDays = spxData.index[:-shiftRows]
    actualClose = spxData[endDays].copy()
    actualClose.index = startDays
    colName = "FuturePrice" + colSuffix
    actualClose.name = colName
    columnList.append(actualClose)
    # future price to start price delta
    futureDelta = actualClose[startDays] - spxData[startDays]
    colName = "FutureDelta" + colSuffix
    futureDelta.name = colName
    columnList.append(futureDelta)
    colName = "FutureDeltaPerc" + colSuffix
    futureDeltaPerc = futureDelta / spxData
    futureDeltaPerc.name = colName
    columnList.append(futureDeltaPerc)
    colName = "FutureDeltaStd" + colSuffix
    futureDeltaStd = futureDeltaPerc / adjVol
    futureDeltaStd.name = colName
    columnList.append(futureDeltaStd)

    for span in trendSpans:
        # save the moving average for the span
        colName = "SMA%03i" % span
        sma = spxData.rolling("%01id" % span).mean()
        sma.name = colName
        columnList.append(sma)
        for localSlopeDays in localSpans:
            smaLocalSlope, smaLocalR2 = calculateTrend_BySpan(sma, localSlopeDays)
            # slope of the SMA
            colName = "SMA%03i" % span + "_%02iD_Slope" % localSlopeDays
            smaLocalSlope.name = colName
            columnList.append(smaLocalSlope)
            # R2 of the SMA trend
            colName = "SMA%03i" % span + "_%02iD_R2" % localSlopeDays
            smaLocalR2.name = colName
            columnList.append(smaLocalR2)

    researchData = pd.concat(columnList, axis=1)
    # calculate the SMA interaction columns
    spanCombinations = [(45, 90), (90, 180)]
    for shortSpan, longSpan in spanCombinations:
        print(
            "Calculating Indicators for SMA combination (%03i, %03i)"
            % (shortSpan, longSpan)
        )
        colNameDiff = "SMADiff_%03i_%03i" % (shortSpan, longSpan)
        smaDiff = (
            researchData["SMA%03i" % shortSpan] - researchData["SMA%03i" % longSpan]
        )
        researchData[colNameDiff] = smaDiff
        for localSlopeDays in localSpans:
            # get the 1st and second derivative of the SMADiff and the diff,sum, and mean of the sma slopes
            # 1st derivative of the SMAdiff
            d1_smaDiff, smaLocalR2 = calculateTrend_BySpan(smaDiff, localSlopeDays)
            # slope of the SMADiff
            d1_smaDiffName = colNameDiff + "_%02iD_d1" % localSlopeDays
            smaLocalSlope.name = d1_smaDiffName
            researchData[d1_smaDiffName] = d1_smaDiff
            # 2nd derivative - only use the largest span
            d2_smaDiff, smaLocalR2 = calculateTrend_BySpan(d1_smaDiff, localSpans[-1])
            # slope of the 1st derivative
            d2_smaDiffName = colNameDiff + "_%02iD_d2" % localSlopeDays
            smaLocalSlope.name = d2_smaDiffName
            researchData[d2_smaDiffName] = d2_smaDiff

            # difference, sum, mean, and weighted mean of the slopes
            shortSMA_Slope = researchData[
                "SMA%03i" % shortSpan + "_%02iD_Slope" % localSlopeDays
            ]
            longSMA_Slope = researchData[
                "SMA%03i" % longSpan + "_%02iD_Slope" % localSlopeDays
            ]
            colName = "SMA_SlopeDiff_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = shortSMA_Slope - longSMA_Slope

            colName = "SMA_SlopeSum_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = shortSMA_Slope + longSMA_Slope

            colName = "SMA_SlopeMean_%03i_%03i" % (shortSpan, longSpan)
            researchData[colName] = (shortSMA_Slope + longSMA_Slope) / 2

            colName = "SMA_SlopeWeightR2_%03i_%03i" % (shortSpan, longSpan)
            shortSMA_R2 = researchData[
                "SMA%03i" % shortSpan + "_%02iD_R2" % localSlopeDays
            ]
            longSMA_R2 = researchData[
                "SMA%03i" % longSpan + "_%02iD_R2" % localSlopeDays
            ]
            researchData[colName] = (
                shortSMA_Slope * shortSMA_R2 - longSMA_Slope * longSMA_R2
            ) / (shortSMA_R2 + longSMA_R2)
    researchData["ShortPrediction"] = researchData.SMADiff_045_090 * days / 45
    researchData["LongPrediction"] = researchData.SMADiff_090_180 * days / 90
    researchData["WeightedAvgSMADiff"] = predictedDiff = (
        researchData.ShortPrediction * 45 + researchData.LongPrediction * 90
    ) / 135
    reducedResearchData = {
        "Prediction": pd.concat(
            [
                researchData["FutureDelta_030Days"],
                (researchData.ShortPrediction * 45 + researchData.LongPrediction * 90)
                / 135,
            ],
            axis=1,
        ),
        "Delta": pd.concat(
            [
                researchData["FutureDelta_030Days"],
                researchData["ShortPrediction"],
                researchData["LongPrediction"],
                researchData["WeightedAvgSMADiff"],
            ],
            axis=1,
        ),
        "Percent": pd.concat(
            [
                researchData["FutureDeltaPerc_030Days"],
                researchData["ShortPrediction"] / researchData.SPX_Close,
                researchData["LongPrediction"] / researchData.SPX_Close,
                (researchData["WeightedAvgSMADiff"]) / researchData.SPX_Close,
            ],
            axis=1,
        ),
        "Std": pd.concat(
            [
                researchData["FutureDeltaStd_030Days"],
                (researchData["ShortPrediction"] / researchData.SPX_Close) / adjVol,
                (researchData["LongPrediction"] / researchData.SPX_Close) / adjVol,
                ((researchData["WeightedAvgSMADiff"]) / researchData.SPX_Close)
                / adjVol,
            ],
            axis=1,
        ),
    }

    reducedResearchData["Prediction"].columns = [
        "FutureDelta_030Days",
        "WeightedAvgSMADiff",
    ]
    reducedResearchData["Delta"].columns = [
        "FutureDelta_030Days",
        "ShortPrediction_Delta",
        "LongPrediction_Delta",
        "WeightedAvgSMADiff_Delta",
    ]
    reducedResearchData["Percent"].columns = [
        "FutureDeltaPerc_030Days",
        "ShortPrediction_Percent",
        "LongPrediction_Percent",
        "WeightedAvgSMADiff_Percent",
    ]
    reducedResearchData["Std Dev"].columns = [
        "FutureDeltaStd_030Days",
        "ShortPrediction_Std",
        "LongPrediction_Std",
        "WeightedAvgSMADiff_Std",
    ]

    plotByTimeDelta(reducedResearchData["Percent"])

    plotXYWithMeanAndStd(
        reducedResearchData["Percent"].FutureDeltaPerc_030Days,
        reducedResearchData["Percent"].WeightedAvgSMADiff_Percent,
    )
    plotXYWithMeanAndStd(
        reducedResearchData["Std Dev"].FutureDeltaStd_030Days,
        reducedResearchData["Std Dev"].WeightedAvgSMADiff_Std,
    )

    print("Prediction And Actual Vs Time")
    prediction = spxData + predictedDiff
    prediction.name = "Predicted_Close"

    stdDevFactor = 1
    resultsDF = pd.concat(
        [
            actualClose,
            (spxData + predictedDiff),
            actualClose * (1 + adjVol * stdDevFactor),
            actualClose * (1 - adjVol * stdDevFactor),
        ],
        axis=1,
    )
    resultsDF.columns = [
        "Actual_%01iDay" % days,
        "Prediction_%01iDay" % days,
        "Plus_%01i_StdDev" % stdDevFactor,
        "Minus_%01i_StdDev" % stdDevFactor,
    ]
    plotByTimeDelta(resultsDF)

    print("Prediction Offset From Actual / Period Adj Volatility")
    offset = ((predictedDiff - futureDelta) / spxData) / adjVol
    offset = offset[pd.notnull(offset)]
    offset.name = "Offset"
    #    validDates = np.inter
    plotXYWithMeanAndStd(futureDelta[offset.index], offset)
    fitStats = stats.norm.fit(offset)
    normFitObject = stats.norm(loc=fitStats[0], scale=fitStats[1])
    xHist = np.linspace(offset.min(), offset.max(), num=100)
    yHist = normFitObject.pdf(xHist)
    plt.hist(offset)
    plt.plot()

    return reducedResearchData, researchData


def studyIndicatorsVsPerformance(
    smaBool=True,
    linearBool=True,
    quadraticBool=False,
    cubicBool=False,
    splineBool=False,
):
    trendSpans = [7, 14, 30, 60, 90, 180, 270, 365]
    localSpans = [7, 14]
    spxData = loadAllSPXData(predictionModel.stockDB, columns="Close")
    spxData.name = "SPX_Close"
    vixData = predictionModel.stockDB.loadFromDB(dbKey="ixVIX", columns="Close")
    vixData.name = "IV30"
    vxvData = predictionModel.stockDB.loadFromDB(dbKey="ixVXV", columns="Close")
    vxvData.name = "IV90"
    columnList = [spxData, vixData, vxvData]
    # group the columns for easy plotting and calculations of columns with similar units
    colGroups = {
        "Price": ["SPX_Close"],
        "PriceDelta": [],
        "FururePrice": [],
        "FutureDelta": [],
        "RawPrice": ["SPX_Close"],
        "Volatility": ["IV30", "IV90"],
        "Slope": [],
        "RawSlope": [],
        "R2": [],
        "RawR2": [],
        "SlopeR2": [],
        "SMA": [],
        "SMADiff": [],
        "SMADiff_d1": [],
        "SMADiff_d2": [],
        "SMARatio": [],
        "SMARatio_d1": [],
        "SMARatio_d2": [],
        "SMA_Slope": [],
        "SMA_R2": [],
        "SMA_SlopeDiff": [],
        "SMA_SlopeSum": [],
        "SMA_SlopeMean": [],
        "SMA_SlopeR2Weighted": [],
        "dSlope": [],
        "SplineFit": [],
        "d1": [],
        "d2": [],
        "d1Quad": [],
        "d2Quad": [],
        "QuadDelta": [],
        "d1Cubic": [],
        "d2Cubic": [],
        "CubicDelta": [],
    }

    def polyFit_PackParms_RollingWrapper(series, deg=2):
        """Fits the values of the series vs. the position in the series to a polynomial of degree 'deg' then
            transforms the fit to predict the price delta moving forward
            o  Returns a string (packaged) of the parms in the format 'deg_p[0]_..._p[deg]_xShift'
            o  Meant to be used in conjunction with the polyFit_UnpackParms function"""
        x = range(series.size)
        xShift = x[-1]
        parms = np.array([deg] + [0.0] * (deg + 1) + [xShift])
        parms[1:-1] = np.polyfit(x, series, 2)
        parms[-2] -= np.polyval(parms[1:-1], xShift)
        return "_".join(parms.astype(str))

    #        return parms[1]
    def polyFit_UnpackParms(parmStr):
        parms = np.array(parmStr.split("_")).astype(float)
        deg = parms[0]
        xShift = parms[-1]
        parms = parms[1:-1]
        return deg, parms, xShift

    def polyFit_FutureDelta_RollingWrapper(series, marketDays, deg=2):
        x = range(series.size)
        parms = np.polyfit(x, series, deg)
        delta = np.polyval(parms, x[-1] + marketDays) - np.polyval(parms, x[-1])
        # easy correction is to enforce a boundary that the stock won't fall more than 90%
        return max(delta, series[-1] * 0.9)

    def polyFit_dn_RollingWrapper(series, deg=2, derivative=2):
        if derivative > deg:
            return 0
        x = range(series.size)
        parms = np.polyfit(x, series, deg)
        values = np.array([x[-1]] * (parms.size - 1) + [1])
        exponents = range(deg + 1)
        exponents.reverse()
        exponents = np.array(exponents)
        for i in range(derivative):
            values = values[1:]
            parms = parms[:-1] * exponents[:-1]
            exponents = exponents[1:]
        return (parms * values).sum()

    #    def polyFit_Deg2_Delta_RollingWrapper(series,predicted_date):
    #        x = series.index-series.index[0]
    #        parms = np.polyfit(x,series,2)
    #        return np.polyval(parms,x[])

    # try strategy of fitting a spline to the previous 3 periods with knots at each span
    # splines are not stable enough at the ends
    def spline_dn_RollingWrapper(series, rowsBetweenKnots, deg=2, derivative=1):
        x = np.arange(series.size)
        t = np.arange(x[0] + rowsBetweenKnots, int(x[-1] * 0.9), rowsBetweenKnots)
        lsqSpl = interpolate.LSQUnivariateSpline(x, researchData.SPX_Close, t=t, k=deg)
        return lsqSpl.derivative(n=derivative)(x[-1])[0]

    def spline_FutureDelta_RollingWrapper(
        series, marketDays, rowsBetweenKnots, deg=3, derivative=1
    ):
        x = np.arange(series.size)
        t = np.arange(x[0] + rowsBetweenKnots, x[-1], rowsBetweenKnots)
        lsqSpl = interpolate.LSQUnivariateSpline(x, series, t=t, k=deg)
        delta = lsqSpl(x[-1] + marketDays) - lsqSpl(x[-1])
        # easy correction is to enforce a boundary that the stock won't fall more than 90%
        return max(delta, series[-1] * 0.9)

    # lsqSplineObjects = {}
    # kIntervals = trendSpans
    # for k in kIntervals:
    #    print(k)
    #    t = np.arange(10,deltaDays[-1]-10,k)
    #    lsqSplineObjects[k] = interpolate.LSQUnivariateSpline(deltaDays,researchData.SPX_Close,t=t)

    for span in trendSpans:
        print(span, ".....")
        estBdaysInSpan = span * 5 / 7
        colGroups[span] = []
        # save the actual future value
        colSuffix = "_%03iDays" % span
        shiftRows = int(round(span * 252.0 / 365.0))
        endDays = spxData.index[shiftRows:]
        startDays = spxData.index[:-shiftRows]
        actualClose = spxData[endDays].copy()
        actualClose.index = startDays
        colName = "FuturePrice" + colSuffix
        colGroups[span].append(colName)
        actualClose.name = colName
        columnList.append(actualClose.copy())
        colGroups["Price"].append(colName)
        colGroups["FururePrice"].append(colName)
        # future price to start price delta
        futureDelta = actualClose[startDays] - spxData[startDays]
        colName = "FutureDelta" + colSuffix
        colGroups[span].append(colName)
        futureDelta.name = colName
        columnList.append(futureDelta.copy())
        colGroups["PriceDelta"].append(colName)
        colGroups["FutureDelta"].append(colName)

        if linearBool:
            # save the slope of the raw data
            slope, r2 = calculateTrend_BySpan(spxData, span)
            colName = "Slope" + colSuffix
            colGroups[span].append(colName)
            colGroups["Slope"].append(colName)
            colGroups["RawSlope"].append(colName)
            slope.name = colName
            columnList.append(slope)
            # save the R2 of the raw data
            colName = "R2" + colSuffix
            colGroups[span].append(colName)
            colGroups["R2"].append(colName)
            colGroups["RawR2"].append(colName)
            r2.name = colName
            columnList.append(r2)
            # save the Slope*R2 of the raw data
            colName = "SlopeR2Product" + colSuffix
            colGroups[span].append(colName)
            colGroups["SlopeR2"].append(colName)
            slopeR2 = slope * r2
            slopeR2.name = colName
            columnList.append(slopeR2)

            # now save the 2nd derivative of the raw price
            for localSlopeDays in localSpans:
                localSlopeGroupName = "d2"
                # localR2GroupName = 'SMA'+'_%02iDayR2'%localSlopeDays
                if localSlopeGroupName not in colGroups.keys():
                    colGroups[localSlopeGroupName] = []
                    # colGroups[localR2GroupName] = []
                d2, R2D2 = calculateTrend_BySpan(spxData, localSlopeDays)
                # slope of the SMA
                colName = "d2%03iSpan_%02iDaySlope" % (span, localSlopeDays)
                colGroups[span].append(colName)
                colGroups["d2"].append(colName)
                d2.name = colName
                columnList.append(d2)
                # R2 of the SMA trend
        #                col_name = 'SMA%03i'%span+'_%02iDayR2'%localSlopeDays
        #                colGroups[span].append(col_name)
        #                colGroups['R2'].append(col_name)
        #                colGroups['SMA_R2'].append(col_name)
        #                colGroups[localR2GroupName].append(col_name)
        #                smaLocalR2.name = col_name
        #                columnList.append(smaLocalR2)

        # save the volatility data for the span
        colName = "Volatility" + colSuffix
        colGroups[span].append(colName)
        colGroups["Volatility"].append(colName)
        vol = annual_volatility(spxData, span)
        vol.name = colName
        columnList.append(vol)

        if smaBool:
            # save the moving average for the span
            colName = "SMA%03i" % span
            colGroups[span].append(colName)
            colGroups["Price"].append(colName)
            colGroups["SMA"].append(colName)
            sma = spxData.rolling("%01id" % span).mean()
            sma.name = colName
            columnList.append(sma)
            for localSlopeDays in localSpans:
                localSlopeGroupName = "SMA" + "_%02iDaySlope" % localSlopeDays
                localR2GroupName = "SMA" + "_%02iDayR2" % localSlopeDays
                if localSlopeGroupName not in colGroups.keys():
                    colGroups[localSlopeGroupName] = []
                    colGroups[localR2GroupName] = []
                smaLocalSlope, smaLocalR2 = calculateTrend_BySpan(sma, localSlopeDays)
                # slope of the SMA
                colName = "SMA%03i" % span + "_%02iD_Slope" % localSlopeDays
                colGroups[span].append(colName)
                colGroups["Slope"].append(colName)
                colGroups["SMA_Slope"].append(colName)
                colGroups[localSlopeGroupName].append(colName)
                smaLocalSlope.name = colName
                columnList.append(smaLocalSlope)
                # R2 of the SMA trend
                colName = "SMA%03i" % span + "_%02iD_R2" % localSlopeDays
                colGroups[span].append(colName)
                colGroups["R2"].append(colName)
                colGroups["SMA_R2"].append(colName)
                colGroups[localR2GroupName].append(colName)
                smaLocalR2.name = colName
                columnList.append(smaLocalR2)

        if quadraticBool or cubicBool or splineBool:
            r = spxData.rolling(estBdaysInSpan, min_periods=int(estBdaysInSpan * 0.6))

        if quadraticBool:
            # quadratic data
            recordQuadSlopes = False
            if recordQuadSlopes:
                # 1st derivative
                colName = "d1Quad" + colSuffix
                colGroups[span].append(colName)
                colGroups["d1Quad"].append(colName)
                colGroups["d1"].append(colName)
                d1 = r.apply(
                    estBdaysInSpan,
                    polyFit_dn_RollingWrapper,
                    kwargs={"deg": 2, "derivative": 1},
                )
                d1.name = colName
                columnList.append(d1)
                # 2nd Derivative
                colName = "d2Quad" + colSuffix
                colGroups[span].append(colName)
                colGroups["d2Quad"].append(colName)
                colGroups["d2"].append(colName)
                d2 = r.apply(
                    "%01id" % span,
                    polyFit_dn_RollingWrapper,
                    kwargs={"deg": 2, "derivative": 2},
                )
                d2.name = colName
                columnList.append(d2)
            for predictSpan in trendSpans:
                predictName = "Quad%03iD_Delta%03iD" % (span, predictSpan)
                colGroups[span].append(predictName)
                colGroups["PriceDelta"].append(predictName)
                colGroups["QuadDelta"].append(predictName)
                delta = r.apply(
                    polyFit_FutureDelta_RollingWrapper,
                    args=[estBdaysInSpan],
                    kwargs={"deg": 2},
                )
                delta.name = predictName
                columnList.append(delta)

        if cubicBool:
            # cubic data
            recordCubicSlopes = False
            if recordCubicSlopes:
                # 1st derivative
                colName = "d1Cubic" + colSuffix
                colGroups[span].append(colName)
                colGroups["d1Cubic"].append(colName)
                colGroups["d1"].append(colName)
                d1 = r.apply(
                    estBdaysInSpan,
                    polyFit_dn_RollingWrapper,
                    kwargs={"deg": 3, "derivative": 1},
                )
                d1.name = colName
                columnList.append(d1)
                # 2nd Derivative
                colName = "d2Cubic" + colSuffix
                colGroups[span].append(colName)
                colGroups["d2Cubic"].append(colName)
                colGroups["d2"].append(colName)
                d2 = r.apply(
                    "%01id" % span,
                    polyFit_dn_RollingWrapper,
                    kwargs={"deg": 3, "derivative": 2},
                )
                d2.name = colName
                columnList.append(d2)
            for predictSpan in trendSpans:
                predictName = "Cubic%03iD_Delta%03iD" % (span, predictSpan)
                colGroups[span].append(predictName)
                colGroups["PriceDelta"].append(predictName)
                colGroups["CubicDelta"].append(predictName)
                delta = r.apply(
                    polyFit_FutureDelta_RollingWrapper,
                    args=[estBdaysInSpan],
                    kwargs={"deg": 3},
                )
                delta.name = predictName
                columnList.append(delta)

    researchData = pd.concat(columnList, axis=1)
    # ************* now calculate the best R2 and corresponding slopes *****************
    if linearBool:
        # loop through spans that controls which span is the minimum span
        minSpans = [0] + trendSpans[:-1]
        r2ColsLeft = list(colGroups["RawR2"])
        slopeColsLeft = list(colGroups["RawSlope"])
        slopeR2ColsLeft = list(colGroups["SlopeR2"])

        for minSpan in minSpans:
            print(
                "Calculating best R2 and slopes for the spans greater than %01i days"
                % minSpan
            )
            # set up
            calcSpans = np.array(trendSpans)
            calcSpans = calcSpans[calcSpans > minSpan]
            firstSpan = calcSpans.min()
            if "R2_%03iDays" % minSpan in r2ColsLeft:
                r2ColsLeft.remove("R2_%03iDays" % minSpan)
                slopeColsLeft.remove("Slope_%03iDays" % minSpan)
                slopeR2ColsLeft.remove("SlopeR2Product_%03iDays" % minSpan)
            # R2 code
            colName = "BestR2_%03iDaysPlus" % firstSpan
            researchData[colName] = researchData[r2ColsLeft].max(axis=1)
            colGroups["R2"].append(colName)
            colGroups["RawR2"].append(colName)
            researchData["R2Std_%03iDaysPlus" % firstSpan] = researchData[
                r2ColsLeft
            ].std(axis=1)
            # Slope code
            researchData["SlopeStd_%03iDaysPlus" % firstSpan] = researchData[
                slopeColsLeft
            ].std(axis=1)
            researchData["SlopeMax_%03iDaysPlus" % firstSpan] = researchData[
                slopeColsLeft
            ].max(axis=1)
            researchData["SlopeMin_%03iDaysPlus" % firstSpan] = researchData[
                slopeColsLeft
            ].min(axis=1)
            researchData["SlopeMean_%03iDaysPlus" % firstSpan] = researchData[
                slopeColsLeft
            ].mean(axis=1)
            researchData["SlopeR2Std_%03iDaysPlus" % firstSpan] = researchData[
                slopeR2ColsLeft
            ].std(axis=1)
            researchData["SlopeR2Max_%03iDaysPlus" % firstSpan] = researchData[
                slopeR2ColsLeft
            ].max(axis=1)
            researchData["SlopeR2Min_%03iDaysPlus" % firstSpan] = researchData[
                slopeR2ColsLeft
            ].min(axis=1)
            researchData["SlopeR2Mean_%03iDaysPlus" % firstSpan] = researchData[
                slopeR2ColsLeft
            ].mean(axis=1)
            researchData["SlopeR2Weighted_%03iDaysPlus" % firstSpan] = researchData[
                slopeR2ColsLeft
            ].sum(axis=1) / researchData[r2ColsLeft].sum(axis=1)
            # adjust the mean to weight by duration or log(duration) as well as R2
            weightedAvgNumerator = (researchData[slopeR2ColsLeft] * calcSpans).sum(
                axis=1
            )
            logWeightedAvgNumerator = (
                researchData[slopeR2ColsLeft] * np.log(calcSpans)
            ).sum(axis=1)
            weightedAvgDenomenator = (researchData[r2ColsLeft].sum(axis=1)) * (
                calcSpans.sum()
            )
            logWeightedAvgDenomenator = (researchData[r2ColsLeft].sum(axis=1)) * (
                np.log(calcSpans).sum()
            )
            researchData["SlopeR2WeightedByR2AndSpan_%03iDaysPlus" % firstSpan] = (
                weightedAvgNumerator / weightedAvgDenomenator
            )
            researchData["SlopeR2WeightedByR2AndLogSpan_%03iDaysPlus" % firstSpan] = (
                logWeightedAvgNumerator / logWeightedAvgDenomenator
            )

            researchData["SlopeOfBestR2_%03iDaysPlus" % firstSpan] = np.nan
            for colName in [
                "SlopeMax_%03iDaysPlus" % firstSpan,
                "SlopeMin_%03iDaysPlus" % firstSpan,
                "SlopeMean_%03iDaysPlus" % firstSpan,
                "SlopeOfBestR2_%03iDaysPlus" % firstSpan,
            ]:
                colGroups["Slope"].append(colName)
                colGroups["RawSlope"].append(colName)
            researchData["SlopeR2ProductOfBestR2_%03iDaysPlus" % firstSpan] = np.nan
            for colName in [
                "SlopeR2Max_%03iDaysPlus" % firstSpan,
                "SlopeR2Min_%03iDaysPlus" % firstSpan,
                "SlopeR2Mean_%03iDaysPlus" % firstSpan,
                "SlopeR2Weighted_%03iDaysPlus" % firstSpan,
                "SlopeR2WeightedByR2AndSpan_%03iDaysPlus" % firstSpan,
                "SlopeR2WeightedByR2AndLogSpan_%03iDaysPlus" % firstSpan,
            ]:
                # researchData[col_name] = np.nan
                colGroups["SlopeR2"].append(colName)
            researchData["TrendSpanOfBestR2_%03iDaysPlus" % firstSpan] = np.nan
            # assign the correct values to the slope and trendspand columns
            for assignSpan in calcSpans:
                unassignedRows = pd.isna(
                    researchData["SlopeOfBestR2_%03iDaysPlus" % firstSpan]
                )
                boolSelect = np.logical_and(
                    unassignedRows,
                    researchData["R2_%03iDays" % assignSpan]
                    == researchData["BestR2_%03iDaysPlus" % firstSpan],
                )
                researchData.loc[
                    boolSelect, "SlopeOfBestR2_%03iDaysPlus" % firstSpan
                ] = researchData["Slope_%03iDays" % assignSpan][boolSelect]
                researchData.loc[
                    boolSelect, "SlopeR2ProductOfBestR2_%03iDaysPlus" % firstSpan
                ] = researchData["SlopeR2Product_%03iDays" % assignSpan][boolSelect]
                researchData.loc[
                    boolSelect, "TrendSpanOfBestR2_%03iDaysPlus" % firstSpan
                ] = assignSpan

    # ******************** now calculate the interactions ******************************
    # slopeSum, slopeDiff, slopeAvg, slopeWeightedAvg

    # SMADiff, SMADiff_LocalSlope, SMASlopeDiff,SMASlopeSum,SMASlopeAvg
    if smaBool:
        spanCombinations = itertools.combinations(trendSpans, 2)
        for shortSpan, longSpan in spanCombinations:
            print(
                "Calculating Indicators for SMA combination (%03i, %03i)"
                % (shortSpan, longSpan)
            )
            colNameDiff = "SMADiff_%03i_%03i" % (shortSpan, longSpan)
            smaDiff = (
                researchData["SMA%03i" % shortSpan] - researchData["SMA%03i" % longSpan]
            )
            researchData[colNameDiff] = smaDiff
            colGroups["SMADiff"].append(colNameDiff)
            colNameRatio = "SMARatio_%03i_%03i" % (shortSpan, longSpan)
            smaRatio = (
                researchData["SMA%03i" % shortSpan] / researchData["SMA%03i" % longSpan]
            )
            researchData[colNameRatio] = smaRatio
            colGroups["SMARatio"].append(colNameRatio)
            for localSlopeDays in localSpans:
                # get the 1st and second derivative of the SMADiff and the diff,sum, and mean of the sma slopes
                # 1st derivative of the SMAdiff
                d1_smaDiff, smaLocalR2 = calculateTrend_BySpan(smaDiff, localSlopeDays)
                # slope of the SMADiff
                d1_smaDiffName = colNameDiff + "_%02iD_d1" % localSlopeDays
                colGroups["SMADiff_d1"].append(d1_smaDiffName)
                smaLocalSlope.name = d1_smaDiffName
                researchData[d1_smaDiffName] = d1_smaDiff
                # 2nd derivative - only use the largest span
                d2_smaDiff, smaLocalR2 = calculateTrend_BySpan(
                    d1_smaDiff, localSpans[-1]
                )
                # slope of the 1st derivative
                d2_smaDiffName = colNameDiff + "_%02iD_d2" % localSlopeDays
                colGroups["SMADiff_d2"].append(d2_smaDiffName)
                smaLocalSlope.name = d2_smaDiffName
                researchData[d2_smaDiffName] = d2_smaDiff

                d1_smaRatio, smaLocalR2 = calculateTrend_BySpan(
                    smaRatio, localSlopeDays
                )
                # slope of the SMARatio
                d1_smaRatioName = colNameRatio + "_%02iD_d1" % localSlopeDays
                colGroups["SMARatio_d1"].append(d1_smaRatioName)
                smaLocalSlope.name = d1_smaRatioName
                researchData[d1_smaRatioName] = d1_smaRatio
                # 2nd derivative - only use the largest span
                d2_smaRatio, smaLocalR2 = calculateTrend_BySpan(
                    d1_smaRatio, localSpans[-1]
                )
                # slope of the 1st derivative
                d2_smaRatioName = colNameRatio + "_%02iD_d2" % localSlopeDays
                colGroups["SMARatio_d2"].append(d2_smaRatioName)
                smaLocalSlope.name = d2_smaRatioName
                researchData[d2_smaRatioName] = d2_smaRatio
                # difference, sum, mean, and weighted mean of the slopes
                shortSMA_Slope = researchData[
                    "SMA%03i" % shortSpan + "_%02iD_Slope" % localSlopeDays
                ]
                longSMA_Slope = researchData[
                    "SMA%03i" % longSpan + "_%02iD_Slope" % localSlopeDays
                ]
                colName = "SMA_SlopeDiff_%03i_%03i" % (shortSpan, longSpan)
                colGroups["SMA_SlopeDiff"].append(colName)
                researchData[colName] = shortSMA_Slope - longSMA_Slope

                colName = "SMA_SlopeSum_%03i_%03i" % (shortSpan, longSpan)
                colGroups["SMA_SlopeSum"].append(colName)
                researchData[colName] = shortSMA_Slope + longSMA_Slope

                colName = "SMA_SlopeMean_%03i_%03i" % (shortSpan, longSpan)
                colGroups["SMA_SlopeMean"].append(colName)
                researchData[colName] = (shortSMA_Slope + longSMA_Slope) / 2

                colName = "SMA_SlopeWeightR2_%03i_%03i" % (shortSpan, longSpan)
                colGroups["SMA_SlopeR2Weighted"].append(colName)
                shortSMA_R2 = researchData[
                    "SMA%03i" % shortSpan + "_%02iD_R2" % localSlopeDays
                ]
                longSMA_R2 = researchData[
                    "SMA%03i" % longSpan + "_%02iD_R2" % localSlopeDays
                ]
                researchData[colName] = (
                    shortSMA_Slope * shortSMA_R2 - longSMA_Slope * longSMA_R2
                ) / (shortSMA_R2 + longSMA_R2)

    corrMatrix = researchData.corr()
    indicatorCols = (
        colGroups["RawSlope"]
        + colGroups["SlopeR2"]
        + colGroups["SMA"]
        + colGroups["SMADiff"]
        + colGroups["SMADiff_d1"]
        + colGroups["SMADiff_d2"]
        + colGroups["SMARatio"]
        + colGroups["SMARatio_d1"]
        + colGroups["SMARatio_d2"]
        + colGroups["SMA_Slope"]
        + colGroups["SMA_SlopeDiff"]
        + colGroups["SMA_SlopeSum"]
        + colGroups["SMA_SlopeMean"]
        + colGroups["SMA_SlopeR2Weighted"]
        + colGroups["QuadDelta"]
        + colGroups["CubicDelta"]
        + colGroups["d1"]
        + colGroups["d2"]
    )
    resultCols = colGroups["FutureDelta"]
    reducedCorrMatrix = corrMatrix.loc[indicatorCols, resultCols]

    # result=reducedCorrMatrix.FutureDelta_030Days;rankedIndicators = reducedCorrMatrix.FutureDelta_030Days.rank(ascending=False).sort_values().index.tolist(); reducedCorrMatrix.FutureDelta_030Days[rankedIndicators]
    # rankedIndicators = reducedCorrMatrix.FutureDelta_060Days.rank(ascending=False).sort_values().index.tolist()
    # reducedCorrMatrix.FutureDelta_060Days[rankedIndicators]
    # plotByTimeDelta(researchData[['FuturePrice_030Days']],pd.Timedelta(days=365),researchData[['SMADiff_090_180']])
    # result = researchData.FutureDelta_030Days; indicator = researchData.SMADiff_090_180; x=np.linspace(result.min(), result.max(), 100)
    # plt.plot(result,indicator,'.',x,np.polyval(np.polyfit(result,indicator,2),x),'k-',lw=3)

    # look into 2nd dirivatives of the price (i.e. slope of the max)
    return researchData, colGroups, reducedCorrMatrix


def testPredictionModel(predictionModel, predictionDays=90, tckr="ixSPX"):
    closeData = loadAllSPXData(predictionModel.stockDB, columns="Close")
    # 252 days in the stock market calendar
    shiftRows = int(round(predictionDays * 252.0 / 365.0))
    endDays = closeData.index[shiftRows:]
    startDays = closeData.index[:-shiftRows]

    actualClose = closeData[endDays].copy()
    actualClose.index = startDays
    predictedClose = predictionModel.predict_mean(startDays, endDays, tckr)
    predictedVolDollar = predictionModel.predict_volatility(
        startDays, endDays, tckr, unit="Dollar"
    )
    predictedVolPerc = np.abs(predictedVolDollar / predictedClose)
    allData = pd.concat(
        [closeData, actualClose, predictedClose, predictedVolDollar, predictedVolPerc],
        axis=1,
    )
    allData.columns = [
        "Close",
        "ActualClose",
        "PredictedClose",
        "PredictedVol_Dollar",
        "PredictedVol_Perc",
    ]
    allData["Deviation_Raw"] = allData.PredictedClose - allData.ActualClose
    allData["Deviation_Perc"] = allData.Deviation_Raw / allData.ActualClose
    allData["Deviation_Norm"] = (allData.Deviation_Perc) / allData.PredictedVol_Perc
    allData["PredPlus2Std"] = predictedClose + 2 * predictedVolDollar
    allData["PredMinus2Std"] = predictedClose - 2 * predictedVolDollar
    allData[
        ["Close", "ActualClose", "PredictedClose", "PredPlus2Std", "PredMinus2Std"]
    ].plot()
    #    firstLayer = [allData.Close, allData.ActualClose, allData.PredictedClose]
    #    overLayer = [allData.PredictedVol_Perc]
    #    plot_SeperateScales(firstLayer,overLayer,overlayMax = allData.PredictedVol_Perc.quantile(q=.9), overlayMin = allData.PredictedVol_Perc.quantile(q=.1)
    return allData


def calculateTrend_BySpan(dataSeries, trailingDays, colName="Source", extraInfo=False):
    if type(dataSeries) == pd.Series:
        if dataSeries.name is None:
            dataSeries.name = "Source"
        else:
            colName = dataSeries.name
        dataDF = pd.DataFrame(data=dataSeries)
    else:
        dataDF = dataSeries
    assert (
        type(dataDF) == pd.DataFrame
    ), "'dataSeries' needs to be either a pd.Series or pd.DataFrame"
    assert (
        colName in dataDF.columns
    ), "col_name needs to be a valid column in the pd.DataFrame"
    firstDate = dataDF.index.min()
    dataDF["DeltaTime"] = (dataDF.index - firstDate).days
    dataDF["XY"] = dataDF[colName] * dataDF["DeltaTime"]
    dataDF["X2"] = dataDF["DeltaTime"] * dataDF["DeltaTime"]
    dataDF["Y2"] = dataDF[colName] * dataDF[colName]
    r = dataDF.rolling("%01id" % trailingDays)
    sumData = r[["DeltaTime", colName, "XY", "X2", "Y2"]].aggregate(np.sum)
    num = r[colName].count()
    # intercept = ((sumY*sumX2) - (sumX*SumXY))/((num*sumX2)-(SumX**2))
    intercept = (
        (sumData[colName] * sumData["X2"]) - (sumData["DeltaTime"] * sumData["XY"])
    ) / ((num * sumData["X2"]) - (sumData["DeltaTime"] ** 2))
    # slope = (num*sumXY - sumX*sumY)/((num*sumX2)-(SumX**2))
    slope = (num * sumData["XY"] - sumData["DeltaTime"] * sumData[colName]) / (
        (num * sumData["X2"]) - (sumData["DeltaTime"] ** 2)
    )
    slope.name = "Slope"
    # trend = intercept + slope*data['DeltaTime']
    # now for R2 = 1-SStot/SSres
    # SStot = Sum((Y-Yavg)**2) = Sum(Y2)-2*Yavg*Sum(Y)+Num*Yavg**2
    # SSres = Sum((Y-Trend)**2) = Sum(Y2) - 2*Intercept*Sum(Y) - 2*Slope*Sum(XY) +Intercept**2*Num+2*Intercept*Slope*Sum(X)+Slope**2*Sum(X2)
    Yavg = r[colName].mean()
    # r2 = pd.Series(data = [0]*len(data.index),index=data.index,dtype=float,name='R2')
    SStot = sumData["Y2"] - 2 * Yavg * sumData[colName] + num * (Yavg ** 2)
    SSres = (
        sumData["Y2"]
        - 2 * intercept * sumData[colName]
        - 2 * slope * sumData["XY"]
        + (intercept ** 2) * num
        + 2 * intercept * slope * sumData["DeltaTime"]
        + (slope ** 2) * sumData["X2"]
    )
    r2 = 1 - SSres / SStot
    r2.name = "R2"
    # now ensure that the output has the correct format
    output = pd.concat([slope, r2], axis=1)[["Slope", "R2"]].sort_index(inplace=False)
    output = output.fillna(method="ffill").fillna(method="bfill")
    if extraInfo:
        output = output.Slope, output.R2, intercept
    else:
        output = output.Slope, output.R2
    return output


def annual_volatility(priceData, trailingDays, source="strorage"):
    # dailyChange = data['Close'].pct_change()
    # dailyVolatility = dailyChange.rolling('%01id'%trailing_days).std()
    # annualVolatility = np.sqrt(252)*dailyVolatility
    output = np.sqrt(252) * (
        priceData.pct_change().rolling("%01id" % trailingDays).std()
    )
    return output


def plotHist(histObj, ax=None, title=None):
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    if title is None:
        if type(histObj) == pd.DataFrame and len(histObj.columns) == 1:
            title = histObj.columns[0]
        elif type(histObj) == pd.Series:
            title = histObj.name
        else:
            title = ""
    plt.sca(ax)
    output = plt.hist(histObj, range=(histObj.min(), histObj.max()))
    plt.title(
        title + "\nMean - %1.4f\nStd Dev - %1.4f" % (histObj.mean(), histObj.std())
    )
    plt.show()
    return output


def plot_SeperateScales(
    df1, df2=None, scale1Min=None, scale1Max=None, scale2Min=None, scale2Max=None
):
    if type(df1) != pd.DataFrame:
        try:
            pd.concat(df1, axis=1)
        except:
            pd.DataFrame(df1)
    if df2 is not None and type(df2) != pd.DataFrame:
        try:
            pd.concat(df2, axis=1)
        except:
            pd.DataFrame(df2)

    #    data1Colors = ['k','b','g','m']
    #    data2Colors = ['r','orange','k','c','m']
    fig, ax1 = plt.subplots()
    if scale1Min is not None and type(scale1Min) == str and "%" in scale1Min:
        scale1Min = float(scale1Min.replace("%", ""))
        scale1Min = np.percentile(df1.values, scale1Min)
    if scale1Max is not None and type(scale1Max) == str and "%" in scale1Max:
        scale1Max = float(scale1Max.replace("%", ""))
        scale1Max = np.percentile(df1.values, scale1Max)
    if scale2Min is not None and type(scale2Min) == str and "%" in scale2Min:
        scale2Min = float(scale2Min.replace("%", ""))
        scale2Min = np.percentile(df2.values, scale2Min)
    if scale2Max is not None and type(scale2Max) == str and "%" in scale2Max:
        scale2Max = float(scale2Max.replace("%", ""))
        scale2Max = np.percentile(df2.values, scale2Max)
    #    colorCnt = 0
    for colName in df1.columns:
        #        ax1.plot(df1[col_name],color=data1Colors[colorCnt],label=col_name)
        #        colorCnt = (colorCnt+1)%len(data1Colors)
        ax1.plot(df1[colName], label=colName)
        ax1.set_ylim(scale1Min, scale1Max)
    if df2 is not None:
        ax2 = ax1.twinx()
        #        colorCnt = 0
        for colName in df2.columns:
            #            ax2.plot(df2[col_name],'--',color=data2Colors[colorCnt],label=col_name)
            #            colorCnt = (colorCnt+1)%len(data2Colors)
            ax2.plot(df2[colName], "--", label=colName)
        ax2.set_ylim(scale2Min, scale2Max)
        ax2.tick_params("y", colors="r")
        ax2.legend()
    else:
        ax2 = None
    ax1.legend()
    plt.show()
    plt.clf()
    plt.close()
    return fig, ax1, ax2


def plotByTimeDelta(
    df1,
    df2=None,
    timeDelta=pd.Timedelta(days=365),
    scale1Min=None,
    scale1Max=None,
    scale2Min=None,
    scale2Max=None,
):
    if type(df1) != pd.DataFrame:
        try:
            pd.concat(df1, axis=1)
        except:
            pd.DataFrame(df1)
    if df2 is not None and type(df2) != pd.DataFrame:
        try:
            pd.concat(df2, axis=1)
        except:
            pd.DataFrame(df2)
    print("All Data")
    plot_SeperateScales(
        df1,
        df2=df2,
        scale1Min=scale1Min,
        scale1Max=scale1Max,
        scale2Min=scale2Min,
        scale2Max=scale2Max,
    )
    plot_Start = (
        df1.index.min() if df2 is None else min(df1.index.min(), df2.index.min())
    )
    lastDate = df1.index.max() if df2 is None else min(df1.index.max(), df2.index.max())
    plot_End = plot_Start + timeDelta
    while plot_End <= lastDate:
        print(
            "*" * 15
            + " %s to %s "
            % (plot_Start.strftime("%m/%d/%Y"), plot_End.strftime("%m/%d/%Y"))
            + "*" * 15
        )
        if df2 is None:
            plot_SeperateScales(
                df1.loc[plot_Start:plot_End, :],
                df2=None,
                scale1Min=scale1Min,
                scale1Max=scale1Max,
                scale2Min=None,
                scale2Max=None,
            )
        else:
            plot_SeperateScales(
                df1.loc[plot_Start:plot_End, :],
                df2=df2.loc[plot_Start:plot_End, :],
                scale1Min=scale1Min,
                scale1Max=scale1Max,
                scale2Min=scale2Min,
                scale2Max=scale2Max,
            )
        plot_Start = plot_End
        plot_End = plot_Start + timeDelta
    # plot the remaining portion of the last map
    print(
        "*" * 15
        + " %s to %s "
        % (plot_Start.strftime("%m/%d/%Y"), lastDate.strftime("%m/%d/%Y"))
        + "*" * 15
    )
    if df2 is None:
        plot_SeperateScales(
            df1.loc[plot_Start:plot_End, :],
            df2=None,
            scale1Min=scale1Min,
            scale1Max=scale1Max,
            scale2Min=None,
            scale2Max=None,
        )
    else:
        plot_SeperateScales(
            df1.loc[plot_Start:plot_End, :],
            df2=df2.loc[plot_Start:plot_End, :],
            scale1Min=scale1Min,
            scale1Max=scale1Max,
            scale2Min=scale2Min,
            scale2Max=scale2Max,
        )


def plotXYWithMeanAndStd(x, y, stdLevels=4):
    xName = "X" if type(x) != pd.Series else x.name
    yName = "Y" if type(x) != pd.Series else y.name
    ax = plt.subplot(111)
    xbar = x.mean()
    ybar = y.mean()
    xstd = x.std()
    ystd = y.std()
    plt.plot(x, y, "b.", xbar, ybar, "ro", label="Mean")
    dataPts = 100
    dataStd = np.zeros((dataPts * 2, 2))
    for i in np.arange(stdLevels) + 1:
        dataStd[:dataPts, 0] = np.linspace(xbar - xstd * i, xbar + xstd * i, dataPts)
        dataStd[dataPts:, 0] = np.flip(dataStd[:dataPts, 0], 0)
        dataStd[:dataPts, 1] = ybar + ystd * i * np.sqrt(
            1 - ((dataStd[:dataPts, 0] - xbar) ** 2) / (xstd * xstd * i * i)
        )
        dataStd[dataPts:, 1] = ybar - ystd * i * np.sqrt(
            1 - ((dataStd[dataPts:, 0] - xbar) ** 2) / (xstd * xstd * i * i)
        )
        plt.plot(dataStd[:, 0], dataStd[:, 1], "--", lw=2)
    ax.text(
        x.min(),
        y.min(),
        "Axis - (mean, std. dev.)\nX - mean %1.2f std %1.2f \nY - mean %1.2f std %1.2f"
        % (xbar, xstd, ybar, ystd),
    )
    plt.title("%s vs %s" % (xName, yName))
    ax.xaxis.set_label_text(xName)
    ax.yaxis.set_label_text(yName)
    plt.show()
    x.rank(pct=True)
    normalDist = [68.27, 95.45, 99.73, 99.994]
    dfIndex = np.arange(stdLevels) + 1
    normalityStats = []
    for i in dfIndex:
        normalFactor = normalDist[i - 1] if i - 1 < len(normalDist) else np.nan
        xFactor = (
            x[np.logical_and(x <= xbar + xstd * i, x >= xbar - xstd * i)].size
            / float(x.size)
            * 100
        )
        yFactor = (
            y[np.logical_and(y <= ybar + ystd * i, y >= ybar - ystd * i)].size
            / float(y.size)
            * 100
        )
        normalityStats.append([normalFactor, xFactor, yFactor])
    normalityStats = pd.DataFrame(
        data=normalityStats, index=dfIndex, columns=["Normal Dist.", xName, yName]
    )
    print(normalityStats)
    return normalityStats


def Plot_cycleColumnsInDFAgainstIndicator(df, indicatorColumn, stdLevels=4):
    dfColumns = list(df.columns)
    dfColumns.remove(indicatorColumn)
    for colName in dfColumns:
        plotXYWithMeanAndStd(df[indicatorColumn], df[colName], stdLevels=stdLevels)


def lookForSizingSignal(sDB, longPeriod, shortPeriod, mapDuration):
    # longPeriod=200;shortPeriod=50;mapDuration = pd.Timedelta(days=365)
    spxData = loadAllSPXData(sDB)
    name_Long = "Avg_%01iDays" % longPeriod
    spxData[name_Long] = spxData.Close.rolling("%01id" % longPeriod).mean()
    name_Short = "Avg_%01iDays" % shortPeriod
    spxData[name_Short] = spxData.Close.rolling("%01id" % shortPeriod).mean()
    spxData["Diff_Avg"] = spxData[name_Short] - spxData[name_Long]
    spxData["DiffPerc_Avg"] = spxData.Diff_Avg / spxData.Close
    spxData["MaxPerc_Diff"] = spxData.DiffPerc_Avg.cummax()
    spxData["MinPerc_Diff"] = spxData.DiffPerc_Avg.cummin()
    spxData[name_Long + "%01iDaySlope" % longPeriod], spxData[
        name_Long + "%01iDayR2" % longPeriod
    ] = calculateTrend_BySpan(spxData[name_Long], longPeriod)
    spxData[name_Long + "%01iDaySlope" % shortPeriod], spxData[
        name_Long + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(spxData[name_Long], shortPeriod)
    spxData[name_Short + "%01iDaySlope" % longPeriod], spxData[
        name_Short + "%01iDayR2" % longPeriod
    ] = calculateTrend_BySpan(spxData[name_Short], longPeriod)
    spxData[name_Short + "%01iDaySlope" % shortPeriod], spxData[
        name_Short + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(spxData[name_Short], shortPeriod)
    spxData["ShortMinusLongSlope_ShortPeriod"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        - spxData[name_Long + "%01iDaySlope" % shortPeriod]
    )
    spxData["ShortPlusLongSlope_ShortPeriod"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        + spxData[name_Long + "%01iDaySlope" % shortPeriod]
    )
    spxData[
        "Max_ShortMinusLongSlope_ShortPeriod"
    ] = spxData.ShortMinusLongSlope_ShortPeriod.cummax()
    spxData[
        "Min_ShortMinusLongSlope_ShortPeriod"
    ] = spxData.ShortMinusLongSlope_ShortPeriod.cummin()
    spxData["WeigthedSlopes_Sum"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        * spxData[name_Short + "%01iDayR2" % shortPeriod]
        + spxData[name_Long + "%01iDaySlope" % shortPeriod]
        * spxData[name_Long + "%01iDayR2" % shortPeriod]
    )
    plot_Start = pd.Timestamp("2000-1-1")
    plot_End = plot_Start + mapDuration
    while plot_End <= spxData.index.max():
        firstLayer = [
            spxData.loc[plot_Start:plot_End, "Close"],
            spxData.loc[plot_Start:plot_End, name_Long],
            spxData.loc[plot_Start:plot_End, name_Short],
        ]
        # overLayMetric = [spxData.loc[plot_Start:plot_End,'WeigthedSlopes_Sum'],spxData.loc[plot_Start:plot_End,'ShortPlusLongSlope_ShortPeriod'],spxData.loc[plot_Start:plot_End,'ShortMinusLongSlope_ShortPeriod']]
        overLayMetric = [
            spxData.loc[plot_Start:plot_End, "ShortPlusLongSlope_ShortPeriod"],
            spxData.loc[plot_Start:plot_End, "DiffPerc_Avg"],
        ]
        plot_SeperateScales(firstLayer, [overLayMetric[0]])
        plot_SeperateScales(
            firstLayer, [overLayMetric[1]], overlayMax=0.2, overlayMin=-0.2
        )
        plot_Start = plot_End
        plot_End = plot_Start + mapDuration
        print("*" * 50)


def calculateExposure(
    maxExposure, medianExposure, minExposure, metric, maxMetric, medianMetric, minMetric
):
    if metric > medianMetric:
        return medianExposure + (maxExposure - medianExposure) * (
            metric - medianMetric
        ) / (maxMetric - medianMetric)
    else:
        return medianExposure - (maxExposure - medianExposure) * (
            medianMetric - metric
        ) / (medianMetric - minMetric)


def capturedGains_3xLeverage(
    sDB,
    longPeriod,
    shortPeriod,
    maxExposure,
    medianExposure,
    minExposure,
    staticExposure=0.35,
    mapDuration=pd.Timedelta(days=365),
):
    #    longPeriod=200;shortPeriod=50; maxExposure=0.7; minExposure=0.1;medianExposure=0.35;mapDuration=pd.Timedelta(days=365)
    spxData = loadAllSPXData(sDB)
    name_Long = "Avg_%01iDays" % longPeriod
    spxData[name_Long] = spxData.Close.rolling("%01id" % longPeriod).mean()
    name_Short = "Avg_%01iDays" % shortPeriod
    spxData[name_Short] = spxData.Close.rolling("%01id" % shortPeriod).mean()
    spxData["Diff_Avg"] = spxData[name_Short] - spxData[name_Long]
    spxData["DiffPerc_Avg"] = spxData.Diff_Avg / spxData.Close
    spxData["MaxPerc_Diff"] = spxData.DiffPerc_Avg.cummax()
    spxData["MinPerc_Diff"] = spxData.DiffPerc_Avg.cummin()
    spxData[name_Long + "%01iDaySlope" % longPeriod], spxData[
        name_Long + "%01iDayR2" % longPeriod
    ] = calculateTrend_BySpan(spxData[name_Long], longPeriod)
    spxData[name_Long + "%01iDaySlope" % shortPeriod], spxData[
        name_Long + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(spxData[name_Long], shortPeriod)
    spxData[name_Short + "%01iDaySlope" % longPeriod], spxData[
        name_Short + "%01iDayR2" % longPeriod
    ] = calculateTrend_BySpan(spxData[name_Short], longPeriod)
    spxData[name_Short + "%01iDaySlope" % shortPeriod], spxData[
        name_Short + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(spxData[name_Short], shortPeriod)
    spxData["ShortMinusLongSlope_ShortPeriod"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        - spxData[name_Long + "%01iDaySlope" % shortPeriod]
    )
    spxData["ShortPlusLongSlope_ShortPeriod"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        + spxData[name_Long + "%01iDaySlope" % shortPeriod]
    )
    spxData[
        "Max_ShortMinusLongSlope_ShortPeriod"
    ] = spxData.ShortMinusLongSlope_ShortPeriod.cummax()
    spxData[
        "Min_ShortMinusLongSlope_ShortPeriod"
    ] = spxData.ShortMinusLongSlope_ShortPeriod.cummin()
    spxData["WeigthedSlopes_Sum"] = (
        spxData[name_Short + "%01iDaySlope" % shortPeriod]
        * spxData[name_Short + "%01iDayR2" % shortPeriod]
        + spxData[name_Long + "%01iDaySlope" % shortPeriod]
        * spxData[name_Long + "%01iDayR2" % shortPeriod]
    )

    metricColName = "ShortPlusLongSlope_ShortPeriod"
    metricMiddle = 0

    model = pd.DataFrame(data=spxData.Close)
    model["DailyReturn_SPX"] = model["Close"].pct_change()
    model["DailyReturn_Leveraged"] = model.DailyReturn_SPX * 3 + 1
    model["DailyReturn_SPX"] = model.DailyReturn_SPX + 1
    model["DailyReturn_StaticLeveraged"] = (
        staticExposure * (model.DailyReturn_Leveraged - 1) + 1
    )
    model.iloc[0, [1, 2, 3]] = 1
    model["CumReturn_SPX"] = model.DailyReturn_SPX.cumprod()
    model["CumReturn_Leveraged"] = model.DailyReturn_Leveraged.cumprod()
    model["CumReturn_StaticLeveraged"] = model.DailyReturn_StaticLeveraged.cumprod()
    model["Metric"] = spxData[metricColName]
    selectBool = model.Metric >= metricMiddle
    model.loc[selectBool, "MetricIsHigh"] = 1
    model.loc[selectBool, "MetricIsLow"] = 0
    selectBool = selectBool == False
    model.loc[selectBool, "MetricIsHigh"] = 0
    model.loc[selectBool, "MetricIsLow"] = -1
    minMetric, maxMetric = model.Metric.cummin(), model.Metric.cummax()
    model["Exposure_V1"] = (
        medianExposure
        + model.MetricIsHigh
        * (maxExposure - medianExposure)
        * (model.Metric - metricMiddle)
        / (maxMetric - metricMiddle)
        + model.MetricIsLow
        * (medianExposure - minExposure)
        * (metricMiddle - model.Metric)
        / (metricMiddle - minMetric)
    )
    maxMetric[maxMetric > 3] = 3
    minMetric[minMetric < -3] = -3
    model["Exposure_V2"] = (
        medianExposure
        + model.MetricIsHigh
        * (maxExposure - medianExposure)
        * (model.Metric - metricMiddle)
        / (maxMetric - metricMiddle)
        + model.MetricIsLow
        * (medianExposure - minExposure)
        * (metricMiddle - model.Metric)
        / (metricMiddle - minMetric)
    )
    model.loc[model.Exposure_V2 > maxExposure, "Exposure_V2"] = maxExposure
    model.loc[model.Exposure_V2 < minExposure, "Exposure_V2"] = minExposure
    del minMetric, maxMetric
    model["Captured_DailyReturns_V1"] = (
        model.Exposure_V1 * (model.DailyReturn_Leveraged - 1) + 1
    )
    model["Captured_TotalReturns_V1"] = model.Captured_DailyReturns_V1.cumprod()
    model["Captured_DailyReturns_V2"] = (
        model.Exposure_V2 * (model.DailyReturn_Leveraged - 1) + 1
    )
    model["Captured_TotalReturns_V2"] = model.Captured_DailyReturns_V2.cumprod()
    plot_Start = pd.Timestamp("2000-1-1")
    plot_End = plot_Start + mapDuration
    while plot_End <= spxData.index.max():
        firstLayer = [
            model.loc[plot_Start:plot_End, "CumReturn_SPX"],
            model.loc[plot_Start:plot_End, "CumReturn_Leveraged"],
            model.loc[plot_Start:plot_End, "CumReturn_StaticLeveraged"],
            model.loc[plot_Start:plot_End, "Captured_TotalReturns_V1"],
            model.loc[plot_Start:plot_End, "Captured_TotalReturns_V2"],
        ]
        overLayMetric = [
            model.loc[plot_Start:plot_End, "Exposure_V1"],
            model.loc[plot_Start:plot_End, "Exposure_V2"],
        ]
        plot_SeperateScales(firstLayer, overLayMetric, overlayMax=1, overlayMin=0)
        plot_Start = plot_End
        plot_End = plot_Start + mapDuration
        print("*" * 50)

    firstLayer = [
        model.CumReturn_SPX,
        model.CumReturn_Leveraged,
        model.Captured_TotalReturns_V1,
        model.Captured_TotalReturns_V2,
        model.CumReturn_StaticLeveraged,
    ]
    overLayer = [model.Exposure_V1, model.Exposure_V2]
    plot_SeperateScales(firstLayer, overLayer, overlayMax=1, overlayMin=0)
    return model[
        [
            "CumReturn_SPX",
            "CumReturn_Leveraged",
            "CumReturn_StaticLeveraged",
            "Captured_TotalReturns_V1",
            "Captured_TotalReturns_V2",
        ]
    ]


def optimizableDynamicSizing_SumSlopes_MetricLimits(
    optimizingParameters, spxData, method
):
    """Start with initialConditions of
longPeriod=200;shortAsFractionOfLong=0.25; maxExposure=0.7; minExposure=0.1;medianExposure=0.35; exposureVariation=0.35; maxMetricLimit = 3; minMetricLimit=-3
    optimizingParameters = [200, 0.25, 0.35,0.35, 3, -3]

optimizing for the Calmar ratio returns a value of 0.11835
with conditions = [200, .25, .5, .05, -5, 1.12375]

optimizing for the total return produces a value of 11.6558x returns
with conditions = [200, 0.25, 0.5, 0.05, -5, 1]

    """
    [
        longPeriod,
        shortAsFractionOfLong,
        medianExposure,
        exposureVariation,
        maxMetricLimit,
        minMetricLimit,
    ] = optimizingParameters
    shortPeriod = int(longPeriod * shortAsFractionOfLong)
    maxExposure = min(0.7, medianExposure + exposureVariation)
    minExposure = max(0.1, medianExposure - exposureVariation)
    name_Long = "Avg_%01iDays" % longPeriod
    name_Short = "Avg_%01iDays" % shortPeriod

    model = pd.DataFrame(data=spxData.Close)
    model[name_Long] = model.Close.rolling("%01id" % longPeriod).mean()
    model[name_Short] = model.Close.rolling("%01id" % shortPeriod).mean()
    model[name_Long + "%01iDaySlope" % shortPeriod], model[
        name_Long + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(model[name_Long], shortPeriod)
    model[name_Short + "%01iDaySlope" % shortPeriod], model[
        name_Short + "%01iDayR2" % shortPeriod
    ] = calculateTrend_BySpan(model[name_Short], shortPeriod)
    model["ShortPlusLongSlope_ShortPeriod"] = (
        model[name_Short + "%01iDaySlope" % shortPeriod]
        + model[name_Long + "%01iDaySlope" % shortPeriod]
    )

    metricColName = "ShortPlusLongSlope_ShortPeriod"
    metricMiddle = 0

    model["DailyReturn_Leveraged"] = model["Close"].pct_change() * 3 + 1
    model.loc[model.index.min(), "DailyReturn_Leveraged"] = 1
    selectBool = model[metricColName] >= metricMiddle
    model.loc[selectBool, "MetricIsHigh"] = 1
    model.loc[selectBool, "MetricIsLow"] = 0
    selectBool = selectBool == False
    model.loc[selectBool, "MetricIsHigh"] = 0
    model.loc[selectBool, "MetricIsLow"] = -1
    minMetric, maxMetric = model[metricColName].cummin(), model[metricColName].cummax()
    maxMetric[maxMetric > maxMetricLimit] = maxMetricLimit
    minMetric[minMetric < minMetricLimit] = minMetricLimit
    model["Exposure"] = (
        medianExposure
        + model.MetricIsHigh
        * (maxExposure - medianExposure)
        * (model[metricColName] - metricMiddle)
        / (maxMetric - metricMiddle)
        + model.MetricIsLow
        * (medianExposure - minExposure)
        * (metricMiddle - model[metricColName])
        / (metricMiddle - minMetric)
    )
    model.loc[model.Exposure > maxExposure, "Exposure"] = maxExposure
    model.loc[model.Exposure < minExposure, "Exposure"] = minExposure
    del minMetric, maxMetric
    model["Captured_DailyReturns"] = (
        model.Exposure * (model.DailyReturn_Leveraged - 1) + 1
    )
    model["Captured_TotalReturns"] = model.Captured_DailyReturns.cumprod()
    if method.upper() == "CALMAR":
        statObj = ffn.PerformanceStats(model.Captured_TotalReturns)
        return -statObj.calmar
    else:
        return -model.loc[model.index.max(), "Captured_TotalReturns"]


def restrictedOptimization(optimizingParameters, spxData, method):
    """
    when medianExposure, exposureVariation, minMetricLimit, maxMetricLimit = 0.35, 0.35, -5, 5 :
    """
    [longPeriod, shortAsFractionOfLong] = optimizingParameters
    medianExposure, exposureVariation, minMetricLimit, maxMetricLimit = (
        0.35,
        0.35,
        -5,
        5,
    )
    newParameters = [
        longPeriod,
        shortAsFractionOfLong,
        medianExposure,
        exposureVariation,
        maxMetricLimit,
        minMetricLimit,
    ]
    return optimizableDynamicSizing_SumSlopes_MetricLimits(
        newParameters, spxData, method
    )


def buildTrendFromParms_SpanWeighted(x, polyTrends, sinusoidalTrends):
    """Depreciated"""
    y = pd.Series(data=np.zeros(x.shape), index=x, name="Trend Fit")
    if len(polyTrends) > 0:
        # need to apply the poly trends in order from largest to least
        #   and weight the following contribution according to the span
        # sort all the parms
        sortedRanges = {}
        sortedParms = {}
        spans = []
        for span, dayRange, parms in polyTrends:
            spans.append(span)
            sortedRanges[span] = dayRange
            sortedParms[span] = parms
        spans.sort(reverse=True)
        spans = np.array(spans)
        polyPredictions = []
        weightingDenometer = np.ones(x.shape)
        weightingDenometer[x > 0] = 0.00001
        for span in spans:
            trendSeries = pd.Series(data=np.zeros(x.shape), index=x, name=span)
            dayRange = sortedRanges[span]
            valid = np.logical_and(x >= dayRange[0], x <= dayRange[1])
            trendSeries[valid] = np.polyval(sortedParms[span], x[valid])
            polyPredictions.append(trendSeries)
        polyPredictions.append(pd.Series(data=np.zeros(x.shape), index=x, name="Final"))
        polyPredictions = pd.concat(polyPredictions, axis=1)
        polyPredictions = polyPredictions.cumsum(axis=1)
        if (x > 0).any():
            predictionIndex = x > 0
            spanSum = spans.sum()
            polyPredictions.loc[predictionIndex, "Final"] = np.zeros(
                (predictionIndex.sum())
            )
            for span in spans:
                polyPredictions.loc[predictionIndex, "Final"] += (
                    polyPredictions.loc[predictionIndex, span] * span / spanSum
                )
        y[:] = polyPredictions.Final[:]
    if len(sinusoidalTrends) > 0:
        for span, dayRange, parms in sinusoidalTrends:
            weighting = np.zeros(x.shape)
            weighting[np.logical_and(x <= 0, x >= max(dayRange[0], -span * 4))] = 1
            xPos = x[x > 0]
            if xPos.size > 0:
                weighting[x > 0] = np.exp(-xPos / float(span))
            y[:] += (
                parms[0] * np.sin(((x + parms[1]) / parms[2]) * np.pi) + parms[3]
            ) * weighting
    assert pd.notnull(y).all(), "Func buildTrendFromParms: output contains a null value"
    if pd.isna(y).any():
        print(y)
    return y


def EstDerivative_ConvolveMethod(x, window_len=90, minRowCnt=None):
    if x.ndim != 1:
        raise (ValueError, "smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise (ValueError, "Input vector needs to be bigger than window size.")
    if window_len < 2:
        return x
    window = np.zeros((4 * window_len - 1,))
    window[:window_len] = -1.0 / window_len
    window[window_len : 2 * window_len] = 1.0 / window_len
    window = np.flip(window, 0)
    s = np.r_[x[window.size - 1 : 0 : -1], x, x[-2 : -window.size - 1 : -1]]
    y = np.convolve(window, s, mode="valid")[(window.size / 2) : -(window.size / 2)]
    return y / 90


def EstDerivative_SMA_Method(series, window_len=90, minRowCnt=None):
    """subtracts the SMA with the given window length from the SMA of 2x the window_len.
        the result is an average amount that the function changes across the window"""
    if series.ndim != 1:
        raise (ValueError, "smooth only accepts 1 dimension arrays.")
    if series.size < window_len:
        raise (ValueError, "Input vector needs to be bigger than window size.")
    if window_len < 2:
        return series
    return (
        series.rolling("%01id" % window_len).mean()
        - series.rolling("%01id" % (2 * window_len)).mean()
    ) / window_len


spxData = loadAllSPXData(sDB)

"""
1/3/18
----------------------------------------------------------------------------
SMA predictions with the 30 day volatility adjusted for the trendspan
----------------------------------------------------------------------------
Code to generate data:
testPeriods = {7:'1w',14:'2w',30:'1m',90:'3m',180:'6m',365:'1y'}
for trendDays in testPeriods.keys():
    label = testPeriods[trendDays]
    print(label)
    output[label] = comparePrediction(priceData,None,trendDays)

*access charts below with output[label][0]

1w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00109583      0.0295909      0.0371138         1.31733
Flat              0.00166482      0.0229773      0.0898886         1.07964
SmoothSMA_1.50x   0.00138891      0.0264488      0.0610089         1.20205
SMA_1x            0.00138032      0.0252568      0.0635012         1.15858
SMA_1.25x          0.0013092      0.0261793      0.0569043         1.19175
VWT              -0.00112796      0.0442925      0.0412871         2.04932
SmoothSMA_1x      0.00153313      0.0247819      0.0743519         1.14209
SmoothSMA_1.75x    0.0013168      0.0274631      0.0543374         1.23914
SmoothSMA_1.25x   0.00146102      0.0255508      0.0676804         1.16957
SMA_1.75x         0.00116695      0.0283587      0.0437106         1.27154
SmoothFlat        0.00182157      0.0232218       0.101038         1.08881
SMA_1.50x         0.00123807      0.0272175      0.0503075         1.22954
SmoothSMA_2x      0.00124468      0.0285815      0.0476659         1.28045

                   R2_PDF    R2_CDF
SMA_2x           0.628793  0.977267
Flat             0.969052  0.995321  <- best so far
SmoothSMA_1.50x  0.865729  0.989692
SMA_1x           0.918673  0.992982
SMA_1.25x        0.880964  0.990791
VWT              -10.6171  0.713266
SmoothSMA_1x     0.931794  0.993417
SmoothSMA_1.75x  0.806785  0.986403
SmoothSMA_1.25x  0.905701  0.991964
SMA_1.75x        0.743399  0.983156
SmoothFlat       0.960805  0.994097
SMA_1.50x        0.824711  0.987588
SmoothSMA_2x     0.722342  0.981885

2w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00218202      0.0359705      0.0563219         1.17362
Flat              0.00329762      0.0306115       0.133034          1.0282
SmoothSMA_1.50x   0.00289311        0.03365      0.0977689         1.10801
SMA_1x            0.00273982      0.0321182      0.0946779         1.06844
SMA_1.25x         0.00260037       0.032885      0.0850889         1.08918
VWT              0.000239656      0.0498657      0.0657562         1.64727
SmoothSMA_1x      0.00318591      0.0322853       0.117453         1.07117
SmoothSMA_1.75x   0.00274672        0.03454      0.0879269         1.13224
SmoothSMA_1.25x   0.00303951      0.0328953       0.107611         1.08758
SMA_1.75x         0.00232147       0.034822      0.0659109         1.14203
SmoothFlat        0.00377149      0.0314325        0.15682         1.04882
SMA_1.50x         0.00246092      0.0337899      0.0754999         1.11379
SmoothSMA_2x      0.00260032      0.0355552       0.078085         1.16003

                   R2_PDF    R2_CDF
SMA_2x           0.903771  0.992265
Flat             0.968753  0.992547
SmoothSMA_1.50x   0.95073  0.993553
SMA_1x           0.972246  0.995309  <- best so far
SMA_1.25x        0.965888  0.995327
VWT              -1.48218  0.897883
SmoothSMA_1x     0.962765   0.99321
SmoothSMA_1.75x  0.936193  0.993066
SmoothSMA_1.25x  0.959016  0.993569
SMA_1.75x        0.934083  0.993924
SmoothFlat       0.952365  0.989308
SMA_1.50x        0.953886  0.994903
SmoothSMA_2x     0.913201  0.991999

1m
The stats for the current predictions are below
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00451674       0.051019      0.0584349         1.14538
Flat              0.00698539      0.0435976       0.201194         1.01104
SmoothSMA_1.50x    0.0059278      0.0481081       0.124568         1.09083
SMA_1x            0.00575106      0.0456882       0.129814         1.04762
SMA_1.25x         0.00544248       0.046749       0.111969          1.0668
VWT               0.00334956      0.0610057       0.105574         1.38395
SmoothSMA_1x       0.0065712      0.0461674       0.160939         1.05662
SmoothSMA_1.75x    0.0056061      0.0493626       0.106383         1.11338
SmoothSMA_1.25x    0.0062495      0.0470387       0.142754         1.07184
SMA_1.75x         0.00482532       0.049429      0.0762797         1.11591
SmoothFlat          0.007858      0.0448537        0.23368         1.03608
SMA_1.50x          0.0051339      0.0480007      0.0941246         1.08964
SmoothSMA_2x       0.0052844      0.0507886      0.0881981         1.13929
                   R2_PDF    R2_CDF
SMA_2x           0.932778  0.994111
Flat             0.934605  0.983295
SmoothSMA_1.50x  0.950325  0.991789
SMA_1x           0.965723  0.992558
SMA_1.25x        0.966692  0.993867
VWT               0.38322  0.961928
SmoothSMA_1x     0.947608  0.988569
SmoothSMA_1.75x  0.943922  0.992553
SmoothSMA_1.25x  0.951088  0.990434
SMA_1.75x        0.952199  0.994749  <- close second
SmoothFlat       0.906721  0.977011
SMA_1.50x        0.962768   0.99463  <- best so far b/c the stdev is slightly
SmoothSMA_2x     0.930064   0.99263                         more accurate

3M
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0135761      0.0819494        0.11785         1.06612
Flat               0.0207074      0.0726917       0.357776         1.00612
SmoothSMA_1.50x    0.0181819      0.0782335        0.24813         1.04388
SMA_1x             0.0171418      0.0742606       0.237813         1.00449
SMA_1.25x          0.0162504      0.0756566       0.207823         1.01422
VWT                0.0153759       0.100354       0.203238         1.24083
SmoothSMA_1x       0.0199652      0.0757719       0.308624            1.03
SmoothSMA_1.75x    0.0172903      0.0800143       0.217883         1.05663
SmoothSMA_1.25x    0.0190735      0.0768126       0.278377         1.03498
SMA_1.75x          0.0144675      0.0795264       0.147841          1.0452
SmoothFlat         0.0235316      0.0756386       0.429612         1.04993
SMA_1.50x          0.0153589        0.07742       0.177832         1.02784
SmoothSMA_2x       0.0163987      0.0821316       0.187636         1.07307

                   R2_PDF    R2_CDF
SMA_2x           0.964621  0.993323  <- best so far
Flat              0.80106  0.946308
SmoothSMA_1.50x  0.892979  0.973892
SMA_1x           0.910189   0.97665
SMA_1.25x        0.929844  0.982136
VWT               0.72701  0.969279
SmoothSMA_1x     0.844038  0.959897
SmoothSMA_1.75x  0.911448  0.979505
SmoothSMA_1.25x  0.870379  0.967353
SMA_1.75x        0.957998  0.990528
SmoothFlat       0.698923  0.920179
SMA_1.50x        0.946033  0.986777
SmoothSMA_2x     0.925065  0.984158

6m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0280502       0.121954       0.227674         1.08272
Flat               0.0420365       0.107232       0.506098         1.07761
SmoothSMA_1.50x    0.0361709       0.114493       0.408679         1.09017
SMA_1x             0.0350433       0.110204       0.366886         1.04715
SMA_1.25x           0.033295       0.112391       0.332083          1.0499
VWT                0.0360965       0.165593       0.332344         1.35748
SmoothSMA_1x       0.0399462       0.111282        0.48183         1.09359
SmoothSMA_1.75x    0.0342833       0.116904       0.372103         1.09441
SmoothSMA_1.25x    0.0380586        0.11261       0.445254         1.08989
SMA_1.75x          0.0297985       0.118303       0.262477         1.06778
SmoothFlat         0.0474968       0.111786       0.628133         1.14671
SMA_1.50x          0.0315467       0.115103        0.29728          1.0568
SmoothSMA_2x       0.0323956       0.119811       0.335527         1.10258

                   R2_PDF    R2_CDF
SMA_2x            0.89175  0.976744  <- best so far
Flat             0.575336  0.886067
SmoothSMA_1.50x  0.698923  0.925999
SMA_1x           0.776085  0.942377
SMA_1.25x        0.812701  0.952865
VWT              0.288528  0.920264
SmoothSMA_1x     0.597075  0.896114
SmoothSMA_1.75x    0.7408  0.938499
SmoothSMA_1.25x  0.651055  0.911904
SMA_1.75x        0.870975  0.970041
SmoothFlat       0.311153  0.812895
SMA_1.50x        0.844417  0.962079
SmoothSMA_2x     0.776545  0.949469

1Y
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0634719       0.189335       0.444182         1.20326
Flat               0.0891292       0.162478       0.769548         1.24149
SmoothSMA_1.50x    0.0770902       0.184548       0.660089         1.30269
SMA_1x             0.0763005       0.169085       0.606865         1.18784
SMA_1.25x          0.0730934          0.173       0.566194         1.18512
VWT                 0.081536       0.266594        0.57605         1.53631
SmoothSMA_1x       0.0835131       0.178047       0.743358         1.31856
SmoothSMA_1.75x    0.0738788       0.189042       0.618454          1.3008
SmoothSMA_1.25x    0.0803016       0.180867       0.701723         1.30862
SMA_1.75x           0.066679       0.183189       0.484853         1.19287
SmoothFlat         0.0963588       0.176024       0.909895         1.39608
SMA_1.50x          0.0698862       0.177722       0.525523          1.1868
SmoothSMA_2x       0.0706673        0.19429        0.57682         1.30299

                    R2_PDF    R2_CDF
SMA_2x            0.516268  0.902227  <- best so far
Flat            -0.0850619  0.693486
SmoothSMA_1.50x -0.0382944  0.767049
SMA_1x            0.287099  0.821005
SMA_1.25x         0.357948    0.8451
VWT               -0.87697   0.76319
SmoothSMA_1x     -0.218369   0.69983
SmoothSMA_1.75x  0.0356102  0.795472
SmoothSMA_1.25x  -0.123043  0.735255
SMA_1.75x         0.473076  0.885565
SmoothFlat       -0.709286  0.515671
SMA_1.50x         0.420061  0.866564
SmoothSMA_2x     0.0980988  0.820751

----------------------------------------------------------------------------
SMA predictions with the 30 day volatility adjusted for the trendspan
----------------------------------------------------------------------------
testPeriods = {7:'1w',14:'2w',30:'1m',90:'3m',180:'6m',365:'1y'}
for trendDays in testPeriods.keys():
    label = testPeriods[trendDays]
    print(label)
    output[label] = comparePrediction(priceData, None, trendDays, trendDays)

*access charts below with output[label][0]


1w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00109583      0.0295909      0.0264461         1.68041
Flat              0.00166482      0.0229773       0.116812         1.44492
SmoothSMA_1.50x   0.00138891      0.0264488      0.0632447          1.5587
SMA_1x            0.00138032      0.0252568      0.0716293         1.52039
SMA_1.25x          0.0013092      0.0261793      0.0603335         1.55325
VWT              -0.00112796      0.0442925      0.0573098         2.78773
SmoothSMA_1x      0.00153313      0.0247819      0.0859882         1.50058
SmoothSMA_1.75x    0.0013168      0.0274631      0.0518729         1.59546
SmoothSMA_1.25x   0.00146102      0.0255508      0.0746164         1.52697
SMA_1.75x         0.00116695      0.0283587      0.0377419         1.63361
SmoothFlat        0.00182157      0.0232218       0.131475         1.45341
SMA_1.50x         0.00123807      0.0272175      0.0490377         1.59111
SmoothSMA_2x      0.00124468      0.0285815      0.0405012         1.63691

                    R2_PDF     R2_CDF
SMA_2x            -1.87552   0.888342
Flat              0.102009   0.948653  <- best so far but was better with 1m
SmoothSMA_1.50x  -0.628083   0.925146                           volatility
SMA_1x           -0.339216   0.934878
SMA_1.25x        -0.583488   0.926836
VWT               -88.0513  0.0728294
SmoothSMA_1x     -0.209244   0.938776
SmoothSMA_1.75x  -0.947859   0.915116
SmoothSMA_1.25x  -0.386874   0.932962
SMA_1.75x          -1.3295   0.903818
SmoothFlat       0.0503864   0.945081
SMA_1.50x        -0.907036   0.916563
SmoothSMA_2x      -1.36536   0.902638

2w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00218202      0.0359705      0.0299053         1.23013
Flat              0.00329762      0.0306115       0.141078         1.08391
SmoothSMA_1.50x   0.00289311        0.03365      0.0844904         1.15457
SMA_1x            0.00273982      0.0321182      0.0854918         1.12232
SMA_1.25x         0.00260037       0.032885      0.0715952         1.14332
VWT              0.000239656      0.0498657      0.0609739         1.77509
SmoothSMA_1x      0.00318591      0.0322853       0.112782         1.11887
SmoothSMA_1.75x   0.00274672        0.03454      0.0703445         1.17873
SmoothSMA_1.25x   0.00303951      0.0328953      0.0986364         1.13454
SMA_1.75x         0.00232147       0.034822       0.043802          1.1975
SmoothFlat        0.00377149      0.0314325       0.169366         1.10264
SMA_1.50x         0.00246092      0.0337899      0.0576986         1.16847
SmoothSMA_2x      0.00260032      0.0355552      0.0561985         1.20677

                   R2_PDF    R2_CDF
SMA_2x           0.827021  0.988242
Flat             0.946246  0.990213
SmoothSMA_1.50x  0.916835  0.991927
SMA_1x           0.944802   0.99379  <- best so far but 1m volatility had a
SMA_1.25x        0.931437  0.993512                 little better agreement
VWT              -3.27992  0.850525
SmoothSMA_1x     0.937441  0.991681
SmoothSMA_1.75x   0.89423   0.99111
SmoothSMA_1.25x  0.930537  0.992084
SMA_1.75x        0.875751  0.990871
SmoothFlat       0.919949  0.985715
SMA_1.50x        0.909362  0.992581
SmoothSMA_2x     0.859961  0.989512

1m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00451674       0.051019      0.0584349         1.14538
Flat              0.00698539      0.0435976       0.201194         1.01104
SmoothSMA_1.50x    0.0059278      0.0481081       0.124568         1.09083
SMA_1x            0.00575106      0.0456882       0.129814         1.04762
SMA_1.25x         0.00544248       0.046749       0.111969          1.0668
VWT               0.00334956      0.0610057       0.105574         1.38395
SmoothSMA_1x       0.0065712      0.0461674       0.160939         1.05662
SmoothSMA_1.75x    0.0056061      0.0493626       0.106383         1.11338
SmoothSMA_1.25x    0.0062495      0.0470387       0.142754         1.07184
SMA_1.75x         0.00482532       0.049429      0.0762797         1.11591
SmoothFlat          0.007858      0.0448537        0.23368         1.03608
SMA_1.50x          0.0051339      0.0480007      0.0941246         1.08964
SmoothSMA_2x       0.0052844      0.0507886      0.0881981         1.13929

                   R2_PDF    R2_CDF
SMA_2x           0.932778  0.994111
Flat             0.934605  0.983295
SmoothSMA_1.50x  0.950325  0.991789
SMA_1x           0.965723  0.992558
SMA_1.25x        0.966692  0.993867
VWT               0.38322  0.961928
SmoothSMA_1x     0.947608  0.988569
SmoothSMA_1.75x  0.943922  0.992553
SmoothSMA_1.25x  0.951088  0.990434
SMA_1.75x        0.952199  0.994749
SmoothFlat       0.906721  0.977011
SMA_1.50x        0.962768   0.99463  <- best so far
SmoothSMA_2x     0.930064   0.99263

3m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0135761      0.0819494       0.142379        0.995033
Flat               0.0207074      0.0726917       0.337599        0.936588
SmoothSMA_1.50x    0.0181819      0.0782335       0.247828        0.968218
SMA_1x             0.0171418      0.0742606       0.239989        0.935796
SMA_1.25x          0.0162504      0.0756566       0.215586        0.945223
VWT                0.0153759       0.100354       0.183056         1.15495
SmoothSMA_1x       0.0199652      0.0757719       0.296897        0.953827
SmoothSMA_1.75x    0.0172903      0.0800143       0.223294        0.980934
SmoothSMA_1.25x    0.0190735      0.0768126       0.272362        0.959148
SMA_1.75x          0.0144675      0.0795264       0.166781        0.975005
SmoothFlat         0.0235316      0.0756386       0.395035        0.970622
SMA_1.50x          0.0153589        0.07742       0.191184        0.958343
SmoothSMA_2x       0.0163987      0.0821316       0.198759        0.997156

                   R2_PDF    R2_CDF
SMA_2x           0.967767  0.991712  <- best so far
Flat             0.827473  0.952366
SmoothSMA_1.50x  0.904595   0.97465
SMA_1x           0.907051  0.975797
SMA_1.25x        0.924608  0.980536
VWT              0.866902  0.980574
SmoothSMA_1x     0.864971  0.963403
SmoothSMA_1.75x  0.922109  0.979498
SmoothSMA_1.25x  0.885458  0.969278
SMA_1.75x        0.955612  0.988548
SmoothFlat       0.767352   0.93458
SMA_1.50x        0.940971  0.984801
SmoothSMA_2x     0.937496  0.983782

6m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0280502       0.121954       0.225865        0.958097
Flat               0.0420365       0.107232       0.467155        0.958902
SmoothSMA_1.50x    0.0361709       0.114493       0.357499        0.943121
SMA_1x             0.0350433       0.110204        0.34651        0.928499
SMA_1.25x           0.033295       0.112391       0.316349        0.930298
VWT                0.0360965       0.165593       0.282224         1.14304
SmoothSMA_1x       0.0399462       0.111282       0.420916        0.948574
SmoothSMA_1.75x    0.0342833       0.116904        0.32579        0.945895
SmoothSMA_1.25x    0.0380586        0.11261       0.389208        0.944018
SMA_1.75x          0.0297985       0.118303       0.256026        0.945185
SmoothFlat         0.0474968       0.111786       0.547751          1.0018
SMA_1.50x          0.0315467       0.115103       0.286188        0.935883
SmoothSMA_2x       0.0323956       0.119811       0.294081        0.952307

                   R2_PDF    R2_CDF
SMA_2x           0.919456  0.978858  <- best so far
Flat              0.68369  0.907448
SmoothSMA_1.50x  0.808506  0.946601
SMA_1x           0.818415  0.949675
SMA_1.25x        0.846254  0.958092
VWT              0.796055   0.96133
SmoothSMA_1x     0.740434  0.925463
SmoothSMA_1.75x   0.83897  0.955778
SmoothSMA_1.25x  0.775768  0.936516
SMA_1.75x        0.896907  0.972687
SmoothFlat       0.561383  0.869472
SMA_1.50x        0.872424   0.96576
SmoothSMA_2x     0.867299  0.964083

1y
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0634719       0.189335       0.396618         1.09723
Flat               0.0891292       0.162478       0.682531         1.14962
SmoothSMA_1.50x    0.0770902       0.184548       0.543099         1.16981
SMA_1x             0.0763005       0.169085       0.539575         1.09643
SMA_1.25x          0.0730934          0.173       0.503835         1.09144
VWT                 0.081536       0.266594       0.478604         1.35709
SmoothSMA_1x       0.0835131       0.178047       0.617017         1.18909
SmoothSMA_1.75x    0.0738788       0.189042       0.506139         1.16506
SmoothSMA_1.25x    0.0803016       0.180867       0.580058         1.17784
SMA_1.75x           0.066679       0.183189       0.432357         1.09184
SmoothFlat         0.0963588       0.176024       0.764855         1.26426
SMA_1.50x          0.0698862       0.177722       0.468096         1.08991
SmoothSMA_2x       0.0706673        0.19429        0.46918         1.16365

                    R2_PDF    R2_CDF
SMA_2x            0.708586  0.929931   <- best so far
Flat              0.215934  0.776112
SmoothSMA_1.50x   0.417678  0.859446
SMA_1x            0.509143  0.868289
SMA_1.25x         0.567115  0.886197
VWT              0.0910755  0.862621
SmoothSMA_1x      0.268242  0.814571
SmoothSMA_1.75x   0.481736  0.878611
SmoothSMA_1.25x   0.346584  0.838156
SMA_1.75x         0.666773  0.916919
SmoothFlat        -0.12414  0.693107
SMA_1.50x          0.61961   0.90237
SmoothSMA_2x      0.538869  0.895799


----------------------------------------------------------------------------
SMA predictions with the volatility calculated from half of the
    trendspan (minimum span = 30d) and adjusted for the trendspan
----------------------------------------------------------------------------
Code to generate data:
testPeriods = {7:'1w',14:'2w',30:'1m',90:'3m',180:'6m',365:'1y'}
for trendDays in testPeriods.keys():
    label = testPeriods[trendDays]
    print(label)
    output[label] = comparePrediction(priceData,None,trendDays,
                                      max(30,trendDays/2))

*access charts below with output[label][0]

1w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00109583      0.0295909      0.0371138         1.31733
Flat              0.00166482      0.0229773      0.0898886         1.07964
SmoothSMA_1.50x   0.00138891      0.0264488      0.0610089         1.20205
SMA_1x            0.00138032      0.0252568      0.0635012         1.15858
SMA_1.25x          0.0013092      0.0261793      0.0569043         1.19175
VWT              -0.00112796      0.0442925      0.0412871         2.04932
SmoothSMA_1x      0.00153313      0.0247819      0.0743519         1.14209
SmoothSMA_1.75x    0.0013168      0.0274631      0.0543374         1.23914
SmoothSMA_1.25x   0.00146102      0.0255508      0.0676804         1.16957
SMA_1.75x         0.00116695      0.0283587      0.0437106         1.27154
SmoothFlat        0.00182157      0.0232218       0.101038         1.08881
SMA_1.50x         0.00123807      0.0272175      0.0503075         1.22954
SmoothSMA_2x      0.00124468      0.0285815      0.0476659         1.28045

                   R2_PDF    R2_CDF
SMA_2x           0.628793  0.977267
Flat             0.969052  0.995321     <- best so far
SmoothSMA_1.50x  0.865729  0.989692
SMA_1x           0.918673  0.992982
SMA_1.25x        0.880964  0.990791
VWT              -10.6171  0.713266
SmoothSMA_1x     0.931794  0.993417
SmoothSMA_1.75x  0.806785  0.986403
SmoothSMA_1.25x  0.905701  0.991964
SMA_1.75x        0.743399  0.983156
SmoothFlat       0.960805  0.994097
SMA_1.50x        0.824711  0.987588
SmoothSMA_2x     0.722342  0.981885

2w
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00218202      0.0359705      0.0563219         1.17362
Flat              0.00329762      0.0306115       0.133034          1.0282
SmoothSMA_1.50x   0.00289311        0.03365      0.0977689         1.10801
SMA_1x            0.00273982      0.0321182      0.0946779         1.06844
SMA_1.25x         0.00260037       0.032885      0.0850889         1.08918
VWT              0.000239656      0.0498657      0.0657562         1.64727
SmoothSMA_1x      0.00318591      0.0322853       0.117453         1.07117
SmoothSMA_1.75x   0.00274672        0.03454      0.0879269         1.13224
SmoothSMA_1.25x   0.00303951      0.0328953       0.107611         1.08758
SMA_1.75x         0.00232147       0.034822      0.0659109         1.14203
SmoothFlat        0.00377149      0.0314325        0.15682         1.04882
SMA_1.50x         0.00246092      0.0337899      0.0754999         1.11379
SmoothSMA_2x      0.00260032      0.0355552       0.078085         1.16003

                   R2_PDF    R2_CDF
SMA_2x           0.903771  0.992265
Flat             0.968753  0.992547
SmoothSMA_1.50x   0.95073  0.993553
SMA_1x           0.972246  0.995309   <- best so far
SMA_1.25x        0.965888  0.995327
VWT              -1.48218  0.897883
SmoothSMA_1x     0.962765   0.99321
SmoothSMA_1.75x  0.936193  0.993066
SmoothSMA_1.25x  0.959016  0.993569
SMA_1.75x        0.934083  0.993924
SmoothFlat       0.952365  0.989308
SMA_1.50x        0.953886  0.994903
SmoothSMA_2x     0.913201  0.991999

1m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x            0.00451674       0.051019      0.0584349         1.14538
Flat              0.00698539      0.0435976       0.201194         1.01104
SmoothSMA_1.50x    0.0059278      0.0481081       0.124568         1.09083
SMA_1x            0.00575106      0.0456882       0.129814         1.04762
SMA_1.25x         0.00544248       0.046749       0.111969          1.0668
VWT               0.00334956      0.0610057       0.105574         1.38395
SmoothSMA_1x       0.0065712      0.0461674       0.160939         1.05662
SmoothSMA_1.75x    0.0056061      0.0493626       0.106383         1.11338
SmoothSMA_1.25x    0.0062495      0.0470387       0.142754         1.07184
SMA_1.75x         0.00482532       0.049429      0.0762797         1.11591
SmoothFlat          0.007858      0.0448537        0.23368         1.03608
SMA_1.50x          0.0051339      0.0480007      0.0941246         1.08964
SmoothSMA_2x       0.0052844      0.0507886      0.0881981         1.13929

                   R2_PDF    R2_CDF
SMA_2x           0.932778  0.994111
Flat             0.934605  0.983295
SmoothSMA_1.50x  0.950325  0.991789
SMA_1x           0.965723  0.992558
SMA_1.25x        0.966692  0.993867
VWT               0.38322  0.961928
SmoothSMA_1x     0.947608  0.988569
SmoothSMA_1.75x  0.943922  0.992553
SmoothSMA_1.25x  0.951088  0.990434
SMA_1.75x        0.952199  0.994749
SmoothFlat       0.906721  0.977011
SMA_1.50x        0.962768   0.99463   <- best so far
SmoothSMA_2x     0.930064   0.99263

3m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0135761      0.0819494       0.112245         1.04091
Flat               0.0207074      0.0726917        0.34732        0.981884
SmoothSMA_1.50x    0.0181819      0.0782335       0.233911         1.01486
SMA_1x             0.0171418      0.0742606       0.229782         0.98016
SMA_1.25x          0.0162504      0.0756566       0.200398        0.989739
VWT                0.0153759       0.100354       0.197708         1.21282
SmoothSMA_1x       0.0199652      0.0757719       0.293133          1.0011
SmoothSMA_1.75x    0.0172903      0.0800143         0.2043         1.02749
SmoothSMA_1.25x    0.0190735      0.0768126       0.263522         1.00603
SMA_1.75x          0.0144675      0.0795264       0.141629         1.02028
SmoothFlat         0.0235316      0.0756386       0.411577         1.02087
SMA_1.50x          0.0153589        0.07742       0.171013         1.00316
SmoothSMA_2x       0.0163987      0.0821316       0.174689         1.04379

                   R2_PDF    R2_CDF
SMA_2x           0.974601  0.994455   <- best so far
Flat             0.816609  0.949739
SmoothSMA_1.50x  0.911481  0.977317
SMA_1x           0.917649  0.978278
SMA_1.25x        0.936826  0.983522
VWT              0.780139  0.973228
SmoothSMA_1x     0.865457  0.964336
SmoothSMA_1.75x    0.9297  0.982566
SmoothSMA_1.25x  0.889953  0.971235
SMA_1.75x        0.966147  0.991667
SmoothFlat       0.735115  0.927939
SMA_1.50x         0.95322     0.988
SmoothSMA_2x     0.943966  0.986948

6m
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0280502       0.121954       0.204063        0.988947
Flat               0.0420365       0.107232       0.479869        0.994927
SmoothSMA_1.50x    0.0361709       0.114493       0.356449        0.987364
SMA_1x             0.0350433       0.110204       0.341966        0.961301
SMA_1.25x           0.033295       0.112391        0.30749        0.962475
VWT                0.0360965       0.165593        0.30627         1.23767
SmoothSMA_1x       0.0399462       0.111282       0.428614         0.99359
SmoothSMA_1.75x    0.0342833       0.116904       0.320367        0.989847
SmoothSMA_1.25x    0.0380586        0.11261       0.392531        0.988616
SMA_1.75x          0.0297985       0.118303       0.238538        0.976379
SmoothFlat         0.0474968       0.111786       0.572943         1.04906
SMA_1.50x          0.0315467       0.115103       0.273014        0.967521
SmoothSMA_2x       0.0323956       0.119811       0.284285         0.99604

                   R2_PDF    R2_CDF
SMA_2x           0.934558   0.98291   <- best so far
Flat             0.658112  0.901527
SmoothSMA_1.50x  0.806415  0.946952
SMA_1x           0.823669   0.95133
SMA_1.25x        0.855868  0.960785
VWT              0.633625  0.946021
SmoothSMA_1x     0.723527   0.92226
SmoothSMA_1.75x  0.842027  0.957352
SmoothSMA_1.25x  0.767024  0.935282
SMA_1.75x        0.911467  0.976566
SmoothFlat       0.496731  0.853932
SMA_1.50x        0.885141   0.96919
SmoothSMA_2x     0.873976  0.966535

1y
                MeanErrorPct StdDevErrorPct MeanStdDevFctr StdDevOfStdFctr
SMA_2x             0.0634719       0.189335        0.38699         1.05438
Flat               0.0891292       0.162478       0.703082         1.11489
SmoothSMA_1.50x    0.0770902       0.184548       0.554403         1.13477
SMA_1x             0.0763005       0.169085       0.545036         1.05614
SMA_1.25x          0.0730934          0.173       0.505525         1.05019
VWT                 0.081536       0.266594       0.511183         1.33785
SmoothSMA_1x       0.0835131       0.178047       0.635665         1.15573
SmoothSMA_1.75x    0.0738788       0.189042       0.513772         1.12938
SmoothSMA_1.25x    0.0803016       0.180867       0.595034         1.14358
SMA_1.75x           0.066679       0.183189       0.426502         1.04931
SmoothFlat         0.0963588       0.176024        0.79819         1.23547
SMA_1.50x          0.0698862       0.177722       0.466013          1.0479
SmoothSMA_2x       0.0706673        0.19429       0.473141         1.12748

                   R2_PDF    R2_CDF
SMA_2x           0.749086  0.935448   <- best so far
Flat             0.224236  0.765485
SmoothSMA_1.50x  0.445546  0.857149
SMA_1x           0.533977  0.868265
SMA_1.25x        0.595825  0.887931
VWT              0.103251  0.850292
SmoothSMA_1x     0.286548  0.807021
SmoothSMA_1.75x  0.514527  0.878405
SmoothSMA_1.25x  0.369616  0.833427
SMA_1.75x        0.703239  0.921395
SmoothFlat      -0.122135  0.669282
SMA_1.50x        0.652172  0.905593
SmoothSMA_2x     0.576677   0.89738


"""
