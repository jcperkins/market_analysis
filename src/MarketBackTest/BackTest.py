# -----------------------------------------------------------------------------
# Name:        MarketStrategies
# Purpose:     Contains the backtest class which will run strategies against
#                historical data
#
# Author:      a0225347
# o
# Created:     26/04/2017
# Copyright:   (c) a0225347 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------
# !/usr/bin/env python

import datetime
import os
from io import StringIO
import ffn
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from src.DataSource import InflationData, OptionData, StockData
import src.Utility as util
from src.Portfolio import Portfolio

ffn.extend_pandas()

"""
debug quick code:
from MarketAnalysis import MarketStrategies
from MarketAnalysis import MarketPortfolio
from MarketAnalysis import MarketData
reload(MarketStrategies)
reload(MarketData)
reload(MarketPortfolio)

#Rolling LEAP strategy
underlyingTckr = 'ixSPX'
bondPortion = 1.0
stockPercentages = tckrPercMap = {'AGG': bondPortion*0.2,
                                  'BOND' :bondPortion*0.2,
                                  'HYG': bondPortion*0.15,
                                  'TLT': bondPortion*0.05,
                                  'SHY': bondPortion*0.1,
                                  'ILTB': bondPortion*0.1,
                                  'IEI': bondPortion*0.1,
                                  'IEF': bondPortion*0.1}
defaultTckrs = {'BOND': 'AGG', 'HYG': 'AGG', 'TLT': 'AGG', 'SHY': 'AGG',
                'ILTB': 'AGG', 'IEI': 'AGG', 'IEF': 'AGG'}
rebalanceFreq = '6m'
spanPurchase = '2y'
discount = 0.8
rolledAfter = 'Expire'
maxRisk = 0.8
specialConditions = []
ms.Strategy_RollingIndexLEAP(
        underlyingTckr, spanPurchase, discount, rolledAfter, maxRisk,
        stockPercentages, rebalanceFreq, specialConditions=specialConditions)


ms.Strategy_BalancedMix(stockPercentages, rebalanceFreq, rebalanceThreshold,
                        defaultTckrs=defaultTckrs, startDate=startDate)
ms.setupAndPlotCompetingStrategies(rebalanceThreshold)
"""


# import urllib2

# -----------------------------------------------------------------------------
# Look into pandas correlation functions and window functions (window meaning
#   window of a graph, i.e. rolling average, st. dev, etc.)
# -----------------------------------------------------------------------------


class BackTest:
    def __init__(
        self,
        file_dir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        stockDB: Optional[StockData] = None,
        optionDB: Optional[OptionData] = None,
        liborDB: Optional[liborDB] = None,
        cpiDB: Optional[InflationData] = None,
        metric: str = "AnnRet_V1",
    ):
        self.mainDir = file_dir if file_dir is not None else default_settings.root_dir
        self.workProxyBool = (
            workProxy if workProxy is not None else default_settings.work_proxy
        )
        self.simulationStartDate = datetime.datetime(1990, 1, 1)
        self.proxies = {}
        if self.workProxyBool:
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        # initialize historical data
        if stockDB is None:
            self.stockDB = StockData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.stockDB = stockDB
        # initialize option data
        if optionDB is None:
            self.optionDB = OptionData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.optionDB = optionDB
        # initialize LIBOR_Rate data
        if liborDB is None:
            self.liborDB = self.optionDB.liborDB
        else:
            self.liborDB = liborDB
        # initialize inflation/CPI data
        if cpiDB is None:
            self.cpiDB = InflationData(
                fileDir=fileDir, workProxy=workProxy, webDriver=optionDB.driver
            )
        else:
            self.cpiDB = cpiDB
        marketClose = datetime.time(15, 30)
        now = datetime.datetime.today()
        self.lastHistoricalDate = datetime.datetime(now.year, now.month, now.day)
        if now.time() < marketClose:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        # make sure that the last date is one that has a chance of the
        #  markets being open
        while self.lastHistoricalDate.weekday() > 4:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        self.metric = metric
        # All Metric Options = ['Start', 'End', 'Risk-free rate',
        #                       'Total Return', 'Daily Sharpe', 'Daily Sortino',
        #                       'CAGR', 'Max Drawdown', 'Calmar Ratio', 'MTD',
        #                       '3m', '6m', 'YTD', '1Y', '3Y (ann.)',
        #                       '5Y (ann.)', '10Y (ann.)',
        #                       'Since Incep. (ann.)', 'Daily Sharpe',
        #                       'Daily Sortino', 'Daily Mean (ann.)',
        #                       'Daily Vol (ann.)', 'Daily Skew', 'Daily Krt',
        #                       'Best Day', 'Worst Day', 'Monthly Sharpe',
        #                       'Monthly Sortino', 'Monthly Mean (ann.)',
        #                       'Monthly Vol (ann.)', 'Monthly Skew',
        #                       'Monthly Krt', 'Best Month', 'Worst Month',
        #                       'Yearly Sharpe', 'Yearly Sortino',
        #                       'Yearly Mean', 'Yearly Vol', 'Yearly Skew',
        #                       'Yearly Krt', 'Best Year', 'Worst Year',
        #                       'Avg. Drawdown', 'Avg. Drawdown Days',
        #                       'Avg. Up Month', 'Avg. Down Month',
        #                       'Win Year %', 'Win 12m % ']
        self.metricCols = [
            "Total Retrn",
            "Daily Sharpe",
            "Daily Sortino",
            "CAGR",
            "Max Drawdown",
            "Calmar Ratio",
            "MTD",
            "3m",
            "6m",
            "YTD",
            "1Y",
            "3Y (ann.)",
            "5Y (ann.)",
            "10Y (ann.)",
            "Since Incep. (ann.)",
            "Daily Sharpe",
            "Daily Sortino",
            "Daily Mean (ann.)",
            "Daily Vol (ann.)",
            "Daily Skew",
            "Daily Krt",
            "Best Day",
            "Worst Day",
            "Monthly Sharpe",
            "Monthly Sortino",
            "Monthly Mean (ann.)",
            "Monthly Vol (ann.)",
            "Monthly Skew",
            "Monthly Krt",
            "Best Month",
            "Worst Month",
            "Yearly Sharpe",
            "Yearly Sortino",
            "Yearly Mean",
            "Yearly Vol",
            "Yearly Skew",
            "Yearly Krt",
            "Best Year",
            "Worst Year",
            "Avg. Drawdown",
            "Avg. Drawdown Days",
            "Avg. Up Month",
            "Avg. Down Month",
            "Win Year %",
            "Win 12m % ",
        ]
        self.optimizeCols = None
        self.optimizationFilePath = None
        self.initializeOptimizationStorage(fileDir)

    # =============================================================================
    # *****************************************************************************
    # ------------------------ Run Strategy Code Block ----------------------------
    # *****************************************************************************
    # =============================================================================

    def runStrategy(
        self,
        strategy,
        startDate=None,
        endDate=None,
        initialValue=10000,
        reInvestDiv=True,
        brokerageTradeFee=0.0,
        additionalOptionContractFee=0.0,
        printStatusBool=False,
        minimizePrint=False,
        showCharts=True,
        fileDir=None,
    ):
        global eventDate
        if strategy is None or not strategy.areConditionsValid():
            return None
        if startDate is None:
            startDate = self.simulationStartDate
        else:
            startDate = util.formatDateInput(startDate)
        if endDate is None:
            endDate = util.formatDateInput(datetime.date.today())
        else:
            endDate = util.formatDateInput(endDate)
        startDate, endDate = strategy.initilizeStrategy(
            self.stockDB,
            self.optionDB,
            self.liborDB,
            startDate=startDate,
            endDate=endDate,
        )
        if not minimizePrint:
            print(startDate, endDate)
        if fileDir is None:
            fileDir = os.path.join(self.mainDir, "PortfolioResults")
        pfName = strategy.getPortfolioName()

        pf = Portfolio.Portfolio(
            initialValue=initialValue,
            reInvestDiv=reInvestDiv,
            pfName=pfName,
            startDate=startDate,
            fileDir=fileDir,
            optionDB=self.optionDB,
            stockDB=self.stockDB,
            brokerageTradeFee=brokerageTradeFee,
            additionalOptionContractFee=additionalOptionContractFee,
            silent=False,
            source="temp_db",
        )

        currentDate = startDate
        eventFlags = ["Start"]
        eventDate = None
        sectionTitle = ""
        width = 50
        while currentDate < endDate:
            sectionTitle = (
                " Advance To "
                + currentDate.strftime("%Y-%m-%d")
                + " - Cmds: "
                + str(eventFlags)
            )
            if not minimizePrint:
                print("\n" + "*" * width)
                padding = max((width - len(sectionTitle)) / 2, 0)
                print("*" * padding + sectionTitle + "*" * padding)
                print("*" * width)
            else:
                print(sectionTitle)
            with util.HiddenPrints(active=minimizePrint):
                pf.advanceToDate(currentDate)
                # adjust BrokerageFees for inflation
                pf.brokerageTradeFee = self.cpiDB.inflationAdjustedValue(
                    currentDate, pf.brokerageTradeFee
                )
                pf.additionalOptionContractFee = self.cpiDB.inflationAdjustedValue(
                    currentDate, pf.additionalOptionContractFee
                )
                eventDate, eventFlags = strategy.process(
                    currentDate,
                    eventFlags,
                    pf,
                    self.stockDB,
                    self.optionDB,
                    self.liborDB,
                )
            assert eventDate >= currentDate, (
                "MarketBacktest.RunStrategy: The next eventDate has been set in the past. \nCurrentDate: %s   eventDate: %s"
                % (str(currentDate), str(eventDate))
            )
            if printStatusBool and not minimizePrint:
                pf.printStatus(statusTitle="Post Processing")
            currentDate = min(endDate, eventDate)
        #            if not printStatusBool and not minimizePrint:
        #                print current_date.strftime("%Y-%m-%d") + ' - '+ pfName
        # print strategy.eventDictionary
        with util.HiddenPrints(active=minimizePrint):
            pf.advanceToDate(endDate)
        pf.printStatus(statusTitle="Final Value")
        pf.finish(showCharts=showCharts)
        del currentDate, eventDate, eventFlags, sectionTitle, width
        return pf

    def runMultipleStrategies(
        self,
        strategyArray,
        startDate=None,
        endDate=None,
        initialValue=10000,
        reInvestDiv=True,
        brokerageTradeFee=0.0,
        additionalOptionContractFee=0.0,
        printStatusBool=False,
        showCharts=True,
        fileDir=None,
    ):
        """wrapper method to run multiple strategies and compare them
            o  strategyArray can be any grouping structure with an indexer (list, array, etc.)
        """
        if startDate is None:
            startDate = self.simulationStartDate
        else:
            startDate = util.formatDateInput(startDate)
        if endDate is None:
            endDate = util.formatDateInput(datetime.date.today())
        else:
            endDate = util.formatDateInput(endDate)
        # cycle through each strategy to find the date range that will work with all strategies
        for strgy in strategyArray:
            startDate, endDate = strgy.calculateStartEndDates(
                self.stockDB,
                self.optionDB,
                self.liborDB,
                startDate=startDate,
                endDate=endDate,
            )

        pfDict = {}
        totalValueDF = []
        if fileDir is None:
            fileDir = os.path.join(
                self.mainDir,
                "PortfolioResults",
                "Start_%04i%02i%02i" % (startDate.year, startDate.month, startDate.day),
            )
        else:
            fileDir = os.path.join(
                fileDir,
                "Start_%04i%02i%02i" % (startDate.year, startDate.month, startDate.day),
            )
        # cycle through and run each strategy
        for strgy in strategyArray:
            pfKey = strgy.getPortfolioName()
            print(
                "\nNow Working on strategy %01i of %01i: %s\n\n"
                % (strategyArray.index(strgy) + 1, len(strategyArray), pfKey)
            )
            pfDict[pfKey] = self.runStrategy(
                strgy,
                startDate=startDate,
                endDate=endDate,
                initialValue=initialValue,
                reInvestDiv=reInvestDiv,
                brokerageTradeFee=brokerageTradeFee,
                additionalOptionContractFee=additionalOptionContractFee,
                printStatusBool=printStatusBool,
                fileDir=fileDir,
                minimizePrint=False,
            )
            totalValueSeries = pfDict[pfKey].valueHistory.TotalValue
            totalValueSeries.name = pfKey
            totalValueDF.append(totalValueSeries)

        totalValueDF = pd.concat(totalValueDF, axis=1).fillna(method="ffill")
        groupStats, figLin, figLog = BackTest.compareStrategies(
            totalValueDF, fileDir, pfDict.keys(), showCharts=showCharts
        )
        if showCharts:
            plt.show()
        return totalValueDF

    @staticmethod
    def compareStrategies(totalValueDF, fileDir, strategyNameList, showCharts=True):
        figLin = None
        figLog = None
        groupStats = ffn.GroupStats(totalValueDF)
        groupStats.display()
        if not os.path.exists(fileDir):
            os.makedirs(fileDir)
        fName = "StatComparison_%s" % ("_".join(strategyNameList))
        if len(fName[:-4]) > 100:
            fName = fName[:100]
        # fName = fName+'.csv'
        groupStats.stats.to_csv(os.path.join(fileDir, fName + ".csv"))
        fName = "HistoryComparison_%s" % ("_".join(strategyNameList))
        if len(fName[:-4]) > 100:
            fName = fName[:100]
        totalValueDF.to_csv(os.path.join(fileDir, fName + ".csv"))
        if showCharts:
            plt.figure()
            totalValueDF.plot(logy=False, figsize=(15, 5), title="StrategyValue")
            plt.tight_layout()
            figLin = plt.gcf()
            figLin.savefig(os.path.join(fileDir, fName + ".png"))
            plt.figure()
            totalValueDF.plot(logy=True, figsize=(15, 5), title="StrategyValue_LogY")
            plt.tight_layout()
            figLog = plt.gcf()
            figLog.savefig(os.path.join(fileDir, fName + ".png"))
            plt.show(block=False)

        return groupStats, figLin, figLog

    # =============================================================================
    # *****************************************************************************
    # ------------------------ Optimization Code Block ----------------------------
    # *****************************************************************************
    # =============================================================================
    def initializeOptimizationStorage(
        self, filePath, storageName=None, optimizeCols=None
    ):
        if storageName is None:
            storageName = "BackTest_Optimize_%s.p" % self.metric
        self.optimizationFilePath = os.path.join(filePath, storageName)
        self.optimizeCols = optimizeCols

    def storeOptimizationRecords(self, reportDF, append=True):
        # process the reportDF
        df = reportDF.loc[~reportDF.isnull().all(axis=1), :].copy()
        df = self.formatOptimizationReport(df)
        # store the processed data
        df.to_pickle(self.optimizationFilePath)
        return True

    def loadOptimizationRecords(self, reportDF=None, columns=None):
        if reportDF is None:
            if os.path.exists(self.optimizationFilePath):
                print(self.optimizationFilePath)
                return pd.read_pickle(self.optimizationFilePath)
            else:
                return pd.DataFrame(index=range(10), columns=columns)
        else:
            df = reportDF.loc[~reportDF.isnull().all(axis=1), :].copy()
        if self.optimizationFilePath is not None and os.path.isfile(
            self.optimizationFilePath
        ):
            df = df.append(self.loadOptimizationRecords(), ignore_index=True)
        if columns is not None:
            for col in columns:
                if col not in df.columns:
                    df[col] = None
        df = self.formatOptimizationReport(df)
        return df

    def formatOptimizationReport(self, df):
        if self.optimizeCols is None:
            return df[~df.index.duplicated(keep="first")].sort_index()
        else:
            return df.drop_duplicates(
                subset=self.optimizeCols, keep="first", inplace=False
            ).sort_index()

    def getStrategyMetricSummary(self, totalValueSeries):
        """options for metric summary are:
            o  ['Start', 'End', 'Risk-free rate', 'Total Return', 'Daily Sharpe', 'Daily Sortino', 'CAGR',
                'Max Drawdown', 'Calmar Ratio', 'MTD', '3m', '6m', 'YTD', '1Y', '3Y (ann.)', '5Y (ann.)',
                '10Y (ann.)', 'Since Incep. (ann.)', 'Daily Sharpe', 'Daily Sortino', 'Daily Mean (ann.)',
                'Daily Vol (ann.)', 'Daily Skew', 'Daily Krt', 'Best Day', 'Worst Day', 'Monthly Sharpe',
                'Monthly Sortino', 'Monthly Mean (ann.)', 'Monthly Vol (ann.)', 'Monthly Skew', 'Monthly Krt',
                'Best Month', 'Worst Month', 'Yearly Sharpe', 'Yearly Sortino', 'Yearly Mean', 'Yearly Vol',
                'Yearly Skew', 'Yearly Krt', 'Best Year', 'Worst Year', 'Avg. Drawdown', 'Avg. Drawdown Days',
                'Avg. Up Month', 'Avg. Down Month', 'Win Year %', 'Win 12m % ']
        """
        data = totalValueSeries.loc[np.isfinite(totalValueSeries)]
        statsObj = ffn.PerformanceStats(data, rf=0.0)
        resultStr = statsObj.to_csv()
        while ",," in resultStr:
            resultStr = resultStr.replace(",,", ",")
        resultStr = resultStr.replace("\n,\n", "\n")
        statsDF = pd.read_csv(StringIO(resultStr), index_col=0, skip_blank_lines=True)
        statsDF.columns = ["Values"]
        metricSelect = np.intersect1d(self.metricCols, statsDF.index)
        return dict(zip(metricSelect, statsDF.loc[metricSelect, "Values"].tolist()))

    def getStrategyPerformanceMetric(self, totalValueSeries, metric=None):
        if metric is None:
            metric = self.metric
        else:
            self.metric = metric
        if metric == "AnnRet_V1":
            # annual return is a time weighted average of X durations getting sequentially smaller and always overlapping
            #   o i.e. the first return is the entire span, the second is the most recent 90% of the span, etc.
            # V1 is 10 durations
            return self.timeperformanceMetric(totalValueSeries, periods=10)
        elif metric == "RetDrwDwnProd_V1":
            return self.timeperformanceMetric_MaxDrawDown_Product(
                totalValueSeries, periods=10
            )
        elif metric == "SharpeRatio_V1":
            return self.timeWeightedSharpeRatio(totalValueSeries, periods=10)
        elif metric == "CalmarRatio_V1":
            return self.timeWeightedCalmarRatio(totalValueSeries, periods=10)
        elif metric == "PureCalmar_V1":
            return self.pureCalmar(totalValueSeries)
        elif metric == "PureCAGR_V1":
            return self.pureCAGR(totalValueSeries)
        else:
            return np.nan

    @staticmethod
    def timeperformanceMetric(totalValueSeries, periods=10):
        """break up the run time into 10 different start dates and rebase the returns.
            recalculate the relevant functions based on a the average return of the
            portfolio weighted by the duration.
            V1 is defined as periods = 10
        """
        #        pfValues = self.valueHistory.TotalValue
        #        pfValues.name = self.pfName
        #        self.pfStatObj = ffn.PerformanceStats(pfValues,rf=0.0)
        startDate = min(totalValueSeries.index)
        endDate = max(totalValueSeries.index)
        totalDays = (endDate - startDate).days
        deltaDays = ((endDate - startDate) / 10).floor("d").days * np.arange(periods)
        weighting = (totalDays - deltaDays).astype(float)
        weighting = weighting / sum(weighting)
        startArray = (startDate + pd.TimedeltaIndex(data=deltaDays, unit="d")).floor(
            "d"
        )
        #        partialDurationStat = np.zeros(weighting.shape)
        weightedAverage = 0
        for i in range(len(startArray)):
            statValue = (
                ffn.PerformanceStats(totalValueSeries[startArray[i] :], rf=0.0).incep
                * 100
            )
            weightedAverage += weighting[i] * statValue
        return weightedAverage

    @staticmethod
    def timeperformanceMetric_MaxDrawDown_Product(totalValueSeries, periods=10):
        startDate = min(totalValueSeries.index)
        endDate = max(totalValueSeries.index)
        totalDays = (endDate - startDate).days
        deltaDays = ((endDate - startDate) / 10).floor("d").days * np.arange(periods)
        weighting = (totalDays - deltaDays).astype(float)
        weighting = weighting / sum(weighting)
        startArray = (startDate + pd.TimedeltaIndex(data=deltaDays, unit="d")).floor(
            "d"
        )
        #        partialDurationStat = np.zeros(weighting.shape)
        weightedAverage = 0
        for i in range(len(startArray)):
            statObj = ffn.PerformanceStats(totalValueSeries[startArray[i] :], rf=0.0)
            annRet = statObj.incep * 100
            drawDown = statObj.max_drawdown
            weightedAverage += weighting[i] * annRet * (1 + drawDown)
        return weightedAverage

    @staticmethod
    def timeWeightedSharpeRatio(totalValueSeries, periods=10):
        startDate = min(totalValueSeries.index)
        endDate = max(totalValueSeries.index)
        totalDays = (endDate - startDate).days
        deltaDays = ((endDate - startDate) / 10).floor("d").days * np.arange(periods)
        weighting = (totalDays - deltaDays).astype(float)
        weighting = weighting / sum(weighting)
        startArray = (startDate + pd.TimedeltaIndex(data=deltaDays, unit="d")).floor(
            "d"
        )
        #        partialDurationStat = np.zeros(weighting.shape)
        weightedAverage = 0
        for i in range(len(startArray)):
            statObj = ffn.PerformanceStats(totalValueSeries[startArray[i] :], rf=0.0)
            ratio = statObj.yearly_sharpe
            weightedAverage += weighting[i] * ratio
        return weightedAverage

    @staticmethod
    def timeWeightedCalmarRatio(totalValueSeries, periods=10):
        startDate = min(totalValueSeries.index)
        endDate = max(totalValueSeries.index)
        totalDays = (endDate - startDate).days
        deltaDays = ((endDate - startDate) / 10).floor("d").days * np.arange(periods)
        weighting = (totalDays - deltaDays).astype(float)
        weighting = weighting / sum(weighting)
        startArray = (startDate + pd.TimedeltaIndex(data=deltaDays, unit="d")).floor(
            "d"
        )
        #        partialDurationStat = np.zeros(weighting.shape)
        weightedAverage = 0
        for i in range(len(startArray)):
            statObj = ffn.PerformanceStats(totalValueSeries[startArray[i] :], rf=0.0)
            ratio = statObj.calmar
            weightedAverage += weighting[i] * ratio
        return weightedAverage

    @staticmethod
    def pureCalmar(totalValueSeries):
        return ffn.PerformanceStats(totalValueSeries, rf=0.0).calmar

    @staticmethod
    def pureCAGR(totalValueSeries):
        return ffn.PerformanceStats(totalValueSeries, rf=0.0).cagr

    def runStrategyAndGetperformanceMetric(
        self,
        reportDF,
        strategyClass,
        argsParms,
        kwParms,
        argParmNames,
        staticRunConditions,
        hidePrints=False,
    ):
        optimizationLoop, startDate, endDate, initialValue, reInvestDiv, brokerageTradeFee, additionalOptionContractFee, printStatusBool, showCharts, fileDir, minimizePrint, forceResults = (
            staticRunConditions
        )
        reportDFIndex = self.optimizationReport_GetIndexByConditions(
            reportDF, argsParms, kwParms, argParmNames
        )
        if reportDFIndex is None or forceResults:
            strategy = strategyClass(*argsParms, **kwParms)
            with util.HiddenPrints(active=hidePrints):
                pf = self.runStrategy(
                    strategy,
                    startDate=startDate,
                    endDate=endDate,
                    initialValue=initialValue,
                    reInvestDiv=reInvestDiv,
                    brokerageTradeFee=brokerageTradeFee,
                    additionalOptionContractFee=additionalOptionContractFee,
                    showCharts=showCharts,
                    fileDir=fileDir,
                    minimizePrint=minimizePrint,
                )
            if pf is not None:
                performanceMetric = self.getStrategyPerformanceMetric(
                    pf.valueHistory.TotalValue
                )
                metricResultDict = self.getStrategyMetricSummary(
                    pf.valueHistory.TotalValue
                )
                result = pf.valueHistory.TotalValue.copy()
            else:
                performanceMetric = np.nan
                result = None
                metricResultDict = {}
            reportDF = self.optimizationReport_SaveRecord(
                reportDF,
                optimizationLoop,
                argsParms,
                kwParms,
                argParmNames,
                performanceMetric,
                metricResultDict,
            )
            self.storeOptimizationRecords(reportDF)
        else:
            performanceMetric = reportDF.loc[reportDFIndex, "PerformanceMetric"]
            pf = None
            result = None
        return reportDF, performanceMetric, result

    def optimizeStrategy(
        self,
        strategyClass,
        initArgParms=None,
        initKwParms=None,
        argOptimizationOrder=None,
        kwOptmizationOrder=None,
        argParmNames=None,
        weightedAvgLimit=0.1,
        resultsFolder=None,
        resultsSubFolder=None,
        reportDF=None,
        startDate=None,
        endDate=None,
        initialValue=100000,
        reInvestDiv=True,
        brokerageTradeFee=4.95,
        additionalOptionContractFee=0.15,
        printStatusBool=False,
        showCharts=False,
        fullOptimization=False,
        fullOptContVarSegQty=5,
        forceIterationPlotting=False,
        debug=False,
    ):
        """Optimize the stratgey based on the initial conditions and the optimization order parameters
            o  strategy is a strategy that is compatible with backtest.runstrategy
            o  initArgParms are the initial values of the strategies arguments
            o  initKwParms are the initial values of the strategies key word arguments
            o  argOptimizationOrder is a list of tuples and the tuples will be an order and
                dictionary of optimizing parameters. Each tuple represents the strategy argument
                at the corresponding index. The dictionary will be optimizing parameters
                ('Values','Min','Max',etc.)
            o  kwOptimiztionOrder will be a dictionary that will be matched to the initKwParms dictionary.
                the values of the dictionary will be tuples of the same form as the tuples in
                argOptimiaztionOrder
            o  fullOptiization - is a boolean that will allow the function to do a full factorial
                search of the solution space, before it finds the local maximum. The thought is to
                get a survey to find the most probable spot that the local maximum from the resulting
                inital conditions will be the absolute maximum return. This will be a very time
                consuming process and the number of runs will be the product of the qty of options
                available for each variable.
            o  fulOptContVarSegQty - is the number of tests to run across the allowed space of a
                designated continous variable

debug:
weightedAvgLimit=0.1; resultsFolder=None;resultsSubFolder=None;reportDF=None;printStatusBool=False; showCharts=False;initialStepAsPercent = 0.5;conditionResult = None;forceIterativePlotting=False
        """
        if initKwParms is None:
            initKwParms = {}
        if argOptimizationOrder is None:
            argOptimizationOrder = []
        if kwOptmizationOrder is None:
            kwOptmizationOrder = {}
        if argParmNames is None:
            argParmNames = []
        if initArgParms is None:
            initArgParms = []
        minimizePrint = True
        if resultsFolder is None or resultsFolder == "PortfolioResults\\":
            resultsFolder = "Optimize_" + strategyClass.__name__
        if resultsSubFolder is None:
            resultsSubFolder = "Parms_"
        # find number of parameters to optimize and create an order to index to instruction mapping
        parameterQty = 0
        orderToKey = (
            {}
        )  # {Order to optimize part:parameter Key which is the index if the Parm is in the initArgParms list or the name of the Parm if it is in initKwParms}
        keyToInstructions = {}  # parameter Key mapped to instructions for optimizing
        for argIndex in range(len(argOptimizationOrder)):
            if type(argOptimizationOrder[argIndex]) == tuple:
                if argOptimizationOrder[argIndex][0] > 0:
                    orderToKey[argOptimizationOrder[argIndex][0]] = argIndex
                    keyToInstructions[argIndex] = argOptimizationOrder[argIndex][1]
                    parameterQty += 1
        for kw in kwOptmizationOrder.keys():
            if type(kwOptmizationOrder[kw]) == tuple:
                if kwOptmizationOrder[kw][0] > 0:
                    orderToKey[kwOptmizationOrder[kw][0]] = kw
                    keyToInstructions[kw] = kwOptmizationOrder[kw][1]
                    parameterQty += 1
        # create the list of static and dynamic variables to calculate a final report columns list
        dynamicCols = []
        for i in np.arange(max(orderToKey.keys())) + 1:
            key = orderToKey[i]
            if type(key) == int:
                dynamicCols.append(argParmNames[key])
            else:
                dynamicCols.append(key)
        dynamicCols.reverse()
        staticCols = []
        for name in argParmNames:
            if not name in dynamicCols:
                staticCols.append(name)
        for name in initKwParms.keys():
            if not name in dynamicCols:
                staticCols.append(name)

        # define the fileDir for the program and initilize the program
        if resultsSubFolder == "Parms_":
            resultsSubFolder += "_".join(dynamicCols)
        if "indexOptions" in initKwParms.keys():
            fileDir = os.path.join(
                self.mainDir,
                "PortfolioResults",
                resultsFolder,
                "StartingValue_%01i_" % initialValue
                + "_".join(initKwParms["indexOptions"]),
                resultsSubFolder,
            )
        else:
            fileDir = os.path.join(
                self.mainDir,
                "PortfolioResults",
                resultsFolder,
                "StartingValue_%01i" % initialValue,
                resultsSubFolder,
            )
        self.optimizationFilePath = fileDir
        if not os.path.exists(fileDir):
            os.makedirs(fileDir)
        self.initializeOptimizationStorage(fileDir, optimizeCols=dynamicCols)
        reportCols = list(
            set(
                ["Round"]
                + staticCols
                + dynamicCols
                + ["PerformanceMetric"]
                + self.metricCols
            )
        )
        reportDF = self.loadOptimizationRecords(reportDF=reportDF, columns=reportCols)

        # initialize optimization routine
        iteration = 0
        staticRunConditions = (
            iteration,
            startDate,
            endDate,
            initialValue,
            reInvestDiv,
            brokerageTradeFee,
            additionalOptionContractFee,
            printStatusBool,
            showCharts,
            fileDir,
            minimizePrint,
            forceIterationPlotting,
        )
        optimizeSequence = orderToKey.keys()
        optimizeSequence.sort()
        if fullOptimization:
            reportDF, optimizedArgs, optimizedKw, optimizedPerfMetric, optimizedResult = self.fullFactorialPreOptimize(
                strategyClass,
                list(initArgParms),
                initKwParms.copy(),
                argParmNames,
                orderToKey,
                keyToInstructions,
                fullOptContVarSegQty,
                staticRunConditions,
                reportDF=reportDF,
                debug=debug,
            )
            # calculate the start and end dates for the index
            startDate, endDate = strategyClass(
                *initArgParms, **initKwParms
            ).calculateStartEndDates(
                self.stockDB,
                self.optionDB,
                self.liborDB,
                startDate=startDate,
                endDate=endDate,
            )
            endDate = endDate + pd.Timedelta(days=1)
            returnIndex = pd.bdate_range(startDate, endDate)
            resultsDF = pd.DataFrame(
                index=returnIndex, columns=["Round_%01i" % iteration]
            )
        else:
            print(
                "\n"
                + "*" * 50
                + "\nRunning the strategy under the initial conditions\n"
                + "*" * 50
                + "\n"
            )
            reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
                reportDF,
                strategyClass,
                initArgParms,
                initKwParms,
                argParmNames,
                staticRunConditions,
            )
            optimizedResult = resultsDF = result
            if result is not None:
                resultsDF.columns = ["Round_%01i" % iteration]
            else:  # initiliaze the index
                # calculate the start and end dates for the index
                startDate, endDate = strategyClass(
                    *initArgParms, **initKwParms
                ).calculateStartEndDates(
                    self.stockDB,
                    self.optionDB,
                    self.liborDB,
                    startDate=startDate,
                    endDate=endDate,
                )
                endDate = endDate + pd.Timedelta(days=1)
                returnIndex = pd.bdate_range(startDate, endDate)
                resultsDF = pd.DataFrame(
                    index=returnIndex, columns=["Round_%01i" % iteration]
                )
            optimizedArgs, optimizedKw, optimizedPerfMetric, optimizedResult = (
                list(initArgParms),
                initKwParms.copy(),
                performanceMetric,
                result,
            )

        bestResult = None
        bestArgs, bestKW, bestPerfMetric, bestResult = (
            list(optimizedArgs),
            optimizedKw.copy(),
            optimizedPerfMetric,
            optimizedResult,
        )

        converged_Total = False
        strike_Total = (
            0
        )  # stop the loop if 2 strikes are reached. Strikes are when the new conditions are worse than the previous conditions
        while not converged_Total:
            iteration += 1
            print(
                "\n"
                + "*" * 50
                + "\nRound %01i of optimizing:" % iteration
                + "\n"
                + "*" * 50
            )
            for order in optimizeSequence:
                key = orderToKey[order]
                instructions = keyToInstructions[key]
                if type(key) == int:
                    argIndex = key
                    kwKey = None
                else:
                    kwKey = key
                    argIndex = None
                staticRunConditions = (
                    iteration,
                    startDate,
                    endDate,
                    initialValue,
                    reInvestDiv,
                    brokerageTradeFee,
                    additionalOptionContractFee,
                    printStatusBool,
                    showCharts,
                    fileDir,
                    minimizePrint,
                    forceIterationPlotting,
                )
                # quick debug: argParms=optimizedArgs;kwParms=optimizedKw;conditionResult = optimizedResult
                reportDF, optimizedArgs, optimizedKw, optimizedPerfMetric, optimizedResult = self.optimizeParameter(
                    strategyClass,
                    argParms=optimizedArgs,
                    kwParms=optimizedKw,
                    argIndex=argIndex,
                    kwKey=kwKey,
                    argParmNames=argParmNames,
                    reportDF=reportDF,
                    instructions=instructions,
                    conditionResult=optimizedResult,
                    staticRunConditions=staticRunConditions,
                    debug=debug,
                )

            # determine if the solution has converged:
            #   o  define convergence as abs(optimizedPerfMetric-previousAvgReturn)<weightedAvgLimit
            converged_Total = (
                abs(optimizedPerfMetric - bestPerfMetric) <= weightedAvgLimit
            )
            if optimizedPerfMetric > bestPerfMetric:
                strike_Total = 0
                bestPerfMetric = optimizedPerfMetric
                bestArgs = optimizedArgs
                bestKW = optimizedKw
                bestResult = optimizedResult
            else:
                strike_Total += 1
                print(
                    "Optimization Method produced a less effective set of conditions than the previous run "
                )
                if strike_Total == 1:
                    converged_Total = True
            if bestResult is not None:
                resultsDF["Round_%01i" % iteration] = bestResult
            else:
                resultsDF["Round_%01i" % iteration] = np.nan

        allArgs = dict(
            zip(argParmNames, bestArgs) + zip(bestKW.keys(), bestKW.values())
        )
        print("The optimized strategy args for the %s metric are:" % self.metric)
        print(str(allArgs))
        self.storeOptimizationRecords(reportDF, append=False)
        reportDF.to_csv(
            os.path.join(fileDir, "OptimizedConditions_Records_%s.csv" % self.metric)
        )
        resultsDF.to_csv(os.path.join(fileDir, "IterativeResults_%s.csv" % self.metric))
        return bestArgs, bestKW, bestPerfMetric, reportDF, resultsDF

    def fullFactorialPreOptimize(
        self,
        strategyClass,
        argParms,
        kwParms,
        argParmNames,
        orderToKey,
        keyToInstructions,
        fullOptContVarSegQty,
        staticRunConditions,
        reportDF=None,
        limitDefaultPerc=0.5,
        debug=False,
    ):
        # create a list of full factorial optimization parameters and kw arguments
        optimizeSequence = orderToKey.keys()
        optimizeSequence.sort()
        variableValues = {}
        loopCnt = []
        # quick pass to collect the variable possibilities
        for order in optimizeSequence:
            key = orderToKey[order]
            instructions = keyToInstructions[key]
            # get the key or index to reference the correct parm
            if type(key) == int:
                argIndex = key
                kwKey = None
            else:
                kwKey = key
                argIndex = None
            # get the parm name and the original value of the parm in case min/max instructions are missing.
            if argIndex is not None:
                originalValue = argParms[argIndex]
            else:
                originalValue = kwParms[kwKey]
            # collect the variable values
            if "Values" in instructions.keys():
                variableValues[key] = instructions["Values"]
                loopCnt.append(len(instructions["Values"]))
            else:
                if "Min" in instructions.keys():
                    minLimit = instructions["Min"]
                else:
                    minLimit = originalValue * (1 - limitDefaultPerc)
                if "Max" in instructions.keys():
                    maxLimit = instructions["Max"]
                else:
                    maxLimit = originalValue * (1 + limitDefaultPerc)
                loopCnt.append(fullOptContVarSegQty)
                stepSize = (maxLimit - minLimit) / float(fullOptContVarSegQty)
                variableValues[key] = np.arange(
                    minLimit + stepSize / 2.0, maxLimit, stepSize
                )
        loopCnt = np.prod(loopCnt)

        # create list of argument/keyword conditions to run
        def adaptConditionsWithNewParmValue(argParms, kwParms, variableOptions):
            argsToChange, kwToChange, variableValues = (
                list(argParms),
                kwParms.copy(),
                variableOptions.copy(),
            )
            if len(variableValues.keys()) == 0:
                return []
            key = variableValues.keys()[0]
            values = variableValues.pop(key)
            conditions = []
            for i in range(len(values)):
                if type(key) == int:
                    argsToChange[key] = values[i]
                else:
                    kwToChange[key] = values[i]
                conditions = (
                    conditions
                    + [(list(argsToChange), kwToChange.copy())]
                    + adaptConditionsWithNewParmValue(
                        list(argsToChange), kwToChange.copy(), variableValues.copy()
                    )
                )
            del argsToChange, kwToChange, variableValues
            return conditions

        conditionList = adaptConditionsWithNewParmValue(
            argParms, kwParms, variableValues
        )
        if debug:
            print(len(conditionList))

        if reportDF is None:
            reportDF = self.loadOptimizationRecords(
                reportDF,
                columns=list(
                    set(
                        ["Round"]
                        + argParmNames
                        + kwParms.keys()
                        + ["PerformanceMetric"]
                        + self.metricCols
                    )
                ),
            )
        # check if the initial conditions have already been run
        print(
            "\n"
            + "*" * 50
            + "\nRunning the strategy under the initial conditions\n"
            + "*" * 50
            + "\n"
        )
        reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
            reportDF,
            strategyClass,
            argParms,
            kwParms,
            argParmNames,
            staticRunConditions,
        )

        optimizedArgs = list(argParms)
        optimizedKW = kwParms.copy()
        optimizedWeightedReturn = performanceMetric
        optimizedResult = result
        # evaluate each possible value and set the optimized parameters to the best combination
        cnt = 0
        for condition in conditionList:
            cnt += 1
            nextArgs = condition[0]
            nextKW = condition[1]
            nameToValueDict = dict(
                zip(argParmNames, nextArgs) + zip(nextKW.keys(), nextKW.values())
            )
            print(
                "\n"
                + "*" * 50
                + "\nRunning condition %01i of %01i: %s"
                % (cnt, len(conditionList), str(nameToValueDict))
                + "\n"
                + "*" * 50
                + "\n"
            )
            reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
                reportDF,
                strategyClass,
                nextArgs,
                nextKW,
                argParmNames,
                staticRunConditions,
            )
            if performanceMetric > optimizedWeightedReturn:
                optimizedWeightedReturn, optimizedArgs, optimizedKW, optimizedResult = (
                    performanceMetric,
                    nextArgs[:],
                    nextKW.copy(),
                    result,
                )
            if debug:
                self.storeOptimizationRecords(reportDF)
        self.storeOptimizationRecords(reportDF)
        return (
            reportDF,
            optimizedArgs,
            optimizedKW,
            optimizedWeightedReturn,
            optimizedResult,
        )

    def optimizeParameter(
        self,
        strategyClass,
        weightedAvgLimit=0.1,
        argParms=None,
        kwParms=None,
        argIndex=None,
        kwKey=None,
        instructions=None,
        argParmNames=None,
        reportDF=None,
        initialStepAsPercent=0.5,
        conditionResult=None,
        staticRunConditions=(
            0,
            None,
            None,
            10000,
            True,
            4.95,
            0.15,
            False,
            False,
            None,
            True,
            False,
        ),
        debug=False,
    ):
        if argParms is None:
            argParms = []
        if kwParms is None:
            kwParms = {}
        if instructions is None:
            instructions = {}
        if argParmNames is None:
            argParmNames = []
        if "Values" in instructions.keys():
            return self.optimizeNominalParameter(
                strategyClass,
                weightedAvgLimit=weightedAvgLimit,
                argParms=argParms,
                kwParms=kwParms,
                argIndex=argIndex,
                kwKey=kwKey,
                instructions=instructions,
                argParmNames=argParmNames,
                reportDF=reportDF,
                conditionResult=conditionResult,
                staticRunConditions=staticRunConditions,
                debug=debug,
            )
        else:
            return self.optimizeContiniousParameter(
                strategyClass,
                weightedAvgLimit=weightedAvgLimit,
                argParms=argParms,
                kwParms=kwParms,
                argIndex=argIndex,
                kwKey=kwKey,
                instructions=instructions,
                argParmNames=argParmNames,
                reportDF=reportDF,
                initialStepAsPercent=initialStepAsPercent,
                conditionResult=conditionResult,
                staticRunConditions=staticRunConditions,
                debug=debug,
            )

    def optimizeContiniousParameter(
        self,
        strategyClass,
        weightedAvgLimit=0.1,
        argParms=None,
        kwParms=None,
        argIndex=None,
        kwKey=None,
        instructions=None,
        argParmNames=None,
        reportDF=None,
        initialStepAsPercent=0.5,
        conditionResult=None,
        staticRunConditions=(
            0,
            None,
            None,
            10000,
            True,
            4.95,
            0.15,
            False,
            False,
            None,
            True,
            False,
        ),
        debug=False,
    ):
        if argParms is None:
            argParms = []
        if kwParms is None:
            kwParms = {}
        if instructions is None:
            instructions = {}
        if argParmNames is None:
            argParmNames = []
        assert (
            argIndex is not None or kwKey is not None
        ), "BackTest.optimizeParameter: argParmIndex and kwKey cannot both be None"

        optimizationLoop, startDate, endDate, initialValue, reInvestDiv, brokerageTradeFee, additionalOptionContractFee, printStatusBool, showCharts, fileDir, minimizePrint, forceResults = (
            staticRunConditions
        )
        if reportDF is None:
            reportDF = self.addRowsToOptimizationReport(
                reportDF,
                columns=["Round"]
                + argParmNames
                + kwParms.keys()
                + ["PerformanceMetric"],
            )

        reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
            reportDF,
            strategyClass,
            argParms,
            kwParms,
            argParmNames,
            staticRunConditions,
        )
        optimizedArgs = argParms[:]
        optimizedKW = kwParms.copy()
        nextArgs = optimizedArgs[:]
        nextKW = optimizedKW.copy()
        optimizedWeightedReturn = performanceMetric
        if result is None:
            optimizedResult = conditionResult
        else:
            optimizedResult = result
        # interpret the argument to optimize and the instructions
        if argIndex is not None:
            optimizedValue = originalValue = optimizedArgs[argIndex]
            parmName = argParmNames[argIndex]
        else:
            optimizedValue = originalValue = optimizedKW[kwKey]
            parmName = kwKey
        if "Min" in instructions.keys():
            scoutMin = minLimit = instructions["Min"]
        else:
            minLimit = -1000000
            scoutMin = originalValue * (1 - initialStepAsPercent)
        if "Max" in instructions.keys():
            scoutMax = maxLimit = instructions["Max"]
        else:
            maxLimit = 1000000
            scoutMax = originalValue * (1 + initialStepAsPercent)
        if "ScoutOrder" in instructions.keys():
            scoutOrder = instructions["ScoutOrder"]
        else:
            scoutOrder = 1
        scoutValues = np.arange(scoutOrder) + 1
        scoutValues = (
            (
                originalValue
                - ((originalValue - scoutMin) / float(scoutOrder + 1)) * scoutValues
            ).tolist()
            + [originalValue]
            + (
                originalValue
                + ((scoutMax - originalValue) / float(scoutOrder + 1)) * scoutValues
            ).tolist()
        )
        scoutValues = self.formatOptimizeParmValue(scoutValues, parmName)
        # add existing data that may match the other parameters to the scoutValues
        matchingIndices = self.optimizationReport_GetAllMatchingIndices(
            reportDF, argParms, kwParms, argParmNames, ignoreCols=parmName
        )
        if matchingIndices is not None:
            scoutValues = np.array(
                list(
                    set(
                        scoutValues.tolist()
                        + reportDF.loc[matchingIndices, parmName].tolist()
                    )
                )
            )
        prevReturns = pd.Series(index=scoutValues, name="performanceMetric")
        prevReturns.sort_index(inplace=True)
        # do a optimization routine on a continious variable
        converged = False
        # first scout out the solutions space to find the best places to start optimizing
        for argValue in scoutValues:
            print(
                "\n"
                + "*" * 50
                + "\nRunning "
                + self.metric
                + " Scouting Condition: "
                + parmName
                + " = "
                + str(argValue)
                + "\n"
                + "*" * 50
                + "\n"
            )
            if argIndex is not None:
                nextArgs[argIndex] = argValue
            else:
                nextKW[kwKey] = argValue

            reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
                reportDF,
                strategyClass,
                nextArgs,
                nextKW,
                argParmNames,
                staticRunConditions,
            )
            if performanceMetric > optimizedWeightedReturn:
                optimizedWeightedReturn, optimizedArgs, optimizedKW, optimizedValue, optimizedResult = (
                    performanceMetric,
                    nextArgs[:],
                    nextKW.copy(),
                    argValue,
                    result,
                )
            prevReturns[argValue] = performanceMetric
            if debug:
                self.storeOptimizationRecords(reportDF)

        # Next, select a new value to run and test the result until the model converges on a maximum
        strikes = (
            0
        )  # stop the loop if 2 strikes are reached. Strikes are when the new conditions are worse than the previous conditions
        nextArgs = optimizedArgs[:]
        nextKW = optimizedKW.copy()
        plotList = []

        def createPlot(plotDetails, axes):
            for layer in plotDetails:
                args, keyWords = layer
                axes.plot(*args, **keyWords)
            axes.legend()
            return axes

        while not converged:
            # ********************************************************************************
            # **************** Optimization Method 1: Quadratic Maximum **********************
            # ********************************************************************************
            # fit the existing data to a quadratic function and calculate the maximimum of the trend to use in the next loop
            #            coef = np.polyfit(prevReturns.index,prevReturns.values,2)
            #            if(coef[0]<0):
            #                #the curvature is negative and the local maximum logic will work
            #                nextValue = -coef[1]/(2*coef[0])
            #                nextValue = max(min(nextValue,maxLimit),minLimit)
            ##                if(nextValue>maxLimit or nextValue<minLimit):
            ##                    converged=True
            ##                    continue
            #                x = np.arange(minLimit,maxLimit,(maxLimit-minLimit)/100.0)
            #                y = np.polyval(coef,x)
            #                plt.plot(x,y,prevReturns.index,prevReturns.values,'o',nextValue,np.polyval(coef,nextValue),'rx')
            #                plt.show(block=False)
            #                optMethodSatisfied = True
            #            else:
            #                raise ValueError("quadratic coeficient is positive which has not been programed yet")
            # ********************************************************************************
            # ************ Optimization Method 2: Avg(Interpolation,Fit) *********************
            # ********************************************************************************
            polyDeg = 3 if prevReturns.size > 4 else 2
            # logic to fit a function to data
            coef = np.polyfit(prevReturns.index, prevReturns.values, polyDeg)
            x = np.arange(minLimit, maxLimit, (maxLimit - minLimit) / 100.0)
            y = np.polyval(coef, x)
            maxValueFromFit = x[y.argmax()]
            maxReturnFromFit = y.max()
            # logig for interpolation method
            interpolation = prevReturns.append(pd.Series(index=x))
            interpolation.sort_index(inplace=True)

            try:
                interpolation = interpolation.interpolate(
                    method="polynomial", order=polyDeg
                )
            except Exception as error:
                print(
                    "Polynomial interpolation of order %01i failed. Now trying linear fit."
                    % polyDeg
                )
                interpolation = interpolation.interpolate(method="polynomial", order=1)

            maxValueFromInterp = interpolation.argmax()
            #            maxReturnFromInterp = interpolation[maxValueFromInterp] - This line would grab multiple rows if the index was duplicated
            maxReturnFromInterp = interpolation.max()
            # average and plot the predictions
            nextValue = (maxValueFromFit + maxValueFromInterp) / 2.0
            nextValue = self.formatOptimizeParmValue(nextValue, parmName)
            predictedReturn = (maxReturnFromFit + maxReturnFromInterp) / 2.0
            if showCharts:
                fig, ax = plt.subplots()
                plotDetails = [
                    ([x, y, "--"], {"color": "black", "label": "Fit"}),
                    (
                        [interpolation.index, interpolation.values, "-"],
                        {"color": "blue", "label": "Interp"},
                    ),
                    (
                        [prevReturns.index, prevReturns.values, "o"],
                        {"color": "orange", "label": "Raw Data"},
                    ),
                    ([maxValueFromFit, maxReturnFromFit, "x"], {"color": "black"}),
                    ([maxValueFromInterp, maxReturnFromInterp, "x"], {"color": "blue"}),
                    (
                        [nextValue, predictedReturn, "X"],
                        {"color": "red", "label": "AvgPred."},
                    ),
                ]
                plotList.append(plotDetails)
                ax = createPlot(plotDetails, ax)
                plt.show(block=False)
                fig.clear()
            optMethodSatisfied = (
                abs(maxValueFromInterp - maxValueFromFit) / nextValue < 0.02
            )  # force continued runs if the two models differ by more than 2% of the average
            print(
                "\n"
                + "*" * 50
                + "\nRunning theoretical "
                + self.metric
                + " peak condition "
                + parmName
                + (" at %01.2f\n" % nextValue)
                + "*" * 50
                + "\n"
            )

            if argIndex is not None:
                nextArgs[argIndex] = nextValue
            else:
                nextKW[kwKey] = nextValue
            reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
                reportDF,
                strategyClass,
                nextArgs,
                nextKW,
                argParmNames,
                staticRunConditions,
            )
            prevReturns[nextValue] = performanceMetric
            #            argumentValues.append(nextValue)
            #            annualReturns.append(performanceMetric)
            converged = (
                optMethodSatisfied
                and abs(performanceMetric - optimizedWeightedReturn) < weightedAvgLimit
                and abs(nextValue - optimizedValue) / optimizedValue < 0.02
            )
            if performanceMetric > optimizedWeightedReturn:
                optimizedWeightedReturn, optimizedArgs, optimizedKW, optimizedValue, optimizedResult = (
                    performanceMetric,
                    nextArgs,
                    nextKW,
                    nextValue,
                    result,
                )
                strikes = (
                    0
                )  # optimized conditions have improved so reset the strike count
            else:
                strikes += 1
                if strikes == 2:
                    converged = True
            if debug:
                self.storeOptimizationRecords(reportDF)

        # create a summary plot of all optimization steps
        if showCharts:
            plotTot_Sq = np.sqrt(len(plotList))
            rows = int(np.ceil(plotTot_Sq))
            cols = int(np.ceil(plotTot_Sq))
            # fig = plt.figure()
            for plotNum in range(len(plotList)):
                ax = plt.subplot(rows, cols, plotNum + 1)
                ax = createPlot(plotList[plotNum], ax)
            plt.show(block=False)
        self.storeOptimizationRecords(reportDF)
        return (
            reportDF,
            optimizedArgs,
            optimizedKW,
            optimizedWeightedReturn,
            optimizedResult,
        )

    def optimizeNominalParameter(
        self,
        strategyClass,
        weightedAvgLimit=0.1,
        argParms=None,
        kwParms=None,
        argIndex=None,
        kwKey=None,
        instructions=None,
        argParmNames=None,
        reportDF=None,
        conditionResult=None,
        staticRunConditions=(
            0,
            None,
            None,
            10000,
            True,
            4.95,
            0.15,
            False,
            False,
            None,
            True,
            False,
        ),
        debug=False,
    ):
        if argParms is None:
            argParms = []
        if kwParms is None:
            kwParms = {}
        if instructions is None:
            instructions = {}
        if argParmNames is None:
            argParmNames = []
        assert (
            argIndex is not None or kwKey is not None
        ), "BackTest.optimizeParameter: argParmIndex and kwKey cannot both be None"
        optimizationLoop, startDate, endDate, initialValue, reInvestDiv, brokerageTradeFee, additionalOptionContractFee, printStatusBool, showCharts, fileDir, minimizePrint, forceResults = (
            staticRunConditions
        )
        if argIndex is not None:
            parmName = argParmNames[argIndex]
        else:
            parmName = kwKey
        print(
            "\n"
            + "*" * 50
            + "\nOptimizing "
            + self.metric
            + " Nominal Variable: "
            + parmName
            + "\n"
            + "*" * 50
            + "\n"
        )

        if reportDF is None:
            reportDF = self.addRowsToOptimizationReport(
                reportDF,
                columns=["Round"]
                + argParmNames
                + kwParms.keys()
                + ["PerformanceMetric"],
            )
        # check if the initial conditions have already been run
        reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
            reportDF,
            strategyClass,
            argParms,
            kwParms,
            argParmNames,
            staticRunConditions,
        )

        optimizedArgs = argParms[:]
        optimizedKW = kwParms.copy()
        optimizedWeightedReturn = performanceMetric
        if result is None:
            optimizedResult = conditionResult
        else:
            optimizedResult = result
        # evaluate each possible value and set the optimized parameters to the best combination
        nextArgs = optimizedArgs[:]
        nextKW = optimizedKW.copy()
        for argValue in instructions["Values"]:
            print(
                "\n"
                + "*" * 50
                + "\nRunning "
                + self.metric
                + " condition "
                + parmName
                + " = "
                + str(argValue)
                + "\n"
                + "*" * 50
                + "\n"
            )
            if argIndex is not None:
                nextArgs[argIndex] = argValue
            else:
                nextKW[kwKey] = argValue
            reportDF, performanceMetric, result = self.runStrategyAndGetperformanceMetric(
                reportDF,
                strategyClass,
                nextArgs,
                nextKW,
                argParmNames,
                staticRunConditions,
            )
            if performanceMetric > optimizedWeightedReturn:
                optimizedWeightedReturn, optimizedArgs, optimizedKW, optimizedValue, optimizedResult = (
                    performanceMetric,
                    nextArgs[:],
                    nextKW.copy(),
                    argValue,
                    result,
                )
        self.storeOptimizationRecords(reportDF)
        return (
            reportDF,
            optimizedArgs,
            optimizedKW,
            optimizedWeightedReturn,
            optimizedResult,
        )

    @staticmethod
    def formatOptimizeParmValue(argValue, parmName):
        """
        Convenient location for individual formatting needs so that they can be kept in one place
        """
        if parmName.upper() in [
            "TARGETSTRIKETOPRICERATIO",
            "MAXPFRISK",
            "REBALANCETHRESHOLD",
            "STKPRTN",
            "RBLNCFRQ",
            "RBLNCTHRSHLD",
            "AGGPERC",
            "BONDPERC",
            "TLTPERC",
            "HYGPERC",
            "ILTBPERC",
            "SHYPERC",
            "IEIPERC",
            "IEFPERC",
        ]:
            # percentages and other 2 decimal float variables
            argValue = np.round(argValue, decimals=2)
        return argValue

    @staticmethod
    def addRowsToOptimizationReport(reportDF, columns=None, rows=10):
        if type(reportDF) == pd.DataFrame:
            columns = reportDF.columns
            newRows = pd.DataFrame(index=range(rows), columns=columns)
            reportDF = reportDF.append(newRows, ignore_index=True)
        else:
            reportDF = pd.DataFrame(index=range(rows), columns=columns)
        return reportDF

    @staticmethod
    def optimizationReport_GetIndexByConditions(reportDF, argParms, KWParms, argNames):
        """Returns the first index value for a row that matches the given parameters"""
        matchingIndices = BackTest.optimizationReport_GetAllMatchingIndices(
            reportDF, argParms, KWParms, argNames, ignoreCols=None
        )
        if matchingIndices is not None and matchingIndices.size > 0:
            return min(matchingIndices)
        else:
            return None

    @staticmethod
    def optimizationReport_GetAllMatchingIndices(
        reportDF, argParms, KWParms, argNames, ignoreCols=None
    ):
        nameToValueDict = dict(
            zip(argNames, argParms) + zip(KWParms.keys(), KWParms.values())
        )
        if ignoreCols is not None:
            if type(ignoreCols) == str:
                ignoreCols = [ignoreCols]
            for col in ignoreCols:
                nameToValueDict.pop(col, None)
        for key in nameToValueDict.keys():
            if type(nameToValueDict[key]) in [
                list,
                np.ndarray,
                dict,
                pd.DataFrame,
                pd.Series,
            ]:
                nameToValueDict[key] = str(nameToValueDict[key])
            elif pd.isna(nameToValueDict[key]):
                nameToValueDict.pop(key)
        matchingBoolArray = [True] * reportDF.index.size
        for key in nameToValueDict.keys():
            matchingBoolArray = np.bitwise_and(
                matchingBoolArray, reportDF[key] == nameToValueDict[key]
            )
        if sum(matchingBoolArray) > 0:
            return reportDF.index[matchingBoolArray]
        else:
            return None

    @staticmethod
    def optimizationReport_SaveRecord(
        reportDF,
        optimizationLoop,
        ArgParms,
        KwParms,
        argParmNames,
        performanceMetric,
        metricResultDict,
    ):
        nameToValueDict = dict(
            zip(argParmNames, ArgParms)
            + zip(KwParms.keys(), KwParms.values())
            + zip(metricResultDict.keys(), metricResultDict.values())
        )
        nameToValueDict["Round"] = optimizationLoop
        nameToValueDict["PerformanceMetric"] = performanceMetric
        writeIndex_Report = reportDF.index[reportDF.isnull().all(axis=1)]
        if writeIndex_Report.size > 0:
            writeIndex_Report = min(writeIndex_Report)
        else:
            print("Adding more rows to reportDF")
            writeIndex_Report = reportDF.index.size
            reportDF = BackTest.addRowsToOptimizationReport(reportDF)
        for statName in nameToValueDict.keys():
            if type(nameToValueDict[statName]) in [
                list,
                np.ndarray,
                dict,
                pd.DataFrame,
                pd.Series,
            ]:
                nameToValueDict[statName] = str(nameToValueDict[statName])
        reportDF.loc[
            writeIndex_Report, nameToValueDict.keys()
        ] = nameToValueDict.values()
        return reportDF


def main():
    print("MarketBackTest\\BackTest.py")


if __name__ == "__main__":
    main()
