# -----------------------------------------------------------------------------
# Name:        BackTestTools
# Purpose:     Contains the backtest class which will run strategies against
#                historical data
#
# Author:      a0225347
# o
# Created:     26/04/2017
# Copyright:   (c) a0225347 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------
# !/usr/bin/env python

from src.DataSource import DataTools
from src.MarketBackTest import BackTest
from src.Strategies import RollingSPXOption
import ffn

# from StringIO import StringIO
from src.Utility import PyBridge

pyB = PyBridge()
StringIO = pyB.import_StringIO()

ffn.extend_pandas()


def optimizeRollingSPXCall():
    optionType: str = "Call"
    purchaseSpan: str = "2y"
    rollAfterSpan: str = "1y"
    targetStrikeToPriceRatio = 0.8
    maxPfRisk = 0.2
    additionalConditions = []
    positionQty = 4
    targetStockPerc = {"AGG": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {}
    rebalanceThreshold = 0.02
    pfName = None
    startDate = None
    endDate = None
    initialValue = 100000
    reInvestDiv = True
    brokerageTradeFee = 4.95
    additionalOptionContractFee = 0.15
    printStatusBool = False
    showCharts = False
    fullOptimization = False
    fullOptContVarSegQty = 5
    forceIterationPlotting = False
    indexOptions = ["ixSPX", "SPY"]
    strategyClass = RollingSPXOption.RollingSPXOption

    initArgParms = [
        optionType,
        purchaseSpan,
        rollAfterSpan,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
    ]
    initKwParms = {
        "defaultTckrs": defaultTckrs,
        "additionalConditions": additionalConditions,
        "positionQty": positionQty,
        "pfName": pfName,
        "indexOptions": indexOptions,
    }
    argOptimizationOrder = [
        (-1, {}),
        (4, {"Values": ["3m", "6m", "1y", "2y"]}),
        (3, {"Values": ["3m", "6m", "1y"]}),
        (1, {"Min": 0.75, "Max": 1.2, "ScoutOrder": 3}),
        (2, {"Min": 0.05, "Max": 0.75, "ScoutOrder": 3}),
    ]
    kwOptmizationOrder = {}
    argParmNames = [
        "optionType",
        "purchaseSpan",
        "rollAfterSpan",
        "targetStrikeToPriceRatio",
        "maxPfRisk",
        "targetStockPerc",
        "rebalanceFreq",
        "rebalanceThreshold",
    ]

    #    sDB,oDB,lDB,iDB = DataTools.initiateAllDataBases(workProxy=True)
    #    #self=bt=BackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)
    #    bt=BackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)
    sDB, oDB, lDB, iDB = DataTools.initiateAllDataBases(workProxy=True)
    self = bt = BackTest.BackTest(
        workProxy=sDB.workProxyBool, stockDB=sDB, optionDB=oDB, liborDB=lDB
    )
    strategyArray = [RollingSPXOption.RollingSPXCalls()]
    strategy = strategyArray[0]
    bt.optimizeStrategy(
        strategyClass,
        initArgParms=initArgParms,
        initKwParms=initKwParms,
        argOptimizationOrder=argOptimizationOrder,
        kwOptmizationOrder=kwOptmizationOrder,
        argParmNames=argParmNames,
        startDate=startDate,
        endDate=endDate,
        initialValue=initialValue,
        reInvestDiv=reInvestDiv,
        brokerageTradeFee=brokerageTradeFee,
        additionalOptionContractFee=additionalOptionContractFee,
        printStatusBool=printStatusBool,
        showCharts=showCharts,
        fullOptimization=fullOptimization,
        fullOptContVarSegQty=fullOptContVarSegQty,
        forceIterationPlotting=forceIterationPlotting,
        debug=True,
    )


def main():
    """
# Debug Quick Code:

from MarketAnalysis import MarketBackTest,MarketData,MarketPortfolio,OptionPositions,MarketHelper as Util
import pandas as pd,numpy as np,datetime
"""
    from src.MarketBackTest import BackTest
    from src.DataSource import StockData
    from src.DataSource import OptionData
    from src.DataSource import InflationData
    from src.DataSource import DataTools
    from src.Portfolio import Portfolio
    from src.Positions import SimpleOption
    import src.Utility as Util
    from src.Strategies import RebalanceStock, RollingSPXOption
    import pandas as pd, numpy as np, datetime, os
    import ffn

    ffn.extend_pandas()
    from matplotlib import pyplot as plt

    startDate = None
    # startDate = "2008-11-05"
    endDate = None
    initialValue = 100000
    reInvestDiv = True
    brokerageTradeFee = 4.95
    additionalOptionContractFee = 0.15
    reload = pyB.reload_function()
    reload(BackTest)
    reload(Portfolio)
    reload(Util)
    reload(RebalanceStock)
    reload(RollingSPXOption)
    reload(SimpleOption)
    sDB, oDB, lDB, iDB = DataTools.initiateAllDataBases(workProxy=True)
    self = bt = BackTest.BackTest(
        workProxy=sDB.workProxyBool, stockDB=sDB, optionDB=oDB, liborDB=lDB
    )
    #    strategyArray = [RollingSPXOption.RollingSPXCalls()]
    #    strategy = strategyArray[0]
    #    pf = bt.runStrategy(strategyArray[0],startDate=startDate,endDate=endDate,initialValue = initialValue,reInvestDiv=reInvestDiv,brokerageTradeFee = brokerageTradeFee,additionalOptionContractFee = additionalOptionContractFee)

    #    reload(MarketData)
    #    reload(MarketPortfolio)
    #    reload(Util)
    #    sDB,oDB,lDB,iDB = MarketData.initiateAllDataBases(workProxy=True)
    #    #sDB,oDB,lDB,iDB = MarketData.initiateAllDataBases(workProxy=False)
    #    #self=bt=BackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)
    #    bt=BackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)

    #    reload(RebalanceStock)
    #    reload(RollingSPXOption)
    #    optimizeRollingSPXCall()
    #    optimizeRollingSPXCall()
    #
    strategyArray = [
        RebalanceStock.SPYOnly(),
        RebalanceStock.BalancedEquity(),
        RebalanceStock.Leveraged(),
        RollingSPXOption.OptimizezedRollingSPXCalls_100k(),
    ]
    #
    #    strategyArray = RebalanceStock.cycleFreqAndThreshold_Leveraged()
    #    #strategyArray = RebalanceStock.cycleFreqAndThreshold_BalancedEquity() + #RebalanceStock.cycleFreqAndThreshold_Leveraged()
    #    #strategyArray = strategyArray+ RollingSPXOption.cycleMaxRiskAndStrikeRatio_RollingSPXCalls()
    #    strategy = strategyArray[0]
    #
    totalValueDF = bt.runMultipleStrategies(
        strategyArray,
        startDate=startDate,
        endDate=endDate,
        initialValue=initialValue,
        reInvestDiv=reInvestDiv,
        brokerageTradeFee=brokerageTradeFee,
        additionalOptionContractFee=additionalOptionContractFee,
    )
    #
    #    #pf = bt.runStrategy(strategyArray[0], startDate=startDate, endDate=endDate, initialValue=initialValue, reInvestDiv=reInvestDiv, brokerageTradeFee=brokerageTradeFee, additionalOptionContractFee=additionalOptionContractFee, printStatusBool=True)

    """
#quick debug block for 6m purchase of SPXROllingCalls
startDate=None;endDate=None;initialValue = 600000;reInvestDiv=True;brokerageTradeFee = 4.95;additionalOptionContractFee = 0.15;reload(MarketBackTest); reload(MarketData); reload(MarketPortfolio); reload(Util); reload(RebalanceStock); reload(RollingSPXOption); sDB,oDB,lDB,iDB=MarketData.initiateAllDataBases(workProxy=True); self=bt=MarketBackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB);strategyArray = [];strategyArray.append(RollingSPXOption.RollingSPXCalls());strategy = strategyArray[0]

printStatusBool=False;showCharts=True;self=bt
if(startDate is None):
    startDate = self.simulationStartDate
else:
    startDate = Util.formatDateInput(startDate)
if(endDate is None):
    endDate = Util.formatDateInput(datetime.date.today())
else:
    endDate = Util.formatDateInput(endDate)
startDate,endDate = strategy.initilizeStrategy(self.stockDB,self.optionDB,self.liborDB,startDate=startDate,endDate=endDate)
print(startDate,endDate)

pfName = strategy.getPortfolioName()

pf = MarketPortfolio.Portfolio(initialValue=initialValue,reInvestDiv=reInvestDiv,pfName=pfName,startDate = startDate,fileDir=self.mainDir,optionDB=self.optionDB,stockDB = self.stockDB,brokerageTradeFee = brokerageTradeFee,additionalOptionContractFee = additionalOptionContractFee,silent=False,source='temp_db')

current_date = startDate
eventFlags = ['Start']
width = 50
while(current_date<endDate):
    print('\n'+'*'*width)
    sectionTitle = ' Advance To ' + current_date.strftime('%Y-%m-%d') + ' - Cmds: ' + str(eventFlags)
    padding = max((width-len(sectionTitle))/2,0)
    print('*'*padding + sectionTitle + '*'*padding)
    print('*'*width)
    pf.advanceToDate(current_date)
    #adjust BrokerageFees for inflation
    pf.brokerageTradeFee = self.cpiDB.inflationAdjustedValue(current_date,pf.brokerageTradeFee)
    pf.additionalOptionContractFee = self.cpiDB.inflationAdjustedValue(current_date,pf.additionalOptionContractFee)
    eventDate,eventFlags = strategy.process(current_date,eventFlags,pf,self.stockDB,self.optionDB,self.liborDB)
    assert (eventDate>=current_date), "MarketBacktest.RunStrategy: The next eventDate has been set in the past. \nCurrentDate: %s   eventDate: %s" % (str(current_date),str(eventDate))
    if(printStatusBool):
        pf.printStatus(statusTitle = "Post Processing")
    current_date = min(endDate,eventDate)
    if not printStatusBool:
        print(current_date.strftime("%Y-%m-%d") + ' - '+ pfName)
    #print(strategy.eventDictionary)
pf.advanceToDate(endDate)
pf.printStatus(statusTitle = "Final Value")
pf.finish(showCharts=showCharts)
    """


#    pass

if __name__ == "__main__":
    main()
# MA = MarketAnalysis()
# stockDist = MA.exploreStockDistribution('SPY')
# stockDist.to_csv(r'C:\Users\cp035982\Python\Projects\MarketAnalysis\Data\SPYDist_20170622.csv')
