"""
-------------------------------------------------------------------------------
 Name:        Portfolio
 Purpose:     Portfolio class for tracking the value and trades of
              a hypothetical portfolio driven by a strategy in analysis

 Author:       Cory Perkins

 Created:      19/07/2017
 Copyright:   (c) Cory Perkins 2017
 Licence:     <your licence>
-------------------------------------------------------------------------------
"""
import numpy as np
import datetime
import os
import ffn
from matplotlib import pyplot as plt
import pandas as pd
from src.DataSource import StockData, OptionData
import src.Utility as Util
from src.Positions.StockPosition import StockPosition
from src.Positions import SimpleOption
from typing import Optional
from config import default_settings


class Portfolio:
    def __init__(
        self,
        loadFromHist=False,
        fPathHistory=None,
        initialValue=10000,
        reInvestDiv=True,
        pfName="GenericPortfolio",
        startDate=datetime.datetime(1990, 1, 1),
        fileDir: Optional[str] = None,
        optionDB=None,
        stockDB=None,
        brokerageTradeFee=0.0,
        additionalOptionContractFee=0.0,
        silent=False,
        source="TEMPDB",
    ):
        if fileDir is None:
            fileDir = default_setttings.root_dir
        if loadFromHist:
            # TODO
            pass
        else:
            if reInvestDiv:
                pfSuffix = "ReinvestDiv"
            else:
                pfSuffix = "CashDiv"
            self.pfName = pfName
            self.mainDir = fileDir
            self.stockPositions = {}
            self.optionPositions = {}
            self.cash = initialValue
            self.log = []
            self.startDate = Util.formatDateInput(startDate)
            self.currentDay = self.startDate
            self.reInvestDiv = reInvestDiv
            self.stockValues = {}
            self.optionValues = {}
            self.brokerageTradeFee = brokerageTradeFee
            self.additionalOptionContractFee = additionalOptionContractFee
            self.source = source.upper()
            if "PortfolioResults" in fileDir:
                self.fileDir = os.path.join(fileDir, pfName + "_" + pfSuffix)
            else:
                self.fileDir = os.path.join(
                    fileDir, "PortfolioResults", pfName + "_" + pfSuffix
                )
            if not os.path.exists(self.fileDir):
                os.makedirs(self.fileDir)
            self.priceHistoryFPath = os.path.join(self.fileDir, "HistoricalPrice.csv")
            self.logFPath = os.path.join(self.fileDir, "Log.csv")
            self.plotDir = self.fileDir
            self.valuesPlotFig = None
            self.sourcesPlotFig = None
            if not stockDB:
                self.stockDB = StockData()
            else:
                self.stockDB = stockDB
            if not optionDB:
                self.optionDB = OptionData()
            else:
                self.optionDB = optionDB
        # init daily value record
        deltaDaysList = range((datetime.datetime.today() - startDate).days)
        self.pfStatObj = None

        def calculateDeltaDate(startDate, deltaDays):
            newDate = startDate + datetime.timedelta(days=deltaDays)
            if newDate.isoweekday() < 6:
                return newDate
            else:
                return datetime.datetime(1, 1, 1)

        pfValueIndex = np.array(
            map(
                lambda x: np.datetime64(calculateDeltaDate(startDate, x)), deltaDaysList
            )
        )
        pfValueIndex = pfValueIndex[pfValueIndex >= startDate]
        self.valueHistory = pd.DataFrame(
            index=pfValueIndex, columns=["TotalValue", "Cash"]
        )
        self.removeColsFromPlots = [
            "Cash",
            "ixVIX",
            "ixVXV",
            "ixVXO",
            "VIX",
            "VXN",
            "VXO",
        ]
        self.log = []
        self.logColomns = ["Action", "Object", "Qty", "Cost", "Comment"]
        self.silent = silent
        self.lastTckrPercMap = None
        self.lastTradeThresholdPerc = None

    def buyStock(self, tckr, qty):
        if tckr in self.stockValues.keys() and self.stockPositions[tckr].qty > 0:
            tckrPrice = self.stockValues[tckr] / float(self.stockPositions[tckr].qty)
            self.stockPositions[tckr].addQty(qty)
            self.stockValues[tckr] = self.stockPositions[tckr].qty * tckrPrice
        else:
            # open a position
            self.stockPositions[tckr] = StockPosition(
                tckr, qty, stockDB=self.stockDB, source=self.source
            )
            tckrPrice = self.stockPositions[tckr].getPrice(self.currentDay)
            self.stockValues[tckr] = self.stockPositions[tckr].qty * tckrPrice
        # self.cash -= self.brokerageTradeFee
        if tckr not in self.valueHistory.columns:
            self.valueHistory[tckr] = 0
        netCashFlow = -qty * tckrPrice - self.brokerageTradeFee
        self.cash += netCashFlow
        self.writeToLog("Buy Stock", tckr, qty, netCashFlow)
        return netCashFlow

    def sellStock(self, tckr, qty):
        if (
            tckr in self.stockValues.keys()
            and tckr in self.stockPositions.keys()
            and self.stockPositions[tckr].qty > 0
        ):
            qty = min(qty, self.stockPositions[tckr].qty)
            tckrPrice = self.stockValues[tckr] / float(self.stockPositions[tckr].qty)
            self.stockPositions[tckr].addQty(-qty)
            self.stockValues[tckr] = self.stockPositions[tckr].qty * tckrPrice
            # if qty == 0 then close the position
            if self.stockPositions[tckr].qty < 0.01:
                self.stockPositions.pop(tckr)
                self.stockValues.pop(tckr)
        else:
            # shorting stock is not currently supported
            print("This portfolio doesn't have any %s to sell" % tckr)
            return 0
        # self.cash -= self.brokerageTradeFee
        netCashFlow = qty * tckrPrice - self.brokerageTradeFee
        self.cash += netCashFlow
        self.writeToLog("Sell Stock", tckr, qty, netCashFlow)
        return netCashFlow

    def transactOption(
        self,
        name,
        contracts,
        underlyingTckr,
        strike,
        exDate,
        optionType,
        eventType="Transaction",
    ):
        notEnoughDataError = ValueError(
            "Not enough data provided to func 'buyOption': if a "
            + "meaningful name is not provided or does not match "
            + "an existing position, then 'underlyingTckr', 'strike', "
            + "'exDate', and 'optionType' must all be provided"
        )

        # check for a functional name
        if name is None or name.upper() == "Default":
            if (
                underlyingTckr is None
                or strike is None
                or exDate is None
                or optionType is None
            ):
                raise notEnoughDataError
            else:
                underlyingTckr = Util.convertTckrToUniversalFormat(underlyingTckr)
                name = underlyingTckr.replace("ix", "") + "%02i%02i%02i%s%01i" % (
                    self.exDate.year % 100,
                    self.exDate.month,
                    self.exDate.day,
                    optionType[0].upper(),
                    strike,
                )

        # process the transaction
        if name in self.optionValues.keys() and name in self.optionPositions.keys():
            # make sure that the posistion is either empty or is
            #  exaclty the same
            assert name in self.optionPositions.keys(), (
                "%s is in optionValues, but not Option Keys" % name
            )
            if (
                Util.convertTckrToUniversalFormat(underlyingTckr)
                == Util.convertTckrToUniversalFormat(self.optionPositions[name].tckr)
                and strike == self.optionPositions[name].strike
                and exDate == self.optionPositions[name].exDate
                and optionType == self.optionPositions[name].optionType
            ):
                self.optionPositions[name].addContracts(contracts)
            elif self.optionPositions[name].contracts == 0:
                self.optionPositions[name].addContracts(contracts)
                self.optionPositions[name].tckr = underlyingTckr
                self.optionPositions[name].strike = strike
                self.optionPositions[name].exDate = exDate
                self.optionPositions[name].optionType = optionType
            else:
                # need to sell existing position before any information
                #  can change
                self.sellOption(name, self.optionPositions[name].contracts)
                if optionType.upper() == "CALL":
                    self.optionPositions[name] = SimpleOption.Call(
                        underlyingTckr,
                        strike,
                        exDate,
                        contracts,
                        positionName=name,
                        optionDB=self.optionDB,
                        source=self.source,
                    )
                elif optionType.upper() == "PUT":
                    self.optionPositions[name] = SimpleOption.Put(
                        underlyingTckr,
                        strike,
                        exDate,
                        contracts,
                        positionName=name,
                        optionDB=self.optionDB,
                        source=self.source,
                    )
                else:
                    raise ValueError(
                        "optionType value of "
                        + optionType
                        + " is not supported or recognized"
                    )
                self.optionPositions[name].addContracts(contracts)
                self.optionPositions[name].tckr = underlyingTckr
                self.optionPositions[name].strike = strike
                self.optionPositions[name].exDate = exDate
                self.optionPositions[name].optionType = optionType
        else:
            # open a position
            if (
                underlyingTckr is None
                or strike is None
                or exDate is None
                or optionType is None
            ):
                raise notEnoughDataError
            if optionType.upper() == "CALL":
                self.optionPositions[name] = SimpleOption.Call(
                    underlyingTckr,
                    strike,
                    exDate,
                    contracts,
                    positionName=name,
                    optionDB=self.optionDB,
                    source=self.source,
                )
            elif optionType.upper() == "PUT":
                self.optionPositions[name] = SimpleOption.Put(
                    underlyingTckr,
                    strike,
                    exDate,
                    contracts,
                    positionName=name,
                    optionDB=self.optionDB,
                    source=self.source,
                )
            else:
                raise ValueError(
                    "optionType value of "
                    + optionType
                    + " is not supported or recognized"
                )
        self.optionValues[name] = self.optionPositions[name].calculateValue(
            self.currentDay
        )
        # self.cash -= self.brokerageTradeFee
        if name not in self.valueHistory.columns:
            self.valueHistory[name] = 0
        assert name in self.optionPositions.keys(), (
            "Cannot calculate the transaction cost, "
            + name
            + " is no longer in optionPosition dictionary"
        )
        valueOfOptions = (
            -self.optionPositions[name].calculateValueOfContract_BS(self.currentDay)
            * contracts
        )
        netCashFlow = (
            valueOfOptions
            - self.brokerageTradeFee
            - abs(contracts) * self.additionalOptionContractFee
        )
        self.cash += netCashFlow
        self.writeToLog(
            eventType + " Option",
            name + "-Exp " + str(self.optionPositions[name].exDate),
            contracts,
            netCashFlow,
        )
        if self.optionPositions[name].contracts == 0:
            # close the position
            self.optionPositions.pop(name)
            self.optionValues.pop(name)
        if self.cash < 0:
            self.rebalance(self.lastTckrPercMap, self.lastTradeThresholdPerc)
        return netCashFlow

    def buyOption(self, name, contracts, underlyingTckr, strike, exDate, optionType):
        """
        Shell function that calls self.transactOption with correct
        parameters and formatting
        """
        netCashFlow = self.transactOption(
            name,
            contracts,
            underlyingTckr=underlyingTckr,
            strike=strike,
            exDate=exDate,
            optionType=optionType,
            eventType="Buy",
        )
        assert contracts == 0 or name in self.optionPositions.keys(), (
            "Option %s should have been purchased but was not" % name
        )
        return netCashFlow

    def sellOption(
        self,
        name,
        contracts,
        underlyingTckr=None,
        strike=None,
        exDate=None,
        optionType=None,
    ):
        """
        Shell function that calls self.transactOption with correct
        parameters and formatting
        """
        if exDate is None:
            exDate = self.optionPositions[name].exDate
            if exDate - self.currentDay <= pd.Timedelta(days=1):
                return self.exciseOption(name, contracts)
        if self.currentDay == exDate:
            return self.exciseOption(name, contracts)
        if underlyingTckr is None:
            underlyingTckr = self.optionPositions[name].tckr
        if strike is None:
            strike = self.optionPositions[name].strike
        if optionType is None:
            optionType = self.optionPositions[name].optionType
        return self.transactOption(
            name,
            -contracts,
            underlyingTckr=underlyingTckr,
            strike=strike,
            exDate=exDate,
            optionType=optionType,
            eventType="Sell",
        )

    def exciseOption(self, name, contracts):
        netCashFlow = 0
        if name in self.optionValues.keys():
            # need to determine if the position was originally bought or sold
            positionContracts = int(
                self.optionPositions[name].qty / self.optionPositions[name].contractSize
            )
            if positionContracts < 0:
                # if position is a liability...
                contracts = min(abs(contracts), abs(positionContracts))
                self.optionPositions[name].addContracts(contracts)
                # make the contracts negative at the end so that it can
                #  inverse the profit/loss result that is calculated as if
                #  the position was an asset
                contracts = -contracts
            else:
                # the position is an asset...
                contracts = min(abs(contracts), abs(positionContracts))
                self.optionPositions[name].addContracts(-contracts)
            self.optionValues[name] = self.optionPositions[name].calculateValue(
                self.currentDay
            )
            netCashFlow = (
                self.optionPositions[name].calculateValueOfContract_Excised(
                    self.currentDay
                )
                * contracts
            )
            self.cash += netCashFlow
        self.writeToLog("Excised Option", name, contracts, netCashFlow)
        return netCashFlow

    def rebalance(self, tckrPercMap, tradeThresholdPerc):
        initialCash = self.cash
        self.lastTckrPercMap = tckrPercMap
        self.lastTradeThresholdPerc = tradeThresholdPerc
        totalStockValue = sum(self.stockValues.values()) + self.cash
        # first check stock positions and if any holdings are not found
        #  then sell them
        newTckrs = tckrPercMap.keys()
        for currentTckr in self.stockValues.keys():
            if currentTckr not in newTckrs:
                qty = self.stockPositions[currentTckr].qty
                self.sellStock(currentTckr, qty)
        # initialize empty position of new tckrs
        for tckr in newTckrs:
            if tckr not in self.stockPositions.keys():
                self.stockPositions[tckr] = StockPosition(
                    tckr, 0, stockDB=self.stockDB, source=self.source
                )
                self.stockValues[tckr] = 0
        # compile a list of symbols that are now outside of the allowed
        #  composition threshold and need to be adjusted. If a symbol needs
        #  to be bought then add it to a list of symbols to be bought
        buyQty = {}
        absBuyRemainder = {}
        sellQty = {}
        for tckr in newTckrs:
            if tckr in self.stockValues.keys():
                tckrValue = float(self.stockValues[tckr])
                tckrQty = self.stockPositions[tckr].qty
            else:
                tckrValue = float(0.0)
                tckrQty = 0
            tckrPrice = float(self.stockPositions[tckr].getPrice(self.currentDay))
            if abs(tckrValue / totalStockValue - 1) > tradeThresholdPerc:
                qtyDelta = totalStockValue * tckrPercMap[tckr] / tckrPrice - tckrQty
                intDelta = int(round(qtyDelta))
                if intDelta < 0:
                    sellQty[tckr] = abs(intDelta)
                elif intDelta > 0:
                    buyQty[tckr] = abs(intDelta)
                    absBuyRemainder[tckr] = abs(intDelta - qtyDelta)
        # if cash is negative deterimine if selling the stock will get the
        #  pf back to positive cash. If not, increase the amount of stock sold
        if self.cash < 0:
            newCash = 0
            if len(sellQty.keys()) == 0:
                for name in tckrPercMap.keys():
                    sellQty[name] = 0
            for tckr in sellQty.keys():
                newCash += (
                    sellQty[tckr] * self.stockPositions[tckr].getPrice(self.currentDay)
                    - self.brokerageTradeFee
                )
            if self.cash + newCash < 0:
                sellingTckrs = sellQty.keys()
                sellIndex = 0
                maxQtyToSell = {}
                currentPriceToSell = {}
                for tckr in self.stockPositions.keys():
                    maxQtyToSell[tckr] = self.stockPositions[tckr].qty
                    currentPriceToSell[tckr] = self.stockPositions[tckr].getPrice(
                        self.currentDay
                    )
                loopCnt = 0
                while self.cash + newCash < 0:
                    tckr = sellingTckrs[sellIndex]
                    if sellQty[tckr] < maxQtyToSell[tckr]:
                        newQty = min(sellQty[tckr] + 1, maxQtyToSell[tckr])
                        newCash += newQty * currentPriceToSell[tckr]
                        sellQty[tckr] = newQty
                        loopCnt = 0
                    else:
                        loopCnt += 1
                    sellIndex = (sellIndex + 1) % len(sellingTckrs)
                    if sum(sellQty.values()) == sum(
                        maxQtyToSell.values()
                    ) or loopCnt > len(sellQty.keys()):
                        break

        # Start by selling all positions only if there are positions
        #  waiting in the buy list to use the funds
        if len(buyQty.keys()) > 0 or self.cash < 0:
            for tckr in sellQty.keys():
                self.sellStock(tckr, sellQty[tckr])
        # if cash is still negative, sell stock until it is positive. This
        #  is an ineffecient method, but this should only be a last resort
        #  catch all block in case something goes wrong somewhere else.
        if self.cash + sum(self.stockValues.values()) < 0:
            return self.cash - initialCash
        # use the remaining funds to buy stocks starting with the positions
        #  that will be closest to the ideal percentage
        while len(buyQty.keys()) > 0:
            closestTckr = ""
            absRemainder = 1
            for tckr in absBuyRemainder.keys():
                if absBuyRemainder[tckr] < absRemainder:
                    absRemainder = absBuyRemainder[tckr]
                    closestTckr = tckr
            absRemainder = absBuyRemainder.pop(closestTckr, None)
            qty = buyQty.pop(closestTckr, None)
            tckrPrice = self.stockPositions[closestTckr].getPrice(self.currentDay)
            cashRequired = tckrPrice * qty
            # reduce the qty of stock to buy if for some reason there are
            #  not enough funds to make the purchase
            while cashRequired + self.brokerageTradeFee > self.cash and qty != 0:
                qty -= 1
                cashRequired = tckrPrice * qty
            if qty > 0:
                self.buyStock(closestTckr, qty)
        return self.cash - initialCash

    def calculateValue(self):
        return (
            self.cash + sum(self.stockValues.values()) + sum(self.optionValues.values())
        )

    def writeToLog(self, action, obj, qty, cost, comment=""):
        self.log.append([action, obj, qty, cost, comment])
        if not self.silent:
            print("\t" + str([action, obj, qty, cost, comment]))

    def getLog(self):
        return pd.DataFrame(data=self.log, columns=self.logColomns)

    def saveLog(self, fPath=None):
        if not fPath:
            fPath = self.logFPath
        self.getLog().to_csv(fPath)

    def recordValue(self):
        if self.currentDay in self.valueHistory.index:
            self.valueHistory.ix[self.currentDay, "TotalValue"] = self.calculateValue()
            self.valueHistory.ix[self.currentDay, "Cash"] = self.cash
            colNames = self.valueHistory.columns
            # record stock positions
            for tckr in self.stockValues.keys():
                if tckr in colNames:
                    self.valueHistory.loc[self.currentDay, tckr] = self.stockValues[
                        tckr
                    ]
                else:
                    # initialize the column and record the value in the
                    #  new columns
                    self.valueHistory[tckr] = 0
                    self.valueHistory.loc[self.currentDay, tckr] = self.stockValues[
                        tckr
                    ]
            # record options
            for optionName in self.optionValues.keys():
                if optionName in colNames:
                    self.valueHistory.loc[
                        self.currentDay, optionName
                    ] = self.optionValues[optionName]
                else:
                    # initialize the column and record the value in the
                    #  new columns
                    self.valueHistory[optionName] = 0
                    self.valueHistory.loc[
                        self.currentDay, optionName
                    ] = self.optionValues[optionName]
            # recording option positions is not yet supported

    def saveHistoricalPrice(self, fPath=None):
        if not fPath:
            fPath = self.priceHistoryFPath
        self.valueHistory.to_csv(fPath)

    def isDayValid(self, date):
        # valid if it is a weekday (assume minimal impact for not accounting
        #  for holidays)
        return self.currentDay.isoweekday() < 6

    def advanceToDate(self, nextDate):
        if nextDate > self.currentDay:
            # self.printStatus(statusTitle='Before Quick Advance')
            self.recordValue()
            initialCash = self.cash
            # get DatetimeIndex of Days to calculate new values for
            dates = Util.formatDateInput(
                self.valueHistory.index[
                    np.logical_and(
                        self.valueHistory.index > self.currentDay,
                        self.valueHistory.index < nextDate,
                    )
                ]
            )
            if dates.size > 0:
                self.valueHistory.loc[dates, "Cash"] = initialCash
                # for each option, calculate the value for the span of days
                for optionName in self.optionPositions.keys():
                    self.valueHistory.loc[dates, optionName] = self.optionPositions[
                        optionName
                    ].calculateValue(dates)

                for tckr in self.stockPositions.keys():
                    value, cashDiv, newQty = self.stockPositions[tckr].calculateValue(
                        dates, self.reInvestDiv
                    )
                    self.valueHistory.loc[dates, tckr] = value
                    self.valueHistory.loc[dates, "Cash"] = (
                        self.valueHistory.loc[dates, "Cash"] + cashDiv
                    )
                    self.stockPositions[tckr].qty = newQty[-1]
                    self.stockValues[tckr] = value[-1]

                self.valueHistory.loc[dates, "TotalValue"] = self.valueHistory.loc[
                    dates, "Cash":
                ].sum(axis=1)
                self.cash = self.valueHistory.loc[dates.max(), "Cash"]
        self.currentDay = nextDate
        # self.printStatus(statusTitle='After Quick Advance')
        return self.calculateValue()

    def advance1Day(self):
        # record the value of the current day and advance to the next day
        #  on the next day adjust the current value and look for dividends
        self.recordValue()
        self.currentDay = self.currentDay + datetime.timedelta(days=1)
        # print("\t"+str(self.currentDay))
        if self.isDayValid(self.currentDay):
            # recalculate the position values and quantities for the day
            for stock in self.stockPositions.keys():
                stockValue, dividendValue = self.stockPositions[stock].calculateValue(
                    self.currentDay
                )
                # if there are dividends then process them according to
                #  the reInvestDiv boolean
                if dividendValue > 0:
                    if self.reInvestDiv:
                        self.stockPositions[stock].addQty(
                            float(dividendValue)
                            / float(
                                self.stockPositions[stock].getPrice(self.currentDay)
                            )
                        )
                        self.stockValues[self.stockPositions[stock].tckr] = (
                            stockValue + dividendValue
                        )
                    else:
                        self.cash += dividendValue
                        self.stockValues[self.stockPositions[stock].tckr] = stockValue
                else:
                    self.stockValues[self.stockPositions[stock].tckr] = stockValue
            for option in self.optionPosition.keys():
                calculatedValue = 0
                if self.optionPosition[option].qty != 0:
                    calculatedValue = self.optionPosition[option].calculateValue(
                        self.currentDay
                    )
                self.optionValues[self.optionPosition[option].name] = calculatedValue

        return self.calculateValue()

    def printStatus(self, statusTitle="", suffixStr=""):
        print(str(self.currentDay) + ": " + statusTitle)
        print("\tThe account value is $" + str(self.calculateValue()))
        print("\tThe account components are:")
        print("\t\tCash - $%0.2f" % self.cash)
        for tckr in self.stockPositions.keys():
            print(
                "\t\t%s - $%0.2f (%0.2f @ $%0.2f) "
                % (
                    tckr,
                    self.stockValues[tckr],
                    self.stockPositions[tckr].qty,
                    self.stockPositions[tckr].getPrice(self.currentDay),
                )
                + "(position calculated value = $%0.2f)"
                % (self.stockPositions[tckr].calculateValue(self.currentDay)[0])
            )
        print(suffixStr)

    @staticmethod
    def formatValueDataFrame(stockDF):
        for col in stockDF.columns:
            if stockDF[col].dtype == np.object:
                stockDF.ix[stockDF[col] == "null", col] = np.nan
                stockDF[col] = stockDF[col].astype(float)
            elif stockDF[col].dtype != float:
                stockDF[col] = stockDF[col].astype(float)
        # remove empty rows
        stockDF = stockDF.loc[np.isfinite(stockDF.TotalValue), :]
        return stockDF.fillna(method="ffill")

    def report(self, showCharts=True):
        pfValues = self.valueHistory.TotalValue
        pfValues.name = self.pfName
        self.pfStatObj = ffn.PerformanceStats(pfValues, rf=0.0)
        self.pfStatObj.display()
        # plot the total return and the value of each component
        if showCharts:
            plotCols = self.valueHistory.columns.tolist()
            for removeCol in np.intersect1d(self.removeColsFromPlots, plotCols):
                plotCols.remove(removeCol)
            self.valueHistory[plotCols].plot(
                logy=False,
                figsize=(15, 5),
                title=self.pfName + ": TotalValue and Component Values",
            )
            plt.tight_layout()
            self.valuesPlotFig = plt.gcf()
            self.valuesPlotFig.savefig(
                os.path.join(self.plotDir, "StrategyAndComponentValues.png")
            )
            # plot the individual performance of each component compared
            #  to the total value of the portfolio
            plt.figure()
            self.stockDB.tempDB["AdjClose"]["Strategy"] = self.valueHistory[
                "TotalValue"
            ]
            plotCols = self.stockDB.tempDB["AdjClose"].columns.tolist()
            for removeCol in np.intersect1d(self.removeColsFromPlots, plotCols):
                plotCols.remove(removeCol)
            self.stockDB.tempDB["AdjClose"][plotCols].rebase().plot(
                logy=True,
                figsize=(15, 5),
                title=self.pfName + ": TotalValue and Source Performance",
            )
            plt.tight_layout()
            self.sourcesPlotFig = plt.gcf()
            self.sourcesPlotFig.savefig(
                os.path.join(self.plotDir, "SourcePerformance.png")
            )
        return self.pfStatObj

    def finish(self, showCharts=True):
        self.recordValue()
        self.valueHistory = self.formatValueDataFrame(self.valueHistory)
        self.saveHistoricalPrice()
        self.saveLog()
        self.report(showCharts=showCharts)


def main():
    pass


#    # from MarketAnalysis.MarketPortfolio import StockPosition, Call, Put
#    reload(MarketData)
#    reload(MarketPortfolio)
#    oDB = MarketData.OptionData(workProxy=False)
#    sDB = MarketData.StockData(workProxy=False)
#    startDate = datetime.datetime(2010, 1, 1)
#    targetExDate = datetime.datetime(2012, 12, 15)
#    startDatePrice = sDB.getValidRows('ixSPX', startDate).Close
#    nextDate = startDate + datetime.timedelta(days=180)
#    price, exDate, strike = oDB.createOption_BS('SPX', startDate,
#                                                targetExDate,
#                                                startDatePrice*0.8, 'Call',
#                                                currentPrice=startDatePrice)
#    exDate = Util.formatDateInput(exDate)
#    bondPortion = 1.0
#    stockPercentages = tckrPercMap = {'AGG': bondPortion * 0.4,
#                                      'HYG': bondPortion * 0.4,
#                                      'SPY': bondPortion * 0.2}
#    tradeThresholdPerc = 0.02
#    self = pf = Portfolio(loadFromHist=False, fPathHistory=None,
#                          initialValue=100000, reInvestDiv=True,
#                          pfName="TestQuickAdvance", startDate=startDate,
#                          fileDir=r'C:\Users\cp035982\Python\Projects\MarketAnalysis', optionDB=oDB,
#                          stockDB=sDB, brokerageTradeFee=0,
#                          additionalOptionContractFee=0, silent=False)
#
#    pf.buyOption('SPXLEAP', 1, underlyingTckr='SPX', strike=strike,
#                 exDate=exDate, optionType='Call')
#    pf.rebalance(tckrPercMap, tradeThresholdPerc)
#    pf.advanceToDate(nextDate)
#    pf.sellOption('SPXLEAP', 1)
#    pf.exciseOption('SPXLEAP', 1)


if __name__ == "__main__":
    main()
