import pandas as pd
import numpy as np
from scipy import stats  # , linspace, polyval, polyfit, sqrt, randn
from src.DataSource import StockData
import src.Utility as util

"""
from MarketAnalysis import MarketPredictions
reload(Prediction)
reload(StockData)
tckr = '.SPX'
self = mp = MarketPredictions.MarketPredictions('tckr',workProxy = True)
data = self.stockDB.loadFromDB(tckr)
"""


def getDefaultPredictionModel(
    fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
    workProxy=True,
    stockDB=None,
):
    return SMADeltaModel(fileDir=fileDir, workProxy=workProxy, stockDB=stockDB)


class BaseModel(object):
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        stockDB=None,
    ):
        self.workProxy = None
        if stockDB is None:
            self.stockDB = StockData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.stockDB = stockDB
        tckr = currentDate = predictedDate = None
        self.distributionInfo = (tckr, currentDate, predictedDate)
        self.distribution = None
        self.meanDB = None
        self.volatilityDB = None
        self.volatilityDB_unit = None

    def getCurrentPrice(self, currentDate, tckr, source="storage"):
        return self.stockDB.getPrice(tckr, currentDate, adjusted=False, source=source)

    def populate_meanDB(
        self, date_index, tckr=".SPX", days_out=None, tckr_source="storage"
    ):
        """
        return a prediction dataframe for each date in the index with a column
        for each interval with the column name formatted as '%1i-Day'
        """
        if days_out is None:
            days_out = [7, 14, 30, 60, 180, 365, 730, 1095, 1825]
        date_index = util.formatDateArray(date_index)
        time_delta = pd.TimedeltaIndex(days_out, unit="days").sort_values()
        self.meanDB = self.predictMean(
            date_index,
            time_delta=time_delta,
            tckr=tckr,
            model_source="storage",
            tckr_source=tckr_source,
        )
        return self.meanDB

    def populate_volatilityDB(
        self,
        date_index,
        tckr=".SPX",
        days_out=None,
        unit="Percent",
        tckr_source="storage",
    ):
        if days_out is None:
            days_out = [7, 14, 30, 60, 180, 365, 730, 1095, 1825]
        date_index = util.formatDateArray(date_index)
        time_delta = pd.TimedeltaIndex(days_out, unit="days").sort_values()

        self.volatilityDB = self.predictVolatility(
            date_index,
            time_delta=time_delta,
            tckr=tckr,
            unit=unit,
            model_source="storage",
            tckr_source=tckr_source,
        )
        self.volatilityDB_unit = unit
        return self.volatilityDB

    def predictMean(
        self,
        dates,
        predictedDate=None,
        time_delta=None,
        tckr="ixSPX",
        model_source="storage",
        tckr_source="storage",
    ):
        if dates is pd.NaT:
            return np.nan
        else:
            dates = util.formatDateInput(dates)
        if predictedDate is not None:
            predictedDate = util.formatDateInput(predictedDate)

        assert predictedDate is not None or time_delta is not None, (
            "Not Enough Data Error in MarketPrediction function "
            + "predict_mean(): 'predicted_date' and 'days_out' are"
            + "both None"
        )
        if time_delta is None:
            time_delta = predictedDate - dates
        elif type(time_delta) in [int, float]:
            time_delta = pd.TimedeltaIndex([time_delta], unit="days")
        elif type(time_delta) in [list, set, np.ndarray, pd.Series]:
            time_delta = pd.TimedeltaIndex(time_delta, unit="days")

        if (
            model_source.upper() in ["TEMPDB", "MEANDB", "AVGDB", "AVERAGEDB"]
            and self.meanDB is not None
        ):
            data = self.predictMean_TempDB(
                dates, time_delta=time_delta, tckr=tckr, tckr_source=tckr_source
            )
        else:
            data = self.predictMean_Storage(
                dates, time_delta=time_delta, tckr=tckr, tckr_source=tckr_source
            )

        return data

    def predictMean_Storage(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """
        Defaults to the current value. If there is more than one
        timedelta, then it will return list of pd.Series (one for each
        time_delta)

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        mean = self.stockDB.getPrice(tckr, dates, adjusted=False, source=tckr_source)
        if type(time_delta) == pd.TimedeltaIndex:
            output = [pd.Series(data=mean, name=td, dtype=float) for td in time_delta]
            mean = pd.concat(output, axis=1)
            if mean.index.size == 1:
                mean = mean.loc[mean.index[0], :]
            else:
                mean = mean[mean.columns.sort_values()]
        else:
            if type(mean) == pd.Series:
                mean.name = time_delta
        return mean

    def predictMean_TempDB(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """Defaults to the current value. If there is more than one
        timedelta, then it will return list of pd.Series (one for each
        time_delta)

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        dates = util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the date_time index for predictions
        dates = pd.DatetimeIndex(dates.date)
        if type(time_delta) in [pd.Timedelta, long, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        prediction_mean = [
            self.interp_from_tempDB(dates, td, self.meanDB) for td in time_delta
        ]
        prediction_mean = pd.concat(prediction_mean, axis=1)
        prediction_mean = prediction_mean[prediction_mean.columns.sort_values()]
        final_cols = time_delta[0] if single_td else time_delta
        final_rows = dates[0] if single_date else dates
        return prediction_mean.loc[final_rows, final_cols]

    @staticmethod
    def interp_from_tempDB(dates, time_delta, tempDB):
        db_cols = tempDB.columns
        if time_delta in db_cols:
            output = tempDB.loc[dates, time_delta]
        elif time_delta < db_cols.min():
            output = tempDB.loc[dates, db_cols.min()]
        else:
            if time_delta < db_cols.max():
                low_col = db_cols[db_cols < time_delta].max()
                high_col = db_cols[db_cols > time_delta].min()
            else:
                low_col = db_cols[db_cols < db_cols.max()].max()
                high_col = db_cols.max()
            slope = (tempDB.loc[dates, high_col] - tempDB.loc[dates, low_col]) / float(
                (high_col - low_col).days
            )
            output = tempDB.loc[dates, low_col] + int(time_delta.days) * slope
            output.name = time_delta
        return output

    def predictVolatility(
        self,
        dates,
        predictedDate=None,
        time_delta=None,
        tckr="ixSPX",
        unit="Percent",
        model_source="storage",
        tckr_source="storage",
    ):
        """
        defaults to the 30d annual volatilty adjusted to represent
        the expected variation around the mean at the preditedDate

        args:
            dates -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if dates is a timestamp and time_delta is
                    a Timedelta
                series if dates is a DatetimeIndex object and
                    time_delta is a Timdedelta object or dates is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both dates and time_delta are structured
                    as indexes
        """
        if dates is pd.NaT:
            return np.nan
        else:
            dates = util.formatDateInput(dates)
        if predictedDate is not None:
            predictedDate = util.formatDateInput(predictedDate)

        assert predictedDate is not None or time_delta is not None, (
            "Not Enough Data Error in MarketPrediction function "
            + "predict_mean(): 'predicted_date' and 'days_out' are"
            + "both None"
        )
        if time_delta is None:
            time_delta = predictedDate - dates
        elif type(time_delta) in [int, float]:
            time_delta = pd.TimedeltaIndex([time_delta], unit="days")
        elif type(time_delta) in [list, set, np.ndarray, pd.Series]:
            time_delta = pd.TimedeltaIndex(time_delta, unit="days")
        else:
            time_delta = time_delta

        if (
            model_source.upper() in ["TEMPDB", "MEANDB", "AVGDB", "AVERAGEDB"]
            and self.meanDB is not None
        ):
            data = self.predictVolatility_TempDB(
                dates,
                time_delta=time_delta,
                unit=unit,
                tckr=tckr,
                tckr_source=tckr_source,
            )
        else:
            data = self.predictVolatility_Storage(
                dates,
                time_delta=time_delta,
                unit=unit,
                tckr=tckr,
                tckr_source=tckr_source,
            )

        return data

    def predictVolatility_Storage(
        self,
        currentDate,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="PERCENT",
        tckr_source="storage",
    ):
        """
        defaults to the 30d annual volatilty adjusted to represent
        the expected variation around the mean at the preditedDate

        args:
            current_date -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if current_date is a DatetimeIndex object and
                    time_delta is a Timdedelta object or current_date is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both current_date and time_delta are structured
                    as indexes
        """
        predicted_mean = self.predictMean_Storage(
            currentDate,
            time_delta=time_delta,
            tckr=tckr,
            model_source="storage",
            tckr_source=tckr_source,
        )
        current_price = self.stockDB.getPrice(
            tckr, currentDate, adjusted=False, source=tckr_source
        )
        volatility = self.annual_volatility(
            tckr, currentDate, time_delta, source=tckr_source
        )
        volatility = volatility * np.sqrt(time_delta / pd.Timedelta(days=365))
        if unit.upper() == "PERCENT":
            multiplier = predicted_mean / current_price
        else:
            multiplier = volatility * (predicted_mean / current_price) * predicted_mean
        return volatility * multiplier

    def predictVolatility_TempDB(
        self,
        currentDate,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="Percent",
        tckr_source="storage",
    ):
        """
        Interpolates from the current temp_db

        args:
            current_date -   a pd.Timestamp or pd.DatetimeIndex of dates from
                            which to launch the prediction
            time_delta -    a pd.TimedeltaIndex object of intervals to predict
            tckr -          the tckr of the underlying stock
            tckr_source -   passed to the stock database object to indicate
                            which source from which to pull the data

        returns:
            the predicted float as a:
                float if current_date is a timestamp and time_delta is
                    a Timedelta
                series if current_date is a DatetimeIndex object and
                    time_delta is a Timdedelta object or current_date is
                    a timestamp object and time_delta is a TimedeltaIndex
                dataframe is both current_date and time_delta are structured
                    as indexes
        """
        if type(time_delta) in [pd.Timedelta, long, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        pred_volatility = [
            self.interp_from_tempDB(currentDate, td, self.volatilityDB)
            for td in time_delta
        ]
        if single_td:
            pred_volatility = pred_volatility[0]
        else:
            pred_volatility = pd.concat(pred_volatility, axis=1)
            pred_volatility = pred_volatility[pred_volatility.columns.sort_values()]
        if unit == self.volatilityDB_unit:
            return pred_volatility
        elif unit.upper() == "PERCENT":
            future_value = self.predictMean(
                currentDate,
                time_delta=time_delta,
                model_source="temp_db",
                tckr_source=tckr_source,
            )
            return future_value * pred_volatility
        else:
            future_value = self.predictMean(
                currentDate,
                time_delta=time_delta,
                model_source="temp_db",
                tckr_source=tckr_source,
            )
            return pred_volatility / future_value

    def setModelInfo(self, currentDate, predictedDate, tckr):
        infoChanged = False
        if tckr is None:
            tckr = self.distributionInfo[0]
        if currentDate is None:
            currentDate = self.distributionInfo[1]
        if predictedDate is None:
            predictedDate = self.distributionInfo[2]
        if (
            self.distributionInfo != (tckr, currentDate, predictedDate)
            or self.distribution is None
        ):
            assert (
                currentDate is not None
                and predictedDate is not None
                and tckr is not None
            ), (
                "Not enough information has been provided. "
                + "The probability distribution has not been "
                + "initilized yet and it needs valid values for "
                + "'current_date', 'predicted_date', and 'tckr'... "
                + "current_date = %s, predicted_date = %s, tckr = %s"
                % (str(currentDate), str(predictedDate), str(tckr))
            )
            infoChanged = True
        return infoChanged

    def getProbDist(
        self, currentDate, predictedDate, tckr, model_source="storage", source="storage"
    ):
        tckr = self.distributionInfo[0] if tckr is None else tckr
        if currentDate is None:
            currentDate = self.distributionInfo[1]
        if predictedDate is None:
            predictedDate = self.distributionInfo[2]
        if (
            self.distributionInfo != (tckr, currentDate, predictedDate)
            or self.distribution is None
        ):
            assert (
                currentDate is not None
                and predictedDate is not None
                and tckr is not None
            ), (
                "Not enough information has been provided. The "
                + "probability distribution has not been "
                + "initilized yet and it needs valid values for "
                + "'current_date', 'predicted_date', and 'tckr'... "
                + "current_date = %s, predicted_date = %s, tckr = %s"
                % (str(currentDate), str(predictedDate), str(tckr))
            )
            self.distributionInfo = (tckr, currentDate, predictedDate)
            prediction_mean = self.predictMean(
                currentDate,
                predictedDate,
                tckr=tckr,
                model_source=model_source,
                tckr_source=source,
            )
            prediction_stdev = self.predictVolatility(
                currentDate,
                predictedDate,
                tckr=tckr,
                model_source=model_source,
                tckr_source=source,
            )
            self.distribution = stats.norm(loc=prediction_mean, scale=prediction_stdev)
        return self.distribution

    def getCumProbFromPrice(
        self,
        price,
        currentDate=None,
        predictedDate=None,
        tckr=None,
        direction="Below",
        source="storage",
    ):
        """
        returns the predicted probability that the price will be above or
        below the price targets provided in the variable 'prices'. The
        variable 'direction' determins if the probability is the probability
        of the future price being above the target price or below the
        target price.
        """
        dist = self.getProbDist(currentDate, predictedDate, tckr)
        probability = dist.cdf(price)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        return probability

    def getPriceFromCumProb(
        self,
        probability,
        currentDate=None,
        predictedDate=None,
        tckr=None,
        direction="Below",
        source="storage",
    ):
        """
        The percent point function is the inverse of the cdf function and
        so this is the inverse of the 'getProbFromPrices function
          o  direction indicates whether the probabilities reflect the
             probability of prices being above or below the returned price
        """
        dist = self.getProbDist(currentDate, predictedDate, tckr)
        if type(probability) == list:
            probability = np.array(probability)
        if direction.upper() == "ABOVE":
            probability = 1 - probability
        price = dist.ppf(probability)
        return price

    def getProbOfPrice(
        self, price, currentDate=None, predictedDate=None, tckr=None, source="storage"
    ):
        return self.getProbDist(currentDate, predictedDate, tckr).pdf(price)

    def calculateTrend_BySpan(
        self, tckr, date, trailingDays, colName="Close", source="storage"
    ):
        date = util.formatDateInput(date)
        if type(trailingDays) == str:
            trailingDays = util.interpretSpan(trailingDays, unit="d")
        if type(date) == pd.DatetimeIndex:
            startDate = min(date) - pd.Timedelta(days=trailingDays)
            endDate = max(date) + pd.Timedelta(days=1)
            tckrPrices = tckrPrices = self.stockDB.loadFromDB(
                dbKey=tckr,
                start=startDate,
                end=endDate,
                columns=[colName],
                source=source,
            )
        else:
            startDate = date - pd.Timedelta(days=trailingDays)
            endDate = date + pd.Timedelta(days=1)
            tckrPrices = tckrPrices = self.stockDB.loadFromDB(
                dbKey=tckr,
                start=startDate,
                end=endDate,
                columns=[colName],
                source=source,
            )
        #            if(date not in tckrPrices.index):
        #                return np.nan,np.nan
        #        start =time.time()
        firstDate = tckrPrices.index.min()
        tckrPrices["DeltaTime"] = (tckrPrices.index - firstDate).days
        tckrPrices["XY"] = tckrPrices[colName] * tckrPrices["DeltaTime"]
        tckrPrices["X2"] = tckrPrices["DeltaTime"] ** 2
        tckrPrices["Y2"] = tckrPrices[colName] ** 2
        r = tckrPrices.rolling("%01id" % trailingDays)
        sumData = r[["DeltaTime", colName, "XY", "X2", "Y2"]].aggregate(np.sum)
        num = r[colName].count()
        # intercept = ((sumY*sumX2) - (sumX*SumXY))/((num*sumX2)-(SumX**2))
        intercept = (
            sumData[colName] * sumData["X2"] - (sumData["DeltaTime"] * sumData["XY"])
        ) / ((num * sumData["X2"]) - (sumData["DeltaTime"] ** 2))
        # slope = (num*sumXY - sumX*sumY)/((num*sumX2)-(SumX**2))
        slope = (num * sumData["XY"] - sumData["DeltaTime"] * sumData[colName]) / (
            (num * sumData["X2"]) - (sumData["DeltaTime"] ** 2)
        )
        slope.name = "Slope"
        # trend = intercept + slope*data['DeltaTime']
        # now for R2 = 1-SStot/SSres
        Yavg = r[colName].mean()
        # r2 = pd.Series(data = [0]*len(data.index),index=data.index,
        #  dtype=float,name='R2')
        SStot = sumData["Y2"] - 2 * Yavg * sumData[colName] + num * (Yavg ** 2)
        SSres = (
            sumData["Y2"]
            - 2 * intercept * sumData[colName]
            - 2 * slope * sumData["XY"]
            + (intercept ** 2) * num
            + 2 * intercept * slope * sumData["DeltaTime"]
            + (slope ** 2) * sumData["X2"]
        )
        r2values = 1 - SSres / SStot
        r2 = pd.Series(data=r2values, index=tckrPrices.index, dtype=float, name="R2")
        # now ensure that the output has the correct format
        if type(date) == pd.DatetimeIndex:
            output = pd.concat([slope, r2, pd.Series(index=date)], axis=1)[
                ["Slope", "R2"]
            ].sort_index(inplace=False)
        else:
            output = pd.concat([slope, r2, pd.Series(index=[date])], axis=1)[
                ["Slope", "R2"]
            ].sort_index(inplace=False)
        output = (
            output.fillna(method="ffill")
            .fillna(method="bfill")
            .loc[date, ["Slope", "R2"]]
        )
        output = output.Slope, output.R2
        return output

    def annual_volatility(self, tckr, dates, trailing_timedelta, source="strorage"):
        # dailyChange = data['Close'].pct_change()
        # dailyVolatility = dailyChange.rolling('%01id'%trailing_days).std()
        # annualVolatility = np.sqrt(252)*dailyVolatility
        dates = util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the DatetimeIndex for predictions
        dates = pd.DatetimeIndex(dates.date)

        if type(trailing_timedelta) == pd.Timedelta:
            single_td = True
            trailing_timedelta = pd.TimedeltaIndex([trailing_timedelta]).sort_values()
        else:
            single_td = False
        startDate = min(dates) - trailing_timedelta
        endDate = max(dates) + pd.Timedelta(days=1)
        dateRange = pd.bdate_range(start=startDate.min(), end=endDate)
        tckr_change = self.stockDB.getPrice(tckr, dateRange, source=source).pct_change()
        output = [
            pd.Series(name=td, data=tckr_change.rolling("%1id" % td.days).std())
            for td in trailing_timedelta
        ]
        output = pd.concat(output, axis=1) * np.sqrt(252)

        final_rows = np.array(dates, dtype="datetime64[ns]")
        while True:
            valid_rows = np.isin(final_rows, output.index)
            if valid_rows.all():
                final_rows = pd.DatetimeIndex(final_rows)
                break
            final_rows[~valid_rows] = final_rows[~valid_rows] - pd.Timedelta(days=1)

        final_cols = trailing_timedelta[0] if single_td else trailing_timedelta
        final_rows = final_rows[0] if single_date else final_rows

        return output.loc[final_rows, final_cols]

    def approximateVIX_Volatility(self, date, predictionDate, source="storage"):
        """ Sets the volatility based on the 1 month and 3 month VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month VIX
                volatilities"""
        date = util.formatDateInput(date)
        predictionDate = util.formatDateInput(predictionDate)
        if type(date) == pd.DatetimeIndex:
            volatilityData = pd.DataFrame(
                index=date, columns=["ixVIX", "ixVXV", "AdjustedVixVolatility"]
            )
        else:
            volatilityData = pd.DataFrame(
                index=[date],
                columns=[
                    "PredictionTimedelta",
                    "ixVIX",
                    "ixVXV",
                    "AdjustedVixVolatility",
                ],
            )

        volatilityData.loc[:, "PredictionTimedelta"] = predictionDate - date
        # load the correct data
        if (volatilityData.PredictionTimedelta > pd.Timedelta(days=30)).any():
            volatilityData.loc[date, "ixVXV"] = self.stockDB.getPrice(
                "ixVXV", date, adjusted=False, source=source
            )
            if (volatilityData.PredictionTimedelta < pd.Timedelta(days=90)).any():
                volatilityData.loc[date, "ixVIX"] = self.stockDB.getPrice(
                    "ixVIX", date, adjusted=False, source=source
                )
                # Assign dates less than or equal to 30 days or have null ixVXV
                selectBool = np.logical_and(
                    volatilityData.PredictionTimedelta <= pd.Timedelta(days=30),
                    pd.isna(volatilityData.ixVXV),
                )
                if selectBool.any():
                    volatilityData.loc[
                        selectBool, "AdjustedVixVolatility"
                    ] = volatilityData.loc[selectBool, "ixVIX"]
                # Assign all dates greater than or equal to 90 days
                selectBool = volatilityData.PredictionTimedelta <= pd.Timedelta(days=30)
                if selectBool.any():
                    volatilityData.loc[
                        selectBool, "AdjustedVixVolatility"
                    ] = volatilityData.loc[selectBool, "ixVXV"]
                # assign all remaing values the weighted average of the 1m
                #  and 3m VIX volatilities
                selectBool = pd.isna(volatilityData.AdjustedVixVolatility)
                if selectBool.any():
                    calculatedValue = volatilityData.ixVIX[selectBool] / 60.0 * (
                        90.0 - volatilityData.PredictionTimedelta[selectBool].days
                    ) + (
                        volatilityData.ixVXV[selectBool]
                        / 60.0
                        * (volatilityData.PredictionTimedelta.days[selectBool] - 30)
                    )
                    volatilityData.loc[
                        selectBool, "AdjustedVixVolatility"
                    ] = calculatedValue
            else:
                # nothing is below 90 so assign VXV to everything and
                #  correct for if there are any missing values
                volatilityData.loc[
                    date, "AdjustedVixVolatility"
                ] = self.stockDB.getPrice("ixVXV", date, adjusted=False, source=source)
                selectBool = pd.isna(volatilityData.AdjustedVixVolatility)
                if selectBool.any():
                    missingDates = volatilityData.index[selectBool]
                    volatilityData.loc[
                        missingDates, "AdjustedVixVolatility"
                    ] = self.stockDB.getPrice(
                        "ixVIX", missingDates, adjusted=False, source=source
                    )
        else:
            # nothing is above 30 days so assign VIX to everything
            volatilityData.loc[date, "AdjustedVixVolatility"] = self.stockDB.getPrice(
                "ixVIX", date, adjusted=False, source=source
            )

        return volatilityData.AdjustedVixVolatility[date]

    def expected_profit_of_stock(
        self, tckr, currentDate, predictedDate, source="storage"
    ):
        """
        expected profit = expected price - current price
                        = mean of dist - current price
        """
        expected_price = self.getProbDist(
            currentDate, predictedDate, tckr, source=source
        ).mean()
        current_price = self.getCurrentPrice(currentDate, tckr, source=source)
        return expected_price - current_price

    def expected_return_of_stock(
        self, tckr, currentDate, predictedDate, source="storage"
    ):
        """
        return = profit / amount risked = profit / price
        """
        expected_price = self.getProbDist(
            currentDate, predictedDate, tckr, source=source
        ).mean()
        current_price = self.getCurrentPrice(currentDate, tckr, source=source)
        return (expected_price / current_price) - 1


class SMADeltaModel(BaseModel):
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        stockDB=None,
    ):
        BaseModel.__init__(self, fileDir=fileDir, workProxy=workProxy, stockDB=stockDB)

    def predictMean_Storage(
        self, dates, time_delta=None, tckr="ixSPX", tckr_source="storage"
    ):
        """
        if 2 weeks or more, it will be a scale factor time the SMA for a
        period equal to the predictiono range minus the SMA for a period equal
        to half of the prediction range
            o  1 week - 0x
            o  2 weeks - 0.75x
            o  3 weeks - 1x
            o  1m - 1.5x
            o  2m+ - 2.0x
        """
        # start with formating
        dates = util.formatDateInput(dates)
        if type(dates) == pd.DatetimeIndex:
            single_date = False
        else:
            single_date = True
            dates = pd.DatetimeIndex([dates])
        # remove the time component of the DatetimeIndex for predictions
        dates = pd.DatetimeIndex(dates.date)
        if type(time_delta) in [pd.Timedelta, long, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        days = [7, 14, 21, 30, 60]
        factors = [0, 0.75, 1.25, 1.6, 2]
        multi_factor = np.interp(time_delta.days, days, factors)

        if multi_factor.size == 1 and multi_factor[0] == 0.0:
            if single_date:
                return self.stockDB.getPrice(
                    tckr, dates[0], adjusted=False, source=tckr_source
                )
            else:
                return self.stockDB.getPrice(
                    tckr, dates, adjusted=False, source=tckr_source
                )
        else:
            start_date = dates.min() - time_delta.max()
            end = dates.max() + pd.Timedelta(days=1)
            tckr_data = pd.bdate_range(start=start_date, end=end)
            tckr_data = self.stockDB.getPrice(tckr, tckr_data, source=tckr_source)
            start_price = tckr_data[dates]
            # sma_diff = sma_short - sma_long
            sma_valid = np.logical_or(
                time_delta >= pd.Timedelta(days=2), multi_factor != 0
            )
            if sma_valid.any():
                sma_diff = [
                    (
                        tckr_data.rolling("%1id" % (d / 2)).mean()
                        - tckr_data.rolling("%1id" % (d)).mean()
                    )[dates]
                    for d in time_delta[sma_valid].days
                ]
                mean = [
                    pd.Series(
                        name=time_delta[sma_valid][i],
                        data=(sma_diff[i] * multi_factor[sma_valid][i] + start_price),
                    )
                    for i in range(sma_valid.sum())
                ]
            if (~sma_valid).any():
                mean += [
                    pd.Series(start_price, name=td) for td in time_delta[~sma_valid]
                ]
            mean = pd.concat(mean, axis=1)
            mean = mean[mean.columns.sort_values()]
        # account for mult. vs single time_delta formatting requirements
        final_cols = time_delta[0] if single_td else time_delta
        final_rows = dates[0] if single_date else dates
        return mean.loc[final_rows, final_cols]

    def predictVolatility_Storage(
        self,
        dates,
        time_delta=pd.Timedelta(30),
        tckr="ixSPX",
        unit="PERCENT",
        tckr_source="storage",
    ):
        """
        The volatility that best matches the variation of the price around the
        predicted mean from the SMA differences is the annual volatility
        calculated from a span of days 1/2 the prediction span with a minimum
        span of 30 days and adjusted for the variation expected in the
        prediction span
            variation % = annual_volatility(priceData,
                                            span=max(30, days_out/2))
                          * np.sqrt(days_out/365.)
        args:
            dates   - The hypothetical date that the prediction is made
                            no information past this date is considered
            predicted_date - The future date for which to predict the
                            volatility
            tckr          - The underlying asset to predict
            span          - Not Used - The span (in days) of historical prices
                            used to estimate the annual volatility. The SMA
                            instance of the predictionModel class overrides
                            the span parameter and recalculates it off of the
                            number of days between predicted_date and
                            dates
            unit          - Indicates the type of return expected. 'Percent'
                            is the percentage of the expected mean that the
                            model expects the price to fluctuate
                            (predicted stdev / predicted mean). 'Dollar' is
                            expected stdev of the price around the new mean
                            in dollars.
        """
        if type(time_delta) in [pd.Timedelta, long, int, float]:
            single_td = True
            time_delta = pd.TimedeltaIndex([time_delta], unit="days").sort_values()
        elif type(time_delta) == pd.TimedeltaIndex:
            single_td = False
            time_delta = time_delta.sort_values()
        else:
            single_td = False
            time_delta = pd.TimedeltaIndex(time_delta, unit="days").sort_values()
        days_out = np.array(time_delta.days, dtype=int)
        time_delta = pd.TimedeltaIndex(np.clip(days_out, 30, 5000), unit="days")
        time_delta = time_delta[0] if single_td else time_delta
        predicted_mean = self.predictMean(
            dates,
            time_delta=time_delta,
            tckr=tckr,
            model_source="storage",
            tckr_source=tckr_source,
        )
        volatility = self.annual_volatility(tckr, dates, time_delta, source=tckr_source)
        volatility = volatility * np.sqrt(time_delta.days / 365.0)
        if unit.upper() == "PERCENT":
            multiplier = 1
        else:
            multiplier = predicted_mean
        return volatility * multiplier


class TrendScaledR2(BaseModel):
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        stockDB=None,
        maxTrailingDays_Vol=18,
        quadCoef_Vol=None,
        trendSpan_Mean=None,
    ):
        BaseModel.__init__(self, fileDir=fileDir, workProxy=workProxy, stockDB=stockDB)
        if quadCoef_Vol is None:
            quadCoef_Vol = [76.506, -1.264, 0.0068]
        if trendSpan_Mean is None:
            trendSpan_Mean = [180, 270, 365]
        self.maxTrailingDays_vol = maxTrailingDays_Vol
        self.quadCoef_Vol = quadCoef_Vol

    #    def PredictMeanColumn_BestRsqTrendScaledByRsq(self,data,daysOut):
    def predictMean(
        self, currentDate, predictedDate, tckr, source="storage", returnExtra=False
    ):
        """
        TODO :  Neeed to update to run with the rest of the inheritance
        Calculates a trend line for several trendSpans and uses the line
        with the best fit. The returned slope is scaled by the R2 value
        """
        currentDate = util.formatDateInput(currentDate)
        predictedDate = util.formatDateInput(predictedDate)
        trendSpans = [180, 270, 365]
        if type(currentDate) == pd.DatetimeIndex:
            slopes = pd.DataFrame(index=currentDate, columns=trendSpans)
        else:
            slopes = pd.DataFrame(index=[currentDate], columns=trendSpans)
        if type(currentDate) == pd.DatetimeIndex:
            r2DF = pd.DataFrame(index=currentDate, columns=trendSpans)
        else:
            r2DF = pd.DataFrame(index=[currentDate], columns=trendSpans)
        for span in trendSpans:
            slopes.loc[currentDate, span], r2DF.loc[
                currentDate, span
            ] = self.calculateTrend_BySpan(
                tckr, currentDate, span, colName="Close", source=source
            )
        # slopeDF = pd.DataFrame(slopes)
        output = pd.DataFrame(
            index=r2DF.index,
            columns=["PredictedMean", "BestSlope", "BestR2", "TrendSpan"],
        )
        output.loc[:, "BestR2"] = r2DF.max(axis=1)
        # now ensure that only one column per row has a True value
        #        output.loc[:, 'BestSlope'] = pd.Series(index=r2DF.index,
        #                                               name = 'BestSlope')
        for span in trendSpans:
            unassignedRows = pd.isna(output.PredictedMean)
            boolSelect = np.logical_and(unassignedRows, r2DF[span] == output.BestR2)
            output.loc[boolSelect, "BestSlope"] = slopes[span][boolSelect]
            output.loc[boolSelect, "TrendSpan"] = span
        daysOut = (predictedDate - currentDate).days
        currentPrice = self.stockDB.getPrice(
            tckr, currentDate, adjusted=False, source="storage"
        )
        output.loc[:, "PredictedMean"] = currentPrice + (
            output.BestSlope * daysOut * output.BestR2
        )
        if returnExtra:
            output = output.loc[currentDate, :]
            return (
                output.PredictedMean,
                output.BestSlope,
                output.BestR2,
                output.TrendSpan,
            )
        else:
            return output.loc[currentDate, "PredictedMean"]

    def predictVolatility(
        self, currentDate, predictedDate, tckr, unit="Percent", source="storage"
    ):
        """
        observed pattern of best fits following the trend:
            Volatility Calculated Span =
                            0.0068(daysOut)^2 - 1.264(daysOut) + 76.506
        This was observed when comparing various Rsq trend predictors
            paired with several volatility spans original work can be found at:
        """
        daysOut = np.mean(np.clip((predictedDate - currentDate).days, 0, 90))
        trailingDays = (
            self.quadCoef_Vol[0]
            + self.quadCoef_Vol[1] * daysOut
            + self.quadCoef_Vol[2] * daysOut ** 2
        )
        trailingDays = pd.Timedelta(days=np.round(trailingDays))
        predictedMean = self.predictMean(
            currentDate, predictedDate, tckr, source=source
        )
        currentPrice = self.stockDB.getPrice(
            tckr, currentDate, adjusted=False, source=source
        )
        volatility = self.annual_volatility(
            tckr, currentDate, trailingDays, source=source
        )
        if unit.upper() == "PERCENT":
            multiplier = predictedMean / currentPrice
        else:
            multiplier = volatility * (predictedMean / currentPrice) * predictedMean
        return np.abs(volatility * multiplier)


def main():
    pass


if __name__ == "__main__":
    main()
