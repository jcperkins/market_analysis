# -*- coding: utf-8 -*-
import sys
import os

print(__file__)
root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if root_dir not in sys.path:
    print("Adding project root directory to python path")
    sys.path.insert(0, root_dir)

# noinspection PyPep8
import config
