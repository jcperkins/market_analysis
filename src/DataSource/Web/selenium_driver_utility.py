from typing import Optional

from config import default_settings
from selenium import webdriver


# noinspection PyUnresolvedReferences
def get_driver(
    driver_path: Optional[str] = None,
    options: Optional[webdriver.chrome.options.Options] = None,
    debug: bool = False,
) -> webdriver:
    if driver_path is None:
        if debug:
            return webdriver.Chrome(
                executable_path=default_settings.chromedriver, options=options
            )
        else:
            return webdriver.Chrome(
                executable_path=default_settings.chromedriver, options=options
            )
            # self.driver = driver = webdriver.PhantomJS(
            #         executable_path=self.phantomJSDriver,
            #         service_log_path=os.path.devnull)
            # driver.set_window_size(1120, 550)
    else:
        return webdriver.Chrome(executable_path=driver_path, options=options)


def close_driver(driver: webdriver) -> webdriver:
    driver.close()
    return driver
