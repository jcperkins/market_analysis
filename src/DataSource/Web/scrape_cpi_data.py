from typing import Optional

import numpy as np
import pandas as pd
import requests
from lxml import html
from selenium import webdriver

from .selenium_driver_utility import get_driver

inflation_url = (
    "https://inflationdata.com/Inflation/Consumer_Price_Index/HistoricalCPI.aspx"
)


def scrape_cpi_data():
    """
    method used from:
    * https://towardsdatascience.com/web-scraping-html-tables-with-python-c9baba21059

    :return: a pandas DataFrame that contains the CPI Inflation ratio with columns: [
        'Year', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Ave'
    ]
    """
    # Create a handle, page, to handle the contents of the website
    page = requests.get(inflation_url)
    # Store the contents of the website under doc
    doc = html.fromstring(page.content)
    # Parse data that are stored between <tr>..</tr> of HTML
    # the table_xpath was found from inspection of the web page
    table_xpath = '//*[@id="GridView1"]'
    tr_elements = doc.xpath(table_xpath + "//tr")

    # Parse table header
    # For each row, store each first element (header) and an empty list
    col = [(t.text_content(), []) for t in tr_elements[0]]
    # rename the last column to Ave instead of 'Ave.'
    col[-1] = ("Ave", [])

    # Create pandas DF
    # each header is appended to a tuple along with an empty list
    qty_cols = len(col)
    # Since out first row is the header, data is stored on the second row onwards
    for j in range(1, len(tr_elements)):
        # T is our j'th row
        row = tr_elements[j]

        # If row is not of size 14, the //tr data is not from our table
        if len(row) != qty_cols:
            break
        i = 0
        # Iterate through each element of the row
        for t in row.iterchildren():
            data = t.text_content()
            # Check if row is empty
            # Convert any numerical value to integers
            try:
                data = int(data) if i == 0 else float(data)
            except ValueError:
                data = None if i == 0 else np.NaN
            # Append the data to the empty list of the i'th column
            col[i][1].append(data)
            # Increment i for the next column
            i += 1

    return pd.DataFrame({title: column for (title, column) in col})


def scrape_cpi_data_selenium(
    selenium_driver: Optional[webdriver.Chrome] = None, debug: bool = False
) -> pd.DataFrame:
    """
    depreciated in favor or the requests based method above
    uses the selenium web driver to mine CPI data data from a chart at 
    'https://inflationdata.com/Inflation/Consumer_Price_Index/HistoricalCPI.aspx'. 
    returns a dataframe
    
    :param selenium_driver: 
    :param debug: 
    :return:
    """
    if selenium_driver is None:
        selenium_driver = get_driver(debug=debug)
    selenium_driver.get(inflation_url)
    print(selenium_driver.title)
    return pd.read_html(
        selenium_driver.find_element_by_xpath('//*[@id="form1"]/div[3]').get_attribute(
            "innerHTML"
        ),
        header=0,
    )[0]
