from typing import Optional, Dict

import requests
from config import default_settings


def create_web_session(proxies: Optional[Dict[str, str]] = None) -> requests.Session:
    """
    creates a new requests session for connecting with the websites

    :param proxies:
    :return:
    """
    s = requests.Session()
    s.proxies = proxies if proxies is not None else default_settings.proxies
    return s
