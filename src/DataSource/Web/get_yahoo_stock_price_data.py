import datetime
import sys
from io import StringIO
from typing import Optional

import numpy as np
import pandas as pd
import requests
import src.Utility as Util
from config import default_settings

from .create_web_session import create_web_session


def _format_yahoo_split_factor(text: str):
    """
    takes the text format of the stock split info returned by yahoo
    and converts it into a float number. Result is the multiplicative
    factor that should be applied to the # of shares owned.
    """
    numerator, denominator = text.split("/")
    return float(numerator) / float(denominator)


def _format_stock_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    """
    takes a dataframe of all numbers (index implied to be the date) and ensures that
    the result is all float data with NaN values where null or invalid values are

    :param df:
    :return:
    """
    for col in df.columns:
        if df[col].dtype == np.object:
            df.loc[df[col] == "null", col] = np.nan
            df[col] = df[col].astype(float)
        elif df[col].dtype is not float:
            df[col] = df[col].astype(float)
    return df.fillna(method="ffill")


def get_stock_prices_from_yahoo(
    symbol: str,
    s: Optional[requests.Session] = None,
    start_date: Optional[pd.Timestamp] = None,
    end_date: Optional[pd.Timestamp] = None,
    crumb_value: Optional[str] = None,
    cookie_value: Optional[str] = None,
) -> pd.DataFrame:
    """
    loads stock data from the yahoo server

    :param symbol: the tckr to query
    :param s: a requests server instance for connecting with the server. If one is not provided then a default session
    will be created with no opportunities to provide any proxy or special information
    :param start_date: will default to 1990-1-1 and is inclusive. It is recommended that one is provided
    :param end_date: will default to the day of the function call and is exclusive
    (will not include data for the actual end_date). It is recommended that one is provided
    :param crumb_value: string value supplied in the request url to authenticate the
    session instance to the server
    :param cookie_value: string value supplied in the request cookies to authenticate the
    session instance to the server
    :return: a pandas DataFrame with
    * columns: ['tckr', 'date', 'open', 'high', 'low', 'close', 'adj_close', 'volume', 'dividends', 'split_factor']
    """
    # initialize inputs
    symbol = Util.convertTckrToYahooFmt(symbol)
    start_date = (
        Util.formatDateInput(start_date) if start_date else pd.Timestamp(1990, 1, 1)
    )  # type: ignore
    end_date = (
        Util.formatDateInput(end_date)
        if end_date
        else pd.Timestamp(datetime.date.today() - datetime.timedelta(days=1))
    )
    if s is None:
        s = create_web_session()
    crumb_value = crumb_value if crumb_value else default_settings.crumbValue
    cookie_value = cookie_value if cookie_value else default_settings.cookieValue
    # bring start date and end date to the same format as yahoo by
    #  setting the time to 6:00 AM and converting a duration in seconds
    timestamp_limits = pd.DatetimeIndex(
        [start_date.date(), end_date.date()]
    ) + pd.Timedelta(hours=6)
    timestamp_limits = timestamp_limits.view("int64") / pd.Timedelta(1, unit="s").view(
        "int64"
    )
    start_yahoo_fmt, end_yahoo_fmt = timestamp_limits

    # get price data
    event = "history"
    url_string = (
        "https://query1.finance.yahoo.com/v7/finance/download"
        + "/%s?period1=%10i&period2=%10i&" % (symbol, start_yahoo_fmt, end_yahoo_fmt)
        + "interval=1d&events=%s&crumb=%s" % (event, crumb_value)
    )
    print(f"Querying Yahoo Data for {symbol} from {start_date} to {end_date}")
    price_response = s.get(url_string, cookies={"B": cookie_value})
    raw_table: str = price_response.content.decode(sys.stdout.encoding)
    price_data = pd.read_csv(
        StringIO(raw_table),
        index_col=0,
        header=0,
        names=["date", "open", "high", "low", "close", "adj_close", "volume"],
        parse_dates=["date"],
        infer_datetime_format=True,
    )

    # Get dividend data
    event = "div"
    url_string = (
        "https://query1.finance.yahoo.com/v7/finance/download"
        + "/%s?period1=%10i&period2=%10i&" % (symbol, start_yahoo_fmt, end_yahoo_fmt)
        + "interval=1d&events=%s&crumb=%s" % (event, crumb_value)
    )
    div_response = s.get(url_string, cookies={"B": cookie_value})
    raw_table = div_response.content.decode(sys.stdout.encoding)
    div_data = pd.read_csv(
        StringIO(raw_table),
        index_col=0,
        header=0,
        names=["date", "dividends"],
        parse_dates=["date"],
        infer_datetime_format=True,
    )
    if len(div_data.index):
        price_data = price_data.join(div_data, how="left")
        # price_data = pd.concat(
        #     [price_data, div_data], axis=1, join_axes=[price_data.index]
        # )
        # Set nan dividend values to 0
        price_data.loc[np.isnan(price_data.dividends), "dividends"] = 0
    else:
        price_data["dividends"] = 0.0

    # Get stock Split
    event = "split"
    url_string = (
        "https://query1.finance.yahoo.com/v7/finance/download"
        + "/%s?period1=%10i&period2=%10i&interval=1d&"
        % (symbol, start_yahoo_fmt, end_yahoo_fmt)
        + "events=%s&crumb=%s" % (event, crumb_value)
    )
    split_response = s.get(url_string, cookies={"B": cookie_value})
    raw_table = split_response.content.decode(sys.stdout.encoding)
    split_data = pd.read_csv(
        StringIO(raw_table),
        index_col=0,
        header=0,
        names=["date", "stock_splits"],
        parse_dates=["date"],
        infer_datetime_format=True,
    )
    if len(split_data.index):
        # Convert text mult. factor to a float
        split_data["split_factor"] = split_data.apply(
            lambda row: _format_yahoo_split_factor(row["stock_splits"]), axis=1
        )
        price_data = price_data.join(split_data, how="left")
        # price_data = pd.concat(
        #    [price_data, split_data["split_factor"]],
        #    axis=1,
        #    join_axes=[price_data.index],
        # )
        # Set nan split factor values to 0
        price_data.loc[np.isnan(price_data.split_factor), "split_factor"] = 1.0
    else:
        price_data["split_factor"] = 1.0

    # final formatting
    price_data = _format_stock_dataframe(price_data)
    price_data["tckr"] = Util.convertTckrToUniversalFormat(symbol)
    price_data = price_data.reset_index()
    final_column_order = [
        "tckr",
        "date",
        "open",
        "high",
        "low",
        "close",
        "adj_close",
        "volume",
        "dividends",
        "split_factor",
    ]
    return price_data.loc[:, final_column_order]
