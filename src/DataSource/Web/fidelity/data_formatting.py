import pandas as pd

# Dictionary of fidelity option web page column names to SQLite3 db column names.
web_to_sql_cols = {
    "QueryTimeStamp": "quote_ts",
    "ExDate": "expiration",
    "DaysToExpiration": "days_to_expiration",
    "UnderlyingSymbol": "tckr",
    "UnderlyingPrice": "stock_price",
    "Type": "option_type",
    "Strike": "strike",
    "Symbol": "symbol",
    "Last": "last",
    "Change": "change",
    "Bid Size": "bid_size",
    "Bid": "bid",
    "Ask": "ask",
    "Ask Size": "ask_size",
    "Volume": "volume",
    "Open Int": "open_int",
    "Imp Vol": "imp_vol",
    "Delta": "delta",
    "Gamma": "gamma",
    "Theta": "theta",
    "Vega": "vega",
    "Rho": "rho",
}
web_cols, sql_cols = zip(*web_to_sql_cols.items())
web_dtypes = {
    "QueryTimeStamp": "<M8[ns]",
    "ExDate": "<M8[ns]",
    "DaysToExpiration": "int32",
    "UnderlyingSymbol": "str",
    "UnderlyingPrice": "float32",
    "Type": "str",
    "Strike": "float32",
    "Symbol": "str",
    "Last": "float32",
    "Change": "float32",
    "Bid Size": "int32",
    "Bid": "float32",
    "Ask": "float32",
    "Ask Size": "int32",
    "Volume": "int32",
    "Open Int": "int32",
    "Imp Vol": "float32",
    "Delta": "float32",
    "Gamma": "float32",
    "Theta": "float32",
    "Vega": "float32",
    "Rho": "float32",
}
sql_dtypes = {web_to_sql_cols[k]: v for k, v in web_dtypes.items()}


def format_fidelity_option_df(
    option_data: pd.DataFrame, start_index: int = 0
) -> pd.DataFrame:
    """
    accepts a dataframe with sql table columns and ensures that the index and column types are correct

    :param option_data:
    :param start_index:
    :return:
    """
    for col in option_data.columns:
        option_data[col] = option_data[col].astype(sql_dtypes[col])
    option_data.index = range(start_index, start_index + len(option_data.index))
    return option_data
