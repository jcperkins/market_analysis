import time

from selenium import webdriver
from config import default_settings
from . import webdriver_delay


def init_option_webpage(driver: webdriver, web_symbol: str) -> int:
    """Pulls up the main options page at Fidelity.com and login using my har """
    option_url = (
        "https://researchtools.fidelity.com//ftgw/mloptions//"
        + "goto//optionChain?priced=Y&symbol=%s" % web_symbol
    )
    # wait 3 sec every time you change the page, b/c I was having problems
    # with it loading a page with wrong dates
    driver.get(option_url)
    time.sleep(webdriver_delay)
    print(driver.title + ": " + web_symbol)
    # check for a login link and click on the login link if it exists
    login_link = driver.find_elements_by_xpath('//*[@id="chainPeak"]')
    if len(login_link) > 0:
        print("\tNeed to Login")
        login_link[0].click()
        time.sleep(webdriver_delay)

        # enter username on new page
        driver.find_elements_by_xpath('//*[@id="userId-input"]')[0].send_keys(
            default_settings.fidelity_usr
        )
        driver.find_elements_by_xpath('//*[@id="password"]')[0].send_keys(
            default_settings.fidelity_pwd
        )
        # press the submit button
        submit_button = driver.find_elements_by_xpath('//*[@id="fs-login-button"]')[0]
        submit_button.click()
        time.sleep(webdriver_delay)
        driver.get(option_url)
        time.sleep(webdriver_delay)
    print("\tLogin Successful")

    # ensure that all dates are unselected
    exdate_bar_element = driver.find_element_by_xpath(
        '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
    )
    selected_elements = exdate_bar_element.find_elements_by_class_name("selected-state")
    if len(selected_elements) > 0:
        for expirationElement in selected_elements:
            expirationElement.click()
            time.sleep(webdriver_delay)
        # click on the apply settings button
        apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
        apply_button.click()
        time.sleep(webdriver_delay)
    # get the total number of expiration dates
    exdate_qty = len(
        driver.find_elements_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
        )
    )
    return exdate_qty
