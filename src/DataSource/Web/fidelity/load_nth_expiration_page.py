import time

import pandas as pd
from selenium import webdriver

from . import webdriver_delay


def load_nth_expiration_page(
    driver: webdriver, i_date: int, query_timestamp: pd.Timestamp
) -> webdriver:
    """
    Loads the web page for the nth expiration date. This function assumes that the webdriver has already
    been initialized to the correct web page for the symbol

    :param driver: a selenium webdriver that has already been initialized to the correct symbol
    on the options portion of fidelity.com
    :param i_date: indicates which expiration date in the fidelity expiration date bar to load
    :param query_timestamp: the timestamp of the query. A sanity check is made to ensure that expired data is ignored
    :return: the driver with the correct page loaded
    """
    # select the current expiration date and apply the settings
    # reset the exDateBar
    exdate_bar_element = driver.find_element_by_xpath(
        '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
    )
    bar_length = len(
        driver.find_elements_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
        )
    )
    selected_elements = exdate_bar_element.find_elements_by_class_name("selected-state")
    if len(selected_elements) > 0:
        for expirationElement in selected_elements:
            expirationElement.click()

    # if there is an offset caused by invalid dates, find the offset
    index_offset = 1
    while True:
        button_date = pd.Timestamp(
            driver.find_element_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
                + "/div/ul/li[%1i]" % index_offset
            ).get_attribute("value")
        )
        if button_date >= query_timestamp.date():
            break
        index_offset += 1
    if i_date + index_offset > bar_length:
        return None
    date_toggle = driver.find_element_by_xpath(
        '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
        + "/div/ul/li[%1i]" % (i_date + index_offset)
    )
    date_toggle.click()
    time.sleep(webdriver_delay)
    # finding all selected dates and 'click' any that are not the intended date so that they are unselected
    selected_elements = exdate_bar_element.find_elements_by_class_name("selected-state")
    while True:
        if len(selected_elements) == 1:
            break
        for expirationElement in selected_elements:
            expirationElement.click()
        date_toggle.click()
        time.sleep(webdriver_delay)

    apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
    apply_button.click()
    time.sleep(webdriver_delay)
    return driver
