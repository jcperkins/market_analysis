import pandas as pd
import numpy as np
import time
import datetime
from lxml import html

from . import webdriver_delay
from .data_formatting import web_dtypes, web_to_sql_cols, web_cols, sql_cols, sql_dtypes
from .parsing_helpers import (
    parse_driver_table_heading,
    call_put_transition_index,
    parse_xml_option_table,
    parse_stock_price,
    parse_expiration_date,
)
import src.Utility as Util
from .load_nth_expiration_page import load_nth_expiration_page


def read_nth_option(
    i_date,
    symbol,
    date_qty,
    query_timestamp,
    driver,
    col_headings=None,
    call_put_transition=None,
    debug=False,
):
    page_timestamp = pd.Timestamp.today()
    load_nth_expiration_page(driver, i_date, page_timestamp)
    # Initialize column headers and dataFrames
    if col_headings is None:
        print("\tMining Column Names")
        col_headings = parse_driver_table_heading(driver)
    if call_put_transition is None:
        call_put_transition = call_put_transition_index(col_headings)

    # Get number of rows in table for the current exdate
    row_qty = driver.execute_script(
        "return document.getElementById"
        + '("fullTable").'
        + "getElementsByClassName("
        + "'mth0').length"
    )

    # Init new dataFrame for the data
    option_data = pd.DataFrame(index=range(row_qty * 2), columns=sql_cols)
    option_data["tckr"] = symbol
    option_data["quote_ts"] = page_timestamp
    option_data["option_type"] = np.array(["Call"] * row_qty + ["Put"] * row_qty)
    # get the price of the underlying symbol
    option_data["stock_price"] = parse_stock_price(driver)
    # get the expiration date info from the page
    exdate, days_to_expiration = parse_expiration_date(driver)
    option_data["expiration"] = exdate
    option_data["days_to_expiration"] = days_to_expiration

    print(
        f"\tMining {symbol} expiration date {i_date + 1} of {date_qty}: {exdate} ({row_qty} rows)"
    )
    table_tree = html.fromstring(
        driver.find_element_by_xpath('//*[@id="fullTable"]').get_attribute("innerHTML")
    )
    option_data = parse_xml_option_table(
        table_tree, option_data, col_headings, call_put_transition
    )

    #        # deselect the current expiration date
    #        time.sleep(self.webdriver_delay)
    #        date_toggle = driver.find_element_by_xpath(
    #                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
    #                + '/div/ul/li[%1i]' % (i_date+1))
    #        date_toggle.click()
    # TODO: probably not worth the complexity to return the col_headings and call_put_transition
    return option_data, col_headings, call_put_transition
