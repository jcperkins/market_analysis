from typing import List, Union, Optional

import numpy as np
import pandas as pd
from lxml.html import HtmlElement

from ..data_formatting import web_to_sql_cols, web_cols, format_fidelity_option_df
from .parse_table_heading import parse_html_table_heading, call_put_transition_index


def parse_xml_option_table(
    table_tree: HtmlElement,
    option_data: pd.DataFrame,
    header_names: Optional[List[str]],
    call_put_transition: Optional[int],
) -> pd.DataFrame:
    """
    Reads the html table of option prices off of the selenium driver and adds the data to the option_data DataFrame.
    This assumes that the correct web page has already been loaded onto the driver.

    :param table_tree: an HtmlElement of the options table of the fidelity web page
    :param option_data: a pandas DataFrame with the same columns of the SQLite3 table
    :param header_names: the pre-parsed names of the table heading
    :param call_put_transition: the column index where data transitions from call options to put options
    :param debug:
    :return:
    """
    if header_names is None:
        print("\tMining Column Names")
        header_names = parse_html_table_heading(table_tree)
    if call_put_transition is None:
        call_put_transition = call_put_transition_index(header_names)

    target_rows = int(len(option_data.index) / 2)
    for iCol in range(len(header_names)):
        web_name = header_names[iCol]
        # no valid data in action cell so if in that column skip it
        if web_name == "Action":
            continue
        assert web_name in web_cols
        sql_name = web_to_sql_cols[web_name]
        # Different columns have different structures that need to be
        #  navigated past the td html tag those that end in '/a' will
        #  also pick up a dummy value from the sub-header of the table
        #  and so first_valid_row needs to be moved to 1 for only
        #  those columns
        xpath_suffix = ""
        if web_name in ["Symbol", "Bid", "Ask"]:
            xpath_suffix = "/a"
        elif web_name in ["Volume", "Open Int"]:
            xpath_suffix = "/div/span"

        # dtypes determination
        data_type = str
        if web_name in [
            "Strike",
            "Last",
            "Change",
            "Bid",
            "Ask",
            "Imp Vol",
            "Delta",
            "Gamma",
            "Theta",
            "Vega",
            "Rho",
        ]:
            data_type = float
        elif web_name in ["Bid Size", "Ask Size", "Volume", "Open Int"]:
            data_type = int

        # start processing data

        # Get data as txt from column
        raw_column_text = table_tree.xpath(
            "//tbody/tr/td[%01i]%s/text()" % (iCol + 1, xpath_suffix)
        )
        col_data = np.array(
            [x.strip().replace("$", "").replace(",", "") for x in raw_column_text],
            dtype=str,
        )
        # there may be junk rows at the beginning so take the last `target_rows` rows of data
        first_valid_row = len(col_data) - target_rows
        col_data = col_data[first_valid_row:]
        if web_name == "Strike":
            # Add the strike to the call and put columns along with the
            #  other information at the start of the line
            option_data[sql_name] = np.tile(col_data.astype(float), 2)
        elif web_name in ["Imp Vol"]:
            # remove the percent sign and return a float
            col_data = np.array([x[:-2] for x in col_data], dtype="|S5")
            col_data[col_data == b""] = np.nan
            if iCol < call_put_transition:
                option_data.loc[option_data.option_type == "Call", sql_name] = col_data
            else:
                option_data.loc[option_data.option_type == "Put", sql_name] = col_data
        else:
            # add the data in the predetermined format such that empty
            #  cells are nan values for numeric columns
            if data_type in [int, float]:
                col_data = col_data.astype("|S5")
                col_data[np.logical_or(col_data == b"", col_data == b"--")] = np.nan
            if iCol < call_put_transition:
                option_data.loc[option_data.option_type == "Call", sql_name] = col_data
            else:
                option_data.loc[option_data.option_type == "Put", sql_name] = col_data

    return format_fidelity_option_df(option_data)
