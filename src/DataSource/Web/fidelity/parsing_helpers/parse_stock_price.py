from selenium import webdriver


def parse_stock_price(driver: webdriver) -> float:
    """Parse the stock price of the underlying stock from the options web page that is loaded into the webdriver"""
    return float(
        driver.find_elements_by_xpath('//*[@id="companyDetailsDiv"]/div/span[1]')[0]
        .text.replace("$", "")
        .replace(",", "")
    )
