from typing import List
from lxml.html import HtmlElement
from selenium import webdriver


def parse_driver_table_heading(driver: webdriver) -> List[str]:
    # Get table headers by looping through the TH values until
    #  one doesn't exist
    return [
        x.text
        for x in driver.find_elements_by_xpath('//*[@id="fullTable"]/thead[1]/tr/th')
    ]


def parse_html_table_heading(table_tree: HtmlElement) -> List[str]:
    return [x for x in table_tree.xpath("//thead[1]/tr/th/span/text()")]


def call_put_transition_index(col_names: List[str]) -> int:
    assert "Strike" in col_names
    return col_names.index("Strike")
