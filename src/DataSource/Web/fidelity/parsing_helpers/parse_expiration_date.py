import datetime
from typing import Tuple

import src.Utility as Util
from selenium import webdriver


def parse_expiration_date(driver: webdriver) -> Tuple[datetime.date, int]:
    """
    Parses the expiration date from the page and returns the date and the number of days
    the page claims that the option has before expiring.
    """
    # get the expiration date info from the first expiration date
    exdate_info = (
        driver.find_elements_by_class_name("mth0")[0].get_attribute("name").split(" ")
    )
    exdate = datetime.date(
        int("20" + exdate_info[2][1:]),
        Util.monthAbbrToNumConversion[exdate_info[0]],
        int(exdate_info[1]),
    )

    if "(" in exdate_info[3]:
        days_to_expiration = int(exdate_info[3][(exdate_info[3].find("(") + 1) :])
    else:
        days_to_expiration = int(exdate_info[4][(exdate_info[4].find("(") + 1) :])

    return exdate, days_to_expiration
