# -----------------------------------------------------------------------------
# Name:        OptionData.py
# Purpose:      Database management class for accessing option data
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import time
import datetime
import os
import pandas as pd
import numpy as np
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from lxml import html
from .StockData import StockData
from .LIBORData import LIBORData
import src.Utility as Util
from typing import Optional, List, Union, Dict, Sequence, Any
from config import default_settings


webdriver_delay = 0.005

# for possible future implementations with quandl
# import quandl
# quandl.ApiConfig.api_key = "CwCravWAoyv4zVzb5U-M"


class OptionData:
    """debug code
from MarketAnalysis import MarketData;reload(MarketData)
workProxy = True
lDB = MarketData.LIBORData(workProxy = workProxy)
sDB = MarketData.LIBORData(workProxy = workProxy)

#spy Call
tckr = 'SPY'
optionType = 'Call'
purchaseDate = "2017-04-16"
exDate = "2019-12-20"
strike = 225;S0 = 232.51
bidAskSpread = [26.85,28.0]

#SPX call
tckr = '.SPX'
optionType = 'Call'
purchaseDate = "2017-08-01"
exDate = "2017-08-28"
strike = 2445
S0 = 2476.31
bidAskSpread = [39.10,39.61]

reload(MarketData)
self = oDB = MarketData.OptionData(workProxy=workProxy, stockDB=sDB,
                                   LIBORData=lDB)
#stockPrice Function
oDB.PriceEst_BS(tckr, optionType, purchaseDate, strike, exDate, S0=S0)
"""

    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
        webDriver: Optional[Any] = None,
        stockDB: Optional[StockData] = None,
        liborDB: Optional[LIBORData] = None,
    ):
        if proxies is None:
            proxies = default_settings.proxies
        self.storageName = "DailyOptionQuotesFidelity"
        self.workProxyBool = workProxy
        self.mainDir = fileDir if fileDir is not None else default_settings.root_dir
        if self.workProxyBool and not len(proxies):
            self.proxies = default_settings.proxies
        else:
            self.proxies = proxies
        #        self.session = requests.session()
        #        self.session.proxies = self.proxies
        #        self.yahooStorageCols = ['Strike','Last','Bid','Ask','Chg','PctChg',
        #           'Vol','Open_Int','IV','Underlying_Price']
        if liborDB is None:
            self.liborDB = LIBORData(
                fileDir=fileDir, workProxy=workProxy, proxies=proxies
            )
        else:
            self.liborDB = liborDB
        if stockDB is None:
            self.stockDB = StockData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.stockDB = stockDB

        # Fidelity web scraping info
        self.webdriver_delay = webdriver_delay
        self.chromedriver = default_settings.chromedriver
        self.phantomJSDriver = default_settings.phantomJSDriver
        self.finalContextColumns = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "UnderlyingPrice",
            "Type",
            "Strike",
        ]
        self.finalDataColumns = [
            "Symbol",
            "Last",
            "Change",
            "Bid Size",
            "Bid",
            "Ask",
            "Ask Size",
            "Volume",
            "Open Int",
            "Imp Vol",
            "Delta",
            "Gamma",
            "Theta",
            "Vega",
            "Rho",
        ]
        self.hd5QueryCols = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "Type",
            "Strike",
            "Volume",
            "Imp Vol",
            "Open Int",
            "Delta",
        ]
        self.finalColumns = self.finalContextColumns + self.finalDataColumns
        self.finalDtypes = [
            "datetime64[ns]",
            "datetime64[ns]",
            np.int,
            np.object,
            np.float,
            np.object,
            np.float,
            np.object,
        ] + [np.float] * 14
        self.dataNameToColNumDict = dict(
            map(
                lambda dataName: (dataName, self.finalColumns.index(dataName)),
                self.finalColumns,
            )
        )
        self.monthAbbrToNumConversion = {
            "Jan": 1,
            "Feb": 2,
            "Mar": 3,
            "Apr": 4,
            "May": 5,
            "Jun": 6,
            "Jul": 7,
            "Aug": 8,
            "Sep": 9,
            "Oct": 10,
            "Nov": 11,
            "Dec": 12,
        }
        self.fidelity_dtypes = {
            "QueryTimeStamp": "<M8[ns]",
            "ExDate": "<M8[ns]",
            "DaysToExpiration": "int32",
            "UnderlyingSymbol": "str",
            "UnderlyingPrice": "float32",
            "Type": "str",
            "Strike": "float32",
            "Symbol": "str",
            "Last": "float32",
            "Change": "float32",
            "Bid Size": "int32",
            "Bid": "float32",
            "Ask": "float32",
            "Ask Size": "int32",
            "Volume": "int32",
            "Open Int": "int32",
            "Imp Vol": "float32",
            "Delta": "float32",
            "Gamma": "float32",
            "Theta": "float32",
            "Vega": "float32",
            "Rho": "float32",
        }
        # Initialize the database
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", self.storageName + ".h5")
        )
        self.keyList = self.storage.keys()
        self.storage.close()
        self.driver = webDriver

    def createOption_BS(
        self,
        tckr,
        purchaseDate,
        targetExpiration,
        targetStrike,
        optionType,
        currentPrice=None,
        strikeCondition="=",
        expirationCondition="=",
        source="storage",
    ):
        """
        o  tckr is the symbol of the underlying security
        o  purchase date is the date that the contracts would be purchased
        o  current price is the price of the stock on the purchase date
        o  target expiration is the ideal expiration date
        o  target strike is the ideal strike price
        o  strike condition can be one of ['<','=','>'] meaning must be
            less than the target, closest, must be greater than the
            target respectively
        o  expiration condition ... see strike condition for similar formatting
            
            tckr = 'ixSPX'
            purchaseDate = datetime.datetime(2017,8,4)
            currentPrice = 2473.48
            targetExpiration = datetime.datetime(2018,12,15)
            targetStrike = currentPrice*.8
            strikeCondition = '='
            expirationCondition = '='
            reload(Util)
        """
        if currentPrice is None:
            currentPrice = self.stockDB.getPrice(tckr, purchaseDate, source=source)
            assert np.isfinite(currentPrice), (
                "Invalid Price returned to "
                + "MarketData.OptionData.createOption_BS. "
                + "Tckr: %s, Date: %s, Price: %s"
                % (tckr, str(purchaseDate), str(currentPrice))
            )

        exDate, optionRange = OptionData.findClosestExDate(
            purchaseDate, targetExpiration, expirationCondition=expirationCondition
        )
        closestStrike = self.findClosestStrikePrice(
            purchaseDate,
            exDate,
            targetStrike,
            optionRange,
            strikeCondition,
            currentPrice,
        )
        priceBS = self.PriceEst_BS(
            tckr,
            optionType,
            purchaseDate,
            closestStrike,
            exDate,
            S0=currentPrice,
            source=source,
        )
        return priceBS, Util.formatDateInput(exDate), closestStrike

    @staticmethod
    def findClosestExDate(purchaseDate, targetExpiration, expirationCondition="="):
        # Find closest expiration date that meets condition
        targetExpiration = Util.convert_date(targetExpiration, date_type=np.datetime64)
        purchaseDate = Util.formatDateInput(purchaseDate)
        exDates = OptionData.estimateSPXOptionDates(purchaseDate)
        if expirationCondition == "<":
            exDates = exDates.loc[exDates.index < targetExpiration]
        elif expirationCondition == "<=":
            exDates = exDates.loc[exDates.index <= targetExpiration]
        elif expirationCondition == ">":
            exDates = exDates.loc[exDates.index > targetExpiration]
        elif expirationCondition == ">=":
            exDates = exDates.loc[exDates.index >= targetExpiration]
        purchaseDate = Util.convert_date(purchaseDate, date_type=np.datetime64)
        exDates["DaysOut"] = np.array(exDates.index - purchaseDate).astype(
            np.timedelta64
        ) / np.timedelta64(1, "D")
        exDates["TargetDelta"] = np.array(exDates.index - targetExpiration).astype(
            np.timedelta64
        ) / np.timedelta64(1, "D")
        if expirationCondition == "<":
            absDelta = exDates.TargetDelta[exDates.TargetDelta < 0].abs()
            closestExpiration = exDates.index[exDates.TargetDelta == absDelta.min()][0]
        elif expirationCondition == ">":
            absDelta = exDates.TargetDelta[exDates.TargetDelta > 0].abs()
            closestExpiration = exDates.index[exDates.TargetDelta == absDelta.min()][0]
        else:  # (expirationCondition in ['=','Closest']):
            absDelta = exDates.TargetDelta.abs()
            closestExpiration = exDates.index[absDelta == absDelta.min()][0]
        return closestExpiration, exDates.loc[closestExpiration, "OptionRange"]

    @staticmethod
    def findClosestStrikePrice(
        purchaseDate, exDate, targetStrike, optionRange, strikeCondition, currentPrice
    ):
        """
        StrikeCondition can be ['=','<','<=','>','>='] indicating
        whether to pick the absolute closest, next lower, or next higher
        available options. '=' indicates closest value
        """
        # print(purchaseDate,exDate,targetStrike,optionRange,strikeCondition)
        deltaDays = (
            Util.convert_date(exDate, date_type=np.datetime64)
            - Util.convert_date(purchaseDate, date_type=np.datetime64)
        ) / np.timedelta64(1, "D")
        offeringType = []
        if "W" in optionRange:
            offeringType.append("weekly")
        if "M" in optionRange:
            offeringType.append("monthly")
        if "Q" in optionRange:
            offeringType.append("quarterly")
        if "Y" in optionRange:
            offeringType.append("yearly")
        strikeOptions = np.array(
            OptionData.estimateSPXStrikesOffered(
                currentPrice, deltaDays, offeringType=offeringType
            )
        )
        if strikeCondition == "<":
            strikeOptions = strikeOptions[strikeOptions < targetStrike]
        elif strikeCondition == "<=":
            strikeOptions = strikeOptions[strikeOptions <= targetStrike]
        elif strikeCondition == ">":
            strikeOptions = strikeOptions[strikeOptions > targetStrike]
        elif strikeCondition == ">=":
            strikeOptions = strikeOptions[strikeOptions >= targetStrike]
        strikeDeltas = np.abs(strikeOptions - targetStrike)
        return float(strikeOptions[strikeDeltas == strikeDeltas.min()])

    def PriceEst_BS(
        self,
        tckr,
        optionType,
        dateOfPrice,
        strike,
        exDate,
        S0=None,
        sigma=None,
        rate=None,
        source="storage",
    ):
        """Estimate the Black and Scholes Price for the Option
            o  tckr is the market symbol for the underlying asset
            o  optionType is 'C' for call or 'P' for put
            o  purchase date is the day that you would sell or buy the option
            o  strike is the strike price of the option contract
            o  exDate is the expiration date of the option contract
            o  S0 is price of the underlying asset
            o  K is the strike price
            o  sigma is the volatility
            o  r is the risk free rate (annual rate, expressed in terms of
               continuous compounding)
            o  T is the time to maturity expressed in years
        """
        tckr = Util.convertTckrToUniversalFormat(tckr)
        exDate = Util.formatDateInput(exDate)
        dateOfPrice = Util.formatDateInput(dateOfPrice)
        daysOut = np.array((exDate - dateOfPrice).days)
        T = daysOut / 365.0
        method = optionType[0].upper()
        if S0 is None:
            S0 = self.stockDB.getPrice(tckr, dateOfPrice, source=source)
        if sigma is None:
            sigma = (
                self.stockDB.PredictSPXVol_1m3mVIX(dateOfPrice, exDate, source=source)
                / 100.0
            )
        if rate is None:
            rate = (
                self.liborDB.getLIBOR_Rate(dateOfPrice, daysOut, source=source) / 100.0
            )
        return Util.BlackScholes(method, S0, strike, sigma, rate, T)

    # noinspection PyUnusedLocal
    @staticmethod
    def estimateAllSPXOptions(
        current_date: pd.Timestamp,
        weekly: bool = True,
        mon: bool = True,
        wed: bool = True,
        fri: bool = True,
        monthly: bool = True,
        quarterly: bool = True,
        yearly: bool = True,
    ) -> None:
        """ in work """
        spx_dates = OptionData.estimateSPXOptionDates(
            current_date,
            weekly=weekly,
            mon=mon,
            wed=wed,
            fri=fri,
            monthly=monthly,
            quarterly=quarterly,
            yearly=yearly,
        )
        spx_dates["exDate"] = spx_dates.index
        spx_dates.index = range(len(spx_dates.index))
        spx_options = pd.DataFrame(columns=spx_dates.columns + ["StrikePrice"])
        for exDate in spx_dates["exDates"]:
            deltaDays = (
                Util.convert_date(exDate, date_type=np.datetime64)
                - Util.convert_date(current_date, date_type=np.datetime64)
            ) / np.timedelta64(1, "D")
        offering_type = []
        if "W" in offering_type:
            offering_type.append("weekly")
        if "M" in offering_type:
            offering_type.append("monthly")
        if "Q" in offering_type:
            offering_type.append("quarterly")
        if "Y" in offering_type:
            offering_type.append("yearly")
        # strikes = np.array(OptionData.estimateSPXStrikesOffered(currentPrice, deltaDays, offeringType=offering_type))
        # newOptions = pd.DataFrame()
        # spx_options = spx_options.append(pd.D)
        return

    @staticmethod
    def estimateSPXOptionDates(
        current_date: pd.Timestamp,
        weekly: bool = True,
        mon: bool = True,
        wed: bool = True,
        fri: bool = True,
        monthly: bool = True,
        quarterly: bool = True,
        yearly: bool = True,
    ) -> pd.DataFrame:
        """
        Assumes:
            o  weekly options for 8 weeks including the current week
            o  monthly options on the last business day and the Friday
                after the 15th day of the month for 6 months
            o  quarterly options on the Friday after the 15th day of the
                last month of the quarter for 4 quarters
            o  yearly options on the Friday after the 15th day of june and
                dec for the next 2 years
        """
        exdates = {}
        # weekly option dates
        if weekly:
            # current_date.isoweekday()%7 will give the the day of the week
            #  starting with sunday as the first day - default is monday
            #  as the first day)
            # endOfWeeklies points to the sunday of the 9th week
            end_of_weeklies = current_date + pd.Timedelta(
                days=(8 * 7) - (current_date.isoweekday() % 7)
            )
            for wdate in pd.bdate_range(
                start=current_date, end=end_of_weeklies, freq="B"
            ):
                wdate = Util.formatDateInput(wdate)
                if mon and wdate.isoweekday() % 7 == 1:
                    if wdate in exdates.keys():
                        exdates[wdate] += "Wm"
                    else:
                        exdates[wdate] = "Wm"
                if wed and wdate.isoweekday() % 7 == 3:
                    if wdate in exdates.keys():
                        exdates[wdate] += "Ww"
                    else:
                        exdates[wdate] = "Ww"
                if fri and wdate.isoweekday() % 7 == 5:
                    if wdate in exdates.keys():
                        exdates[wdate] += "Wf"
                    else:
                        exdates[wdate] = "Wf"
        # monthly option dates
        if monthly:
            plus6months = current_date + pd.Timedelta(days=180)
            final_year, final_month = plus6months.year, plus6months.month
            iterate_year, iterate_month = current_date.year, current_date.month
            while final_year > iterate_year or (
                final_year == iterate_year and final_month >= iterate_month
            ):
                middle_of_month = Util.middle_friday_of_month(
                    iterate_year, iterate_month
                )
                end_of_month = Util.last_businessday_of_month(
                    iterate_year, iterate_month
                )
                if middle_of_month in exdates.keys():
                    exdates[middle_of_month] += "M"
                else:
                    exdates[middle_of_month] = "M"
                if end_of_month in exdates.keys():
                    exdates[end_of_month] += "M"
                else:
                    exdates[end_of_month] = "M"
                iterate_month += 1
                iterate_year += int(iterate_month / 13)
                iterate_month = iterate_month % 12 if iterate_month != 12 else 12

        # Quarterly option dates
        if quarterly:
            lastMonthInQuarterList = np.array([3, 6, 9, 12])
            # Add 1 to current_date.month as a simple cheat to avoid some
            #  issues in backtest where
            # Options are called days before new offerings are available.
            #  Nothing changes for months that don't have offerings, but
            #  months with offerings will get to pick from offers a
            #  year currentDay
            startMonth = min(
                lastMonthInQuarterList[
                    lastMonthInQuarterList >= (current_date.month + 1) % 13
                ]
            )
            startYear = current_date.year
            for i in range(4):
                month = startMonth + i * 3
                year = startYear + int(month / 12)
                month %= 12
                if month == 0:
                    month = 12
                    year -= 1
                middle_of_month = Util.middle_friday_of_month(year, month)
                end_of_month = Util.last_businessday_of_month(year, month)
                if middle_of_month in exdates.keys():
                    exdates[middle_of_month] += "Q"
                else:
                    exdates[middle_of_month] = "Q"
                if end_of_month in exdates.keys():
                    exdates[end_of_month] += "Q"
                else:
                    exdates[end_of_month] = "Q"

        if yearly:
            # Next year's yearly options
            # Cut off the yearly options so that the last date is the Jan
            #  offering in the third year after the current one
            year = current_date.year
            juneDate = Util.middle_friday_of_month(year + 1, 6)
            decDate = Util.middle_friday_of_month(year + 1, 12)
            if juneDate in exdates.keys():
                exdates[juneDate] += "Y"
            else:
                exdates[juneDate] = "Y"
            if decDate in exdates.keys():
                exdates[decDate] += "Y"
            else:
                exdates[decDate] = "Y"
            # Following year's yearly options
            juneDate = Util.middle_friday_of_month(year + 2, 6)
            decDate = Util.middle_friday_of_month(year + 2, 12)
            if juneDate in exdates.keys():
                exdates[juneDate] += "Y"
            else:
                exdates[juneDate] = "Y"
            if decDate in exdates.keys():
                exdates[decDate] += "Y"
            else:
                exdates[decDate] = "Y"
        exdates = pd.DataFrame(data=exdates, index=["OptionRange"]).transpose()

        exdates = exdates.sort_index()
        return exdates

    @staticmethod
    def createGenericStrikePriceRange(
        current_price, strike_deltas, lower_bounds, upper_bounds
    ):
        strikes = []
        for i in range(len(strike_deltas)):
            delta = strike_deltas[i]
            start = round(
                lower_bounds[i] * current_price
                + (delta - lower_bounds[i] * current_price % delta),
                1,
            )
            end = round(
                upper_bounds[i] * current_price
                - upper_bounds[i] * current_price % delta,
                1,
            )
            num = int((end - start) / delta) + 1
            strikes += np.linspace(start, end, num=num, endpoint=True).tolist()
        strikes = list(set(strikes))
        strikes.sort()
        return strikes

    @staticmethod
    def estimateWeeklyExpirationStrikes(currentPrice):
        """ 5 deltas to model
            largest delta: start at 45% and ends at 120%
            max-1: start at 50% of the currentprice and end at 115%
            max-2: start at 72% and end at 112%
            max-3: start at 80% and end at 109%
            smallest: start at 85% and end at 107%
        """
        lowerBounds = [0.85, 0.80, 0.72, 0.5, 0.45]
        upperBounds = [1.07, 1.09, 1.12, 1.15, 1.2]
        if currentPrice > 1000:
            strikeDeltas = [5, 10, 25, 50, 100]
        elif currentPrice > 500:
            strikeDeltas = [2.5, 5, 10, 25, 50]
        elif currentPrice > 200:
            strikeDeltas = [1, 2.5, 5, 10, 25]
        elif currentPrice > 100:
            strikeDeltas = [0.5, 1, 2.5, 5, 10]
        else:
            strikeDeltas = [0.25, 0.5, 1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            currentPrice, strikeDeltas, lowerBounds, upperBounds
        )

    @staticmethod
    def estimateMonthlyExpirationStrikes(current_price):
        """
        3 delta's
            largest delta: starts at 40% ends at  120%
            n-1: starts at 55% and ends at 115%
            n-2: starts at 70% and ends at 110%
        """
        lower_bounds = [0.70, 0.55, 0.40]
        upper_bounds = [1.10, 1.15, 1.20]
        if current_price > 1000:
            strike_deltas = [25, 50, 100]
        elif current_price > 500:
            strike_deltas = [10, 25, 50]
        elif current_price > 200:
            strike_deltas = [5, 10, 25]
        elif current_price > 100:
            strike_deltas = [2.5, 5, 10]
        else:
            strike_deltas = [1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            current_price, strike_deltas, lower_bounds, upper_bounds
        )

    @staticmethod
    def estimateQuarterlyExpirationStrikes(current_price, days_out):
        """
        3 deltas to model
            largest delta: start at  (19.655ln(x) - 66.444)/100 percent
                of the currentprice and ends at 120%
            middle delta: start at (18.373ln(days out) - 42.749)/100 percent
                of the currentprice and end at 115%
            smallest delta: start at (19.752ln(x) - 37.118)/100 percent of
                the currentprice and end at 110%
        """
        lower_bounds = [
            (19.752 * np.log(days_out) - 37.118) / 100.0,
            (18.373 * np.log(days_out) - 42.749) / 100.0,
            (19.655 * np.log(days_out) - 66.444) / 100.0,
        ]
        upper_bounds = [1.10, 1.15, 1.20]
        if current_price > 1000:
            strike_deltas = [25, 50, 100]
        elif current_price > 500:
            strike_deltas = [10, 25, 50]
        elif current_price > 200:
            strike_deltas = [5, 10, 25]
        elif current_price > 100:
            strike_deltas = [2.5, 5, 10]
        else:
            strike_deltas = [1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            current_price, strike_deltas, lower_bounds, upper_bounds
        )

    @staticmethod
    def estimateYearlyExpirationStrikes(current_price):
        """ 3 delta's
            largest delta: starts at 0+delta ends at  140%
            n-1: starts at 25% and ends at 115%
            n-2: starts at 35% and ends at 110%"""
        assert np.isfinite(current_price), (
            "Invalid price passed to "
            + "MarketHelper.estimateYearlyExpirationStrikes, "
            + str(current_price)
        )
        lower_bounds = [0.35, 0.25, 0.01]
        upper_bounds = [1.10, 1.15, 1.40]
        if current_price > 1000:
            strike_deltas = [25, 50, 100]
        elif current_price > 500:
            strike_deltas = [10, 25, 50]
        elif current_price > 200:
            strike_deltas = [5, 10, 25]
        elif current_price > 100:
            strike_deltas = [2.5, 5, 10]
        elif current_price > 50:
            strike_deltas = [1, 2.5, 5]
        else:
            strike_deltas = [0.25, 0.5, 1]
        return OptionData.createGenericStrikePriceRange(
            current_price, strike_deltas, lower_bounds, upper_bounds
        )

    @staticmethod
    def estimateSPXStrikesOffered(
        current_price: float,
        days_out: Union[float, int],
        offeringCode: Optional[str] = None,
        offeringType: Optional[Union[str, List[str]]] = None,
    ) -> List[float]:
        """
        offeringCode is the same as the offeringRange returned
            by estimateSPXOptionDates
        offeringType is depreciated but still valid as a backup. It can
            be one or more of ['weekly','monthly','quarterly','yearly']
        """
        if offeringType is None:
            offeringType = ["weekly", "monthly", "quarterly", "yearly"]
        if type(offeringType) == str:
            offeringType = [offeringType]
        indicators = []
        if offeringCode is not None:
            offeringCode = offeringCode.upper()
            if "W" in offeringCode:
                indicators.append("W")
            if "M" in offeringCode:
                indicators.append("M")
            if "Q" in offeringCode:
                indicators.append("Q")
            if "Y" in offeringCode:
                indicators.append("Y")
        else:
            # Backup implementation of offeringType
            for offering in offeringType:
                indicators.append(offering[0].upper())

        strikes = []
        for indicator in indicators:
            if indicator in ["W", "M", "Q", "Y"]:
                if indicator == "W":
                    strikes += OptionData.estimateWeeklyExpirationStrikes(current_price)
                elif indicator == "M":
                    strikes += OptionData.estimateMonthlyExpirationStrikes(
                        current_price
                    )
                elif indicator == "Q":
                    strikes += OptionData.estimateQuarterlyExpirationStrikes(
                        current_price, days_out
                    )
                elif indicator == "Y":
                    strikes += OptionData.estimateYearlyExpirationStrikes(current_price)
        strikes = list(set(strikes))
        strikes.sort()
        return strikes

    def loadFromDB(
        self,
        underlyingSymbol: str,
        optionType: Optional[str] = None,
        query_Dates: Optional[Union[pd.DatetimeIndex, pd.Timestamp]] = None,
        ex_Dates: Optional[Union[pd.DatetimeIndex, pd.Timestamp]] = None,
        strike: Optional[Sequence[float]] = None,
        days_to_expiration: Optional[Sequence[float]] = None,
        delta=None,
        columns: Optional[Union[str, List[str]]] = None,
        debug: bool = False,
    ):
        """
        Key word arguments that are dates or values should be formatted
        as tuples or lists such that (min,max). If the value isn't a tuple
        then the function will look for an exact match
        """
        # Ensure proper formatting of inputs
        underlyingSymbol = underlyingSymbol.upper().replace(".", "ix")
        if query_Dates:
            if not type(query_Dates) in [list, tuple]:
                query_Dates = [
                    datetime.datetime(
                        query_Dates.year, query_Dates.month, query_Dates.day
                    ),
                    (
                        datetime.datetime(
                            query_Dates.year, query_Dates.month, query_Dates.day
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            elif query_Dates[0] == query_Dates[1]:
                query_Dates = [
                    datetime.datetime(
                        query_Dates[0].year, query_Dates[0].month, query_Dates[0].day
                    ),
                    (
                        datetime.datetime(
                            query_Dates[0].year,
                            query_Dates[0].month,
                            query_Dates[0].day,
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            if query_Dates[0] is not None:
                query_Dates[0] = Util.formatDateInput(query_Dates[0])
            if query_Dates[1] is not None:
                query_Dates[1] = Util.formatDateInput(query_Dates[1])

        if ex_Dates:
            if not type(ex_Dates) in [list, tuple]:
                ex_Dates = [
                    datetime.datetime(ex_Dates.year, ex_Dates.month, ex_Dates.day),
                    (
                        datetime.datetime(ex_Dates.year, ex_Dates.month, ex_Dates.day)
                        + datetime.timedelta(days=1)
                    ),
                ]
            elif ex_Dates[0] == ex_Dates[1]:
                ex_Dates = [
                    datetime.datetime(
                        ex_Dates[0].year, ex_Dates[0].month, ex_Dates[0].day
                    ),
                    (
                        datetime.datetime(
                            ex_Dates[0].year, ex_Dates[0].month, ex_Dates[0].day
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            if ex_Dates[0]:
                ex_Dates[0] = Util.formatDateInput(ex_Dates[0])
            if ex_Dates[1]:
                ex_Dates[1] = Util.formatDateInput(ex_Dates[1])

        if optionType and "C" in optionType.upper():
            optionType = "Call"
        elif optionType and "P" in optionType.upper():
            optionType = "Put"
        elif optionType:
            raise ValueError(
                "optionType: %s is neither " % optionType
                + "'Call' or 'Put' and is not valid"
            )

        if strike and not type(strike) in [list, tuple]:
            strike = [strike, strike]

        if days_to_expiration and not type(days_to_expiration) in [list, tuple]:
            days_to_expiration = [days_to_expiration, days_to_expiration]

        if delta and type(delta) not in [list, tuple]:
            delta = [delta, delta]

        def addToQueryConditions(conditions, range_var, name_str, colname):
            # create the indexCoordinates
            if not range_var:
                return conditions
            elif range_var[0] == range_var[1]:
                conditions.append("%s==%s[0]" % (colname, name_str))
            elif range_var[0]:
                if range_var[1]:
                    conditions.append("%s>=%s[0]" % (colname, name_str))
                    conditions.append("%s<=%s[1]" % (colname, name_str))
                else:
                    conditions.append("%s>=%s[0]" % (colname, name_str))
            else:
                if range_var[1]:
                    conditions.append("%s<=%s[1]" % (colname, name_str))
            return conditions

        queryConditions = addToQueryConditions(
            [], query_Dates, "query_Dates", "QueryTimeStamp"
        )
        queryConditions = addToQueryConditions(
            queryConditions, ex_Dates, "ex_Dates", "ExDate"
        )
        queryConditions = addToQueryConditions(
            queryConditions, strike, "strike", "Strike"
        )
        queryConditions = addToQueryConditions(
            queryConditions,
            days_to_expiration,
            "days_to_expiration",
            "DaysToExpiration",
        )
        queryConditions = addToQueryConditions(queryConditions, delta, "delta", "Delta")

        whereStr = ""
        if optionType is not None:
            queryConditions.append("Type==optionType")
            if whereStr == "":
                whereStr += "Type=optionType"
            else:
                whereStr += " & Type=optionType"

        indexCoordinates = self.storage.select_as_coordinates(
            underlyingSymbol, queryConditions
        )

        if columns is not None:
            if type(columns) != list:
                columns = [columns]

        if debug:
            print(whereStr)
        self.storage.open()
        if len(indexCoordinates) > 0:
            if columns:
                # Use both index and columns conditions
                optionData = self.storage.select(
                    underlyingSymbol, where=indexCoordinates, columns=columns
                )
            else:
                # Use only the index condition
                optionData = self.storage.select(
                    underlyingSymbol, where=indexCoordinates
                )
        else:
            if columns:
                # Use only the columns condition
                optionData = self.storage.select(underlyingSymbol, columns=columns)
            else:
                # Use none of the conditions
                optionData = self.storage.get(underlyingSymbol)
        self.storage.close()
        return optionData

    def populateTempDB(self):
        pass

    def getMinMaxDate(self):
        pass

    def getValidRows(self):
        pass

    def updateDB(self):
        return self.mineFidelityOptions(["ixSPX", "SPY", "IVV"])

    def updateStorageQueryColumns(self):
        self.storage.open()
        for tckr in self.storage.keys():
            print(tckr)
            self.storage.put(
                tckr,
                self.storage.get(tckr),
                format="table",
                data_columns=self.hd5QueryCols,
            )
        self.storage.close()

    # =========================================================================
    # *************************************************************************
    # ----------------- Begin Fidelity Web Scraping Functions -----------------
    # *************************************************************************
    # =========================================================================
    def mineFidelityOptions(
        self,
        symbols: Union[str, List[str]],
        loadInBatches: Optional[bool] = True,
        returnData: Optional[bool] = False,
        debug: Optional[bool] = False,
    ) -> Union[None, Dict[str, pd.DataFrame]]:
        output_total: Dict[str, pd.DataFrame] = {}
        if type(symbols) is not list:
            symbols = [symbols]

        for symbol in symbols:
            print("Mining Options for '%s'" % symbol)
            if returnData:
                output_total[symbol] = self.mineFidelityOptions_SingleStock(
                    symbol,
                    loadInBatches=loadInBatches,
                    returnData=returnData,
                    debug=debug,
                )
            else:
                self.mineFidelityOptions_SingleStock(
                    symbol,
                    loadInBatches=loadInBatches,
                    returnData=returnData,
                    debug=debug,
                )
                output_total = None
        self.closeDriver()
        return output_total

    def mineFidelityOptions_SingleStock(
        self,
        symbol: str,
        loadInBatches: Optional[bool] = False,
        returnData: Optional[bool] = False,
        debug: Optional[bool] = False,
    ):
        # noinspection PyArgumentList
        start = pd.Timestamp.today()
        symbol = symbol.upper()
        storageSymbol = symbol.replace(".", "ix")
        websymbol = ".SPY" if symbol == "ixSPX" else symbol
        # get the time of the query
        query_timestamp = pd.Timestamp(datetime.datetime.today())
        optiondata_total = self.formatFidelityOptionDataFrame(
            pd.DataFrame(columns=self.finalColumns)
        )
        col_headings = None
        call_put_transition = None
        if self.driver is None:
            if debug:  # having troubles with the headless implementation
                # BUG - Having problems with phantomJS not loading the
                # fidelity webpage. Switching to chromedriver in headless mode
                # self.driver = driver = webdriver.Chrome(
                #        executable_path=self.chromedriver)
                chrome_options = Options()
                chrome_options.add_argument("--headless")
                chrome_options.add_argument("--window-size=1920x1080")
                self.driver = driver = webdriver.Chrome(
                    executable_path=self.chromedriver, chrome_options=chrome_options
                )
            else:
                print(self.chromedriver)
                self.driver = driver = webdriver.Chrome(
                    executable_path=self.chromedriver
                )
            print("Loaded WebDriver")
        else:
            driver = self.driver

        exDateQty = self.init_option_webpage(driver, websymbol)
        # Cycle through expiration dates one at a time and harvest each
        #  set of data
        iExDate = 0
        while True:
            #            try:
            optionData, col_headings, call_put_transition = self.read_nth_option(
                iExDate,
                symbol,
                exDateQty,
                query_timestamp,
                driver,
                col_headings=col_headings,
                call_put_transition=call_put_transition,
            )
            if optionData is not None:
                optiondata_total = self.append_option_data(
                    optiondata_total, optionData, loadInBatches, storageSymbol
                )
            iExDate += 1
            if iExDate == exDateQty:
                break
        #            except Exception as ex:
        #                print("\t\t Failed mining %s Option %01i of %01i. " % (
        #                        symbol, iExDate+1, exDateQty)
        #                        + "Extending webDriver wait time and rerunning")
        #                print(ex)
        #                self.webdriver_delay = self.webdriver_delay+0.5
        #                exDateQty = self.init_option_webpage(driver, websymbol)

        # if load in batches is false, then we need to load the entire
        #  dataset at this point and then close the webdriver
        if not loadInBatches:
            self.storage.open()
            if "/" + storageSymbol in self.storage.keys():
                startIndex = self.storage.select_column(storageSymbol, "index").size
                optiondata_total = self.formatFidelityOptionDataFrame(
                    optiondata_total, startIndex=startIndex
                )
                self.storage.append(
                    storageSymbol,
                    optiondata_total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            else:
                optiondata_total = self.formatFidelityOptionDataFrame(
                    optiondata_total, startIndex=0
                )
                self.storage.put(
                    storageSymbol,
                    optiondata_total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            self.storage.close()
            if not returnData:
                optiondata_total = None
        else:
            if returnData:
                optiondata_total = self.loadFromDB(symbol, query_Dates=query_timestamp)
            else:
                optiondata_total = None
        end = datetime.datetime.today()
        print(
            "Completed fidelity option query for "
            + "'%s' in %s" % (symbol, str(end - start))
        )
        return optiondata_total

    def closeDriver(self):
        self.driver.quit()
        self.driver = None

    def init_option_webpage(self, driver, websymbol):
        optionURL = (
            "https://researchtools.fidelity.com//ftgw/mloptions//"
            + "goto//optionChain?priced=Y&symbol=%s" % websymbol
        )
        # wait 3 sec every time you change the page, b/c I was having problems
        # with it loading a page with wrong dates
        driver.get(optionURL)
        time.sleep(self.webdriver_delay)
        print(driver.title + ": " + websymbol)
        # check for a login link and click on the login link if it exists
        loginLink = driver.find_elements_by_xpath('//*[@id="chainPeak"]')
        if len(loginLink) > 0:
            print("\tNeed to Login")
            loginLink[0].click()
            time.sleep(self.webdriver_delay)

            # enter username on new page
            driver.find_elements_by_xpath('//*[@id="userId-input"]')[0].send_keys(
                "coryperk"
            )
            driver.find_elements_by_xpath('//*[@id="password"]')[0].send_keys(
                "Hebrews135"
            )
            # press the submit button
            submit_button = driver.find_elements_by_xpath('//*[@id="fs-login-button"]')[
                0
            ]
            submit_button.click()
            time.sleep(self.webdriver_delay)
            driver.get(optionURL)
            time.sleep(self.webdriver_delay)
        print("\tLogin Successful")

        # ensure that all dates are unselected
        exDateBarElement = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
        )
        selectedElements = exDateBarElement.find_elements_by_class_name(
            "selected-state"
        )
        if len(selectedElements) > 0:
            for expirationElement in selectedElements:
                expirationElement.click()
                time.sleep(self.webdriver_delay)
            # click on the apply settings button
            apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
            apply_button.click()
            time.sleep(self.webdriver_delay)
        # get the total number of expiration dates
        exDateQty = len(
            driver.find_elements_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
            )
        )
        return exDateQty

    def append_option_data(self, data_total, data_single, loadInBatches, storageSymbol):
        data_total = data_total.append(data_single)
        if loadInBatches:
            # If load in batches is true, then append each expiration
            #  date data to stored dataset and create a dataset if
            #  necessary
            self.storage.open()
            if "/" + storageSymbol in self.storage.keys():
                startIndex = self.storage.select_column(storageSymbol, "index").size
                data_single = self.formatFidelityOptionDataFrame(
                    data_single, startIndex=startIndex
                )
                self.storage.append(
                    storageSymbol,
                    data_single,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            else:
                data_single = self.formatFidelityOptionDataFrame(
                    data_single, startIndex=0
                )
                self.storage.put(
                    storageSymbol,
                    data_single,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            self.storage.close()

        return data_total

    def formatFidelityOptionDataFrame(self, optionData, startIndex=0):
        for col in optionData.columns:
            optionData[col] = optionData[col].astype(self.fidelity_dtypes[col])
        optionData.index = range(startIndex, startIndex + len(optionData.index))
        return optionData

    def read_nth_option(
        self,
        i_date,
        symbol,
        date_qty,
        query_timestamp,
        driver,
        col_headings=None,
        call_put_transition=None,
        debug=False,
    ):
        # select the current expiration date and apply the settings
        # reset the exDateBar
        exdate_bar_element = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
        )
        bar_length = len(
            driver.find_elements_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
            )
        )
        selected_elements = exdate_bar_element.find_elements_by_class_name(
            "selected-state"
        )
        if len(selected_elements) > 0:
            for expirationElement in selected_elements:
                expirationElement.click()

        # if there is an offset caused by invalid dates, find the offset
        index_offset = 1
        while True:
            button_date = pd.Timestamp(
                driver.find_element_by_xpath(
                    '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
                    + "/div/ul/li[%1i]" % index_offset
                ).get_attribute("value")
            )
            if button_date > query_timestamp:
                break
            index_offset += 1
        if i_date + index_offset > bar_length:
            return None, col_headings, call_put_transition
        date_toggle = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
            + "/div/ul/li[%1i]" % (i_date + index_offset)
        )
        date_toggle.click()
        time.sleep(self.webdriver_delay)
        # ensure that only the selected map is toggled
        selected_elements = exdate_bar_element.find_elements_by_class_name(
            "selected-state"
        )
        while True:
            if len(selected_elements) == 1:
                break
            for expirationElement in selected_elements:
                expirationElement.click()
            date_toggle.click()
            time.sleep(self.webdriver_delay)

        apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
        apply_button.click()
        time.sleep(self.webdriver_delay)
        # Initialize column headers and dataFrames
        if col_headings is None or call_put_transition is None:
            # Get table headers by looping through the TH values until
            #  one doesn't exist
            print("\tMining Column Names")
            col_headings = []
            col_header_element_list = driver.find_elements_by_xpath(
                '//*[@id="fullTable"]/thead[1]/tr/th'
            )
            for iCol in range(len(col_header_element_list)):
                col_headings.append(col_header_element_list[iCol].text)
            call_put_transition = col_headings.index("Strike")

        # get the price of the underlying symbol
        underlying_price = float(
            driver.find_elements_by_xpath('//*[@id="companyDetailsDiv"]/div/span[1]')[0]
            .text.replace("$", "")
            .replace(",", "")
        )

        # get the expiration date info from the first expiration date
        exdate_info = (
            driver.find_elements_by_class_name("mth0")[0]
            .get_attribute("name")
            .split(" ")
        )
        exdate = datetime.date(
            int("20" + exdate_info[2][1:]),
            self.monthAbbrToNumConversion[exdate_info[0]],
            int(exdate_info[1]),
        )
        if "(" in exdate_info[3]:
            days_to_expiration = int(exdate_info[3][(exdate_info[3].find("(") + 1) :])
        else:
            days_to_expiration = int(exdate_info[4][(exdate_info[4].find("(") + 1) :])
        # Get number of rows in table for the current exdate
        rowqty = driver.execute_script(
            "return document.getElementById"
            + '("fullTable").'
            + "getElementsByClassName("
            + "'mth0').length"
        )
        print(
            "\tMining %s expiration date " % symbol
            + "%01i of %01i: %s (%01i rows)"
            % (i_date + 1, date_qty, str(exdate), rowqty)
        )
        # Init new dataFrame for the expiration date
        optiondata = pd.DataFrame(index=range(rowqty * 2), columns=self.finalColumns)
        # Line prefix is = ['QueryDate','QueryTime','ExDate',
        #  'DaysToExpiration','UnderlyingSymbol','UnderlyingPrice',
        #  'Type','Strike']
        optiondata["QueryTimeStamp"] = np.array(
            [np.datetime64(query_timestamp)] * (rowqty * 2)
        ).astype(self.fidelity_dtypes["QueryTimeStamp"])
        optiondata["ExDate"] = np.array([np.datetime64(exdate)] * (rowqty * 2)).astype(
            self.fidelity_dtypes["ExDate"]
        )
        optiondata["DaysToExpiration"] = np.array(
            [days_to_expiration] * (rowqty * 2)
        ).astype(self.fidelity_dtypes["DaysToExpiration"])
        optiondata["UnderlyingSymbol"] = np.array([symbol] * (rowqty * 2)).astype(
            self.fidelity_dtypes["UnderlyingSymbol"]
        )
        optiondata["UnderlyingPrice"] = np.array(
            [float(str(underlying_price).replace("$", "").replace(",", ""))]
            * (rowqty * 2)
        ).astype(self.fidelity_dtypes["UnderlyingPrice"])
        setcall_bool_list = [True] * rowqty + [False] * rowqty
        setput_bool_list = [False] * rowqty + [True] * rowqty
        optiondata["Type"] = np.array(["Call"] * rowqty + ["Put"] * rowqty).astype(
            self.fidelity_dtypes["Type"]
        )
        optiondata = self.processSingleExDateOptionData_XMLParser(
            driver,
            optiondata,
            col_headings,
            call_put_transition,
            setcall_bool_list,
            setput_bool_list,
            debug=debug,
        )

        #        # unselect the current expiration date
        #        time.sleep(self.webdriver_delay)
        #        date_toggle = driver.find_element_by_xpath(
        #                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
        #                + '/div/ul/li[%1i]' % (i_date+1))
        #        date_toggle.click()
        return optiondata, col_headings, call_put_transition

    # noinspection PyUnusedLocal
    def processSingleExDateOptionData_XMLParser(
        self,
        driver: webdriver,
        optiondata: pd.DataFrame,
        header_names: List[str],
        call_put_transition: int,
        setCallBoolList: List[bool],
        setPutBoolList: List[bool],
        debug: Union[int, bool] = 0,
    ):
        table_tree = html.fromstring(
            driver.find_element_by_xpath('//*[@id="fullTable"]').get_attribute(
                "innerHTML"
            )
        )
        target_rows = int(len(optiondata.index) / 2)
        for iCol in range(len(header_names)):
            dataname = header_names[iCol]
            # Different columns have different structures that need to be
            #  navigated past the td html tag those that end in '/a' will
            #  also pick up a dummy value from the subheader of the table
            #  and so first_valid_row needs to be moved to 1 for only
            #  those columns
            xpath_suffix = ""
            if dataname in ["Symbol", "Bid", "Ask"]:
                xpath_suffix = "/a"
            elif dataname in ["Volume", "Open Int"]:
                xpath_suffix = "/div/span"

            # dtype determination
            datatype = str
            if dataname in [
                "Strike",
                "Last",
                "Change",
                "Bid",
                "Ask",
                "Imp Vol",
                "Delta",
                "Gamma",
                "Theta",
                "Vega",
                "Rho",
            ]:
                datatype = float
            elif dataname in ["Bid Size", "Ask Size", "Volume", "Open Int"]:
                datatype = int

            # start processing data

            # no valid data in action cell
            if dataname == "Action":
                continue

            # Get data as txt from column
            raw_column_text = table_tree.xpath(
                "//tbody/tr/td[%01i]%s/text()" % (iCol + 1, xpath_suffix)
            )
            colTextList = np.array(
                [x.strip().replace("$", "").replace(",", "") for x in raw_column_text],
                dtype=str,
            )
            first_valid_row = len(colTextList) - target_rows
            colTextList = colTextList[first_valid_row:]
            if dataname == "Strike":
                # Add the strike to the call and put columns along with the
                #  other information at the start of the line
                optiondata.loc[setCallBoolList, dataname] = colTextList.astype(float)
                optiondata.loc[setPutBoolList, dataname] = colTextList.astype(float)
            elif dataname in ["Imp Vol"]:
                # remove the percent sign and return a float
                colTextList = np.array([x[:-2] for x in colTextList], dtype="|S5")
                colTextList[colTextList == b""] = np.nan
                if iCol < call_put_transition:
                    optiondata.loc[setCallBoolList, dataname] = colTextList.astype(
                        float
                    )
                else:
                    optiondata.loc[setPutBoolList, dataname] = colTextList.astype(float)
            else:
                # add the data in the predetermined format such that empty
                #  cells are nan values for numeric columns
                if datatype in [int, float]:
                    colTextList = colTextList.astype("|S5")
                    colTextList[
                        np.logical_or(colTextList == b"", colTextList == b"--")
                    ] = np.nan
                if iCol < call_put_transition:
                    optiondata.loc[setCallBoolList, dataname] = colTextList.astype(
                        datatype
                    )
                else:
                    optiondata.loc[setPutBoolList, dataname] = colTextList.astype(
                        datatype
                    )

        # Need to convert the numeric columns back to numeric dtypes
        # optiondata[['Strike','Last','Bid','Ask','Change','Imp Vol',
        #  'Delta','Gamma','Theta','Vega','Rho']] = optiondata[['Strike',
        #  'Last','Bid','Ask','Change','Imp Vol','Delta','Gamma','Theta',
        #  'Vega','Rho']].apply(pd.to_numeric,downcast = 'float')
        # optiondata[['Bid Size','Ask Size','Volume','Open Int']] =
        #  optiondata[['Strike','Last','Bid','Ask','Change','Bid Size',
        #  'Ask Size','Volume','Open Int','Imp Vol','Delta','Gamma','Theta',
        #  'Vega','Rho']].apply(pd.to_numeric,downcast = 'integer')
        return self.formatFidelityOptionDataFrame(optiondata)

    def calculate_best_strategies(
        self,
        tckr,
        query_date,
        fid_data=None,
        predictionModel=None,
        n_mc=100,
        cum_cycles=100,
        max_risk=0.2,
        total_pf=100000,
        broker_fee=4.95,
        addtl_option_fee=0.12,
        return_all_data=True,
        source="storage",
    ):
        """
        date_selection determines how to select the query date to match to
            the option data. Options are:
                'Closest'   - select the closest QueryTimeStamp to
                              the query_date
                'Day_First' - select the first QueryTimeStamp
                              within the same day as query_date
                'Day_Last'  - select the last QueryTimeStamp
                              within the same day as query_date
                'Day_X'     - where X is an integer, select the Xth
                              date (starting at 0) within the same day
                              as query_date. Default to the last occurrence
                              if X is too large
        debug:
        tckr = '.SPX'
        query_date = pd.Timestamp(2018,1,30)
        predictionModel=None
        n_mc=100
        cum_cycles=100
        max_risk=0.2
        total_pf=100000
        broker_fee=4.95
        addtl_option_fee=.12
        source='storage'
        """
        from src.Positions import ComplexOption

        tckr = Util.convertTckrToUniversalFormat(tckr)
        if predictionModel is None:
            from src import MarketPredictions_Depreciated

            predictionModel = MarketPredictions_Depreciated.getDefaultPredictionModel()
        if fid_data is None:
            query_date = Util.formatDateInput(query_date)
            fid_data = self.options_for_date(tckr, query_date, method="closest")
        # remove exdate less than 5 days away
        all_data = self.summarize_option_data(
            fid_data.loc[fid_data.DaysToExpiration > 4, :], price_calc="weighted"
        )
        current_price = all_data.UnderlyingPrice.iloc[0]
        query_date = all_data.QueryTimeStamp.iloc[0]
        all_exdates = pd.DatetimeIndex(list(set(all_data.ExDate)))
        output = []
        for i_date in range(all_exdates.size):
            exdate = all_exdates[i_date]
            print("* " * 30)
            print(
                "Calculating Best Strategy for expiration date"
                + " %1i of %1i: %s"
                % (i_date, all_exdates.size, exdate.strftime("%m-%d-%Y"))
            )
            print("* " * 30)
            sub_DB = all_data.loc[
                all_data.ExDate == exdate, ["Symbol", "Type", "Strike", "Bid", "Ask"]
            ].drop_duplicates(subset="Symbol")
            select_call = sub_DB.Type == "Call"
            call_symbol = sub_DB.loc[select_call, "Symbol"].values
            call_strike = sub_DB.loc[select_call, "Strike"].values
            call_sell = sub_DB.loc[select_call, "Bid"].values
            call_buy = sub_DB.loc[select_call, "Ask"].values
            put_symbol = sub_DB.loc[~select_call, "Symbol"].values
            put_strike = sub_DB.loc[~select_call, "Strike"].values
            put_sell = sub_DB.loc[~select_call, "Bid"].values
            put_buy = sub_DB.loc[~select_call, "Ask"].values
            data, arbitrage = ComplexOption.run_all_strategies_single(
                tckr,
                query_date,
                exdate,
                call_symbol,
                call_strike,
                call_sell,
                call_buy,
                put_symbol,
                put_strike,
                put_sell,
                put_buy,
                predictionModel=predictionModel,
                optionDB=self,
                current_price=current_price,
                n_mc=n_mc,
                cum_cycles=cum_cycles,
                max_risk=max_risk,
                total_pf=total_pf,
                broker_fee=broker_fee,
                addtl_option_fee=addtl_option_fee,
                return_all_data=return_all_data,
                source=source,
            )
            output.append(data)

        output = pd.concat(output, axis=0, ignore_index=True)
        output.sort_values("Avg_PF_Return_PerDay", ascending=False, inplace=True)
        output.index = range(len(output.index))
        column_order = [
            "Strategy",
            "Category",
            "Outcome_Qty",
            "Yield_Cycles",
            "PurchaseDate",
            "ExpirationDate",
            "DaysToExp",
            "DaysInvested",
            "Max_Risk",
            "CurrentPrice",
            "Prediction_Price",
            "Prediction_Std",
            "Total_PF",
            "Trade_Fee",
            "Addtl_Contract_Fee",
            "Strike_0",
            "Strike_1",
            "Strike_2",
            "Strike_3",
            "Price_0",
            "Price_1",
            "Price_2",
            "Price_3",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Median_PF_Return",
            "Mean_PF_Return",
            "Std_PF_Return",
            "Avg_PF_Return_PerDay",
            "Avg_Ann_Return",
            "Expected_Stock_Return",
            "AvgReturnVsStock",
            "Expected_Gain_Return",
            "Win_Percentage",
            "Ann_Volatility",
        ]
        output = output.loc[:, column_order]

        return output

    def test_mc_predictions(
        self,
        tckr="ixSPX",
        predictionModel=None,
        n_mc=100,
        cum_cycles=100,
        max_risk=0.2,
        total_pf=100000,
        broker_fee=4.95,
        addtl_option_fee=0.12,
        return_all_data=True,
        source="storage",
    ):
        """
        run the default prediction model against all the fidelity options
        that have been pulled and test the monte carlo algorithm's accuracy/
        predictability

        debug:
        tckr = 'ixSPX'
        predictionModel=None
        n_mc=100
        cum_cycles=100
        max_risk=0.2
        total_pf=100000
        broker_fee=4.95
        addtl_option_fee=0.12
        return_all_data=True
        source='storage'
        """
        from src.Positions import ComplexOption

        if predictionModel is None:
            from src import MarketPredictions_Depreciated

            predictionModel = MarketPredictions_Depreciated.getDefaultPredictionModel()
        str_cols = ["Strategy", "Category"]
        date_cols = ["PurchaseDate", "ExpirationDate"]
        num_cols = [
            "Outcome_Qty",
            "Yield_Cycles",
            "DaysToExp",
            "DaysInvested",
            "Max_Risk",
            "CurrentPrice",
            "Prediction_Price",
            "Prediction_Std",
            "Total_PF",
            "Trade_Fee",
            "Addtl_Contract_Fee",
            "Strike_0",
            "Strike_1",
            "Strike_2",
            "Strike_3",
            "Price_0",
            "Price_1",
            "Price_2",
            "Price_3",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Median_PF_Return",
            "Mean_PF_Return",
            "Std_PF_Return",
            "Avg_PF_Return_PerDay",
            "Avg_Ann_Return",
            "Expected_Stock_Return",
            "AvgReturnVsStock",
            "Expected_Gain_Return",
            "Win_Percentage",
            "Ann_Volatility",
        ]
        final_cols = str_cols + date_cols + num_cols
        self.storage.open()
        unique_query_dates = pd.DatetimeIndex(
            list(set(self.storage.select_column("/" + tckr, "QueryTimeStamp")))
        )
        self.storage.close()
        final_df = []
        for i_query in range(unique_query_dates.size):
            query_date = unique_query_dates[i_query]
            print("\n" + "* " * 30)
            print(
                "Calculating Best Strategies for query date"
                + " %1i of %1i: %s"
                % (i_query, unique_query_dates.size, query_date.strftime("%m-%d-%Y"))
            )
            print("* " * 30)
            fid_data = self.options_for_date(
                tckr, query_date, method="closest"
            ).drop_duplicates()
            fid_data = fid_data.loc[fid_data.DaysToExpiration > 4, :]
            if fid_data.size == 0:
                continue
            fid_data = self.summarize_option_data(fid_data, price_calc="weighted")
            current_price = fid_data.UnderlyingPrice.iloc[0]
            unique_exdates = pd.DatetimeIndex(list(set(fid_data.ExDate)))
            # noinspection PyArgumentList
            unique_exdates = unique_exdates[unique_exdates < pd.Timestamp.today()]
            for i_ex in range(unique_exdates.size):
                exDate = unique_exdates[i_ex]
                sub_DB = fid_data.loc[
                    fid_data.ExDate == exDate,
                    ["Symbol", "Type", "Strike", "Bid", "Ask"],
                ].drop_duplicates(subset="Symbol")
                select_call = sub_DB.Type == "Call"
                call_strike = sub_DB.loc[select_call, "Strike"].values
                call_sell = sub_DB.loc[select_call, "Bid"].values
                call_buy = sub_DB.loc[select_call, "Ask"].values
                put_strike = sub_DB.loc[~select_call, "Strike"].values
                put_sell = sub_DB.loc[~select_call, "Bid"].values
                put_buy = sub_DB.loc[~select_call, "Ask"].values
                print(
                    "EXDate %1i of %1i: %s (%1i strikes)"
                    % (
                        i_ex,
                        unique_exdates.size,
                        exDate.strftime("%m-%d-%Y"),
                        call_strike.size,
                    )
                )
                print(" - " * 20)

                strategy_spread = ComplexOption.run_all_strategies_single(
                    tckr,
                    query_date,
                    exDate,
                    "todo:need to figure out",
                    call_strike,
                    call_sell,
                    call_buy,
                    "todo:need to figure out",
                    put_strike,
                    put_sell,
                    put_buy,
                    predictionModel=predictionModel,
                    optionDB=self,
                    current_price=current_price,
                    n_mc=n_mc,
                    cum_cycles=cum_cycles,
                    max_risk=max_risk,
                    total_pf=total_pf,
                    broker_fee=broker_fee,
                    addtl_option_fee=addtl_option_fee,
                    return_all_data=return_all_data,
                    source=source,
                )
                final_df.append(strategy_spread.loc[:, final_cols])

        final_df = pd.concat(final_df, axis=0, ignore_index=True)
        final_df[date_cols] = final_df[date_cols].apply(pd.to_datetime)
        final_df[num_cols] = final_df[num_cols].apply(pd.to_numeric)
        unique_exdates = pd.DatetimeIndex(list(set(final_df.ExpirationDate)))
        ex_prices = predictionModel.getCurrentPrice(unique_exdates, tckr, source=source)
        ex_prices.name = "Expiration_Price"
        final_df = pd.merge(
            final_df,
            pd.DataFrame(ex_prices),
            how="left",
            left_on="ExpirationDate",
            right_index=True,
        )
        final_df["Tckr_Gain"] = final_df.Expiration_Price / final_df.CurrentPrice
        final_df["Actual_Value"] = np.nan
        final_df["Actual_Gain"] = np.nan
        co = ComplexOption.ComplexOption
        for strategy in list(set(final_df.Strategy)):
            select_rows = final_df.Strategy == strategy
            strategy_data = final_df.loc[select_rows, :]
            positions, cashflow_sign, quantity = co.interpret_strategy(strategy)
            legs = len(positions)
            strategy_ex_price = strategy_data.Expiration_Price
            strike_cols = ["Strike_0", "Strike_1", "Strike_2", "Strike_3"][:legs]
            strategy_strikes = strategy_data[strike_cols]
            strategy_premium = strategy_data.Premium
            actual_value = co.calc_value_expiration(
                strategy_ex_price,
                strategy_strikes,
                premium=strategy_premium,
                positions=positions,
                cashflow_sign=cashflow_sign,
                quantity=quantity,
            )
            actual_gain = actual_value / np.abs(strategy_data.Max_Loss.values).reshape(
                actual_value.size, 1
            )
            final_df.loc[select_rows, "Actual_Value"] = actual_value
            final_df.loc[select_rows, "Actual_Gain"] = actual_gain
        correlation = final_df.corr().sort_values("Actual_Gain")["Actual_Gain"]
        print(correlation)
        return final_df, correlation

    # =======================================================================
    # ***********************************************************************
    # -------------------- Begin Fidelity Database Access -------------------
    # ***********************************************************************
    # =======================================================================

    def closest_query_date(self, dbKey, query_date):
        """ OPTIMIZE - Can optimize by cleaning up the file open and
        close functionality"""
        self.storage.open()
        db_dates = pd.DatetimeIndex(
            list(set(self.storage.select_column("/" + dbKey, "QueryTimeStamp")))
        )
        self.storage.close()
        if type(query_date) in [
            pd.DatetimeIndex,
            pd.Index,
            list,
            set,
            np.ndarray,
            pd.Series,
        ]:
            query_date = Util.formatDateArray(
                [
                    db_dates[np.abs(db_dates - date).argmin()]
                    for date in Util.formatDateInput(query_date)
                ]
            )
        else:
            query_date = db_dates[
                np.abs(db_dates - Util.formatDateInput(query_date)).argmin()
            ]
        return query_date

    # noinspection PyUnusedLocal
    def options_for_date(self, dbKey, query_date, method="closest"):
        """
        OPTIMIZE - Can optimize by cleaning up the file open and
        close functionality
        'method' determines how to select the query_date to match to
            the option data. Options are:
                'Exact'     - must be an exact match to a record in the DB
                'Closest'   - select the closest QueryTimeStamp to
                              the query_date
                'Day_First' - select the first QueryTimeStamp
                              within the same day as query_date
                'Day_Last'  - select the last QueryTimeStamp
                              within the same day as query_date
                'Day_X'     - where X is an integer, select the Xth
                              date (starting at 0) within the same day
                              as query_date. Default to the last occurrence
                              if X is too large
        """
        dbKey = Util.convertTckrToUniversalFormat(dbKey)
        query_date = Util.formatDateArray(query_date)
        method = method.upper()
        if method.upper() == "EXACT":
            pass
        elif method.upper() == "CLOSEST":
            query_date = self.closest_query_date(dbKey, query_date)
        elif "DAY" in method:
            if not self.storage.is_open:
                self.storage.open()
            db_dates = pd.DatetimeIndex(
                list(set(self.storage.select_column("/" + dbKey, "QueryTimeStamp")))
            )
            if "FIRST" in method:
                index_select = 0
            elif "LAST" in method:
                index_select = -1
            elif "_" in method and method.split("_")[-1].isdigit():
                index_select = int(method.split("_")[-1])
            else:
                index_select = None
            query_date = db_dates[np.isin(db_dates.date, query_date.date)]
            if index_select is not None:
                query_date = query_date[[min(query_date.size, index_select)]]

        if not self.storage.is_open:
            self.storage.open()
        fid_options = self.storage.select(
            "/" + dbKey, where="QueryTimeStamp = query_date"
        )
        self.storage.close()
        return fid_options

    @staticmethod
    def summarize_option_data(fid_options, price_calc="weighted"):
        """
        reduces some of the information stored in the database and calculates
        a price estimation based on the weighted averaged of the bid ask
        spread
        """
        final_columns = [
            "QueryTimeStamp",
            "Symbol",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "UnderlyingPrice",
            "Type",
            "Strike",
            "Price",
            "Bid",
            "Ask",
            "Imp Vol",
        ]
        if price_calc.lower() == "weighted":
            fid_options.loc[fid_options.loc[:, "Bid Size"] == 0, "Bid Size"] = 1
            fid_options.loc[fid_options.loc[:, "Ask Size"] == 0, "Ask Size"] = 1
            fid_options.loc[:, "Price"] = (
                fid_options["Bid"] * fid_options["Bid Size"]
                + fid_options["Ask"] * fid_options["Ask Size"]
            ) / (fid_options["Bid Size"] + fid_options["Ask Size"])
        if price_calc.lower() == "middle":
            fid_options.loc[:, "Price"] = (fid_options["Bid"] + fid_options["Ask"]) / 2
        return fid_options[final_columns]


def main():
    print("DataSource\\OptionData.py")


if __name__ == "__main__":
    main()
