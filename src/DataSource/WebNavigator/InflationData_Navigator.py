import pandas as pd
from selenium import webdriver
from typing import Optional
from .SeleniumNavigator import SeleniumNavigator


# noinspection PyPep8Naming
class InflationData_SeleniumNavigator(SeleniumNavigator):
    def __init__(self, existing_driver=None):
        SeleniumNavigator.__init__(self, existing_driver)
        self.inflation_url = (
            "https://inflationdata.com/Inflation/"
            + "Consumer_Price_Index/HistoricalCPI.aspx"
        )

    def scrape_CPI_data(self, debug: bool = False) -> pd.DataFrame:
        self.start_driver(debug)
        self.driver.get(self.inflation_url)
        print(self.driver.title)
        return pd.read_html(
            self.driver.find_element_by_xpath('//*[@id="form1"]/div[3]').get_attribute(
                "innerHTML"
            ),
            header=0,
        )[0]
