import os
import requests
from typing import Optional, Dict
from config import default_settings


class RequestsNavigator(object):
    def __init__(
        self, workProxy: Optional[bool] = None, proxies: Optional[Dict[str, str]] = None
    ):
        workProxy: bool = workProxy if workProxy is not None else default_settings.work_proxy
        proxies: Dict[
            str, str
        ] = proxies if proxies is not None else default_settings.proxies
        self.proxies: Dict[str, str] = {}
        if workProxy:
            if proxies is None:
                self.proxies = {
                    "http": "http://webproxy.ext.ti.com:80/",
                    "https": "https://webproxy.ext.ti.com:80",
                    "ftp": "http://webproxy.ext.ti.com:80",
                }
            else:
                self.proxies = proxies
            os.environ["HTTPS_PROXY"] = self.proxies["https"]

    def create_web_session(self) -> requests.Session:
        s = requests.Session()
        s.proxies = self.proxies
        return s
