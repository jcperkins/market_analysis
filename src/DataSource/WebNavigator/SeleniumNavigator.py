from selenium import webdriver
from typing import Optional
from types import ModuleType
from config import default_settings


class SeleniumNavigator(object):
    def __init__(self, existing_driver: Optional[ModuleType] = None):
        self.chromedriver: str = default_settings.chromedriver
        self.phantomJSDriver: str = default_settings.phantomJSDriver
        self.driver: Optional[ModuleType] = existing_driver

    def start_driver(self, debug: bool = False) -> webdriver:
        if self.driver is None:
            if debug:
                self.driver = webdriver.Chrome(executable_path=self.chromedriver)
            else:
                self.driver = webdriver.Chrome(executable_path=self.chromedriver)
                # self.driver = driver = webdriver.PhantomJS(
                #         executable_path=self.phantomJSDriver,
                #         service_log_path=os.path.devnull)
                # driver.set_window_size(1120, 550)
        return self.driver

    def close_driver(self) -> None:
        self.driver.close()
        self.driver = None
        return
