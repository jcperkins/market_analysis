import datetime
import sys
import requests
import pandas as pd
import numpy as np
from io import StringIO
from .RequestsNavigator import RequestsNavigator
from ..Mixins.StockCalcMixin import StockCalcMixin
import src.Utility as Util
from typing import Optional, Union, Dict
from config import default_settings


class YahooStockNavigator(RequestsNavigator, StockCalcMixin):
    def __init__(
        self,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
        cookieValue: Optional[str] = None,
        crumbValue: Optional[str] = None,
    ):
        RequestsNavigator.__init__(self, workProxy=workProxy, proxies=proxies)
        self.cookieValue: str = cookieValue if cookieValue is not None else default_settings.cookieValue
        self.crumbValue: str = crumbValue if crumbValue is not None else default_settings.crumbValue

    def queryYahooStockData(
        self,
        symbol: str,
        start_date: Optional[pd.Timestamp] = None,
        end_date: Optional[pd.Timestamp] = None,
        initial_adj_price: Optional[Union[float, int]] = None,
        s: Optional[requests.Session] = None,
    ) -> pd.DataFrame:
        """
        loads stock data from the yahoo database
            o  start_date will default to 1990-1-1 and is inclusive
            o  end_date will default to the day of the function call and
                is exclusive (will not include data for the actual end_date)
        """
        symbol = Util.convertTckrToYahooFmt(symbol)
        if s is None:
            s = self.create_web_session()
        # date processing
        start_date = (
            Util.formatDateInput(start_date) if start_date else pd.Timestamp(1990, 1, 1)
        )  # type: ignore
        end_date = (
            Util.formatDateInput(end_date)
            if end_date
            else pd.Timestamp(datetime.date.today() - datetime.timedelta(days=1))
        )  # type: ignore
        # bring start date and end date to the same format as yahoo by
        #  setting the time to 6:00 AM
        timestamp_limits = pd.DatetimeIndex(
            [start_date.date(), end_date.date()]
        ) + pd.Timedelta(hours=6)
        timestamp_limits = timestamp_limits.view("int64") / pd.Timedelta(
            1, unit="s"
        ).view("int64")
        start_date_timestamp, end_date_timestamp = timestamp_limits

        # get pricedata
        event = "history"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "interval=1d&events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = s.get(url_string, cookies={"B": self.cookieValue})
        print(
            "Querying Yahoo Data for %s from %s to %s" % (symbol, start_date, end_date)
        )
        raw_table: str = response.content.decode(sys.stdout.encoding)
        priceData = pd.read_csv(
            StringIO(raw_table),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        newColNames = list(priceData.columns)
        newColNames[newColNames.index("Adj Close")] = "AdjClose"
        priceData.columns = newColNames
        # Get dividend data
        event = "div"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "interval=1d&events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = s.get(url_string, cookies={"B": self.cookieValue})
        raw_table = response.content.decode(sys.stdout.encoding)
        eventData = pd.read_csv(
            StringIO(raw_table),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        if len(eventData.index):
            priceData = pd.concat(
                [priceData, eventData], axis=1, join_axes=[priceData.index]
            )
            # Set nan dividend values to 0
            priceData.loc[np.isnan(priceData.Dividends), "Dividends"] = 0
        else:
            priceData["Dividends"] = 0.0
        # Get stock Split
        event = "split"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&interval=1d&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = s.get(url_string, cookies={"B": self.cookieValue})
        raw_table = response.content.decode(sys.stdout.encoding)
        eventData = pd.read_csv(
            StringIO(raw_table),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        if len(eventData.index):
            # Convert text mult. factor to a float
            eventData["SplitFactor"] = eventData.apply(
                lambda row: self.formatYahooStockSplitFactor(row["Stock Splits"]),
                axis=1,
            )
            priceData = pd.concat(
                [priceData, eventData["SplitFactor"]],
                axis=1,
                join_axes=[priceData.index],
            )
            # Set nan split factor values to 0
            priceData.loc[np.isnan(priceData.SplitFactor), "SplitFactor"] = 1.0
        else:
            priceData["SplitFactor"] = 1.0
        # Calculate adj column - assume dividends are reinvested
        priceData = self.formatStockDataFrame(priceData)
        priceData["AdjClose"] = self.adjustedPrice(
            priceData, initialAdjPrice=initial_adj_price
        )
        return priceData[~priceData.index.duplicated(keep="first")]

    @staticmethod
    def formatYahooStockSplitFactor(text: str):
        """
        takes the text format of the stock split info returned by yahoo
        and converts it into a float number. Result is the multiplicative
        factor that should be applied to the # of shares owned. """
        numerator, denominator = text.split("/")
        return float(numerator) / float(denominator)
