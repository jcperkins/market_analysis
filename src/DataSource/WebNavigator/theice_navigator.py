import pandas as pd
from selenium import webdriver


class TheICE_SeleniumNavigator(object):
    def __init__(self, web_driver: webdriver, webdriver_delay: float = 0.005):
        # TempDB has the same format as the hd5 table except that it has
        #  only the relevant data for the dates
        self.driver = webdriver

    def go_to_startpage(self):
        self.driver.get("https://www.theice.com/marketdata/reports/170")
        self.checkWebPageIntegrity()
        return self

    def checkWebPageIntegrity(self) -> bool:
        self.check_for_recaptcha()
        return self.check_cookie_disclaimer()

    def check_for_recaptcha(self) -> bool:
        reCaptchaForms = self.driver.find_elements_by_id("reportCenterRecaptchaForm")
        if len(reCaptchaForms) > 0:
            pd.user_input(
                "User needs to get the program through the "
                + "reCAPTCHA check. Press enter on the module "
                + "to continue..."
            )
            # look for reCaptcha prompts again for the final
            reCaptchaForms = self.driver.find_elements_by_id(
                "reportCenterRecaptchaForm"
            )

        return len(reCaptchaForms) == 0

    def check_cookie_disclaimer(self) -> bool:
        cookie_buttons = self.driver.find_elements_by_id("allow-cookie-button")
        if len(cookie_buttons) > 0:
            cookie_buttons[0].click()
            cookie_buttons = self.driver.find_elements_by_id("allow-cookie-button")

        return len(cookie_buttons) == 0

    def goto_rates_for_date(self, querydate: pd.Timestamp) -> bool:
        url_string = (
            "https://www.theice.com/marketdata/reports"
            + "/icebenchmarkadmin/ICELiborHistoricalRates.shtml"
            + "?criteria.reportDate="
            + "%01i/%01i/%02i&criteria.currencyCode=USD"
            % (querydate.month, querydate.day, querydate.year % 100)
        )
        self.driver.get(url_string)

        return True
