from lxml import html
import requests
import pandas as pd
import numpy as np
from .RequestsNavigator import RequestsNavigator
from typing import List, Optional, Union, Dict


# noinspection PyPep8Naming
class WSJ_Navigator(RequestsNavigator):
    libor_cols: List[str] = ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]

    def __init__(
        self, workProxy: Optional[bool] = None, proxies: Optional[Dict[str, str]] = None
    ):
        RequestsNavigator.__init__(self, workProxy=workProxy, proxies=proxies)

    def scrape_libor_data(
        self, querydate: pd.Timestamp, s: Optional[requests.Session] = None
    ) -> pd.Series:
        """
        gets the libor data from wsj.com for the given date
        and returns a pandas series in the same format as the prior selenium version such
        that the index is ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"] and the name is the timestamp
        """
        print(f"Getting LIBOR rates for {querydate.date()}")
        expected_rows: List[str] = [
            "Libor Overnight",
            "Libor 1 Week",
            "Libor 1 Month",
            "Libor 2 Month",
            "Libor 3 Month",
            "Libor 6 Month",
            "Libor 1 Year",
        ]
        row_offset = 5
        rate_info = pd.Series(
            name=querydate, index=self.libor_cols, dtype=np.dtype(float)
        )

        url_str = (
            f"http://wsj.com/mdc/public/page/2_3020-libor-"
            f"{querydate.year:04}{querydate.month:02}{querydate.day:02}"
            f".html?mod=mdc_pastcalendar"
        )
        page = requests.get(url_str) if s is None else s.get(url_str)
        tree = html.fromstring(page.content)
        table_list = tree.xpath('//*[@id="column0"]/div/table')
        if len(table_list) == 0:
            # if not table then return an empty Series
            return rate_info
        else:
            # not elegant, but diving into the table then the body is the only way I could get it to work
            tbody = table_list[0].xpath("//tbody")[0]
            for i in range(len(expected_rows)):
                assert (
                    tbody.xpath(f"//tr[{row_offset + i}]/td[1]/text()")[0]
                    == expected_rows[i]
                )
                rate_info[self.libor_cols[i]] = float(
                    tbody.xpath(f"//tr[{row_offset + i}]/td[2]/text()")[0]
                )

            return rate_info

    def queryLiborRates(
        self, dates: Union[pd.Timestamp, pd.DatetimeIndex]
    ) -> pd.DataFrame:
        # for performance so connection doesn't have to be recreated
        s = self.create_web_session()
        if isinstance(dates, pd.Timestamp):
            dates = pd.DatetimeIndex(dates)
        rate_table = pd.concat(
            [self.scrape_libor_data(x, s) for x in dates], axis=1, sort=True
        ).T
        return rate_table[self.libor_cols].fillna(method="ffill")
