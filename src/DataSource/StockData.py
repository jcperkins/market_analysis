# -----------------------------------------------------------------------------
# Name:        MarketData
# Purpose:      Database management class for accessing all types of dat
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import datetime
from typing import Dict, List, Optional, Tuple, TypeVar, Union, overload

import numpy as np
import pandas as pd
import src.Utility as Util
from src.types import (
    RawDateCollection,
    RawDateSingle,
    RawDateType,
    Scalar,
    TorArray,
    TorList,
)

from .Mixins import BaseStorageDBMixin, StockTempDBMixin
from .WebNavigator import YahooStockNavigator

# for possible future implemtations with quandl
# import quandl
# quandl.ApiConfig.api_key = "CwCravWAoyv4zVzb5U-M"

TStockData = TypeVar("TStockData", bound="StockData")


class StockData(
    Util.metaclass_resolver(YahooStockNavigator, StockTempDBMixin, BaseStorageDBMixin)
):
    """
    Historical Price Database on my local machine

    This class is full of helper methods for getting data from the sqlite3 database in
    pandas DataFrame and Series formats

    todo: for fundamentals look up intrinio
    """

    # noinspection PyCallByClass
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
        cookieValue: Optional[str] = None,
        crumbValue: Optional[str] = None,
    ):
        YahooStockNavigator.__init__(
            self,
            workProxy=workProxy,
            proxies=proxies,
            cookieValue=cookieValue,
            crumbValue=crumbValue,
        )
        StockTempDBMixin.__init__(self, None)
        BaseStorageDBMixin.__init__(self, "TckrData", fileDir=fileDir)
        #        if the cookie/crumb combination ever expires you can refresh them by:
        #            1. use Chrome with the developer tools on to access a yahoo
        #               finance spreadsheet.
        #            2. the crumb value is contained in the url to get the value
        #            3. the cookie value is a cookie set on your browser under the
        #               name 'B'
        #            note: So far, I've only had to change the crumb value
        self.default_key = "ixSPX"
        # common tckr look-up lists
        # todo: these should really be stored in a look-up table somewhere
        self.commonTckrVariations: Dict[str, str] = {
            ".VIX": "^VIX",
            "$VIX": "^VIX",
            ".VXV": "^VXV",
            "$VXV": "^VXV",
        }
        self.no_update_keys: List[str] = ["SPXLFAKE", "XIV"]

        self.hdf_index_columns: List[str] = [
            "Close",
            "AdjClose",
            "Volume",
            "Dividends",
            "SplitFactor",
        ]
        self.data_types: Dict[str, type] = {
            "Open": float,
            "High": float,
            "Low": float,
            "Close": float,
            "AdjClose": float,
            "Volume": float,
            "Dividends": float,
            "SplitFactor": float,
        }
        # properties about time of day context
        market_close = datetime.time(15, 30)
        self.simulationStartDate = pd.Timestamp(1990, 1, 1)
        # noinspection PyArgumentList
        now = pd.Timestamp.today()
        self.lastHistoricalDate = pd.Timestamp(now.date())
        if now.time() < market_close:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        # Make sure that the last date is one that has a chance of the
        #  markets being open
        while self.lastHistoricalDate.weekday() > 4:
            self.lastHistoricalDate = self.lastHistoricalDate - pd.Timedelta(days=1)

    def initStockData(self, tckr: str, silent: bool = False) -> bool:
        """
        create a page in the stockDB storage object for the provided tckr

        :param tckr: string of the tckr to query the historical data for
        :param silent: boolean to control whether the print statements are get to the screen
        :return: a boolean indicating the success of the function
        """
        tckr = Util.convertTckrToUniversalFormat(tckr)
        if not silent:
            print("Initializing %s data" % tckr)
        data = self.queryYahooStockData(tckr, start_date=self.simulationStartDate)
        if len(data.index) > 0:
            self.storage.open()
            data = self.formatStockDataFrame(data)
            self.storage.put(
                tckr, data, format="table", data_columns=self.hdf_index_columns
            )
            self.storage.close()
        self.keyList.append(tckr)
        return True

    def populateTempDB(
        self,
        tckrs: Union[str, List[str]] = "SPX",
        start: Optional[RawDateType] = None,
        end: Optional[Union[str, RawDateSingle]] = None,
        data_cols: Optional[TorList[str]] = None,
        replaceDB: bool = True,
    ) -> Dict[str, pd.DataFrame]:
        """
        function that calls 'loadFromDB' and stores the result in the internal temp_db so
        that future data requests are faster. The resulting

        :param tckrs: string or list of strings that represent tckrs to get data for
        :param start: the first date to pull data for
        :param end: the last date to collect the data for
        :param data_cols: the column names in the storage object that are of interest
        :param replaceDB: boolean that controls whether the internal temp_db property is overwritten
        :return: dictionary that is organized so that each type of data
                (i.e. close, open, etc.) serves as the keys and each column
                of the dataframe is a tckr
        """
        if isinstance(data_cols, str):
            data_cols = [data_cols]
        elif data_cols is None or len(data_cols) == 0:
            data_cols = ["Close", "AdjClose", "Dividends", "SplitFactor"]
        if start is None:
            start = pd.Timestamp("1990-1-1")
        if end is None:
            # noinspection PyArgumentList
            end = pd.Timestamp.today()
        if isinstance(tckrs, str):
            tckrs = [Util.convertTckrToUniversalFormat(tckrs)]
        else:
            tckrs = [Util.convertTckrToUniversalFormat(x) for x in tckrs]
        self.tempdb_keys = data_cols
        self.tempdb_cols = tckrs
        panelData: Dict[str, pd.DataFrame] = {}
        for tckr in tckrs:
            tckrData = self.loadFromDB(
                tckr,
                Util.formatDateInput(start),
                Util.formatDateInput(end),
                columns=data_cols,
                source="storage",
            )
            for column in tckrData.columns:
                if column not in panelData.keys():
                    panelData[column] = pd.DataFrame(columns=tckrs)
                panelData[column][tckr] = tckrData[column]
        # fill missing values
        for col_name in panelData.keys():
            panelData[col_name] = panelData[col_name].fillna(method="ffill")
        if replaceDB or self.tempDB is None:
            self.tempDB = panelData
        else:
            for dfName in panelData.keys():
                if dfName in self.tempDB.keys():
                    # Merge new rows by collecting the tckrs that are not\
                    #  already included and then merging only that data
                    newTckrs = []
                    for tckr in panelData[dfName].columns:
                        if tckr not in self.tempDB[dfName].columns:
                            newTckrs.append(tckr)
                    self.tempDB[dfName] = pd.merge(
                        self.tempDB[dfName],
                        panelData[dfName][newTckrs],
                        how="left",
                        left_index=True,
                        right_index=True,
                    )

        # clean volatility columns
        if "ixVIX" in tckrs and "ixVXV" in tckrs:
            selectNANs = np.isnan(self.tempDB["Close"]["ixVXV"])
            self.tempDB["Close"].loc[selectNANs, ["ixVXV"]] = self.tempDB["Close"].loc[
                selectNANs, ["ixVIX"]
            ]
        return self.tempDB

    # if start is None, returns np.nan
    @overload
    def loadFromDB(
        self,
        dbKey: str,
        start: None,
        end: Optional[Union[str, RawDateSingle]],
        columns: Optional[List[str]],
        source: Optional[str],
    ) -> float:
        ...

    # returns scalar if start is a single date, end is none and columns is a string
    @overload
    def loadFromDB(
        self,
        dbKey: str,
        start: RawDateSingle,
        end: None,
        columns: str,
        source: Optional[str],
    ) -> Scalar:
        ...

    # returns series if start is a single date, end is none and columns is a list
    @overload
    def loadFromDB(
        self,
        dbKey: str,
        start: RawDateSingle,
        end: None,
        columns: Optional[List[str]],
        source: Optional[str],
    ) -> pd.Series:
        ...

    # if columns is a a string and start and end are single dates
    # returns scalar if start == end
    # returns series if start != end
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateSingle = None,
        end: RawDateSingle = None,
        columns: str = None,
        source: Optional[str] = "storage",
    ) -> Union[str, float, int, pd.Timestamp, np.datetime64, pd.Series]:
        ...

    # returns series if start is datetimeIndex and column is a string (regardless of end)
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateCollection = None,
        end: Optional[RawDateSingle] = None,
        columns: str = None,
        source: Optional[str] = "storage",
    ) -> pd.Series:
        ...

    # returns dataframe if start is datetimeIndex and column is a list or None (regardless of end)
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateCollection = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[List[str]] = None,
        source: Optional[str] = "storage",
    ) -> pd.DataFrame:
        ...

    def loadFromDB(
        self,
        dbKey: str = "",
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[TorList[str]] = None,
        source: str = "storage",
    ) -> Union[Scalar, pd.DataFrame, pd.Series]:
        """
        queries from the storage DB or the temp_db depending on source indicated
            Input conditions:
                o  start can be a single date or a
                    (list,array,pd.DatetimeIndex) of dates. If it is a list,
                    then only the matching dates will be returned
            returns a pd.DataFrame unless columns is a string and the
                name of a column, then it returns a pd.Series
        """
        if start is pd.NaT or start is None:
            return np.nan

        start = Util.formatDateInput(start)
        # todo: add functionality for end to be a string like in InflationData
        end = Util.formatDateInput(end)
        if source.upper() != "STORAGE":
            data = self.loadFromDB_tempDB(
                tckr=dbKey, start=start, end=end, columns=columns
            )
        else:
            data = self.loadFromDB_storage(
                dbKey=dbKey, start=start, end=end, columns=columns
            )

        # data should always be a DF at this point
        if type(columns) == str:
            if (
                (not isinstance(start, pd.DatetimeIndex))
                and (end is None or start == end)
                and (start in data.index)
            ):
                return data.loc[start, columns]  # returns a scalar
            else:
                return data.loc[:, columns]  # returns a series
        else:
            if (
                (type(start) != pd.DatetimeIndex)
                and (end is None or start == end)
                and (start in data.index)
            ):
                return data.loc[start, :]  # returns a series
            else:
                return data

    def getMinMaxDate(
        self, dbKey: str = "", source: str = "STORAGE"
    ) -> Tuple[pd.Timestamp, pd.Timestamp]:
        """
        return the first and last date in the 'Close' price table with valid close prices for the tckr provided
        """
        if source.upper() == "STORAGE":
            return self.getMinMaxDate_storage(dbKey=dbKey)
        else:
            return self.getMinMaxDate_tempDB(dbKey=dbKey, data_name="Close")

    def getValidDate(
        self,
        date: pd.Timestamp,
        dbKey: str = "",
        columns: Optional[TorList[str]] = None,
        close_storage: bool = True,
        source: str = "STORAGE",
    ) -> pd.Timestamp:
        """ returns the index value <= date that has valid data in colname.

        :param date: date in a RawDateSingle format that will be matched to a valid row in the db
        :param columns: column name to check for valid data. Default is None
        :return: the date that indicates which row has the most recent valid data """
        if source.upper() == "STORAGE":
            return self.getValidDate_storage(
                date, dbKey=dbKey, columns=columns, close_storage=close_storage
            )
        else:
            return self.getValidDate_tempDB(
                dbKey=dbKey, columns=columns, close_storage=close_storage
            )

    def getValidIndices(
        self,
        dbKey: str = "",
        raw_dates: Optional[RawDateType] = None,
        columns: Optional[TorList[str]] = None,
        source: str = "STORAGE",
    ) -> pd.DatetimeIndex:
        """returns a datetimeindex of the dates in raw_dates a that have valid values for all of the provided columns
        of the database indicated by source
        input:
        o  date: specify the range of dates to consider. Default is None. If date is none,
        then return all data in the table that has a valid value for the indicated column. If
        date is a datetimeIndex then return the dates within the index that also have valid
        values for the column specified. If a single date, then the date is converted to a datetimeIndex
        first
        o  columns: column in the table to consider for whether the row has valid data

        :param raw_dates: Optional[RawDateType]
        :param columns: Optional[str]
        :return: pd.DatetimeIndex

        """
        if source.upper() == "STORAGE":
            return self.getValidIndices_storage(
                dbKey=dbKey, raw_dates=raw_dates, columns=columns
            )
        else:
            return self.getValidIndices_tempDB(
                dbKey=dbKey, raw_dates=raw_dates, columns=columns
            )

    def getValidRows(
        self,
        dbKey: str = "",
        dates: Optional[RawDateType] = None,
        source: str = "STORAGE",
    ) -> pd.DataFrame:
        """returns data in the format of the db indicated by source for the given tckr based on the date provided
        o  if date is a single time then func will return a pd.Series
            of the latest row in the DB that is still less than the
            date provided and has a finite close price
        o  if date is none then it will return all
            rows that have finite prices
        o  if date is list or iterable object, then it will return
            a pd.DataFrame of all the rows of the DB that match one
            of the records. If no records match it will return an empty
            pd.DataFrame"""
        if source.upper() == "STORAGE":
            return self.getValidRows_storage(dbKey=dbKey, dates=dates)
        else:
            return self.getValidRows_tempDB(dbKey=dbKey, dates=dates)

    # def queryQuandl(self, symbol, start_date=None, end_date=None, initial_adj_price=None):
    #     symbol = Util.convertTckrToYahooFmt(symbol)
    #     data = quandl.get("WIKI/%s" % symbol)
    #     return data

    # if date is a single date return a scalar
    @overload
    def getColumnValueForEachDate(
        self, tckr: str, date: pd.Timestamp, colName: str, source: str
    ) -> Scalar:
        ...

    # if date is a collection of dates then return a series
    @overload
    def getColumnValueForEachDate(
        self, tckr: str, date: RawDateCollection, colName: str, source: str
    ) -> pd.Series:
        ...

    def getColumnValueForEachDate(
        self, tckr: str, date: RawDateType, colName: str, source: str = "storage"
    ) -> TorArray[Scalar]:
        """
        returns the values of the column for the tckr based on the date provided
            o  if date is a single time then func will return a scalar of
                the latest row in the DB that is still less than the date
                provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                prices that are finite
            o  if date is list or iterable object, then func will return
                a series of the last valid value for each date provided
                as long as the dates are > the start date of the tckr data
        """
        tckr = Util.convertTckrToUniversalFormat(tckr)
        date = Util.formatDateInput(date)
        # If only one date in the list, then treat it like a single date
        #  and don't include it in list processing
        if isinstance(date, pd.DatetimeIndex):
            values = pd.Series(index=date, name=colName)
            validValues = self.loadFromDB(dbKey=tckr, start=date, columns=colName)
            values[validValues.index] = validValues
            if date.min() != validValues.index.min():
                firstValidRow = self.getValidRows(tckr, date.min(), source=source)
                # assert type(firstValidRow) in [pd.DataFrame, float], (
                #     "Unexpected Data type returned from "
                #     + "stockDate.getValidRows, "
                #     + str(type(firstValidRow))
                #     + "\n\tTckr: "
                #     + tckr
                #     + ", MinDate: "
                #     + str(date)
                #     + ", ColName: "
                #     + col_name
                #     + ", Source: "
                #     + source
                # )
                if len(firstValidRow) > 0:
                    values[date.min()] = self.getValidRows(
                        tckr, date.min(), source=source
                    )[colName]
            return values.fillna(method="ffill")
        else:
            values = self.getValidRows(tckr, date, source=source)
            if values is np.nan or len(values.index) == 0:
                return np.nan
            else:
                return values[colName]

    @overload
    def getPrice(
        self,
        tckr: str,
        date: RawDateSingle,
        adjusted: bool = False,
        source: str = "storage",
    ) -> float:
        ...

    @overload
    def getPrice(
        self,
        tckr: str,
        date: RawDateCollection,
        adjusted: bool = False,
        source: str = "storage",
    ) -> pd.Series:
        ...

    def getPrice(
        self,
        tckr: str,
        date: RawDateType,
        adjusted: bool = False,
        source: str = "storage",
    ) -> Union[float, pd.Series]:
        """
        returns the close/adjusted price for the tckr based on the date
        provided

            o  if date is a single time then func will return a scalar of
                the latest row in the DB that is still less than the date
                provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                prices that are finite
            o  if date is list or iterable object, then func will return
                a series of the last valid price for each date provided
                as long as the dates are > the start date of the tckr data
        """
        if adjusted:
            colName = "AdjClose"
        else:
            colName = "Close"
        price = self.getColumnValueForEachDate(tckr, date, colName, source=source)
        if isinstance(price, pd.Series) and pd.isnull(price).any():
            null_index = price.index[pd.isna(price)]
            for i in null_index:
                price[i] = self.getColumnValueForEachDate(
                    tckr, i, colName, source=source
                )
        return price

    def PredictSPXVol_1m3mVIX(self, date, exDate, source="storage"):
        """
        Sets the annual volatility based on the 1 month and 3 month
        VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month
                VIX volatility
        """
        # todo: pull this out of stockDB and into an Analysis Directory
        date = Util.formatDateInput(date)
        exDate = Util.formatDateInput(exDate)
        daysOut = (exDate - date).days

        def interpolate_Volatility(x: pd.Series) -> float:
            if np.isfinite(x.M3) and x.DaysOut > 30:
                if x.DaysOut > 90:
                    volatility = x.M3
                elif x.DaysOut <= 0:
                    volatility = 0
                else:
                    volatility = (x.DaysOut - 30) * ((x.M3 - x.M1) / 60) + x.M1
            else:
                volatility = x.M1
            return volatility

        vol1M = self.getPrice("ixVIX", date, source=source)
        vol3M = self.getPrice("ixVXV", date, source=source)
        if type(date) == pd.DatetimeIndex:
            daysOut = pd.Series(data=daysOut, index=date, name="DaysOut")
            vixDF = pd.concat([vol1M, vol3M, daysOut], axis=1)
            vixDF.columns = ["M1", "M3", "DaysOut"]
            vixDF["Volatility"] = vixDF.apply(
                lambda x: interpolate_Volatility(x), axis=1
            )
            return vixDF.Volatility
        else:
            row = pd.Series(data=[vol1M, vol3M, daysOut], index=["M1", "M3", "DaysOut"])
            return interpolate_Volatility(row)

    def PredictSPXVolSingle_1m3mVIX(self, date, daysOut, source="storage"):
        """
        Sets the annual volatility based on the 1 month and 3 month
        VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month
                VIX volatility
        """
        # todo: pull this out of stockDB and into an Analysis Directory
        date = Util.formatDateInput(date)
        # If days out is >= 90 then we don't need the 1m VIX
        if daysOut >= 90:
            row = self.getValidRows("ixVXV", date, source=source)
            if source.upper() == "STORAGE":
                if row.size == 0:
                    # If empty look for the 1m data
                    row = self.getValidRows("ixVIX", date, source=source)
                    if row.size == 0:
                        volatility = np.nan
                    else:
                        volatility = row.Close
                else:
                    volatility = row.Close
            else:
                volatility = self.tempDB["Close"]["ixVXN"][row]
                if volatility is None or np.isnan(volatility):
                    # If empty look for the 1m data
                    volatility = self.tempDB["Close"]["ixVXN"][row]
                    if volatility is None or np.isnan(volatility):
                        volatility = np.nan
        else:
            # Will definitely need the 1m VIX
            row = self.getValidRows("ixVIX", date, source=source)
            if source.upper() == "STORAGE":
                if row.size == 0:
                    vol_1m = np.nan
                else:
                    vol_1m = row.close
            else:
                vol_1m = self.tempDB["Close"]["ixVXN"][row]
            if daysOut <= 30:
                volatility = vol_1m
            else:
                # get most recent volatility of 3m VXV
                if source.upper() == "STORAGE":
                    row = self.getValidRows("ixVXV", date)
                    if row.size == 0:
                        vol_3m = vol_1m
                    else:
                        vol_3m = row.Close
                else:
                    vol_3m = self.tempDB["Close"]["ixVXN"][row]
                # model as the weighted average
                volatility = (
                    vol_1m * (90.0 - daysOut) / 60.0 + vol_3m * (daysOut - 30) / 60.0
                )
        return volatility

    def reInitializeAllData(self, tckrs: TorList[str] = None) -> None:
        self.storage.open()
        if tckrs is None:
            tckrs = self.storage.keys()
        elif type(tckrs) == str:
            tckrs = [tckrs]
        for tckr in tckrs:
            rawTckr = tckr.replace("/", "")
            if rawTckr not in self.no_update_keys:
                status = self.initStockData(rawTckr)
                if status:
                    print("Successful")
                else:
                    print("Failed")
        self.storage.close()
        return

    def updateDB(self, save_csv: bool = True) -> None:
        self.storage.open()
        for rawTckr in self.storage.keys():
            tckr = rawTckr.replace("/", "")
            if tckr not in self.no_update_keys:
                self.updateTable(tckr)
        if save_csv:
            self.saveBackUpToCSV()
            print("Successfully saved all stock data to CSV")
        self.storage.close()
        return

    def updateTable(self, tckr: str = "") -> None:
        """updates a single dataframe in the database"""
        assert (
            tckr != ""
        ), "Bad value for tckr passed into StockDB.updateTable(). Tckr = ''"
        try:
            self.storage.open()
            tckr = Util.convertTckrToUniversalFormat(tckr)
            minStorageDate, maxStorageDate = self.getMinMaxDate_storage(tckr)
            # noinspection PyArgumentList
            end_date: pd.Timestamp = pd.Timestamp.today()
            if end_date.time() < datetime.time(15, 30):  # markets close at 3:30
                end_date = end_date.date() - pd.Timedelta(days=1)
            else:
                end_date = end_date.date()
            # make sure that end_date is a business day
            end_date = pd.bdate_range(end_date - pd.Timedelta(days=4), end_date).max()
            if maxStorageDate < end_date:
                # add 1 day to end_date for margin
                end_date = end_date + pd.Timedelta(days=1)
                lastAdjPrice: float = self.loadFromDB(
                    tckr,
                    maxStorageDate,
                    maxStorageDate,
                    columns="AdjClose",
                    source="storage",
                )
                # Load the relevant data
                data = self.queryYahooStockData(
                    tckr,
                    start_date=maxStorageDate,
                    end_date=end_date,
                    initial_adj_price=lastAdjPrice,
                )
                if len(data.index) > 0:
                    self.storage.open()
                    # Append all but the first row of data because that one
                    #  is already in the DB
                    # debug: sTable = self.storage.select(tckr, stop=5)
                    self.storage.append(
                        tckr,
                        data.iloc[:, :],
                        format="table",
                        data_columns=self.hdf_index_columns,
                    )
                print("%s: Successfully Updated" % tckr)
            else:
                print("%s is already up to date" % tckr)
        except Exception as ex:
            print("%s: Failed - " % tckr)
            print(ex)
            raise ex
        self.storage.close()
        return

    # =========================================================================
    # *************************************************************************
    # --------------- Begin Wrapper and Depreciated Functions -----------------
    # *************************************************************************
    # =========================================================================


def main():
    print("DataSource\\StockData.py")


if __name__ == "__main__":
    main()
