# -----------------------------------------------------------------------------
# Name:        DataTools.py
# Purpose:      toolset for initializing and updating the data sources
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------
import datetime
from typing import Optional, Any, Dict, Tuple

from sqlalchemy.orm import Session as SQL_Session

from .InflationData import InflationData
from .LIBORData import LIBORData
from .OptionData import OptionData
from .StockData import StockData
from src.DataSource.sql_db.UpdateFunctions import (
    update_stock_prices,
    update_inflation_data,
)


def initiateAllDataBases(
    fileDir: Optional[str] = None,
    workProxy: Optional[bool] = None,
    proxies: Optional[Dict[str, str]] = None,
    cookieValue: Optional[str] = None,
    crumbValue: Optional[str] = None,
    webDriver: Optional[Any] = None,
) -> Tuple[StockData, OptionData, LIBORData, InflationData]:
    """
    Returns the stockDB,optionDB and liborDB (in that order) based on
    the input criteria.
    All DB's are coherent and linked to each other as needed
    """
    if proxies is None:
        proxies = {}
    sDB = StockData(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        cookieValue=cookieValue,
        crumbValue=crumbValue,
    )
    lDB = LIBORData(fileDir=fileDir, workProxy=workProxy, proxies=proxies)
    oDB = OptionData(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        webDriver=webDriver,
        stockDB=sDB,
        liborDB=lDB,
    )
    iDB = InflationData(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        existing_webdriver=oDB.driver,
    )
    return sDB, oDB, lDB, iDB


def updateAllDBs(
    db_session: SQL_Session,
    fileDir: Optional[str] = None,
    workProxy: Optional[bool] = None,
    proxies: Optional[Dict[str, str]] = None,
    cookieValue: Optional[str] = None,
    crumbValue: Optional[str] = None,
    webDriver: Optional[Any] = None,
) -> Tuple[StockData, OptionData, LIBORData, InflationData]:
    if proxies is None:
        proxies = {}
    sDB, oDB, lDB, iDB = initiateAllDataBases(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        cookieValue=cookieValue,
        crumbValue=crumbValue,
        webDriver=webDriver,
    )
    sDB.updateDB()
    lDB.updateDB()
    iDB.updateDB()
    print("Updating SQLite DataBase:")
    update_stock_prices(db_session, cookie_value=cookieValue, crumb_value=crumbValue)
    update_inflation_data(db_session)
    print("Finished updating SQLite DataBase:")

    return sDB, oDB, lDB, iDB


def main():
    strike = 1000
    exDate = datetime.datetime(2019, 12, 20, 0, 0)
    underlyingTckr = "SPX"
    purchaseDate = datetime.datetime(2017, 8, 8)
    oDB = OptionData(workProxy=True)
    print(oDB.PriceEst_BS(underlyingTckr, "Call", purchaseDate, strike, exDate))


if __name__ == "__main__":
    main()
