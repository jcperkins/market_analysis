import numpy as np
from .StockData import StockData


def createFake3XFund(stockDB: StockData) -> None:
    stockDB.storage.open()
    tckr = "SPXLFAKE"
    data = stockDB.storage["/ixSPX"]
    change = data["Close"].pct_change() * 3
    change[0] = 0
    fakeValue = np.cumprod(change + 1) * 10
    data["Close"] = fakeValue
    data["Open"] = fakeValue
    data["High"] = fakeValue
    data["Low"] = fakeValue
    data["AdjClose"] = fakeValue
    data["Volume"] = 0
    data["Dividends"] = 0
    data["SplitFactor"] = 1
    stockDB.storage.put(tckr, data, format="table", data_columns=stockDB.data_columns)
    stockDB.storage.close()
    return
