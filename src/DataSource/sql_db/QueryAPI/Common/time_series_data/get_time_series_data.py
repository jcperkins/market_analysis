from typing import Optional, Union, overload, Iterable

import pandas as pd
import src.Utility as Util
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import Generic_ORM_Constructor
from src.DataSource.sql_db.local_types import TColumnObj
from src.types import RawDateSingle, RawDateType, RawDateCollection, Scalar

from .get_all_timeseries_data import get_all_timeseries_df, get_all_timeseries_column
from .get_data_with_datetimeindex import (
    get_df_for_datetimeindex,
    get_column_for_datetimeindex,
)
from .get_last_record import get_valid_row
from .get_valid_scalar import get_valid_scalar
from ..db_column_helpers import column_info, is_sql_column
from ..db_column_helpers import format_sql_columns
from ..query_data import TClauseElementLooksLikeBool


# if dates is a single dates and get_cols is single column return a scalar
@overload
def get_time_series_data(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: Union[str, TColumnObj],
    get_cols: Union[str, TColumnObj],
    dates: RawDateSingle,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, Scalar]:
    ...


# if dates is single dates and get_cols None or a collection return a row of
# data structured as a series
@overload
def get_time_series_data(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: Union[str, TColumnObj],
    get_cols: Union[None, Iterable[Union[str, TColumnObj]]],
    dates: RawDateSingle,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, pd.Series]:
    ...


# if dates is a None value or collection and get_cols is a single column return a column of
# data structured as a series
@overload
def get_time_series_data(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: Union[str, TColumnObj],
    get_cols: Union[str, TColumnObj],
    dates: Optional[RawDateCollection] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, pd.Series]:
    ...


# if both dates and get_cols are None or collections then the result is a dataframe
@overload
def get_time_series_data(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: Union[str, TColumnObj],
    get_cols: Union[None, str, TColumnObj, Iterable[Union[str, TColumnObj]]] = None,
    dates: Optional[RawDateCollection] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, pd.DataFrame]:
    ...


def get_time_series_data(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: Union[str, TColumnObj],
    get_cols: Union[None, str, TColumnObj, Iterable[Union[str, TColumnObj]]] = None,
    dates: Optional[RawDateType] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, Scalar, pd.Series, pd.DataFrame]:
    """
    Provides a value for every value specified in `dates` using the ffill method when one is not
    available. This functions serves as the most generic version of the get with timestamp functions
    and allows for a pandas like interface where combinations of collections or scalar column and time
    criterion will return the expected scalar, series or dataframe results. This function also makes
    every attempt to return valid data for each timestamp, even if has to pull from previous records.

    returns the most recent values of the specified columns in the db for every dates provided
    * values of None default to all possible values
    * if dates and get_cols are single elements / not collections - the result is the scalar matching the criteria
    * if either dates or get_cols are a collection and the other is singular - return a series with either
    the column names or the dates serving as the index
    o if both dates and get_cols are collections then return a dataframe of the intersection with the dates
    serving as the index

    """
    assert ts_col is not None
    ts_col, ts_name = column_info(ts_col, orm_model)
    dates = Util.formatDateInput(dates)
    query_cols, col_names = format_sql_columns(get_cols, orm_model)

    # Begin the decision tree for how to process the query request
    if dates is None:
        if is_sql_column(query_cols):
            return get_all_timeseries_column(
                session,
                orm_model,
                ts_col,
                query_cols,
                sql_filters=sql_filters,
                **kwargs
            )
        else:
            return get_all_timeseries_df(
                session,
                orm_model,
                ts_col,
                get_cols=query_cols,
                sql_filters=sql_filters,
                **kwargs
            )
    elif isinstance(dates, pd.DatetimeIndex):
        if is_sql_column(query_cols):
            return get_column_for_datetimeindex(
                session,
                orm_model,
                ts_col,
                dt_index=dates,
                get_col=query_cols,
                sql_filters=sql_filters,
                **kwargs
            )
        else:
            return get_df_for_datetimeindex(
                session,
                orm_model,
                ts_col,
                dt_index=dates,
                get_cols=query_cols,
                sql_filters=sql_filters,
                **kwargs
            )
    else:
        if is_sql_column(query_cols):
            # single dates and single column returns a scalar
            return get_valid_scalar(
                session,
                ts_col,
                get_col=query_cols,
                ts=dates,
                allow_equal=True,
                sql_filters=sql_filters,
                **kwargs
            )
        else:
            # single dates and multiple columns returns a row as a Series
            return get_valid_row(
                session,
                orm_model,
                ts_col,
                get_cols=query_cols,
                ts=dates,
                sql_filters=sql_filters,
                allow_equal=True,
                **kwargs
            )
