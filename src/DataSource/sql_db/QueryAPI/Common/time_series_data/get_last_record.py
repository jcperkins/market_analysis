from typing import Optional, Iterable, Union

import pandas as pd
from sqlalchemy import func
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import Generic_ORM_Constructor, Generic_ORM
from src.DataSource.sql_db.Models.orm_factory import orm_to_series
from src.DataSource.sql_db.local_types import TColumnObj

from .get_valid_scalar import get_valid_scalar
from ..db_column_helpers import column_info
from ..query_data import TClauseElementLooksLikeBool


def get_last_record(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: TColumnObj,
    ts: Optional[pd.Timestamp] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    allow_equal: bool = True,
    **kwargs
) -> Union[None, Generic_ORM]:
    """
    get_last_record returns the record with the last timestamp that meets the filtering criterion.
    :param session: db connection to use to pull data
    :param orm_model: the model that defines which table to use
    :param ts_col: the column that will be the timestamp used to select the record by
    :param ts: the timestamp value
    :param sql_filters: list of sqlalchemy ClauseElements to that will be passed to filter()
    :param allow_equal: if True then records are allowed to equal the ts value. If False, then records
    must be less than the ts value
    :param kwargs: remaining key word filters that will be passed to filter_by()
    :return: None if no records are found and a ORM model instance otherwise
    """
    # clean the sql_filters argument
    sql_filters = [] if sql_filters is None else list(sql_filters)
    # construct the subquery that finds the previous date
    max_ts_subquery = session.query(func.max(ts_col)).filter_by(**kwargs)
    if ts:
        subquery_filter = (
            ts_col <= pd.Timestamp(ts) if allow_equal else ts_col < pd.Timestamp(ts)
        )
        max_ts_subquery = max_ts_subquery.filter(subquery_filter)

    if len(sql_filters) > 0:
        max_ts_subquery = max_ts_subquery.filter(*sql_filters)
        final_filters = sql_filters + [ts_col == max_ts_subquery]
    else:
        final_filters = [ts_col == max_ts_subquery]
    return session.query(orm_model).filter_by(**kwargs).filter(*final_filters).first()


def get_valid_row(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    ts_col: TColumnObj,
    get_cols: Optional[Iterable[TColumnObj]] = None,
    ts: Optional[pd.Timestamp] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    allow_equal: bool = True,
    **kwargs
) -> Union[None, pd.Series]:
    """
    returns db row formatted as a pandas series of the last non-null values for get_cols
    with the name == the date. The index of the series with be the names of the columns
    in get_cols
    :param session: db connection to use to pull data
    :param orm_model: the model that defines which table to use
    :param ts_col: the column that will be the timestamp used to select the record by
    :param get_cols: columns from the orm_model to pull
    :param ts: the timestamp value
    :param sql_filters: list of sqlalchemy ClauseElements to that will be passed to filter()
    :param allow_equal: if True then records are allowed to equal the ts value. If False, then records
    must be less than the ts value
    :param kwargs: remaining key word filters that will be passed to filter_by()
    :return: None if no records are found and series of values with names of the get_cols as the index and
    named with the value of ts
    """
    assert session and orm_model and ts_col
    record = get_last_record(
        session,
        orm_model,
        ts_col=ts_col,
        ts=ts,
        allow_equal=allow_equal,
        sql_filters=sql_filters,
        **kwargs
    )
    if record is None:
        return None
    get_cols = (
        get_cols if get_cols is not None else [c for c in orm_model.__table__.columns]
    )
    col_names = [c.name for c in get_cols if c.name != ts_col.name]
    final_data = orm_to_series(record)
    final_data.name = final_data[ts_col.name] if ts is None else ts
    final_data = final_data[col_names]

    # pull the previous record if there are any null values
    null_cols = final_data.index[final_data.isna()]
    # if len == 0 then no corrections needed
    if len(null_cols) == 1:
        # just get the value for the null column
        query_col = column_info(null_cols[0], orm_model)[0]
        new_value = get_valid_scalar(
            session,
            ts_col,
            get_col=query_col,
            ts=final_data.name,
            allow_equal=False,
            sql_filters=sql_filters,
            **kwargs
        )
        if pd.notna(new_value):
            final_data[null_cols[0]] = new_value
    else:
        # more than one value... pull the previous record and then pull each column as a scalar if there
        # are still null values
        prev_record = get_last_record(
            session,
            orm_model,
            ts_col=ts_col,
            ts=final_data.name,
            allow_equal=False,
            sql_filters=sql_filters,
            **kwargs
        )
        if prev_record is not None:
            final_data[null_cols] = orm_to_series(prev_record)[null_cols]

            # check if there are still any null values
            null_cols = final_data.index[final_data.isna()]
            # if still null values then cycle through each column and try to find a valid value
            if len(null_cols) > 0:
                for c_name in null_cols:
                    query_col = column_info(c_name, orm_model)[0]
                    print("past column_info")
                    new_value = get_valid_scalar(
                        session,
                        ts_col,
                        get_col=query_col,
                        ts=getattr(prev_record, ts_col.name),
                        allow_equal=False,
                        sql_filters=sql_filters,
                        **kwargs
                    )
                    if pd.notna(new_value):
                        final_data[c_name] = new_value

    return final_data
