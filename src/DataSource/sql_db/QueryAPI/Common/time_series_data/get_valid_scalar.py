from typing import Optional, Union, Iterable

import pandas as pd
from sqlalchemy import func
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import Generic_ORM_Constructor
from src.DataSource.sql_db.local_types import TColumnObj
from src.types import RawDateSingle, Scalar

from ..db_column_helpers import is_sql_column
from ..query_data import TClauseElementLooksLikeBool


def get_valid_scalar(
    session: SqlSession,
    ts_col: TColumnObj,
    get_col: TColumnObj,
    ts: Optional[RawDateSingle] = None,
    allow_equal: bool = True,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[Scalar, None]:
    """
    returns the most recent value for the provided column that IS NOT NULL. Conditionally allowed to equal
    the ts parameter if allow_equal is set to True

    :param session: db connection to use to pull data
    :param ts_col: the column that will be the timestamp used to select the record by
    :param get_col: sqlalchemy column object from the orm_model to pull
    :param ts: the timestamp that will be the maximum date allowed
    :param allow_equal: if True then records are allowed to equal the ts value. If False, then records
    must be less than the ts value
    :param sql_filters: list of sqlalchemy ClauseElements to that will be passed to filter()
    :param kwargs: remaining key word sql_filters that will be passed to filter_by()
    :return: a scalar value or None if no records satisfy the criteria
    """
    assert (
        session
        and ts_col
        and is_sql_column(ts_col)
        and get_col
        and is_sql_column(get_col)
    )
    # clean the sql_filters argument
    sql_filters = [] if sql_filters is None else list(sql_filters)
    sql_filters.append(get_col.isnot(None))
    # construct the subquery that finds the previous date
    max_ts_subquery = (
        session.query(func.max(ts_col)).filter_by(**kwargs).filter(*sql_filters)
    )
    if ts:
        subquery_filter = (
            ts_col <= pd.Timestamp(ts) if allow_equal else ts_col < pd.Timestamp(ts)
        )
        max_ts_subquery = max_ts_subquery.filter(subquery_filter)

    return (
        session.query(get_col)
        .filter_by(**kwargs)
        .filter(*sql_filters, ts_col == max_ts_subquery)
        .scalar()
    )
