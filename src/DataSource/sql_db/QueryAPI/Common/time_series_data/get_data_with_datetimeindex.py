from typing import Optional, Union, Iterable, Type

import numpy as np
import pandas as pd
import src.Utility as Util
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import Generic_ORM
from src.DataSource.sql_db.local_types import TColumnObj
from src.types import RawDateCollection

from .get_last_record import get_valid_row
from ..db_column_helpers import is_sql_column, column_info, column_list_info
from ..query_data import query_table, TClauseElementLooksLikeBool


def get_df_for_datetimeindex(
    session: SqlSession,
    orm_model: Type[Generic_ORM],
    ts_col: Union[str, TColumnObj],
    dt_index: RawDateCollection,
    get_cols: Optional[Iterable[TColumnObj]] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[pd.DataFrame]:
    """
    Provides a value for every value specified in `dt_index` using the ffill method
    and returns a pandas DataFrame object with dt_index set to the index. Queries that return
    no data but are error free will return an empty dataframe

    :param session: sqlalchemy session object
    :param orm_model: a custom class/constructor of a sqlalchemy model
    :param ts_col: the column to consider set as the index and filter on
    :param dt_index: the dates to pull
    :param get_cols: columns from the orm_model to pull. None defaults to all columns
    :param sql_filters: additional sqlalchemy filter objects for fine tuned filtering, does not actually allow
    boolean values
    :param kwargs: keyword arguments that will be passed to query.filter_by()
    :return: pandas DataFrame with dt_index set to the index
    """
    assert (
        dt_index is not None
        and ts_col is not None
        and orm_model is not None
        and session is not None
    )
    dt_index = Util.formatDateArray(dt_index)
    ts_col, ts_name = column_info(ts_col, orm_model)
    ts_filters = [ts_col >= dt_index.min(), ts_col <= dt_index.max()]
    ts_adjusted_filters = (
        ts_filters if sql_filters is None else list(sql_filters) + ts_filters
    )

    # ensure the ts data will be queried
    assert is_sql_column(get_cols) is False
    if get_cols is None:
        query_cols = None
    else:
        col_names = [x.name for x in get_cols]
        query_cols = (
            list(get_cols) if ts_name in col_names else [ts_col] + list(get_cols)
        )
    assert query_cols is None or len(query_cols) > 1

    db_data = query_table(
        session, orm_model, get_cols=query_cols, filters=ts_adjusted_filters, **kwargs
    )
    if db_data.empty:
        return db_data.set_index(ts_name)
    if db_data.duplicated(subset=ts_name).any():
        raise ValueError(
            "Not enough criteria was provided and the resulting DatetimeIndex has duplicates"
        )
    db_data = db_data.set_index(ts_name)
    print(db_data)
    # the final dataframe starts with just the correct index and columns and then add the data from the db is assigned
    final_df = pd.DataFrame(index=dt_index, columns=db_data.columns)
    # enforcing the index intersection more as a formality and best practice. It shouldn't be needed
    idx_intersect = np.intersect1d(dt_index, db_data.index)
    final_df.loc[idx_intersect, :] = db_data.loc[idx_intersect, :]

    # check if there are any invalid values in the first record and try to get a valid number
    min_ts = dt_index.min()
    invalid_col_names = list(final_df.columns[pd.isna(final_df.loc[min_ts, :])])
    if len(invalid_col_names) > 0:
        invalid_cols, invalid_col_names = column_list_info(invalid_col_names, orm_model)
        replacement_data = get_valid_row(
            session,
            orm_model,
            ts_col=ts_col,
            get_cols=invalid_cols,
            ts=min_ts,
            allow_equal=False,
            sql_filters=sql_filters,
            **kwargs
        )
        if replacement_data is not None:
            invalid_col_names = list(invalid_col_names)
            final_df.loc[min_ts, invalid_col_names] = replacement_data[
                invalid_col_names
            ]

    # sort and forward fill the data so the last valid value is extended into nan cells
    return final_df.sort_index().fillna(method="ffill")


def get_column_for_datetimeindex(
    session: SqlSession,
    orm_model: Type[Generic_ORM],
    ts_col: Union[str, TColumnObj],
    dt_index: RawDateCollection,
    get_col: TColumnObj,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[pd.Series]:
    """
    Provides a value for every value specified in `dt_index` using the ffill method
    and returns a pandas Series object with dt_index set to the index. Queries that return
    no data but are error free will return an empty series

    :param session: sqlalchemy session object
    :param orm_model: a custom class/constructor of a sqlalchemy model
    :param ts_col: the column to consider set as the index and filter on
    :param dt_index: the dates to pull
    :param get_col: column from the orm_model to pull
    :param sql_filters: additional sqlalchemy filter objects for fine tuned filtering, does not actually allow
    boolean values
    :param kwargs: keyword arguments that will be passed to query.filter_by()
    :return: pandas Series with dt_index set to the index
    """
    assert ts_col and orm_model and get_col and is_sql_column(get_col)
    ts_col, ts_name = column_info(ts_col, orm_model)
    assert (
        get_col.name != ts_name
    ), "get_col needs to be a different column from the timestamp column"
    df = get_df_for_datetimeindex(
        session,
        orm_model,
        ts_col=ts_col,
        dt_index=dt_index,
        get_cols=[ts_col, get_col],
        sql_filters=sql_filters,
        **kwargs
    )
    return df[get_col.name]
