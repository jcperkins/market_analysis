from typing import Optional, Union, Iterable, Type

import pandas as pd
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import Generic_ORM
from src.DataSource.sql_db.local_types import TColumnObj

from ..db_column_helpers import is_sql_column, column_info
from ..query_data import query_table, TClauseElementLooksLikeBool


def get_all_timeseries_df(
    session: SqlSession,
    orm_model: Type[Generic_ORM],
    ts_col: Union[str, TColumnObj],
    get_cols: Optional[Iterable[TColumnObj]] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> pd.DataFrame:
    """
    returns a DataFrame of all timeseries data in the table that match the filter criteria. The result
    will have the timeseries data as the index and will be filled to remove null values. Queries that return
    no data but are error free will return an empty DataFrame

    :param session: sqlalchemy session object
    :param orm_model: a custom class/constructor of a sqlalchemy model
    :param ts_col: the column to consider set as the index and filter on
    :param get_cols: columns from the orm_model to pull. None defaults to all columns
    :param sql_filters: additional sqlalchemy filter objects for fine tuned filtering, does
    not actually allow boolean values
    :param kwargs: keyword arguments that will be passed to query.filter_by()
    :return: pandas DataFrame or Series with the ts_col set to the index
    """
    assert session is not None and ts_col is not None and orm_model is not None
    ts_col, ts_name = column_info(ts_col, orm_model)

    # ensure the ts data will be queried
    assert is_sql_column(get_cols) is False
    if get_cols is None:
        query_cols = None
    else:
        col_names = [x.name for x in get_cols]
        query_cols = (
            list(get_cols) if ts_name in col_names else [ts_col] + list(get_cols)
        )
    assert query_cols is None or len(query_cols) > 1

    db_data = query_table(
        session, orm_model, get_cols=query_cols, filters=sql_filters, **kwargs
    )
    if db_data.duplicated(subset=ts_name).any():
        raise ValueError(
            "Not enough criterion was provided and the resulting DatetimeIndex has duplicates"
        )

    # set the index, ensure that it is sorted and fill any null values
    return db_data.set_index(ts_name).sort_index().fillna(method="ffill")


def get_all_timeseries_column(
    session: SqlSession,
    orm_model: Type[Generic_ORM],
    ts_col: Union[str, TColumnObj],
    get_col: TColumnObj,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[None, pd.Series]:
    """
    returns a pandas Series of all timeseries data in the table that match the filter criteria. The result
    will have the timeseries data as the index and will be filled to remove null values. Queries that return
    no data but are error free will return an empty series

    :param session: sqlalchemy session object
    :param orm_model: a custom class/constructor of a sqlalchemy model
    :param ts_col: the column to consider set as the index and filter on
    :param get_col: column from the orm_model to pull
    :param sql_filters: additional sqlalchemy filter objects for fine tuned filtering, does
    not actually allow boolean values
    :param kwargs: keyword arguments that will be passed to query.filter_by()
    :return: pandas DataFrame or Series with the ts_col set to the index
    """
    assert (
        session is not None
        and ts_col is not None
        and orm_model is not None
        and get_col is not None
    )
    assert is_sql_column(get_col)
    ts_col, ts_name = column_info(ts_col, orm_model)
    assert (
        get_col.name != ts_name
    ), "get_col needs to be a different column from the timestamp column"
    df = get_all_timeseries_df(
        session,
        orm_model,
        ts_col=ts_col,
        get_cols=[ts_col, get_col],
        sql_filters=sql_filters,
        **kwargs
    )
    return df[get_col.name] if df is not None else None
