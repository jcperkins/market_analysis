from typing import Optional, Iterable, Union, TypeVar

import numpy as np
import pandas as pd
from sqlalchemy import Date, DateTime
from sqlalchemy.orm import Session as SqlSession
from sqlalchemy.sql import ClauseElement
from src.DataSource.sql_db.Models import Generic_ORM_Constructor

from .date_columns import date_column_names
from ...local_types import TColumnObj

TClauseElementLooksLikeBool = TypeVar(
    "TClauseElementLooksLikeBool", bool, ClauseElement
)


def query_column(
    session: SqlSession,
    col: TColumnObj,
    filters: Optional[Iterable[ClauseElement]] = None,
    **kwargs
) -> pd.Series:
    """
    returns a series with column data fitting the sql_filters indicated with the sql_filters and kwargs parameters

    :param session: the sqlalchemy session to use to get data
    :param col: sqlalchemy.Column object indicating which column to pull. this must be supplied
    :param filters: list of filter clause elements that will be spread into a .filter() function if provided.
    This allows for more complex filtering than the kwargs parameter
    :param kwargs: simple key word formatted sql_filters that will be passed to a filter_by() function
    :return: an sqlalchemy Query object that can be executed for the data
    """
    assert col is not None
    parse_dates = col.name if type(col.type) in [Date, DateTime] else None
    q = session.query(col).filter_by(**kwargs)
    if filters is not None:
        q = q.filter(*filters)
    return pd.read_sql(q.statement, con=session.get_bind(), parse_dates=parse_dates)[
        col.name
    ]


def query_table(
    session: SqlSession,
    orm_model: Generic_ORM_Constructor,
    get_cols: Optional[Iterable[Union[str, TColumnObj]]] = None,
    filters: Optional[Iterable[ClauseElement]] = None,
    **kwargs
) -> pd.DataFrame:
    """
    returns a dataframe with columns from get_cols and data fitting the sql_filters indicated with kwargs

    :param session: the sqlalchemy session to use to get data
    :param orm_model: the orm_model that which table to query
    :param get_cols: a list of sqlalchemy.Column objects indicating which columns to pull. None will be
    interpreted to mean all columns
    :param filters: list of filter clause elements that will be spread into a .filter() function if provided.
    This allows for more complex filtering than the kwargs parameter
    :param kwargs: simple key word formatted sql_filters that will be passed to a filter_by() function
    :return: an sqlalchemy Query object that can be executed for the data
    """
    parse_dates = date_column_names(orm_model)
    if get_cols is None:
        q = session.query(orm_model).filter_by(**kwargs)
    else:
        # verify get_columns format and indicate if any selected columns are dates
        get_cols = [getattr(orm_model, c) if type(c) == str else c for c in get_cols]
        parse_dates = list(np.intersect1d(parse_dates, [c.name for c in get_cols]))
        q = session.query(*get_cols).filter_by(**kwargs)
    if filters is not None:
        q = q.filter(*filters)
    return pd.read_sql(q.statement, con=session.get_bind(), parse_dates=parse_dates)
