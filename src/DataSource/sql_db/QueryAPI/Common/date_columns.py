from typing import List, Union

from sqlalchemy import Date, DateTime, Column, Table
from sqlalchemy.ext.declarative import DeclarativeMeta


def date_columns(model_or_table: Union[Table, DeclarativeMeta]) -> List[Column]:
    """
    return the sqlalchemy columns that have a date-like type

    :param model_or_table: either an ORM model or the sql Table behind the model. The function will
    interpret between the two
    :return:
    """
    table = (
        model_or_table
        if isinstance(model_or_table, Table)
        else model_or_table.__table__
    )
    return [
        c
        for c in table.columns
        if isinstance(c.type, Date) or isinstance(c.type, DateTime)
    ]


def date_column_names(model_or_table: Union[Table, DeclarativeMeta]) -> List[str]:
    """
    return the sqlalchemy column names that have a date-like type

    :param model_or_table: either an ORM model or the sql Table behind the model. The function will
    interpret between the two
    :return:
    """
    return [c.name for c in date_columns(model_or_table)]
