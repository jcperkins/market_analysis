from typing import Any, Union, Tuple, overload, Optional, Iterable

from sqlalchemy import Column
from sqlalchemy.orm.attributes import InstrumentedAttribute

from ...connect_info import Base


def is_column_like(obj: Any) -> bool:
    """returns True if obj is something that could be interpreted as a single column"""
    return type(obj) == str or is_sql_column(obj)


def is_sql_column(obj: Any) -> bool:
    """returns True if obj is an accepted instance of a sqlalchemy column object"""
    return isinstance(obj, Column) or isinstance(obj, InstrumentedAttribute)


@overload
def column_info(column: str, model: Base) -> Tuple[InstrumentedAttribute, str]:
    ...


@overload
def column_info(
    column: Union[InstrumentedAttribute, Column], model: Optional[Base]
) -> Tuple[InstrumentedAttribute, str]:
    ...


def column_info(
    column: Union[str, InstrumentedAttribute, Column], model: Optional[Base] = None
) -> Tuple[InstrumentedAttribute, str]:
    """
    returns a tuple with (sqlalchemy column obj, column name) representing the column.
    Model is only needed if the column is a string
    :param column: a string representing the column name or the sqlalchemy column object
    :param model: an sqlalchemy ORM model to be used if column is only a string
    :return:
    """
    return (
        (getattr(model, column), column)
        if type(column) == str
        else (column, column.name)
    )


def column_list_info(
    columns: Iterable[Union[str, InstrumentedAttribute, Column]], orm_model: Base
) -> Tuple[Iterable[InstrumentedAttribute], Iterable[str]]:
    """returns a tuple of containing a list of column objects and a list of column names"""
    # noinspection PyTypeChecker
    return zip(*[column_info(c, orm_model) for c in columns])


@overload
def format_sql_columns(columns: None, orm_model: Base) -> Tuple[None, None]:
    ...


@overload
def format_sql_columns(
    columns: Union[str, InstrumentedAttribute, Column], orm_model: Base
) -> Tuple[InstrumentedAttribute, str]:
    ...


@overload
def format_sql_columns(
    columns: Union[Iterable[Union[str, InstrumentedAttribute, Column]]], orm_model: Base
) -> Tuple[Iterable[InstrumentedAttribute], Iterable[str]]:
    ...


def format_sql_columns(
    columns: Union[
        None,
        str,
        InstrumentedAttribute,
        Column,
        Iterable[Union[str, InstrumentedAttribute, Column]],
    ],
    orm_model: Base,
) -> Union[
    Tuple[None, None],
    Tuple[InstrumentedAttribute, str],
    Tuple[Iterable[InstrumentedAttribute], Iterable[str]],
]:
    """
    Returns a tuple containing (None, None) or the column information in the same form as the columns argument
    If every column in the table is desired use orm_model.__table__.columns as the input for `columns`
    """
    if columns is None:
        return None, None
    if is_column_like(columns):
        # single column
        print("single column")
        return column_info(columns, orm_model)
    else:
        print("column collection")
        return column_list_info(columns, orm_model)
