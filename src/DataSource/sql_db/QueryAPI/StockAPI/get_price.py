from typing import Optional, Union, overload, Iterable

import pandas as pd
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.sql_db.Models import StockPrice
from src.types import RawDateCollection, RawDateSingle, RawDateType

from ..Common import get_time_series_data
from ..Common.query_data import TClauseElementLooksLikeBool


@overload
def get_price(
    session: SqlSession,
    tckr: str,
    select_dates: RawDateSingle,
    adjusted: Optional[bool] = True,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> float:
    ...


@overload
def get_price(
    session: SqlSession,
    tckr: str,
    select_dates: Union[None, RawDateCollection],
    adjusted: Optional[bool] = True,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> pd.Series:
    ...


def get_price(
    session: SqlSession,
    tckr: str,
    select_dates: Optional[RawDateType] = None,
    adjusted: Optional[bool] = True,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    **kwargs
) -> Union[float, pd.Series]:
    """
    returns the close/adjusted price for the tckr based on the date
    provided

        o if select_dates is None then return every price for the tckr
          if select_dates is a single time then func will return a scalar of
            the latest row in the DB that is still less than the date
            provided and has a finite close price
        o  if select_dates is an empty list (i.e. []) then it will return all
            prices that are finite
        o  if select_dates is list or iterable object, then func will return
            a series of the last valid price for each date provided
            as long as the dates are > the start date of the tckr data
    """
    return get_time_series_data(
        session,
        StockPrice,
        ts_col=StockPrice.date,
        get_cols=StockPrice.adj_close if adjusted else StockPrice.close,
        dates=select_dates,
        sql_filters=sql_filters,
        tckr=tckr,
        **kwargs
    )
