import pandas as pd
import numpy as np
import src.Utility as Util

# TODO: refactored this out of the predition model class, but need to adapt it to SQLite databases
def approximate_vix_volatility(date, prediction_date, source="storage"):
    """ Sets the volatility based on the 1 month and 3 month VIX index
        o   < 1 month and it defaults to the 1 month VIX
        o   > 3 months and it defaults to the 3 month VXN
        o   else weighted average of the 1month and 3 month VIX
            volatilities"""
    date = Util.formatDateInput(date)
    prediction_date = Util.formatDateInput(prediction_date)
    if type(date) == pd.DatetimeIndex:
        volatility_data = pd.DataFrame(
            index=date, columns=["ixVIX", "ixVXV", "AdjustedVixVolatility"]
        )
    else:
        volatility_data = pd.DataFrame(
            index=[date],
            columns=[
                "PredictionTimedelta",
                "ixVIX",
                "ixVXV",
                "AdjustedVixVolatility",
            ],
        )

    volatility_data.loc[:, "PredictionTimedelta"] = prediction_date - date
    # load the correct data
    if (volatility_data.PredictionTimedelta > pd.Timedelta(days=30)).any():
        volatility_data.loc[date, "ixVXV"] = self.stockDB.getPrice(
            "ixVXV", date, adjusted=False, source=source
        )
        if (volatility_data.PredictionTimedelta < pd.Timedelta(days=90)).any():
            volatility_data.loc[date, "ixVIX"] = self.stockDB.getPrice(
                "ixVIX", date, adjusted=False, source=source
            )
            # Assign dates less than or equal to 30 days or have null ixVXV
            select_bool = np.logical_and(
                volatility_data.PredictionTimedelta <= pd.Timedelta(days=30),
                pd.isna(volatility_data.ixVXV),
            )
            if select_bool.any():
                volatility_data.loc[
                    select_bool, "AdjustedVixVolatility"
                ] = volatility_data.loc[select_bool, "ixVIX"]
            # Assign all dates greater than or equal to 90 days
            select_bool = volatility_data.PredictionTimedelta <= pd.Timedelta(
                days=30
            )
            if select_bool.any():
                volatility_data.loc[
                    select_bool, "AdjustedVixVolatility"
                ] = volatility_data.loc[select_bool, "ixVXV"]
            # assign all remaining values the weighted average of the 1m
            #  and 3m VIX volatility
            select_bool = pd.isna(volatility_data.AdjustedVixVolatility)
            if select_bool.any():
                calculated_value = volatility_data.ixVIX[select_bool] / 60.0 * (
                        90.0 - volatility_data.PredictionTimedelta[select_bool].days
                ) + (
                                           volatility_data.ixVXV[select_bool]
                                           / 60.0
                                           * (volatility_data.PredictionTimedelta.days[select_bool] - 30)
                                   )
                volatility_data.loc[
                    select_bool, "AdjustedVixVolatility"
                ] = calculated_value
        else:
            # nothing is below 90 so assign VXV to everything and
            #  correct for if there are any missing values
            volatility_data.loc[
                date, "AdjustedVixVolatility"
            ] = self.stockDB.getPrice("ixVXV", date, adjusted=False, source=source)
            select_bool = pd.isna(volatility_data.AdjustedVixVolatility)
            if select_bool.any():
                missing_dates = volatility_data.index[select_bool]
                volatility_data.loc[
                    missing_dates, "AdjustedVixVolatility"
                ] = self.stockDB.getPrice(
                    "ixVIX", missing_dates, adjusted=False, source=source
                )
    else:
        # nothing is above 30 days so assign VIX to everything
        volatility_data.loc[date, "AdjustedVixVolatility"] = self.stockDB.getPrice(
            "ixVIX", date, adjusted=False, source=source
        )

    return volatility_data.AdjustedVixVolatility[date]