from typing import Optional, Iterable

import pandas as pd
from sqlalchemy.orm import Session
from src.DataSource.sql_db.Models import StockPrice

from ..Common import get_last_record
from ..Common.query_data import TClauseElementLooksLikeBool


def get_last_price(
    session: Session,
    tckr: str,
    ts: Optional[pd.Timestamp] = None,
    sql_filters: Optional[Iterable[TClauseElementLooksLikeBool]] = None,
    allow_equal: bool = True,
    **kwargs
) -> StockPrice:
    """
    StockPrice specific wrapper around get_last_record. This is provided for convenience
    and backwards compatibility

    :param session: db connection to use to pull data
    :param tckr:
    :param ts: the timestamp value
    :param sql_filters: list of sqlalchemy ClauseElements to that will be passed to filter()
    :param allow_equal: if True then records are allowed to equal the ts value. If False, then records
    must be less than the ts value
    :param kwargs: remaining key word filters that will be passed to filter_by()
    :return: None if no records are found and a ORM model instance otherwise    """
    return get_last_record(
        session,
        StockPrice,
        StockPrice.date,
        ts=ts,
        allow_equal=allow_equal,
        sql_filters=sql_filters,
        tckr=tckr,
        **kwargs
    )
