import datetime
from typing import Optional, Union

import numpy as np
import pandas as pd
import requests
import src.Utility as Util
from sqlalchemy.orm import Session as SQL_Session
from src.DataSource.sql_db.QueryAPI import StockAPI
from src.DataSource.Web import get_stock_prices_from_yahoo
from src.DataSource.Web.create_web_session import create_web_session
from src.DataSource.sql_db.Models import StockPrice

forbid_update = ["SPXLFAKE", "XIV"]


def _recalculate_adj_close(
    df: pd.DataFrame, initial_adj_price: Optional[float]
) -> pd.Series:
    """
    calculate the adjusted price of the stock based off of the first close and resulting dividends.
    Final adj price will represent the value of 1 stock unit purchased
        at the first closing price with all subsequent dividends
        reinvested. Split factor is assumed to be 1 unless there
        is a split event

    :param df: is a pd.DataFrame with the minimum columns of 'close' and 'dividends'
    :param initial_adj_price: the adj_close value of the last record in the local db
    that will be used to base all future adj_close values on
    :return: a series of the new values for the adj_close column
    """
    # initialize all data
    close_price = df.close
    dividends = df.dividends
    # splitFactor = df.SplitFactor
    initial_units = initial_adj_price / close_price[0] if initial_adj_price else 1

    equivalent_qty = np.zeros(close_price.shape)
    for rowNum in range(len(df.index)):
        if rowNum == 0:
            equivalent_qty[rowNum] = initial_units
        else:
            # Removing the split factor multiplier because it the data is
            #  wrong for EFA and I haven't seen a case where it is right
            # equivalent_qty[rowNum] = (equivalent_qty[rowNum-1] +
            #  (equivalent_qty[rowNum-1] * dividends[rowNum] /
            #  close_price[rowNum]))) * splitFactor[rowNum]
            equivalent_qty[rowNum] = equivalent_qty[rowNum - 1] + (
                equivalent_qty[rowNum - 1] * dividends[rowNum] / close_price[rowNum]
            )

    adj_close = close_price * equivalent_qty
    return adj_close


def update_single_stock_from_yahoo(
    tckr: str,
    db_session: SQL_Session,
    web_session: Optional[requests.Session] = None,
    crumb_value: Optional[str] = None,
    cookie_value: Optional[str] = None,
) -> Union[None, pd.DataFrame]:
    """
    update the SQLite3 database of the session instance with price data from Yahoo to the last
    record in the db to now

    :param tckr:
    :param db_session: the sqlalchemy session for the SQLite3 db
    :param web_session: a requests server instance for connecting with the Yahoo server. If one is not provided
    then a default session will be created with no opportunities to provide any proxy or special information
    :param crumb_value: string value supplied in the request url to authenticate the
    session instance to the server
    :param cookie_value: string value supplied in the request cookies to authenticate the
    session instance to the server
    :return: a dataframe of results from the Yahoo server
    """
    # process inputs
    assert tckr != "", "Bad value for tckr passed into StockDB.updateTable(). Tckr = ''"
    try:
        tckr = Util.convertTckrToUniversalFormat(tckr)
        last_price = StockAPI.get_last_price(db_session, tckr, ts=None)
        assert (
            last_price is not None
        ), f"Expected {tckr} to have an existing record but it doesnt"
        # noinspection PyArgumentList
        end_date = pd.Timestamp.today()
        # markets close at 3:30
        if end_date.time() < datetime.time(15, 30):
            end_date = pd.Timestamp(end_date.date() - pd.Timedelta(days=1))
        else:
            end_date = pd.Timestamp(end_date.date())
        # make sure that end_date is a business day
        end_date = pd.bdate_range(end_date - pd.Timedelta(days=4), end_date).max()

        if last_price.date < end_date:
            # add 1 day to end_date for margin
            end_date = end_date + pd.Timedelta(days=1)

            # get the relevant data from Yahoo
            data = get_stock_prices_from_yahoo(
                tckr,
                s=web_session,
                start_date=last_price.date,
                end_date=end_date,
                crumb_value=crumb_value,
                cookie_value=cookie_value,
            )
            if len(data.index) > 0:
                # fix the adj_close values
                data["adj_close"] = _recalculate_adj_close(data, last_price.adj_close)

                # load each row of the data
                def add_stock_price_to_db(r: pd.Series) -> bool:
                    d = r.to_dict()
                    # noinspection PyUnresolvedReferences
                    d["date"] = d["date"].date()
                    s = StockPrice(**d)
                    if not s.is_in_table(db_session):
                        db_session.add(s)
                        return True
                    else:
                        return False

                out = data.apply(add_stock_price_to_db, axis=1)
                db_session.commit()
                print(f"{tckr}: Successfully Updated with {out.sum()} new records")
                return data
        print(f"{tckr} is already up to date")
        return None
    except Exception as ex:
        print("%s: Failed - " % tckr)
        print(ex)
        raise ex


def update_stock_prices(
    db_session: SQL_Session,
    web_session: Optional[requests.Session] = None,
    crumb_value: Optional[str] = None,
    cookie_value: Optional[str] = None,
) -> pd.DataFrame:
    """
    update the SQLite3 database of the session instance with price data for each existing tckr in the DB
    from Yahoo. Requests a time span from the last record in the db to now. Tickers included in the forbid_update list
    at the top of this file are not updated

    :param db_session: the sqlalchemy session for the SQLite3 db
    :param web_session: a requests server instance for connecting with the Yahoo server. If one is not provided
    then a default session will be created with no opportunities to provide any proxy or special information
    :param crumb_value: string value supplied in the request url to authenticate the
    session instance to the server
    :param cookie_value: string value supplied in the request cookies to authenticate the
    session instance to the server
    :return: a dataframe of results from the Yahoo server
    """
    if web_session is None:
        web_session = create_web_session()
    yahoo_data = []
    for (t,) in db_session.query(StockPrice.tckr).distinct():
        if t in forbid_update:
            continue
        yahoo_data.append(
            update_single_stock_from_yahoo(
                t,
                db_session=db_session,
                web_session=web_session,
                crumb_value=crumb_value,
                cookie_value=cookie_value,
            )
        )

    return pd.concat(yahoo_data, axis=1, ignore_index=True)
