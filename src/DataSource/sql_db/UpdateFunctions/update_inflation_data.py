import datetime
from typing import List

import pandas as pd
from sqlalchemy.orm import Session
from src.DataSource.Web import scrape_cpi_data
from src.DataSource.sql_db.Models import RateCPI


def _transform_raw_data(raw_df: pd.DataFrame) -> pd.DataFrame:
    """
    takes a DataFrame with columns [
         'Year', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Ave'
    ] and returns a dataframe with numbers instead of months and transposed so that the new
    columns are ['year', 'month', 'rate']
    :param raw_df:
    :return:
    """
    raw_cols = [
        "Year",
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
        "Ave",
    ]
    transformed_cols = ["year", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    # drop the Ave column
    df = raw_df.loc[:, raw_cols[:-1]]
    # rename the columns to the numeric equivalent
    df.columns = transformed_cols
    # transform to year, month index
    df = df.set_index("year")
    df.columns.name = "month"
    s = df.stack("month").dropna()
    s.name = "rate"

    return (
        pd.DataFrame(s)
        .reset_index()  # converts year/month into columns
        .sort_values(axis=0, by=["year", "month"], ascending=False)
        .reset_index(drop=True)
    )  # to reset the integer index to ascending integers


def update_inflation_data(db_session: Session) -> List[RateCPI]:
    """
    Reads the last record of the dataframe and scrapes new data off the web if needed. Returns
    a list of `RateCPI` objects that were added to the table.

    :param db_session:
    :return: list of `RateCPI` objects that were added to the table
    """
    out = []
    last_record = RateCPI.get_last(db_session)
    current = datetime.date.today()
    assert last_record is not None
    assert current is not None

    # Update if a full calendar month has passed since the last record
    if (current.year - last_record.year) * 12 + (current.month - last_record.month) > 1:
        print(f"Updating the CPI inflation rate data")
        cpi_data = _transform_raw_data(scrape_cpi_data())
        is_new = (cpi_data.year * 13 + cpi_data.month) > (
            last_record.year * 13 + last_record.month
        )
        if is_new.sum():
            for i, row in cpi_data.loc[is_new, :].iterrows():
                cpi = RateCPI(year=row.year, month=row.month, rate=row.rate)
                if not cpi.is_in_table(db_session):
                    print("Adding Rate: " + str(cpi))
                    db_session.add(cpi)
                    out.append(cpi)

            db_session.commit()

    return out
