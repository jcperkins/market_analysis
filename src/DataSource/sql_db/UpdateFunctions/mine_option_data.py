import datetime
from typing import Optional, Union, List

import pandas as pd
from config import default_settings
from selenium import webdriver
from sqlalchemy.orm import Session as SqlSession
from src.DataSource.Web.fidelity import init_option_webpage, read_nth_option
from src.DataSource.Web.selenium_driver_utility import get_driver, close_driver
from src.DataSource.sql_db.Models import OptionPrice
from .process_option_expiration_data import process_option_expiration_data


def scrape_all_expiration_dates(
    driver: webdriver,
    db_session: SqlSession,
    symbol: str,
    load_batches: Optional[bool] = False,
    return_data: Optional[bool] = False,
    debug: Optional[bool] = False,
) -> Union[None, pd.DataFrame]:
    """
    Load all option data for all expiration dates on the fidelity website for the provided stock symbol.

    This function has been hardcoded to call the function process_option_expiration_data(df)
    which is a hook to do any processing and evaluation of the data that may be time sensitive

    also calls a function process_option_expiration which is a hook to apply any
    processing
    :param driver: a selenium webdriver that will access the fidelity website
    :param db_session: a sqlalchemy session that will connect to the database
    :param symbol: the stock symbol to query
    :param load_batches: boolean flag that determines whether to load each expiration data
    into the database as it is processed or to load the data for every expiration date at the end
    :param return_data: boolean flag. If true return all the data and if false return None
    :param debug:
    :return:
    """
    # noinspection PyArgumentList
    start = pd.Timestamp.today()
    symbol = symbol.upper()
    web_symbol = "SPY" if symbol == "ixSPX" else symbol
    # get the time of the query
    query_timestamp = pd.Timestamp(datetime.datetime.today())
    data_list = []
    col_headings = None
    call_put_transition = None

    exdate_qty = init_option_webpage(driver, web_symbol)
    # Cycle through expiration dates one at a time and harvest each set of data
    i_exdate = 0
    while i_exdate < exdate_qty:
        option_data, col_headings, call_put_transition = read_nth_option(
            i_exdate,
            symbol,
            exdate_qty,
            query_timestamp,
            driver,
            col_headings=col_headings,
            call_put_transition=call_put_transition,
        )
        if option_data is not None:
            process_option_expiration_data(option_data)
            data_list.append(option_data)
            if load_batches:
                OptionPrice.upload_df(option_data, db_session, commit=True, debug=debug)

        i_exdate += 1

    if return_data:
        # need to combine all data in the list
        all_data = pd.concat(data_list, ignore_index=True, sort=False)
        if not load_batches:
            # no data has been loaded yet so load the whole dataframe now
            OptionPrice.upload_df(all_data, db_session, commit=True, debug=debug)
        out = all_data
    else:
        if not load_batches:
            # no data has been loaded yet so load all of the individual DataFrames now
            for df in data_list:
                OptionPrice.upload_df(df, db_session, commit=True)
        out = None

    # noinspection PyArgumentList
    print(
        f"Completed fidelity option query for {symbol} in {str(pd.Timestamp.today() - start)}"
    )

    return out


def mine_option_data(
    driver: webdriver,
    db_session: SqlSession,
    symbols: Union[str, List[str]],
    load_batches: Optional[bool] = True,
    return_data: Optional[bool] = False,
    keep_driver: Optional[bool] = False,
    debug: Optional[bool] = False,
) -> Union[None, pd.DataFrame]:
    data_list: List[pd.DataFrame] = []
    if type(symbols) is not list:
        symbols = [symbols]

    if driver is None:
        print("Loading WebDriver...")
        if debug:
            # having troubles with the headless implementation
            # BUG - Having problems with phantomJS not loading the
            # fidelity web page. Switching to chromedriver in headless mode

            # noinspection PyUnresolvedReferences
            chrome_options = webdriver.chrome.options.Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=1920x1080")
            driver = get_driver(options=chrome_options)
        else:
            print(f"Using ChromeDriver located at {default_settings.chromedriver}")
            driver = get_driver(driver_path=default_settings.chromedriver)

    for symbol in symbols:
        print("Mining Options for '%s'" % symbol)
        data_list.append(
            scrape_all_expiration_dates(
                driver,
                db_session,
                symbol,
                load_batches=load_batches,
                return_data=return_data,
                debug=debug,
            )
        )

    if not keep_driver:
        close_driver(driver)
    if return_data:
        return pd.concat(data_list, ignore_index=True, sort=False)
    else:
        return
