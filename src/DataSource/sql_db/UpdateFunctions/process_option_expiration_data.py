"""
-------------------------------------------------------------------------------
Name:        process_option_expiration_data.py
Project:     MarketAnalysis
Module:      MarketAnalysis
Purpose:

Author:      Cory Perkins

Created:     2/2/20, 7:11 PM
Modified:    2/2/20, 7:11 PM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""
from typing import Tuple, Union

import numpy as np
import pandas as pd
from src.Analytics.Options import find_risk_free_trade

cumulative_risk_free = []
# todo: turn hardcoded annual_return_cutoff into a config option
# the annualized return that will be the minimum risk free return
# (as defined as max_loss / initial_investment + 1) that will be saved
annual_return_cutoff = 1.08


def process_option_expiration_data(
    df: pd.DataFrame
) -> Tuple[pd.DataFrame, Union[None, pd.DataFrame]]:
    """
    Hook to do any time-sensitive processing. This function will be called directly after
    any option data is scraped off of the webpage. This function should return a tuple with
    the first record always being the original dataframe

    df should only have one unique tckr, quote_ts and expiration
    """
    quote_ts = pd.Timestamp(df.quote_ts.values[0])
    ex_date = pd.Timestamp(df.expiration.values[0])
    ex_ts = pd.Timestamp(
        year=ex_date.year,
        month=ex_date.month,
        day=ex_date.day,
        hour=15,
        minute=30,
        second=0,
    )
    days_to_expire = (ex_ts - quote_ts) / pd.to_timedelta(1, unit="D")
    rf_df: Union[None, pd.DataFrame] = None
    if not df.empty:
        rf_df = find_risk_free_trade(df)
        if rf_df is not None:
            # if initial_investment is zero or negative, approximate the investment
            # as $0.01 ($1 txn fee for 100 contracts)
            rf_df["min_ann_return"] = np.power(
                1 + (rf_df.max_loss / np.maximum(rf_df.initial_investment, 0.01)),
                365 / float(days_to_expire),
            )
            rf_df["max_ann_return"] = np.power(
                1 + (rf_df.max_profit / np.maximum(rf_df.initial_investment, 0.01)),
                365 / float(days_to_expire),
            )
            rf_df = rf_df.loc[
                np.logical_or(
                    rf_df.min_ann_return >= annual_return_cutoff,
                    rf_df.max_ann_return >= annual_return_cutoff,
                ),
                :,
            ].sort_values("min_ann_return", ascending=False)
            if not rf_df.empty:
                print(
                    f"{len(rf_df)} records with annual return "
                    f"from {rf_df.min_ann_return.min()} and {rf_df.min_ann_return.max()}"
                )
                print(rf_df)

                cumulative_risk_free.append(rf_df)

    return df, rf_df
