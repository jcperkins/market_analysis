import datetime
import pandas as pd
import src.Utility as Util
from src.DataSource.sql_db.Models import LIBOR
from src.DataSource.Web import create_web_session

"""
need to look into quandle
* FRED/USDONTD156N - Overnight
* FRED/USD1WKD156N - 1w
* FRED/USD2WKD156N - 2w
* FRED/USD2MTD156N - 2m
* FRED/USD4MTD156N - 4m
* FRED/USD5MTD156N - 5m
* FRED/USD7MTD156N - 7m
* FRED/USD8MTD156N - 8m
* FRED/USD9MTD156N - 9m
* FRED/USD10MD156N - 10m
* FRED/USD11MD156N - 11m

or from ice...
https://www.theice.com/marketdata/reports/icebenchmarkadmin/ICELiborHistoricalRates.shtml?excelExport=&criteria.reportDate=12%2F17%2F19&reportId=170&criteria.currencyCode=USD

or from FRED
https://fred.stlouisfed.org/search?st=LIBOR
fred_api = 'a15705a51423767789dce53745ef63cd'
"""


def update_libor_rates(includeToday: bool = False):
    max_date = LIBOR.get_max_date()
    start = max_date + datetime.timedelta(days=1)
    if includeToday:
        end = Util.formatDateInput(datetime.date.today())  # type: ignore
    else:
        end = Util.formatDateInput(  # type: ignore
            datetime.date.today() - datetime.timedelta(days=1)
        )
    if start < end:
        dates = pd.date_range(start=start, end=end, freq="d")
        try:
            # Query from the online database
            s = create_web_session()
            if isinstance(dates, pd.Timestamp):
                dates = pd.DatetimeIndex(dates)
            rate_table = pd.concat(
                [self.scrape_libor_data(x, s) for x in dates], axis=1, sort=True
            ).T
            rate_data = self.queryLiborRates(dates)
            self.storage.open()
            self.storage.append(
                dbKey, rate_data, format="table", data_columns=self.data_columns
            )
            self.storage.close()
        except Exception as message:
            print(message)
            # self.formatStorageDB()
    return
