from sqlalchemy.orm import Session
from sqlalchemy.sql import ClauseElement


def scrub(table_name):
    """removes all characters that are not '_' or alphanumeric"""
    return "".join(c for c in table_name if c.isalnum() or c == "_")


def condition_exists(session, model, **kwargs) -> bool:
    """
    returns True if a row matching the kwargs conditions can be found

    :param Session session:
    :param model: an ORM object mapped to one of the DB tables
    :param kwargs:
    :return:
    """
    # noinspection PyArgumentList
    return session.query(model).filter_by(**kwargs).first() is not None


def get_or_create(session, model, defaults=None, **kwargs):
    """
    Returns the sqlalchemy ORM object and adds it to the db if does not already exist
    :param Session session:
    :param model: an ORM object mapped to one of the DB tables
    :param defaults:
    :param kwargs:
    :return:
    """
    # noinspection PyArgumentList
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict(
            (k, v) for k, v in kwargs.items() if not isinstance(v, ClauseElement)
        )
        params.update(defaults or {})
        # noinspection PyArgumentList
        instance = model(**params)
        session.add(instance)
        return instance, True
