from sqlalchemy import Column, Float, DateTime, String, Integer, UniqueConstraint
from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import Session
import pandas as pd

from ..Common import condition_exists
from ..connect_info import Base


class OptionPrice(Base):
    """ A quote for an Option off of the fidelity web page """

    __tablename__ = "options"
    # an option quote is unique if the quote timestamp and symbol are unique
    # the symbol is calculated from the underlying tckr, expiration date and strike prices
    __table_args__ = (UniqueConstraint("quote_ts", "symbol"),)

    # columns
    id = Column(Integer, primary_key=True)
    quote_ts = Column(DateTime, nullable=False)
    symbol = Column(String, nullable=False)
    tckr = Column(String, nullable=False)
    expiration = Column(DateTime, nullable=False)
    option_type = Column(String, nullable=False)
    strike = Column(Float, nullable=False)
    stock_price = Column(Float)
    last = Column(Float)
    change = Column(Float)
    bid = Column(Float)
    ask = Column(Float)
    bid_size = Column(Float)
    ask_size = Column(Float)
    volume = Column(Float)
    open_int = Column(Float)
    imp_vol = Column(Float)
    delta = Column(Float)
    gamma = Column(Float)
    theta = Column(Float)
    vega = Column(Float)
    rho = Column(Float)
    days_to_expiration = Column(Float)

    # static methods
    @staticmethod
    def upload_df(
        df: pd.DataFrame, session: Session, commit: bool = True, debug: bool = False
    ):
        """
        Attempts to load the dataframe through the pandas to_sql function but if any records are
        already in the table, it will catch the error and add each new/unique row individually

        :param df: a dataframe of option data. It must have the columns:
            ['quote_ts', 'symbol', 'tckr', 'expiration', 'option_type', 'strike', 'stock_price',
             'last', 'change', 'bid', 'ask', 'bid_size', 'ask_size', 'volume', 'open_int', 'imp_vol',
             'delta', 'gamma', 'theta', 'vega', 'rho', 'days_to_expiration']
        :param session: an open sqlalchemy session to the database
        :param commit: boolean flag to determine whether or not to commit the data to the db after loading it
        :param debug: print the status states as the function progresses
        :return: a pandas series of boolean values indicating which records were new and added to the table
        """
        # if empty df then skip
        if df.shape[0] == 0:
            return pd.Series(index=df.index)

        try:
            if debug:
                print("Attempting to load the entire DataFrame through pandas")
            # write the data to the db
            df.to_sql(
                "options", con=session.get_bind(), if_exists="append", index=False
            )
            if debug:
                print("Successfully Loaded the entire dataframe")
            return pd.Series(data=[True] * len(df.index), index=df.index)
        except DBAPIError:
            if debug:
                print(
                    "df had rows that already existed in the database, adding each row individually"
                )
            qty_added = 0
            qty_skipped = 0
            qty_processed = 0
            total_rows = df.shape[0]

            def add_option(r: pd.Series) -> bool:
                """adds the row of the dataframe only if the record is not already in the table"""
                nonlocal qty_added, qty_processed, qty_skipped, total_rows
                qty_processed += 1
                o = OptionPrice(**r.to_dict())
                if not o.is_in_table(session):
                    session.add(o)
                    qty_added += 1
                    out = True
                else:
                    qty_skipped += 1
                    out = False

                if qty_processed % 1000 == 0:
                    print(
                        f"Row {qty_processed} of {total_rows} - {qty_added} added and {qty_skipped} skipped"
                    )

                return out

            # load each row of the data
            result = df.apply(add_option, axis=1)
            if commit:
                session.commit()
            return result

    # relationships

    # methods
    def is_in_table(self, session: Session) -> bool:
        """
        is_in_table wraps the condition_exists helper function. An option quote is unique if the
        quote timestamp and symbol are unique and the symbol is calculated from the underlying
        tckr, expiration date and strike prices
        """
        return condition_exists(
            session, OptionPrice, quote_ts=self.quote_ts, symbol=self.symbol
        )

    def __repr__(self):
        return (
            f"<OptionPrice(id={self.id}, quote_ts={self.quote_ts}, symbol={self.symbol},"
            f" tckr={self.tckr}, expiration={self.expiration}, option_type={self.option_type},"
            f" strike={self.strike}, stock_price={self.stock_price}, last={self.last}, change={self.change},"
            f" bid={self.bid}, ask={self.ask}, bid_size={self.bid_size}, ask_size={self.ask_size},"
            f" volume={self.volume}, open_int={self.open_int}, imp_vol={self.imp_vol}, delta={self.delta},"
            f" gamma={self.gamma}, theta={self.theta}, vega={self.vega}, rho={self.rho},"
            f" days_to_expiration={self.days_to_expiration}"
            f")>"
        )
