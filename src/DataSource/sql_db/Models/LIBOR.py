from typing import Optional, Dict

import pandas as pd
from sqlalchemy import Column, Float, Integer, Date, UniqueConstraint
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from ..Common import condition_exists
from ..connect_info import Base

""" a easy reference dictionary of the types of each column in the table """
sql_dtypes = {"id": Integer, "date": Date, "duration": Float, "rate": Float}


class LIBOR(Base):
    """ A LIBOR interest rate for a given loan duration """

    __tablename__ = "libor"
    # a LIBOR rate is unique if the date and duration are unique
    __table_args__ = (UniqueConstraint("date", "duration"),)

    # columns
    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    duration = Column(Float, nullable=False)
    rate = Column(Float, nullable=False)

    # relationships
    # static helper functions
    @staticmethod
    def get_min_date(session: Session) -> pd.Timestamp:
        """ return the first date for the tckr provided """
        return pd.Timestamp(session.query(func.min(LIBOR.date)).scalar())

    @staticmethod
    def get_max_date(session: Session) -> pd.Timestamp:
        """ return the last date for the tckr provided """
        return pd.Timestamp(session.query(func.max(LIBOR.date)).scalar())

    @staticmethod
    def get_valid_date(session: Session, ts: Optional[pd.Timestamp]) -> pd.Timestamp:
        """
        returns the date value <= date that has valid data.

        :param session:
        :param ts: date in a pandas Timestamp format that will be matched to a valid row in the db
        :return: the pandas timestamp that indicates which row has the most recent valid data
        """
        if ts:
            return pd.Timestamp(
                session.query(func.max(LIBOR.date))
                .filter(LIBOR.date <= pd.Timestamp(ts).date())
                .scalar()
            )
        else:
            return LIBOR.get_max_date(session)

    # methods
    def is_in_table(self, session: Session) -> bool:
        """is_in_table wraps the condition_exists helper function. A LIBOR rate is unique if the date
        and duration are unique."""
        return condition_exists(session, LIBOR, date=self.date, duration=self.duration)

    def __repr__(self):
        return f"<LIBOR(id={self.id}, date={self.date}, duration={self.duration}, rate={self.rate})>"
