from typing import Optional

import pandas as pd
from sqlalchemy import Column, Float, Integer, UniqueConstraint, or_, and_
from sqlalchemy.orm import Session

from ..Common import condition_exists
from ..connect_info import Base


class RateCPI(Base):
    """ A CPI inflation rate for a given month """

    __tablename__ = "cpi_rates"
    # a CPI inflation rate is unique if the year and month are unique
    __table_args__ = (UniqueConstraint("year", "month"),)

    # columns
    id = Column(Integer, primary_key=True)
    year = Column(Integer, nullable=False)
    month = Column(Integer, nullable=False)
    rate = Column(Float, nullable=False)

    # relationships

    # methods
    def is_in_table(self, session: Session) -> bool:
        """is_in_table wraps the condition_exists helper function. A CPI inflation rate is unique if
        the year and month are unique."""
        return condition_exists(session, RateCPI, year=self.year, month=self.month)

    def __repr__(self):
        return f"<RateCPI(id={self.id}, year={self.year}, month={self.month}, rate={self.rate})>"

    # Static Functions
    @staticmethod
    def get_last(session: Session) -> "RateCPI":
        return (
            session.query(RateCPI)
            .order_by(RateCPI.year.desc(), RateCPI.month.desc())
            .first()
        )

    @staticmethod
    def get_previous(session: Session, ts: Optional[pd.Timestamp] = None) -> "RateCPI":
        if ts is None:
            return RateCPI.get_last(session)

        return (
            session.query(RateCPI)
            .filter(
                or_(
                    RateCPI.year < ts.year,
                    and_(RateCPI.year == ts.year, RateCPI.month <= ts.month),
                )
            )
            .order_by(RateCPI.year.desc(), RateCPI.month.desc())
            .first()
        )
