from typing import TypeVar, Dict, Type, Iterable, List

import pandas as pd
from . import Generic_ORM
from collections import Counter

TOrmInstance = TypeVar("TOrmInstance", bound=Generic_ORM)


def orm_to_dict(record: Generic_ORM) -> Dict:
    return {col.name: getattr(record, col.name) for col in record.__table__.columns}


def orm_to_series(record: Generic_ORM) -> pd.Series:
    return pd.Series(orm_to_dict(record))


def orm_from_dict(orm_cls: Type[TOrmInstance], d: Dict) -> TOrmInstance:
    return orm_cls(**d)


def orm_from_series(orm_cls: Type[TOrmInstance], s: pd.Series) -> TOrmInstance:
    return orm_cls(**s.to_dict())


def orm_list_to_df(orm_list: Iterable[Generic_ORM], **kwargs) -> pd.DataFrame:
    """
    returns a dataframe from a list of ORM model instances (i.e. records)

    :param orm_list: list of ORM Model instances that need to be a dataframe
    :param kwargs: key word arguments that will be passed to pd.DataFrame.from_records
    :return: dataframe of ORM data
    """
    return pd.DataFrame.from_records([orm_to_dict(x) for x in orm_list], **kwargs)


def orm_list_from_df(
    orm_cls: Type[TOrmInstance], df: pd.DataFrame
) -> List[TOrmInstance]:
    """
    converts a dataframe of ORM data to a list of ORM model instances (i.e. records)

    :param orm_cls:
    :param df: dataframe of ORM data
    :return: list of ORM records
    """
    cols = df.columns
    return [orm_cls(**dict(zip(cols, row))) for row in df[cols].values]


def orm_equal(o1: Generic_ORM, o2: Generic_ORM) -> bool:
    return type(o1) == type(o2) and Counter(orm_to_dict(o1)) == Counter(orm_to_dict(o2))
