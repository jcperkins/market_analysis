from typing import Optional

import pandas as pd
from sqlalchemy import Column, String, Float, Date, UniqueConstraint, Integer
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from ..Common import condition_exists
from ..connect_info import Base

""" a easy reference dictionary of the types of each column in the table """
sql_dtypes = {
    "id": Integer,
    "tckr": String,
    "date": Date,
    "open": Float,
    "high": Float,
    "low": Float,
    "close": Float,
    "adj_close": Float,
    "volume": Float,
    "dividends": Float,
    "split_factor": Float,
}


class StockPrice(Base):
    """ An end of day quote for a stock tckr """

    __tablename__ = "stock_price"
    # a stock price is unique if the tckr and date of price are unique
    __table_args__ = (UniqueConstraint("tckr", "date"),)

    # columns
    id = Column(Integer, primary_key=True)
    tckr = Column(String, nullable=False)
    date = Column(Date, nullable=False)
    open = Column(Float, nullable=False)
    high = Column(Float, nullable=False)
    low = Column(Float, nullable=False)
    close = Column(Float, nullable=False)
    adj_close = Column(Float, nullable=False)
    volume = Column(Float, nullable=False)
    dividends = Column(Float, nullable=False)
    split_factor = Column(Float)

    # relationships

    # static helper functions
    @staticmethod
    def get_min_date(session: Session, tckr: str) -> pd.Timestamp:
        """ return the first date for the tckr provided """
        return pd.Timestamp(
            session.query(func.min(StockPrice.date))
            .filter(StockPrice.tckr == tckr)
            .scalar()
        )

    @staticmethod
    def get_max_date(session: Session, tckr: str) -> pd.Timestamp:
        """ return the last date for the tckr provided """
        return pd.Timestamp(
            session.query(func.max(StockPrice.date))
            .filter(StockPrice.tckr == tckr)
            .scalar()
        )

    @staticmethod
    def get_valid_date(
        session: Session, tckr: str, ts: Optional[pd.Timestamp]
    ) -> pd.Timestamp:
        """
        returns the date value <= date that has valid data.

        :param session:
        :param tckr: the stock tckr to search under
        :param ts: date in a pandas Timestamp format that will be matched to a valid row in the db
        :return: the pandas timestamp that indicates which row has the most recent valid data
        """
        if ts:
            return pd.Timestamp(
                session.query(func.max(StockPrice.date))
                .filter(
                    StockPrice.tckr == tckr, StockPrice.date <= pd.Timestamp(ts).date()
                )
                .scalar()
            )
        else:
            return StockPrice.get_max_date(session, tckr)

    # methods
    def is_in_table(self, session: Session) -> bool:
        """is_in_table wraps the condition_exists helper function. A stock price is unique if
        the tckr and date of price are unique."""
        return condition_exists(session, StockPrice, tckr=self.tckr, date=self.date)

    def __repr__(self):
        return (
            f"<StockPrice(id={self.id}, tckr={self.tckr}, date={self.date}, open={self.open},"
            f" high={self.high}, low={self.low}, close={self.close}, adj_close={self.adj_close},"
            f" volume={self.volume}, dividends={self.dividends}, split_factor={self.split_factor}"
            f")>"
        )
