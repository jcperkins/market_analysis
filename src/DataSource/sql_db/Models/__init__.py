from typing import Union, Type

from .LIBOR import LIBOR
from .OptionPrice import OptionPrice
from .RateCPI import RateCPI
from .StockPrice import StockPrice

Generic_ORM = Union[StockPrice, RateCPI, OptionPrice, LIBOR]
Generic_ORM_Constructor = Union[
    Type[StockPrice], Type[RateCPI], Type[OptionPrice], Type[LIBOR]
]
