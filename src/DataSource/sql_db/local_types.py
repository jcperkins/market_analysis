from typing import Union

from sqlalchemy import Column
from sqlalchemy.orm.attributes import InstrumentedAttribute

TColumnObj = Union[Column, InstrumentedAttribute]
