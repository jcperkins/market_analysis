"""
connect_info.py

Location for the default connection objects of the project. This will have several
ORM objects that should probably be isolated into the ORM directory but are included
here for the convenience of having all of these objects located in the same file
* the engine and sessionFactory are not initialized until `set_engine` is called
* once initialized, use `session_factory()` to get a new Session

Default Values:
* the default `echo` setting is True
* the default DB path is inside a 'Data' directory in the current working directory
"""
# import sqlite3
from pathlib import Path
from typing import Union

from sqlalchemy import create_engine, engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

Base = declarative_base()
# noinspection PyUnresolvedReferences
from . import Models

# Default values
echo = True
db_path: Union[str, Path] = Path.cwd().joinpath("Data", "MarketData.sqlite")
sql_engine = None
_SessionFactory = None


def _validate_db_path(p: Union[str, Path] = None) -> Union[Path, str]:
    """
    if none then returns the string to create a path in memory. Ensures that the parent directory
    and the db file exist
    :param p:
    :return:
    """
    if p is None or p == ":memory:" or p == "":
        return ":memory:"

    p = Path(p).resolve()
    if not p.is_file():
        if not p.parent.is_dir():
            print("Making root directory for database:", str(p.parent))
            p.parent.mkdir()
        # print("Initializing an empty db file: ", p)
        # sqlite3.connect(
        #     p, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
        # )

    return p


def get_engine() -> engine:
    """Returns the global engine object if it is set or sets it with the default values if not set"""
    return sql_engine if sql_engine is not None else set_engine()


def set_engine(fpath: Union[str, Path] = "default", echo_parm: bool = None):
    global echo, db_path, sql_engine, _SessionFactory
    # don't change the global params if echo_parm is None or fpath is 'default'
    if echo_parm is not None:
        echo = echo_parm
    if fpath != "default":
        db_path = _validate_db_path(fpath)
        print("setting the DB path to", db_path)

    # set the global engine and _SessionFactory objects
    sql_engine = create_engine("sqlite:///" + str(db_path), echo=echo)
    # use session_factory() to get a new Session
    _SessionFactory = sessionmaker(bind=sql_engine)
    return sql_engine


def get_db_path():
    return db_path


def set_db_path(p: Union[str, Path]):
    """sets the db_path to the provided argument with no validation and returns the value"""
    global db_path
    db_path = p
    return db_path


def session_factory() -> Session:
    """Returns a session for the current engine/connection and creates one using the existing
    settings if one does not exist"""
    if not _SessionFactory:
        # set_engine will also assign a valid sessionmaker object to _SessionFactory
        set_engine()
    Base.metadata.create_all(sql_engine)
    # noinspection PyCallingNonCallable
    return _SessionFactory()


def get_metadata() -> MetaData:
    return Base.metadata
