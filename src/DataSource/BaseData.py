# -----------------------------------------------------------------------------
# Name:        BaseData
# Purpose:      Parent database class for accessing all types of data
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import datetime
import numpy as np
import pandas as pd
from .Mixins import BaseTempDBMixin, BaseStorageDBMixin
import src.Utility as Util
from typing import Dict, List, Optional, Union, overload, Type, TypeVar
from src.types import RawDateType, RawDateSingle, RawDateCollection, TorList, Scalar

TBaseData = TypeVar("TBaseData", bound="BaseData")


class BaseData(Util.metaclass_resolver(BaseTempDBMixin, BaseStorageDBMixin)):
    """
    All Databases need to have the following functions:
        o  loadFromDB
        o  getMinMaxDate
        o  getValidRows
        o  populateTempDB
        o  updateDB
    """

    def __init__(
        self,
        storename_or_basedata: Union[str, Type[TBaseData]],
        fileDir: Optional[str] = None,
    ):
        if isinstance(storename_or_basedata, str):
            storename = storename_or_basedata
            BaseStorageDBMixin.__init__(self, storename, fileDir=fileDir)
            BaseTempDBMixin.__init__(self, None)

            self.default_key = {
                "InflationData": "CPI",
                "LIBORData": "LIBOR",
                "TckrData": "ixSPX",
                "DailyOptionQuotesFidelity": "ixSPX",
            }[storename]

            # common tckr look-up lists
            # todo: these should really be stored in a look-up table somewhere
            self.commonTckrVariations: Dict[str, str] = {
                ".VIX": "^VIX",
                "$VIX": "^VIX",
                ".VXV": "^VXV",
                "$VXV": "^VXV",
            }
            self.no_update_keys: List[str] = ["SPXLFAKE", "XIV"]

            # properties about time of day context
            market_close = datetime.time(15, 30)
            self.simulationStartDate: pd.Timestamp = pd.Timestamp(1990, 1, 1)
            # noinspection PyArgumentList
            now = pd.Timestamp.today()
            self.lastHistoricalDate: pd.Timestamp = pd.Timestamp(now.date())
            if now.time() < market_close:
                self.lastHistoricalDate: pd.Timestamp = self.lastHistoricalDate - pd.Timedelta(
                    days=1
                )
            # Make sure that the last date is one that has a chance of the
            #  markets being open
            while self.lastHistoricalDate.weekday() > 4:
                self.lastHistoricalDate: pd.Timestamp = self.lastHistoricalDate - pd.Timedelta(
                    days=1
                )
        else:  # must be a basedata obj to copy
            copy_obj = storename_or_basedata
            BaseStorageDBMixin.__init__(self, copy_obj)
            BaseTempDBMixin.__init__(self, copy_obj)

            self.default_key: str = copy_obj.default_key

            # common tckr look-up lists
            self.commonTckrVariations: Dict[str, str] = copy_obj.commonTckrVariations
            self.no_update_keys: List[str] = copy_obj.no_update_keys

            # properties about time of day context
            self.simulationStartDate = copy_obj.simulationStartDate
            self.lastHistoricalDate = copy_obj.lastHistoricalDate

    def copy(self, basedata_instance) -> TBaseData:
        return BaseData(basedata_instance)

    def populateTempDB(
        self,
        keys: Optional[TorList[str]] = None,
        columns: Optional[TorList[str]] = None,
        start: Optional[RawDateSingle] = None,
        end: Optional[RawDateSingle] = None,
    ) -> TBaseData:
        return self.populate_tempDB_from_dict(
            self.subset_summary(keys=keys, start=start, end=end, columns=columns)
        )

    # if start is None, returns np.nan
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: None = None,
        end: Optional[Union[str, RawDateSingle]] = None,
        columns: Optional[List[str]] = None,
        source: Optional[str] = "storage",
    ) -> float:
        ...

    # returns scalar if start is a single date, end is none and columns is a string
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateSingle = None,
        end: None = None,
        columns: str = None,
        source: Optional[str] = "storage",
    ) -> Scalar:
        ...

    # returns series if start is a single date, end is none and columns is a list
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateSingle = None,
        end: None = None,
        columns: Optional[List[str]] = None,
        source: Optional[str] = "storage",
    ) -> pd.Series:
        ...

    # if columns is a a string and start and end are single dates
    # returns scalar if start == end
    # returns series if start != end
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateSingle = None,
        end: RawDateSingle = None,
        columns: str = None,
        source: Optional[str] = "storage",
    ) -> Union[str, float, int, pd.Timestamp, np.datetime64, pd.Series]:
        ...

    # returns series if start is datetimeIndex and column is a string (regardless of end)
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateCollection = None,
        end: Optional[RawDateSingle] = None,
        columns: str = None,
        source: Optional[str] = "storage",
    ) -> pd.Series:
        ...

    # returns dataframe if start is datetimeIndex and column is a list or None (regardless of end)
    @overload
    def loadFromDB(
        self,
        dbKey: str = "",
        start: RawDateCollection = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[List[str]] = None,
        source: Optional[str] = "storage",
    ) -> pd.DataFrame:
        ...

    def loadFromDB(
        self,
        dbKey: str = "",
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[TorList[str]] = None,
        source: str = "storage",
    ) -> Union[Scalar, pd.DataFrame, pd.Series]:
        """
        queries from the storage DB or the temp_db depending on source indicated
            Input conditions:
                o  start can be a single date or a
                    (list,array,pd.DatetimeIndex) of dates. If it is a list,
                    then only the matching dates will be returned
            returns a pd.DataFrame unless columns is a string and the
                name of a column, then it returns a pd.Series
        """
        if start is pd.NaT or start is None:
            return np.nan

        start = Util.formatDateInput(start)
        # todo: add functionality for end to be a string like in InflationData
        end = Util.formatDateInput(end)
        if source.upper() != "STORAGE" and self.tempDB is not None:
            data = self.loadFromDB_tempDB(start=start, end=end, columns=columns)
        else:
            data = self.loadFromDB_storage(
                dbKey=dbKey, start=start, end=end, columns=columns
            )

        # data should always be a DF at this point
        if type(columns) == str:
            if (
                (not isinstance(start, pd.DatetimeIndex))
                and (end is None or start == end)
                and (start in data.index)
            ):
                return data.loc[start, columns]  # returns a scalar
            else:
                return data.loc[:, columns]  # returns a series
        else:
            if (
                (type(start) != pd.DatetimeIndex)
                and (end is None or start == end)
                and (start in data.index)
            ):
                return data.loc[start, :]  # returns a series
            else:
                return data

    def updateDB(self) -> None:
        self.storage.open()
        for raw_key in self.storage.keys():
            raw_key = raw_key.replace("/", "")
            if raw_key not in self.no_update_keys:
                self.updateTable(raw_key)
        self.storage.close()
        return

    def updateTable(self, dbKey: str = "") -> None:
        pass
        return


def main():
    print("DataSource\\BaseData.py")


if __name__ == "__main__":
    main()
