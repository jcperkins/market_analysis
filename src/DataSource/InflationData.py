# -----------------------------------------------------------------------------
# Name:        InflationData.py
# Purpose:      Database management class for accessing inflation data
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import datetime
from typing import Dict, List, Optional, Tuple, TypeVar, Union

import numpy as np
import pandas as pd
from selenium import webdriver

import src.Utility as Util
from src.types import RawDateSingle, RawDateType

from .BaseData import BaseData
from .WebNavigator import InflationData_SeleniumNavigator

webdriver_delay = 0.005

TInflationData = TypeVar("TInflationData", bound="InflationData")


class InflationData(Util.metaclass_resolver(BaseData, InflationData_SeleniumNavigator)):
    # noinspection PyUnusedLocal,PyCallByClass
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
        existing_webdriver=None,
    ):
        BaseData.__init__(self, "InflationData", fileDir=fileDir)
        InflationData_SeleniumNavigator.__init__(self, existing_webdriver)
        self.default_key = "CPI"
        # temp_db has the same format as the hd5 table except that it has
        #  only the relevant data for the dates
        self.data_columns: List[str] = ["Year", "Month", "CPI"]
        self.data_types: Optional[
            List[str]
        ] = None  # value of None will allow pandas to infer

        self.extra_tempdb_cols: Optional[Dict[str, Tuple[float, RawDateType]]] = None
        self.referenceDate = pd.Timestamp(2017, 7, 15)

    @staticmethod
    def transformDate(
        date: RawDateType, day_of_month: Union[str, float, int] = "15"
    ) -> RawDateType:
        if type(day_of_month) == str:
            day_of_month = "%02i" % int(day_of_month)
        else:
            day_of_month = "%02i" % day_of_month
        date = Util.formatDateInput(date)
        if isinstance(date, pd.DatetimeIndex):
            transformed_date = Util.formatDateInput(
                (
                    date.year.astype("|S4")
                    + "-"
                    + date.month.astype("|S2")
                    + "-"
                    + [day_of_month] * len(date)
                ).unique()
            )
        elif date is not None:
            transformed_date = pd.Timestamp(date.year, date.month, int(day_of_month))
        else:
            transformed_date = date
        return transformed_date

    def getCPIForYearAndMonth(self, year, month, source="storage"):
        if source.lower() == "storage":
            valid_index = self.getValidIndices_storage(
                raw_dates=datetime.datetime(year, month, 15), columns="CPI"
            )
        else:
            valid_index = self.getValidIndices_tempDB(
                raw_dates=datetime.datetime(year, month, 15), columns="CPI"
            )
        return self.loadFromDB(
            start=valid_index, end=None, columns="CPI", source=source
        )

    def getCPIForDate(self, date, includeYearMonth=False, source="storage"):
        date = Util.formatDateInput(date)
        if date is None:
            date = self.referenceDate
        if type(date) == pd.DatetimeIndex:
            df_out = pd.DataFrame(
                data=np.stack([date.year, date.month], axis=1).astype(int),
                index=date,
                columns=["Year", "Month"],
            )
            if (
                date.min().year == date.max().year
                and date.min().month == date.max().month
            ):
                cpi = self.getCPIForYearAndMonth(
                    date.min().year, date.min().month, source=source
                )
                df_out["cpi"] = cpi
            else:
                min_date = self.transformDate(date.min())
                max_date = self.transformDate(date.max(), day_of_month="20")
                cpi_data = self.loadFromDB(start=min_date, end=max_date, source=source)
                df_out = pd.merge(df_out, cpi_data, how="left", on=["Year", "Month"])
                df_out.index = date
            if includeYearMonth:
                return df_out[["Year", "Month", "cpi"]]
            else:
                return df_out.CPI
        else:
            cpi = self.getCPIForYearAndMonth(date.year, date.month, source=source)
            if includeYearMonth:
                return date.year, date.month, cpi
            else:
                return cpi

    def inflationRatio(self, date, reference_date=None, source="storage"):
        date = Util.formatDateInput(date)
        if reference_date is None:
            reference_date = self.referenceDate
        reference_date = Util.formatDateInput(reference_date)
        if date is None:
            return
        refVal = self.getCPIForDate(reference_date, source=source)
        originalValue = self.getCPIForDate(date, source=source)
        ratio = originalValue / refVal
        return ratio

    def inflationAdjustedValue(
        self, date, refValue, reference_date=None, source="storage"
    ):
        return (
            self.inflationRatio(date, reference_date=reference_date, source=source)
            * refValue
        )

    def loadFromDB(
        self,
        dbKey: str = "CPI",
        start: Optional[RawDateType] = None,
        end: Optional[Union[str, RawDateSingle]] = None,
        columns: Optional[Union[str, List[str]]] = None,
        source: str = "storage",
    ) -> pd.DataFrame:
        """
            Will load the data including the start up to but not including the end
            end can also be the following strings "END", "LAST", "TODAY", "ALL", "START", "SAME" but cannot be a datetime index
        """
        if columns is None:
            columns: List[str] = []
        elif type(columns) == str:
            columns: List[str] = [columns]

        if start is None:
            if end is not None:
                # format the only valid date
                if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
                    end = Util.formatDateInput(datetime.date.today())
                elif type(end) == str and end.upper() in ["START", "SAME"]:
                    end = start
                else:
                    end = self.transformDate(
                        Util.formatDateInput(end), day_of_month="20"
                    )
        else:
            start = Util.formatDateInput(start)
            if isinstance(start, pd.Timestamp):
                if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
                    # noinspection PyArgumentList
                    end = pd.Timestamp.today()
                elif type(end) == str and end.upper() in ["START", "SAME"]:
                    end = start
                else:
                    end = Util.formatDateInput(end)
                    assert isinstance(end, pd.Timestamp)
                start = self.transformDate(start, day_of_month="15")
                end = self.transformDate(end, day_of_month="20")
            else:
                assert isinstance(start, pd.DatetimeIndex)
                start = self.transformDate(start, day_of_month="15")
                end = None

        return BaseData.loadFromDB(
            self, dbKey=dbKey, start=start, end=end, columns=columns, source=source
        )

    def updateTable(self, dbKey: str = "CPI", debug: bool = False) -> None:
        # find last year/month and determine if the storage needs to updated
        min_date, max_date = self.getMinMaxDate_storage()
        current_month_date = datetime.datetime.today()
        current_month_date = datetime.datetime(
            current_month_date.year, current_month_date.month, 15
        )
        # Update if a full calendar month has passed since the last record
        if (current_month_date - max_date).days > 90:
            rawCPIDF = self.scrape_CPI_data(debug=debug)
            rawCPIDF["Month"] = 0
            # The number of cells will be the number of years*12 because
            #  we do not care about the average columns
            qty = len(rawCPIDF.index) * 12
            tallSkinnyDF = pd.DataFrame(
                data=np.zeros((qty, 3)), columns=["Year", "Month", "CPI"], dtype=float
            )
            start_index: int = 0
            months: List[str] = [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
            ]
            for colName in rawCPIDF.columns:
                if colName in months:
                    colDF = rawCPIDF.loc[:, ["Year", "Month", colName]]
                    colDF["Month"] = Util.month2int(colName)
                    colDF.columns = ["Year", "Month", "CPI"]
                    tallSkinnyDF.loc[
                        start_index : start_index + len(colDF.index) - 1,
                        ["Year", "Month", "CPI"],
                    ] = np.array(colDF)
                    start_index += len(colDF.index)

            tallSkinnyDF["Year"] = tallSkinnyDF.Year.astype(int)
            tallSkinnyDF["Month"] = tallSkinnyDF.Month.astype(int)

            tallSkinnyDF.index = tallSkinnyDF.apply(
                lambda row: datetime.datetime(int(row.Year), int(row.Month), 15), axis=1
            )
            tallSkinnyDF = (
                tallSkinnyDF[~tallSkinnyDF.index.duplicated(keep="first")]
                .sort_index()
                .dropna()
            )

            self.storage.open()
            self.storage.put(
                "/CPI", tallSkinnyDF, format="Table", data_columns=self.data_columns
            )
            self.storage.close()
            self.close_driver()
        return


def main():
    print("DataSource\\InflationData.py")


if __name__ == "__main__":
    main()
