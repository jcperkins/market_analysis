# -----------------------------------------------------------------------------
# Name:        TreasuryData.py
# Purpose:      Database management class for accessing treasury data
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import os

import pandas as pd
import requests
from typing import Optional, Dict
from types import ModuleType

from config import default_settings

webdriver_delay = 0.005


class TreasuryData:
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
        webDriver: Optional[ModuleType] = None,
        webMethod: str = "requests",
    ):
        if proxies is None:
            proxies = default_settings.proxies

        self.storageName = "TreasuryData"
        self.workProxyBool = (
            workProxy if workProxy is not None else default_settings.work_proxy
        )
        self.mainDir = fileDir if fileDir is not None else default_settings.root_dir
        if self.workProxyBool and proxies is None:
            self.proxies = default_settings.proxies
            # {
            #    "http": "http://webproxy.ext.ti.com:80/",
            #    "https": "https://webproxy.ext.ti.com:80",
            #    "ftp": "http://webproxy.ext.ti.com:80",
            # }
        else:
            self.proxies = proxies
        self.session = requests.session()
        self.session.proxies = self.proxies
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", self.storageName + ".h5")
        )
        self.treasuryList = self.storage.keys()
        self.storage.close()
        self.data_columns = None
        self.tempDB = None
        # Backup web access
        self.webMethod = webMethod
        self.driver = webDriver
        self.chromedriver = default_settings.chromedriver
        self.phantomJSDriver = default_settings.phantomJSDriver


def main():
    print("DataSource\\TreasuryData.py")


if __name__ == "__main__":
    main()
