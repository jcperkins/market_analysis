import pandas as pd
from typing import Dict, Type


def df_has_types(df: pd.DataFrame, check_types: Dict[str, Type]) -> bool:
    existing_types: pd.Series = df.dtypes
    for col in check_types.keys():
        if col not in df.columns or existing_types[col].type != check_types[col]:
            return False
    return True


def storage_has_types(
    storage: pd.HDFStore, dbKey: str, check_types: Dict[str, Type]
) -> bool:
    """
    pull the top 5 rows and make sure that every column provided in
    check_types matches
    """
    # grab the top 5 rows then use the df_has_types function on the head of the table
    storage.open()
    out = df_has_types(storage.select(dbKey, stop=5), check_types)
    storage.close()
    return out


def push_dtypes_df(
    df: pd.DataFrame, new_types: Dict[str, Type], inplace: bool = True
) -> pd.DataFrame:
    res: pd.DataFrame = df if inplace else df.copy()
    original_types = res.dtypes
    df_cols = original_types.index
    for col in new_types.keys():
        assert col in df_cols
        if original_types[col].type != new_types[col]:
            res[col] = res[col].astype(new_types[col])
    return res


def reset_storage(storage, data_columns):
    """
    reads and puts the df into the storage object (useful in the transition from py2 to py3)
    """
    storage.open()
    for k in storage.keys():
        print("rewriting " + k)
        tempDF = storage[k]
        storage.put(k, tempDF, format="table", data_columns=data_columns)
    storage.close()
