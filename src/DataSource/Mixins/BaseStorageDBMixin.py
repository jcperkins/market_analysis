import datetime
import os
from typing import Dict, List, Optional, Tuple, Type, TypeVar, Union
from collections import Counter

import numpy as np
import pandas as pd

import src.Utility as Util
from src.types import RawDateSingle, RawDateType, TorList
from config import default_settings


TBaseStorageMixin = TypeVar("TBaseStorageMixin", bound="BaseStorageDBMixin")


class BaseStorageDBMixin(object):
    def __init__(
        self,
        name_or_obj: Union[str, Type[TBaseStorageMixin]],
        fileDir: Optional[str] = None,
    ):
        if isinstance(name_or_obj, str):
            self.storageName: str = name_or_obj
            self.mainDir: str = fileDir if fileDir is not None else default_settings.root_dir
            if not os.path.exists(os.path.join(self.mainDir, "Data")):
                os.makedirs(os.path.join(self.mainDir, "Data"))
            self.storage = pd.HDFStore(
                os.path.join(self.mainDir, "Data", "%s.h5" % self.storageName)
            )
            self.keyList: List[str] = [
                Util.convertTckrToUniversalFormat(x) for x in self.storage.keys()
            ]
            self.no_update_keys: List[str] = []
            if len(self.keyList) == 0:
                self.default_key: str = ""
                self.storageCols: List[str] = []
            else:
                self.default_key: str = self.keyList[0]
                self.storageCols = self.storage.select(
                    self.default_key, stop=2
                ).columns.tolist()
            self.data_columns: List[str] = []
            self.data_types: Optional[
                Dict[str, type]
            ] = None  # value of None will allow pandas to infer
        else:
            self.storageName = name_or_obj.storageName
            self.mainDir = name_or_obj.mainDir
            self.storage = name_or_obj.storage
            self.keyList: List[str] = list(name_or_obj.keyList)
            self.no_update_keys: List[str] = list(name_or_obj.no_update_keys)
            self.default_key: str = name_or_obj.default_key
            self.storageCols: List[str] = list(name_or_obj.storageCols)
            self.data_columns: List[str] = list(name_or_obj.data_columns)
            self.data_types: Optional[
                Dict[str, type]
            ] = None if name_or_obj.data_types is None else name_or_obj.data_types.copy()
        self.storage.close()

    def copy(self) -> TBaseStorageMixin:
        return BaseStorageDBMixin(self)

    def loadFromDB_storage(
        self,
        dbKey: str = "",
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[TorList[str]] = None,
    ) -> pd.DataFrame:
        """
        queries from the storage database
            Input conditions:
                o  start can be a single date or a
                    (list,array,pd.DatetimeIndex) of dates. If it is a list,
                    then only the matching dates will be returned
            always returns a pd.DataFrame additional formatting will be
                provided if the function is called through 'loadFromDB'
        """
        # format key
        if dbKey is None or dbKey == "":
            dbKey = self.default_key
        else:
            dbKey = Util.convertTckrToUniversalFormat(dbKey)
        # date formating
        start = Util.formatDateInput(start)
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = Util.formatDateInput(end)

        # logic for columns selection
        if columns is None or (isinstance(columns, list) and (len(columns) == 0)):
            columns = None
        elif type(columns) == str:
            columns = [columns]
        else:
            if len(columns) == 0 or Counter(columns) == Counter(self.storageCols):
                columns = None

        # Logic for row selection
        self.storage.open()
        if start is None and end is None:
            # return each row by setting index_coordinates to None
            index_coordinates = None
        else:
            index = self.storage.select_column("/" + dbKey, "index")
            if type(start) == pd.DatetimeIndex:
                index_coordinates = index.isin(start)
            else:
                if start == end:
                    end_select = True
                    start_select = index == start
                else:
                    if isinstance(start, pd.Timestamp):
                        start_select = index >= start
                    else:
                        start_select = True
                    if isinstance(end, pd.Timestamp):
                        end_select = index < end
                    else:
                        end_select = True
                index_coordinates = np.logical_and(start_select, end_select)
            if isinstance(index_coordinates, bool):
                if index_coordinates:
                    # shouldn't get here, but just in case... pull all rows
                    index_coordinates = None
                else:
                    if columns is None:
                        return pd.DataFrame(
                            pd.DatetimeIndex([]), columns=self.storageCols
                        )
                    else:
                        return pd.DataFrame(pd.DatetimeIndex([]), columns=columns)

        t_data = self.storage.select(dbKey, where=index_coordinates, columns=columns)
        self.storage.close()
        t_data = t_data[~t_data.index.duplicated(keep="first")].fillna(method="ffill")
        return t_data

    def subset_summary(
        self,
        keys: Optional[TorList[str]] = None,
        columns: Optional[TorList[str]] = None,
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
    ) -> Dict[str, pd.DataFrame]:
        """
        returns a dictionary where each key is one of the provided keys that will also correspond to one
        of the keys of the storage object and the value of each key is the column data for the provided columns
        :param keys: Optional[TorList[str]]
        :param columns: Optional[TorList[str]]
        :param start: Optional[RawDateType]
        :param end: Optional[RawDateType]
        :return: Dict[str, pd.DataFrame]
        """
        if keys is None:
            keys = self.keyList
        if columns is None:
            columns = self.storageCols
        out: Dict[str, pd.DataFrame] = {}
        for k in keys:
            out[k] = self.loadFromDB_storage(
                dbKey=k, start=start, end=end, columns=columns
            )
        return out

    def getMinMaxDate_storage(
        self, dbKey: str = ""
    ) -> Tuple[pd.Timestamp, pd.Timestamp]:
        if dbKey is None or dbKey == "":
            dbKey = self.default_key
        else:
            dbKey = Util.convertTckrToUniversalFormat(dbKey)
        self.storage.open()
        index: pd.DatetimeIndex = self.storage.select_column("/" + dbKey, "index")
        min_date: pd.Timestamp = index.min()
        max_date: pd.Timestamp = index.max()
        self.storage.close()
        return min_date, max_date

    def getValidDate_storage(
        self,
        date: pd.Timestamp,
        dbKey: str = "",
        columns: Optional[TorList[str]] = None,
        close_storage: bool = True,
    ) -> pd.Timestamp:
        """
        returns the index value <= date that has valid data in colname. If dbKey is None then it will
        use the default_key

        :param date: pd.Timestamp
        :param columns: Optional[str]
        :return: pd.Timestamp
        :param dbKey: Optional[str]
        :param close_storage: bool
        :return:
        """
        assert isinstance(date, pd.Timestamp)
        if dbKey is None or dbKey == "":
            dbKey = Util.convertTckrToUniversalFormat(self.default_key)
        else:
            dbKey = Util.convertTckrToUniversalFormat(dbKey)
        self.storage.open()
        if columns is None:
            index: pd.DatetimeIndex = self.storage.select_column("/" + dbKey, "index")
            if close_storage:
                self.storage.close()
            return index[index <= date].max()
        else:
            temp_df = self.storage.select(
                "/" + dbKey, where=["index <= date"], columns=columns
            )
            if close_storage:
                self.storage.close()
            return temp_df.index[~temp_df.isnull().any(axis=1)].max()

    def getValidIndices_storage(
        self,
        dbKey: str = "",
        raw_dates: Optional[RawDateType] = None,
        columns: Optional[TorList[str]] = None,
    ) -> pd.DatetimeIndex:
        """returns valid indices of the temp db object that also have valid records in the column
        input:
        o  date: specify the range of dates to consider. Default is None. If date is none,
        then return all data in the table that has a valid value for the indicated column. If
        date is a datetimeIndex then return the dates within the index that also have valid
        values for the column specified. If a single date, then the date is converted to a datetimeIndex
        first
        o  columns: column in the table to consider for whether the row has valid data

        :param dbKey: str
        :param raw_dates: Optional[RawDateType]
        :param columns: Optional[TorList[str]]
        :return: pd.DatetimeIndex"""
        if dbKey == "":
            dbKey = Util.convertTckrToUniversalFormat(self.default_key)
        else:
            dbKey = Util.convertTckrToUniversalFormat(dbKey)
        raw_dates = Util.formatDateArray(raw_dates)
        self.storage.open()
        if columns is None:
            out = self.storage.select_column("/" + dbKey, "index")
            if raw_dates is not None:
                out = np.intersect1d(out, raw_dates)
        else:
            if not isinstance(columns, list):
                columns = [columns]
            if raw_dates is None:
                temp_df = self.storage.select("/" + dbKey, columns=columns)
                out = temp_df.index[~temp_df.isnull().any(axis=1)].index
            elif len(raw_dates) == 0:
                out = []
            else:
                if len(raw_dates) == 1:
                    # alternative method is to use select as coordinates and
                    # pass to where clauseat the end
                    # where = self.storage.select_as_coordinates("/" + dbKey, "index == raw_dates[0]")
                    where = ["index == raw_dates[0]"]
                else:
                    # alternative method is to use select as coordinates and
                    # pass to where clauseat the end
                    # where = self.storage.select_as_coordinates(
                    #     "/" + dbKey, "index >= raw_dates.min() & index<= raw_dates.min()")
                    where = ["index >= raw_dates.min()", "index<= raw_dates.min()"]

                temp_df = self.storage.select("/" + dbKey, where=where, columns=columns)

                out = np.intersect1d(
                    temp_df.index[~temp_df.isnull().any(axis=1)], raw_dates
                )

        self.storage.close()
        return pd.DatetimeIndex(out)

    def getValidRows_storage(
        self, dbKey: str = "", dates: Optional[RawDateType] = None
    ) -> pd.DataFrame:
        """ returns data in the format of the storage file for the given
            tckr based on the date provided
            o  if date is a single time then func will return a pd.Series
                of the latest row in the DB that is still less than the
                date provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                rows that have finite prices
            o  if date is list or iterable object, then it will return
                a pd.DataFrame of all the rows of the DB that match one
                of the records. If no records match it will return an empty
                pd.DataFrame
        """
        valid_dates: pd.DatetimeIndex = self.getValidIndices_storage(
            dbKey=dbKey, raw_dates=dates, columns=None
        )
        if valid_dates is pd.NaT or len(valid_dates) == 0:
            return pd.DataFrame(columns=self.storageCols, index=pd.DatetimeIndex([]))
        return self.loadFromDB_storage(
            dbKey=dbKey, start=valid_dates, end=None, columns=None
        )

    def map_to_index(
        self,
        dates: RawDateType,
        columns: Optional[TorList[str]] = None,
        dbKey: Optional[str] = None,
    ) -> pd.DatetimeIndex:
        """
        will provide a 1-to-1 mapping of the dates in the index that are <= each date
        provided in the raw_dates parameter and have valid data for columns. If columns is None,
        then only records in the index will be used

        :param raw_dates: RawDateType
        :param columns: Optional[TorList[str]]
        :return: pd.DatetimeIndex
        """
        dates = Util.formatDateArray(dates)
        if columns is None or columns == []:
            columns = None
        elif isinstance(columns, str):
            columns = [columns]
        if dbKey is None or dbKey not in self.keyList:
            dbKey = self.default_key
        else:
            dbKey = Util.convertTckrToUniversalFormat(dbKey)

        self.storage.open()
        if columns is None:
            # get the whole index
            existing_index = self.storage.select_column("/" + dbKey, "index")
        else:
            # get the relevant columns
            existing_table = self.loadFromDB_storage(
                dbKey=dbKey,
                start=dates.min() - pd.Timedelta(30, "D"),
                end=dates.max() + pd.Timedelta(30, "D"),
                columns=columns,
            )
            # select only the index values with non-null data
            existing_index = existing_table.index[~existing_table.isnull().any(axis=1)]
        self.storage.close()

        index_union = dates.union(existing_index)
        date_mapping = pd.Series([pd.NaT] * len(index_union), index=index_union)
        date_mapping[existing_index] = existing_index
        return pd.DatetimeIndex(date_mapping.fillna(method="ffill")[dates].values)

        """date_mapping = pd.Series(dates, index=dates)
        b_unmatched = ~np.isin(dates, existing_index)
        date_mapping[b_unmatched] = pd.NaT
        # if pd.isnull(date_mapping[0]):
        #     date_mapping[0] = existing_index[existing_index < dates[0]].max()
        # return pd.DatetimeIndex(date_mapping.fillna(method="ffill").values)
        return pd.DatetimeIndex(
            date_mapping.apply(
                lambda x: x if pd.isnull()
            ).values
        ) """

    def formatStorageDB(
        self,
        reformatColumns: bool = False,
        columntypes: Optional[Union[List[Type], Dict[str, Type], Type]] = None,
        saveCSV: bool = False,
    ) -> TBaseStorageMixin:
        self.storage.open()
        for dbKey in self.storage.keys():
            df = self.storage["/" + dbKey]
            df = (
                df[~df.index.duplicated(keep="first")]
                .sort_index()
                .fillna(method="ffill")
            )
            if reformatColumns and columntypes is not None:
                if isinstance(columntypes, dict):
                    for col in columntypes.keys():
                        dataformat = columntypes[col]
                        if dataformat == float:
                            if df[col].dtype == np.object:
                                df.loc[df[col] == "null", col] = np.nan
                            df[col] = df[col].astype(float)
                        if df[col].dtype != dataformat:
                            df[col] = df[col].astype(dataformat)
                elif isinstance(columntypes, type) or (
                    isinstance(columntypes, list) and len(columntypes) == 1
                ):
                    # apply the type to every column
                    dataformat = (
                        columntypes if isinstance(columntypes, type) else columntypes[0]
                    )

                    for col in df.columns:
                        if dataformat == float:
                            if df[col].dtype == np.object:
                                df.loc[df[col] == "null", col] = np.nan
                            df[col] = df[col].astype(dataformat)
                        elif df[col].dtype != dataformat:
                            df[col] = df[col].astype(dataformat)
                        elif len(self.storageCols) == len(columntypes):
                            for colIndex in range(len(columntypes)):
                                dataformat = columntypes[colIndex]
                                if dataformat == float:
                                    if df[df.columns[colIndex]].dtype == np.object:
                                        df.loc[df[col] == "null", col] = np.nan
                                    df[col] = df[col].astype(dataformat)
                                elif df[col].dtype != dataformat:
                                    df[col] = df[col].astype(dataformat)
                else:
                    raise ValueError(
                        "Not enough dTypes provided in the list. "
                        + "%01i columns in the DB and only %01i "
                        % (len(self.storageCols), len(columntypes))
                        + "dtypes were provided"
                    )
            if saveCSV:
                save_dir = os.path.join(
                    self.mainDir, "Data", "BackUp", self.storageName + "CSV"
                )
                if not os.path.exists(save_dir):
                    os.makedirs(save_dir)
                print(f"Saving {dbKey} data to .csv")
                df.to_csv(os.path.join(save_dir, dbKey + ".csv"))
            self.storage.put(
                "/" + dbKey, df, format="Table", data_columns=self.data_columns
            )
        self.storage.close()
        return self

    def saveBackUpToCSV(self) -> TBaseStorageMixin:
        save_dir = os.path.join(
            self.mainDir, "Data", "BackUp", self.storageName + "CSV"
        )
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        self.storage.open()
        for tckr in self.storage.keys():
            fname = tckr.replace("/", "")
            print("Saving %s data to .csv" % fname)
            self.storage.get(tckr).to_csv(os.path.join(save_dir, fname + ".csv"))
        self.storage.close()
        return self

    def loadFromCSVBackup(self) -> TBaseStorageMixin:
        load_dir = os.path.join(
            self.mainDir, "Data", "BackUp", self.storageName + "CSV"
        )
        file_names = os.listdir(load_dir)
        self.storage.open()
        for fname in file_names:
            print("Loading data from %s" % fname)
            tckr = fname.split(".")[0]
            tckr_data = pd.read_csv(
                os.path.join(load_dir, fname),
                index_col=0,
                dtype=self.data_types,
                parse_dates=True,
            )
            self.storage.put(
                tckr, tckr_data, format="table", data_columns=self.data_columns
            )
        self.storage.close()
        return self
