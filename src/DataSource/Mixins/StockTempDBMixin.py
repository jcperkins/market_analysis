import datetime
from typing import Dict, List, Optional, Tuple, Type, TypeVar, Union

import numpy as np
import pandas as pd

import src.Utility as Util
from src.types import RawDateSingle, RawDateType, TorList

TStockTempDBMixin = TypeVar("TStockTempDBMixin", bound="StockTempDBMixin")


class StockTempDBMixin:
    def __init__(
        self,
        tempDB: Optional[
            Union[Type[TStockTempDBMixin], Dict[str, pd.DataFrame]]
        ] = None,
    ):
        # self.temp_db: Optional[Dict[str, pd.DataFrame]] = temp_db
        if tempDB is None:
            self.tempDB = None
            self.tempdb_cols: List[str] = []
            self.tempdb_keys: List[str] = []
        elif isinstance(tempDB, StockTempDBMixin):
            self.tempdb_keys: List[str] = list(tempDB.tempdb_keys)
            self.tempdb_cols: List[str] = list(tempDB.tempdb_cols)
            self.tempDB: Dict[str, pd.DataFrame] = {}
            for k in tempDB.tempDB.keys():
                self.tempDB[k] = tempDB.tempDB[k].copy()
        else:
            assert isinstance(tempDB, dict)
            self.tempDB: Dict[str, pd.DataFrame] = {}
            for k in tempDB.keys():
                self.tempDB[k] = tempDB[k].copy()
            self.tempdb_cols: List[str] = self.get_all_tempDB_columns()
            self.tempdb_keys: List[str] = [x for x in self.tempDB.keys()]

    def copy(self) -> TStockTempDBMixin:
        return StockTempDBMixin(self)

    def populate_tempDB_from_dict(
        self, storage_data: Dict[str, pd.DataFrame]
    ) -> TStockTempDBMixin:
        """
        always receive storage data as a dictionary where the key is dbKey of the storage object
        and the value is the Dataframe of the data. For stock, this will swap the columns and the keys
        so that each value of the dictionary is a dataframe of the data of the tckrs for
        each column of data i.e. key 'Close' will be linked to a DF of the close price for each tckr
        """
        tempDB: Dict[str, List[pd.Series]] = {}
        keys = set()
        self.tempdb_cols = [x for x in storage_data.keys()]
        for tckr in storage_data.keys():
            for col in storage_data[tckr].columns:
                if col not in tempDB:
                    tempDB[col] = []
                tempDB[col].append(storage_data[tckr][col].copy().rename(tckr))
        # now concat all of the series objects into a dataframe
        self.tempDB = {}
        for k in tempDB.keys():
            self.tempDB[k] = pd.concat(tempDB[k], axis=1, join="outer", sort=False)
        self.tempdb_keys = [x for x in self.tempDB.keys()]
        return self

    def get_all_tempDB_columns(self) -> List[str]:
        if self.tempDB is None:
            return []
        else:
            return list(
                set(
                    [
                        name
                        for key in self.tempDB.keys()
                        for name in self.tempDB[key].columns
                    ]
                )
            )

    def set_tempDB(
        self, newdata: Optional[Dict[str, pd.DataFrame]]
    ) -> TStockTempDBMixin:
        self.tempDB: Optional[Dict[str, pd.DataFrame]] = newdata
        if self.tempDB is None:
            self.tempdb_cols: List[str] = []
            self.tempdb_keys: List[str] = []
        else:
            self.tempdb_cols: List[str] = self.get_all_tempDB_columns()
            self.tempdb_keys: List[str] = [x for x in self.tempDB.keys()]
        return self

    def loadFromDB_tempDB(
        self,
        dbKey: str = "",
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
        data_names: Optional[Union[TorList[str], TorList[int], TorList[float]]] = None,
    ):
        """
        override the parent function because of the unique temp_db
        structure: queries from the temp_db. This function largely just exists
        for easy use with the generic loadFromDB function.
        For accessing the data for multiple tckrs by the type of data ('Close', 'Open', etc)
        pull from the temp_db directly as this is a condition that is unique to stock data
        Input conditions:
            o  if tckr is left empty, then the first tckr in self.tempDB_cols will be used
            o  start can be a single date or a (list,array,pd.DatetimeIndex)
                of dates. If it is a list, then only the matching dates
                will be returned
        returns a pd.DataFrame
        """
        assert self.tempDB is not None
        # process the tckr input
        if dbKey is None or dbKey == "":
            dbKey = self.tempdb_cols[0]
        dbKey = Util.convertTckrToUniversalFormat(dbKey)
        assert dbKey in self.tempdb_cols

        # process the date inputs
        start = Util.formatDateInput(start)
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = Util.formatDateInput(end)

        # format columns
        if data_names == [] or data_names is None:
            data_names = self.tempdb_keys
        elif type(data_names) == str:
            assert data_names in self.tempdb_keys
            data_names = [data_names]

        # Logic for the date ranges
        if start is None and end is None:
            # return every valid row for the tckr
            data_list = [self.tempDB[dn].loc[:, dbKey].rename(dn) for dn in data_names]
        elif isinstance(start, pd.DatetimeIndex):
            # pull the rows indicated by start independent of 'end'
            data_list = [
                self.tempDB[col].loc[start, dbKey].rename(col) for col in data_names
            ]
        else:
            if start == end:
                # only pull the data for the row indicated by start
                data_list = [
                    self.tempDB[col].loc[[start], dbKey].rename(col)
                    for col in data_names
                ]
            else:
                if start is None:
                    # end cannot be None (previous if statement) so get every row < end
                    data_list = [
                        self.tempDB[col]
                        .loc[self.tempDB[col].index < end, dbKey]
                        .rename(col)
                        for col in data_names
                    ]
                else:
                    # 'start' is not None but 'end' can still be None
                    if end is None:
                        data_list = [
                            self.tempDB[col]
                            .loc[self.tempDB[col].index >= start, dbKey]
                            .rename(col)
                            for col in data_names
                        ]
                    else:
                        data_list = [
                            self.tempDB[col]
                            .loc[
                                np.logical_and(
                                    self.tempDB[col].index >= start,
                                    self.tempDB[col].index < end,
                                ),
                                dbKey,
                            ]
                            .rename(col)
                            for col in data_names
                        ]
        # tckrData = pd.concat(data, axis=1, join_axes=[data[data_names[0]].index])
        tckrData = pd.concat(data_list, axis=1, join="outer", sort=False)
        return tckrData.loc[
            np.logical_and(
                np.logical_and(
                    ~tckrData.index.isna(), ~np.all(tckrData.isna(), axis=1)
                ),
                ~tckrData.index.duplicated(keep="first"),
            ),
            :,
        ]

    def getMinMaxDate_tempDB(
        self, dbKey: str = "", data_name: Optional[str] = None
    ) -> Tuple[pd.Timestamp, pd.Timestamp]:
        """
        return the first and last date in the table with valid data for the tckr provided
        """
        assert self.tempDB is not None
        if data_name is None:
            assert "Close" in self.tempDB
            data_name = "Close"
        assert data_name in self.tempDB
        if dbKey is None or dbKey == "" or dbKey not in self.tempDB[data_name].columns:
            valid_data = ~self.tempDB[data_name].isnull().any(axis=1)
            valid_index = self.tempDB[data_name].index[valid_data]
        else:
            valid_data = ~self.tempDB[data_name].loc[:, dbKey].isnull()
            valid_index = self.tempDB[data_name].index[valid_data]
        return valid_index.min(), valid_index.max()

    def getValidDate_tempDB(
        self,
        date: pd.Timestamp,
        tckr: str = "",
        columns: Optional[TorList[str]] = None,
        source: str = "STORAGE",
    ) -> pd.Timestamp:
        """
        returns the index value <= date that has valid data in colname.
        if columns is None, but pull the data for 'Close'

        :param date: date in a RawDateSingle format that will be matched to a valid row in the db
        :param columns: column name to check for valid data. Default is None
        :return: the date that indicates which row has the most recent valid data
        """
        date = Util.formatDateInput(date)
        assert self.tempDB is not None and isinstance(date, pd.Timestamp)
        if tckr is None or tckr == "":
            tckr = Util.convertTckrToUniversalFormat(self.tempdb_cols[0])
        else:
            tckr = Util.convertTckrToUniversalFormat(tckr)
        assert tckr in self.tempdb_cols

        if columns is None or (isinstance(columns, list) and len(columns) == 0):
            columns = ["Close"]
        elif isinstance(columns, str):
            columns = [columns]

        matched_index = self.tempDB[columns[0]].index[
            np.logical_and(
                self.tempDB[columns[0]].index <= date,
                ~pd.isnull(self.tempDB[columns[0]].loc[:, tckr]),
            )
        ]
        if len(columns) > 1:
            for c in columns[1:]:
                new_index = self.tempDB[c].index[
                    np.logical_and(
                        self.tempDB[c].index <= date, ~pd.isnull(self.tempDB[c][tckr])
                    )
                ]
                matched_index = matched_index.intersection(new_index)
        return matched_index.max()

    def getValidIndices_tempDB(
        self,
        tckr: str = "",
        raw_dates: Optional[RawDateType] = None,
        columns: Optional[TorList[str]] = None,
    ) -> pd.DatetimeIndex:
        """
        returns a datetimeindex of the dates in raw_dates a that have valid values for all of the provided columns
        of the temp db object
        input:
        o  date: specify the range of dates to consider. Default is None. If date is none,
        then return all data in the table that has a valid value for the indicated column. If
        date is a datetimeIndex then return the dates within the index that also have valid
        values for the column specified. If a single date, then the date is converted to a datetimeIndex
        first
        o  columns: column in the table to consider for whether the row has valid data

        :param raw_dates: Optional[RawDateType]
        :param columns: Optional[str]
        :return: pd.DatetimeIndex
        """
        assert self.tempDB is not None
        # interpret the tckr
        if tckr is None or tckr == "":
            tckr = Util.convertTckrToUniversalFormat(self.tempdb_cols[0])
        else:
            tckr = Util.convertTckrToUniversalFormat(tckr)
        assert tckr in self.tempdb_cols
        # interpret the columns argument
        if columns is None or (isinstance(columns, list) and len(columns) == 0):
            columns = ["Close"]
        elif isinstance(columns, str):
            columns = [columns]
        # interpret the raw_dates argumnet
        if raw_dates is None:
            raw_dates = self.tempDB[columns[0]].index[
                ~pd.isnull(self.tempDB[columns[0]][tckr])
            ]
        else:
            raw_dates = Util.formatDateArray(raw_dates)
        assert isinstance(raw_dates, pd.DatetimeIndex)

        matched_index = raw_dates.copy()
        for c in columns:
            matched_index = matched_index.intersection(
                self.tempDB[c].index[~pd.isnull(self.tempDB[c][tckr])]
            )
        return matched_index

    def getValidRows_tempDB(
        self, tckr: str = "", raw_dates: Optional[RawDateType] = None
    ) -> pd.DataFrame:
        """ returns data in the format of the storage file for the given
            tckr based on the date[s] provided
            o  if date is a single time then func will return a pd.Series
                of the latest row in the DB that is still less than the
                date provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                rows that have finite prices
            o  if date is list or iterable object, then it will return
                a pd.DataFrame of all the rows of the DB that match one
                of the records. If no records match it will return an empty
                pd.DataFrame
        """
        valid_dates: pd.DatetimeIndex = self.getValidIndices_tempDB(
            tckr=tckr, raw_dates=raw_dates, columns=None
        )
        if valid_dates is pd.NaT or len(valid_dates) == 0:
            return pd.DataFrame(columns=self.tempdb_cols, index=pd.DatetimeIndex([]))
        return self.loadFromDB_tempDB(
            dbKey=tckr, start=valid_dates, end=None, data_names=None
        )

    # todo:
    def map_to_index(
        self,
        raw_dates: RawDateType,
        tckr: str = "",
        columns: Optional[TorList[str]] = None,
    ) -> pd.DatetimeIndex:
        """
        will provide a 1-to-1 mapping of the dates in the index that are <= each date
        provided in the raw_dates parameter and have valid data for columns. If columns is None,
        then all columns will be used

        :param raw_dates: RawDateType
        :param columns: Optional[TorList[str]]
        :return: pd.DatetimeIndex
        """
        assert self.tempDB is not None
        if tckr is None or tckr == "":
            tckr = Util.convertTckrToUniversalFormat(self.tempdb_cols[0])
        else:
            tckr = Util.convertTckrToUniversalFormat(tckr)
        assert tckr in self.tempdb_cols
        assert raw_dates is not None
        raw_dates = Util.formatDateArray(raw_dates)
        if columns is None:
            columns = self.tempdb_keys
        elif type(columns) == str:
            columns = [columns]

        mapped_index = pd.Series(raw_dates, index=raw_dates, name="mapped")

        # all df's in temp_db should have the same index
        valid_index = raw_dates.union(self.tempDB[columns[0]].index)
        mapped_index = pd.Series(
            [pd.NaT] * len(valid_index), index=valid_index, name="mapped_index"
        )

        for col in columns:
            col_data = self.tempDB[col].loc[:, tckr]
            valid_index = valid_index.intersection(col_data.index[~pd.isnull(col_data)])

        mapped_index[valid_index] = valid_index
        return pd.DatetimeIndex(mapped_index.fillna(method="ffill")[raw_dates])
