import numpy as np
import pandas as pd
from typing import Optional


class StockCalcMixin(object):
    @staticmethod
    def adjustedPrice(
        stockDF: pd.DataFrame, initialAdjPrice: Optional[float] = None
    ) -> pd.Series:
        """
        calculate the adjusted price of the stock based off of the first
            close and resulting dividends.
        Final adj price will represent the value of 1 stock unit purchased
            at the first closing price with all subsequent dividends
            reinvested. Split factor is assumed to be 1 unless there
            is a split event

        o stock data is a pd.DataFrame with the minimum columns
            ['Close', 'Dividends', 'SplitFactor']
        o initial_adj_price (default=first close price) is the last valid
            adjPrice of a dataset that you will be appending to
            """
        # initialize all data
        closePrice = stockDF.Close
        dividends = stockDF.Dividends
        # splitFactor = stockDF.SplitFactor
        if initialAdjPrice is not None:
            initialUnits = initialAdjPrice / closePrice[0]
        else:
            initialUnits = 1

        equivalentQty = np.zeros(closePrice.shape)
        for rowNum in range(len(stockDF.index)):
            if rowNum == 0:
                equivalentQty[rowNum] = initialUnits
            else:
                # Removing the split factor multiplier because it the data is
                #  wrong for EFA and I haven't seen a case where it is right
                # equivalentQty[rowNum] = (equivalentQty[rowNum-1] +
                #  (equivalentQty[rowNum-1] * dividends[rowNum] /
                #  closePrice[rowNum]))) * splitFactor[rowNum]
                equivalentQty[rowNum] = equivalentQty[rowNum - 1] + (
                    equivalentQty[rowNum - 1] * dividends[rowNum] / closePrice[rowNum]
                )

        adjClose = closePrice * equivalentQty
        return adjClose

    @staticmethod
    def formatStockDataFrame(stockDF: pd.DataFrame) -> pd.DataFrame:
        for col in stockDF.columns:
            if stockDF[col].dtype == np.object:
                stockDF.loc[stockDF[col] == "null", col] = np.nan
                stockDF[col] = stockDF[col].astype(float)
            elif stockDF[col].dtype is not float:
                stockDF[col] = stockDF[col].astype(float)
        return stockDF.fillna(method="ffill")
