import datetime
from typing import Dict, List, Optional, Tuple, Type, TypeVar, Union

import numpy as np
import pandas as pd

import src.Utility as Util
from src.types import RawDateSingle, RawDateType, TorList

TBaseTempDBMixin = TypeVar("TBaseTempDBMixin", bound="BaseTempDBMixin")


class BaseTempDBMixin(object):
    def __init__(
        self,
        tempDB: Optional[
            Union[pd.DataFrame, Dict[str, pd.DataFrame], Type[TBaseTempDBMixin]]
        ] = None,
    ):
        if tempDB is None:
            self.tempDB = None
            self.tempdb_cols: List[str] = []
        elif isinstance(tempDB, BaseTempDBMixin):
            self.tempdb_cols = list(tempDB.tempdb_cols)
            self.tempDB = tempDB.tempDB.copy()
        elif isinstance(tempDB, pd.DataFrame):
            self.tempDB = tempDB.copy()
            self.tempdb_cols = list(tempDB.columns)
        else:
            assert isinstance(tempDB, dict)
            key_list = [x for x in tempDB.keys()]
            if len(key_list) == 0:
                self.tempDB = None
                self.tempdb_cols: List[str] = []
            else:
                assert len(key_list) == 1
                self.tempDB = tempDB[key_list[0]].copy()
                self.tempdb_cols = list(self.tempDB.columns)

    def copy(self) -> TBaseTempDBMixin:
        return BaseTempDBMixin(self)

    def populate_tempDB_from_dict(
        self, storage_data: Dict[str, pd.DataFrame]
    ) -> TBaseTempDBMixin:
        """
        always receive storage data as a dictionary where the key is dbKey of the storage object
        and the value is the Dataframe of the data. For base, this will largely a dummy function
        that stores the data in the internal temp_db so that future data requests are faster
        this function
        """
        assert type(storage_data) == dict
        keys = list(storage_data.keys())
        assert len(keys) > 0
        self.set_tempDB(storage_data[keys[0]])
        return self

    def set_tempDB(self, newdata: Optional[pd.DataFrame]) -> TBaseTempDBMixin:
        if isinstance(newdata, pd.DataFrame):
            self.tempDB = newdata.copy()
            self.tempdb_cols = list(newdata.columns)
        else:
            assert newdata is None
            self.tempDB = None
            self.tempdb_cols = []
        return self

    def loadFromDB_tempDB(
        self,
        start: Optional[RawDateType] = None,
        end: Optional[RawDateSingle] = None,
        columns: Optional[TorList[Union[str, int, float]]] = None,
    ) -> pd.DataFrame:
        """
        Will load the data including the start up to but not including the end.
        If both start and end are none then it will return the indicated columns

        Returns: DataFrame
            Always returns a pd.DataFrame. additional formatting will be provided
            if the function is called through 'loadFromDB'
        """
        start = Util.formatDateInput(start)
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = Util.formatDateInput(end)
        # process variations on columns
        if columns == [] or columns is None:
            columns = self.tempDB.columns
        if type(columns) in [str, int, float]:
            columns = [columns]
        # before I change the format of a string column check if I should
        #  pull the entire DB
        if start is None and end is None:
            # return the whole table
            data = self.tempDB.loc[:, columns]
            data = data[~data.index.duplicated(keep="first")].fillna(method="ffill")
            return data
        elif type(start) == pd.DatetimeIndex:
            index_coordinates = self.tempDB.index.isin(start)
        else:
            if start == end:
                end_select = True
                start_select = self.tempDB.index == start
            else:
                if start is not None:
                    start_select = self.tempDB.index >= start
                else:
                    start_select = True
                if end is not None:
                    end_select = self.tempDB.index < end
                else:
                    end_select = True
            index_coordinates = np.logical_and(start_select, end_select)
            if type(index_coordinates) == bool:
                index_coordinates = [index_coordinates] * len(self.tempDB.index)
        data = self.tempDB.loc[index_coordinates, columns]
        data = data[~data.index.duplicated(keep="first")].fillna(method="ffill")
        return data

    def getMinMaxDate_tempDB(self) -> Tuple[pd.Timestamp, pd.Timestamp]:
        """
        return the first and last date in the table
        """
        assert self.tempDB is not None
        return self.tempDB.index.min(), self.tempDB.index.max()

    def getValidDate_tempDB(
        self, date: pd.Timestamp, columns: Optional[TorList[str]] = None
    ) -> pd.Timestamp:
        """
        returns the index value <= date that has valid data in colname.

        :param date: date in a RawDateSingle format that will be matched to a valid row in the db
        :param columns: column name to check for valid data. Default is None
        :return: the date that indicates which row has the most recent valid data
        """
        assert self.tempDB is not None and isinstance(date, pd.Timestamp)
        if columns is None:
            return self.tempDB.index[self.tempDB.index <= date].max()
        else:
            if type(columns) == str:
                columns = [columns]
            return self.tempDB.index[
                np.logical_and(
                    self.tempDB.index <= date,
                    ~self.tempDB.loc[:, columns].isnull().any(axis=1),
                )
            ].max()

    def getValidIndices_tempDB(
        self,
        raw_dates: Optional[RawDateType] = None,
        columns: Optional[TorList[str]] = None,
    ) -> pd.DatetimeIndex:
        """
        returns a datetimeindex of the dates in raw_dates a that have valid values for all of the provided columns
        of the temp db object
        input:
        o  date: specify the range of dates to consider. Default is None. If date is none,
        then return all data in the table that has a valid value for the indicated column. If
        date is a datetimeIndex then return the dates within the index that also have valid
        values for the column specified. If a single date, then the date is converted to a datetimeIndex
        first
        o  columns: column in the table to consider for whether the row has valid data

        :param raw_dates: Optional[RawDateType]
        :param columns: Optional[str]
        :return: pd.DatetimeIndex
        """
        assert self.tempDB is not None
        if raw_dates is None:
            if columns is None:
                return self.tempDB.index
            else:
                if type(columns) == str:
                    columns = [columns]
                return self.tempDB.index[~self.tempDB[columns].isnull().any(axis=1)]
        else:
            raw_dates = Util.formatDateArray(raw_dates)
            if columns is None:
                return pd.DatetimeIndex(np.intersect1d(self.tempDB.index, raw_dates))
            else:
                if type(columns) == str:
                    columns = [columns]
                subset = self.tempDB.loc[raw_dates, columns]
                return subset.index[~subset.isnull().any(axis=1)]

    def getValidRows_tempDB(self, date: Optional[RawDateType] = None) -> pd.DataFrame:
        """ returns data in the format of the storage file for the given
            tckr based on the date provided
            o  if date is an empty list (i.e. []) then it will return all
                rows that have finite data
            o  if date is list or iterable object, then it will return
                a pd.DataFrame of all the rows of the DB that match one
                of the records. If no records match it will return an empty
                pd.DataFrame
        """
        valid_dates: pd.DatetimeIndex = self.getValidIndices_tempDB(
            raw_dates=date, columns=None
        )
        if valid_dates is pd.NaT:
            return pd.DataFrame(columns=self.tempDB.columns)
        return self.loadFromDB_tempDB(start=valid_dates, end=None, columns=None)

    def map_to_index(
        self, dates: RawDateType, columns: Optional[TorList[str]] = None
    ) -> pd.DatetimeIndex:
        """
        will provide a 1-to-1 mapping of the dates in the index that are <= each date
        provided in the raw_dates parameter and have valid data for columns. If columns is None,
        then all columns will be used

        :param raw_dates: RawDateType
        :param columns: Optional[TorList[str]]
        :return: pd.DatetimeIndex
        """
        assert self.tempDB is not None
        assert dates is not None
        dates = Util.formatDateArray(dates)
        if columns is None:
            columns = self.tempDB.columns
        elif type(columns) == str:
            columns = [columns]

        table_data = pd.DataFrame(columns=columns, index=dates)
        existing_data = self.tempDB.loc[self.tempDB.index.intersection(dates), columns]
        table_data.loc[existing_data.index, :] = existing_data.loc[:, :]
        table_data["final_index"] = table_data.apply(
            lambda r: self.getValidDate_tempDB(r.name, columns)
            if r.isnull().any()
            else r.name,
            axis=1,
        )
        return pd.DatetimeIndex(table_data["final_index"].values)
