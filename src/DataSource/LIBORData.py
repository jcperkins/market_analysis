# -----------------------------------------------------------------------------
# Name:        LIBORData.py
# Purpose:      Database management class for accessing all types of dat
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import datetime
import pandas as pd
import numpy as np
from .BaseData import BaseData
from .WebNavigator import WSJ_Navigator
import src.Utility as Util
from typing import Optional, Dict


class LIBORData(BaseData, WSJ_Navigator):
    def __init__(
        self,
        fileDir: Optional[str] = None,
        workProxy: Optional[bool] = None,
        proxies: Optional[Dict[str, str]] = None,
    ):
        BaseData.__init__(self, "LIBORData", fileDir=fileDir)
        WSJ_Navigator.__init__(self, workProxy=workProxy, proxies=proxies)
        self.default_key = "LIBOR"

    def getLIBOR_Rate(self, date, loanDays, source="storage"):
        # print(date, loanDays, source)
        date = Util.formatDateInput(date)
        if date is None:
            date = Util.formatDateInput(datetime.date.today())

        def interpolated_Rate(r, firstRateCol=0):
            colDaysList = [1, 7, 30, 60, 90, 180, 365]
            colDaysArray = np.array([1, 7, 30, 60, 90, 180, 365])
            # print(r)
            # assert r.LoanDays>=0, ("r.LoanDays < 0, " + str(r)
            #  + ' - ' + str(r.LoanDays) +  ' - ' + str(date)
            if r.LoanDays <= 0:
                return r[firstRateCol]
            lowIndex = colDaysList.index(colDaysArray[colDaysArray <= r.LoanDays].max())
            if r.LoanDays >= colDaysList[-1]:
                return r[firstRateCol + lowIndex]
            #            highIndex = lowIndex+1
            #            lowRate = r[1+lowIndex]
            #            highRate = r(1+highIndex)
            #            lowSpan = colDaysList[lowIndex]
            #            highSpan = colDaysList[highIndex]
            #            slope = (highRate-lowRate)/(highSpan-lowSpan)
            #            interpolation = (loanDays-lowSpan)*slope+lowRate
            return (
                (r.LoanDays - colDaysList[firstRateCol + lowIndex])
                * (r[firstRateCol + lowIndex + 1] - r[firstRateCol + lowIndex])
                / (
                    colDaysList[firstRateCol + lowIndex + 1]
                    - colDaysList[firstRateCol + lowIndex]
                )
                + r[firstRateCol + lowIndex]
            )

        if type(date) == pd.DatetimeIndex:
            if loanDays is None or type(loanDays) in [int, float]:
                dfOut = pd.DataFrame(
                    data=[loanDays] * len(date), index=date, columns=["LoanDays"]
                )
            else:
                if len(loanDays) == len(date):
                    dfOut = pd.DataFrame(
                        data=loanDays, index=date, columns=["LoanDays"]
                    )
                else:
                    raise ValueError(
                        "variable loanDays is not the same size " + "as variable date"
                    )
            rateData = self.loadFromDB(start=date, source=source)
            dfOut = pd.merge(
                dfOut,
                rateData,
                how="left",
                left_index=True,
                right_index=True,
                sort=True,
            )
            if rateData.index.min() != dfOut.index.min():
                firstRate = self.getValidRows_storage(
                    dbKey="LIBOR", date=dfOut.index.min()
                )
                dfOut.iloc[0, 1:] = firstRate
            dfOut = dfOut.fillna(method="ffill")
            if loanDays is None:
                # Return an extrapolated dataSet from the DB
                return dfOut[self.storageCols]
            dfOut["Rate"] = dfOut.apply(lambda x: interpolated_Rate(x), axis=1)
            return dfOut.Rate
        else:
            if loanDays is None:
                return
            else:
                row = self.getValidRows_storage("LIBOR", date)
                row["LoanDays"] = loanDays
                return interpolated_Rate(row)

    def queryAndStoreLiborRate(self, date: pd.Timestamp):
        """
        query and store the annualized LIBOR rate for 1d,1w,1m,2m,3m,6m,1yr
        """
        date = Util.formatDateInput(date)
        try:
            # Query from the online database
            rate = self.queryLiborRates(date)
            self.storage.open()
            self.storage.append(
                "LIBOR", rate, format="table", data_columns=self.data_columns
            )
            self.storage.close()
            return rate
        except Exception as message:
            print(message)
            return None

    def updateDB(self):
        return self.updateTable()

    def updateTable(self, dbKey: str = "LIBOR", includeToday: bool = False):
        """ depreciated due to the use of an extra screen on the ICE webpage"""
        minDate, maxDate = self.getMinMaxDate_storage(dbKey="LIBOR")
        start = maxDate + datetime.timedelta(days=1)
        if includeToday:
            end = Util.formatDateInput(datetime.date.today())  # type: ignore
        else:
            end = Util.formatDateInput(  # type: ignore
                datetime.date.today() - datetime.timedelta(days=1)
            )
        if start < end:
            dates = pd.date_range(start=start, end=end, freq="d")
            try:
                # Query from the online database
                rate_data = self.queryLiborRates(dates)
                self.storage.open()
                self.storage.append(
                    dbKey, rate_data, format="table", data_columns=self.data_columns
                )
                self.storage.close()
            except Exception as message:
                print(message)
                # self.formatStorageDB()
        return

    def initDB(self) -> pd.DataFrame:
        date = Util.formatDateInput("2010-01-01")
        # Query from the online database
        rate = self.queryLiborRates(date)
        self.storage.open()
        self.storage.put("LIBOR", rate, format="table", data_columns=self.data_columns)
        self.storage.close()
        self.updateTable()
        return rate

    # ------------------  Graveyard -------------------


def main():
    print("DataSource\\LIBORData.py")


if __name__ == "__main__":
    main()
