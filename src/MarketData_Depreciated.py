# -----------------------------------------------------------------------------
# Name:        MarketData
# Purpose:      Database management class for accessing all types of dat
#                required for stock market analysis
# Author:       Cory Perkins
#
# Created:      19/07/2017
# Copyright:   (c) Cory Perkins 2017
# Licence:     <your licence>
# -----------------------------------------------------------------------------

import time
import datetime
import os
import requests
import quandl
import pandas as pd
import numpy as np
from io import StringIO
from selenium import webdriver
from lxml import html
from scipy.interpolate import interp1d
import src.Utility as MH

quandl.ApiConfig.api_key = "CwCravWAoyv4zVzb5U-M"
webdriver_delay = 0.005
chromedriver = "C:\\Users\\cp035982\\Python\\SeleniumWebDrivers\\chromedriver.exe"
phantomJSDriver = (
    "C:\\Users\\cp035982\\Python\\SeleniumWebDrivers\\"
    + "phantomjs-2.1.1-windows\\bin\\phantomjs.exe"
)


class BaseData(object):
    """
    All Databases need to have the following functions:
        o  loadFromDB
        o  getMinMaxDate
        o  getValidRows
        o  populateTempDB
    """

    def __init__(
        self,
        storageName,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=False,
        proxies=None,
    ):
        if proxies is None:
            proxies = {}
        self.mainDir = fileDir
        # initialize the data connection parameters
        self.workProxyBool = workProxy
        if workProxy and not len(proxies):
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        else:
            self.proxies = proxies
        self.session = requests.session()
        self.session.proxies = self.proxies
        if workProxy:
            os.environ["HTTPS_PROXY"] = self.proxies["https"]
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", "%s.h5" % storageName)
        )
        self.keyList = self.storage.keys()
        self.storageCols = self.storage.select(self.keyList[0], stop=2).columns.tolist()
        self.tempDBCols = self.storageCols
        self.emptyDF = pd.DataFrame(columns=self.storageCols)
        self.emptyRow = pd.Series(index=self.storageCols)
        self.storage.close()
        self.data_columns = []
        marketClose = datetime.time(15, 30)
        self.simulationStartDate = datetime.datetime(1990, 1, 1)
        now = datetime.datetime.today()
        self.lastHistoricalDate = MH.formatDateInput(now.date())
        if now.time() < marketClose:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        # Make sure that the last date is one that has a chance of the
        #  markets being open
        while self.lastHistoricalDate.weekday() > 4:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        self.commonTckrVariations = {
            ".VIX": "^VIX",
            "$VIX": "^VIX",
            ".VXV": "^VXV",
            "$VXV": "^VXV",
        }
        self.fakeTckrs = ["SPXLFAKE"]
        # TempDB will data source that strategies can prepopulate for
        #  quicker access each dataSource will have it's own format based
        #  on the data structure
        self.tempDB = None

    def loadFromDB(
        self, dbKey=None, start=None, end=None, columns=None, source="Storage"
    ):
        """queries from the storage DB or the temp_db depending on source
            Input conditions:
                o  start can be a single date or a
                    (list,array,pd.DatetimeIndex) of dates. If it is a list,
                    then only the matching dates will be returned
            returns a pd.DataFrame unless columns is a string and the
                name of a column, then it returns a pd.Series"""
        if start is pd.NaT:
            return np.nan
        else:
            start = MH.formatDateInput(start)
            end = MH.formatDateInput(end)
        if source.upper() == "TEMPDB" and self.tempDB is not None:
            data = self.loadFromDB_TempDB(
                dbKey=dbKey, start=start, end=end, columns=columns
            )
        else:
            data = self.loadFromDB_Storage(
                dbKey=dbKey, start=start, end=end, columns=columns
            )
        # data should always be a DF at this point
        if type(columns) == str:
            if (
                (type(start) != pd.DatetimeIndex)
                and (start == end)
                and (start in data.index)
            ):
                data = data.loc[start, columns]
            else:
                data = data.loc[:, columns]
        else:
            if (
                (type(start) != pd.DatetimeIndex)
                and (start == end)
                and (start in data.index)
            ):
                data = data.loc[start, :]
        return data

    def loadFromDB_TempDB(self, dbKey=None, start=None, end=None, columns=None):
        """
        Will load the data including the start up to but not
        including the end"""
        if columns is None:
            columns = []
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        start = MH.formatDateInput(start)
        indexCoordinates = np.array([])
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = MH.formatDateInput(end)
        # process variations on columns
        if columns == [] or columns is None:
            columns = self.tempDB.columns
        if type(columns) in [str, int, float]:
            columns = [columns]
        # before I change the format of a string column check if I should
        #  pull the entire DB
        if start is None and end is None:
            # return the whole table
            data = self.tempDB.loc[:, columns]
            data = data[~data.index.duplicated(keep="first")].fillna(method="ffill")
            return data
        elif type(start) == pd.DatetimeIndex:
            indexCoordinates = self.tempDB.index.isin(start)
        else:
            if start == end:
                endSelect = True
                startSelect = self.tempDB.index == start
            else:
                if start is not None:
                    startSelect = self.tempDB.index >= start
                else:
                    startSelect = True
                if end is not None:
                    endSelect = self.tempDB.index < end
                else:
                    endSelect = True
            indexCoordinates = np.logical_and(startSelect, endSelect)
            if type(indexCoordinates) == bool:
                indexCoordinates = [indexCoordinates] * len(self.tempDB.index)
        data = self.tempDB.loc[indexCoordinates, columns]
        data = data[~data.index.duplicated(keep="first")].fillna(method="ffill")
        return data

    def loadFromDB_Storage(self, dbKey=None, start=None, end=None, columns=None):
        """queries from the storage database
            Input conditions:
                o  start can be a single date or a
                    (list,array,pd.DatetimeIndex) of dates. If it is a list,
                    then only the matching dates will be returned
            always returns a pd.DataFrame additional formatting will be
                provided if the function is called through 'loadFromDB"""
        if columns is None:
            columns = []
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        start = MH.formatDateInput(start)
        indexCoordinates = np.array([])
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = MH.formatDateInput(end)
        # format columns
        if (type(columns) == list and len(columns) == 0) or columns is None:
            columns = self.storageCols
        elif type(columns) == str:
            columns = [columns]
        self.storage.open()
        # if only pulling 1 column then it is still faster to use the
        #  storage.select(columns=columns) functinality then calling
        #  storage.select_columns() 2 times (Using temp_db is still
        #  almost 100 times faster)
        # logic for date selection
        index = self.storage.select_column("/" + dbKey, "index")
        if start is None and end is None:
            # return the whole table
            indexCoordinates = np.array([True] * len(index))
        elif type(start) == pd.DatetimeIndex:
            indexCoordinates = index.isin(start)
        else:
            if start == end:
                endSelect = True
                startSelect = index == start
            else:
                if start is not None:
                    startSelect = index >= start
                else:
                    startSelect = True
                if end is not None:
                    endSelect = index < end
                else:
                    endSelect = True
            indexCoordinates = np.logical_and(startSelect, endSelect)
            if type(indexCoordinates) == bool:
                indexCoordinates = np.array([indexCoordinates] * len(index))

        if len(indexCoordinates) > 0:
            if indexCoordinates.sum() == 0:
                # no rows fit the criteria
                if type(start) == pd.DatetimeIndex:
                    tckrData = pd.DataFrame(index=start, columns=columns)
                else:
                    tckrData = pd.DataFrame(
                        index=pd.DatetimeIndex([start]), columns=columns
                    )
            else:
                if sum(indexCoordinates) == 1:
                    # only 1 row needed
                    indexCoordinates = "index == " + str(index[indexCoordinates].values[0])
                if columns is None or columns == []:
                    # use only the index condition
                    tckrData = self.storage.select(dbKey, where=indexCoordinates)
                else:
                    # use both index and columns conditions
                    tckrData = self.storage.select(
                        dbKey, where=indexCoordinates, columns=columns
                    )
        else:
            if columns == self.storageCols:
                # use none of the conditions
                tckrData = self.storage.get(dbKey)
            else:
                # use only the columns condition
                tckrData = self.storage.select(dbKey, columns=columns)
        self.storage.close()
        tckrData = tckrData[~tckrData.index.duplicated(keep="first")].fillna(
            method="ffill"
        )
        return tckrData

    def populateTempDB(self, dbKey=None, start=None, end=None, columns=None):
        """ largely a dummy function that calls 'loadFromDB' and stores
        the result in the internal temp_db"""
        if columns is None:
            columns = []
        self.tempDB = self.loadFromDB(
            dbKey=dbKey, start=start, end=end, columns=columns, source="storage"
        )
        return self.tempDB

    def getMinMaxDate(self, dbKey):
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        self.storage.open()
        index = self.storage.select_column("/" + dbKey, "index")
        minDate = index.min()
        maxDate = index.max()
        self.storage.close()
        return minDate, maxDate

    def getValidIndices(self, dbKey=None, date=None, colName=None, source="Storage"):
        """ o  if date is an empty list [], then return all the indices as
                valid index options
            o  if date is a list of dates, then returns the subset of the
                    list that is contained in the DB index
            o  if just a date format then return the largest date in the
                    index that is still <= date and has a finite value for
                    the column
        """
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        date = MH.formatDateInput(date)
        minDate, maxDate = self.getMinMaxDate(dbKey)
        # quick check for gross validity
        if type(date) == pd.DatetimeIndex:
            if date.max() < minDate:
                return pd.NaT
            else:
                date = date[date >= minDate]
        else:
            if date < minDate:
                return pd.NaT
        noColumn = False
        if colName is None:
            noColumn = True
            if source.upper() == "STORAGE":
                colName = self.storageCols[0]
            else:
                colName = self.tempDBCols[0]
        self.storage.open()
        validDBSeries = self.loadFromDB(
            dbKey=dbKey, start=None, end=None, columns=colName, source=source
        )
        if not noColumn:
            validDBSeries = validDBSeries[validDBSeries.notnull()]
        # by this point validDBSeries is a series of only finite closing prices
        if type(date) == pd.DatetimeIndex:
            # prosessing for date lists -
            if len(date) == 0:
                validDates = validDBSeries.index
            else:
                validDates = np.intersect1d(validDBSeries.index, date)
        else:
            validDates = validDBSeries.index.asof(date)
        return validDates

    def getValidRows(self, dbKey=None, date=None, source="storage"):
        """ returns data in the format of the storage file for the given
            tckr based on the date provided
            o  if date is a single time then func will return a pd.Series
                of the latest row in the DB that is still less than the
                date provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                rows that have finite prices
            o  if date is list or iterable object, then it will return
                a pd.DataFrame of all the rows of the DB that match one
                of the records. If no records match it will return an empty
                pd.DataFrame
        """
        validDates = self.getValidIndices(
            dbKey=dbKey, date=date, colName=None, source=source
        )
        if validDates is pd.NaT:
            return np.nan
        return self.loadFromDB(
            dbKey=dbKey, start=validDates, end=validDates, source=source
        )

    def formatStorageDB(
        self,
        reformatColumns=False,
        columnTypes=None,
        saveCSV=False,
        parentDir=None,
        subDir=None,
        fName=None,
    ):
        if columnTypes is None:
            columnTypes = {}
        self.storage.open()
        for dbKey in self.storage.keys():
            df = self.storage["/" + dbKey]
            df = (
                df[~df.index.duplicated(keep="first")]
                .sort_index()
                .fillna(method="ffill")
            )
            if saveCSV:
                if parentDir is None:
                    fPath = os.path.join(self.mainDir, "Data")
                else:
                    fPath = parentDir
                if type(subDir) == str:
                    fPath = os.path.join(fPath, subDir)
                if fName[-4:] == ".csv":
                    fPath = os.path.join(fPath, fName)
                else:
                    fPath = os.path.join(fPath, fName + ".csv")
                df.to_csv(fPath)
            if reformatColumns:
                if type(columnTypes) == dict:
                    for col in columnTypes.keys():
                        dataFormat = columnTypes[col]
                        if dataFormat == float:
                            if df[col].dtype == np.object:
                                df.loc[df[col] == "null", col] = np.nan
                            df[col] = df[col].astype(float)
                        if df[col].dtype != dataFormat:
                            df[col] = df[col].astype(dataFormat)
                elif type(columnTypes) == list and len(columnTypes) > 0:
                    if len(columnTypes) == 1:
                        dataFormat = columnTypes[0]
                        for col in df.columns:
                            if dataFormat == float:
                                if df[col].dtype == np.object:
                                    df.loc[df[col] == "null", col] = np.nan
                                df[col] = df[col].astype(dataFormat)
                            elif df[col].dtype != dataFormat:
                                df[col] = df[col].astype(dataFormat)
                    elif len(self.storageCols) == len(columnTypes):
                        for colIndex in range(len(columnTypes)):
                            dataFormat = columnTypes[colIndex]
                            if dataFormat == float:
                                if df[df.columns[colIndex]].dtype == np.object:
                                    df.loc[df[col] == "null", col] = np.nan
                                df[col] = df[col].astype(dataFormat)
                            elif df[col].dtype != dataFormat:
                                df[col] = df[col].astype(dataFormat)
                    else:
                        raise ValueError(
                            "Not enough dTypes provided in the list. "
                            + "%01i columns in the DB and only %01i "
                            % (len(self.storageCols), len(columnTypes))
                            + "dtypes were provided"
                        )
                elif type(columnTypes) == type:
                    for col in df.columns:
                        if columnTypes == float:
                            if df[col].dtype == np.object:
                                df.loc[df[col] == "null", col] = np.nan
                            df[col] = df[col].astype(columnTypes)
                        elif df[col].dtype != columnTypes:
                            df[col] = df[col].astype(columnTypes)
            self.storage.put(
                "/" + dbKey, df, format="Table", data_columns=self.data_columns
            )
        self.storage.close()

    def updateDB(self):
        self.storage.open()
        for rawKey in self.storage.keys():
            rawKey = rawKey.replace("/", "")
            if rawKey not in self.fakeTckrs:
                self.updateTable(rawKey)
        self.storage.close()

    def updateTable(self, dbKey=None):
        pass


# =============================================================================
# *****************************************************************************
# ---------------------------- STOCK DATA CLASS -------------------------------
# *****************************************************************************
# =============================================================================


class StockData(BaseData):
    """Historical Price Database on my local machine

    for fundamentals look up intrinio"""

    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=False,
        proxies=None,
        cookieValue=None,
        crumbValue=None,
    ):
        if proxies is None:
            proxies = {}
        self.storageName = "TckrData"
        BaseData.__init__(
            self,
            self.storageName,
            fileDir=fileDir,
            workProxy=workProxy,
            proxies=proxies,
        )
        #        if the cookie/crumb combination ever expires you can refresh them by:
        #            1. use Chrome with the developer tools on to access a yahoo
        #               finance spreadsheet.
        #            2. the crumb value is contained in the url to get the value
        #            3. the cookie value is a cookie set on your browser under the
        #               name 'B'
        #            note: So far, I've only had to change the crumb value
        if cookieValue is None:
            cookieValue = "b7thnptblu4mf&b=3&s=v2"
        if crumbValue is None:
            crumbValue = "S9m6uSRL6La"
        self.cookieValue = cookieValue
        self.crumbValue = crumbValue
        self.data_columns = ["Close", "AdjClose", "Volume", "Dividends", "SplitFactor"]
        # Make sure that the last date is one that has a chance of the
        #  markets being open
        while self.lastHistoricalDate.weekday() > 4:
            self.lastHistoricalDate = self.lastHistoricalDate - datetime.timedelta(
                days=1
            )
        self.commonTckrVariations = {".VIX": "^VIX", "$VIX": "^VIX"}
        self.fakeTckrs = ["SPXLFAKE"]
        #       temp_db will be a dictionary of pd.DataFrames with the each
        #           type of data ('Close','AdjClose',etc) as the keys and the
        #           tckrs as the column names
        self.tempDB = None

    def initStockData(self, tckr, silent=False):
        tckr = MH.convertTckrToUniversalFormat(tckr)
        successBool = False
        print("Initializing %s data" % tckr)
        data = self.queryYahooStockData(tckr, start_date=self.simulationStartDate)
        if len(data.index):
            self.storage.open()
            data = self.formatStockDataFrame(data)
            self.storage.put(tckr, data, format="table", data_columns=self.data_columns)
            self.storage.close()
        self.keyList.append(tckr)
        successBool = True
        return successBool

    def populateTempDB(
        self,
        tckrs,
        start="1990-1-1",
        end=datetime.date.today(),
        dataCols=None,
        replaceDB=True,
    ):
        if dataCols is None:
            dataCols = ["Close", "AdjClose", "Dividends", "SplitFactor"]
        self.tempDBCols = dataCols
        panelData = {}
        tckrs = map(lambda x: MH.convertTckrToUniversalFormat(x), tckrs)
        for tckr in tckrs:
            tckrData = self.loadFromDB(
                tckr,
                MH.formatDateInput(start),
                MH.formatDateInput(end),
                columns=dataCols,
                source="storage",
            )
            for column in tckrData.columns:
                columnData = tckrData[column]
                columnData.name = tckr
                if column in panelData.keys():
                    panelData[column].append(columnData)
                else:
                    panelData[column] = [columnData]
        for panel in panelData.keys():
            panelData[panel] = pd.concat(panelData[panel], axis=1).fillna(
                method="ffill"
            )
        if replaceDB or self.tempDB is None:
            self.tempDB = panelData
        else:
            for dfName in panelData.keys():
                if dfName in self.tempDB.keys():
                    # Merge new rows by collecting the tckrs that are not\
                    #  already included and then merging only that data
                    newTckrs = []
                    for tckr in panelData[dfName].columns:
                        if tckr not in self.tempDB[dfName].columns:
                            newTckrs.append(tckr)
                    self.tempDB[dfName] = pd.merge(
                        self.tempDB[dfName],
                        panelData[dfName][newTckrs],
                        how="left",
                        left_index=True,
                        right_index=True,
                    )

        # clean volatility columns
        if "ixVIX" in tckrs and "ixVXV" in tckrs:
            selectNANs = np.isnan(self.tempDB["Close"]["ixVXV"])
            self.tempDB["Close"].loc[selectNANs, ["ixVXV"]] = self.tempDB["Close"].loc[
                selectNANs, ["ixVIX"]
            ]
        return self.tempDB

    def loadFromDB_TempDB(self, dbKey=None, start=None, end=None, columns=None):
        """
        overide the parent function because of the unique temp_db
        structure: queries from the temp_db
        Input conditions:
            o  start can be a single date or a (list,array,pd.DatetimeIndex)
                of dates. If it is a list, then only the matching dates
                will be returned
        returns a pd.DataFrame unless columns is a string and the name of
            a column, then it returns a pd.Series
        """
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        start = MH.formatDateInput(start)
        indexCoordinates = []
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY"]:
            end = datetime.date.today()
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        end = MH.formatDateInput(end)
        # format columns
        if columns == [] or columns is None:
            columns = self.tempDB.keys()
        elif type(columns) == str:
            columns = [columns]
        # Logic for the date ranges
        if type(start) == pd.DatetimeIndex:
            indexCoordinates = self.tempDB[columns[0]].index.isin(start)
        elif start is None and end is None:
            # return the whole table
            indexCoordinates = [True] * len(self.tempDB[columns[0]].index)
        else:
            if start == end:
                endSelect = True
                startSelect = self.tempDB[columns[0]].index == start
            else:
                if start is not None:
                    startSelect = self.tempDB[columns[0]].index >= start
                else:
                    startSelect = True
                if end is not None:
                    endSelect = self.tempDB[columns[0]].index < end
                else:
                    endSelect = True
            indexCoordinates = np.logical_and(startSelect, endSelect)
            if type(indexCoordinates) == bool:
                indexCoordinates = [indexCoordinates] * len(
                    self.tempDB[columns[0]].index
                )
        # compile the data together
        data = {}
        for col in columns:
            data[col] = self.tempDB[col][dbKey][indexCoordinates]
            data[col].name = col

        tckrData = pd.concat(data, axis=1, join_axes=[data[columns[0]].index])
        tckrData = tckrData[~tckrData.index.duplicated(keep="first")]
        return tckrData

    def queryYahooStockData(
        self, symbol, start_date=None, end_date=None, initialAdjPrice=None
    ):
        """ loads stock data from the yahoo database
            o  start_date will default to 1990-1-1 and is inclusive
            o  end_date will default to the day of the function call and
                is exclusive (will not include data for the actual end_date)
        """
        symbol = MH.convertTckrToYahooFmt(symbol)
        # date processing
        if start_date is None:
            start_date = datetime.datetime(1990, 1, 1)
        else:
            start_date = MH.formatDateInput(start_date)
        if end_date is None:
            end_date = MH.formatDateInput(datetime.date.today()) - datetime.timedelta(
                days=1
            )
        else:
            end_date = MH.formatDateInput(end_date)
        # bring start date and end date to the same format as yahoo by
        #  setting the time to 6:00 AM
        timestamp_limits = pd.DatetimeIndex(
            [start_date.date(), end_date.date()]
        ) + pd.Timedelta(hours=6)
        timestamp_limits = timestamp_limits.view("int64") / pd.Timedelta(
            1, unit="s"
        ).view("int64")
        start_date_timestamp, end_date_timestamp = timestamp_limits

        cookies = {"B": self.cookieValue}
        # get pricedata
        event = "history"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "interval=1d&events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = self.session.get(url_string, cookies=cookies)
        print(
            "Querying Yahoo Data for %s from %s to %s" % (symbol, start_date, end_date)
        )
        priceData = pd.read_csv(
            StringIO(response.content),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        newColNames = list(priceData.columns)
        newColNames[newColNames.index("Adj Close")] = "AdjClose"
        priceData.columns = newColNames
        # Get dividend data
        event = "div"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "interval=1d&events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = self.session.get(url_string, cookies=cookies)
        eventData = pd.read_csv(
            StringIO(response.content),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        if len(eventData.index):
            priceData = pd.concat(
                [priceData, eventData], axis=1, join_axes=[priceData.index]
            )
            # Set nan divident values to 0
            priceData.loc[np.isnan(priceData.Dividends), "Dividends"] = 0
        else:
            priceData["Dividends"] = 0.0
        # Get stock Split
        event = "split"
        url_string = (
            "https://query1.finance.yahoo.com/v7/finance/download"
            + "/%s?period1=%10i&period2=%10i&interval=1d&"
            % (symbol, start_date_timestamp, end_date_timestamp)
            + "events=%s&crumb=%s" % (event, self.crumbValue)
        )
        response = self.session.get(url_string, cookies=cookies)
        eventData = pd.read_csv(
            StringIO(response.content),
            index_col=0,
            parse_dates=["Date"],
            infer_datetime_format=True,
        )
        if len(eventData.index):
            # Convert text mult. factor to a float
            eventData["SplitFactor"] = eventData.apply(
                lambda row: self.formatYahooStockSplitFactor(row["Stock Splits"]),
                axis=1,
            )
            priceData = pd.concat(
                [priceData, eventData["SplitFactor"]],
                axis=1,
                join_axes=[priceData.index],
            )
            # Set nan split factor values to 0
            priceData.loc[np.isnan(priceData.SplitFactor), "SplitFactor"] = 1.0
        else:
            priceData["SplitFactor"] = 1.0
        # Calculate adj column - assume dividends are reinvested
        priceData = self.formatStockDataFrame(priceData)
        priceData["AdjClose"] = self.adjustedPrice(
            priceData, initialAdjPrice=initialAdjPrice
        )
        return priceData[~priceData.index.duplicated(keep="first")]

    def queryQuandl(self, symbol, start_date=None, end_date=None, initialAdjPrice=None):
        symbol = MH.convertTckrToYahooFmt(symbol)
        data = quandl.get("WIKI/%s" % symbol)
        return data

    def getColumnValueForEachDate(self, tckr, date, colName, source="storage"):
        tckr = MH.convertTckrToUniversalFormat(tckr)
        date = MH.formatDateInput(date)
        # If only one date in the list, then treat it like a single date
        #  and don't include it in list processing
        if type(date) == pd.DatetimeIndex:
            values = pd.Series(index=date, name=colName)
            validValues = self.loadFromDB(tckr, date, columns=colName)
            values[validValues.index] = validValues
            if date.min() != validValues.index.min():
                firstValidRow = self.getValidRows(tckr, date.min(), source=source)
                assert type(firstValidRow) in [pd.Series, float], (
                    "Unexpected Data type returned from "
                    + "stockDate.getValidRows, "
                    + str(type(firstValidRow))
                    + "\n\tTckr: "
                    + tckr
                    + ", MinDate: "
                    + str(date)
                    + ", ColName: "
                    + colName
                    + ", Source: "
                    + source
                )
                if type(firstValidRow) == pd.Series:
                    values[date.min()] = self.getValidRows(
                        tckr, date.min(), source=source
                    )[colName]
            return values.fillna(method="ffill")
        else:
            values = self.getValidRows(tckr, date, source=source)
            if values is np.nan:
                return values
            else:
                return values[colName]

    def getPrice(self, tckr, date, adjusted=False, source="storage"):
        """
        returns the close/adjusted price for the tckr based on the date
        provided

            o  if date is a single time then func will return a scalar of
                the latest row in the DB that is still less than the date
                provided and has a finite close price
            o  if date is an empty list (i.e. []) then it will return all
                prices that are finite
            o  if date is list or iterable object, then func will return
                a series of the last valid price for each date provided
                as long as the dates are > the start date of the tckr data
        """
        if adjusted:
            colName = "AdjClose"
        else:
            colName = "Close"
        price = self.getColumnValueForEachDate(tckr, date, colName, source=source)
        if type(price) == pd.Series and pd.isna(price).any():
            null_index = price.index[pd.isna(price)]
            for i in null_index:
                price[i] = self.getColumnValueForEachDate(
                    tckr, i, colName, source=source
                )
        return price

    @staticmethod
    def adjustedPrice(stockDF, initialAdjPrice=None):
        """
        calculate the adjusted price of the stock based off of the first
            close and resulting dividends.
        Final adj price will represent the value of 1 stock unit purchesed
            at the first closing price with all subsequent dividends
            reinvested. Split factor is assumed to be 1 unless there
            is a split event

        o stock data is a pd.DataFrame with the minimum columns
            ['Close', 'Dividends', 'SplitFactor']
        o initial_adj_price (default=first close price) is the last valid
            adjPrice of a dataset that you will be appending to
            """
        # initialize all data
        closePrice = stockDF.Close
        dividends = stockDF.Dividends
        # splitFactor = stockDF.SplitFactor
        if initialAdjPrice is not None:
            initialUnits = initialAdjPrice / closePrice[0]
        else:
            initialUnits = 1

        equivalentQty = np.zeros(closePrice.shape)
        for rowNum in range(len(stockDF.index)):
            if rowNum == 0:
                equivalentQty[rowNum] = initialUnits
            else:
                # Removing the split factor mulplier because it the data is
                #  wrong for EFA and I haven't seen a case where it is right
                # equivalentQty[rowNum] = (equivalentQty[rowNum-1] +
                #  (equivalentQty[rowNum-1] * dividends[rowNum] /
                #  closePrice[rowNum]))) * splitFactor[rowNum]
                equivalentQty[rowNum] = equivalentQty[rowNum - 1] + (
                    equivalentQty[rowNum - 1] * dividends[rowNum] / closePrice[rowNum]
                )

        adjClose = closePrice * equivalentQty
        return adjClose

    def formatStockDataFrame(self, stockDF, startIndex=0):
        for col in stockDF.columns:
            if stockDF[col].dtype == np.object:
                stockDF.loc[stockDF[col] == "null", col] = np.nan
                stockDF[col] = stockDF[col].astype(float)
            elif stockDF[col].dtype is not float:
                stockDF[col] = stockDF[col].astype(float)
        return stockDF.fillna(method="ffill")

    @staticmethod
    def formatYahooStockSplitFactor(text):
        """
        takes the text format of the stock split info returned by yahoo
        and converts it into a float number. Result is the multiplicative
        factor that should be applied to the # of shares owned. """
        numerator, denomentator = text.split("/")
        return float(numerator) / float(denomentator)

    def PredictSPXVol_1m3mVIX(self, date, exDate, source="storage"):
        """
        Sets the annual volatility based on the 1 month and 3 month
        VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month
                VIX volatilities"""
        date = MH.formatDateInput(date)
        exDate = MH.formatDateInput(exDate)
        daysOut = (exDate - date).days

        def interpolate_Volatility(row):
            if np.isfinite(row.M3) and row.DaysOut > 30:
                if row.DaysOut > 90:
                    volatility = row.M3
                elif row.DaysOut <= 0:
                    volatility = 0
                else:
                    volatility = (row.DaysOut - 30) * (
                        (row.M3 - row.M1) / (60)
                    ) + row.M1
            else:
                volatility = row.M1
            return volatility

        vol1M = self.getPrice("ixVIX", date, source=source)
        vol3M = self.getPrice("ixVXV", date, source=source)
        if type(date) == pd.DatetimeIndex:
            daysOut = pd.Series(data=daysOut, index=date, name="DaysOut")
            vixDF = vixDF = pd.concat([vol1M, vol3M, daysOut], axis=1)
            vixDF.columns = ["M1", "M3", "DaysOut"]
            vixDF["Volatility"] = vixDF.apply(
                lambda row: interpolate_Volatility(row), axis=1
            )
            return vixDF.Volatility
        else:
            row = pd.Series(data=[vol1M, vol3M, daysOut], index=["M1", "M3", "DaysOut"])
            return interpolate_Volatility(row)

    def PredictSPXVolSingle_1m3mVIX(self, date, daysOut, source="storage"):
        """
        Sets the annual volatility based on the 1 month and 3 month
        VIX index
            o   < 1 month and it defaults to the 1 month VIX
            o   > 3 months and it defaults to the 3 month VXN
            o   else weighted average of the 1month and 3 month
                VIX volatilities
        """
        date = MH.formatDateInput(date)
        # If days out is >= 90 then we don't need the 1m VIX
        if daysOut >= 90:
            row = self.getValidRows("ixVXV", date, source=source)
            if source.upper() == "STORAGE":
                if row.size == 0:
                    # If empty look for the 1m data
                    row = self.getValidRows("ixVIX", date, source=source)
                    if row.size == 0:
                        volatility = np.nan
                    else:
                        volatility = row.Close
                else:
                    volatility = row.Close
            else:
                volatility = self.tempDB["Close"].ixVXN[row]
                if volatility is None or np.isnan(volatility):
                    # If empty look for the 1m data
                    volatility = self.tempDB["Close"].ixVIX[row]
                    if volatility is None or np.isnan(volatility):
                        volatility = np.nan
        else:
            # Will definitely need the 1m VIX
            row = self.getValidRows("ixVIX", date, source=source)
            if source.upper() == "STORAGE":
                if row.size == 0:
                    vol_1m = np.nan
                else:
                    vol_1m = row.close
            else:
                vol_1m = self.tempDB["Close"].ixVIX[row]
            if daysOut <= 30:
                volatility = vol_1m
            else:
                # get most recent volatility of 3m VXV
                if source.upper() == "STORAGE":
                    row = self.getValidRows("ixVXV", date)
                    if row.size == 0:
                        vol_3m = vol_1m
                    else:
                        vol_3m = row.Close
                else:
                    vol_3m = self.tempDB["Close"].ixVXV[row]
                # model as the weighted average
                volatility = (
                    vol_1m * (90.0 - daysOut) / 60.0 + vol_3m * (daysOut - 30) / 60.0
                )
        return volatility

    def reInitializeAllData(self, tckrs=None):
        self.storage.open()
        if tckrs is None:
            tckrs = self.storage.keys()
        for tckr in tckrs:
            rawTckr = tckr.replace("/", "")
            if rawTckr not in self.fakeTckrs:
                status = self.initStockData(rawTckr)
                if status:
                    print("Successful")
                else:
                    print("Failed")
        self.storage.close()

    def updateDB(self, save_csv=True):
        self.storage.open()
        for rawTckr in self.storage.keys():
            tckr = rawTckr.replace("/", "")
            if tckr not in self.fakeTckrs:
                self.updateTable(tckr)
        if save_csv:
            self.saveAllStockToCSV()
            print("Successfully saved all stock data to CSV")
        self.storage.close()

    def updateTable(self, tckr):
        """updates a single dataframe in the database"""
        try:
            self.storage.open()
            tckr = MH.convertTckrToUniversalFormat(tckr)
            minStorageDate, maxStorageDate = self.getMinMaxDate(tckr)
            end_date = pd.Timestamp.today()
            if end_date.time() < datetime.time(15, 30):
                end_date = end_date.date() - pd.Timedelta(days=1)
            else:
                end_date = end_date.date()
            end_date = pd.bdate_range(end_date - pd.Timedelta(days=4), end_date).max()
            if maxStorageDate < end_date:
                # add 1 day to end_date for margin
                end_date = end_date + pd.Timedelta(days=1)
                lastAdjPrice = self.loadFromDB(
                    tckr,
                    maxStorageDate,
                    maxStorageDate,
                    columns="AdjClose",
                    source="storage",
                )
                # Load the relevant data
                data = self.queryYahooStockData(
                    tckr,
                    start_date=maxStorageDate,
                    end_date=end_date,
                    initialAdjPrice=lastAdjPrice,
                )
                if len(data.index) > 0:
                    self.storage.open()
                    # Append all but the first row of data because that one
                    #  is already in the DB
                    self.storage.append(
                        tckr,
                        data.iloc[1:, :],
                        format="table",
                        data_columns=self.data_columns,
                    )
                print("%s: Successfully Updated" % tckr)
            else:
                print("%s is already up to date" % tckr)
        except Exception as ex:
            print("%s: Failed - " % tckr)
            print(ex)
        self.storage.close()

    def saveAllStockToCSV(self):
        saveDir = os.path.join(self.mainDir, "Data", "StockCSVs")
        if not os.path.exists(saveDir):
            os.makedirs(saveDir)
        self.storage.open()
        for tckr in self.storage.keys():
            fName = tckr.replace("/", "")
            print("Saving %s data to .csv" % fName)
            self.storage.get(tckr).to_csv(os.path.join(saveDir, fName + ".csv"))
        self.storage.close()

    # =========================================================================
    # *************************************************************************
    # --------------- Begin Wrapper and Depreciated Functions -----------------
    # *************************************************************************
    # =========================================================================

    def getTckrData(self, tckr, fromDate, toDate=None, columns=None, source="storage"):
        """
        depreciated function: use refresh to refresh the database or
        loadFromDB to get data from the DB
        """
        tckr = MH.convertTckrToUniversalFormat(tckr)
        if fromDate is None and toDate is None:
            pass
        else:
            if fromDate is None:
                fromDate = self.simulationStartDate
            else:
                fromDate = MH.formatDateInput(fromDate)
            if toDate is None:
                toDate = self.lastHistoricalDate
            else:
                toDate = MH.formatDateInput(toDate)
        return self.loadFromDB(tckr, fromDate, toDate, columns=columns, source=source)


# =============================================================================
# *****************************************************************************
# --------------------------- LIBOR RATES CLASS -------------------------------
# *****************************************************************************
# =============================================================================


class LIBORData(BaseData):
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        proxies=None,
        webDriver=None,
    ):
        if proxies is None:
            proxies = {}
        self.storageName = "LIBORData"
        BaseData.__init__(
            self,
            self.storageName,
            fileDir=fileDir,
            workProxy=workProxy,
            proxies=proxies,
        )
        # TempDB has the same format as the hd5 table except that it has
        #  only the relevant data for the dates
        self.chromedriver = (
            "C:\\Users\\a0225347\\Documents\\Python"
            + "\\SeleniumWebDrivers\\chromedriver.exe"
        )
        self.phantomJSDriver = (
            "C:\\Users\\a0225347\\Documents\\Python\\"
            + "SeleniumWebDrivers\\"
            + "phantomjs-2.1.1-windows\\bin\\"
            + "phantomjs.exe"
        )
        self.driver = webDriver

    def getLIBOR_Rate(self, date, loanDays, source="storage"):
        # print date, loanDays, source
        date = MH.formatDateInput(date)
        if date is None:
            date = MH.formatDateInput(datetime.date.today())

        def interpolated_Rate(row, firstRateCol=0):
            colDaysList = [1, 7, 30, 60, 90, 180, 365]
            colDaysArray = np.array([1, 7, 30, 60, 90, 180, 365])
            # print row
            # assert row.LoanDays>=0, ("row.LoanDays < 0, " + str(row)
            #  + ' - ' + str(row.LoanDays) +  ' - ' + str(date)
            if row.LoanDays <= 0:
                return row[firstRateCol]
            lowIndex = colDaysList.index(
                colDaysArray[colDaysArray <= row.LoanDays].max()
            )
            if row.LoanDays >= colDaysList[-1]:
                return row[firstRateCol + lowIndex]
            #            highIndex = lowIndex+1
            #            lowRate = row[1+lowIndex]
            #            highRate = row(1+highIndex)
            #            lowSpan = colDaysList[lowIndex]
            #            highSpan = colDaysList[highIndex]
            #            slope = (highRate-lowRate)/(highSpan-lowSpan)
            #            interpolation = (loanDays-lowSpan)*slope+lowRate
            return (
                (row.LoanDays - colDaysList[firstRateCol + lowIndex])
                * (row[firstRateCol + lowIndex + 1] - row[firstRateCol + lowIndex])
                / (
                    colDaysList[firstRateCol + lowIndex + 1]
                    - colDaysList[firstRateCol + lowIndex]
                )
                + row[firstRateCol + lowIndex]
            )

        if type(date) == pd.DatetimeIndex:
            if loanDays is None or type(loanDays) in [int, float]:
                dfOut = pd.DataFrame(
                    data=[loanDays] * len(date), index=date, columns=["LoanDays"]
                )
            else:
                if len(loanDays) == len(date):
                    dfOut = pd.DataFrame(
                        data=loanDays, index=date, columns=["LoanDays"]
                    )
                else:
                    raise ValueError(
                        "variable loanDays is not the same size " + "as variable date"
                    )
            rateData = self.loadFromDB(start=date, source=source)
            dfOut = pd.merge(
                dfOut,
                rateData,
                how="left",
                left_index=True,
                right_index=True,
                sort=True,
            )
            if rateData.index.min() != dfOut.index.min():
                firstRate = self.getValidRows("LIBOR", dfOut.index.min())
                dfOut.iloc[0, 1:] = firstRate
            dfOut = dfOut.fillna(method="ffill")
            if loanDays is None:
                # Return an extrapolated dataSet from the DB
                return dfOut[self.storageCols]
            dfOut["Rate"] = dfOut.apply(lambda row: interpolated_Rate(row), axis=1)
            return dfOut.Rate
        else:
            if loanDays is None:
                return
            else:
                row = self.getValidRows("LIBOR", date)
                row["LoanDays"] = loanDays
                return interpolated_Rate(row)

    def queryAndStoreLiborRate(self, date, recurseUntilValid=True, recursiveLimit=20):
        """
        query and store the annualized LIBOR rate for 1d,1w,1m,2m,3m,6m,1yr
        """
        date = MH.formatDateInput(date)
        try:
            # Query from the online database
            rate = self.queryLiborRates(
                date, recurseUntilValid=recurseUntilValid, recursiveLimit=recursiveLimit
            )
            self.storage.open()
            self.storage.append(
                "LIBOR", rate, format="table", data_columns=self.data_columns
            )
            self.storage.close()
            return rate
        except Exception as message:
            print(message)
            return None

    def queryLiborRates(
        self,
        date,
        recurseCount=0,
        recurseUntilValid=True,
        recursiveLimit=20,
        debug=False,
    ):
        date = MH.formatDateInput(date)
        url_string = (
            "https://www.theice.com/marketdata/reports"
            + "/icebenchmarkadmin/ICELiborHistoricalRates.shtml?"
            + "excelExport=&criteria.reportDate="
            + "%01i/%01i/%02i&criteria.currencyCode=USD"
            % (date.month, date.day, date.year % 100)
        )
        # cookies = {'TS0156c7aa':'0100e6d495d573c25fa86d1cbea437b4e46d17
        #  a84144b84755f37922ac9ef1adf38cf7739fb842cc1d95d6313a77dd9d4346
        #  697fc503b02db1a088cbb8a86349b4953edb792cb6fda92fc9d4b72f68c02e
        #  5643d21bfd261ba679fad8c7bd43d7818be6e1b2e44a55d978120f3b4af9f1
        #  74e8d32a615adce2d8aaa62d7c60dc5678d90bb823','Path':'/','Domain':
        #  '.www.theice.com'}
        response = self.session.get(url_string)  # ,cookies=cookies)
        rawData = pd.read_csv(StringIO(response.content))

        if rawData.size < 4:
            if recurseUntilValid and recurseCount < 20:
                newDate = MH.formatDateInput(date - datetime.timedelta(days=1))
                return self.queryLiborRates(
                    newDate,
                    recurseCount=recurseCount + 1,
                    recurseUntilValid=recurseUntilValid,
                    recursiveLimit=recursiveLimit,
                )
            else:
                raise ValueError("The date did not have a LIBOR Rate")
        if rawData.size >= 4:
            processedData = pd.DataFrame(
                data=[rawData.iloc[:-1, -1].tolist()],
                index=[datetime.datetime(date.year, date.month, date.day)],
                columns=["1d", "1w", "1m", "2m", "3m", "6m", "1yr"],
            )
        else:
            processedData = pd.DataFrame(
                columns=["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]
            )
        return processedData

    def queryLiborRates_Selenium(self, dates, debug=False):
        dates = MH.formatDateArray(dates)

        def checkWebPageIntegrity(driver):
            reCaptchaForms = driver.find_elements_by_id("reportCenterRecaptchaForm")
            if len(reCaptchaForms) > 0:
                raw_input(
                    "User needs to get the program through the "
                    + "reCAPTCHA check. Press enter on the module "
                    + "to continue..."
                )
            if driver.title is None:
                disclaimerAcceptButton = driver.find_element_by_xpath(
                    "/html/body/div[3]/div/div[2]/div/div/div[2]/button"
                )
                disclaimerAcceptButton.click()
                time.sleep(1)

        def convertWebLabels(rawIndex, rateCols=None):
            if rateCols is None:
                rateCols = ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]
            substitutions = {
                "Overnight": "1d",
                "1 Week": "1w",
                "1 Month": "1m",
                "2 Month": "2m",
                "3 Month": "3m",
                "6 Month": "6m",
                "1 Year": "1yr",
            }
            rawIndex = np.array(rawIndex)
            if rawIndex[0] == 0:
                rawIndex = np.array(rateCols)[-rawIndex.size :]
            else:
                for i in range(len(rawIndex)):
                    rawIndex[i] = substitutions.get(rawIndex[i], rawIndex[i])
            return rawIndex

        rateCols = ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]
        dailyRateList = []

        if self.driver is None:
            self.driver = driver = webdriver.Chrome(executable_path=self.chromedriver)
        else:
            driver = self.driver
        driver.get("https://www.theice.com/marketdata/reports/170")
        checkWebPageIntegrity(driver)

        for queryDate in dates:
            url_string = (
                "https://www.theice.com/marketdata/reports"
                + "/icebenchmarkadmin/ICELiborHistoricalRates.shtml"
                + "?criteria.reportDate="
                + "%01i/%01i/%02i&criteria.currencyCode=USD"
                % (queryDate.month, queryDate.day, queryDate.year % 100)
            )
            driver.get(url_string)
            checkWebPageIntegrity(driver)
            reloadCnt = 0
            while driver.title != "ICE LIBOR Historical Rates" and reloadCnt < 5:
                driver.get("https://www.theice.com/marketdata/reports/170")
                checkWebPageIntegrity(driver)
                driver.get(url_string)
                checkWebPageIntegrity(driver)
                reloadCnt += 1
            rateTableElements = driver.find_elements_by_xpath("/html/body/div/div")
            if len(rateTableElements) > 0:
                rateTable = pd.read_html(
                    rateTableElements[0].get_attribute("innerHTML"), header=0
                )[0]
                rateTable.index = convertWebLabels(rateTable.index)
                if rateTable.size >= 4:
                    rateTable = rateTable.iloc[:, -1]
                    rateTable.name = queryDate
                    dailyRateList.append(rateTable)
                else:
                    print("Bad Data Found for " + str(queryDate))
                    print(rateTable)
            else:
                rateTable = pd.Series(name=queryDate, index=rateCols)
                dailyRateList.append(rateTable)
        rateTable = pd.concat(dailyRateList, axis=1).T
        rateTable = rateTable[rateCols].fillna(method="ffill")
        driver.close()
        self.driver = None
        return rateTable

    def populateTempDB(self, dbKey="LIBOR", start=None, end=None, columns=None):
        """
        Wrapper function to call the parent function with the correct data
        """
        if columns is None:
            columns = []
        return BaseData.populateTempDB(
            self, dbKey=dbKey, start=start, end=end, columns=columns
        )

    def loadFromDB(
        self, dbKey="LIBOR", start=None, end=None, columns=None, source="storage"
    ):
        """
        Will load the data including the start up to but not including the end
        """
        if columns is None:
            columns = []
        return BaseData.loadFromDB(
            self, dbKey=dbKey, start=start, end=end, columns=columns, source=source
        )

    def getMinMaxDate(self, dbKey="LIBOR"):
        """
        Wrapper function to call the parent function with the correct data
        """
        return BaseData.getMinMaxDate(self, dbKey)

    def getValidRows(self, dbKey="LIBOR", date=None):
        """
        Wrapper function to call the parent function with the correct data
        """
        return BaseData.getValidRows(self, dbKey=dbKey, date=date)

    def updateSpan(self, start, end):
        """ Depreciated
        loads for every day in between start and end including end points
        need:
        """
        currentDay = MH.formatDateInput(start)
        end = MH.formatDateInput(end)
        while currentDay <= end:
            try:
                print(currentDay)
                self.queryAndStoreLiborRate(currentDay)
            except Exception as message:
                print("\tException:" + str(message))
                pass
            currentDay = currentDay + datetime.timedelta(days=1)
            time.sleep(1)
        self.formatStorageDB(self)

    def updateTable(self, dbKey="LIBOR", includeToday=False):
        """ depreciated due to the use of an extra screen on the ICE webpage"""
        minDate, maxDate = self.getMinMaxDate()
        start = maxDate + datetime.timedelta(days=1)
        if includeToday:
            end = MH.formatDateInput(datetime.date.today())
        else:
            end = MH.formatDateInput(datetime.date.today() - datetime.timedelta(days=1))
        if start < end:
            dates = pd.DatetimeIndex(start=start, end=end, freq="d")
            try:
                # Query from the online database
                rateData = self.queryLiborRates_Selenium(dates.copy())
                self.storage.open()
                self.storage.append(
                    "LIBOR", rateData, format="table", data_columns=self.data_columns
                )
                self.storage.close()
            except Exception as message:
                print(message)
        # self.formatStorageDB()

    def formatStorageDB(self, saveCSV=True, reformatColumns=True, columnTypes=float):
        return BaseData.formatStorageDB(
            self,
            reformatColumns=reformatColumns,
            columnTypes=columnTypes,
            saveCSV=True,
            parentDir=os.path.join(self.mainDir, "Data"),
            subDir="HistoricalLiborYields",
            fName="LIBOR_Rates.csv",
        )

    def initDB(self):
        date = MH.formatDateInput("2010-01-01")
        # Query from the online database
        rate = self.queryLiborRates(date, recurseUntilValid=True, recursiveLimit=20)
        self.storage.open()
        self.storage.put("LIBOR", rate, format="table", data_columns=self.data_columns)
        self.storage.close()
        self.updateSpan("2005-3-1", "2017-8-8")
        return rate

    def recoverFromCSVBackup(self):
        fpath = r"C:\Users\cp035982\Python\Projects\MarketAnalysis\Data\HistoricalLiborYields\LIBOR_Rates.csv"
        liborDB = pd.read_csv(
            fpath, index_col=0, parse_dates=[0], infer_datetime_format=True
        )
        self.storage.open()
        self.storage.put(
            "LIBOR", liborDB, format="table", data_columns=self.data_columns
        )
        self.storage.close()

    def loadHistoricalManualCSV(
        self,
        fPath=(
            r"C:\Users\cp035982\Python\Projects\MarketAnalysis\Data\HistoricalLiborYields"
            + "\hitstoricalLIBORRatesManual.csv"
        ),
    ):
        """Use as a restore from back up function"""
        historicalDF = pd.read_csv(fPath)
        start = datetime.datetime(1989, 9, 1)
        end = datetime.datetime(2004, 5, 31)
        # Logic to create the 1d, 1w, and 2m columns
        self.storage.open()
        storageData = self.storage["/LIBOR"]
        storageData = storageData.loc[storageData.index > end, :]
        dayRatio = (storageData["1d"] / storageData["1m"]).mean()
        weekRatio = (storageData["1w"] / storageData["1m"]).mean()
        historicalDF["1d"] = dayRatio * historicalDF["1m"]
        historicalDF["1w"] = weekRatio * historicalDF["1m"]
        historicalDF["2m"] = (historicalDF["1m"] + historicalDF["3m"]) / 2.0

        # Create a final format DF to append to storage
        index = pd.date_range(start=start, end=end, freq="D")
        formatedDF = pd.DataFrame(index=index, columns=["Year", "Month"], dtype=float)
        formatedDF["Year"] = formatedDF.apply(lambda row: row.name.year, axis=1)
        formatedDF["Month"] = formatedDF.apply(lambda row: row.name.month, axis=1)
        formatedDF["Date"] = formatedDF.index

        # Merge the two tables on the year and month
        formatedDF = pd.merge(
            formatedDF, historicalDF, how="left", on=["Year", "Month"], left_index=True
        )
        formatedDF.index = formatedDF.Date
        formatedDF = formatedDF.loc[:, ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]]
        formatedDF = pd.merge(
            storageData,
            formatedDF,
            how="outer",
            on=["1d", "1w", "1m", "2m", "3m", "6m", "1yr"],
            left_index=True,
            right_index=True,
            sort=True,
        )
        self.storage.put(
            "/LIBOR", formatedDF, format="Table", data_columns=self.data_columns
        )
        self.formatStorageDB()
        self.storage.close()
        return formatedDF

    def loadMonthofRates(self, year, month):
        currentDay = datetime.datetime(year, month, 1)
        while currentDay.month == month:
            try:
                print(currentDay)
                self.queryAndStoreLiborRate(currentDay)
            except Exception as message:
                print("\tException:" + str(message))
                pass
            currentDay = currentDay + datetime.timedelta(days=1)
            time.sleep(1)

    def getLIBOR_Rate_Single(self, date, loanDays, source="storage"):
        """depreciated use 'getLIBOR_Rate' insetead"""
        allRates = self.loadFromDB(start=date, end=date, source=source)
        while type(allRates) != pd.DataFrame:
            date = date - datetime.timedelta(days=1)
            allRates = self.loadFromDB(start=date, source=source)
        if type(allRates) == pd.DataFrame:
            if loanDays >= 365:
                return allRates.loc[0, "1yr"]
            days = [1, 7, 30, 60, 90, 180, 365]
            rates = allRates.loc[0, :]
            f = interp1d(days, rates)
            return float(f(loanDays))
        else:
            return self.getLIBOR_Rate(date - datetime.timedelta(days=1), loanDays)

    def loadAllLIBOR_RatesForDate(
        self, date, recurseUntilValid=True, recursiveLimit=20, source="storage"
    ):
        """dpreciated: use loadFromDB instead
            return the annualized LIBOR rate for 1d,1w,1m,2m,3m,6m,1yr"""
        date = MH.formatDateInput(date)
        try:
            # Check if rate is in storage
            self.storage.open()
            if "/LIBOR" in self.storage.keys():
                rate = self.loadFromDB(start=date, end=date, source=source)
                if len(rate) > 0:
                    return rate
            # else query from the online database
            rate = self.queryLiborRates(
                date, recurseUntilValid=recurseUntilValid, recursiveLimit=recursiveLimit
            )
            self.storage.open()
            self.storage.append(
                "LIBOR", rate, format="table", data_columns=self.data_columns
            )
            self.storage.close()
            return rate
        except Exception as message:
            print(message)
            return None


# =============================================================================
# *****************************************************************************
# --------------------------- INFLATION (CPI) CLASS ---------------------------
# *****************************************************************************
# =============================================================================


class InflationData(BaseData):
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        proxies=None,
        webDriver=None,
    ):
        if proxies is None:
            proxies = {}
        self.storageName = "InflationData"
        BaseData.__init__(
            self,
            self.storageName,
            fileDir=fileDir,
            workProxy=workProxy,
            proxies=proxies,
        )
        # temp_db has the same format as the hd5 table except that it has
        #  only the relevant data for the dates
        self.data_columns = ["Year", "Month", "CPI"]
        # web scraping info
        self.chromedriver = (
            "C:\\Users\\a0225347\\Documents\\Python"
            + "\\SeleniumWebDrivers\\chromedriver.exe"
        )
        self.phantomJSDriver = (
            "C:\\Users\\a0225347\\Documents\\Python\\"
            + "SeleniumWebDrivers\\"
            + "phantomjs-2.1.1-windows\\bin\\"
            + "phantomjs.exe"
        )
        self.driver = webDriver
        self.tempDB = None
        self.referenceDate = pd.Timestamp(2017, 7, 15)

    def transformDate(self, date, dayOfMonth="15"):
        if type(dayOfMonth) == str:
            dayOfMonth = "%02i" % int(dayOfMonth)
        else:
            dayOfMonth = "%02i" % dayOfMonth
        date = MH.formatDateInput(date)
        if type(date) == pd.DatetimeIndex:
            transformedDate = MH.formatDateInput(
                (
                    date.year.astype("|S4")
                    + "-"
                    + date.month.astype("|S2")
                    + "-"
                    + [dayOfMonth] * len(date)
                ).unique()
            )
        elif date is not None:
            transformedDate = pd.Timestamp(date.year, date.month, int(dayOfMonth))
        else:
            transformedDate = date
        return transformedDate

    def getCPIForYearAndMonth(self, year, month, source="storage"):
        validIndex = self.getValidIndices(
            date=datetime.datetime(year, month, 15), colName="CPI", source=source
        )
        return self.loadFromDB(
            start=validIndex, end=validIndex, columns="CPI", source=source
        )

    def getCPIForDate(self, date, includeYearMonth=False, source="storage"):
        date = MH.formatDateInput(date)
        if date is None:
            date = self.referenceDate
        if type(date) == pd.DatetimeIndex:
            dfOut = pd.DataFrame(
                data=np.stack([date.year, date.month], axis=1).astype(int),
                index=date,
                columns=["Year", "Month"],
            )
            if (
                date.min().year == date.max().year
                and date.min().month == date.max().month
            ):
                CPI = self.getCPIForYearAndMonth(
                    date.min().year, date.min().month, source=source
                )
                dfOut["CPI"] = CPI
            else:
                minDate = self.transformDate(date.min())
                maxDate = self.transformDate(date.max(), dayOfMonth="20")
                cpiData = self.loadFromDB(start=minDate, end=maxDate, source=source)
                dfOut = pd.merge(dfOut, cpiData, how="left", on=["Year", "Month"])
                dfOut.index = date
            if includeYearMonth:
                return dfOut[["Year", "Month", "CPI"]]
            else:
                return dfOut.CPI
        else:
            CPI = self.getCPIForYearAndMonth(date.year, date.month, source=source)
            if includeYearMonth:
                return date.year, date.month, CPI
            else:
                return CPI

    def inflationRatio(self, date, referenceDate=None, source="storage"):
        date = MH.formatDateInput(date)
        if referenceDate is None:
            referenceDate = self.referenceDate
        referenceDate = MH.formatDateInput(referenceDate)
        if date is None:
            return
        refVal = self.getCPIForDate(referenceDate, source=source)
        originalValue = self.getCPIForDate(date, source=source)
        ratio = originalValue / refVal
        return ratio

    def inflationAdjustedValue(
        self, date, refValue, referenceDate=None, source="storage"
    ):
        return (
            self.inflationRatio(date, referenceDate=referenceDate, source=source)
            * refValue
        )

    def getMinMaxDate(self, dbKey="CPI"):
        return BaseData.getMinMaxDate(self, dbKey)

    def getValidIndices(self, dbKey="CPI", date=None, colName=None, source="storage"):
        transformedDate = self.transformDate(date)
        return BaseData.getValidIndices(
            self, dbKey=dbKey, date=transformedDate, colName=colName, source=source
        )

    def getValidRows(self, dbKey="CPI", date=None, source="storage"):
        """
        returns the dataframe row as a series with a finite price that
        is closest to the given date and not later than the given date
        """
        return BaseData.getValidRows(self, dbKey=dbKey, date=date, source=source)

    def populateTempDB(self, start=None, end=None, columns=None, extraColumns=None):
        """
        largely a dummy function that calls 'loadFromDB' and stores the
        result in the internal temp_db

        extraColumns is a dictionary of {ColName:(Value,ReferenceDate),...}
        and the value will be value adjusted inflation for each date
        """
        if columns is None:
            columns = []
        if extraColumns is None:
            extraColumns = {}
        self.tempDB = self.loadFromDB(
            start=start, end=end, columns=columns, source="storage"
        )
        for colName in extraColumns.keys():
            value, refDate = extraColumns[colName]
            refDate = MH.formatDateInput(refDate, dateType=pd.Timestamp)
            refValue = self.getCPIForYearAndMonth(
                refDate.year, refDate.month, source="temp_db"
            )
            self.tempDB[colName] = value * (self.tempDB.CPI / refValue)
        return self.tempDB

    def loadFromDB(
        self, dbKey="CPI", start=None, end=None, columns=None, source="storage"
    ):
        """
        Will load the data including the start up to but not including the end
        """
        if columns is None:
            columns = []
        start = MH.formatDateInput(start)
        if type(end) == str and end.upper() in ["END", "LAST", "TODAY", "ALL"]:
            end = MH.formatDateInput(datetime.date.today())
        elif type(end) == str and end.upper() in ["START", "SAME"]:
            end = start
        else:
            end = MH.formatDateInput(end)
        if start is not None and type(start) != pd.DatetimeIndex and start == end:
            # Catch the condition that we want to load only the 1 day
            start = end = self.transformDate(start, dayOfMonth="15")
        else:
            start = self.transformDate(start, dayOfMonth="15")
            if type(start) == pd.DatetimeIndex:
                end = None
            else:
                end = self.transformDate(MH.formatDateInput(end), dayOfMonth="20")
        return BaseData.loadFromDB(
            self, dbKey=dbKey, start=start, end=end, columns=columns, source=source
        )

    def updateTable(self, dbKey="CPI", debug=False):
        # find last year/month and determine if the storage needs to updated
        minDate, maxDate = self.getMinMaxDate()
        currentMonthDate = datetime.datetime.today()
        currentMonthDate = datetime.datetime(
            currentMonthDate.year, currentMonthDate.month, 15
        )
        # Update if a full calendar month has passed since the last record
        if (currentMonthDate - maxDate).days > 90:
            inflationURL = (
                "https://inflationdata.com/Inflation/"
                + "Consumer_Price_Index/HistoricalCPI.aspx"
            )
            if self.driver is None:
                if debug:
                    self.driver = driver = webdriver.Chrome(
                        executable_path=self.chromedriver
                    )
                else:
                    self.driver = driver = webdriver.PhantomJS(
                        executable_path=self.phantomJSDriver,
                        service_log_path=os.path.devnull,
                    )
                    driver.set_window_size(1120, 550)
            else:
                driver = self.driver
            driver.get(inflationURL)
            print(driver.title)
            rawCPIDF = pd.read_html(
                driver.find_element_by_xpath('//*[@id="form1"]/div[3]').get_attribute(
                    "innerHTML"
                ),
                header=0,
            )[0]
            rawCPIDF["Month"] = 0
            # The number of cells will be the number of years*12 because
            #  we do not care about the average columns
            qty = len(rawCPIDF.index) * 12
            tallSkinnyDF = pd.DataFrame(
                data=np.zeros((qty, 3)), columns=["Year", "Month", "CPI"], dtype=float
            )
            startIndex = 0
            for colName in rawCPIDF.columns:
                if colName in [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec",
                ]:
                    colDF = rawCPIDF.loc[:, ["Year", "Month", colName]]
                    colDF["Month"] = MH.month2int(colName)
                    colDF.columns = ["Year", "Month", "CPI"]
                    tallSkinnyDF.loc[
                        startIndex : startIndex + len(colDF.index) - 1,
                        ["Year", "Month", "CPI"],
                    ] = np.array(colDF)
                    startIndex += len(colDF.index)

            tallSkinnyDF["Year"] = tallSkinnyDF.Year.astype(int)
            tallSkinnyDF["Month"] = tallSkinnyDF.Month.astype(int)

            tallSkinnyDF.index = tallSkinnyDF.apply(
                lambda row: datetime.datetime(int(row.Year), int(row.Month), 15), axis=1
            )
            tallSkinnyDF = (
                tallSkinnyDF[~tallSkinnyDF.index.duplicated(keep="first")]
                .sort_index()
                .dropna()
            )

            self.storage.open()
            self.storage.put(
                "/CPI", tallSkinnyDF, format="Table", data_columns=self.data_columns
            )
            self.storage.close()
            driver.close()

    def sortStorageDB(self, saveCSV=True):
        return BaseData.formatStorageDB(
            reformatColumns=True,
            columnTypes=float,
            saveCSV=True,
            parentDir=os.path.join(self.mainDir, "Data"),
            subDir="HistoricalInflationData",
            fName="CPI_InflationRates.csv",
        )


# =============================================================================
# *****************************************************************************
# --------------------- TREASURY RATES AND DATA CLASS -------------------------
# *****************************************************************************
# =============================================================================


class TreasuryData:
    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=True,
        proxies=None,
        webDriver=None,
        webMethod="requests",
    ):
        if proxies is None:
            proxies = {}
        self.workProxyBool = workProxy
        self.mainDir = r"C:\Users\cp035982\Python\Projects\MarketAnalysis"
        if self.workProxyBool and not len(proxies):
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        else:
            self.proxies = proxies
        self.session = requests.session()
        self.session.proxies = self.proxies
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", "TreasuryData.h5")
        )
        self.treasuryList = self.storage.keys()
        self.storage.close()
        self.data_columns = None
        self.tempDB = None
        # Backup web access
        self.webMethod = webMethod
        self.driver = webDriver
        self.chromedriver = (
            "C:\\Users\\a0225347\\Documents\\Python"
            + "\\SeleniumWebDrivers\\chromedriver.exe"
        )
        self.phantomJSDriver = (
            "C:\\Users\\a0225347\\Documents\\Python\\"
            + "SeleniumWebDrivers\\"
            + "phantomjs-2.1.1-windows\\bin\\"
            + "phantomjs.exe"
        )


# =============================================================================
# *****************************************************************************
# --------------------------- OPTION DATA CLASS -------------------------------
# *****************************************************************************
# =============================================================================


class OptionData:
    """debug code
from MarketAnalysis import MarketData;reload(MarketData)
workProxy = True
lDB = MarketData.LIBORData(workProxy = workProxy)
sDB = MarketData.LIBORData(workProxy = workProxy)

#spy Call
tckr = 'SPY'
optionType = 'Call'
purchaseDate = "2017-04-16"
exDate = "2019-12-20"
strike = 225;S0 = 232.51
bidAskSpread = [26.85,28.0]

#SPX call
tckr = '.SPX'
optionType = 'Call'
purchaseDate = "2017-08-01"
exDate = "2017-08-28"
strike = 2445
S0 = 2476.31
bidAskSpread = [39.10,39.61]

reload(MarketData)
self = oDB = MarketData.OptionData(workProxy=workProxy, stockDB=sDB,
                                   LIBORData=lDB)
#stockPrice Function
oDB.PriceEst_BS(tckr, optionType, purchaseDate, strike, exDate, S0=S0)
"""

    def __init__(
        self,
        fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
        workProxy=False,
        proxies=None,
        webDriver=None,
        stockDB=None,
        liborDB=None,
    ):
        if proxies is None:
            proxies = {}
        self.workProxyBool = workProxy
        self.mainDir = fileDir
        if self.workProxyBool and not len(proxies):
            self.proxies = {
                "http": "http://webproxy.ext.ti.com:80/",
                "https": "https://webproxy.ext.ti.com:80",
                "ftp": "http://webproxy.ext.ti.com:80",
            }
        else:
            self.proxies = proxies
        #        self.session = requests.session()
        #        self.session.proxies = self.proxies
        #        self.yahooStorageCols = ['Strike','Last','Bid','Ask','Chg','PctChg',
        #           'Vol','Open_Int','IV','Underlying_Price']
        if liborDB is None:
            self.liborDB = LIBORData(
                fileDir=fileDir, workProxy=workProxy, proxies=proxies
            )
        else:
            self.liborDB = liborDB
        if stockDB is None:
            self.stockDB = StockData(fileDir=fileDir, workProxy=workProxy)
        else:
            self.stockDB = stockDB

        # Fidelity web scraping info
        self.webdriver_delay = webdriver_delay
        self.chromedriver = chromedriver
        self.phantomJSDriver = phantomJSDriver
        self.finalContextColumns = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "UnderlyingPrice",
            "Type",
            "Strike",
        ]
        self.finalDataColumns = [
            "Symbol",
            "Last",
            "Change",
            "Bid Size",
            "Bid",
            "Ask",
            "Ask Size",
            "Volume",
            "Open Int",
            "Imp Vol",
            "Delta",
            "Gamma",
            "Theta",
            "Vega",
            "Rho",
        ]
        self.hd5QueryCols = [
            "QueryTimeStamp",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "Type",
            "Strike",
            "Volume",
            "Imp Vol",
            "Open Int",
            "Delta",
        ]
        self.finalColumns = self.finalContextColumns + self.finalDataColumns
        self.finalDtypes = [
            "datetime64[ns]",
            "datetime64[ns]",
            np.int,
            np.object,
            np.float,
            np.object,
            np.float,
            np.object,
        ] + [np.float] * 14
        self.dataNameToColNumDict = dict(
            map(
                lambda dataName: (dataName, self.finalColumns.index(dataName)),
                self.finalColumns,
            )
        )
        self.monthAbbrToNumConvertion = {
            "Jan": 1,
            "Feb": 2,
            "Mar": 3,
            "Apr": 4,
            "May": 5,
            "Jun": 6,
            "Jul": 7,
            "Aug": 8,
            "Sep": 9,
            "Oct": 10,
            "Nov": 11,
            "Dec": 12,
        }
        self.fidelityDtypes = {
            "QueryTimeStamp": "<M8[ns]",
            "ExDate": "<M8[ns]",
            "DaysToExpiration": "int32",
            "UnderlyingSymbol": "str",
            "UnderlyingPrice": "float32",
            "Type": "str",
            "Strike": "float32",
            "Symbol": "str",
            "Last": "float32",
            "Change": "float32",
            "Bid Size": "int32",
            "Bid": "float32",
            "Ask": "float32",
            "Ask Size": "int32",
            "Volume": "int32",
            "Open Int": "int32",
            "Imp Vol": "float32",
            "Delta": "float32",
            "Gamma": "float32",
            "Theta": "float32",
            "Vega": "float32",
            "Rho": "float32",
        }
        # Inititialize the database
        self.storage = pd.HDFStore(
            os.path.join(self.mainDir, "Data", "DailyOptionQuotesFidelity.h5")
        )
        self.keyList = self.storage.keys()
        self.storage.close()
        self.driver = webDriver

    def createOption_BS(
        self,
        tckr,
        purchaseDate,
        targetExpiration,
        targetStrike,
        optionType,
        currentPrice=None,
        strikeCondition="=",
        expirationCondition="=",
        source="storage",
    ):
        """
        o  tckr is the symbol of the underlying security
        o  purchase date is the date that the contracts would be purchased
        o  current price is the price of the stock on the purchase date
        o  target expiration is the ideal expiration date
        o  target strike is the ideal strike price
        o  strike condition can be one of ['<','=','>'] meaning must be
            less than the target, closest, must be greater than the
            target respectively
        o  expiration condition ... see strike condition for similar formatting
tckr = 'ixSPX'
purchaseDate = datetime.datetime(2017,8,4)
currentPrice = 2473.48
targetExpiration = datetime.datetime(2018,12,15)
targetStrike = currentPrice*.8
strikeCondition = '='
expirationCondition = '='
reload(MH)
        """
        if currentPrice is None:
            currentPrice = self.stockDB.getPrice(tckr, purchaseDate, source=source)
            assert np.isfinite(currentPrice), (
                "Invalid Price returned to "
                + "MarketData.OptionData.createOption_BS. "
                + "Tckr: %s, Date: %s, Price: %s"
                % (tckr, str(purchaseDate), str(currentPrice))
            )

        exDate, optionRange = self.findClosestExDate(
            purchaseDate, targetExpiration, expirationCondition=expirationCondition
        )
        closestStrike = self.findClosestStrikePrice(
            purchaseDate,
            exDate,
            targetStrike,
            optionRange,
            strikeCondition,
            currentPrice,
        )
        priceBS = self.PriceEst_BS(
            tckr,
            optionType,
            purchaseDate,
            closestStrike,
            exDate,
            S0=currentPrice,
            source=source,
        )
        return priceBS, MH.formatDateInput(exDate), closestStrike

    @staticmethod
    def findClosestExDate(purchaseDate, targetExpiration, expirationCondition="="):
        # Find closest expiration date that meets condition
        targetExpiration = MH.formatDateInput(targetExpiration, dateType=np.datetime64)
        purchaseDate = MH.formatDateInput(purchaseDate)
        exDates = OptionData.estimateSPXOptionDates(purchaseDate)
        if expirationCondition == "<":
            exDates = exDates.loc[exDates.index < targetExpiration]
        elif expirationCondition == "<=":
            exDates = exDates.loc[exDates.index <= targetExpiration]
        elif expirationCondition == ">":
            exDates = exDates.loc[exDates.index > targetExpiration]
        elif expirationCondition == ">=":
            exDates = exDates.loc[exDates.index >= targetExpiration]
        purchaseDate = MH.formatDateInput(purchaseDate, dateType=np.datetime64)
        exDates["DaysOut"] = np.array(exDates.index - purchaseDate).astype(
            np.timedelta64
        ) / np.timedelta64(1, "D")
        exDates["TargetDelta"] = np.array(exDates.index - targetExpiration).astype(
            np.timedelta64
        ) / np.timedelta64(1, "D")
        if expirationCondition == "<":
            absDelta = exDates.TargetDelta[exDates.TargetDelta < 0].abs()
            closestExpiration = exDates.index[exDates.TargetDelta == absDelta.min()][0]
        elif expirationCondition == ">":
            absDelta = exDates.TargetDelta[exDates.TargetDelta > 0].abs()
            closestExpiration = exDates.index[exDates.TargetDelta == absDelta.min()][0]
        else:  # (expirationCondition in ['=','Closest']):
            absDelta = exDates.TargetDelta.abs()
            closestExpiration = exDates.index[absDelta == absDelta.min()][0]
        return closestExpiration, exDates.loc[closestExpiration, "OptionRange"]

    @staticmethod
    def findClosestStrikePrice(
        purchaseDate, exDate, targetStrike, optionRange, strikeCondition, currentPrice
    ):
        """
        StrikeCondition can be ['=','<','<=','>','>='] indicating
        whether to pick the absolute closest, next lower, or next higher
        available options. '=' indicates closest value
        """
        # print purchaseDate,exDate,targetStrike,optionRange,strikeCondition
        deltaDays = (
            MH.formatDateInput(exDate, dateType=np.datetime64)
            - MH.formatDateInput(purchaseDate, dateType=np.datetime64)
        ) / np.timedelta64(1, "D")
        offeringType = []
        if "W" in optionRange:
            offeringType.append("weekly")
        if "M" in optionRange:
            offeringType.append("monthly")
        if "Q" in optionRange:
            offeringType.append("quarterly")
        if "Y" in optionRange:
            offeringType.append("yearly")
        strikeOptions = np.array(
            OptionData.estimateSPXStrikesOffered(
                currentPrice, deltaDays, offeringType=offeringType
            )
        )
        if strikeCondition == "<":
            strikeOptions = strikeOptions[strikeOptions < targetStrike]
        elif strikeCondition == "<=":
            strikeOptions = strikeOptions[strikeOptions <= targetStrike]
        elif strikeCondition == ">":
            strikeOptions = strikeOptions[strikeOptions > targetStrike]
        elif strikeCondition == ">=":
            strikeOptions = strikeOptions[strikeOptions >= targetStrike]
        strikeDeltas = np.abs(strikeOptions - targetStrike)
        return float(strikeOptions[strikeDeltas == strikeDeltas.min()])

    def PriceEst_BS(
        self,
        tckr,
        optionType,
        dateOfPrice,
        strike,
        exDate,
        S0=None,
        sigma=None,
        rate=None,
        source="storage",
    ):
        """Estimate the Black and Scholes Price for the Option
            o  tckr is the market symbol for the underlying asset
            o  optionType is 'C' for call or 'P' for put
            o  purchase date is the day that you would sell or buy the option
            o  strike is the strike price of the option contract
            o  exDate is the expiration date of the option contract
            o  S0 is price of the underlying asset
            o  K is the strike price
            o  sigma is the volatility
            o  r is the risk free rate (annual rate, expressed in terms of
               continuous compounding)
            o  T is the time to maturity expressed in years
        """
        tckr = MH.convertTckrToUniversalFormat(tckr)
        exDate = MH.formatDateInput(exDate)
        dateOfPrice = MH.formatDateInput(dateOfPrice)
        daysOut = np.array((exDate - dateOfPrice).days)
        T = daysOut / 365.0
        method = optionType[0].upper()
        if S0 is None:
            S0 = self.stockDB.getPrice(tckr, dateOfPrice, source=source)
        if sigma is None:
            sigma = (
                self.stockDB.PredictSPXVol_1m3mVIX(dateOfPrice, exDate, source=source)
                / 100.0
            )
        if rate is None:
            rate = (
                self.liborDB.getLIBOR_Rate(dateOfPrice, daysOut, source=source) / 100.0
            )
        return MH.BlackScholes(method, S0, strike, sigma, rate, T)

    @staticmethod
    def estimateAllSPXOptions(
        currentDate,
        weekly=True,
        mon=True,
        wed=True,
        fri=True,
        monthly=True,
        quarterly=True,
        yearly=True,
    ):
        """ in work """
        spxDates = OptionData.estimateSPXOptionDates(
            currentDate,
            weekly=weekly,
            mon=mon,
            wed=wed,
            fri=fri,
            monthly=monthly,
            quarterly=quarterly,
            yearly=yearly,
        )
        spxDates["exDate"] = spxDates.index
        spxDates.index = range(spxDates.index.size)
        spxOptions = pd.DataFrame(columns=spxDates.columns + ["StrikePrice"])
        for exDate in spxDates.index:
            deltaDays = (
                MH.formatDateInput(exDate, dateType=np.datetime64)
                - MH.formatDateInput(currentDate, dateType=np.datetime64)
            ) / np.timedelta64(1, "D")
        offeringType = []
        if "W" in offeringType:
            offeringType.append("weekly")
        if "M" in offeringType:
            offeringType.append("monthly")
        if "Q" in offeringType:
            offeringType.append("quarterly")
        if "Y" in offeringType:
            offeringType.append("yearly")
        strikes = np.array(
            OptionData.estimateSPXStrikesOffered(
                currentPrice, deltaDays, offeringType=offeringType
            )
        )
        newOptions = pd.DataFrame()
        spxOptions = spxOptions.append(pd.D)

    @staticmethod
    def estimateSPXOptionDates(
        currentDate,
        weekly=True,
        mon=True,
        wed=True,
        fri=True,
        monthly=True,
        quarterly=True,
        yearly=True,
    ):
        """
        Assumes:
            o  weekly options for 8 weeks including the current week
            o  monthly options on the last business day and the Friday
                after the 15th day of the month for 6 months
            o  quarterly options on the Friday after the 15th day of the
                last month of the quarter for 4 quarters
            o  yearly options on the Friday after the 15th day of june and
                dec for the next 2 years
        """
        exDates = {}
        # weekly option dates
        if weekly:
            # current_date.isoweekday()%7 will give the the day of the week
            #  starting with sunday as the first day - defualt is monday
            #  as the first day)
            # endoOfWeeklies points to the sunday of the 9th week
            endOfWeeklies = currentDate + pd.Timedelta(
                days=(8 * 7) - (currentDate.isoweekday() % 7)
            )
            for wDate in pd.bdate_range(start=currentDate, end=endOfWeeklies, freq="B"):
                wDate = MH.formatDateInput(wDate)
                if mon and wDate.isoweekday() % 7 == 1:
                    if wDate in exDates.keys():
                        exDates[wDate] += "Wm"
                    else:
                        exDates[wDate] = "Wm"
                if wed and wDate.isoweekday() % 7 == 3:
                    if wDate in exDates.keys():
                        exDates[wDate] += "Ww"
                    else:
                        exDates[wDate] = "Ww"
                if fri and wDate.isoweekday() % 7 == 5:
                    if wDate in exDates.keys():
                        exDates[wDate] += "Wf"
                    else:
                        exDates[wDate] = "Wf"
        # monthly option dates
        if monthly:
            plus6Months = currentDate + pd.Timedelta(days=180)
            finalYear, finalMonth = plus6Months.year, plus6Months.month
            iterateYear, iterateMonth = currentDate.year, currentDate.month
            while finalYear > iterateYear or (
                finalYear == iterateYear and finalMonth >= iterateMonth
            ):
                middleOfMonth = MH.findMiddleFridayOfMonth(iterateYear, iterateMonth)
                endOfMonth = MH.findLastBusinessDayOfMonth(iterateYear, iterateMonth)
                if middleOfMonth in exDates.keys():
                    exDates[middleOfMonth] += "M"
                else:
                    exDates[middleOfMonth] = "M"
                if endOfMonth in exDates.keys():
                    exDates[endOfMonth] += "M"
                else:
                    exDates[endOfMonth] = "M"
                iterateMonth += 1
                iterateYear += int(iterateMonth / 13)
                iterateMonth = iterateMonth % 12 if iterateMonth != 12 else 12

        # Quarterly option dates
        if quarterly:
            lastMonthInQuarterList = np.array([3, 6, 9, 12])
            # Add 1 to current_date.month as a simple cheat to avoid some
            #  issues in backtest where
            # Options are called days before new offerings are available.
            #  Nothing changes for months that don't have offerings, but
            #  months with offerings will get to pick from offers a
            #  year currentDay
            startMonth = min(
                lastMonthInQuarterList[
                    lastMonthInQuarterList >= (currentDate.month + 1) % 13
                ]
            )
            startYear = currentDate.year
            for i in range(4):
                month = startMonth + i * 3
                year = startYear + int(month / 12)
                month %= 12
                if month == 0:
                    month = 12
                    year -= 1
                middleOfMonth = MH.findMiddleFridayOfMonth(year, month)
                endOfMonth = MH.findLastBusinessDayOfMonth(year, month)
                if middleOfMonth in exDates.keys():
                    exDates[middleOfMonth] += "Q"
                else:
                    exDates[middleOfMonth] = "Q"
                if endOfMonth in exDates.keys():
                    exDates[endOfMonth] += "Q"
                else:
                    exDates[endOfMonth] = "Q"

        if yearly:
            # Next year's yearly options
            # Cut off the yearly options so that the last date is the Jan
            #  offering in the third year after the current one
            year = currentDate.year
            juneDate = MH.findMiddleFridayOfMonth(year + 1, 6)
            decDate = MH.findMiddleFridayOfMonth(year + 1, 12)
            if juneDate in exDates.keys():
                exDates[juneDate] += "Y"
            else:
                exDates[juneDate] = "Y"
            if decDate in exDates.keys():
                exDates[decDate] += "Y"
            else:
                exDates[decDate] = "Y"
            # Following year's yearly options
            juneDate = MH.findMiddleFridayOfMonth(year + 2, 6)
            decDate = MH.findMiddleFridayOfMonth(year + 2, 12)
            if juneDate in exDates.keys():
                exDates[juneDate] += "Y"
            else:
                exDates[juneDate] = "Y"
            if decDate in exDates.keys():
                exDates[decDate] += "Y"
            else:
                exDates[decDate] = "Y"
        exDates = pd.DataFrame(data=exDates, index=["OptionRange"]).transpose()

        exDates = exDates.sort_index()
        return exDates

    @staticmethod
    def createGenericStrikePriceRange(
        currentPrice, strikeDeltas, lowerBounds, upperBounds
    ):
        strikes = []
        for i in range(len(strikeDeltas)):
            delta = strikeDeltas[i]
            start = round(
                lowerBounds[i] * currentPrice
                + (delta - lowerBounds[i] * currentPrice % delta),
                1,
            )
            end = round(
                upperBounds[i] * currentPrice - upperBounds[i] * currentPrice % delta, 1
            )
            num = int((end - start) / delta) + 1
            strikes += np.linspace(start, end, num=num, endpoint=True).tolist()
        strikes = list(set(strikes))
        strikes.sort()
        return strikes

    @staticmethod
    def estimateWeeklyExperirationStrikes(currentPrice):
        """ 5 deltas to model
            largest delta: start at 45% and ends at 120%
            max-1: start at 50% of the currentprice and end at 115%
            max-2: start at 72% and end at 112%
            max-3: start at 80% and end at 109%
            smallest: start at 85% and end at 107%
        """
        lowerBounds = [0.85, 0.80, 0.72, 0.5, 0.45]
        upperBounds = [1.07, 1.09, 1.12, 1.15, 1.2]
        if currentPrice > 1000:
            strikeDeltas = [5, 10, 25, 50, 100]
        elif currentPrice > 500:
            strikeDeltas = [2.5, 5, 10, 25, 50]
        elif currentPrice > 200:
            strikeDeltas = [1, 2.5, 5, 10, 25]
        elif currentPrice > 100:
            strikeDeltas = [0.5, 1, 2.5, 5, 10]
        elif currentPrice > 50:
            strikeDeltas = [0.25, 0.5, 1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            currentPrice, strikeDeltas, lowerBounds, upperBounds
        )

    @staticmethod
    def estimateMonthlyExperiationStrikes(currentPrice):
        """
        3 delta's
            largest delta: starts at 40% ends at  120%
            n-1: starts at 55% and ends at 115%
            n-2: starts at 70% and ends at 110%
        """
        lowerBounds = [0.70, 0.55, 0.40]
        upperBounds = [1.10, 1.15, 1.20]
        if currentPrice > 1000:
            strikeDeltas = [25, 50, 100]
        elif currentPrice > 500:
            strikeDeltas = [10, 25, 50]
        elif currentPrice > 200:
            strikeDeltas = [5, 10, 25]
        elif currentPrice > 100:
            strikeDeltas = [2.5, 5, 10]
        elif currentPrice > 50:
            strikeDeltas = [1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            currentPrice, strikeDeltas, lowerBounds, upperBounds
        )

    @staticmethod
    def estimateQuarterlyExperirationStrikes(currentPrice, daysOut):
        """
        3 deltas to model
            largest delta: start at  (19.655ln(x) - 66.444)/100 percent
                of the currentprice and ends at 120%
            middle delt: start at (18.373ln(days out) - 42.749)/100 percent
                of the currentprice and end at 115%
            smallest delta: start at (19.752ln(x) - 37.118)/100 percent of
                the currentprice and end at 110%
        """
        lowerBounds = [
            (19.752 * np.log(daysOut) - 37.118) / 100.0,
            (18.373 * np.log(daysOut) - 42.749) / 100.0,
            (19.655 * np.log(daysOut) - 66.444) / 100.0,
        ]
        upperBounds = [1.10, 1.15, 1.20]
        if currentPrice > 1000:
            strikeDeltas = [25, 50, 100]
        elif currentPrice > 500:
            strikeDeltas = [10, 25, 50]
        elif currentPrice > 200:
            strikeDeltas = [5, 10, 25]
        elif currentPrice > 100:
            strikeDeltas = [2.5, 5, 10]
        elif currentPrice > 50:
            strikeDeltas = [1, 2.5, 5]
        return OptionData.createGenericStrikePriceRange(
            currentPrice, strikeDeltas, lowerBounds, upperBounds
        )

    @staticmethod
    def estimateYearlyExpirationStrikes(currentPrice):
        """ 3 delta's
            largest delta: starts at 0+delta ends at  140%
            n-1: starts at 25% and ends at 115%
            n-2: starts at 35% and ends at 110%"""
        assert np.isfinite(currentPrice), (
            "Invalid price passed to "
            + "MarketHelper.estimateYearlyExpirationStrikes, "
            + str(currentPrice)
        )
        lowerBounds = [0.35, 0.25, 0.01]
        upperBounds = [1.10, 1.15, 1.40]
        if currentPrice > 1000:
            strikeDeltas = [25, 50, 100]
        elif currentPrice > 500:
            strikeDeltas = [10, 25, 50]
        elif currentPrice > 200:
            strikeDeltas = [5, 10, 25]
        elif currentPrice > 100:
            strikeDeltas = [2.5, 5, 10]
        elif currentPrice > 50:
            strikeDeltas = [1, 2.5, 5]
        else:
            strikeDeltas = [0.25, 0.5, 1]
        return OptionData.createGenericStrikePriceRange(
            currentPrice, strikeDeltas, lowerBounds, upperBounds
        )

    @staticmethod
    def estimateSPXStrikesOffered(
        currentPrice, daysOut, offeringCode=None, offeringType=None
    ):
        """
        offeringCode is the same as the offeringRange returned
            by estimateSPXOptionDates
        offeringType is depreciated but still valid as a backup. It can
            be one or more of ['weekly','monthly','quarterly','yearly']
        """
        if offeringType is None:
            offeringType = ["weekly", "monthly", "quarterly", "yearly"]
        if type(offeringType) == str:
            offeringType = [offeringType]
        indicators = []
        if offeringCode is not None:
            offeringCode = offeringCode.upper()
            if "W" in offeringCode:
                indicators.append("W")
            if "M" in offeringCode:
                indicators.append("M")
            if "Q" in offeringCode:
                indicators.append("Q")
            if "Y" in offeringCode:
                indicators.append("Y")
        else:
            # Backup implementation of offeringType
            for offering in offeringType:
                indicators.append(offering[0].upper())

        strikes = []
        for indicator in indicators:
            if indicator in ["W", "M", "Q", "Y"]:
                if indicator == "W":
                    strikes += OptionData.estimateWeeklyExperirationStrikes(
                        currentPrice
                    )
                elif indicator == "M":
                    strikes += OptionData.estimateMonthlyExperiationStrikes(
                        currentPrice
                    )
                elif indicator == "Q":
                    strikes += OptionData.estimateQuarterlyExperirationStrikes(
                        currentPrice, daysOut
                    )
                elif indicator == "Y":
                    strikes += OptionData.estimateYearlyExpirationStrikes(currentPrice)
        strikes = list(set(strikes))
        strikes.sort()
        return strikes

    def loadFromDB(
        self,
        underlyingSymbol,
        optionType=None,
        query_Dates=None,
        ex_Dates=None,
        strike=None,
        daysToExpiration=None,
        delta=None,
        columns=None,
        debug=False,
    ):
        """
        Key word arguments that are dates or values should be formated
        as tuples or lists such that (min,max). If the value isn't a tuple
        then the function will look for an exact match
        """
        # Ensure proper formatting of inputs
        underlyingSymbol = underlyingSymbol.upper().replace(".", "ix")
        if query_Dates:
            if not type(query_Dates) in [list, tuple]:
                query_Dates = [
                    datetime.datetime(
                        query_Dates.year, query_Dates.month, query_Dates.day
                    ),
                    (
                        datetime.datetime(
                            query_Dates.year, query_Dates.month, query_Dates.day
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            elif query_Dates[0] == query_Dates[1]:
                query_Dates = [
                    datetime.datetime(
                        query_Dates[0].year, query_Dates[0].month, query_Dates[0].day
                    ),
                    (
                        datetime.datetime(
                            query_Dates[0].year,
                            query_Dates[0].month,
                            query_Dates[0].day,
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            if query_Dates[0] is not None:
                query_Dates[0] = MH.formatDateInput(query_Dates[0])
            if query_Dates[1] is not None:
                query_Dates[1] = MH.formatDateInput(query_Dates[1])

        if ex_Dates:
            if not type(ex_Dates) in [list, tuple]:
                ex_Dates = [
                    datetime.datetime(ex_Dates.year, ex_Dates.month, ex_Dates.day),
                    (
                        datetime.datetime(ex_Dates.year, ex_Dates.month, ex_Dates.day)
                        + datetime.timedelta(days=1)
                    ),
                ]
            elif ex_Dates[0] == ex_Dates[1]:
                ex_Dates = [
                    datetime.datetime(
                        ex_Dates[0].year, ex_Dates[0].month, ex_Dates[0].day
                    ),
                    (
                        datetime.datetime(
                            ex_Dates[0].year, ex_Dates[0].month, ex_Dates[0].day
                        )
                        + datetime.timedelta(days=1)
                    ),
                ]
            if ex_Dates[0]:
                ex_Dates[0] = MH.formatDateInput(ex_Dates[0])
            if ex_Dates[1]:
                ex_Dates[1] = MH.formatDateInput(ex_Dates[1])

        if optionType and "C" in optionType.upper():
            optionType = "Call"
        elif optionType and "P" in optionType.upper():
            optionType = "Put"
        elif optionType:
            raise ValueError(
                "optionType: %s is neither " % optionType
                + "'Call' or 'Put' and is not valid"
            )

        if strike and not type(strike) in [list, tuple]:
            strike = [strike, strike]

        if daysToExpiration and not type(daysToExpiration) in [list, tuple]:
            daysToExpiration = [daysToExpiration, daysToExpiration]

        if delta and not type(delta) in [list, tuple]:
            delta = [delta, delta]

        def addToQueryConditions(queryConditions, rangeVar, varNameStr, colName):
            # create the indexCoordinates
            if not rangeVar:
                return queryConditions
            elif dateRange[0] == dateRange[1]:
                queryConditions.append("%s==%s[0]" % (colName, varNameStr))
            elif dateRange[0]:
                if dateRange[1]:
                    queryConditions.append("%s>=%s[0]" % (colName, varNameStr))
                    queryConditions.append("%s<=%s[1]" % (colName, varNameStr))
                else:
                    queryConditions.append("%s>=%s[0]" % (colName, varNameStr))
            else:
                if dateRange[1]:
                    queryConditions.append("%s<=%s[1]" % (colName, varNameStr))
            return queryConditions

        queryConditions = buildWhereStrRangeComponent(
            [], query_Dates, "query_Dates", "QueryTimeStamp"
        )
        queryConditions = buildWhereStrRangeComponent(
            queryConditions, ex_Dates, "ex_Dates", "ExDate"
        )
        queryConditions = buildWhereStrRangeComponent(
            queryConditions, strike, "strike", "Strike"
        )
        queryConditions = buildWhereStrRangeComponent(
            queryConditions, daysToExpiration, "daysToExpiration", "DaysToExpiration"
        )
        queryConditions = buildWhereStrRangeComponent(
            queryConditions, delta, "delta", "Delta"
        )

        whereStr = ""
        if optionType is not None:
            queryConditions.append("Type==optionType")
            if whereStr == "":
                whereStr += "Type=optionType"
            else:
                whereStr += " & Type=optionType"

        indexCoordinates = self.storage.select_as_coordinates(tckr, queryConditions)

        if columns is not None:
            if type(columns) != list:
                columns = [columns]

        if debug:
            print(whereStr)
        self.storage.open()
        if len(indexCoordinates) > 0:
            if columns:
                # Use both index and columns conditions
                optionData = self.storage.select(
                    underlyingSymbol, where=indexCoordinates, columns=columns
                )
            else:
                # Use only the index condition
                optionData = self.storage.select(
                    underlyingSymbol, where=indexCoordinates
                )
        else:
            if columns:
                # Use only the columns condition
                optionData = self.storage.select(underlyingSymbol, columns=columns)
            else:
                # Use none of the conditions
                optionData = self.storage.get(underlyingSymbol)
        self.storage.close()
        return optionData

    def populateTempDB(self):
        pass

    def getMinMaxDate(self):
        pass

    def getValidRows(self):
        pass

    def updateDB(self):
        return self.mineFidelityOptions(["ixSPX", "SPY", "IVV"])

    def updateStorageQueryColumns(self):
        self.storage.open()
        for tckr in self.storage.keys():
            print(tckr)
            self.storage.put(
                tckr,
                self.storage.get(tckr),
                format="table",
                data_columns=self.hd5QueryCols,
            )
        self.storage.close()

    # =========================================================================
    # *************************************************************************
    # ----------------- Begin Fidelity Web Scraping Functions -----------------
    # *************************************************************************
    # =========================================================================
    def mineFidelityOptions(
        self, symbols, loadInBatches=True, returnData=False, debug=False
    ):
        outputTotal = {}
        if type(symbols) is not list:
            symbols = [symbols]

        for symbol in symbols:
            print("Mining Options for '%s'" % symbol)
            if returnData:
                outputTotal[symbol] = self.mineFidelityOptions_SingleStock(
                    symbol,
                    loadInBatches=loadInBatches,
                    returnData=returnData,
                    debug=debug,
                )
            else:
                self.mineFidelityOptions_SingleStock(
                    symbol,
                    loadInBatches=loadInBatches,
                    returnData=returnData,
                    debug=debug,
                )
                outputTotal = None
        self.closeDriver()
        return outputTotal

    def mineFidelityOptions_SingleStock(
        self, symbol, loadInBatches=False, returnData=False, debug=False
    ):
        # Initialize and clean up mandatory variables
        start = datetime.datetime.today()
        symbol = symbol.upper()
        storageSymbol = symbol.replace(".", "ix")
        websymbol = "SPY" if symbol == "ixSPX" else symbol
        # get the time of the query
        queryTimeStamp = pd.Timestamp(datetime.datetime.today())
        optionData_Total = self.formatFidelityOptionDataFrame(
            pd.DataFrame(columns=self.finalColumns)
        )
        col_headings = None
        call_put_transition = None
        if self.driver is None:
            if debug:
                self.driver = driver = webdriver.Chrome(
                    executable_path=self.chromedriver
                )
            else:
                # BUG - Having problems with phantomJS not loading the
                # fidelity webpage. Switching to chromedriver until I can
                # figure it out
                self.driver = driver = webdriver.Chrome(
                    executable_path=self.chromedriver
                )
            #                self.driver = driver = webdriver.PhantomJS(
            #                        executable_path=self.phantomJSDriver,
            #                        service_log_path=os.path.devnull)
            #                driver.set_window_size(1120, 550)
            print("Loaded WebDriver")
        else:
            driver = self.driver

        exDateQty = self.init_option_webpage(driver, websymbol)
        # Cycle through experation dates one at a time and harvest each
        #  set of data
        iExDate = 0
        while True:
            #            try:
            optionData, col_headings, call_put_transition = self.read_nth_option(
                iExDate,
                symbol,
                exDateQty,
                queryTimeStamp,
                driver,
                col_headings=col_headings,
                call_put_transition=call_put_transition,
            )
            if optionData is not None:
                optionData_Total = self.append_option_data(
                    optionData_Total,
                    optionData,
                    loadInBatches,
                    returnData,
                    storageSymbol,
                )
            iExDate += 1
            if iExDate == exDateQty:
                break
        #            except Exception as ex:
        #                print("\t\t Failed mining %s Option %01i of %01i. " % (
        #                        symbol, iExDate+1, exDateQty)
        #                        + "Extending webDriver wait time and rerunning")
        #                print(ex)
        #                self.webdriver_delay = self.webdriver_delay+0.5
        #                exDateQty = self.init_option_webpage(driver, websymbol)

        # if load in batches is false, then we need to load the entire
        #  dataset at this point and then close the webdriver
        if not loadInBatches:
            self.storage.open()
            if "/" + storageSymbol in self.storage.keys():
                startIndex = self.storage.select_column(storageSymbol, "index").size
                optionData_Total = self.formatFidelityOptionDataFrame(
                    optionData_Total, startIndex=startIndex
                )
                self.storage.append(
                    storageSymbol,
                    optionData_Total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            else:
                optionData_Total = self.formatFidelityOptionDataFrame(
                    optionData_Total, startIndex=0
                )
                self.storage.put(
                    storageSymbol,
                    optionData_Total,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            self.storage.close()
            if not returnData:
                optionData_Total = None
        else:
            if returnData:
                optionData_Total = self.loadFromDB(symbol, query_Date=queryTimeStamp)
            else:
                optionData_Total = None
        end = datetime.datetime.today()
        print(
            "Completed fidelity option query for "
            + "'%s' in %s" % (symbol, str(end - start))
        )
        return optionData_Total

    def closeDriver(self):
        self.driver.quit()
        self.driver = None

    def init_option_webpage(self, driver, websymbol):
        optionURL = (
            "https://researchtools.fidelity.com//ftgw/mloptions//"
            + "goto//optionChain?priced=Y&symbol=%s" % websymbol
        )
        # wait 3 sec every time you change the page, b/c I was having problems
        # with it loading a page with wrong dates
        driver.get(optionURL)
        time.sleep(3)
        print(driver.title + ": " + websymbol)
        # check for a login link and click on the login link if it exists
        loginLink = driver.find_elements_by_xpath('//*[@id="chainPeak"]')
        if len(loginLink) > 0:
            print("\tNeed to Login")
            loginLink[0].click()
            time.sleep(self.webdriver_delay)

            # enter username on new page
            driver.find_elements_by_xpath('//*[@id="userId-input"]')[0].send_keys(
                "coryperk"
            )
            driver.find_elements_by_xpath('//*[@id="password"]')[0].send_keys(
                "Hebrews135"
            )
            # press the submit button
            submit_button = driver.find_elements_by_xpath('//*[@id="fs-login-button"]')[
                0
            ]
            submit_button.click()
            time.sleep(self.webdriver_delay)
            driver.get(optionURL)
            time.sleep(3)
        print("\tLogin Successful")

        # ensure that all dates are unselected
        exDateBarElement = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
        )
        selectedElements = exDateBarElement.find_elements_by_class_name(
            "selected-state"
        )
        if len(selectedElements) > 0:
            for expirationElement in selectedElements:
                expirationElement.click()
                time.sleep(self.webdriver_delay)
            # click on the apply settings button
            apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
            apply_button.click()
            time.sleep(self.webdriver_delay)
        # get the total number of expiration dates
        exDateQty = len(
            driver.find_elements_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
            )
        )
        return exDateQty

    def append_option_data(
        self, data_total, data_single, loadInBatches, returnData, storageSymbol
    ):
        data_total = data_total.append(data_single)
        if loadInBatches:
            # If load in batches is true, then append each expiration
            #  date data to stored dataset and create a dataset if
            #  necessary
            self.storage.open()
            if "/" + storageSymbol in self.storage.keys():
                startIndex = self.storage.select_column(storageSymbol, "index").size
                data_single = self.formatFidelityOptionDataFrame(
                    data_single, startIndex=startIndex
                )
                self.storage.append(
                    storageSymbol,
                    data_single,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            else:
                data_single = self.formatFidelityOptionDataFrame(
                    data_single, startIndex=0
                )
                self.storage.put(
                    storageSymbol,
                    data_single,
                    format="table",
                    data_columns=self.hd5QueryCols,
                )
            self.storage.close()

        return data_total

    def formatFidelityOptionDataFrame(self, optionData, startIndex=0):
        for col in optionData.columns:
            optionData[col] = optionData[col].astype(self.fidelityDtypes[col])
        optionData.index = range(startIndex, startIndex + optionData.index.size)
        return optionData

    def read_nth_option(
        self,
        i_date,
        symbol,
        date_qty,
        queryTimeStamp,
        driver,
        col_headings=None,
        call_put_transition=None,
        debug=False,
    ):
        # select the current expiration date and apply the settings
        # reset the exDateBar
        exDateBarElement = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]/div/ul'
        )
        bar_length = len(
            driver.find_elements_by_xpath(
                '//*[@id="OptionSearchForm"]/div[2]/div[1]' + "/div[2]/div/ul/li"
            )
        )
        selectedElements = exDateBarElement.find_elements_by_class_name(
            "selected-state"
        )
        if len(selectedElements) > 0:
            for expirationElement in selectedElements:
                expirationElement.click()

        # if there is an offset caused by invalid dates, find the offset
        index_offset = 1
        while True:
            button_date = pd.Timestamp(
                driver.find_element_by_xpath(
                    '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
                    + "/div/ul/li[%1i]" % (index_offset)
                ).get_attribute("value")
            )
            if button_date > queryTimeStamp:
                break
            index_offset += 1
        if i_date + index_offset > bar_length:
            return None, col_headings, call_put_transition
        date_toggle = driver.find_element_by_xpath(
            '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
            + "/div/ul/li[%1i]" % (i_date + index_offset)
        )
        date_toggle.click()
        time.sleep(self.webdriver_delay)
        # ensure that only the selected map is toggled
        selectedElements = exDateBarElement.find_elements_by_class_name(
            "selected-state"
        )
        while True:
            if len(selectedElements) == 1:
                break
            for expirationElement in selectedElements:
                expirationElement.click()
            date_toggle.click()
            time.sleep(self.webdriver_delay)

        apply_button = driver.find_element_by_xpath('//*[@id="Bttn_Apply"]')
        apply_button.click()
        time.sleep(self.webdriver_delay)
        # Initialize column headers and dataFrames
        if col_headings is None or call_put_transition is None:
            # Get table headers by looping through the TH values until
            #  one doesn't exist
            print("\tMining Column Names")
            col_headings = []
            iCol = 1
            colHeaderElementList = driver.find_elements_by_xpath(
                '//*[@id="fullTable"]/thead[1]/tr/th'
            )
            for iCol in range(len(colHeaderElementList)):
                col_headings.append(colHeaderElementList[iCol].text)
            call_put_transition = col_headings.index("Strike")

        # get the price of the underlying symbol
        underlyingPrice = float(
            driver.find_elements_by_xpath('//*[@id="companyDetailsDiv"]/div/span[1]')[0]
            .text.replace("$", "")
            .replace(",", "")
        )

        # get the expiration date info from the first expiration date
        exDateInfo = (
            driver.find_elements_by_class_name("mth0")[0]
            .get_attribute("name")
            .split(" ")
        )
        exDate = datetime.date(
            int("20" + exDateInfo[2][1:]),
            self.monthAbbrToNumConvertion[exDateInfo[0]],
            int(exDateInfo[1]),
        )
        if "(" in exDateInfo[3]:
            daysToExpiration = int(exDateInfo[3][(exDateInfo[3].find("(") + 1) :])
        else:
            daysToExpiration = int(exDateInfo[4][(exDateInfo[4].find("(") + 1) :])
        # Get number of rows in table for the current exDate
        rowQty = driver.execute_script(
            "return document.getElementById"
            + '("fullTable").'
            + "getElementsByClassName("
            + "'mth0').length"
        )
        print(
            "\tMining %s expiration date " % (symbol)
            + "%01i of %01i: %s (%01i rows)"
            % (i_date + 1, date_qty, str(exDate), rowQty)
        )
        # Init new dataFrame for the expiration date
        optionData = pd.DataFrame(index=range(rowQty * 2), columns=self.finalColumns)
        # Line prefix is = ['QueryDate','QueryTime','ExDate',
        #  'DaysToExpiration','UnderlyingSymbol','UnderlyingPrice',
        #  'Type','Strike']
        optionData["QueryTimeStamp"] = np.array(
            [np.datetime64(queryTimeStamp)] * (rowQty * 2)
        ).astype(self.fidelityDtypes["QueryTimeStamp"])
        optionData["ExDate"] = np.array([np.datetime64(exDate)] * (rowQty * 2)).astype(
            self.fidelityDtypes["ExDate"]
        )
        optionData["DaysToExpiration"] = np.array(
            [daysToExpiration] * (rowQty * 2)
        ).astype(self.fidelityDtypes["DaysToExpiration"])
        optionData["UnderlyingSymbol"] = np.array([symbol] * (rowQty * 2)).astype(
            self.fidelityDtypes["UnderlyingSymbol"]
        )
        optionData["UnderlyingPrice"] = np.array(
            [float(str(underlyingPrice).replace("$", "").replace(",", ""))]
            * (rowQty * 2)
        ).astype(self.fidelityDtypes["UnderlyingPrice"])
        setCallBoolList = [True] * rowQty + [False] * rowQty
        setPutBoolList = [False] * rowQty + [True] * rowQty
        optionData["Type"] = np.array(["Call"] * (rowQty) + ["Put"] * (rowQty)).astype(
            self.fidelityDtypes["Type"]
        )
        optionData = self.processSingleExDateOptionData_XMLParser(
            driver,
            optionData,
            col_headings,
            call_put_transition,
            setCallBoolList,
            setPutBoolList,
            debug=debug,
        )

        #        # unselect the current expiration date
        #        time.sleep(self.webdriver_delay)
        #        date_toggle = driver.find_element_by_xpath(
        #                '//*[@id="OptionSearchForm"]/div[2]/div[1]/div[2]'
        #                + '/div/ul/li[%1i]' % (i_date+1))
        #        date_toggle.click()
        return optionData, col_headings, call_put_transition

    def processSingleExDateOptionData_XMLParser(
        self,
        driver,
        optionData,
        tableHeaderNames,
        callToPutTransistionIndex,
        setCallBoolList,
        setPutBoolList,
        debug=0,
    ):
        tableTree = html.fromstring(
            driver.find_element_by_xpath('//*[@id="fullTable"]').get_attribute(
                "innerHTML"
            )
        )
        targetRows = len(optionData.index) / 2
        for iCol in range(len(tableHeaderNames)):
            dataName = tableHeaderNames[iCol]
            # Different columns have different structurs that need to be
            #  navigated past the td html tag those that end in '/a' will
            #  also pick up a dummy value from the subheader of the table
            #  and so firstValidRow needs to be moved to 1 for only
            #  those columns
            xPathSuffix = ""
            if dataName in ["Symbol", "Bid", "Ask"]:
                xPathSuffix = "/a"
            elif dataName in ["Volume", "Open Int"]:
                xPathSuffix = "/div/span"

            # dtype determiniation
            datatype = str
            if dataName in [
                "Strike",
                "Last",
                "Change",
                "Bid",
                "Ask",
                "Imp Vol",
                "Delta",
                "Gamma",
                "Theta",
                "Vega",
                "Rho",
            ]:
                datatype = float
            elif dataName in ["Bid Size", "Ask Size", "Volume", "Open Int"]:
                datatype = int

            # start processing data

            # no valid data in action cell
            if dataName == "Action":
                continue

            # Get data as txt from column
            colTextList = np.array(
                map(
                    lambda x: x.strip().replace("$", "").replace(",", ""),
                    tableTree.xpath(
                        "//tbody/tr/td[%01i]%s/text()" % (iCol + 1, xPathSuffix)
                    ),
                ),
                dtype=str,
            )
            firstValidRow = len(colTextList) - targetRows
            colTextList = colTextList[firstValidRow:]
            if dataName == "Strike":
                # Add the strik to the call and put columns along with the
                #  other information at the start of the line
                optionData.loc[setCallBoolList, dataName] = colTextList.astype(float)
                optionData.loc[setPutBoolList, dataName] = colTextList.astype(float)
            elif dataName in ["Imp Vol"]:
                # remove the percent sign and return a float
                colTextList = np.array(map(lambda x: x[:-2], colTextList), dtype="|S5")
                colTextList[colTextList == ""] = np.nan
                if iCol < callToPutTransistionIndex:
                    optionData.loc[setCallBoolList, dataName] = colTextList.astype(
                        float
                    )
                else:
                    optionData.loc[setPutBoolList, dataName] = colTextList.astype(float)
            else:
                # add the data in the predetermined format such that empty
                #  cells are nan values for numeric columns
                if datatype in [int, float]:
                    colTextList = colTextList.astype("|S5")
                    colTextList[
                        np.logical_or(colTextList == "", colTextList == "--")
                    ] = np.nan
                if iCol < callToPutTransistionIndex:
                    optionData.loc[setCallBoolList, dataName] = colTextList.astype(
                        datatype
                    )
                else:
                    optionData.loc[setPutBoolList, dataName] = colTextList.astype(
                        datatype
                    )

        # Need to convert the numeric columns back to numeric dtypes
        # optionData[['Strike','Last','Bid','Ask','Change','Imp Vol',
        #  'Delta','Gamma','Theta','Vega','Rho']] = optionData[['Strike',
        #  'Last','Bid','Ask','Change','Imp Vol','Delta','Gamma','Theta',
        #  'Vega','Rho']].apply(pd.to_numeric,downcast = 'float')
        # optionData[['Bid Size','Ask Size','Volume','Open Int']] =
        #  optionData[['Strike','Last','Bid','Ask','Change','Bid Size',
        #  'Ask Size','Volume','Open Int','Imp Vol','Delta','Gamma','Theta',
        #  'Vega','Rho']].apply(pd.to_numeric,downcast = 'integer')
        return self.formatFidelityOptionDataFrame(optionData)

    def calculate_best_strategies(
        self,
        tckr,
        query_date,
        fid_data=None,
        predictionModel=None,
        n_mc=100,
        cum_cycles=100,
        max_risk=0.2,
        total_pf=100000,
        broker_fee=4.95,
        addtl_option_fee=0.12,
        date_selection="Closest",
        return_all_data=True,
        source="storage",
    ):
        """
        date_selection determines how to select the query date to match to
            the option data. Options are:
                'Closest'   - select the closest QueryTimeStamp to
                              the query_date
                'Day_First' - select the first QueryTimeStamp
                              within the same day as query_date
                'Day_Last'  - select the last QueryTimeStamp
                              within the same day as query_date
                'Day_X'     - where X is an integer, select the Xth
                              date (starting at 0) within the same day
                              as query_date. Default to the last occurance
                              if X is too large
        debug:
        tckr = '.SPX'
        query_date = pd.Timestamp(2018,1,30)
        predictionModel=None
        n_mc=100
        cum_cycles=100
        max_risk=0.2
        total_pf=100000
        broker_fee=4.95
        addtl_option_fee=.12
        date_selection='Closest'
        source='storage'
        """
        from MarketAnalysis.Positions import ComplexOption

        tckr = MH.convertTckrToUniversalFormat(tckr)
        date_selection = date_selection.upper()
        if predictionModel is None:
            from MarketAnalysis import MarketPredictions_Depreciated

            predictionModel = MarketPredictions_Depreciated.getDefaultPredictionModel_HDF5()
        if fid_data is None:
            query_date = MH.formatDateInput(query_date)
            fid_data = self.options_for_date(tckr, query_date, method="closest")
        # remove exDate less than 5 days away
        all_data = self.summarize_option_data(
            fid_data.loc[fid_data.DaysToExpiration > 4, :], price_calc="weighted"
        )
        current_price = all_data.UnderlyingPrice.iloc[0]
        query_date = all_data.QueryTimeStamp.iloc[0]
        all_exdates = pd.DatetimeIndex(list(set(all_data.ExDate)))
        output = []
        for i_date in range(all_exdates.size):
            exDate = all_exdates[i_date]
            print("* " * 30)
            print(
                "Calculating Best Strategy for expiratin date"
                + " %1i of %1i: %s"
                % (i_date, all_exdates.size, exDate.strftime("%m-%d-%Y"))
            )
            print("* " * 30)
            sub_DB = all_data.loc[
                all_data.ExDate == exDate, ["Symbol", "Type", "Strike", "Bid", "Ask"]
            ].drop_duplicates(subset="Symbol")
            select_call = sub_DB.Type == "Call"
            call_symbol = sub_DB.loc[select_call, "Symbol"].values
            call_strike = sub_DB.loc[select_call, "Strike"].values
            call_sell = sub_DB.loc[select_call, "Bid"].values
            call_buy = sub_DB.loc[select_call, "Ask"].values
            put_symbol = sub_DB.loc[~select_call, "Symbol"].values
            put_strike = sub_DB.loc[~select_call, "Strike"].values
            put_sell = sub_DB.loc[~select_call, "Bid"].values
            put_buy = sub_DB.loc[~select_call, "Ask"].values
            data, arbitrage = ComplexOption.run_all_strategies_single(
                tckr,
                query_date,
                exDate,
                call_symbol,
                call_strike,
                call_sell,
                call_buy,
                put_symbol,
                put_strike,
                put_sell,
                put_buy,
                predictionModel=predictionModel,
                optionDB=self,
                current_price=current_price,
                n_mc=n_mc,
                cum_cycles=cum_cycles,
                max_risk=max_risk,
                total_pf=total_pf,
                broker_fee=broker_fee,
                addtl_option_fee=addtl_option_fee,
                return_all_data=return_all_data,
                source=source,
            )
            output.append(data)

        output = pd.concat(output, axis=0, ignore_index=True)
        output.sort_values("Avg_PF_Return_PerDay", ascending=False, inplace=True)
        output.index = range(output.index.size)
        column_order = [
            "Strategy",
            "Category",
            "Outcome_Qty",
            "Yield_Cycles",
            "PurchaseDate",
            "ExpirationDate",
            "DaysToExp",
            "DaysInvested",
            "Max_Risk",
            "CurrentPrice",
            "Prediction_Price",
            "Prediction_Std",
            "Total_PF",
            "Trade_Fee",
            "Addtl_Contract_Fee",
            "Strike_0",
            "Strike_1",
            "Strike_2",
            "Strike_3",
            "Price_0",
            "Price_1",
            "Price_2",
            "Price_3",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Median_PF_Return",
            "Mean_PF_Return",
            "Std_PF_Return",
            "Avg_PF_Return_PerDay",
            "Avg_Ann_Return",
            "Expected_Stock_Return",
            "AvgReturnVsStock",
            "Expected_Gain_Return",
            "Win_Percentage",
            "Ann_Volatility",
        ]
        output = output.loc[:, column_order]

        return output

    def test_mc_predictions(
        self,
        tckr="ixSPX",
        predictionModel=None,
        n_mc=100,
        cum_cycles=100,
        max_risk=0.2,
        total_pf=100000,
        broker_fee=4.95,
        addtl_option_fee=0.12,
        return_all_data=True,
        source="storage",
    ):
        """
        run the default prediction model against all the fidelity options
        that have been pulled and test the monte carlo algorithm's accuracy/
        predictability

        debug:
        tckr = 'ixSPX'
        predictionModel=None
        n_mc=100
        cum_cycles=100
        max_risk=0.2
        total_pf=100000
        broker_fee=4.95
        addtl_option_fee=0.12
        return_all_data=True
        source='storage'
        """
        from MarketAnalysis.Positions import ComplexOption

        if predictionModel is None:
            from MarketAnalysis import MarketPredictions_Depreciated

            predictionModel = MarketPredictions_Depreciated.getDefaultPredictionModel_HDF5()
        str_cols = ["Strategy", "Category"]
        date_cols = ["PurchaseDate", "ExpirationDate"]
        num_cols = [
            "Outcome_Qty",
            "Yield_Cycles",
            "DaysToExp",
            "DaysInvested",
            "Max_Risk",
            "CurrentPrice",
            "Prediction_Price",
            "Prediction_Std",
            "Total_PF",
            "Trade_Fee",
            "Addtl_Contract_Fee",
            "Strike_0",
            "Strike_1",
            "Strike_2",
            "Strike_3",
            "Price_0",
            "Price_1",
            "Price_2",
            "Price_3",
            "Premium",
            "Max_Loss",
            "Max_Profit",
            "Median_PF_Return",
            "Mean_PF_Return",
            "Std_PF_Return",
            "Avg_PF_Return_PerDay",
            "Avg_Ann_Return",
            "Expected_Stock_Return",
            "AvgReturnVsStock",
            "Expected_Gain_Return",
            "Win_Percentage",
            "Ann_Volatility",
        ]
        final_cols = str_cols + date_cols + num_cols
        self.storage.open()
        unique_query_dates = pd.DatetimeIndex(
            list(set(self.storage.select_column("/" + tckr, "QueryTimeStamp")))
        )
        self.storage.close()
        final_df = []
        for i_query in range(unique_query_dates.size):
            query_date = unique_query_dates[i_query]
            print("\n" + "* " * 30)
            print(
                "Calculating Best Strategies for query date"
                + " %1i of %1i: %s"
                % (i_query, unique_query_dates.size, query_date.strftime("%m-%d-%Y"))
            )
            print("* " * 30)
            fid_data = self.options_for_date(
                tckr, query_date, method="closest"
            ).drop_duplicates()
            fid_data = fid_data.loc[fid_data.DaysToExpiration > 4, :]
            if fid_data.size == 0:
                continue
            fid_data = self.summarize_option_data(fid_data, price_calc="weighted")
            current_price = fid_data.UnderlyingPrice.iloc[0]
            unique_exdates = pd.DatetimeIndex(list(set(fid_data.ExDate)))
            unique_exdates = unique_exdates[unique_exdates < pd.Timestamp.today()]
            for i_ex in range(unique_exdates.size):
                exDate = unique_exdates[i_ex]
                sub_DB = fid_data.loc[
                    fid_data.ExDate == exDate,
                    ["Symbol", "Type", "Strike", "Bid", "Ask"],
                ].drop_duplicates(subset="Symbol")
                select_call = sub_DB.Type == "Call"
                call_strike = sub_DB.loc[select_call, "Strike"].values
                call_sell = sub_DB.loc[select_call, "Bid"].values
                call_buy = sub_DB.loc[select_call, "Ask"].values
                put_strike = sub_DB.loc[~select_call, "Strike"].values
                put_sell = sub_DB.loc[~select_call, "Bid"].values
                put_buy = sub_DB.loc[~select_call, "Ask"].values
                print(
                    "EXDate %1i of %1i: %s (%1i strikes)"
                    % (
                        i_ex,
                        unique_exdates.size,
                        exDate.strftime("%m-%d-%Y"),
                        call_strike.size,
                    )
                )
                print(" - " * 20)

                strgy_pred = ComplexOption.run_all_strategies_single(
                    tckr,
                    query_date,
                    exDate,
                    call_strike,
                    call_sell,
                    call_buy,
                    put_strike,
                    put_sell,
                    put_buy,
                    predictionModel=predictionModel,
                    optionDB=self,
                    current_price=current_price,
                    n_mc=n_mc,
                    cum_cycles=cum_cycles,
                    max_risk=max_risk,
                    total_pf=total_pf,
                    broker_fee=broker_fee,
                    addtl_option_fee=addtl_option_fee,
                    return_all_data=return_all_data,
                    source=source,
                )
                final_df.append(strgy_pred.loc[:, final_cols])

        final_df = pd.concat(final_df, axis=0, ignore_index=True)
        final_df[date_cols] = final_df[date_cols].apply(pd.to_datetime)
        final_df[num_cols] = final_df[num_cols].apply(pd.to_numeric)
        unique_exdates = pd.DatetimeIndex(list(set(final_df.ExpirationDate)))
        ex_prices = predictionModel.getCurrentPrice(unique_exdates, tckr, source=source)
        ex_prices.name = "Expiration_Price"
        final_df = pd.merge(
            final_df,
            pd.DataFrame(ex_prices),
            how="left",
            left_on="ExpirationDate",
            right_index=True,
        )
        final_df["Tckr_Gain"] = final_df.Expiration_Price / final_df.CurrentPrice
        final_df["Actual_Value"] = np.nan
        final_df["Actual_Gain"] = np.nan
        co = ComplexOption.ComplexOption
        for strategy in list(set(final_df.Strategy)):
            select_rows = final_df.Strategy == strategy
            strat_data = final_df.loc[select_rows, :]
            positions, cashflow_sign, quantity = co.interpret_strategy(strategy)
            legs = len(positions)
            strat_ex_price = strat_data.Expiration_Price
            strike_cols = ["Strike_0", "Strike_1", "Strike_2", "Strike_3"][:legs]
            strat_strikes = strat_data[strike_cols]
            strat_premium = strat_data.Premium
            actual_value = co.calc_value_expiration(
                strat_ex_price,
                strat_strikes,
                premium=strat_premium,
                positions=positions,
                cashflow_sign=cashflow_sign,
                quantity=quantity,
            )
            actual_gain = actual_value / np.abs(strat_data.Max_Loss.values).reshape(
                actual_value.size, 1
            )
            final_df.loc[select_rows, "Actual_Value"] = actual_value
            final_df.loc[select_rows, "Actual_Gain"] = actual_gain
        correlation = final_df.corr().sort_values("Actual_Gain")["Actual_Gain"]
        print(correlation)
        return final_df, correlation

    # =======================================================================
    # ***********************************************************************
    # -------------------- Begin Fidelity Database Access -------------------
    # ***********************************************************************
    # =======================================================================

    def closest_query_date(self, dbKey, query_date):
        """ OPTIMIZE - Can optimize by cleaning up the file open and
        close functionality"""
        self.storage.open()
        db_dates = pd.DatetimeIndex(
            list(set(self.storage.select_column("/" + dbKey, "QueryTimeStamp")))
        )
        self.storage.close()
        if type(query_date) in [
            pd.DatetimeIndex,
            pd.Index,
            list,
            set,
            np.ndarray,
            pd.Series,
        ]:
            query_date = MH.formatDateArray(
                [
                    db_dates[np.abs(db_dates - date).argmin()]
                    for date in MH.formatDateInput(query_date)
                ]
            )
        else:
            query_date = db_dates[
                np.abs(db_dates - MH.formatDateInput(query_date)).argmin()
            ]
        return query_date

    def options_for_date(self, dbKey, query_date, method="closest"):
        """
        OPTIMIZE - Can optimize by cleaning up the file open and
        close functionality
        'method' determines how to select the query_date to match to
            the option data. Options are:
                'Exact'     - must be an exact match to a record in the DB
                'Closest'   - select the closest QueryTimeStamp to
                              the query_date
                'Day_First' - select the first QueryTimeStamp
                              within the same day as query_date
                'Day_Last'  - select the last QueryTimeStamp
                              within the same day as query_date
                'Day_X'     - where X is an integer, select the Xth
                              date (starting at 0) within the same day
                              as query_date. Default to the last occurance
                              if X is too large
        """
        dbKey = MH.convertTckrToUniversalFormat(dbKey)
        query_date = MH.formatDateArray(query_date)
        method = method.upper()
        if method.upper() == "EXACT":
            pass
        elif method.upper() == "CLOSEST":
            query_date = self.closest_query_date(dbKey, query_date)
        elif "DAY" in method:
            if not self.storage.is_open:
                self.storage.open()
            db_dates = pd.DatetimeIndex(
                list(set(self.storage.select_column("/" + dbKey, "QueryTimeStamp")))
            )
            if "FIRST" in method:
                index_select = 0
            elif "LAST" in method:
                index_select = -1
            elif "_" in method and method.split("_")[-1].isdigit():
                index_select = int(method.split("_")[-1])
            else:
                index_select = None
            query_date = db_dates[np.isin(db_dates.date, query_date.date)]
            if index_select is not None:
                query_date = query_date[[min(query_date.size, index_select)]]

        if not self.storage.is_open:
            self.storage.open()
        fid_options = self.storage.select(
            "/" + dbKey, where="QueryTimeStamp = query_date"
        )
        self.storage.close()
        return fid_options

    def summarize_option_data(self, fid_options, price_calc="weighted"):
        """
        reduces some of the information stored in the database and calculates
        a price estimation based on the weighted averaged of the bid ask
        spread
        """
        final_columns = [
            "QueryTimeStamp",
            "Symbol",
            "ExDate",
            "DaysToExpiration",
            "UnderlyingSymbol",
            "UnderlyingPrice",
            "Type",
            "Strike",
            "Price",
            "Bid",
            "Ask",
            "Imp Vol",
        ]
        if price_calc.lower() == "weighted":
            fid_options.loc[fid_options.loc[:, "Bid Size"] == 0, "Bid Size"] = 1
            fid_options.loc[fid_options.loc[:, "Ask Size"] == 0, "Ask Size"] = 1
            fid_options.loc[:, "Price"] = (
                fid_options["Bid"] * fid_options["Bid Size"]
                + fid_options["Ask"] * fid_options["Ask Size"]
            ) / (fid_options["Bid Size"] + fid_options["Ask Size"])
        if price_calc.lower() == "middle":
            fid_options.loc[:, "Price"] = (fid_options["Bid"] + fid_options["Ask"]) / 2
        return fid_options[final_columns]

    def clean_options_db(self):
        self.storage.open()
        keys = self.storage.keys()
        for key in keys:
            data = self.storage["/" + key]
            data.index = None
            keep_index = None


def initiateAllDataBases(
    fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
    workProxy=True,
    proxies=None,
    cookieValue=None,
    crumbValue=None,
    webDriver=None,
):
    """
    Returns the stockDB,optionDB and liborDB (in that order) based on
    the input criteria.
    All DB's are coherent and linked to each other as needed
    """
    if proxies is None:
        proxies = {}
    sDB = StockData(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        cookieValue=cookieValue,
        crumbValue=crumbValue,
    )
    lDB = LIBORData(fileDir=fileDir, workProxy=workProxy, proxies=proxies)
    oDB = OptionData(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        webDriver=webDriver,
        stockDB=sDB,
        liborDB=lDB,
    )
    iDB = InflationData(
        fileDir=fileDir, workProxy=workProxy, proxies=proxies, webDriver=oDB.driver
    )
    return sDB, oDB, lDB, iDB


def updateAllDBs(
    fileDir=r"C:\Users\cp035982\Python\Projects\MarketAnalysis",
    workProxy=True,
    proxies=None,
    cookieValue=None,
    crumbValue=None,
    webDriver=None,
):
    if proxies is None:
        proxies = {}
    sDB, oDB, lDB, iDB = initiateAllDataBases(
        fileDir=fileDir,
        workProxy=workProxy,
        proxies=proxies,
        cookieValue=cookieValue,
        crumbValue=crumbValue,
        webDriver=webDriver,
    )
    sDB.updateDB()
    lDB.updateDB()
    iDB.updateDB()
    return sDB, oDB, lDB, iDB


def main():
    strike = 1000
    exDate = datetime.datetime(2019, 12, 20, 0, 0)
    underlyingTckr = "SPX"
    purchaseDate = datetime.datetime(2017, 8, 8)
    oDB = OptionData(workProxy=True)
    print(oDB.PriceEst_BS(underlyingTckr, "Call", purchaseDate, strike, exDate))


if __name__ == "__main__":
    main()
