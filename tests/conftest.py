import pytest
import os
from pathlib import Path

# noinspection PyUnresolvedReferences
from . import context


@pytest.fixture(scope="session")
def abs_test_dir() -> str:
    return os.path.abspath("./tests/")


@pytest.fixture(scope="session")
def abs_temp_dir() -> str:
    temp_path = os.path.abspath(r"./temp/")
    if not os.path.exists(temp_path):
        os.mkdir(temp_path)
    return temp_path


@pytest.fixture(scope="session")
def path_to_fixtures() -> Path:
    """the resolved path to the fixtures directory"""
    return Path(Path(__file__).parent, "fixtures").resolve()
