#!/usr/bin/env python
# noinspection PyUnresolvedReferences
import pytest
import numpy as np
import pandas as pd

# noinspection PyUnresolvedReferences
from .. import context
from src.Utility import convertTckrToYahooFmt, convertTckrToUniversalFormat


def test_convertTckrToYahooFmt():
    # S&P 500 tckrs
    for x in ["SpX", ".SPx", "^sPX", "$SPy", "ixSPX", "IxGSpC", ".GSpC", "$GsPC"]:
        assert convertTckrToYahooFmt(x) == "^GSPC"
        assert convertTckrToYahooFmt(x.lower()) == "^GSPC"
        assert convertTckrToYahooFmt(x.upper()) == "^GSPC"
    # VIX tickers
    for x in ["ViX", ".vIX", "^VIx", "$Vix", "ixVIX"]:
        assert convertTckrToYahooFmt(x) == "^VIX"
        assert convertTckrToYahooFmt(x.lower()) == "^VIX"
        assert convertTckrToYahooFmt(x.upper()) == "^VIX"
    for x in ["VxV", ".vXV", "^VXv", "$vXv", "ixVXV"]:
        assert convertTckrToYahooFmt(x) == "^VXV"
        assert convertTckrToYahooFmt(x.lower()) == "^VXV"
        assert convertTckrToYahooFmt(x.upper()) == "^VXV"
    for x in ["Vxo", ".vXO", "^VxO", "$VXo", "ixVXO"]:
        assert convertTckrToYahooFmt(x) == "^VXO"
        assert convertTckrToYahooFmt(x.lower()) == "^VXO"
        assert convertTckrToYahooFmt(x.upper()) == "^VXO"
    # other tickers
    for x in ["AgG", "SPy", "BoNd", "TxN"]:
        assert convertTckrToYahooFmt(x) == x.upper()
        assert convertTckrToYahooFmt(x.lower()) == x.upper()
        assert convertTckrToYahooFmt(x.upper()) == x.upper()


def test_convertTckrToUniversalFormat():
    assert convertTckrToUniversalFormat("SPY") == "SPY"
    assert convertTckrToUniversalFormat("/SPY") == "SPY"
    assert convertTckrToUniversalFormat("^SPY") == "ixSPY"
    assert convertTckrToUniversalFormat("$SPY") == "ixSPY"
