"""
-------------------------------------------------------------------------------
Name:        test_project_data_to_dates.py
Project:     MarketAnalysis
Module:      MarketAnalysis
Purpose:

Author:      Cory Perkins

Created:     1/28/20, 9:36 AM
Modified:    1/28/20, 9:36 AM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import pytest
from src.Utility import project_data_to_dates

@pytest.fixture(scope='module')
def price_index():
    return pd.DatetimeIndex(
        [
            "2019-09-03",
            "2019-09-04",
            "2019-09-05",
            "2019-09-06",
            "2019-09-09",
            "2019-09-10",
            "2019-09-11",
            "2019-09-12",
            "2019-09-13",
            "2019-09-16",
            "2019-09-17",
            "2019-09-18",
            "2019-09-19",
            "2019-09-20",
            "2019-09-23",
            "2019-09-24",
            "2019-09-25",
            "2019-09-26",
            "2019-09-27",
            "2019-09-30",
            "2019-10-01",
            "2019-10-02",
            "2019-10-03",
            "2019-10-04",
            "2019-10-07",
            "2019-10-08",
            "2019-10-09",
            "2019-10-10",
            "2019-10-11",
            "2019-10-14",
            "2019-10-15",
            "2019-10-16",
            "2019-10-17",
            "2019-10-18",
            "2019-10-21",
            "2019-10-22",
            "2019-10-23",
            "2019-10-24",
            "2019-10-25",
            "2019-10-28",
            "2019-10-29",
            "2019-10-30",
            "2019-10-31",
            "2019-11-01",
            "2019-11-04",
            "2019-11-05",
            "2019-11-06",
            "2019-11-07",
            "2019-11-08",
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15",
            "2019-11-18",
            "2019-11-19",
            "2019-11-20",
            "2019-11-21",
            "2019-11-22",
            "2019-11-25",
            "2019-11-26",
            "2019-11-27",
            "2019-11-29",
            "2019-12-02",
            "2019-12-03",
            "2019-12-04",
            "2019-12-05",
            "2019-12-06",
            "2019-12-09",
            "2019-12-10",
        ],
        dtype="datetime64[ns]",
        name="date",
        freq=None,
    )


@pytest.fixture(scope='module')
def adj_close_data(price_index):
    return pd.Series(
        np.array(
            [
                2906.27002,
                2937.780029,
                2976.0,
                2978.709961,
                2978.429932,
                2979.389893,
                3000.929932,
                3009.570068,
                3007.389893,
                2997.959961,
                3005.699951,
                3006.72998,
                3006.790039,
                2992.070068,
                2991.780029,
                2966.600098,
                2984.870117,
                2977.620117,
                2961.790039,
                2976.73999,
                2940.25,
                2887.610107,
                2910.629883,
                2952.01001,
                2938.790039,
                2893.060059,
                2919.399902,
                2938.129883,
                2970.27002,
                2966.149902,
                2995.679932,
                2989.689941,
                2997.949951,
                2986.199951,
                3006.719971,
                2995.98999,
                3004.52002,
                3010.290039,
                3022.550049,
                3039.419922,
                3036.889893,
                3046.77002,
                3037.560059,
                3066.909912,
                3078.27002,
                3074.620117,
                3076.780029,
                3085.179932,
                3093.080078,
                3087.01001,
                3091.840088,
                3094.040039,
                3096.629883,
                3120.459961,
                3122.030029,
                3120.179932,
                3108.459961,
                3103.540039,
                3110.290039,
                3133.639893,
                3140.52002,
                3153.629883,
                3140.97998,
                3113.870117,
                3093.199951,
                3112.76001,
                3117.429932,
                3145.909912,
                3135.959961,
                3132.52002,
            ]
        ),
        index=price_index,
        name="adj_close",
    )


@pytest.fixture(scope='module')
def close_data(price_index):
    return pd.Series(
        np.array(
            [
                2906.27002,
                2937.780029,
                2976.0,
                2978.709961,
                2978.429932,
                2979.389893,
                3000.929932,
                3009.570068,
                3007.389893,
                2997.959961,
                3005.699951,
                3006.72998,
                3006.790039,
                2992.070068,
                2991.780029,
                2966.600098,
                2984.870117,
                2977.620117,
                2961.790039,
                2976.73999,
                2940.25,
                2887.610107,
                2910.629883,
                2952.01001,
                2938.790039,
                2893.060059,
                2919.399902,
                2938.129883,
                2970.27002,
                2966.149902,
                2995.679932,
                2989.689941,
                2997.949951,
                2986.199951,
                3006.719971,
                2995.98999,
                3004.52002,
                3010.290039,
                3022.550049,
                3039.419922,
                3036.889893,
                3046.77002,
                3037.560059,
                3066.909912,
                3078.27002,
                3074.620117,
                3076.780029,
                3085.179932,
                3093.080078,
                3087.01001,
                3091.840088,
                3094.040039,
                3096.629883,
                3120.459961,
                3122.030029,
                3120.179932,
                3108.459961,
                3103.540039,
                3110.290039,
                3133.639893,
                3140.52002,
                3153.629883,
                3140.97998,
                3113.870117,
                3093.199951,
                3112.76001,
                3117.429932,
                3145.909912,
                3135.959961,
                3132.52002,
            ]
        ),
        index=price_index,
        name="close",
    )


@pytest.fixture(scope='module')
def series_data(adj_close_data):
    """Wrapping function for readability"""
    return adj_close_data


@pytest.fixture(scope='module')
def df_data(price_index, adj_close_data, close_data):
    """Dataframe data for testing"""
    df = pd.concat([adj_close_data, close_data], axis=1, ignore_index=False)
    df['tckr'] = 'SPX'
    return df[['tckr', 'adj_close', 'close']]


@pytest.fixture(scope='module')
def missing_date(price_index):
    t_delta = pd.Timedelta(days=1)
    test_date = price_index.min() + t_delta
    while test_date in price_index:
        test_date += t_delta
    assert test_date not in price_index
    return test_date


@pytest.fixture(scope='module')
def existing_lower_date(price_index, missing_date):
    ts = price_index[price_index < missing_date].max()
    assert ts in price_index and ts < missing_date
    return ts


@pytest.fixture(scope='module')
def existing_higher_date(price_index, missing_date):
    ts = price_index[price_index > missing_date].min()
    assert ts in price_index and ts > missing_date
    return ts


class TestProjectDataToDates:
    def test_series_data_and_single_dates_return_scalar_values(self, series_data, existing_lower_date):
        assert project_data_to_dates(series_data, existing_lower_date) == series_data[existing_lower_date]

    def test_series_data_and_single_missing_date_return_value_according_to_method(self, series_data, missing_date, existing_lower_date, existing_higher_date):
        assert project_data_to_dates(series_data, missing_date, method='ffill') == series_data[existing_lower_date]
        assert project_data_to_dates(series_data, missing_date, method='bfill') == series_data[existing_higher_date]

    def test_dataframe_data_and_single_dates_return_row_of_values(self, df_data, existing_lower_date):
        assert df_data.loc[existing_lower_date,:].equals(project_data_to_dates(df_data, existing_lower_date))

    def test_dataframe_data_and_single_missing_date_return_row_of_values_according_to_method(self, df_data, missing_date, existing_lower_date, existing_higher_date):
        assert df_data.loc[existing_lower_date,:].equals(project_data_to_dates(df_data, missing_date, method='ffill'))
        assert df_data.loc[existing_higher_date,:].equals(project_data_to_dates(df_data, missing_date, method='bfill'))

    def test_series_data_and_multiple_dates_return_series_with_value_for_each_date(self, series_data, missing_date, existing_lower_date, existing_higher_date):
        probe_index = pd.DatetimeIndex([existing_lower_date, missing_date,existing_higher_date], name='date')
        expect_series = series_data[pd.DatetimeIndex([existing_lower_date, existing_lower_date, existing_higher_date])]
        expect_series.index = probe_index
        assert expect_series.equals(project_data_to_dates(series_data, probe_index, method='ffill'))
        expect_series = series_data[pd.DatetimeIndex([existing_lower_date, existing_higher_date, existing_higher_date])]
        expect_series.index = probe_index
        assert expect_series.equals(project_data_to_dates(series_data, probe_index, method='bfill'))

    def test_dataframe_data_and_multiple_dates_return_rows_with_value_for_each_date(self, df_data, missing_date, existing_lower_date, existing_higher_date):
        probe_index = pd.DatetimeIndex([existing_lower_date, missing_date,existing_higher_date])
        expect_df = df_data.loc[pd.DatetimeIndex([existing_lower_date, existing_lower_date, existing_higher_date]),:]
        expect_df.index = probe_index
        assert expect_df.equals(project_data_to_dates(df_data, probe_index, method='ffill'))
        expect_df = df_data.loc[pd.DatetimeIndex([existing_lower_date, existing_higher_date, existing_higher_date]),:]
        expect_df.index = probe_index
        assert expect_df.equals(project_data_to_dates(df_data, probe_index, method='bfill'))


if __name__ == '__main__':
    print('PyTest TestProjectDataToDates')
