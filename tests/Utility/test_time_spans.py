# noinspection PyUnresolvedReferences
import pytest

# noinspection PyUnresolvedReferences
from .. import context
from src.Utility import month2int


def test_month2int():
    codex = {
        "jan": 1,
        "feb": 2,
        "mar": 3,
        "apr": 4,
        "may": 5,
        "jun": 6,
        "jul": 7,
        "aug": 8,
        "sep": 9,
        "oct": 10,
        "nov": 11,
        "dec": 12,
    }
    for m in codex.keys():
        assert month2int(m) == codex[m]
