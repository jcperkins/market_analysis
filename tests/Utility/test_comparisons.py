#!/usr/bin/env python
# noinspection PyUnresolvedReferences
import pytest
import numpy as np
import pandas as pd

# noinspection PyUnresolvedReferences
from .. import context
from src.Utility import (
    is_same,
    assert_same,
    is_iterable_equal,
    assert_iterable_equal,
    is_equal,
    assert_equal,
    is_subset,
    assert_subset,
)


def test_is_same():
    l1 = [1, 2]
    l2 = l1
    assert is_same(l1, l2)
    assert not is_same([1, 2], [1, 2])


def test_assert_same():
    l1 = [1, 2]
    l2 = l1
    assert assert_same(l1, l2)
    with pytest.raises(AssertionError):
        assert_same([1, 2], [1, 2])


def test_equality_basics():
    # tests for basic types
    assert is_equal("a", "a")
    assert assert_equal("a", "a")
    assert not is_equal("a", "b")
    with pytest.raises(AssertionError):
        assert_equal("a", "b")
    assert not is_equal(1, "b")
    with pytest.raises(AssertionError):
        assert_equal(1, "b")
    assert is_equal(1, 1)
    assert assert_equal(1, 1)
    assert is_equal(1, 1.0)
    assert assert_equal(1, 1.0)
    assert not is_equal(1, 2)
    with pytest.raises(AssertionError):
        assert_equal(1, 2)
    assert is_equal(1.1, 1.1)
    assert assert_equal(1.1, 1.1)
    assert not is_equal(1.1, 1.2)
    with pytest.raises(AssertionError):
        assert_equal(1.1, 1.2)
    assert is_equal(np.nan, np.nan)
    assert assert_equal(np.nan, np.nan)
    assert not is_equal(np.nan, 1)
    with pytest.raises(AssertionError):
        assert_equal(np.nan, 1)


def test_equality_dict():
    # test dict cases
    assert is_equal({}, {})
    assert assert_equal({}, {})

    assert not is_equal({"a": "a"}, {"a": "a", "b": "b"})
    with pytest.raises(AssertionError):
        assert_equal({"a": "a"}, {"a": "a", "b": "b"})

    assert not is_equal({"a": "a", "b": "b"}, {"a": "a"})
    with pytest.raises(AssertionError):
        assert_equal({"a": "a", "b": "b"}, {"a": "a"})

    assert is_equal({"a": "a", "b": "b"}, {"a": "a", "b": "b"})
    assert assert_equal({"a": "a", "b": "b"}, {"a": "a", "b": "b"})

    assert not is_equal({"a": 1, "b": "b"}, {"a": "a", "b": "b"})
    with pytest.raises(AssertionError):
        assert_equal({"a": 1, "b": "b"}, {"a": "a", "b": "b"})

    assert is_equal({"a": [1, 2, 3], "b": "b"}, {"a": [1, 2, 3], "b": "b"})
    assert assert_equal({"a": [1, 2, 3], "b": "b"}, {"a": [1, 2, 3], "b": "b"})

    assert not is_equal({"a": [1, 2, [3, 4, 5]], "b": "b"}, {"a": [1, 2, 3], "b": "b"})
    with pytest.raises(AssertionError):
        assert_equal({"a": [1, 2, [3, 4, 5]], "b": "b"}, {"a": [1, 2, 3], "b": "b"})

    assert is_equal(
        {"a": [1, {"c": "c"}, [3, 4, [5, 6, 7]]], "b": {"b2": "b"}},
        {"a": [1, {"c": "c"}, [3, 4, [5, 6, 7]]], "b": {"b2": "b"}},
    )
    assert assert_equal(
        {"a": [1, {"c": "c"}, [3, 4, [5, 6, 7]]], "b": {"b2": "b"}},
        {"a": [1, {"c": "c"}, [3, 4, [5, 6, 7]]], "b": {"b2": "b"}},
    )


def test_equality_iterables():
    # tests for iterables - test 'is_iterable_equal fully' and then test token cases for 'is_equal'
    # note a set doesn't have order so the value of order_matters is mute if one or both are sets
    a, b = [], []
    assert is_iterable_equal(a, b, order_matters=True)
    assert is_iterable_equal(set(a), set(b), order_matters=True)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert is_iterable_equal(a, b, order_matters=False)
    assert is_iterable_equal(set(a), set(b), order_matters=False)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    assert assert_iterable_equal(a, b, order_matters=True)
    assert assert_iterable_equal(set(a), set(b), order_matters=True)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=False)
    assert assert_iterable_equal(set(a), set(b), order_matters=False)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1], []
    assert not is_iterable_equal(a, b, order_matters=True)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert not is_iterable_equal(a, b, order_matters=False)
    assert not is_iterable_equal(set(a), set(b), order_matters=False)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=False)
    with pytest.raises(AssertionError):
        assert_iterable_equal(set(a), set(b), order_matters=False)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1, 2, 3], [1, 2, 3]
    assert is_iterable_equal(a, b, order_matters=True)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert is_iterable_equal(a, b, order_matters=False)
    assert is_iterable_equal(set(a), set(b), order_matters=False)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    assert assert_iterable_equal(a, b, order_matters=True)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=False)
    assert assert_iterable_equal(set(a), set(b), order_matters=False)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1, 2, 3], [1, 3, 2]
    assert not is_iterable_equal(a, b, order_matters=True)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert is_iterable_equal(a, b, order_matters=False)
    assert is_iterable_equal(set(a), set(b), order_matters=False)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=False)
    assert assert_iterable_equal(set(a), set(b), order_matters=False)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1, 2, 3], [1, 2, 4]
    assert not is_iterable_equal(a, b, order_matters=True)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert not is_iterable_equal(a, b, order_matters=False)
    assert not is_iterable_equal(set(a), set(b), order_matters=False)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=False)
    with pytest.raises(AssertionError):
        assert_iterable_equal(set(a), set(b), order_matters=False)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1, {"c": "c"}, [3, 4, [5, 6, 7]]], [1, {"c": "c"}, [3, 4, [5, 6, 7]]]
    assert is_iterable_equal(a, b, order_matters=True)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert is_iterable_equal(a, b, order_matters=False)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    assert assert_iterable_equal(a, b, order_matters=True)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=False)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    a, b = [1, {"c": "c"}, [3, 4, [5, 6, 7]]], [1, [3, 4, [5, 6, 7]], {"c": "c"}]
    assert not is_iterable_equal(a, b, order_matters=True)
    assert not is_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert is_iterable_equal(a, b, order_matters=False)
    assert is_iterable_equal(tuple(a), tuple(b), order_matters=False)

    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(tuple(a), tuple(b), order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=False)
    assert assert_iterable_equal(tuple(a), tuple(b), order_matters=False)

    # test simple is_equal/assert_equal cases
    assert is_equal([1, 2, 3], [1, 2, 3], order_matters=True)
    assert not is_equal([1, 2, 3], [1, 3, 2], order_matters=True)
    assert not is_equal([1, 2, 3], [1, 2, 4], order_matters=True)
    assert is_equal([1, 2, 3], [1, 2, 3], order_matters=False)
    assert is_equal([1, 2, 3], [1, 3, 2], order_matters=False)
    assert not is_equal([1, 2, 3], [1, 2, 4], order_matters=False)

    assert assert_equal([1, 2, 3], [1, 2, 3], order_matters=True)
    with pytest.raises(AssertionError):
        assert_equal([1, 2, 3], [1, 3, 2], order_matters=True)
    with pytest.raises(AssertionError):
        assert_equal([1, 2, 3], [1, 2, 4], order_matters=True)
    assert assert_equal([1, 2, 3], [1, 2, 3], order_matters=False)
    assert assert_equal([1, 2, 3], [1, 3, 2], order_matters=False)
    with pytest.raises(AssertionError):
        assert_equal([1, 2, 3], [1, 2, 4], order_matters=False)

    # test for NaN values
    a, b = [1, 2, 3, np.nan], [1, 2, 3, np.nan]
    assert is_iterable_equal(a, b, order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=True)
    a, b = [1, 2, np.nan, 3], [1, 2, 3, np.nan]
    assert not is_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    a, b = [1, 2, 3, np.nan], [1, 2, 3]
    assert not is_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    a, b = [1, 2, 3, np.nan], [1, 2, 3, pd.NaT]
    assert not is_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    a, b = [1, 2, 3, pd.NaT], [1, 2, 3, pd.NaT]
    assert is_iterable_equal(a, b, order_matters=True)
    assert assert_iterable_equal(a, b, order_matters=True)
    a, b = [1, 2, pd.NaT, 3], [1, 2, 3, pd.NaT]
    assert not is_iterable_equal(a, b, order_matters=True)
    with pytest.raises(AssertionError):
        assert_iterable_equal(a, b, order_matters=True)
    return


def test_equality_array_index_1():
    const_now = pd.Timestamp.now()
    # array and pd.index values
    a, b = ([], [])
    assert is_equal(np.array(a), np.array(b))
    assert is_equal(pd.Index(a), pd.Index(b))
    assert assert_equal(np.array(a), np.array(b))
    assert assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))

    a, b = [1, 2, 3], []
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))

    a, b = [1, 2, 3], [1, 2, 3]
    assert is_equal(np.array(a), np.array(b))
    assert is_equal(pd.Index(a), pd.Index(b))
    assert assert_equal(np.array(a), np.array(b))
    assert assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))

    a, b = [1, 2, 3], [1, 3, 2]
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))

    a, b = [1, 2, 3], [1, 2, 4]
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))

    a, b = ["a", "b", "c"], ["a", "b", "c"]
    assert is_equal(np.array(a), np.array(b))
    assert is_equal(pd.Index(a), pd.Index(b))
    assert assert_equal(np.array(a), np.array(b))
    assert assert_equal(pd.Index(a), pd.Index(b))

    a, b = ["a", "c", "b"], ["a", "b", "c"]
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))

    a, b = ["a", "b", "c"], ["a", "b", "d"]
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))

    a = [[1, 2, 3], [4, 5, 6]]
    b = [[1, 2, 3], [4, 5, 6]]
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(np.array(a), np.array(b))
    a = [[1, 2, 3], [4, 5, 6]]
    b = [[1, 2, 3], [4, 5, 1000006]]
    assert not is_equal(np.array(a), np.array(b))  # diff values
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    a = [[1, 2], [3, 4], [5, 6]]
    b = [[1, 2, 3], [4, 5, 6]]
    assert not is_equal(np.array(a), np.array(b))  # diff shape
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))

    # now consider NaN and NaT values
    a, b = [1, 2, 3, np.nan], [1, 2, 3, np.nan]
    assert is_equal(np.array(a), np.array(b))
    assert is_equal(pd.Index(a), pd.Index(b))
    assert assert_equal(np.array(a), np.array(b))
    assert assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert is_equal(a, b)
    assert is_equal(np.array(a), np.array(b))
    assert assert_equal(a, b)
    assert assert_equal(np.array(a), np.array(b))

    a, b = [1, 2, np.nan, 3], [1, 2, 3, np.nan]
    assert not is_equal(np.array(a), np.array(b))
    assert not is_equal(pd.Index(a), pd.Index(b))
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(pd.Index(a), pd.Index(b))
    a, b = (pd.TimedeltaIndex(a, unit="day"), pd.TimedeltaIndex(b, unit="day"))
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))
    a, b = (a + const_now, b + const_now)
    assert not is_equal(a, b)
    assert not is_equal(np.array(a), np.array(b))
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(np.array(a), np.array(b))

    return


def test_equality_series():
    # empty
    a = pd.Series([], name="name1")
    b = pd.Series([], name="name1")
    assert is_equal(a, b)
    assert assert_equal(a, b)

    # same numeric same index
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    assert is_equal(a, b)
    assert assert_equal(a, b)

    # same numeric, different index
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([1, 2, 3], index=[4, 5, 6], name="name1")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)

    # different numeric same index
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([4, 5, 6], index=[1, 2, 3], name="name1")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)

    # different numeric order same index
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([1, 3, 2], index=[1, 2, 3], name="name1")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)

    # different length
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([1, 2], index=[1, 2], name="name1")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)

    # same alpha same index
    a = pd.Series(["a", "b", "c"], index=[1, 2, 3], name="name1")
    b = pd.Series(["a", "b", "c"], index=[1, 2, 3], name="name1")
    assert is_equal(a, b)
    assert assert_equal(a, b)

    # different alpha same index
    a = pd.Series(["a", "b", "c"], index=[1, 2, 3], name="name1")
    b = pd.Series(["d", "e", "f"], index=[1, 2, 3], name="name1")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)

    # different name
    a = pd.Series([1, 2, 3], index=[1, 2, 3], name="name1")
    b = pd.Series([1, 2, 3], index=[1, 2, 3], name="name2")
    assert not is_equal(a, b)
    with pytest.raises(AssertionError):
        assert_equal(a, b)
    return


def test_equality_dataframe():
    num1 = [1, 2, 3]
    num2 = [1, 3, 2]  # diff order
    num3 = [1, 2, 9]  # diff values
    alpha1 = ["a", "b", "c"]
    alpha2 = ["a", "c", "b"]  # diff order
    alpha3 = ["a", "b", "z"]  # diff values
    dur1 = pd.TimedeltaIndex(num1, unit="day")
    dur2 = pd.TimedeltaIndex(num2, unit="day")  # diff order
    dur3 = pd.TimedeltaIndex(num3, unit="day")  # diff values
    ts1 = dur1 + pd.Timestamp.now()

    # empty DF object with variety of column names
    df1 = pd.DataFrame(columns=alpha1)
    df2 = pd.DataFrame(columns=alpha2)
    df3 = pd.DataFrame(columns=alpha3)
    assert is_equal(df1, df1.copy(), order_matters=True)
    assert is_equal(df1, df1.copy(), order_matters=False)
    assert assert_equal(df1, df1.copy(), order_matters=True)
    assert assert_equal(df1, df1.copy(), order_matters=False)
    assert not is_equal(df1, df2, order_matters=True)
    with pytest.raises(AssertionError):
        assert_equal(df1, df2, order_matters=True)
    assert is_equal(df1, df2, order_matters=False)
    assert assert_equal(df1, df2, order_matters=False)
    assert not is_equal(df1, df3, order_matters=True)
    with pytest.raises(AssertionError):
        assert_equal(df1, df3, order_matters=True)
    assert not is_equal(df1, df3, order_matters=False)
    with pytest.raises(AssertionError):
        assert_equal(df1, df3, order_matters=False)

    # variety of data with reordered columns
    df1 = pd.concat(
        [
            pd.Series(num1, index=num1, name="col1"),
            pd.Series(alpha1, index=num2, name="col2"),
            pd.Series(dur1, index=num3, name="col3"),
            pd.Series(ts1, index=num1, name="col4"),
        ],
        axis=1,
    )
    assert is_equal(df1, df1.copy())
    assert assert_equal(df1, df1.copy())
    df2 = df1.copy().loc[:, ["col4", "col3", "col2", "col1"]]
    assert not is_equal(df1, df2, order_matters=True)
    with pytest.raises(AssertionError):
        assert_equal(df1, df2, order_matters=True)
    assert is_equal(df1, df2, order_matters=False)
    assert assert_equal(df1, df2, order_matters=False)
    # a couple of more complicated DataFrame configurations with alt indices
    num_ind = df1.index.copy()

    alt_indices = {
            "num": pd.Index(num_ind),
            "alpha": pd.Index(["a", "b", "c", "d"]),
            "dur": pd.TimedeltaIndex(num_ind, unit="day"),
            "ts": pd.DatetimeIndex(pd.TimedeltaIndex(num_ind, unit="day") + pd.Timestamp.now()),
        }
    df2 = df1.copy()
    for df1_col in alt_indices.keys():
        df1.index = alt_indices[df1_col]
        for df2_col in alt_indices.keys():
            df2.index = alt_indices[df2_col]
            if df1_col == df2_col:
                assert is_equal(df1, df2)
                assert assert_equal(df1, df2)
            else:
                assert not is_equal(df1, df2)
                with pytest.raises(AssertionError):
                    assert_equal(df1, df2)
    return


def test_subset():
    a = [1, 2, 3]
    b = [1, 2, 3, 4]
    c = [1, 2, 4]
    d = [1, 2, 3, np.NaN]
    e = [1, 2, 3, pd.NaT]
    assert is_subset(a, list(a))
    assert assert_subset(a, list(a))
    assert is_subset(a, b)
    assert assert_subset(a, b)
    assert not is_subset(b, a)
    with pytest.raises(AssertionError):
        assert_subset(b, a)
    assert not is_subset(a, c)
    with pytest.raises(AssertionError):
        assert_subset(a, c)
    assert is_subset(a, d)
    assert assert_subset(a, d)
    assert not is_subset(d, a)
    with pytest.raises(AssertionError):
        assert_subset(d, a)
    assert is_subset(d, list(d))
    assert assert_subset(d, list(d))
    assert not is_subset(d, b)
    with pytest.raises(AssertionError):
        assert_subset(d, b)
    return
