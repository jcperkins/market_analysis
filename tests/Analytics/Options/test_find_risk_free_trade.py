"""
-------------------------------------------------------------------------------
Name:        test_find_risk_free_trade.py
Project:     MarketAnalysis_Dev
Module:      MarketAnalysis_Dev
Purpose:

Author:      Cory Perkins

Created:     1/28/20, 5:31 PM
Modified:    1/28/20, 5:31 PM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import pytest
from src.Analytics.Options import find_risk_free_trade
from collections import Counter

priority_columns = [
    "quote_ts",
    "symbol",
    "tckr",
    "stock_price",
    "expiration",
    "option_type",
    "strike",
    "ask",
    "bid",
]
risk_free_trade_columns = [
    "tckr",
    "quote_ts",
    "expiration",
    "option_type",
    "stock_price",
    "sell_symbol",
    "sell_strike",
    "sell_price",
    "buy_symbol",
    "buy_strike",
    "buy_price",
    "max_loss",
    "max_profit",
    "initial_investment",
]
tckr = "SPY"
stock_price = 289.230011
quote_ts = pd.Timestamp("2019-04-18 09:18:37.213470")
expiration = pd.Timestamp("2019-07-19")
base_df = pd.DataFrame(columns=priority_columns)
base_df["strike"] = [280, 282, 284, 286, 288, 290, 292, 294, 296, 298]
base_df["quote_ts"] = quote_ts
base_df["stock_price"] = stock_price
base_df["expiration"] = expiration
base_df["tckr"] = "SPY"


@pytest.fixture(scope="module")
def call_df():
    df = base_df.copy()
    df["ask"] = [14.05, 12.5, 11.0, 9.5, 8.1, 6.8, 5.65, 4.55, 3.6, 2.8]
    df["bid"] = [13.95, 12.35, 10.85, 9.4, 8.05, 6.75, 5.55, 4.5, 3.55, 2.75]
    df["symbol"] = [f"-SPY190719C{int(x)}" for x in df.strike]
    df["option_type"] = "Call"
    assert np.all(pd.notna(df))
    return df


@pytest.fixture(scope="module")
def put_df():
    df = base_df.copy()
    df["ask"] = [4.2, 4.65, 5.15, 5.7, 6.35, 7.10, 8.0, 8.95, 10.10, 11.3]
    df["bid"] = [4.1, 4.6, 5.1, 5.65, 6.3, 7.05, 7.9, 8.85, 9.9, 11.15]
    df["symbol"] = [f"-SPY190719P{int(x)}" for x in df.strike]
    df["option_type"] = "Put"
    assert np.all(pd.notna(df))
    return df


@pytest.fixture(scope="module")
def all_df(call_df, put_df):
    return pd.concat([call_df, put_df], axis=0, ignore_index=True)


@pytest.fixture(scope="module")
def wide_spread_df(all_df):
    """
    The Protective collar equation is so sensitive that I need to increase the spread across
    the board to prevent risk-free trades naturally occurring in call_df and put_df.

    Add $0.60 to the spread of each option... increase each ask price and decrease each bid by $0.30
    """
    wide_df = all_df.copy()
    wide_df["bid"] = wide_df.bid - 0.30
    wide_df["ask"] = wide_df.ask + 0.30
    return wide_df


@pytest.fixture(scope="module")
def zero_risk_bullish_call_spread_records(call_df):
    """
    I want 2 records to verify if there is a case where multiple records are found
    One should have the premium exactly equal 0 and the other go a little into the
    positive

    294/292 - premium = -$1.15 less than required to get to risk free
    * subtract 0.58 from the bid and ask of the 296 strike
    * add 0.57 to the bid and ask of the 298 strike
    298/296 - premium = -$0.85 adapt each side by $0.45 to to a positive max_loss
    * subtract 0.45 from the bid and ask of the 296 strike
    * add 0.45 to the bid and ask of the 298 strike
    """
    df = (
        call_df.loc[call_df.strike.isin([292, 294, 296, 298]), :]
        .copy()
        .set_index("strike", inplace=False)
    )
    df.loc[292, ["bid", "ask"]] = df.loc[292, ["bid", "ask"]] + np.array([-0.58, -0.58])
    df.loc[294, ["bid", "ask"]] = df.loc[294, ["bid", "ask"]] + np.array([+0.57, +0.57])
    df.loc[296, ["bid", "ask"]] = df.loc[296, ["bid", "ask"]] + np.array([-0.45, -0.45])
    df.loc[298, ["bid", "ask"]] = df.loc[298, ["bid", "ask"]] + np.array([+0.45, +0.45])
    assert np.all(pd.notna(df))
    assert (df.loc[298, "bid"] - df.loc[296, "ask"]) > 0
    assert (df.loc[294, "bid"] - df.loc[292, "ask"]) == 0
    return df.reset_index(drop=False, inplace=False)


@pytest.fixture(scope="module")
def trades_bullish_call(zero_risk_bullish_call_spread_records) -> pd.DataFrame:
    zr_df = zero_risk_bullish_call_spread_records.copy()
    zr_df.index = pd.Index(zr_df.strike.values)
    x_sell = 294
    x_buy = 292
    record1 = pd.Series(
        {
            "option_type": "bullish_vertical_call_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    x_sell = 298
    x_buy = 296
    record2 = pd.Series(
        {
            "option_type": "bullish_vertical_call_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    df = pd.concat([record1, record2], axis=1).T
    df["tckr"] = tckr
    df["quote_ts"] = quote_ts
    df["expiration"] = expiration
    premium = df.sell_price - df.buy_price
    x_delta = np.abs(df.sell_strike - df.buy_strike)
    df["max_loss"] = -1 * premium
    df["max_profit"] = premium + x_delta
    df["initial_investment"] = -1 * premium
    return df[risk_free_trade_columns]


@pytest.fixture(scope="module")
def zero_risk_bearish_call_spread_records(call_df):
    """
    I want 2 records to verify if there is a case where multiple records are found
    One should have the max loss exactly equal 0 and the other go a little into the
    positive

    280/282 - max loss = -$0.55
    * add 0.28 from the bid and ask of the 296 strike
    * subtract 0.27 to the bid and ask of the 298 strike
    284/286 - max loss = -$0.65 adapt each side by $0.35 to to a positive max_loss
    * add 0.35 from the bid and ask of the 284 strike
    * subtract 0.35 to the bid and ask of the 286 strike
    """
    df = (
        call_df.loc[call_df.strike.isin([280, 282, 284, 286]), :]
        .copy()
        .set_index("strike", inplace=False)
    )
    df.loc[280, ["bid", "ask"]] = df.loc[280, ["bid", "ask"]] + np.array([+0.30, +0.30])
    df.loc[282, ["bid", "ask"]] = df.loc[282, ["bid", "ask"]] + np.array([-0.30, -0.30])
    df.loc[284, ["bid", "ask"]] = df.loc[284, ["bid", "ask"]] + np.array([+0.33, +0.33])
    df.loc[286, ["bid", "ask"]] = df.loc[286, ["bid", "ask"]] + np.array([-0.32, -0.32])
    assert np.all(pd.notna(df))
    assert ((df.loc[280, "bid"] - df.loc[282, "ask"]) - (282 - 280)) > 0
    assert ((df.loc[284, "bid"] - df.loc[286, "ask"]) - (286 - 284)) == 0
    return df.reset_index(drop=False, inplace=False)


@pytest.fixture(scope="module")
def trades_bearish_call(zero_risk_bearish_call_spread_records) -> pd.DataFrame:
    zr_df = zero_risk_bearish_call_spread_records.copy()
    zr_df.index = pd.Index(zr_df.strike.values)
    x_sell = 280
    x_buy = 282
    record1 = pd.Series(
        {
            "option_type": "bearish_vertical_call_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    x_sell = 284
    x_buy = 286
    record2 = pd.Series(
        {
            "option_type": "bearish_vertical_call_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    df = pd.concat([record1, record2], axis=1).T
    df["tckr"] = tckr
    df["quote_ts"] = quote_ts
    df["expiration"] = expiration
    premium = df.sell_price - df.buy_price
    x_delta = np.abs(df.sell_strike - df.buy_strike)
    df["max_loss"] = -1 * (premium - x_delta)
    df["max_profit"] = premium
    df["initial_investment"] = -1 * premium
    return df[risk_free_trade_columns]


@pytest.fixture(scope="module")
def zero_risk_bullish_put_spread_records(put_df):
    """
    I want 2 records to verify if there is a case where multiple records are found
    One should have the premium exactly equal 0 and the other go a little into the
    positive

    292/294 - max loss = -$1.15
    * subtract $0.58 from the bid and ask of the 292 strike
    * add $0.57 to the bid and ask of the 294 strike
    296/298 - premium = -$0.95
    * subtract $0.50 from the bid and ask of the 296 strike
    * add $0.50 to the bid and ask of the 298 strike
    """
    df = (
        put_df.loc[put_df.strike.isin([292, 294, 296, 298]), :]
        .copy()
        .set_index("strike", inplace=False)
    )
    df.loc[292, ["bid", "ask"]] = df.loc[292, ["bid", "ask"]] + np.array([-0.58, -0.58])
    df.loc[294, ["bid", "ask"]] = df.loc[294, ["bid", "ask"]] + np.array([+0.57, +0.57])
    df.loc[296, ["bid", "ask"]] = df.loc[296, ["bid", "ask"]] + np.array([-0.50, -0.50])
    df.loc[298, ["bid", "ask"]] = df.loc[298, ["bid", "ask"]] + np.array([+0.50, +0.50])
    assert np.all(pd.notna(df))
    assert ((df.loc[294, "bid"] - df.loc[292, "ask"]) - (294 - 292)) == 0
    assert ((df.loc[298, "bid"] - df.loc[296, "ask"]) - (298 - 296)) > 0
    return df.reset_index(drop=False, inplace=False)


@pytest.fixture(scope="module")
def trades_bullish_put(zero_risk_bullish_put_spread_records) -> pd.DataFrame:
    zr_df = zero_risk_bullish_put_spread_records.copy()
    zr_df.index = pd.Index(zr_df.strike.values)
    x_sell = 294
    x_buy = 292
    record1 = pd.Series(
        {
            "option_type": "bullish_vertical_put_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    x_sell = 298
    x_buy = 296
    record2 = pd.Series(
        {
            "option_type": "bullish_vertical_put_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    df = pd.concat([record1, record2], axis=1).T
    df["tckr"] = tckr
    df["quote_ts"] = quote_ts
    df["expiration"] = expiration
    premium = df.sell_price - df.buy_price
    x_delta = np.abs(df.sell_strike - df.buy_strike)
    df["max_loss"] = -1 * (premium - x_delta)
    df["max_profit"] = premium
    df["initial_investment"] = -1 * premium
    return df[risk_free_trade_columns]


@pytest.fixture(scope="module")
def zero_risk_bearish_put_spread_records(put_df):
    """
    I want 2 records to verify if there is a case where multiple records are found
    One should have the premium exactly equal 0 and the other go a little into the
    positive

    280/282 - max loss = -$0.55
    * add 0.30 from the bid and ask of the 296 strike
    * subtract 0.30 to the bid and ask of the 298 strike
    284/286 - max loss = -$0.60 adapt each side by $0.35 to to a positive max_loss
    * add 0.31 from the bid and ask of the 284 strike
    * subtract 0.30 to the bid and ask of the 286 strike
    """
    df = (
        put_df.loc[put_df.strike.isin([280, 282, 284, 286]), :]
        .copy()
        .set_index("strike", inplace=False)
    )
    df.loc[280, ["bid", "ask"]] = df.loc[280, ["bid", "ask"]] + np.array([+0.30, +0.30])
    df.loc[282, ["bid", "ask"]] = df.loc[282, ["bid", "ask"]] + np.array([-0.30, -0.30])
    df.loc[284, ["bid", "ask"]] = df.loc[284, ["bid", "ask"]] + np.array([+0.31, +0.31])
    df.loc[286, ["bid", "ask"]] = df.loc[286, ["bid", "ask"]] + np.array([-0.30, -0.30])
    assert np.all(pd.notna(df))
    assert (df.loc[280, "bid"] - df.loc[282, "ask"]) > 0
    assert (df.loc[284, "bid"] - df.loc[286, "ask"]) > 0
    return df.reset_index(drop=False, inplace=False)


@pytest.fixture(scope="module")
def trades_bearish_put(zero_risk_bearish_put_spread_records) -> pd.DataFrame:
    zr_df = zero_risk_bearish_put_spread_records.copy()
    zr_df.index = pd.Index(zr_df.strike.values)
    x_sell = 280
    x_buy = 282
    record1 = pd.Series(
        {
            "option_type": "bearish_vertical_put_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    x_sell = 284
    x_buy = 286
    record2 = pd.Series(
        {
            "option_type": "bearish_vertical_put_spread",
            "stock_price": stock_price,
            "sell_symbol": zr_df.loc[x_sell, "symbol"],
            "sell_strike": zr_df.loc[x_sell, "strike"],
            "sell_price": zr_df.loc[x_sell, "bid"],
            "buy_symbol": zr_df.loc[x_buy, "symbol"],
            "buy_strike": zr_df.loc[x_buy, "strike"],
            "buy_price": zr_df.loc[x_buy, "ask"],
        }
    )
    df = pd.concat([record1, record2], axis=1).T
    df["tckr"] = tckr
    df["quote_ts"] = quote_ts
    df["expiration"] = expiration
    premium = df.sell_price - df.buy_price
    x_delta = np.abs(df.sell_strike - df.buy_strike)
    df["max_loss"] = -1 * premium
    df["max_profit"] = premium + x_delta
    df["initial_investment"] = -1 * premium
    return df[risk_free_trade_columns]


@pytest.fixture(scope="module")
def zero_risk_collar_records(all_df) -> pd.DataFrame:
    """
    The only records that are currently risk free are 10 collars where the ask
    and the put are equal [280, 282, 284, 286, 288, 290, 292, 294, 296, 298]. The
    remaining 6 conditions need sample cases to test:
    1. xp < stock_price < xc
      * xc = 290, xp = 288; max_loss = -0.830011
    2. stock_price < xp < xc
      * xc = 298, xp = 296; max_loss = -0.580011
    3. xp < xc < stock_price
      * xc = 286, xp = 284; max_loss = -0.980011
    4. xc < stock_price < xp
      * xc = 288, xp = 290; max_loss = -0.280011
    5. stock_price < xc < xp
      * Xc = 292; xp = 294; max_loss = -0.630011
    6. xc < xp < stock_price - ALREADY EXISTS
      * Xc = 280; xp = 282; max_loss = 0.019989
    """
    records = [
        {"i_call": ("Call", 290), "i_put": ("Put", 288), "gap": 0.84},
        {"i_call": ("Call", 298), "i_put": ("Put", 296), "gap": 0.60},
        {"i_call": ("Call", 286), "i_put": ("Put", 284), "gap": 1.00},
        {"i_call": ("Call", 288), "i_put": ("Put", 290), "gap": 0.30},
        {"i_call": ("Call", 292), "i_put": ("Put", 294), "gap": 0.64},
    ]
    keep_idx = [r["i_call"] for r in records] + [r["i_put"] for r in records]
    df = (
        all_df.copy()
        .set_index(["option_type", "strike"], inplace=False)
        .loc[keep_idx, :]
    )
    for r in records:
        # add half the gap to the call prices and subtract half the gap from the put prices
        call_index = r["i_call"]
        put_index = r["i_put"]
        delta = r["gap"] / 2
        df.loc[call_index, ["bid", "ask"]] = df.loc[
            call_index, ["bid", "ask"]
        ] + np.array([delta, delta])
        df.loc[put_index, ["bid", "ask"]] = df.loc[
            put_index, ["bid", "ask"]
        ] - np.array([delta, delta])

    return df.reset_index(drop=False, inplace=False)[priority_columns]


@pytest.fixture(scope="module")
def trades_collar(all_df, zero_risk_collar_records) -> pd.DataFrame:
    """
    The expected result for key records that combine for zero risk protective collar trades
    1. xp < stock_price < xc
      * xc = 290, xp = 288
    2. stock_price < xp < xc
      * xc = 298, xp = 296
    3. xp < xc < stock_price
      * xc = 286, xp = 284
    4. xc < stock_price < xp
      * xc = 288, xp = 290
    5. stock_price < xc < xp
      * Xc = 292; xp = 294
    6. xc < xp < stock_price
      * Xc = 280; xp = 282
    7. xc = xp < stock_price
      * xc = 280; xp = 280
    8. stock_price < xc = xp
      * xc = 296 xp = 296
    """
    records = [
        ("-SPY190719C290", "-SPY190719P288"),
        ("-SPY190719C298", "-SPY190719P296"),
        ("-SPY190719C286", "-SPY190719P284"),
        ("-SPY190719C288", "-SPY190719P290"),
        ("-SPY190719C292", "-SPY190719P294"),
        ("-SPY190719C280", "-SPY190719P282"),
        ("-SPY190719C280", "-SPY190719P280"),
        ("-SPY190719C296", "-SPY190719P296"),
    ]
    zr_df = (
        update_option_records(all_df, zero_risk_collar_records)
        .copy()
        .set_index("symbol", inplace=False)
    )
    row_list = []
    for call_symbol, put_symbol in records:
        row_list.append(
            pd.Series(
                {
                    "option_type": "protective_collar",
                    "stock_price": stock_price,
                    "sell_symbol": call_symbol,
                    "sell_strike": zr_df.loc[call_symbol, "strike"],
                    "sell_price": zr_df.loc[call_symbol, "bid"],
                    "buy_symbol": put_symbol,
                    "buy_strike": zr_df.loc[put_symbol, "strike"],
                    "buy_price": zr_df.loc[put_symbol, "ask"],
                }
            )
        )
    df = pd.concat(row_list, axis=1).T
    df["tckr"] = tckr
    df["quote_ts"] = quote_ts
    df["expiration"] = expiration
    premium = df.sell_price - df.buy_price
    put_at_xp = 0
    call_at_xp = np.minimum(df.sell_strike - df.buy_strike, 0)
    stock_at_xp = df.buy_strike - df.stock_price
    df["max_loss"] = premium + put_at_xp + call_at_xp + stock_at_xp
    put_at_xc = np.maximum(df.buy_strike - df.sell_strike, 0)
    call_at_xc = 0
    stock_at_xc = df.sell_strike - df.stock_price
    df["max_profit"] = premium + put_at_xc + call_at_xc + stock_at_xc
    df["initial_investment"] = df.stock_price - premium
    return df[risk_free_trade_columns]


def update_option_records(main_data: pd.DataFrame, *args) -> pd.DataFrame:
    """Updates the options in main_data dataframe with the new records that follow main_data"""
    assert "symbol" in main_data.columns
    df = main_data.copy().set_index("symbol")
    for new_records in args:
        assert "symbol" in new_records.columns
        df.update(new_records.set_index("symbol"), overwrite=True)
    return df.reset_index(drop=False, inplace=False)


class TestFindRiskFreeTrade:
    def test_no_risk_free_returns_none(self, wide_spread_df):
        assert find_risk_free_trade(wide_spread_df) is None

    def test_bullish_call_spread(
        self, all_df, zero_risk_bullish_call_spread_records, trades_bullish_call
    ):
        df = update_option_records(all_df, zero_risk_bullish_call_spread_records)
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_bullish_call)
        # test if trades_bullish_call is a subset of result inner merge with no key or index will only keep
        # rows that are the same for all matching columns
        assert len(result.merge(trades_bullish_call, how="inner")) == len(
            trades_bullish_call
        )

    def test_bearish_call_spread(
        self, all_df, zero_risk_bearish_call_spread_records, trades_bearish_call
    ):
        df = update_option_records(all_df, zero_risk_bearish_call_spread_records)
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_bearish_call)
        # test if trades_bullish_call is a subset of result inner merge with no key or index will only keep
        # rows that are the same for all matching columns
        assert len(result.merge(trades_bearish_call, how="inner")) == len(
            trades_bearish_call
        )

    def test_bullish_put_spread(
        self, all_df, zero_risk_bullish_put_spread_records, trades_bullish_put
    ):
        df = update_option_records(all_df, zero_risk_bullish_put_spread_records)
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_bullish_put)
        # test if trades_bullish_call is a subset of result inner merge with no key or index will only keep
        # rows that are the same for all matching columns
        assert len(result.merge(trades_bullish_put, how="inner")) == len(
            trades_bullish_put
        )

    def test_bearish_put_spread(
        self, all_df, zero_risk_bearish_put_spread_records, trades_bearish_put
    ):
        df = update_option_records(all_df, zero_risk_bearish_put_spread_records)
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_bearish_put)
        # test if trades_bullish_call is a subset of result inner merge with no key or index will only keep
        # rows that are the same for all matching columns
        assert len(result.merge(trades_bearish_put, how="inner")) == len(
            trades_bearish_put
        )

    def test_all_zero_risk_spreads(
        self,
        all_df,
        zero_risk_bullish_call_spread_records,
        trades_bullish_call,
        zero_risk_bearish_call_spread_records,
        trades_bearish_call,
        zero_risk_bullish_put_spread_records,
        trades_bullish_put,
        zero_risk_bearish_put_spread_records,
        trades_bearish_put,
    ):
        df = update_option_records(
            all_df,
            zero_risk_bullish_call_spread_records,
            zero_risk_bearish_call_spread_records,
            zero_risk_bullish_put_spread_records,
            zero_risk_bearish_put_spread_records,
        )
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_bearish_put)
        # test if trades_bullish_call is a subset of result inner merge with no key or index will only keep
        # rows that are the same for all matching columns
        assert len(result.merge(trades_bullish_call, how="inner")) == len(
            trades_bullish_call
        )
        assert len(result.merge(trades_bearish_call, how="inner")) == len(
            trades_bearish_call
        )
        assert len(result.merge(trades_bullish_put, how="inner")) == len(
            trades_bullish_put
        )
        assert len(result.merge(trades_bearish_put, how="inner")) == len(
            trades_bearish_put
        )

    def test_all_zero_risk_collars(
        self, all_df, zero_risk_collar_records, trades_collar
    ):
        df = update_option_records(all_df, zero_risk_collar_records)
        result = find_risk_free_trade(df)
        assert Counter(result.columns) == Counter(trades_collar)
        string_columns = ["tckr", "option_type", "sell_symbol", "buy_symbol"]
        datetime_columns = ["quote_ts", "expiration"]
        numeric_columns = [
            c
            for c in risk_free_trade_columns
            if c not in string_columns and c not in datetime_columns
        ]
        result = result.set_index(string_columns, inplace=False, drop=True)
        expected = trades_collar.set_index(string_columns, inplace=False, drop=True)
        # test if all of the expected trade label info is contained in result
        assert np.all(expected.index.isin(result.index))
        # test the numeric columns
        assert np.all(
            np.isclose(
                result.loc[expected.index, numeric_columns].values.astype(float),
                expected.loc[expected.index, numeric_columns].values.astype(float),
            )
        )


if __name__ == "__main__":
    print("PyTest TestFindRiskFreeTrade")
