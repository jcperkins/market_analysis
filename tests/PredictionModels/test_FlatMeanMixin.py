"""
-------------------------------------------------------------------------------
Name:        test_FlatMeanMixin.py
Project:     MarketAnalysis
Module:      MarketAnalysis
Purpose:

Author:      Cory Perkins

Created:     1/28/20, 10:36 AM
Modified:    1/28/20, 10:36 AM
Copyright:   (c) Cory Perkins 2020
Licence:     MIT
-------------------------------------------------------------------------------
"""
import pandas as pd
import pytest
from src.Model import FlatPrediction30DayVol


@pytest.fixture(scope="module")
def m(adj_close_data):
    """the prediction module to use for the tests"""
    return FlatPrediction30DayVol().initialize("SPX", adj_close_data)


@pytest.fixture(scope="module")
def missing_date(price_index):
    t_delta = pd.Timedelta(days=1)
    test_date = price_index.min() + t_delta
    while test_date in price_index:
        test_date += t_delta
    assert test_date not in price_index
    return test_date


@pytest.fixture(scope="module")
def existing_lower_date(price_index, missing_date):
    ts = price_index[price_index < missing_date].max()
    assert ts in price_index and ts < missing_date
    return ts


@pytest.fixture(scope="module")
def existing_higher_date(price_index, missing_date):
    ts = price_index[price_index > missing_date].min()
    assert ts in price_index and ts > missing_date
    return ts


class TestPredictMean:
    @pytest.mark.parametrize("td_days", [3, 30, 180])
    def test_single_existing_date_returns_price_of_day_for_all_time_deltas(
        self, m, price_index, adj_close_data, td_days
    ):
        existing_date = pd.Timestamp(price_index[2])
        expected_price = adj_close_data[existing_date]
        assert expected_price == m.predict_mean(
            existing_date, pd.Timedelta(td_days, unit="days")
        )

    @pytest.mark.parametrize("td_days", [3, 30, 180])
    def test_single_missing_date_returns_previous_price(
        self, m, missing_date, existing_lower_date, td_days
    ):
        assert m.data[existing_lower_date] == m.predict_mean(
            missing_date, pd.Timedelta(td_days, unit="days")
        )

    @pytest.mark.parametrize("td_days", [3, 30, 180])
    def test_multiple_dates_return_price_for_each_and_named_for_each_time_delta(
        self, m, missing_date, existing_lower_date, existing_higher_date, td_days
    ):
        probe_idx = pd.DatetimeIndex(
            [existing_lower_date, missing_date, existing_higher_date], name="date"
        )
        expected_series = m.data[
            [existing_lower_date, existing_lower_date, existing_higher_date]
        ]
        expected_series.index = probe_idx
        expected_series.name = f"{td_days}-Day"
        assert expected_series.equals(
            m.predict_mean(probe_idx, pd.Timedelta(td_days, unit="days"))
        )


class TestPopulateMeanDF:
    """tests the basic populate method on the API class"""
    def test_return_df_with_column_for_each_time_delta(self, m, price_index):
        probe_dates = price_index[:5]
        days_out = pd.TimedeltaIndex([3, 7, 30], unit="days").sort_values()
        df = m.populate_mean_df(probe_dates, days_out=days_out)
        for td in days_out:
            expected_col = m.predict_mean(probe_dates, td)
            assert expected_col.name in df.columns
            assert expected_col.equals(df[expected_col.name])

    def test_return_df_with_column_for_each_int_in_list(self, m, price_index):
        probe_dates = price_index[:5]
        days_out = pd.TimedeltaIndex([3, 7, 30], unit="days").sort_values()
        days_out_int = list(days_out.days)
        df = m.populate_mean_df(probe_dates, days_out=days_out_int)
        for td in days_out:
            expected_col = m.predict_mean(probe_dates, td)
            assert expected_col.name in df.columns
            assert expected_col.equals(df[expected_col.name])

    def test_days_out_is_none_returns_column_for_7_14_30_60_180_365_730_1095_1825_days(self, m, price_index):
        probe_dates = price_index[:5]
        days_out = pd.TimedeltaIndex([7, 14, 30, 60, 180, 365, 730, 1095, 1825], unit="days")
        days_out_int = list(days_out.days)
        df = m.populate_mean_df(probe_dates)
        for td in days_out:
            expected_col = m.predict_mean(probe_dates, td)
            assert expected_col.name in df.columns
            assert expected_col.equals(df[expected_col.name])
