from src.Model import FlatPrediction30DayVol
import pandas as pd
import numpy as np


class TestInitialization:
    def test_adj_price_loads(self, adj_close_data):
        m = FlatPrediction30DayVol()
        m.initialize('SPX', adj_close=adj_close_data)
        assert isinstance(m.data, pd.Series)
        assert m.data.name == 'price'
        assert np.all(m.data.values == adj_close_data.values)
        assert m.data is not adj_close_data

    def test_final_price_normalizes_data(self, adj_close_data):
        final_close = 1
        m = FlatPrediction30DayVol()
        m.initialize('SPX', adj_close=adj_close_data, final_close=final_close)
        assert isinstance(m.data, pd.Series)
        assert m.data.name == 'price'
        assert m.data[m.data.index.max()] == 1
        assert np.allclose(m.data.values, adj_close_data.values / adj_close_data[adj_close_data.index.max()], rtol=0.001)
        assert m.data is not adj_close_data
