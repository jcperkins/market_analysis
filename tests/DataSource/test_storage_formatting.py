import pytest
import os
import numpy as np
import pandas as pd

# noinspection PyUnresolvedReferences
from .. import context
from src.DataSource.Storage_Format import (
    df_has_types,
    storage_has_types,
    push_dtypes_df,
)


@pytest.fixture
def simple_df() -> pd.DataFrame:
    """
    creates a simple pandas dataframe with a column 'a' that is of
    type np.object_ and a column 'b' that is of type np.int32
    """
    return pd.DataFrame(
        {"a": np.array(["str1", "str2"], dtype=str), "b": np.array([1, 2], dtype=int)}
    )


@pytest.fixture
def simple_storage(simple_df) -> pd.HDFStore:
    """
    creates a simple pd.HDFStore storage object that contains
    the simple dataframe under the label 'simple'
    """
    # use a temp directory in the home directory
    temp_path = os.path.abspath("./temp/")
    if not os.path.exists(temp_path):
        os.mkdir(temp_path)

    # create the tempStorage object and put the simple DF there
    tempStorage = pd.HDFStore(os.path.join(temp_path, "simple_storage_obj.h5"))
    tempStorage.open()
    tempStorage.put("simple", simple_df, format="table", append=False)
    tempStorage.close()
    return tempStorage


class TestDataSourceFormatting(object):
    def test_df_format_check(self, simple_df):
        # test DF format checking
        assert df_has_types(simple_df, {"a": np.object_, "b": np.int32}) is True
        assert df_has_types(simple_df, {"a": np.object_, "b": np.int64}) is False
        assert df_has_types(simple_df, {"a": str, "b": np.int32}) is False
        assert df_has_types(simple_df, {"a": np.object_, "c": np.int32}) is False

    def test_storage_format_check(self, simple_storage):
        assert (
            storage_has_types(
                simple_storage, "simple", {"a": np.object_, "b": np.int32}
            )
            is True
        )

        assert (
            storage_has_types(
                simple_storage, "simple", {"a": np.object_, "b": np.int64}
            )
            is False
        )
        assert (
            storage_has_types(simple_storage, "simple", {"a": str, "b": np.int32})
            is False
        )
        assert (
            storage_has_types(
                simple_storage, "simple", {"a": np.object_, "c": np.int32}
            )
            is False
        )

    def test_df_format_push(self, simple_df):
        assert df_has_types(simple_df, {"a": np.object_, "b": np.int32}) is True
        new_types = {"a": np.object_, "b": np.int32}
        push_dtypes_df(simple_df, new_types)
        assert df_has_types(simple_df, new_types) is True
        new_types = {"a": np.object_, "b": np.float64}
        push_dtypes_df(simple_df, new_types)
        assert df_has_types(simple_df, new_types) is True
