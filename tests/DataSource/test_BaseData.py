# noinspection PyUnresolvedReferences
import os
from typing import Dict, List, Optional, Tuple, Type, Union, cast

import numpy as np
import pandas as pd
import pytest

# noinspection PyUnresolvedReferences
from .. import context
import src.Utility as Util
from src.DataSource import BaseData
from src.DataSource.Mixins import BaseStorageDBMixin, BaseTempDBMixin
from src.types import RawDateCollection, RawDateSingle, RawDateType, TorList


class TestBaseData(object):
    def test_init(self, abs_temp_dir, stock_storageDB):
        temp_obj = BaseData("LIBORData", fileDir=abs_temp_dir)
        assert isinstance(temp_obj, BaseData)
        assert isinstance(temp_obj, BaseStorageDBMixin)
        assert isinstance(temp_obj, BaseTempDBMixin)
        assert temp_obj.storageName == "LIBORData"
        assert temp_obj.mainDir == abs_temp_dir
        assert temp_obj.tempDB is None
        assert temp_obj.simulationStartDate == pd.Timestamp(1990, 1, 1)
        assert isinstance(temp_obj.lastHistoricalDate, pd.Timestamp)

        # test the copy function for the new properties

        # first change the temp object in a couple of key places
        # base data properites
        temp_obj.storageName = "Copied_Name"
        temp_obj.default_key = "Copied_Key"
        temp_obj.lastHistoricalDate = temp_obj.lastHistoricalDate - pd.Timedelta(7, "D")

        # base storage properties
        temp_obj.storageCols = ["copy_col1", "copy_col2"]
        temp_obj.keyList = ["copy_key1", "copy_key2"]
        temp_obj.data_types = {"copy_col1": float, "copy_col2": float}

        # base tempdb properties
        temp_obj.tempdb_cols = ["copy_col1", "copy_col2"]
        temp_obj.tempDB = pd.DataFrame(columns=temp_obj.tempdb_cols)

        # copy and test
        copy_obj = BaseData(temp_obj)
        assert isinstance(temp_obj, BaseData)
        assert isinstance(temp_obj, BaseStorageDBMixin)
        assert isinstance(temp_obj, BaseTempDBMixin)
        assert copy_obj.storageName == "Copied_Name"
        assert copy_obj.default_key == "Copied_Key"
        assert copy_obj.lastHistoricalDate == temp_obj.lastHistoricalDate
        # the HDFStore object is only object that can be shared
        assert Util.is_same(copy_obj.storage, temp_obj.storage)
        assert not Util.is_same(copy_obj.storageCols, temp_obj.storageCols)
        Util.assert_equal(copy_obj.storageCols, temp_obj.storageCols)

        assert not Util.is_same(copy_obj.keyList, temp_obj.keyList)
        Util.assert_equal(copy_obj.keyList, temp_obj.keyList)

        assert not Util.is_same(copy_obj.data_types, temp_obj.data_types)
        Util.assert_equal(copy_obj.data_types, temp_obj.data_types)

        assert not Util.is_same(copy_obj.tempdb_cols, temp_obj.tempdb_cols)
        Util.assert_equal(copy_obj.tempdb_cols, temp_obj.tempdb_cols)

        assert not Util.is_same(copy_obj.tempDB, temp_obj.tempDB)
        Util.assert_equal(copy_obj.tempDB, temp_obj.tempDB)

        return

    def test_copy(self, abs_temp_dir, stock_storageDB):
        temp_obj = BaseData("LIBORData", fileDir=abs_temp_dir)
        # first change the temp object in a couple of key places
        # base data properites
        temp_obj.storageName = "Copied_Name"
        temp_obj.default_key = "Copied_Key"
        temp_obj.lastHistoricalDate = temp_obj.lastHistoricalDate - pd.Timedelta(7, "D")

        # base storage properties
        temp_obj.storageCols = ["copy_col1", "copy_col2"]
        temp_obj.keyList = ["copy_key1", "copy_key2"]
        temp_obj.data_types = {"copy_col1": float, "copy_col2": float}

        # base tempdb properties
        temp_obj.tempdb_cols = ["copy_col1", "copy_col2"]
        temp_obj.tempDB = pd.DataFrame(columns=temp_obj.tempdb_cols)

        # copy and test
        copy_obj = BaseData(temp_obj)
        assert isinstance(temp_obj, BaseData)
        assert isinstance(temp_obj, BaseStorageDBMixin)
        assert isinstance(temp_obj, BaseTempDBMixin)
        assert copy_obj.storageName == "Copied_Name"
        assert copy_obj.default_key == "Copied_Key"
        assert copy_obj.lastHistoricalDate == temp_obj.lastHistoricalDate
        # the HDFStore object is only object that can be shared
        assert Util.is_same(copy_obj.storage, temp_obj.storage)
        assert not Util.is_same(copy_obj.storageCols, temp_obj.storageCols)
        Util.assert_equal(copy_obj.storageCols, temp_obj.storageCols)
        assert not Util.is_same(copy_obj.keyList, temp_obj.keyList)
        Util.assert_equal(copy_obj.keyList, temp_obj.keyList)
        assert not Util.is_same(copy_obj.data_types, temp_obj.data_types)
        Util.assert_equal(copy_obj.data_types, temp_obj.data_types)
        assert not Util.is_same(copy_obj.tempdb_cols, temp_obj.tempdb_cols)
        Util.assert_equal(copy_obj.tempdb_cols, temp_obj.tempdb_cols)
        assert not Util.is_same(copy_obj.tempDB, temp_obj.tempDB)
        Util.assert_equal(copy_obj.tempDB, temp_obj.tempDB)
        return

    def test_populateTempDB(self, abs_temp_dir, libor_tempDB):
        baseObj = BaseData("LIBORData", fileDir=abs_temp_dir)
        assert baseObj.tempDB is None
        # start with most common use (covers 90% of use) and add to it later
        Util.assert_same(baseObj, baseObj.populateTempDB())
        Util.assert_equal(libor_tempDB.tempDB, baseObj.tempDB)
        return

    def test_loadFromDB(self, abs_temp_dir: str, libor_data: pd.DataFrame):
        # test the various formatting conditions
        base_obj = BaseData("LIBORData", fileDir=abs_temp_dir)
        base_obj.populateTempDB()
        start_ref: pd.Timestamp = libor_data.index[5]
        end_ref: pd.Timestamp = libor_data.index[10]
        index_span: pd.DatetimeIndex = libor_data.index[5:10]

        # if start is None, returns np.nan
        assert pd.isnull(base_obj.loadFromDB(start=None, source="storage"))
        assert pd.isnull(base_obj.loadFromDB(start=pd.NaT, source="storage"))
        assert pd.isnull(base_obj.loadFromDB(start=None, source="temp_db"))
        assert pd.isnull(base_obj.loadFromDB(start=pd.NaT, source="temp_db"))

        # for remaining tests use the following conditions which have format
        #    (start_arg, end_arg, columns_arg, res_type, res_index, res_columns)
        conditions: List[
            Tuple[
                Optional[Union[pd.TimeStamp, pd.DatetimeIndex]],
                Optional[pd.TimeStamp],
                Optional[TorList[str]],
                type,
                Union[pd.Timestamp, pd.DatetimeIndex],
                TorList[str],
            ]
        ] = [
            # returns scalar if start is a single date, end is none and columns is a string
            (start_ref, None, "1d", float, start_ref, "1d"),
            # returns series if start is a single date, end is none and columns is a list
            (start_ref, None, ["1d"], pd.Series, start_ref, ["1d"]),
            # if columns is a a string and start and end are single dates returns scalar if start == end
            (start_ref, start_ref, "1d", float, start_ref, "1d"),
            # if columns is a a string and start and end are single dates returns scalar if start != end
            (start_ref, end_ref, "1d", pd.Series, index_span, "1d"),
            # returns series if start is datetimeIndex and column is a string (regardless of end)
            (
                pd.DatetimeIndex([start_ref]),
                end_ref,
                "1d",
                pd.Series,
                pd.DatetimeIndex([start_ref]),
                "1d",
            ),
            # returns dataframe if start is datetimeIndex and column is a list or None (regardless of end)
            (index_span, None, ["1d"], pd.DataFrame, index_span, ["1d"]),
            (index_span, end_ref, None, pd.DataFrame, index_span, libor_data.columns),
        ]

        for t_cond in conditions:
            (start_arg, end_arg, columns_arg, res_type, res_index, res_columns) = t_cond
            print(res_type)
            result = libor_data.loc[res_index, res_columns]
            assert isinstance(result, res_type)
            Util.assert_equal(
                result,
                base_obj.loadFromDB(
                    start=start_arg, end=end_arg, columns=columns_arg, source="storage"
                ),
            )
            Util.assert_equal(
                result,
                base_obj.loadFromDB(
                    start=start_arg, end=end_arg, columns=columns_arg, source="temp_db"
                ),
            )
        return
