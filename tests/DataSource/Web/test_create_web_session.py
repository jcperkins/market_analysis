import pytest
from requests import Session
from src.DataSource.Web import create_web_session


def test_create_web_session():
    assert isinstance(create_web_session(), Session) is True
    assert isinstance(create_web_session({"https": "proxy_url"}), Session) is True
