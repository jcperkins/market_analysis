import pytest
from config import default_settings
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from src.DataSource.Web.selenium_driver_utility import get_driver, close_driver


@pytest.mark.slow
def get_status(driver):
    try:
        driver.get_window_size()
        return "Alive"
    except WebDriverException:
        return "Dead"


@pytest.mark.slow
def test_selenium_utility_functions():
    for kw in [{}, {"debug": True}, {"driver_path": default_settings.chromedriver}]:
        d = get_driver(**kw)
        assert isinstance(d, webdriver.Chrome)
        assert get_status(d) == "Alive"
        close_driver(d)
        assert get_status(d) == "Dead"
