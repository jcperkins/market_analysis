import numpy as np
import pandas as pd

# noinspection PyProtectedMember
from src.DataSource.Web.get_yahoo_stock_price_data import (
    _format_yahoo_split_factor,
    _format_stock_dataframe,
)


def test_format_yahoo_split_factor():
    res = _format_yahoo_split_factor("1/1")
    assert type(res) == float
    assert res == 1

    assert _format_yahoo_split_factor("1/2") == 0.5
    assert _format_yahoo_split_factor("10/1") == 10.0


def test_format_stock_dataframe():
    dates = pd.date_range(start="2019-01-01", periods=3, freq="D", name="date")
    raw_df = pd.concat(
        [
            pd.Series(dates),
            pd.Series([np.NaN, 2.0, 3.0], name="float_col", dtype=float),
            pd.Series([1, 2, 3], name="int_col", dtype=int),
            pd.Series(["1", "2", "null"], name="str_col", dtype=str),
        ],
        axis=1,
    ).set_index("date")
    expected = pd.concat(
        [
            pd.Series(dates),
            pd.Series([np.NaN, 2.0, 3.0], name="float_col", dtype=float),
            pd.Series([1.0, 2.0, 3.0], name="int_col", dtype=float),
            pd.Series([1.0, 2.0, 2.0], name="str_col", dtype=float),
        ],
        axis=1,
    ).set_index("date")

    assert expected.equals(_format_stock_dataframe(raw_df))
