import datetime
from pathlib import Path

import pytest
from sqlalchemy import Column, String, Integer, DateTime, Float
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from src.DataSource.sql_db.Common import condition_exists, get_or_create, scrub

# create a sandbox environment for the Common functions to test in
base_common = declarative_base()


class BasicData(base_common):
    """ An basic model for testing the common functions of the ORM database """

    __tablename__ = "basic_data"

    # columns
    id = Column(Integer, primary_key=True)
    string_col = Column(String, nullable=False)
    datetime_col = Column(DateTime, nullable=False)
    float_col = Column(Float, nullable=False)


@pytest.fixture()
def raw1():
    return {
        "string_col": "a",
        "datetime_col": datetime.datetime(2019, 1, 1),
        "float_col": 1.0,
    }


@pytest.fixture()
def session_common(tmpdir) -> Session:
    # set the global engine and _SessionFactory objects
    sql_engine = create_engine(
        "sqlite:///" + str(Path(tmpdir).joinpath("db_table_access_testing.sqlite")),
        echo=False,
    )
    # use session_factory() to get a new Session
    _SessionFactory = sessionmaker(bind=sql_engine)
    base_common.metadata.create_all(sql_engine)
    return _SessionFactory()


def test_table_access(session_common, raw1):
    assert (
        condition_exists(
            session_common,
            BasicData,
            string_col=raw1["string_col"],
            datetime_col=raw1["datetime_col"],
            float_col=raw1["float_col"],
        )
        is False
    )
    row1, bool_added = get_or_create(session_common, BasicData, **raw1)
    assert bool_added is True
    session_common.commit()
    assert isinstance(row1, BasicData)
    assert row1.id == 1
    assert row1.string_col == raw1["string_col"]
    assert row1.datetime_col == raw1["datetime_col"]
    assert row1.float_col == raw1["float_col"]

    assert (
        condition_exists(
            session_common,
            BasicData,
            string_col=raw1["string_col"],
            datetime_col=raw1["datetime_col"],
            float_col=raw1["float_col"],
        )
        is True
    )

    row1_retrieved, bool_added = get_or_create(
        session_common,
        BasicData,
        string_col=raw1["string_col"],
        datetime_col=raw1["datetime_col"],
        float_col=raw1["float_col"],
    )
    assert bool_added is False

    assert isinstance(row1_retrieved, BasicData)
    assert row1_retrieved.id == 1
    assert row1_retrieved.string_col == raw1["string_col"]
    assert row1_retrieved.datetime_col == raw1["datetime_col"]
    assert row1_retrieved.float_col == raw1["float_col"]


def test_scrub():
    assert scrub("singleword") == "singleword"
    assert scrub("multi_word_table") == "multi_word_table"
    assert scrub("); drop tables --") == "droptables"
