from src.DataSource.sql_db.Models.orm_factory import (
    orm_equal,
    orm_to_dict,
    orm_from_dict,
    orm_to_series,
    orm_from_series,
    orm_list_from_df,
    orm_list_to_df,
)
import pandas as pd
import numpy as np
import src.Utility as Util


class TestOrmFactoryEquality:
    def test_different_classes_return_false(
        self, person_model, city_model, raw_people, raw_cities
    ):
        assert (
            orm_equal(person_model(**raw_people[0]), city_model(**raw_cities[0]))
            is False
        )

    def test_same_object_return_true(self, person_model, raw_people):
        o1 = person_model(**raw_people[0])
        assert orm_equal(o1, o1)

    def test_different_object_same_data_return_true(self, person_model, raw_people):
        assert orm_equal(person_model(**raw_people[0]), person_model(**raw_people[0]))

    def test_different_data_return_false(self, person_model, raw_people):
        assert (
            orm_equal(person_model(**raw_people[2]), person_model(**raw_people[1]))
            is False
        )


class TestOrmFactoryConversions:
    def test_dictionary_conversions(self, person_model, raw_people):
        raw = raw_people[0]
        print(raw)
        orm = orm_from_dict(person_model, raw)
        print(orm)
        print(person_model.__table__.columns.keys())
        assert isinstance(orm, person_model)
        for k, v in raw.items():
            assert v == getattr(orm, k)

        d = orm_to_dict(orm)
        print(d)
        assert isinstance(d, dict)
        for k, v in raw.items():
            assert v == d[k]

    def test_series_conversion(self, person_model, raw_people):
        raw_s = pd.Series(raw_people[0])
        orm = orm_from_series(person_model, raw_s)
        assert isinstance(orm, person_model)
        for k in raw_s.index:
            assert raw_s[k] == getattr(orm, k)

        s = orm_to_series(orm)
        assert isinstance(s, pd.Series)
        for k in raw_s.index:
            assert raw_s[k] == s[k]

    def test_dataframe_to_orm_list(self, person_model, raw_people):
        df = pd.DataFrame(raw_people)
        assert df.shape == (len(raw_people), 7)
        orm_list = orm_list_from_df(person_model, df)
        assert len(orm_list) == len(raw_people)
        for i in df.index:
            obj = orm_list[i]
            assert isinstance(obj, person_model)
            test_cols = raw_people[i].keys()
            for c in test_cols:
                x1 = df.loc[i, c]
                x2 = getattr(obj, c)
                assert x1 == x2 or (pd.isna(x1) and pd.isna(x2))

    def test_dataframe_from_orm_list(self, person_model, raw_people):
        orm_list = [person_model(**p) for p in raw_people]
        assert len(orm_list) == len(raw_people)
        df = orm_list_to_df(orm_list)
        # extra column in df because id is included but not populated
        assert df.shape == (len(raw_people), 8)

        for i, obj in enumerate(orm_list):
            test_cols = raw_people[i].keys()
            for c in test_cols:
                x1 = df.loc[i, c]
                x2 = getattr(obj, c)
                assert x1 == x2 or (pd.isna(x1) and pd.isna(x2))
