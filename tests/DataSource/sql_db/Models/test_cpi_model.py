import pandas as pd
from src.DataSource.sql_db.Models import RateCPI


class TestCPIModel:
    def test_basic_functionality(self, session_models, sql_engine_models, raw_cpi):
        i1 = RateCPI(**raw_cpi[0])
        assert i1.is_in_table(session_models) is False
        session_models.add(i1)
        session_models.commit()
        assert i1.is_in_table(session_models) is True

        df = pd.read_sql("select * from cpi_rates", sql_engine_models)
        print(df)
        assert df.loc[0, "year"] == i1.year
        assert df.loc[0, "month"] == i1.month
        assert df.loc[0, "rate"] == i1.rate

        # now add the rest of them
        for x in raw_cpi:
            cpi_obj = RateCPI(**x)
            if not cpi_obj.is_in_table(session_models):
                session_models.add(cpi_obj)
        session_models.commit()

    def test_stringify(self, session_models):
        obj = session_models.query(RateCPI).get(1)
        obj_str = str(obj)
        assert obj_str.startswith("<RateCPI(")
        assert obj_str.endswith(")>")
        key_properties = ["id", "year", "month", "rate"]
        for k in key_properties:
            assert f"{k}={getattr(obj, k)}" in obj_str

    def test_last_and_previous(self, session_models, raw_cpi):
        cpi = RateCPI.get_last(session_models)
        assert cpi.year == raw_cpi[-1]["year"] and cpi.month == raw_cpi[-1]["month"]

        cpi = RateCPI.get_previous(session_models)
        assert cpi.year == raw_cpi[-1]["year"] and cpi.month == raw_cpi[-1]["month"]
        cpi = RateCPI.get_previous(
            session_models,
            pd.Timestamp(
                year=raw_cpi[-1]["year"], month=raw_cpi[-1]["month"] + 1, day=15
            ),
        )
        assert cpi.year == raw_cpi[-1]["year"] and cpi.month == raw_cpi[-1]["month"]
        cpi = RateCPI.get_previous(
            session_models,
            pd.Timestamp(year=raw_cpi[1]["year"], month=raw_cpi[1]["month"], day=15),
        )
        assert cpi.year == raw_cpi[1]["year"] and cpi.month == raw_cpi[1]["month"]
