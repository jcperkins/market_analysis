import pandas as pd
import numpy as np
from src.DataSource.sql_db.Models import OptionPrice


class TestOptionModel:
    def test_basic_functionality(self, session_models, sql_engine_models, raw_options):
        o1 = OptionPrice(**raw_options[0])
        assert o1.is_in_table(session_models) is False
        session_models.add(o1)
        session_models.commit()
        assert o1.is_in_table(session_models) is True

        df = pd.read_sql(
            "select * from options",
            sql_engine_models,
            parse_dates=["quote_ts", "expiration"],
        )
        assert df.loc[0, "quote_ts"] == o1.quote_ts
        assert df.loc[0, "symbol"] == o1.symbol

        session_models.commit()

    def test_upload_df_empty(self, session_models, raw_options):
        # first try an emtpy dataframe
        df = pd.DataFrame.from_records(raw_options)
        result = OptionPrice.upload_df(df.loc[[], :], session_models)
        assert isinstance(result, pd.Series)
        assert len(result) == 0

    def test_upload_df_all_new(self, session_models, raw_options):
        df = pd.DataFrame.from_records(raw_options)
        result = OptionPrice.upload_df(df.loc[[1, 2], :], session_models, debug=True)
        assert isinstance(result, pd.Series)
        assert len(result) == 2
        # index based selection i.e. it copies the index
        assert result.loc[1]
        assert result.loc[2]
        assert result.sum() == 2

        o2 = session_models.query(OptionPrice).get(2)
        assert df.loc[1, "quote_ts"] == o2.quote_ts
        assert df.loc[1, "symbol"] == o2.symbol

        o3 = session_models.query(OptionPrice).get(3)
        assert df.loc[2, "quote_ts"] == o3.quote_ts
        assert df.loc[2, "symbol"] == o3.symbol

    def test_upload_df_mixed(self, session_models, raw_options):
        df = pd.DataFrame.from_records(raw_options)
        # try all records already in the table (the first 3 rows)
        result = OptionPrice.upload_df(
            df.loc[[0, 1, 2], :], session_models, commit=False, debug=True
        )
        assert isinstance(result, pd.Series)
        assert len(result) == 3
        assert result.sum() == 0
        for idx_df in [0, 1, 2]:
            x = session_models.query(OptionPrice).get(idx_df + 1)
            assert df.loc[idx_df, "quote_ts"] == x.quote_ts
            assert df.loc[idx_df, "symbol"] == x.symbol

        # try a fresh attempt with all new records are new
        result = OptionPrice.upload_df(
            df.loc[[3, 4, 5, 6], :], session_models, commit=True
        )
        assert isinstance(result, pd.Series)
        assert len(result) == 4
        assert result.sum() == 4
        for idx_df in [3, 4, 5, 6]:
            x = session_models.query(OptionPrice).get(idx_df + 1)
            assert df.loc[idx_df, "quote_ts"] == x.quote_ts
            assert df.loc[idx_df, "symbol"] == x.symbol

        # try mixed
        result = OptionPrice.upload_df(df.loc[[5, 6, 7, 8], :], session_models)
        assert isinstance(result, pd.Series)
        assert len(result) == 4
        assert result.sum() == 2
        assert np.all(np.equal(result, [False, False, True, True]))
        assert np.all(np.equal(result.index, [5, 6, 7, 8]))
        for idx_df in [5, 6, 7, 8]:
            x = session_models.query(OptionPrice).get(idx_df + 1)
            assert df.loc[idx_df, "quote_ts"] == x.quote_ts
            assert df.loc[idx_df, "symbol"] == x.symbol

        # now add the rest of them
        result = OptionPrice.upload_df(df, session_models)
        print(result)
        assert len(result) == df.shape[0]
        assert result.sum() == (df.shape[0] - 9)

    def test_stringify(self, session_models):
        obj = session_models.query(OptionPrice).get(1)
        obj_str = str(obj)
        assert obj_str.startswith("<OptionPrice(")
        assert obj_str.endswith(")>")
        key_properties = [
            "id",
            "quote_ts",
            "expiration",
            "days_to_expiration",
            "tckr",
            "stock_price",
            "option_type",
            "strike",
            "symbol",
            "last",
            "change",
            "bid_size",
            "bid",
            "ask",
            "ask_size",
            "volume",
            "open_int",
            "imp_vol",
            "delta",
            "gamma",
            "theta",
            "vega",
            "rho",
        ]
        for k in key_properties:
            assert f"{k}={getattr(obj, k)}" in obj_str
