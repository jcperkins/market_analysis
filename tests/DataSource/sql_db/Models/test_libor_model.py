import pandas as pd
from src.DataSource.sql_db.Models import LIBOR


class TestLIBORModel:
    def test_basic_functionality(self, session_models, sql_engine_models, raw_libor):
        l1 = LIBOR(**raw_libor[0])
        assert l1.is_in_table(session_models) is False
        session_models.add(l1)
        session_models.commit()
        assert l1.is_in_table(session_models) is True

        df = pd.read_sql("select * from libor", sql_engine_models, parse_dates="date")
        print(df)
        assert df.loc[0, "date"].date() == l1.date
        assert df.loc[0, "duration"] == l1.duration
        assert df.loc[0, "rate"] == l1.rate

        # now add the rest of them
        for x in raw_libor:
            libor_obj = LIBOR(**x)
            if not libor_obj.is_in_table(session_models):
                session_models.add(libor_obj)
        session_models.commit()

    def test_get_min_date(self, session_models, raw_libor):
        assert LIBOR.get_min_date(session_models) == pd.Timestamp(raw_libor[0]["date"])

    def test_get_max_date(self, session_models, raw_libor):
        assert LIBOR.get_max_date(session_models) == pd.Timestamp(raw_libor[-1]["date"])

    def test_get_valid_date_none(self, session_models, raw_libor):
        assert LIBOR.get_valid_date(session_models, ts=None) == pd.Timestamp(
            raw_libor[-1]["date"]
        )

    def test_get_valid_date_exact(self, session_models, raw_libor):
        ts = pd.Timestamp(raw_libor[10]["date"])
        assert LIBOR.get_valid_date(session_models, ts=ts) == ts
        # noinspection PyTypeChecker
        assert LIBOR.get_valid_date(session_models, ts=ts.date()) == ts

    def test_get_valid_date_lt(self, session_models, raw_libor):
        ts1 = pd.Timestamp(raw_libor[6]["date"])
        ts2 = pd.Timestamp(raw_libor[7]["date"])
        # verify that the indexes straddle a date change
        assert ts1 < ts2
        ts_middle = ts1 + (ts2 - ts1) / 2
        ts = LIBOR.get_valid_date(session_models, ts=ts_middle)
        assert ts == ts1

    def test_stringify(self, session_models):
        obj = session_models.query(LIBOR).get(1)
        obj_str = str(obj)
        assert obj_str.startswith("<LIBOR(")
        assert obj_str.endswith(")>")
        key_properties = ["id", "date", "duration", "rate"]
        for k in key_properties:
            assert f"{k}={getattr(obj, k)}" in obj_str
