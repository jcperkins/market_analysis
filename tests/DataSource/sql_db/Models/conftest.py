from pathlib import Path
import pytest
from src.DataSource.sql_db import set_engine, session_factory


@pytest.fixture(scope="session")
def session_models(tmpdir_factory):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("db_models_testing.sqlite")
        .resolve()
    )
    print("file path to db used for testing the models: " + str(fname))
    set_engine(fname, echo_parm=False)
    return session_factory()


@pytest.fixture(scope="session")
def sql_engine_models(session_models):
    return session_models.get_bind()
