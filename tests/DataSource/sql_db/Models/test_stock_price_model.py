import pandas as pd
from src.DataSource.sql_db.Models import StockPrice


class TestStockPriceModel:
    def test_initialization(self, raw_stock):
        assert raw_stock is not None
        raw0 = raw_stock[0].copy()
        sp = StockPrice(**raw0)
        assert isinstance(sp, StockPrice)
        assert sp.tckr == raw0["tckr"]
        assert sp.date == raw0["date"]
        assert sp.open == raw0["open"]
        assert sp.high == raw0["high"]
        assert sp.low == raw0["low"]
        assert sp.close == raw0["close"]
        assert sp.adj_close == raw0["adj_close"]
        assert sp.volume == raw0["volume"]
        assert sp.dividends == raw0["dividends"]
        assert sp.split_factor == raw0["split_factor"]

    def test_store_retrieve(self, session_models, raw_stock):
        sp0 = StockPrice(**raw_stock[0])
        assert sp0.is_in_table(session_models) is False
        session_models.add(sp0)
        session_models.commit()
        assert sp0.is_in_table(session_models) is True
        assert sp0.id == 1

        # add more stock prices to
        session_models.add(StockPrice(**raw_stock[1]))
        session_models.add(StockPrice(**raw_stock[2]))
        session_models.add(StockPrice(**raw_stock[3]))
        session_models.add(StockPrice(**raw_stock[4]))

        sp_from_db = session_models.query(StockPrice).get(2)
        assert isinstance(sp_from_db, StockPrice)
        assert sp_from_db.id == 2
        assert sp_from_db.tckr == raw_stock[1]["tckr"]
        assert sp_from_db.date == raw_stock[1]["date"]

    def test_get_min_date(self, session_models, raw_stock):
        ts = StockPrice.get_min_date(session_models, "AGG")
        assert isinstance(ts, pd.Timestamp)
        assert ts == pd.Timestamp(raw_stock[0]["date"])

    def test_get_max_date(self, session_models, raw_stock):
        ts = StockPrice.get_max_date(session_models, "AGG")
        assert ts == raw_stock[4]["date"]

    def test_get_valid_date_none(self, session_models, raw_stock):
        ts = StockPrice.get_valid_date(session_models, "AGG", ts=None)
        assert ts == raw_stock[4]["date"]

    def test_get_valid_date_exact(self, session_models, raw_stock):
        ts_id2 = raw_stock[1]["date"]
        ts = StockPrice.get_valid_date(session_models, "AGG", ts=ts_id2)
        assert ts == ts_id2

    def test_get_valid_date_lt(self, session_models, raw_stock):
        ts_id3 = pd.Timestamp(raw_stock[2]["date"])
        ts_id4 = pd.Timestamp(raw_stock[3]["date"])
        ts_middle = ts_id3 + (ts_id4 - ts_id3) / 2
        ts = StockPrice.get_valid_date(session_models, "AGG", ts=ts_middle)
        assert ts == ts_id3.to_pydatetime()

    def test_stringify(self, session_models):
        sp = session_models.query(StockPrice).get(1)
        sp_str = str(sp)
        assert sp_str.startswith("<StockPrice(")
        assert sp_str.endswith(")>")
        key_properties = [
            "id",
            "tckr",
            "date",
            "open",
            "high",
            "low",
            "close",
            "adj_close",
            "dividends",
            "split_factor",
        ]
        for k in key_properties:
            assert f"{k}={getattr(sp, k)}" in sp_str
