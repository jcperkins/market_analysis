import datetime
from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from sqlalchemy import (
    Column,
    String,
    Float,
    Date,
    UniqueConstraint,
    Integer,
    DateTime,
    ForeignKey,
)
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# TODO: make this inherit from the generic base class if I ever can implement it
GenericBase = declarative_base()


class City(GenericBase):
    """Generic Table of cities to use as a categorical database"""

    __tablename__ = "city"
    # a stock price is unique if the tckr and date of price are unique
    __table_args__ = (UniqueConstraint("city", "state"),)

    # columns
    id = Column(Integer, primary_key=True)
    city = Column(String, nullable=False)
    state = Column(String, nullable=False)
    last_updated = Column(DateTime, nullable=False)
    population = Column(Integer, nullable=False)
    area_sqkm = Column(Float, nullable=False)


class Person(GenericBase):
    """Generic Table of people to use as a records database """

    __tablename__ = "person"
    # a stock price is unique if the tckr and date of price are unique
    __table_args__ = (UniqueConstraint("first_name", "last_name", "age"),)

    # columns
    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey("city.id"))
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    alias = Column(String)
    birthday = Column(Date, nullable=False)
    age = Column(Integer, nullable=False)
    score = Column(Float, nullable=False)

    # relationships
    # home_city = relationship("City", back_populates="people")


class CityData(GenericBase):
    """Generic Table of people to use as a records database """

    __tablename__ = "city_data"
    # a stock price is unique if the tckr and date of price are unique
    __table_args__ = (UniqueConstraint("city_id", "ts"),)

    # columns
    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey("city.id"))
    ts = Column(DateTime)
    alpha1 = Column(String)
    numeric1 = Column(Float)
    numeric2 = Column(Float)
    numeric3 = Column(Float)


@pytest.fixture(scope="session")
def city_model():
    return City


@pytest.fixture(scope="session")
def person_model():
    return Person


@pytest.fixture(scope="session")
def city_data_model():
    return CityData


@pytest.fixture(scope="session")
def raw_cities():
    return [
        dict(
            id=1,
            city="Dallas",
            state="TX",
            last_updated=datetime.datetime(
                year=2020, month=1, day=1, hour=8, minute=30
            ),
            population=2000000,
            area_sqkm=4000,
        ),
        dict(
            id=2,
            city="Fort Worth",
            state="TX",
            last_updated=datetime.datetime(
                year=2019, month=8, day=1, hour=8, minute=30
            ),
            population=1000000,
            area_sqkm=10000,
        ),
        dict(
            id=3,
            city="Richardson",
            state="TX",
            last_updated=datetime.datetime(
                year=2020, month=1, day=15, hour=8, minute=30
            ),
            population=850000,
            area_sqkm=5000,
        ),
        dict(
            id=4,
            city="Garland",
            state="TX",
            last_updated=datetime.datetime(
                year=2019, month=12, day=1, hour=8, minute=30
            ),
            population=50000,
            area_sqkm=3000,
        ),
        dict(
            id=5,
            city="Plano",
            state="TX",
            last_updated=datetime.datetime(
                year=2020, month=1, day=10, hour=8, minute=30
            ),
            population=950000,
            area_sqkm=6000,
        ),
        dict(
            id=6,
            city="New York",
            state="NY",
            last_updated=datetime.datetime(
                year=2019, month=1, day=10, hour=8, minute=30
            ),
            population=950000,
            area_sqkm=6000,
        ),
    ]


@pytest.fixture(scope="session")
def raw_people():
    return [
        dict(
            city_id=3,
            first_name="James",
            last_name="Perkins",
            alias="Cory",
            birthday=datetime.date(year=1999, month=10, day=24),
            age=21,
            score=40.25,
        ),
        dict(
            city_id=3,
            first_name="Karen",
            last_name="Perkins",
            birthday=datetime.date(year=1998, month=2, day=24),
            age=22,
            score=40.25,
        ),
        dict(
            city_id=3,
            first_name="Alan",
            last_name="Davis",
            alias="Robert",
            birthday=datetime.date(year=1982, month=4, day=9),
            age=38,
            score=24.5,
        ),
        dict(
            city_id=3,
            first_name="Kim",
            last_name="Davis",
            birthday=datetime.date(year=1981, month=10, day=17),
            age=39,
            score=30.2,
        ),
        dict(
            city_id=4,
            first_name="James",
            last_name="Perkins",
            birthday=datetime.date(year=1955, month=5, day=1),
            age=75,
            score=65.2,
        ),
        dict(
            city_id=1,
            first_name="James",
            last_name="Perkins",
            birthday=datetime.date(year=1991, month=7, day=7),
            age=29,
            score=5.63,
        ),
        dict(
            city_id=1,
            first_name="Cassie",
            last_name="Perkins",
            birthday=datetime.date(year=1992, month=11, day=12),
            age=28,
            score=8.32,
        ),
    ]


@pytest.fixture(scope="session")
def ts_index() -> pd.DatetimeIndex:
    """unique dates in the city_data_df DataFrame"""
    return pd.date_range(
        start=pd.Timestamp(year=2020, month=1, day=1),
        end=pd.Timestamp(year=2020, month=1, day=4),
        periods=5,
    )


@pytest.fixture(scope="session")
def city_data_df(ts_index) -> pd.DataFrame:
    """
    Data where the numeric data is generated randomly and will look similar to the df below
        id	city_id	ts	                alpha1	numeric1	numeric2	numeric3
    0	1	1	    2020-01-01 00:00:00	str1	0.007216	7.284662	13.977155
    1	2	1	    2020-01-01 18:00:00	str2	0.497204	7.606501	3.524826
    2	3	1	    2020-01-02 12:00:00	str3	0.512003	0.861407	76.610837
    3	4	1	    2020-01-03 06:00:00	str4	0.127931	3.677853	28.785314
    4	5	1	    2020-01-04 00:00:00	str5	0.415176	2.523006	20.206167
    5	6	3	    2020-01-01 00:00:00	str6	0.844916	1.329927	74.695384
    6	7	3	    2020-01-01 18:00:00	str7	0.594634	7.026975	32.017001
    7	8	3	    2020-01-02 12:00:00	str8	0.286980	5.346947	6.907265
    8	9	3	    2020-01-03 06:00:00	str9	0.246318	4.797235	76.489924
    9	10	3	    2020-01-04 00:00:00	str10	0.213966	6.475723	3.282838
    """
    return pd.concat(
        [
            pd.Series(range(1, 11), name="id"),
            pd.Series([1] * 5 + [3] * 5, name="city_id"),
            pd.Series(np.tile(ts_index, 2), name="ts"),
            pd.Series([f"str{x + 1}" for x in range(10)], name="alpha1"),
            pd.DataFrame(
                np.random.rand(10, 3), columns=["numeric1", "numeric2", "numeric3"]
            )
            * np.array([[1, 10, 100]]),
        ],
        ignore_index=False,
        axis=1,
    )


@pytest.fixture(scope="session")
def generic_session(tmpdir_factory, raw_cities, raw_people, city_data_df):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("generic_db_testing.sqlite")
        .resolve()
    )
    print("file path to db used for the generic tables: " + str(fname))

    # set the global engine and _SessionFactory objects
    sql_engine = create_engine("sqlite:///" + str(fname), echo=False)
    # create a session factory that will then create a final session
    _SessionFactory = sessionmaker(bind=sql_engine)

    GenericBase.metadata.create_all(sql_engine)
    session = _SessionFactory()

    # add the raw records
    for rc in raw_cities:
        session.add(City(**rc))
    session.commit()
    for rp in raw_people:
        session.add(Person(**rp))
    session.commit()
    city_data_df.to_sql("city_data", sql_engine, if_exists="replace", index=False)
    session.commit()

    return session


@pytest.fixture(scope="session")
def generic_sql_engine(generic_session):
    return generic_session.get_bind()


@pytest.fixture(scope="session")
def city_df(generic_session, raw_cities):
    df = pd.read_sql(
        "select * from city",
        con=generic_session.get_bind(),
        parse_dates="last_updated",
        index_col=None,
    )
    assert df.shape == (len(raw_cities), len(City.__table__.columns))
    assert isinstance(df.loc[0, "last_updated"], pd.Timestamp)
    return df


@pytest.fixture(scope="session")
def person_df(generic_session, raw_people):
    df = pd.read_sql(
        "select * from person",
        con=generic_session.get_bind(),
        parse_dates="birthday",
        index_col=None,
    )
    assert df.shape == (len(raw_people), len(Person.__table__.columns))
    assert isinstance(df.loc[0, "birthday"], pd.Timestamp)
    return df


@pytest.fixture(scope="session")
def df_null_data(city_data_df, ts_index) -> pd.DataFrame:
    """
    provides multiple NaN scenarios based on the column for the data corresponding to city_id == 3
    * alpha1 has a single null value at ts_2
    * numeric1 has 1 NaN value at ts_0
    * numeric2 has 3 consecutive NaN values at ts_1, ts_2, ts_3
    * numeric3 is NaN for all city_id==3 rows

    | id |  ts  | city_id | alpha1 | numeric1 | numeric2 | numeric3 |
    -----------------------------------------------------------
    | 1  | ts_0 |    1    |  str  |     #     |    #   |      #       |
    ...
    | 6  | ts_0 |    3    |  str  |    NaN    |    #   |     NaN      |
    | 7  | ts_1 |    3    |  str  |     #     |   NaN  |     NaN      |
    | 8  | ts_2 |    3    |  None |     #     |   NaN  |     NaN      |
    | 9  | ts_3 |    3    |  str  |     #     |   NaN  |     NaN      |
    | 10 | ts_4 |    3    |  str  |     #     |    #   |     NaN      |
    """
    df_multi = city_data_df.copy().set_index(
        ["city_id", "ts"], drop=True, inplace=False
    )
    # assign None alpha
    df_multi.loc[(3, ts_index[2]), "alpha1"] = None
    # assign NaN values
    df_multi.loc[(3, ts_index[0]), "numeric1"] = np.NaN
    df_multi.loc[
        [(3, ts_index[1]), (3, ts_index[2]), (3, ts_index[3])], "numeric2"
    ] = np.NaN
    df_multi.loc[pd.IndexSlice[3, :], "numeric3"] = np.NaN
    df_multi = df_multi.reset_index(drop=False, inplace=False)[city_data_df.columns]

    # validate the results
    na_values = pd.isna(df_multi.loc[df_multi.city_id == 3, :])
    assert na_values["alpha1"].values[2]
    assert na_values["numeric1"].values[0]
    assert np.all(na_values["numeric2"].values[1:4])
    assert np.all(na_values["numeric3"])

    return df_multi


@pytest.fixture(scope="session")
def generic_null_data_session(tmpdir_factory, df_null_data, raw_cities, raw_people):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("generic_null_data_testing.sqlite")
        .resolve()
    )
    print("file path to db used for the generic tables with null data: " + str(fname))
    # set the global engine and _SessionFactory objects
    sql_engine = create_engine("sqlite:///" + str(fname), echo=False)
    # create a session factory that will then create a final session
    _SessionFactory = sessionmaker(bind=sql_engine)

    GenericBase.metadata.create_all(sql_engine)
    session = _SessionFactory()

    # add the raw records
    for rc in raw_cities:
        session.add(City(**rc))
    session.commit()
    for rp in raw_people:
        session.add(Person(**rp))
    session.commit()
    df_null_data.to_sql("city_data", sql_engine, if_exists="replace", index=False)
    session.commit()

    return session


@pytest.fixture(scope="session")
def generic_ts_df_reindexed(city_data_df) -> pd.DataFrame:
    """Returns the `city_data_df` DataFrame with the city_id and ts columns set as the index for easy access"""
    return city_data_df.set_index(["city_id", "ts"], drop=True, inplace=False)


@pytest.fixture(scope="session")
def generic_ts_df_nulls_reindexed(df_null_data) -> pd.DataFrame:
    """
    Returns the `df_null_data` DataFrame with the city_id and ts columns set as the index
    for easy access to the data
    """
    return df_null_data.set_index(["city_id", "ts"], drop=True, inplace=False)
