from pathlib import Path
from typing import List

import pytest
import sqlalchemy
from sqlalchemy.orm import Session
from src.DataSource.sql_db import (
    get_engine,
    set_engine,
    get_db_path,
    set_db_path,
    get_metadata,
    session_factory,
)

# noinspection PyProtectedMember
from src.DataSource.sql_db.connect_info import _validate_db_path, Base


@pytest.fixture(scope="module")
def expected_tables() -> List[str]:
    return ["stock_price", "libor", "options", "cpi_rates"]


class TestConnectionInfo(object):
    def test_get_metadata(self):
        assert get_metadata() == Base.metadata

    def test_paths_to_memory(self):
        assert _validate_db_path(None) == ":memory:"
        assert _validate_db_path("") == ":memory:"
        assert _validate_db_path(":memory:") == ":memory:"

    def test_engine_to_memory(self, expected_tables):
        sql_engine = set_engine(":memory:", echo_parm=False)
        assert get_db_path() == ":memory:"
        assert get_engine() == sql_engine
        session = session_factory()
        assert isinstance(session, Session)
        metadata = get_metadata()
        # verify that tables got populated
        t_metadata = [t for t in metadata.tables.keys()]
        t_inspection = [t for t in sqlalchemy.inspect(sql_engine).get_table_names()]
        for t in expected_tables:
            assert t in t_metadata
            assert t in t_inspection

    def test_shallow_db(self, tmpdir: Path, expected_tables: List[str]):
        shallow_db_path = Path(tmpdir).joinpath("temp_db.sqlite")
        assert shallow_db_path.exists() is False
        sql_engine = set_engine(fpath=shallow_db_path)
        assert get_engine() == sql_engine
        assert get_db_path() == shallow_db_path.resolve()
        # assert shallow_db_path.is_file() is True
        session = session_factory()
        assert isinstance(session, Session)
        metadata = get_metadata()
        # verify that tables got populated
        t_metadata = [t for t in metadata.tables.keys()]
        t_inspection = [t for t in sqlalchemy.inspect(sql_engine).get_table_names()]
        for t in expected_tables:
            assert t in t_metadata
            assert t in t_inspection

        # quick test for when the routine is called with the file already exists
        sql_engine = set_engine(fpath=shallow_db_path)
        assert get_engine() == sql_engine
        assert get_db_path() == shallow_db_path.resolve()
        # ... with 'default'
        sql_engine = set_engine(fpath="default")
        assert get_engine() == sql_engine
        assert get_db_path() == shallow_db_path.resolve()

    def test_set_db_path(self):
        assert get_db_path() != ":memory:"
        assert set_db_path(":memory:") == ":memory:"
        assert get_db_path() == ":memory:"

    def test_nested_relative_db(self, tmpdir: Path, expected_tables: List[str]):
        print(str(tmpdir))
        nested_db_path = Path(tmpdir).joinpath("temp", "temp_db.sqlite").resolve()
        relative_path = Path(tmpdir).joinpath("temp", "..", "temp", "temp_db.sqlite")
        print(str(relative_path))

        assert nested_db_path.exists() is False
        assert nested_db_path.parent.exists() is False
        sql_engine = set_engine(fpath=relative_path)
        assert get_engine() == sql_engine
        assert get_db_path() == nested_db_path
        assert nested_db_path.parent.exists() is True
        # assert nested_db_path.exists() is True
        session = session_factory()
        assert isinstance(session, Session)
        metadata = get_metadata()
        # verify that tables got populated
        t_metadata = [t for t in metadata.tables.keys()]
        t_inspection = [t for t in sqlalchemy.inspect(sql_engine).get_table_names()]
        for t in expected_tables:
            assert t in t_metadata
            assert t in t_inspection
