import numpy as np
import pandas as pd
import pytest

# noinspection PyProtectedMember
from src.DataSource.sql_db.UpdateFunctions.update_inflation_data import (
    _transform_raw_data,
)


@pytest.fixture()
def raw_cpi_df() -> pd.DataFrame:
    data = [
        [
            2019,
            251.712,
            252.776,
            254.202,
            255.548,
            256.092,
            256.143,
            256.571,
            256.558,
            256.759,
            257.346,
            257.208,
            np.NaN,
            np.NaN,
        ],
        [
            2018,
            247.867,
            248.991,
            249.554,
            250.546,
            251.588,
            251.989,
            252.006,
            252.146,
            252.439,
            252.885,
            252.038,
            251.233,
            251.107,
        ],
    ]
    columns = [
        "Year",
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
        "Ave",
    ]
    return pd.DataFrame(data=data, columns=columns)


@pytest.fixture()
def formatted_cpi_df() -> pd.DataFrame:
    data = [
        [2019, 11, 257.208],
        [2019, 10, 257.346],
        [2019, 9, 256.759],
        [2019, 8, 256.558],
        [2019, 7, 256.571],
        [2019, 6, 256.143],
        [2019, 5, 256.092],
        [2019, 4, 255.548],
        [2019, 3, 254.202],
        [2019, 2, 252.776],
        [2019, 1, 251.712],
        [2018, 12, 251.233],
        [2018, 11, 252.038],
        [2018, 10, 252.885],
        [2018, 9, 252.439],
        [2018, 8, 252.146],
        [2018, 7, 252.006],
        [2018, 6, 251.989],
        [2018, 5, 251.588],
        [2018, 4, 250.546],
        [2018, 3, 249.554],
        [2018, 2, 248.991],
        [2018, 1, 247.867],
    ]
    columns = ["year", "month", "rate"]
    return pd.DataFrame(data=data, columns=columns)


def test_transform_raw_data(raw_cpi_df: pd.DataFrame, formatted_cpi_df: pd.DataFrame):
    assert formatted_cpi_df.equals(_transform_raw_data(raw_cpi_df)) is True
