import pandas as pd
import numpy as np
from src.DataSource.sql_db.QueryAPI.Common import get_time_series_data
from collections import Counter


class TestGetTimeSeriesData:
    def test_no_date_no_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[:, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col=city_data_model.ts,
            get_cols=None,
            dates=None,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_no_date_single_column_name_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[:, "alpha1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols="alpha1",
            dates=None,
            sql_filters=[city_data_model.city_id == 3],
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_no_date_single_column_obj_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[:, "alpha1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=city_data_model.alpha1,
            dates=None,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_no_date_column_list_length1_returns_df_with_one_column(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[:, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col=city_data_model.ts,
            get_cols=[city_data_model.alpha1],
            dates=None,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_no_date_multi_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        expected = (
            city_data_df.loc[
                city_data_df.city_id == 3,
                ["ts", "alpha1", "numeric1", "numeric2", "numeric3"],
            ]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[:, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col=city_data_model.ts,
            get_cols=[
                city_data_model.alpha1,
                "numeric1",
                city_data_model.numeric2,
                "numeric3",
            ],
            dates=None,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_single_date_no_columns_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1]
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=None,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_single_date_single_column_returns_scalar(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1]
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, "alpha1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=city_data_model.alpha1,
            dates=date_info,
            city_id=3,
        )
        assert type(result) == str
        assert expected == result

        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "numeric1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, "numeric1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=city_data_model.numeric1,
            dates=date_info,
            city_id=3,
        )
        assert type(result) == float
        assert expected == result

    def test_single_date_column_list_length1_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1]
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, ["alpha1"]]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=[city_data_model.alpha1],
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_single_date_multi_columns_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1]
        expected = (
            city_data_df.loc[
                city_data_df.city_id == 3,
                ["ts", "alpha1", "numeric1", "numeric2", "numeric3"],
            ]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=[
                city_data_model.alpha1,
                "numeric1",
                city_data_model.numeric2,
                "numeric3",
            ],
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_date_list_length1_no_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = [ts_index[1]]
        sql_get = None
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_date_list_length1_single_column_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1:2]
        sql_get = city_data_model.alpha1
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, "alpha1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_date_list_length1_column_list_length1_returns_df_with_one_column(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = np.array([ts_index[1]])
        sql_get = [city_data_model.alpha1]
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_date_list_length1_multi_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = [ts_index[1]]
        sql_get = [
            city_data_model.alpha1,
            "numeric1",
            city_data_model.numeric2,
            "numeric3",
        ]
        expected = (
            city_data_df.loc[
                city_data_df.city_id == 3,
                ["ts", "alpha1", "numeric1", "numeric2", "numeric3"],
            ]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_multi_date_no_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1:-1]
        sql_get = None
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_multi_date_single_column_returns_series(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1:-1]
        sql_get = city_data_model.alpha1
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, "alpha1"]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_multi_date_column_list_length1_returns_df_with_one_column(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1:-1]
        sql_get = [city_data_model.alpha1]
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_multi_date_multi_columns_returns_df(
        self, generic_session, city_data_model, city_data_df, ts_index
    ):
        date_info = ts_index[1:-1]
        sql_get = [
            city_data_model.alpha1,
            "numeric1",
            city_data_model.numeric2,
            "numeric3",
        ]
        expected = (
            city_data_df.loc[
                city_data_df.city_id == 3,
                ["ts", "alpha1", "numeric1", "numeric2", "numeric3"],
            ]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
            .loc[date_info, :]
        )
        result = get_time_series_data(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=sql_get,
            dates=date_info,
            city_id=3,
        )
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])
