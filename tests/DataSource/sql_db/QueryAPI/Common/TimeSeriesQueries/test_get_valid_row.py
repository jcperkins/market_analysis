from typing import List

import numpy as np
import pandas as pd
import pytest
from src.DataSource.sql_db.QueryAPI.Common import column_info
from src.DataSource.sql_db.QueryAPI.Common.time_series_data import get_valid_row


@pytest.fixture(scope="module")
def df_valid(generic_ts_df_reindexed) -> pd.DataFrame:
    """Short hand variable for the descriptive but lengthy `generic_ts_df_reindexed` DataFrame"""
    return generic_ts_df_reindexed


@pytest.fixture(scope="module")
def df_nulls(generic_ts_df_nulls_reindexed) -> pd.DataFrame:
    """Short hand variable for the descriptive but lengthy `generic_ts_df_nulls_reindexed` DataFrame"""
    return generic_ts_df_nulls_reindexed


@pytest.fixture(scope="module")
def get_df_cols() -> List[str]:
    return ["alpha1", "numeric1", "numeric2", "numeric3"]


@pytest.fixture(scope="module")
def col_names_no_date(get_df_cols) -> List[str]:
    return ["city_id"] + get_df_cols


@pytest.fixture(scope="module")
def col_objs_no_date(col_names_no_date, city_data_model):
    return [column_info(x, city_data_model)[0] for x in col_names_no_date]


@pytest.fixture(scope="module")
def col_names_with_date(col_names_no_date) -> List[str]:
    return ["ts"] + col_names_no_date


@pytest.fixture(scope="module")
def col_objs_with_date(col_objs_no_date, city_data_model):
    return [city_data_model.ts] + col_objs_no_date


class TestGetValidRow:
    def test_no_date_returns_last_valid_values(
        self,
        generic_session,
        city_data_model,
        df_valid,
        ts_index,
        get_df_cols,
        col_names_no_date,
        col_objs_no_date,
        col_objs_with_date,
    ):
        # setup
        expected = df_valid.loc[(3, ts_index.max()), get_df_cols].copy()
        assert isinstance(expected, pd.Series)
        expected.name = pd.Timestamp(ts_index.max())
        expected["city_id"] = 3
        expected = expected[col_names_no_date]

        x_kwargs_no_date = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            city_id=3,
        )
        # testing
        assert isinstance(x_kwargs_no_date, pd.Series)
        assert len(col_names_no_date) == len(x_kwargs_no_date.index)
        for c in x_kwargs_no_date.index:
            assert c in col_names_no_date
        assert x_kwargs_no_date[col_names_no_date].equals(expected)

        x_sql_filters_with_date = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_with_date,
            ts_col=city_data_model.ts,
            sql_filters=[city_data_model.city_id == 3],
        )
        assert isinstance(x_sql_filters_with_date, pd.Series)
        assert len(col_names_no_date) == len(x_sql_filters_with_date.index)
        for c in x_sql_filters_with_date.index:
            assert c in col_names_no_date
        assert x_sql_filters_with_date[col_names_no_date].equals(expected)

    def test_no_get_cols_returns_all_columns(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        # setup
        expected = df_valid.loc[(3, ts_index.max()), :].copy()
        assert isinstance(expected, pd.Series)
        expected.name = pd.Timestamp(ts_index.max())
        expected["city_id"] = 3

        x = get_valid_row(
            generic_session, city_data_model, ts_col=city_data_model.ts, city_id=3
        )
        # testing
        assert isinstance(x, pd.Series)
        assert len(x.index) == (len(df_valid.columns) + 1)
        assert len(x.index) == len(expected.index)
        for c in x.index:
            assert c in expected.index
        assert x[expected.index].equals(expected)

    def test_less_than_min_timestamp_returns_none(
        self, generic_session, city_data_model, df_valid, ts_index, col_objs_no_date
    ):
        x = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            ts=ts_index.min() - pd.Timedelta(1, unit="hour"),
            city_id=3,
        )
        assert x is None

    def test_exact_date_allow_equals_returns_value(
        self,
        generic_session,
        city_data_model,
        df_valid,
        ts_index,
        get_df_cols,
        col_names_no_date,
        col_objs_no_date,
    ):
        # setup
        query_ts = pd.Timestamp(ts_index[2])
        expected = df_valid.loc[(3, query_ts), get_df_cols].copy()
        assert isinstance(expected, pd.Series)
        expected.name = query_ts
        expected["city_id"] = 3
        expected = expected[col_names_no_date]

        # testing
        x = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            allow_equal=True,
            ts=query_ts,
            city_id=3,
        )
        assert isinstance(x, pd.Series)
        assert len(col_names_no_date) == len(x.index)
        for c in x.index:
            assert c in col_names_no_date
        assert x[col_names_no_date].equals(expected)

    def test_exact_date_no_equals_returns_prev_value(
        self,
        generic_session,
        city_data_model,
        df_valid,
        ts_index,
        get_df_cols,
        col_names_no_date,
        col_objs_no_date,
    ):
        # setup
        query_ts = pd.Timestamp(ts_index[2])
        expected_ts = pd.Timestamp(ts_index[1])
        assert query_ts > expected_ts
        expected = df_valid.loc[(3, expected_ts), get_df_cols].copy()
        assert isinstance(expected, pd.Series)
        expected.name = query_ts
        expected["city_id"] = 3
        expected = expected[col_names_no_date]

        # testing
        x = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            allow_equal=False,
            ts=query_ts,
            city_id=3,
        )
        assert isinstance(x, pd.Series)
        assert len(col_names_no_date) == len(x.index)
        for c in x.index:
            assert c in col_names_no_date
        assert x[col_names_no_date].equals(expected)

    def test_lt_date_returns_prev_value(
        self,
        generic_session,
        city_data_model,
        df_valid,
        ts_index,
        get_df_cols,
        col_names_no_date,
        col_objs_no_date,
    ):
        # setup
        ts_low = pd.Timestamp(ts_index[2])
        ts_high = pd.Timestamp(ts_index[3])
        assert ts_high > ts_low
        query_ts = ts_low + ((ts_high - ts_low) / 2)
        expected = df_valid.loc[(3, ts_low), get_df_cols].copy()
        assert isinstance(expected, pd.Series)
        expected.name = query_ts
        expected["city_id"] = 3
        expected = expected[col_names_no_date]

        # testing
        x = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            allow_equal=True,
            ts=query_ts,
            city_id=3,
        )
        assert isinstance(x, pd.Series)
        assert len(col_names_no_date) == len(x.index)
        for c in x.index:
            assert c in col_names_no_date
        assert x[col_names_no_date].equals(expected)

        # should be true regardless of the value of allow_equals
        x = get_valid_row(
            generic_session,
            city_data_model,
            get_cols=col_objs_no_date,
            ts_col=city_data_model.ts,
            allow_equal=False,
            ts=query_ts,
            city_id=3,
        )
        assert isinstance(x, pd.Series)
        assert len(col_names_no_date) == len(x.index)
        for c in x.index:
            assert c in col_names_no_date
        assert x[col_names_no_date].equals(expected)

    def test_single_nan_column_is_filled(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - ts_index[3] should have a NaN value only in numeric2 that
        # can can be filled with the value at ts_index[0] as long as numeric3 is not pulled
        query_ts = pd.Timestamp(ts_index[3])
        assert pd.isna(df_nulls.loc[(3, query_ts), "numeric2"])
        assert (
            pd.isna(
                df_nulls.loc[(3, query_ts), ["alpha1", "numeric1", "numeric2"]]
            ).sum()
            == 1
        )
        get_col_names = ["city_id", "alpha1", "numeric1", "numeric2"]
        get_col_obj = [column_info(x, city_data_model)[0] for x in get_col_names]
        x = get_valid_row(
            generic_null_data_session,
            city_data_model,
            get_cols=get_col_obj,
            ts_col=city_data_model.ts,
            ts=query_ts,
            city_id=3,
        )

        assert isinstance(x, pd.Series) and len(x.index) == 4
        assert x.name == query_ts
        assert x.city_id == 3
        assert x.alpha1 == df_nulls.loc[(3, ts_index[3]), "alpha1"]
        assert x.numeric1 == df_nulls.loc[(3, ts_index[3]), "numeric1"]
        assert x.numeric2 == df_nulls.loc[(3, ts_index[0]), "numeric2"]

    def test_fill_nan_values_when_possible(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - ts_index[2] should have:
        # * None in `alpha1` that will be filled by the value at ts_index[1]
        # * NaN in `numeric2` that will be filled by the value at ts_index[0]
        query_ts = pd.Timestamp(ts_index[2])
        assert np.all(pd.isna(df_nulls.loc[(3, query_ts), ["alpha1", "numeric2"]]))
        get_col_names = ["city_id", "alpha1", "numeric1", "numeric2"]
        get_col_obj = [column_info(x, city_data_model)[0] for x in get_col_names]
        x = get_valid_row(
            generic_null_data_session,
            city_data_model,
            get_cols=get_col_obj,
            ts_col=city_data_model.ts,
            ts=query_ts,
            city_id=3,
        )

        assert isinstance(x, pd.Series) and len(x.index) == 4
        assert x.name == query_ts
        assert x.city_id == 3
        assert x.alpha1 == df_nulls.loc[(3, ts_index[1]), "alpha1"]
        assert x.numeric1 == df_nulls.loc[(3, ts_index[2]), "numeric1"]
        assert x.numeric2 == df_nulls.loc[(3, ts_index[0]), "numeric2"]

    def test_allow_nan_if_all_previous_cells_are_nan(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - ts_index[2] should have:
        # * None in `alpha1` that will be filled by the value at ts_index[1]
        # * NaN in `numeric2` that will be filled by the value at ts_index[0]
        # * NaN in `numeric3` that cannot be filled
        query_ts = pd.Timestamp(ts_index[2])
        assert np.all(
            pd.isna(df_nulls.loc[(3, query_ts), ["alpha1", "numeric2", "numeric3"]])
        )
        get_col_names = ["city_id", "alpha1", "numeric1", "numeric2", "numeric3"]
        get_col_obj = [column_info(x, city_data_model)[0] for x in get_col_names]
        x = get_valid_row(
            generic_null_data_session,
            city_data_model,
            get_cols=get_col_obj,
            ts_col=city_data_model.ts,
            ts=query_ts,
            city_id=3,
        )

        assert isinstance(x, pd.Series) and len(x.index) == 5
        assert x.name == query_ts
        assert x.city_id == 3
        assert x.alpha1 == df_nulls.loc[(3, ts_index[1]), "alpha1"]
        assert x.numeric1 == df_nulls.loc[(3, ts_index[2]), "numeric1"]
        assert x.numeric2 == df_nulls.loc[(3, ts_index[0]), "numeric2"]
        assert pd.isna(x.numeric3)

    def test_allow_nan_if_earliest_record_is_nan(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - ts_index[0] should have NaN values in columns numeric1 and numeric3 that cannot be filled:
        query_ts = pd.Timestamp(ts_index[0])
        assert np.all(pd.isna(df_nulls.loc[(3, query_ts), ["numeric1", "numeric3"]]))
        get_col_names = ["city_id", "alpha1", "numeric1", "numeric2", "numeric3"]
        get_col_obj = [column_info(x, city_data_model)[0] for x in get_col_names]
        x = get_valid_row(
            generic_null_data_session,
            city_data_model,
            get_cols=get_col_obj,
            ts_col=city_data_model.ts,
            ts=query_ts,
            city_id=3,
        )

        assert isinstance(x, pd.Series) and len(x.index) == 5
        assert x.name == query_ts
        assert x.city_id == 3
        assert x.alpha1 == df_nulls.loc[(3, ts_index[0]), "alpha1"]
        assert pd.isna(x.numeric1)
        assert x.numeric2 == df_nulls.loc[(3, ts_index[0]), "numeric2"]
        assert pd.isna(x.numeric3)

    def test_single_nan_column_cannot_be_filled_all_default_parameters(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - the last ts only has a NaN value for numeric3, but it cannot be filled
        query_ts = pd.Timestamp(ts_index.max())
        assert pd.isna(df_nulls.loc[(3, query_ts), "numeric3"])
        assert (
            pd.isna(
                df_nulls.loc[
                    (3, query_ts), ["alpha1", "numeric1", "numeric2", "numeric3"]
                ]
            ).sum()
            == 1
        )
        x = get_valid_row(
            generic_null_data_session,
            city_data_model,
            ts_col=city_data_model.ts,
            city_id=3,
        )

        assert isinstance(x, pd.Series) and len(x.index) == (len(df_nulls.columns) + 1)
        assert x.name == query_ts
        assert x.city_id == 3
        assert x.alpha1 == df_nulls.loc[(3, query_ts), "alpha1"]
        assert x.numeric1 == df_nulls.loc[(3, query_ts), "numeric1"]
        assert x.numeric2 == df_nulls.loc[(3, query_ts), "numeric2"]
        assert pd.isna(x.numeric3)
