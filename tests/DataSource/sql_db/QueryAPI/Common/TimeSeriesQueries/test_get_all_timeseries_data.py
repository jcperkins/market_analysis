from collections import Counter

import numpy as np
import pandas as pd
from src.DataSource.sql_db.QueryAPI.Common.time_series_data import (
    get_all_timeseries_df,
    get_all_timeseries_column,
)


class TestDfTimeSeriesNoIndex:
    def test_no_cols_returns_all_columns_kwarg_filters_ts_col_obj(
        self, generic_session, city_data_model, city_data_df
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
        )
        result = get_all_timeseries_df(
            generic_session, city_data_model, city_data_model.ts, city_id=3
        )

        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_retrieves_correct_columns_sql_filters_ts_col_string(
        self, generic_session, city_data_model, city_data_df
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1", "numeric1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
        )
        result = get_all_timeseries_df(
            generic_session,
            city_data_model,
            ts_col="ts",
            get_cols=[
                city_data_model.ts,
                city_data_model.alpha1,
                city_data_model.numeric1,
            ],
            sql_filters=[city_data_model.city_id == 3],
        )

        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_retrieves_column_list_length1(
        self, generic_session, city_data_model, city_data_df
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "numeric2"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
        )
        result = get_all_timeseries_df(
            generic_session,
            city_data_model,
            ts_col=city_data_model.ts,
            get_cols=[city_data_model.numeric2],
            city_id=3,
        )

        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        assert expected.equals(result[expected.columns])

    def test_return_empty_df_if_no_data(self, generic_session, city_data_model):
        result = get_all_timeseries_df(
            generic_session, city_data_model, city_data_model.ts, city_id=100
        )
        expected_columns = [
            c.name for c in city_data_model.__table__.columns if c.name != "ts"
        ]
        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected_columns)
        assert result.shape == (0, len(expected_columns))

    def test_fills_null_values_when_possible(
        self, generic_null_data_session, city_data_model, df_null_data
    ):
        expected = (
            df_null_data.loc[df_null_data.city_id == 3, :]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")
        )
        result = get_all_timeseries_df(
            generic_null_data_session,
            city_data_model,
            ts_col=city_data_model.ts,
            city_id=3,
        )

        assert isinstance(result, pd.DataFrame)
        assert Counter(result.columns) == Counter(expected.columns)
        mask_nan = pd.isna(expected)
        assert mask_nan.values.sum() == pd.isna(result).values.sum()
        assert np.all(pd.isna(result.values[mask_nan]))
        assert np.all(expected.values[~mask_nan] == result.values[~mask_nan])


class TestColumnTimeSeriesNoIndex:
    def test_retrieves_correct_column_kwarg_filters(
        self, generic_session, city_data_model, city_data_df
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "alpha1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")["alpha1"]
        )
        result = get_all_timeseries_column(
            generic_session,
            city_data_model,
            city_data_model.ts,
            get_col=city_data_model.alpha1,
            city_id=3,
        )

        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_retrieves_correct_column_sql_filters(
        self, generic_session, city_data_model, city_data_df
    ):
        expected = (
            city_data_df.loc[city_data_df.city_id == 3, ["ts", "numeric1"]]
            .set_index("ts")
            .sort_index()
            .fillna(method="ffill")["numeric1"]
        )
        result = get_all_timeseries_column(
            generic_session,
            city_data_model,
            "ts",
            get_col=city_data_model.numeric1,
            sql_filters=[city_data_model.city_id == 3],
        )

        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_return_empty_series_if_no_data(self, generic_session, city_data_model):
        result = get_all_timeseries_column(
            generic_session,
            city_data_model,
            city_data_model.ts,
            get_col=city_data_model.numeric2,
            city_id=100,
        )
        assert isinstance(result, pd.Series)
        assert len(result) == 0
        assert result.name == "numeric2"
