import pandas as pd
import pytest
from src.DataSource.sql_db.QueryAPI.Common.time_series_data import get_valid_scalar


@pytest.fixture(scope="module")
def df_valid(generic_ts_df_reindexed) -> pd.DataFrame:
    """Short hand variable for the descriptive but lengthy `generic_ts_df_reindexed` DataFrame"""
    return generic_ts_df_reindexed


@pytest.fixture(scope="module")
def df_nulls(generic_ts_df_nulls_reindexed) -> pd.DataFrame:
    """Short hand variable for the descriptive but lengthy `generic_ts_df_nulls_reindexed` DataFrame"""
    return generic_ts_df_nulls_reindexed


class TestGetValidScalar:
    def test_no_date_returns_last_valid_value_filter_with_kwargs(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric1,
            ts_col=city_data_model.ts,
            city_id=3,
        )
        assert type(x) == float
        assert x == df_valid.loc[(3, ts_index.max()), "numeric1"]

    def test_no_date_returns_last_valid_value_filter_with_sql_filters(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric1,
            ts_col=city_data_model.ts,
            sql_filters=[city_data_model.city_id == 3],
        )
        assert type(x) == float
        assert x == df_valid.loc[(3, ts_index.max()), "numeric1"]

    def test_less_than_min_timestamp_returns_none(
        self, generic_session, city_data_model, ts_index
    ):
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.alpha1,
            ts_col=city_data_model.ts,
            ts=ts_index.min() - pd.Timedelta(1, unit="hour"),
            city_id=3,
        )
        assert x is None

    def test_exact_date_allow_equals_returns_value(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric2,
            ts_col=city_data_model.ts,
            allow_equal=True,
            ts=pd.Timestamp(ts_index[0]),
            city_id=3,
        )
        assert type(x) == float
        assert x == df_valid.loc[(3, ts_index[0]), "numeric2"]

    def test_first_timestamp_no_equals_returns_none(
        self, generic_session, city_data_model, ts_index
    ):
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric2,
            ts_col=city_data_model.ts,
            allow_equal=False,
            ts=pd.Timestamp(ts_index[0]),
            city_id=3,
        )
        assert x is None

    def test_exact_date_no_equals_returns_prev_value(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        ts_0 = pd.Timestamp(ts_index[0])
        ts_1 = pd.Timestamp(ts_index[1])
        assert ts_0 < ts_1
        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric3,
            ts_col=city_data_model.ts,
            ts=ts_1,
            allow_equal=False,
            city_id=3,
        )
        assert type(x) == float
        assert x == df_valid.loc[(3, ts_0), "numeric3"]

    def test_lt_date_returns_prev_value(
        self, generic_session, city_data_model, df_valid, ts_index
    ):
        ts_0 = pd.Timestamp(ts_index[0])
        ts_1 = pd.Timestamp(ts_index[1])
        assert ts_0 < ts_1
        ts_query = ts_0 + (ts_1 - ts_0) / 2
        expected = df_valid.loc[(3, ts_0), "numeric3"]

        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric3,
            ts_col=city_data_model.ts,
            ts=ts_query,
            city_id=3,
        )
        assert type(x) == float
        assert x == expected

        x = get_valid_scalar(
            generic_session,
            get_col=city_data_model.numeric3,
            ts_col=city_data_model.ts,
            ts=ts_query,
            city_id=3,
            allow_equal=False,
        )
        assert type(x) == float
        assert x == expected

    def test_date_of_invalid_returns_prev_value(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        # setup - ts_index[3], numeric2 has a NaN value that can can be filled with the value at ts_index[0]
        x = get_valid_scalar(
            generic_null_data_session,
            get_col=city_data_model.numeric2,
            ts_col=city_data_model.ts,
            ts=pd.Timestamp(ts_index[3]),
            city_id=3,
        )
        assert type(x) == float
        assert x == df_nulls.loc[(3, ts_index[0]), "numeric2"]

    def test_no_value_available_returns_none(
        self, generic_null_data_session, city_data_model, df_nulls, ts_index
    ):
        x = get_valid_scalar(
            generic_null_data_session,
            ts_col=city_data_model.ts,
            get_col=city_data_model.numeric3,
            city_id=3,
        )
        assert x is None
