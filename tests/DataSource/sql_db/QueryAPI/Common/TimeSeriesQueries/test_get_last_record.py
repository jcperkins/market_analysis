import pandas as pd
import pytest
from src.DataSource.sql_db.QueryAPI.Common import get_last_record


@pytest.fixture(scope="module")
def df_data(generic_ts_df_reindexed) -> pd.DataFrame:
    """Short hand variable for the descriptive but lengthy `generic_ts_df_reindexed` DataFrame"""
    return generic_ts_df_reindexed


class TestGetLastRecord:
    def test_get_no_date(self, generic_session, city_data_model, df_data, ts_index):
        expected = df_data.loc[(3, ts_index.max()), :]
        d_obj = get_last_record(
            generic_session, city_data_model, city_data_model.ts, ts=None, city_id=3
        )
        assert isinstance(d_obj, city_data_model)
        assert d_obj.id == expected.id
        assert d_obj.ts == ts_index.max()

    def test_no_records(self, generic_session, city_data_model, df_data, ts_index):
        ts_id = pd.Timestamp(ts_index.min()) - pd.Timedelta(1, unit="min")
        d_obj = get_last_record(
            generic_session, city_data_model, city_data_model.ts, ts=ts_id, city_id=3
        )
        assert d_obj is None

    def test_exact_date_allow_equals(
        self, generic_session, city_data_model, df_data, ts_index
    ):
        ts_id = pd.Timestamp(ts_index[1])
        expected = df_data.loc[(3, ts_id), :]
        d_obj = get_last_record(
            generic_session,
            city_data_model,
            city_data_model.ts,
            ts=ts_id,
            allow_equal=True,
            city_id=3,
        )
        assert isinstance(d_obj, city_data_model)
        assert d_obj.id == expected.id
        assert d_obj.ts == ts_id

    def test_exact_date_no_equals(
        self, generic_session, city_data_model, df_data, ts_index
    ):
        ts_id = pd.Timestamp(ts_index[1])
        expected = df_data.loc[(3, ts_index[0]), :]
        d_obj = get_last_record(
            generic_session,
            city_data_model,
            city_data_model.ts,
            ts=ts_id,
            allow_equal=False,
            city_id=3,
        )
        assert isinstance(d_obj, city_data_model)
        assert d_obj.id == expected.id
        assert d_obj.ts == ts_index[0]

    def test_lt_date(self, generic_session, city_data_model, df_data, ts_index):
        # setup
        ts_low = pd.Timestamp(ts_index[2])
        ts_high = pd.Timestamp(ts_index[3])
        ts_middle = ts_low + (ts_high - ts_low) / 2
        expected = df_data.loc[(3, ts_low), :]

        # testing
        d_obj = get_last_record(
            generic_session,
            city_data_model,
            city_data_model.ts,
            ts=ts_middle,
            sql_filters=[city_data_model.city_id == 3],
        )
        assert isinstance(d_obj, city_data_model)
        assert d_obj.id == expected.id
        assert d_obj.ts == ts_low
