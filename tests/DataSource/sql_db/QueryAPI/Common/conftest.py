from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from src.DataSource.sql_db import set_engine, session_factory
from src.DataSource.sql_db.Models import StockPrice


@pytest.fixture(scope="session")
def tckr():
    return "SPY"


@pytest.fixture(scope="session")
def dt_index(stock_df, tckr):
    idx = pd.DatetimeIndex(stock_df.loc[stock_df.tckr == tckr, "date"].copy()[1:4])
    assert isinstance(idx, pd.DatetimeIndex) and len(idx) == 3
    return idx


@pytest.fixture(scope="session")
def stock_df_1nan(stock_df, tckr, dt_index) -> pd.DataFrame:
    """
    sets the close of the last index in dt_index to NaN and the adj_close of the second index in
    dt_index to nan
    | date | tckr | close | adj_close |
    -----------------------------------
    |  ts  | tckr |   #   |     #     |
    | dt_0 | tckr |   #   |     #     |
    | dt_1 | tckr |   #   |    NaN    |
    | dt_2 | tckr |  NaN  |     #     |
    |  ts  | tckr |   #   |     #     |
    |  ts  |!tckr |   #   |     #     |
    ...
    """

    df_1nan = stock_df.copy()
    close_bool = np.logical_and(df_1nan.tckr == tckr, df_1nan.date == dt_index[-1])
    adj_close_bool = np.logical_and(df_1nan.tckr == tckr, df_1nan.date == dt_index[1])
    assert (
        np.sum(close_bool) == 1
    ), "fixture 'stock_df_1nan' - filter criteria is lax when nullifying 'close'"
    assert (
        np.sum(adj_close_bool) == 1
    ), "fixture 'stock_df_1nan' - sql_filters are lax when nullifying 'adj_close'"
    assert dt_index[-1] == pd.Timestamp(df_1nan.loc[close_bool, "date"].values[0])
    assert dt_index[1] == pd.Timestamp(df_1nan.loc[adj_close_bool, "date"].values[0])
    df_1nan.loc[close_bool, "close"] = np.NaN
    df_1nan.loc[adj_close_bool, "adj_close"] = np.NaN
    assert np.isnan(
        df_1nan.loc[
            np.logical_and(df_1nan.tckr == tckr, df_1nan.date == dt_index[-1]), "close"
        ]
    ).values[0]
    assert np.isnan(
        df_1nan.loc[
            np.logical_and(df_1nan.tckr == tckr, df_1nan.date == dt_index[1]),
            "adj_close",
        ]
    ).values[0]
    return df_1nan


@pytest.fixture(scope="session")
def stock_df_2nan(stock_df_1nan, tckr, dt_index):
    """
    sets the close of the first index in dt_index to NaN in addition to the NaN values set on
    stock_df_1nan

    | date | tckr | close | adj_close |
    -----------------------------------
    ...
    |  ts  | tckr |   #   |     #     |
    | dt_0 | tckr |  NaN  |     #     |
    | dt_1 | tckr |   #   |    NaN    |
    | dt_2 | tckr |  NaN  |     #     |
    |  ts  | tckr |   #   |     #     |
    |  ts  |!tckr |   #   |     #     |
    ...
    """
    df_2nan = stock_df_1nan.copy()
    close_bool = np.logical_and(df_2nan.tckr == tckr, df_2nan.date == dt_index[0])
    assert (
        np.sum(close_bool) == 1
    ), "fixture 'stock_df_1nan' - filter criteria is lax when nullifying 'close'"
    assert dt_index[0] == pd.Timestamp(df_2nan.loc[close_bool, "date"].values[0])
    df_2nan.loc[close_bool, "close"] = np.NaN
    assert np.isnan(
        df_2nan.loc[
            np.logical_and(df_2nan.tckr == tckr, df_2nan.date == dt_index[0]), "close"
        ]
    ).values[0]
    return df_2nan


@pytest.fixture(scope="session")
def stock_df_multi_nan(stock_df_2nan, tckr, dt_index):
    """
    provides multiple NaN scenarios based on the column for the data corresponding to tckr
    * adj_close has a NaN at dt_1
    * close has 2 non-consecutive NaN values at dt_2 and dt_0
    * volume has 3 consecutive NaN values at all three dt_index values
    * split_factor is NaN for all values

    | date |  tckr  | close | adj_close | volume | split_factor |
    -----------------------------------------------------------
    ...
    |  ts  |  tckr  |   #   |     #     |    #   |     NaN      |
    | dt_0 |  tckr  |  NaN  |     #     |   NaN  |     NaN      |
    | dt_1 |  tckr  |   #   |    NaN    |   NaN  |     NaN      |
    | dt_2 |  tckr  |  NaN  |     #     |   NaN  |     NaN      |
    |  ts  |  tckr  |   #   |     #     |    #   |     NaN      |
    |  ts  | !tckr  |   #   |     #     |    #   |      #       |
    ...
    """
    df_multi = stock_df_2nan.copy()
    select_tckr = df_multi.tckr == tckr
    # assign NaN volumes
    df_multi.loc[
        np.logical_and(select_tckr, df_multi.date.isin(dt_index)), ["volume"]
    ] = np.NaN
    # assign NaN split_factors
    df_multi.loc[select_tckr, ["split_factor"]] = np.NaN

    # validate the results
    na_values = pd.isna(df_multi)
    na_volumes = na_values["volume"].values
    assert na_volumes.sum() == 3
    for dt in dt_index:
        ts = pd.Timestamp(dt)
        assert na_volumes[np.logical_and(select_tckr, df_multi.date == ts)]
    na_splits = na_values["split_factor"].values
    assert np.all(na_splits[select_tckr])

    return df_multi


@pytest.fixture(scope="session")
def stock_1nan_session(tmpdir_factory, stock_df_1nan):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("stock_1nan_db.sqlite")
        .resolve()
    )
    print("file path to db used for testing the models: " + str(fname))
    engine = set_engine(fname, echo_parm=False)
    session = session_factory()

    stock_df_1nan.to_sql(
        StockPrice.__tablename__, engine, if_exists="replace", index=False
    )

    session.commit()

    return session


@pytest.fixture(scope="session")
def stock_2nan_session(tmpdir_factory, stock_df_2nan):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("stock_2nan_db.sqlite")
        .resolve()
    )
    print("file path to db used for testing the models: " + str(fname))
    engine = set_engine(fname, echo_parm=False)
    session = session_factory()

    stock_df_2nan.to_sql(
        StockPrice.__tablename__, engine, if_exists="replace", index=False
    )

    session.commit()

    return session


@pytest.fixture(scope="session")
def stock_multi_nan_session(tmpdir_factory, stock_df_multi_nan):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("stock_multi_nan_db.sqlite")
        .resolve()
    )
    print("file path to db used for testing the models: " + str(fname))
    engine = set_engine(fname, echo_parm=True)
    session = session_factory()

    stock_df_multi_nan.to_sql(
        StockPrice.__tablename__, engine, if_exists="replace", index=False
    )

    session.commit()

    return session


@pytest.fixture(scope="session")
def expected_df_0nan(stock_df, tckr, dt_index) -> pd.DataFrame:
    return (
        stock_df.loc[stock_df.tckr == tckr, ["date", "tckr", "close", "adj_close"]]
        .copy()
        .set_index("date", inplace=False, drop=True)
        .loc[dt_index, ["tckr", "close", "adj_close"]]
    )


@pytest.fixture(scope="session")
def expected_series_0nan(expected_df_0nan) -> pd.Series:
    return expected_df_0nan.close


@pytest.fixture(scope="session")
def expected_df_1nan(stock_df_1nan, tckr, dt_index) -> pd.DataFrame:
    df = (
        stock_df_1nan.loc[
            stock_df_1nan.tckr == tckr, ["date", "tckr", "close", "adj_close"]
        ]
        .copy()
        .set_index("date", inplace=False, drop=True)
        .sort_index(axis=0, inplace=False, ascending=True)
        .fillna(method="ffill", axis=0, inplace=False)
        .loc[dt_index, ["tckr", "close", "adj_close"]]
    )

    assert df.loc[dt_index[-1], "close"] == df.loc[dt_index[-2], "close"]
    assert df.loc[dt_index[1], "adj_close"] == df.loc[dt_index[0], "adj_close"]
    return df


@pytest.fixture(scope="session")
def expected_series_1nan(expected_df_1nan, tckr, dt_index) -> pd.Series:
    return expected_df_1nan.close


@pytest.fixture(scope="session")
def expected_df_2nan(stock_df_2nan, tckr, dt_index) -> pd.DataFrame:
    df = (
        stock_df_2nan.loc[
            stock_df_2nan.tckr == tckr, ["date", "tckr", "close", "adj_close"]
        ]
        .copy()
        .set_index("date", inplace=False, drop=True)
        .sort_index(axis=0, inplace=False, ascending=True)
        .fillna(method="ffill", axis=0, inplace=False)
    )

    prev_ts = df.index[df.index < dt_index.min()].max()
    prev_close = df.loc[prev_ts, "close"]

    df = df.loc[dt_index, ["tckr", "close", "adj_close"]]
    assert df.loc[dt_index[0], "close"] == prev_close
    return df


@pytest.fixture(scope="session")
def expected_series_2nan(expected_df_2nan, tckr, dt_index) -> pd.Series:
    return expected_df_2nan.close
