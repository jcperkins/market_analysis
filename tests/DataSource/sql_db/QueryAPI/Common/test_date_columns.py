from sqlalchemy import Column, String, Float, Date, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base

from src.DataSource.sql_db.QueryAPI.Common import date_columns, date_column_names

DateColumnBase = declarative_base()


class NoDateColumns(DateColumnBase):
    __tablename__ = "no_dates_table"
    id = Column(Integer, primary_key=True)
    alpha1 = Column(String)
    numeric1 = Column(Integer)
    numeric2 = Column(Float)


class DateColumn(DateColumnBase):
    __tablename__ = "date_table"
    id = Column(Integer, primary_key=True)
    date1 = Column(Date)
    alpha1 = Column(String)
    numeric1 = Column(Integer)
    numeric2 = Column(Float)


class DateTimeColumn(DateColumnBase):
    __tablename__ = "datetime_table"
    id = Column(Integer, primary_key=True)
    datetime1 = Column(DateTime)
    alpha1 = Column(String)
    numeric1 = Column(Integer)
    numeric2 = Column(Float)


class BothColumns(DateColumnBase):
    __tablename__ = "date_datetime_table"
    id = Column(Integer, primary_key=True)
    date1 = Column(Date)
    datetime1 = Column(DateTime)
    alpha1 = Column(String)
    numeric1 = Column(Integer)
    numeric2 = Column(Float)


def test_no_date_columns():
    cols = date_columns(NoDateColumns)
    names = date_column_names(NoDateColumns)
    assert len(cols) == 0
    assert len(names) == 0

    cols = date_columns(NoDateColumns.__table__)
    names = date_column_names(NoDateColumns.__table__)
    assert len(cols) == 0
    assert len(names) == 0


def test_date_column():
    cols = date_columns(DateColumn)
    names = date_column_names(DateColumn)
    assert len(cols) == 1
    assert len(names) == 1
    assert DateColumn.date1 in cols
    assert DateColumn.date1.name in names

    cols = date_columns(DateColumn.__table__)
    names = date_column_names(DateColumn.__table__)
    assert len(cols) == 1
    assert len(names) == 1
    assert DateColumn.date1 in cols
    assert DateColumn.date1.name in names


def test_datetime_column():
    cols = date_columns(DateTimeColumn)
    names = date_column_names(DateTimeColumn)
    assert len(cols) == 1
    assert len(names) == 1
    assert DateTimeColumn.datetime1 in cols
    assert DateTimeColumn.datetime1.name in names

    cols = date_columns(DateTimeColumn.__table__)
    names = date_column_names(DateTimeColumn.__table__)
    assert len(cols) == 1
    assert len(names) == 1
    assert DateTimeColumn.datetime1 in cols
    assert DateTimeColumn.datetime1.name in names


def test_date_and_datetime_column():
    cols = date_columns(BothColumns)
    names = date_column_names(BothColumns)
    assert len(cols) == 2
    assert len(names) == 2
    assert BothColumns.date1 in cols
    assert BothColumns.date1.name in names
    assert BothColumns.datetime1 in cols
    assert BothColumns.datetime1.name in names

    cols = date_columns(BothColumns.__table__)
    names = date_column_names(BothColumns.__table__)
    assert len(cols) == 2
    assert len(names) == 2
    assert BothColumns.date1 in cols
    assert BothColumns.date1.name in names
    assert BothColumns.datetime1 in cols
    assert BothColumns.datetime1.name in names
