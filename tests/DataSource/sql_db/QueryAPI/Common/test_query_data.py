from collections import Counter
from typing import List

import numpy as np
import pandas as pd
from src.DataSource.sql_db.QueryAPI.Common import query_table, query_column
from src.DataSource.sql_db.QueryAPI.Common.query_data import TClauseElementLooksLikeBool


def assert_tables_equal(t1, t2):
    """Index doesn't matter so assert same shape and force equal indices"""
    assert isinstance(t1, pd.DataFrame)
    assert isinstance(t2, pd.DataFrame)
    assert t1.shape == t2.shape
    assert Counter(t1.columns) == Counter(t2.columns)
    # indexes don't matter so reset them to ensure that they are same/same
    t1 = t1.sort_values(list(t1.columns)).reset_index(drop=True)
    t2 = t2.sort_values(list(t1.columns)).reset_index(drop=True)
    assert t1.equals(t2.loc[t1.index, t1.columns])
    return True


def assert_columns_equal(c1, c2):
    """Index doesn't matter so assert same shape and force equal indices"""
    assert isinstance(c1, pd.Series)
    assert isinstance(c2, pd.Series)
    assert len(c1.index) == len(c2.index)

    # indexes don't matter so reset them to ensure that they are same/same
    c1 = c1.sort_values(inplace=False).reset_index(drop=True)
    c2 = c2.sort_values(inplace=False).reset_index(drop=True)

    assert c1.name == c2.name
    assert c1.equals(c2)
    return True


def test_query_table_no_filters(generic_session, city_model, city_df):
    print(city_df)
    assert assert_tables_equal(city_df, query_table(generic_session, city_model))


def test_query_table_specify_columns_with_dates(generic_session, city_model, city_df):
    get_col_names = ["city", "state", "last_updated", "population"]
    subset = city_df[get_col_names].copy()
    get_cols = [c for c in city_model.__table__.columns if c.name in get_col_names]

    result = query_table(generic_session, city_model, get_cols=get_cols)
    assert assert_tables_equal(subset, result)

    result = query_table(generic_session, city_model, get_cols=get_col_names)
    assert assert_tables_equal(subset, result)


def test_query_table_specify_columns_no_dates(generic_session, city_model, city_df):
    get_col_names = ["city", "state", "population"]
    subset = city_df[get_col_names].copy()
    get_cols = [c for c in city_model.__table__.columns if c.name in get_col_names]
    result = query_table(generic_session, city_model, get_cols=get_col_names)
    assert assert_tables_equal(subset, result)

    result = query_table(generic_session, city_model, get_cols=get_cols)
    assert assert_tables_equal(subset, result)


def test_query_table_kwargs(generic_session, city_model, city_df):
    city_value = city_df.city[0]
    subset = city_df.loc[city_df.city == city_value, :]
    result = query_table(generic_session, city_model, city=city_value)
    assert assert_tables_equal(subset, result)

    date_value = subset.last_updated[0]
    subset = subset.loc[subset.last_updated == date_value, :]
    result = query_table(
        generic_session, city_model, city=city_value, last_updated=date_value
    )
    assert assert_tables_equal(subset, result)


def test_query_table_filters(generic_session, city_model, city_df):
    population_cutoff = city_df["population"].mean()
    filters: List[TClauseElementLooksLikeBool] = [
        city_model.population > population_cutoff
    ]
    subset = city_df.loc[city_df.population > population_cutoff, :]
    result = query_table(generic_session, city_model, filters=filters)
    assert assert_tables_equal(subset, result)


def test_query_table_all_options(generic_session, city_model, city_df):
    get_col_names = ["city", "state", "last_updated", "population"]
    state_value = "TX"
    population_cutoff = city_df.loc[city_df.state == state_value, "population"].mean()
    filters = [city_model.population > population_cutoff]
    subset = city_df.loc[
        np.logical_and(
            city_df.state == state_value, city_df.population > population_cutoff
        ),
        get_col_names,
    ]
    # noinspection PyTypeChecker
    result = query_table(
        generic_session,
        city_model,
        get_cols=get_col_names,
        filters=filters,
        state=state_value,
    )
    assert assert_tables_equal(subset, result)


def test_query_date_column_no_filters(generic_session, city_model, city_df):
    assert assert_columns_equal(
        city_df.last_updated, query_column(generic_session, city_model.last_updated)
    )


def test_query_not_date_column_no_filters(generic_session, city_model, city_df):
    assert assert_columns_equal(
        city_df.city, query_column(generic_session, city_model.city)
    )


def test_query_column_keyword_filters(generic_session, city_model, city_df):
    assert assert_columns_equal(
        city_df.loc[city_df.state == "TX", "last_updated"],
        query_column(generic_session, city_model.last_updated, state="TX"),
    )


def test_query_column_all_options(generic_session, city_model, city_df):
    population_cutoff = city_df.loc[city_df.state == "AGG", "population"].mean()
    # noinspection PyTypeChecker
    assert assert_columns_equal(
        city_df.loc[
            np.logical_and(
                city_df.state == "TX", city_df.population > population_cutoff
            ),
            "last_updated",
        ],
        query_column(
            generic_session,
            city_model.last_updated,
            filters=[city_model.population > population_cutoff],
            state="TX",
        ),
    )
