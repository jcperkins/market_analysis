from typing import Iterable
import pytest
from sqlalchemy import Column, String, Float, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.attributes import InstrumentedAttribute
from src.DataSource.sql_db.QueryAPI.Common import (
    is_sql_column,
    column_info,
    is_column_like,
    column_list_info,
    format_sql_columns,
)

DbHelperBase = declarative_base()


class DbHelperTable(DbHelperBase):
    __tablename__ = "datetime_table"
    id = Column(Integer, primary_key=True)
    datetime1 = Column(DateTime)
    alpha1 = Column(String)
    numeric1 = Column(Integer)
    numeric2 = Column(Float)


@pytest.fixture
def names_multi():
    return ["datetime1", "alpha1", "numeric1", "numeric2"]


@pytest.fixture
def objects_multi():
    return [
        DbHelperTable.datetime1,
        DbHelperTable.alpha1,
        DbHelperTable.numeric1,
        DbHelperTable.numeric2,
    ]


def test_is_column_like():
    assert is_column_like(["alpha1"]) is False

    assert is_column_like("alpha1")

    element = DbHelperTable.alpha1
    assert isinstance(element, InstrumentedAttribute)
    assert is_column_like(element)

    element = getattr(DbHelperTable.__table__.columns, "alpha1")
    assert isinstance(element, Column)
    assert is_column_like(element)


def test_is_sql_column():
    assert is_sql_column("alpha1") is False

    element = DbHelperTable.alpha1
    assert isinstance(element, InstrumentedAttribute)
    assert is_sql_column(element)

    element = getattr(DbHelperTable.__table__.columns, "alpha1")
    assert isinstance(element, Column)
    assert is_sql_column(element)


class TestColumnInfo:
    def test_column_info_str(self):
        result = column_info("datetime1", DbHelperTable)
        assert (
            isinstance(result[0], InstrumentedAttribute)
            and result[0] == DbHelperTable.datetime1
        )
        assert type(result[1]) == str and result[1] == "datetime1"

    def test_column_info_obj(self):
        result = column_info(DbHelperTable.datetime1, DbHelperTable)
        assert (
            isinstance(result[0], InstrumentedAttribute)
            and result[0] == DbHelperTable.datetime1
        )
        assert type(result[1]) == str and result[1] == "datetime1"

    def test_column_info_obj_no_model(self):
        # noinspection PyArgumentList
        result = column_info(DbHelperTable.datetime1)
        assert (
            isinstance(result[0], InstrumentedAttribute)
            and result[0] == DbHelperTable.datetime1
        )
        assert type(result[1]) == str and result[1] == "datetime1"


class TestColumnListInfo:
    def test_single_string_column_list(self):
        objs, names = column_list_info(["alpha1"], DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == 1 and len(names) == 1
        assert objs == [DbHelperTable.alpha1]
        assert names == ["alpha1"]

    def test_single_obj_column_list(self):
        objs, names = column_list_info([DbHelperTable.alpha1], DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == 1 and len(names) == 1
        assert objs == [DbHelperTable.alpha1]
        assert names == ["alpha1"]

    def test_string_columns(self, names_multi, objects_multi):
        objs, names = column_list_info(names_multi, DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == len(names_multi) and len(names) == len(names_multi)
        assert objs == objects_multi
        assert names == names_multi

    def test_sql_object_columns(self, names_multi, objects_multi):
        objs, names = column_list_info(objects_multi, DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == len(names_multi) and len(names) == len(names_multi)
        assert objs == objects_multi
        assert names == names_multi

    def test_mixed_columns(self, names_multi, objects_multi):
        mixed = [
            names_multi[i] if i % 2 == 0 else objects_multi[i]
            for i in range(len(names_multi))
        ]
        objs, names = column_list_info(mixed, DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == len(names_multi) and len(names) == len(names_multi)
        assert objs == objects_multi
        assert names == names_multi


class TestFormatColumns:
    def test_columns_are_none_returns_none(self):
        objs, names = format_sql_columns(None, DbHelperTable)
        assert objs is None
        assert names is None

    def test_single_column_returns_single_info(self):
        objs, names = format_sql_columns("alpha1", DbHelperTable)
        assert isinstance(objs, InstrumentedAttribute) and isinstance(names, str)
        assert objs == DbHelperTable.alpha1
        assert names == "alpha1"

        objs, names = format_sql_columns(DbHelperTable.alpha1, DbHelperTable)
        assert isinstance(objs, InstrumentedAttribute) and isinstance(names, str)
        assert objs == DbHelperTable.alpha1
        assert names == "alpha1"

    def test_column_collection_returns_collection_info(
        self, names_multi, objects_multi
    ):
        objs, names = format_sql_columns(names_multi, DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == len(names_multi) and len(names) == len(names_multi)
        assert objs == objects_multi
        assert names == names_multi

        objs, names = format_sql_columns(objects_multi, DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == len(names_multi) and len(names) == len(names_multi)
        assert objs == objects_multi
        assert names == names_multi

    def test_column_collection_one_object_returns_collection_info(self):
        objs, names = format_sql_columns(["alpha1"], DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == 1 and len(names) == 1
        assert objs == [DbHelperTable.alpha1]
        assert names == ["alpha1"]

        objs, names = format_sql_columns([DbHelperTable.alpha1], DbHelperTable)
        assert isinstance(objs, Iterable) and isinstance(names, Iterable)
        objs, names = list(objs), list(names)
        assert len(objs) == 1 and len(names) == 1
        assert objs == [DbHelperTable.alpha1]
        assert names == ["alpha1"]
