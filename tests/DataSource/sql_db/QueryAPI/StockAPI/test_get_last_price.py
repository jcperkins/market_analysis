import pandas as pd
from src.DataSource.sql_db.QueryAPI.StockAPI import get_last_price
from src.DataSource.sql_db.Models import StockPrice


def test_get_last_price_no_date(data_session, raw_stock):
    sp = get_last_price(data_session, "AGG")
    assert isinstance(sp, StockPrice)
    assert sp.id == 5
    assert sp.date == raw_stock[4]["date"]


def test_get_last_price_exact_date(data_session, raw_stock):
    ts_id2 = pd.Timestamp(raw_stock[1]["date"])
    sp = get_last_price(data_session, "AGG", ts=ts_id2)
    assert isinstance(sp, StockPrice)
    assert sp.id == 2
    assert sp.date == raw_stock[1]["date"]


def test_get_last_price_lt_date(data_session, raw_stock):
    ts_id3 = pd.Timestamp(raw_stock[2]["date"])
    ts_id4 = pd.Timestamp(raw_stock[3]["date"])
    ts_middle = ts_id3 + (ts_id4 - ts_id3) / 2
    sp = get_last_price(data_session, "AGG", ts=ts_middle)
    assert isinstance(sp, StockPrice)
    assert sp.id == 3
    assert sp.date == raw_stock[2]["date"]
