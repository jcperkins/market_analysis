import pytest

import numpy as np
import pandas as pd

from src.DataSource.sql_db.Models import StockPrice
from src.DataSource.sql_db.QueryAPI.StockAPI import get_price


tckr = "AGG"


@pytest.fixture()
def existing_date(stock_df):
    date = pd.Timestamp(year=2019, month=12, day=6)  # friday
    assert date in pd.DatetimeIndex(stock_df["date"])
    return date


@pytest.fixture()
def missing_date(stock_df):
    date = pd.Timestamp(year=2019, month=12, day=7)  # saturday
    assert date not in pd.DatetimeIndex(stock_df["date"])
    return date


@pytest.fixture()
def existing_date_2(stock_df):
    date = pd.Timestamp(year=2019, month=12, day=9)  # monday
    assert date in pd.DatetimeIndex(stock_df["date"])
    return date


class TestGetPrice:
    def test_adjusted_price_no_dates_sql_filters_return_series_with_all_possible_data(
        self, data_session, stock_df
    ):
        expected = (
            stock_df.loc[stock_df.tckr == tckr, ["date", "adj_close"]]
            .set_index("date")
            .sort_index()
            .fillna(method="ffill")
        )
        unique_dates = expected.index
        ts_cutoff = unique_dates[2]
        expected = expected.loc[expected.index >= ts_cutoff, "adj_close"]

        result = get_price(
            data_session,
            tckr,
            select_dates=None,
            adjusted=True,
            sql_filters=[StockPrice.date >= ts_cutoff],
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_close_price_no_dates_kwarg_filters_return_series_with_all_possible_data(
        self, data_session, stock_df, existing_date
    ):
        expected = (
            stock_df.loc[stock_df.tckr == tckr, ["date", "close"]]
            .set_index("date")
            .sort_index()
            .fillna(method="ffill")
            .loc[[existing_date], "close"]
        )

        result = get_price(
            data_session, tckr, select_dates=None, adjusted=False, date=existing_date
        )
        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_close_price_multiple_non_matching_dates(
        self, data_session, stock_df, existing_date, missing_date, existing_date_2
    ):
        expected = (
            stock_df.loc[stock_df.tckr == tckr, ["date", "close"]]
            .set_index("date")
            .sort_index()
            .fillna(method="ffill")
            .loc[[existing_date, existing_date, existing_date_2], "close"]
        )
        expected.index = pd.DatetimeIndex(
            [existing_date, missing_date, existing_date_2]
        )
        result = get_price(
            data_session,
            tckr,
            select_dates=[existing_date, missing_date, existing_date_2],
            adjusted=False,
        )

        assert isinstance(result, pd.Series)
        assert expected.equals(result)

    def test_adjusted_price_single_exact_date_returns_scalar(
        self, data_session, stock_df, existing_date
    ):
        expected = (
            stock_df.loc[stock_df.tckr == tckr, ["date", "adj_close"]]
            .set_index("date")
            .sort_index()
            .fillna(method="ffill")
            .loc[existing_date, "adj_close"]
        )
        result = get_price(
            data_session, tckr, select_dates=existing_date, adjusted=True
        )
        assert type(result) == float
        assert expected == result

    def test_adjusted_price_single_mismatching_date_returns_previous_scalar(
        self, data_session, stock_df, existing_date, missing_date
    ):
        expected = (
            stock_df.loc[stock_df.tckr == tckr, ["date", "adj_close"]]
            .set_index("date")
            .sort_index()
            .fillna(method="ffill")
            .loc[existing_date, "adj_close"]
        )
        result = get_price(data_session, tckr, select_dates=missing_date, adjusted=True)
        assert type(result) == float
        assert expected == result
