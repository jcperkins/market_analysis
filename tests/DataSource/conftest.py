import datetime
import os
from pathlib import Path
from typing import Dict

import numpy as np
import pandas as pd
import pytest
from src.DataSource import LIBORData, InflationData, StockData
from src.DataSource.Mixins import BaseStorageDBMixin, BaseTempDBMixin, StockTempDBMixin
from src.DataSource.sql_db import set_engine, session_factory
from src.DataSource.sql_db.Models import StockPrice, LIBOR, RateCPI, OptionPrice

# noinspection PyUnresolvedReferences
from .. import context


@pytest.fixture(scope="session")
def raw_stock():
    """ returns a list of 'AGG' quotes with the date property increasing for higher index """
    out = [
        dict(
            tckr="AGG",
            date=datetime.date(2019, 12, 5),
            open=112.580002,
            high=112.739998,
            low=112.529999,
            close=112.68,
            adj_close=192.982882600207,
            volume=4374700.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="AGG",
            date=datetime.date(2019, 12, 6),
            open=112.470001,
            high=112.68,
            low=112.410004,
            close=112.519997,
            adj_close=192.708851359839,
            volume=4648200.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="AGG",
            date=datetime.date(2019, 12, 9),
            open=112.690002,
            high=112.720001,
            low=112.57,
            close=112.580002,
            adj_close=192.811619711547,
            volume=7398300.0,
            dividends=0.246,
            split_factor=1.0,
        ),
        dict(
            tckr="AGG",
            date=datetime.date(2019, 12, 10),
            open=112.669998,
            high=112.669998,
            low=112.489998,
            close=112.540001,
            adj_close=192.743111473289,
            volume=3761100.0,
            dividends=0.0,
            split_factor=2.0,
        ),
        dict(
            tckr="AGG",
            date=datetime.date(2019, 12, 11),
            open=112.669998,
            high=112.900002,
            low=112.610001,
            close=112.839996,
            adj_close=193.256901851933,
            volume=3764500.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="ixSPX",
            date=datetime.date(2019, 12, 5),
            open=3119.209961,
            high=3119.449951,
            low=3103.76001,
            close=3117.429932,
            adj_close=3117.429932,
            volume=3355750000.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="ixSPX",
            date=datetime.date(2019, 12, 6),
            open=3134.620117,
            high=3150.600098,
            low=3134.620117,
            close=3145.909912,
            adj_close=3145.909912,
            volume=3479480000.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="ixSPX",
            date=datetime.date(2019, 12, 9),
            open=3141.860107,
            high=3148.870117,
            low=3135.459961,
            close=3135.959961,
            adj_close=3135.959961,
            volume=3345990000.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="ixSPX",
            date=datetime.date(2019, 12, 10),
            open=3135.360107,
            high=3142.120117,
            low=3126.090088,
            close=3132.52002,
            adj_close=3132.52002,
            volume=3343790000.0,
            dividends=0.0,
            split_factor=2.0,
        ),
        dict(
            tckr="SPY",
            date=datetime.date(2019, 12, 5),
            open=312.230011,
            high=312.25,
            low=310.579987,
            close=312.019989,
            adj_close=512.998833223463,
            volume=40709000.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="SPY",
            date=datetime.date(2019, 12, 6),
            open=314.119995,
            high=315.309998,
            low=314.109985,
            close=314.869995,
            adj_close=517.684589919262,
            volume=48927000.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="SPY",
            date=datetime.date(2019, 12, 9),
            open=314.440002,
            high=315.179993,
            low=313.799988,
            close=313.880005,
            adj_close=516.056925882318,
            volume=34838500.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="SPY",
            date=datetime.date(2019, 12, 10),
            open=313.820007,
            high=314.549988,
            low=312.809998,
            close=313.529999,
            adj_close=515.481473424299,
            volume=52649800.0,
            dividends=0.0,
            split_factor=1.0,
        ),
        dict(
            tckr="SPY",
            date=datetime.date(2019, 12, 11),
            open=314.029999,
            high=314.700012,
            low=313.440002,
            close=314.420013,
            adj_close=516.944764750652,
            volume=53429100.0,
            dividends=0.0,
            split_factor=1.0,
        ),
    ]

    # demonstrate that the dates increase through the list
    for i in range(len(out)):
        if i != 0 and out[i]["tckr"] == out[i - 1]["tckr"]:
            assert out[i]["date"] > out[i - 1]["date"]
    return out


@pytest.fixture(scope="session")
def raw_libor():
    """ returns a list or raw libor rows """
    out = [
        dict(date=datetime.date(2019, 12, 5), duration=1.0, rate=2.37313),
        dict(date=datetime.date(2019, 12, 5), duration=7.0, rate=2.39663),
        dict(date=datetime.date(2019, 12, 5), duration=30.0, rate=2.46713),
        dict(date=datetime.date(2019, 12, 5), duration=60.0, rate=2.50688),
        dict(date=datetime.date(2019, 12, 5), duration=90.0, rate=2.56513),
        dict(date=datetime.date(2019, 12, 5), duration=180.0, rate=2.63850),
        dict(date=datetime.date(2019, 12, 5), duration=365.0, rate=2.73688),
        dict(date=datetime.date(2019, 12, 6), duration=1.0, rate=2.37788),
        dict(date=datetime.date(2019, 12, 6), duration=7.0, rate=2.41088),
        dict(date=datetime.date(2019, 12, 6), duration=30.0, rate=2.46663),
        dict(date=datetime.date(2019, 12, 6), duration=60.0, rate=2.50838),
        dict(date=datetime.date(2019, 12, 6), duration=90.0, rate=2.55988),
        dict(date=datetime.date(2019, 12, 6), duration=180.0, rate=2.61738),
        dict(date=datetime.date(2019, 12, 6), duration=365.0, rate=2.74550),
        dict(date=datetime.date(2019, 12, 7), duration=1.0, rate=2.37788),
        dict(date=datetime.date(2019, 12, 7), duration=7.0, rate=2.41088),
        dict(date=datetime.date(2019, 12, 7), duration=30.0, rate=2.46663),
        dict(date=datetime.date(2019, 12, 7), duration=60.0, rate=2.50838),
        dict(date=datetime.date(2019, 12, 7), duration=90.0, rate=2.55988),
        dict(date=datetime.date(2019, 12, 7), duration=180.0, rate=2.61738),
        dict(date=datetime.date(2019, 12, 7), duration=365.0, rate=2.74550),
    ]
    # demonstrate that the dates increase through the list
    for i in range(len(out)):
        if i != 0:
            assert out[i]["date"] >= out[i - 1]["date"]

    return out


@pytest.fixture(scope="session")
def raw_cpi():
    """ returns a list or raw cpi inflation rows """
    return [
        dict(year=2019, month=11, rate=257.346),
        dict(year=2019, month=12, rate=258.259),
        dict(year=2020, month=1, rate=260.143),
    ]


@pytest.fixture(scope="session")
def raw_options():
    """ returns a list of raw option price rows """
    return [
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213C2720",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 2720.0,
            "stock_price": 3143.43994140625,
            "last": 0.0,
            "change": 0.0,
            "bid": 420.8999938964844,
            "ask": 427.20001220703125,
            "bid_size": 40.0,
            "ask_size": 40.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213P2720",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 2720.0,
            "stock_price": 3143.43994140625,
            "last": 0.10000000149011612,
            "change": -0.15000000596046448,
            "bid": 0.05000000074505806,
            "ask": 0.15000000596046448,
            "bid_size": 1152.0,
            "ask_size": 637.0,
            "volume": 30.0,
            "open_int": 477.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213C2900",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 2900.0,
            "stock_price": 3143.43994140625,
            "last": 217.39999389648438,
            "change": 0.0,
            "bid": 242.60000610351562,
            "ask": 246.0,
            "bid_size": 70.0,
            "ask_size": 70.0,
            "volume": 0.0,
            "open_int": 8.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213P2900",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 2900.0,
            "stock_price": 3143.43994140625,
            "last": 0.3700000047683716,
            "change": -0.4300000071525574,
            "bid": 0.30000001192092896,
            "ask": 0.3499999940395355,
            "bid_size": 278.0,
            "ask_size": 274.0,
            "volume": 149.0,
            "open_int": 5039.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213C3085",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 3085.0,
            "stock_price": 3143.43994140625,
            "last": 64.5,
            "change": 19.899999618530273,
            "bid": 63.599998474121094,
            "ask": 64.80000305175781,
            "bid_size": 39.0,
            "ask_size": 77.0,
            "volume": 12.0,
            "open_int": 323.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW191213P3085",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 3085.0,
            "stock_price": 3143.43994140625,
            "last": 5.260000228881836,
            "change": -7.880000114440918,
            "bid": 5.300000190734863,
            "ask": 5.5,
            "bid_size": 89.0,
            "ask_size": 196.0,
            "volume": 24.0,
            "open_int": 1543.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 7.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117C2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2505.0,
            "stock_price": 3144.14990234375,
            "last": 0.0,
            "change": 0.0,
            "bid": 638.4000244140625,
            "ask": 643.4000244140625,
            "bid_size": 103.0,
            "ask_size": 103.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117P2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2505.0,
            "stock_price": 3144.14990234375,
            "last": 1.690000057220459,
            "change": 0.0,
            "bid": 0.8500000238418579,
            "ask": 0.8999999761581421,
            "bid_size": 169.0,
            "ask_size": 387.0,
            "volume": 0.0,
            "open_int": 160.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117C2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2505.0,
            "stock_price": 3144.360107421875,
            "last": 0.0,
            "change": 0.0,
            "bid": 638.7000122070312,
            "ask": 643.7999877929688,
            "bid_size": 45.0,
            "ask_size": 45.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117P2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2505.0,
            "stock_price": 3144.360107421875,
            "last": 2.25,
            "change": 0.0,
            "bid": 0.8500000238418579,
            "ask": 0.949999988079071,
            "bid_size": 350.0,
            "ask_size": 795.0,
            "volume": 0.0,
            "open_int": 74.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117C2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2750.0,
            "stock_price": 3144.14990234375,
            "last": 350.70001220703125,
            "change": 0.0,
            "bid": 397.0,
            "ask": 402.1000061035156,
            "bid_size": 103.0,
            "ask_size": 103.0,
            "volume": 0.0,
            "open_int": 5552.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117P2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2750.0,
            "stock_price": 3144.14990234375,
            "last": 3.9000000953674316,
            "change": -1.0700000524520874,
            "bid": 3.799999952316284,
            "ask": 3.9000000953674316,
            "bid_size": 137.0,
            "ask_size": 998.0,
            "volume": 23.0,
            "open_int": 31394.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117C2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2750.0,
            "stock_price": 3144.360107421875,
            "last": 276.20001220703125,
            "change": 0.0,
            "bid": 397.3999938964844,
            "ask": 402.3999938964844,
            "bid_size": 45.0,
            "ask_size": 36.0,
            "volume": 0.0,
            "open_int": 10.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117P2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2750.0,
            "stock_price": 3144.360107421875,
            "last": 4.0,
            "change": -1.2799999713897705,
            "bid": 3.799999952316284,
            "ask": 4.0,
            "bid_size": 1326.0,
            "ask_size": 369.0,
            "volume": 22.0,
            "open_int": 595.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117C3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 3000.0,
            "stock_price": 3144.14990234375,
            "last": 165.5,
            "change": 18.5,
            "bid": 164.0,
            "ask": 168.1999969482422,
            "bid_size": 653.0,
            "ask_size": 550.0,
            "volume": 8.0,
            "open_int": 47719.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200117P3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 3000.0,
            "stock_price": 3144.14990234375,
            "last": 19.600000381469727,
            "change": -6.679999828338623,
            "bid": 19.600000381469727,
            "ask": 19.799999237060547,
            "bid_size": 267.0,
            "ask_size": 224.0,
            "volume": 1057.0,
            "open_int": 56634.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117C3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 3000.0,
            "stock_price": 3144.360107421875,
            "last": 165.10000610351562,
            "change": 40.29999923706055,
            "bid": 165.60000610351562,
            "ask": 167.10000610351562,
            "bid_size": 23.0,
            "ask_size": 28.0,
            "volume": 2.0,
            "open_int": 926.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPXW200117P3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 3000.0,
            "stock_price": 3144.360107421875,
            "last": 20.399999618530273,
            "change": -6.139999866485596,
            "bid": 19.899999618530273,
            "ask": 20.100000381469727,
            "bid_size": 33.0,
            "ask_size": 23.0,
            "volume": 12.0,
            "open_int": 1690.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 42.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515C2585",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 2585.0,
            "stock_price": 3144.300048828125,
            "last": 0.0,
            "change": 0.0,
            "bid": 575.5,
            "ask": 581.0999755859375,
            "bid_size": 350.0,
            "ask_size": 250.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515P2585",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 2585.0,
            "stock_price": 3144.300048828125,
            "last": 25.100000381469727,
            "change": 0.0,
            "bid": 21.399999618530273,
            "ask": 21.899999618530273,
            "bid_size": 1171.0,
            "ask_size": 456.0,
            "volume": 0.0,
            "open_int": 8.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515C2805",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 2805.0,
            "stock_price": 3144.300048828125,
            "last": 0.0,
            "change": 0.0,
            "bid": 381.20001220703125,
            "ask": 386.6000061035156,
            "bid_size": 378.0,
            "ask_size": 681.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515P2805",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 2805.0,
            "stock_price": 3144.300048828125,
            "last": 0.0,
            "change": 0.0,
            "bid": 44.599998474121094,
            "ask": 45.20000076293945,
            "bid_size": 182.0,
            "ask_size": 384.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515C3020",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 3020.0,
            "stock_price": 3144.300048828125,
            "last": 202.60000610351562,
            "change": 0.0,
            "bid": 209.6999969482422,
            "ask": 210.89999389648438,
            "bid_size": 47.0,
            "ask_size": 79.0,
            "volume": 0.0,
            "open_int": 1.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-06 09:16:13.895572"),
            "symbol": "-SPX200515P3020",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 3020.0,
            "stock_price": 3144.300048828125,
            "last": 109.0999984741211,
            "change": 0.0,
            "bid": 84.19999694824219,
            "ask": 85.0,
            "bid_size": 206.0,
            "ask_size": 89.0,
            "volume": 0.0,
            "open_int": 45.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 161.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213C2720",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 2720.0,
            "stock_price": 3172.340087890625,
            "last": 0.0,
            "change": 0.0,
            "bid": 446.3999938964844,
            "ask": 450.8999938964844,
            "bid_size": 1.0,
            "ask_size": 2.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213P2720",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 2720.0,
            "stock_price": 3172.340087890625,
            "last": 0.05000000074505806,
            "change": 0.0,
            "bid": 0.0,
            "ask": 0.05000000074505806,
            "bid_size": 0.0,
            "ask_size": 868.0,
            "volume": 0.0,
            "open_int": 1748.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213C2900",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 2900.0,
            "stock_price": 3172.340087890625,
            "last": 247.5,
            "change": 0.0,
            "bid": 266.3999938964844,
            "ask": 270.70001220703125,
            "bid_size": 1.0,
            "ask_size": 2.0,
            "volume": 0.0,
            "open_int": 6.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213P2900",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 2900.0,
            "stock_price": 3172.340087890625,
            "last": 0.05999999865889549,
            "change": -0.03999999910593033,
            "bid": 0.0,
            "ask": 0.05000000074505806,
            "bid_size": 0.0,
            "ask_size": 44.0,
            "volume": 92.0,
            "open_int": 8172.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213C3085",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Call",
            "strike": 3085.0,
            "stock_price": 3172.340087890625,
            "last": 84.41000366210938,
            "change": 27.299999237060547,
            "bid": 81.30000305175781,
            "ask": 86.9000015258789,
            "bid_size": 41.0,
            "ask_size": 30.0,
            "volume": 15.0,
            "open_int": 344.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW191213P3085",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2019-12-13 00:00:00"),
            "option_type": "Put",
            "strike": 3085.0,
            "stock_price": 3172.340087890625,
            "last": 0.4000000059604645,
            "change": -1.0199999809265137,
            "bid": 0.3499999940395355,
            "ask": 0.44999998807907104,
            "bid_size": 692.0,
            "ask_size": 10.0,
            "volume": 423.0,
            "open_int": 3456.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 1.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117C2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2505.0,
            "stock_price": 3175.260009765625,
            "last": 0.0,
            "change": 0.0,
            "bid": 663.4000244140625,
            "ask": 672.2999877929688,
            "bid_size": 100.0,
            "ask_size": 202.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117P2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2505.0,
            "stock_price": 3175.260009765625,
            "last": 1.149999976158142,
            "change": 0.0,
            "bid": 0.699999988079071,
            "ask": 0.800000011920929,
            "bid_size": 1940.0,
            "ask_size": 855.0,
            "volume": 0.0,
            "open_int": 655.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117C2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2505.0,
            "stock_price": 3175.31005859375,
            "last": 0.0,
            "change": 0.0,
            "bid": 664.9000244140625,
            "ask": 671.2000122070312,
            "bid_size": 10.0,
            "ask_size": 40.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117P2505",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2505.0,
            "stock_price": 3175.31005859375,
            "last": 2.25,
            "change": 0.0,
            "bid": 0.699999988079071,
            "ask": 0.800000011920929,
            "bid_size": 865.0,
            "ask_size": 195.0,
            "volume": 0.0,
            "open_int": 74.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117C2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2750.0,
            "stock_price": 3175.260009765625,
            "last": 405.3999938964844,
            "change": 54.70000076293945,
            "bid": 421.0,
            "ask": 429.8999938964844,
            "bid_size": 100.0,
            "ask_size": 202.0,
            "volume": 1.0,
            "open_int": 5552.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117P2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2750.0,
            "stock_price": 3175.260009765625,
            "last": 2.7200000286102295,
            "change": -1.2799999713897705,
            "bid": 2.5999999046325684,
            "ask": 2.799999952316284,
            "bid_size": 2845.0,
            "ask_size": 1967.0,
            "volume": 18.0,
            "open_int": 35296.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117C2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 2750.0,
            "stock_price": 3175.31005859375,
            "last": 276.20001220703125,
            "change": 0.0,
            "bid": 422.5,
            "ask": 429.70001220703125,
            "bid_size": 50.0,
            "ask_size": 59.0,
            "volume": 0.0,
            "open_int": 10.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117P2750",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 2750.0,
            "stock_price": 3175.31005859375,
            "last": 4.099999904632568,
            "change": 0.0,
            "bid": 2.6500000953674316,
            "ask": 2.799999952316284,
            "bid_size": 332.0,
            "ask_size": 171.0,
            "volume": 0.0,
            "open_int": 465.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117C3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 3000.0,
            "stock_price": 3175.260009765625,
            "last": 162.5,
            "change": 0.0,
            "bid": 182.3000030517578,
            "ask": 189.39999389648438,
            "bid_size": 339.0,
            "ask_size": 665.0,
            "volume": 0.0,
            "open_int": 50511.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200117P3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 3000.0,
            "stock_price": 3175.260009765625,
            "last": 12.75,
            "change": -6.239999771118164,
            "bid": 13.199999809265137,
            "ask": 13.5,
            "bid_size": 246.0,
            "ask_size": 806.0,
            "volume": 1554.0,
            "open_int": 61185.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117C3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Call",
            "strike": 3000.0,
            "stock_price": 3175.31005859375,
            "last": 159.89999389648438,
            "change": 0.0,
            "bid": 186.0,
            "ask": 188.3000030517578,
            "bid_size": 5.0,
            "ask_size": 5.0,
            "volume": 0.0,
            "open_int": 925.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPXW200117P3000",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-01-17 00:00:00"),
            "option_type": "Put",
            "strike": 3000.0,
            "stock_price": 3175.31005859375,
            "last": 13.399999618530273,
            "change": -6.5,
            "bid": 13.199999809265137,
            "ask": 13.5,
            "bid_size": 60.0,
            "ask_size": 100.0,
            "volume": 55.0,
            "open_int": 1725.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 36.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515C2585",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 2585.0,
            "stock_price": 3173.320068359375,
            "last": 0.0,
            "change": 0.0,
            "bid": 599.2999877929688,
            "ask": 610.7000122070312,
            "bid_size": 300.0,
            "ask_size": 700.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515P2585",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 2585.0,
            "stock_price": 3173.320068359375,
            "last": 25.100000381469727,
            "change": 0.0,
            "bid": 18.600000381469727,
            "ask": 19.100000381469727,
            "bid_size": 166.0,
            "ask_size": 801.0,
            "volume": 0.0,
            "open_int": 8.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515C2805",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 2805.0,
            "stock_price": 3173.320068359375,
            "last": 0.0,
            "change": 0.0,
            "bid": 404.1000061035156,
            "ask": 413.20001220703125,
            "bid_size": 303.0,
            "ask_size": 454.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515P2805",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 2805.0,
            "stock_price": 3173.320068359375,
            "last": 0.0,
            "change": 0.0,
            "bid": 38.20000076293945,
            "ask": 38.900001525878906,
            "bid_size": 412.0,
            "ask_size": 728.0,
            "volume": 0.0,
            "open_int": 0.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515C3020",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Call",
            "strike": 3020.0,
            "stock_price": 3173.320068359375,
            "last": 202.60000610351562,
            "change": 0.0,
            "bid": 229.0,
            "ask": 230.39999389648438,
            "bid_size": 10.0,
            "ask_size": 10.0,
            "volume": 0.0,
            "open_int": 1.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
        {
            "quote_ts": pd.Timestamp("2019-12-12 09:16:24.976922"),
            "symbol": "-SPX200515P3020",
            "tckr": ".SPX",
            "expiration": pd.Timestamp("2020-05-15 00:00:00"),
            "option_type": "Put",
            "strike": 3020.0,
            "stock_price": 3173.320068359375,
            "last": 109.0999984741211,
            "change": 0.0,
            "bid": 72.9000015258789,
            "ask": 73.80000305175781,
            "bid_size": 100.0,
            "ask_size": 167.0,
            "volume": 0.0,
            "open_int": 45.0,
            "imp_vol": np.NaN,
            "delta": np.NaN,
            "gamma": np.NaN,
            "theta": np.NaN,
            "vega": np.NaN,
            "rho": np.NaN,
            "days_to_expiration": 155.0,
        },
    ]


@pytest.fixture(scope="session")
def data_session(tmpdir_factory, raw_stock, raw_libor, raw_cpi, raw_options):
    fname = (
        Path(tmpdir_factory.mktemp("DataSources"))
        .joinpath("general_data_db.sqlite")
        .resolve()
    )
    print("file path to db used for testing the models: " + str(fname))
    set_engine(fname, echo_parm=False)
    session = session_factory()

    # add the raw records
    for rs in raw_stock:
        session.add(StockPrice(**rs))
    for rl in raw_libor:
        session.add(LIBOR(**rl))
    for ri in raw_cpi:
        session.add(RateCPI(**ri))
    for ro in raw_options:
        session.add(OptionPrice(**ro))

    session.commit()

    return session


@pytest.fixture(scope="session")
def sql_data_engine(data_session):
    return data_session.get_bind()


@pytest.fixture(scope="session")
def stock_df(data_session):
    return pd.read_sql(
        "select * from stock_price",
        con=data_session.get_bind(),
        parse_dates="date",
        index_col=None,
    )


@pytest.fixture(scope="session")
def cpi_df(data_session):
    return pd.read_sql(
        "select * from cpi_rates", con=data_session.get_bind(), index_col=None
    )


@pytest.fixture(scope="session")
def libor_df(data_session):
    return pd.read_sql(
        "select * from libor",
        con=data_session.get_bind(),
        parse_dates="date",
        index_col=None,
    )


@pytest.fixture(scope="session")
def options_df(data_session):
    df = pd.read_sql(
        "select * from options",
        con=data_session.get_bind(),
        parse_dates=["quote_ts", "expiration"],
        index_col=None,
    )
    assert isinstance(df.loc[0, "quote_ts"], pd.Timestamp)
    return df


@pytest.fixture(scope="session")
def agg_data(abs_test_dir: str) -> pd.DataFrame:
    """
    creates a simple pandas dataframe with the price data for SPY
    """
    out = pd.read_csv(
        os.path.join(abs_test_dir, "fixtures", "StorageData", "AGG.csv"),
        index_col=0,
        parse_dates=True,
    )
    return out.loc[~out.index.isnull(), :]


@pytest.fixture(scope="session")
def spy_data(abs_test_dir: str) -> pd.DataFrame:
    """
    creates a simple pandas dataframe with the price data for SPY
    """
    out = pd.read_csv(
        os.path.join(abs_test_dir, "fixtures", "StorageData", "SPY.csv"),
        index_col=0,
        parse_dates=True,
    )
    return out.loc[~out.index.isnull(), :]


@pytest.fixture(scope="session")
def ixSPX_data(abs_test_dir: str) -> pd.DataFrame:
    """
    creates a simple pandas dataframe with the price data for SPY
    """
    out = pd.read_csv(
        os.path.join(abs_test_dir, "fixtures", "StorageData", "ixSPX.csv"),
        index_col=0,
        parse_dates=True,
    )
    return out.loc[~out.index.isnull(), :]


@pytest.fixture(scope="session")
def libor_data(abs_test_dir: str) -> pd.DataFrame:
    """
    creates a simple pandas dataframe with a subset of libor data
    """
    out = pd.read_csv(
        os.path.join(abs_test_dir, "fixtures", "StorageData", "LIBOR.csv"),
        index_col=0,
        parse_dates=True,
    )
    return out.loc[~out.index.isnull(), :]


@pytest.fixture(scope="session")
def inflation_data(abs_test_dir: str) -> pd.DataFrame:
    """
    creates a simple pandas dataframe with a subset of inflation data
    """
    out = pd.read_csv(
        os.path.join(abs_test_dir, "fixtures", "StorageData", "InflationData.csv"),
        index_col=0,
        parse_dates=True,
    )
    return out.loc[~out.index.isnull(), :]


@pytest.fixture(scope="session")
def stock_storage(
    abs_temp_dir: str,
    agg_data: pd.DataFrame,
    spy_data: pd.DataFrame,
    ixSPX_data: pd.DataFrame,
) -> pd.HDFStore:
    """
    creates a simple pd.HDFStore storage object that contains
    the price data for ixSPX, SPY, and AGG
    """
    # use a temp directory in the home directory
    if not os.path.exists(os.path.join(abs_temp_dir, "Data")):
        os.makedirs(os.path.join(abs_temp_dir, "Data"))
    # create the tempStorage object and put the simple DF there
    tempStorage = pd.HDFStore(os.path.join(abs_temp_dir, "Data", "TckrData.h5"))
    tempStorage.open()
    tempStorage.put("AGG", agg_data.copy(), format="table", append=False)
    tempStorage.put("SPY", spy_data.copy(), format="table", append=False)
    tempStorage.put("ixSPX", ixSPX_data.copy(), format="table", append=False)
    tempStorage.close()
    return tempStorage


@pytest.fixture(scope="session")
def libor_storage(abs_temp_dir: str, libor_data: pd.DataFrame) -> pd.HDFStore:
    """
    creates a simple pd.HDFStore storage object that contains
    the libor data
    """
    # use a temp directory in the home directory
    if not os.path.exists(os.path.join(abs_temp_dir, "Data")):
        os.makedirs(os.path.join(abs_temp_dir, "Data"))
    # create the tempStorage object and put the simple DF there
    tempStorage = pd.HDFStore(os.path.join(abs_temp_dir, "Data", "LIBORData.h5"))
    tempStorage.open()
    tempStorage.put("LIBOR", libor_data.copy(), format="table", append=False)
    tempStorage.close()
    return tempStorage


@pytest.fixture(scope="session")
def inflation_storage(abs_temp_dir: str, inflation_data: pd.DataFrame) -> pd.HDFStore:
    """
    creates a simple pd.HDFStore storage object that contains
    the inflation data
    """
    # use a temp directory in the home directory
    if not os.path.exists(os.path.join(abs_temp_dir, "Data")):
        os.makedirs(os.path.join(abs_temp_dir, "Data"))
    # create the tempStorage object and put the simple DF there
    tempStorage = pd.HDFStore(os.path.join(abs_temp_dir, "Data", "InflationData.h5"))
    tempStorage.open()
    tempStorage.put("CPI", inflation_data.copy(), format="table", append=False)
    tempStorage.close()
    return tempStorage


@pytest.fixture(scope="session")
def stock_storage_summary(
    agg_data: pd.DataFrame, spy_data: pd.DataFrame, ixSPX_data: pd.DataFrame
) -> Dict[str, pd.DataFrame]:
    return {"AGG": agg_data.copy(), "SPY": spy_data.copy(), "ixSPX": ixSPX_data.copy()}


@pytest.fixture(scope="session")
def libor_storage_summary(libor_data: pd.DataFrame) -> Dict[str, pd.DataFrame]:
    return {"LIBOR": libor_data.copy()}


@pytest.fixture(scope="session")
def inflation_storage_summary(inflation_data: pd.DataFrame) -> Dict[str, pd.DataFrame]:
    return {"CPI": inflation_data.copy()}


@pytest.fixture(scope="session")
def stock_tempDB(
    agg_data: pd.DataFrame, spy_data: pd.DataFrame, ixSPX_data: pd.DataFrame
) -> StockTempDBMixin:
    tempDB = {}
    for col in agg_data.columns:
        tempDB[col] = pd.concat(
            [
                agg_data[col].copy().rename("AGG"),
                spy_data[col].copy().rename("SPY"),
                ixSPX_data[col].copy().rename("ixSPX"),
            ],
            axis=1,
            join="outer",
            sort=False,
        )
    return StockTempDBMixin(tempDB)


@pytest.fixture(scope="session")
def stock_storageDB(
    abs_temp_dir: str, stock_storage: pd.HDFStore
) -> BaseStorageDBMixin:
    out = BaseStorageDBMixin("TckrData", abs_temp_dir)
    out.default_key = "SPY"
    return out


@pytest.fixture(scope="session")
def libor_tempDB(libor_data: pd.DataFrame) -> BaseTempDBMixin:
    return BaseTempDBMixin(libor_data)


@pytest.fixture(scope="session")
def libor_storageDB(
    abs_temp_dir: str, libor_storage: pd.HDFStore
) -> BaseStorageDBMixin:
    out = BaseStorageDBMixin("LIBORData", abs_temp_dir)
    out.default_key = "LIBOR"
    return out


@pytest.fixture(scope="session")
def inflation_tempDB(inflation_data: pd.DataFrame) -> BaseTempDBMixin:
    return BaseTempDBMixin(inflation_data)


@pytest.fixture(scope="session")
def inflation_storageDB(
    abs_temp_dir: str, inflation_storage: pd.HDFStore
) -> BaseStorageDBMixin:
    out = BaseStorageDBMixin("InflationData", abs_temp_dir)
    out.default_key = "CPI"
    return out


@pytest.fixture(scope="session")
def inflationDB(
    abs_temp_dir: str, inflation_storage: pd.HDFStore, inflation_tempDB: BaseTempDBMixin
) -> BaseStorageDBMixin:
    out = InflationData(abs_temp_dir, False)
    out.tempDB = inflation_tempDB.tempDB
    out.tempdb_cols = inflation_tempDB.tempdb_cols
    return out


@pytest.fixture(scope="session")
def liborDB(
    abs_temp_dir: str, libor_storage: pd.HDFStore, libor_data: BaseTempDBMixin
) -> BaseStorageDBMixin:
    out = LIBORData(abs_temp_dir, False)
    out.tempDB = libor_tempDB.tempDB
    out.tempdb_cols = libor_tempDB.tempdb_cols
    return out


@pytest.fixture(scope="session")
def stockDB(
    abs_temp_dir: str, stock_tempDB: StockTempDBMixin, stock_storage: pd.HDFStore
) -> BaseStorageDBMixin:
    out = StockData(abs_temp_dir, False)
    out.tempDB = stock_tempDB.tempDB
    out.tempdb_cols = stock_tempDB.tempdb_cols
    out.tempdb_keys = stock_tempDB.tempdb_keys
    return out
