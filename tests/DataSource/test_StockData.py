# noinspection PyUnresolvedReferences
import os
from typing import Dict, List, Optional, Tuple, Type, Union, cast

import numpy as np
import pandas as pd
import pytest

# noinspection PyUnresolvedReferences
from .. import context
import src.Utility as Util
from src.DataSource import StockData
from src.DataSource.Mixins import BaseStorageDBMixin, StockTempDBMixin
from src.types import RawDateCollection, RawDateSingle, RawDateType, TorList


class TestStockData(object):
    def test_getPrice(
        self, agg_data: pd.DataFrame, spy_data: pd.DataFrame, stockDB: StockData
    ):
        # existing_date = pd.Timestamp(2017, 11, 10)  # friday
        # missing_date = pd.Timestamp(2017, 11, 11)  # saturday
        #
        # assert isinstance(stockDB.getPrice("SPY", existing_date), float)
        # Util.assert_equal(
        #     stockDB.getPrice("SPY", existing_date, adjusted=False, source="storage"),
        #     spy_data.loc[existing_date, "Close"],
        # )
        # Util.assert_equal(
        #     stockDB.getPrice("SPY", missing_date, adjusted=True, source="tempdb"),
        #     spy_data.loc[existing_date, "AdjClose"],
        # )
        # assert isinstance(stockDB.getPrice("AGG", [existing_date]), pd.Series)
        # Util.assert_equal(
        #     stockDB.getPrice("AGG", [existing_date], adjusted=False, source="storage"),
        #     agg_data.loc[[existing_date], "Close"],
        # )
        # Util.assert_equal(
        #     stockDB.getPrice("AGG", missing_date, adjusted=True, source="tempdb"),
        #     agg_data.loc[[existing_date], "AdjClose"],
        # )
        pass
