# noinspection PyUnresolvedReferences
from typing import Dict, List, Tuple, Union
from collections import Counter

import numpy as np
import pandas as pd
import pytest

# noinspection PyUnresolvedReferences
from ... import context
import src.Utility as Util
from src.DataSource.Mixins import StockTempDBMixin


class TestStockTempDBMixin(object):
    def test_init(
        self,
        stock_tempDB: StockTempDBMixin,
        stock_storage_summary: Dict[str, pd.DataFrame],
    ):
        expected_tckrs = ["AGG", "SPY", "ixSPX"]
        expected_names = [
            "Open",
            "High",
            "Low",
            "Close",
            "AdjClose",
            "Volume",
            "Dividends",
            "SplitFactor",
        ]
        temp_obj = StockTempDBMixin(None)
        assert temp_obj.tempDB is None
        assert isinstance(temp_obj.tempdb_cols, list)
        assert len(temp_obj.tempdb_cols) == 0
        assert isinstance(temp_obj.tempdb_keys, list)
        assert len(temp_obj.tempdb_keys) == 0

        # now test the instance populated with just the data
        temp_obj = StockTempDBMixin(stock_tempDB.tempDB)
        assert id(temp_obj) != id(stock_tempDB)
        assert isinstance(temp_obj.tempDB, dict)
        assert isinstance(temp_obj.tempdb_cols, list)
        assert isinstance(temp_obj.tempdb_keys, list)

        for key in expected_names:
            assert key in stock_tempDB.tempDB
            assert isinstance(temp_obj.tempDB[key], pd.DataFrame)
            for tckr in expected_tckrs:
                assert tckr in temp_obj.tempDB[key].columns
                assert id(temp_obj.tempDB[key]) != id(stock_tempDB.tempDB[key])
                Util.assert_equal(
                    temp_obj.tempDB[key][tckr],
                    stock_tempDB.tempDB[key][tckr],
                    order_matters=False,
                )

        # now test the copy function of the constructor
        temp_obj = StockTempDBMixin(stock_tempDB)
        assert id(temp_obj) != id(stock_tempDB)
        assert isinstance(temp_obj.tempDB, dict)
        assert isinstance(temp_obj.tempdb_cols, list)
        assert isinstance(temp_obj.tempdb_keys, list)

        for key in expected_names:
            assert key in stock_tempDB.tempDB
            assert isinstance(temp_obj.tempDB[key], pd.DataFrame)
            for tckr in expected_tckrs:
                assert tckr in temp_obj.tempDB[key].columns
                assert id(temp_obj.tempDB[key]) != id(stock_tempDB.tempDB[key])
                Util.assert_equal(
                    temp_obj.tempDB[key][tckr],
                    stock_tempDB.tempDB[key][tckr],
                    order_matters=False,
                )

    def test_copy(self, stock_tempDB: StockTempDBMixin):
        temp_obj: StockTempDBMixin = stock_tempDB.copy()
        assert id(temp_obj) != id(stock_tempDB)
        assert isinstance(temp_obj.tempDB, dict)
        assert isinstance(temp_obj.tempdb_cols, list)
        assert isinstance(temp_obj.tempdb_keys, list)

        for key in temp_obj.tempDB.keys():
            assert key in stock_tempDB.tempDB
            assert isinstance(temp_obj.tempDB[key], pd.DataFrame)
            for tckr in temp_obj.tempDB[key].columns:
                assert tckr in temp_obj.tempDB[key].columns
                assert id(temp_obj.tempDB[key]) != id(stock_tempDB.tempDB[key])
                Util.assert_equal(
                    temp_obj.tempDB[key][tckr],
                    stock_tempDB.tempDB[key][tckr],
                    order_matters=False,
                )

    def test_set_db(self, stock_tempDB: StockTempDBMixin):
        # if setting with None should reset the properties to defaults
        # should be empty list if temp_db is None
        temp_obj = StockTempDBMixin(None)
        temp_obj.set_tempDB(stock_tempDB.tempDB)

        assert isinstance(temp_obj.tempDB, dict)
        assert isinstance(temp_obj.tempdb_cols, list)
        assert len(temp_obj.tempdb_cols) == len(stock_tempDB.tempdb_cols)
        assert isinstance(temp_obj.tempdb_keys, list)
        assert len(temp_obj.tempdb_keys) == len(stock_tempDB.tempdb_keys)

        temp_obj.set_tempDB(None)
        assert temp_obj.tempDB is None
        assert temp_obj.tempdb_cols == []
        assert temp_obj.tempdb_keys == []
        return

    def test_get_all_columns(self):
        # should be empty list if temp_db is None
        temp_obj = StockTempDBMixin(None)
        assert temp_obj.get_all_tempDB_columns() == []

        expected_keys = ["a", "b", "c"]
        expected_cols = ["col1", "col2", "col3", "col4", "col5", "col6"]
        fake_db = {
            "a": pd.DataFrame(columns=["col1", "col2", "col3"]),
            "b": pd.DataFrame(columns=["col4", "col5"]),
            "c": pd.DataFrame(columns=["col1", "col2", "col3", "col4", "col5", "col6"]),
        }
        temp_obj = StockTempDBMixin(fake_db)
        assert Counter(temp_obj.get_all_tempDB_columns()) == Counter(expected_cols)
        return

    def test_populate_tempDB_from_dict(
        self,
        stock_tempDB: StockTempDBMixin,
        stock_storage_summary: Dict[str, pd.DataFrame],
    ):
        temp_obj = StockTempDBMixin(None)
        temp_obj.populate_tempDB_from_dict(stock_storage_summary)

        assert isinstance(temp_obj.tempDB, dict)
        assert isinstance(temp_obj.tempdb_cols, list)
        assert len(temp_obj.tempdb_cols) == len(stock_tempDB.tempdb_cols)
        assert isinstance(temp_obj.tempdb_keys, list)
        assert len(temp_obj.tempdb_keys) == len(stock_tempDB.tempdb_keys)

        for key in temp_obj.tempdb_keys:
            assert key in stock_tempDB.tempdb_keys
            assert isinstance(temp_obj.tempDB[key], pd.DataFrame)
            for col in temp_obj.tempDB[key].columns:
                assert col in stock_tempDB.tempdb_cols
        return

    def test_getMinMaxDate_tempDB(
        self,
        agg_data: pd.DataFrame,
        spy_data: pd.DataFrame,
        stock_tempDB: StockTempDBMixin,
    ):
        # use SPY and AGG to test because I have purposely shortened AGG
        agg_min = agg_data.index.min()
        agg_max = agg_data.index.max()
        spy_min = spy_data.index.min()
        spy_max = spy_data.index.max()

        min_date, max_date = stock_tempDB.getMinMaxDate_tempDB()
        assert min_date == agg_min
        assert max_date == agg_max

        min_date, max_date = stock_tempDB.getMinMaxDate_tempDB(dbKey="AGG")
        assert min_date == agg_min
        assert max_date == agg_max

        min_date, max_date = stock_tempDB.getMinMaxDate_tempDB(
            dbKey="AGG", data_name="Close"
        )
        assert min_date == agg_min
        assert max_date == agg_max
        return

        min_date, max_date = stock_tempDB.getMinMaxDate_tempDB(dbKey="SPY")
        assert min_date == spy_min
        assert max_date == spy_max

        min_date, max_date = stock_tempDB.getMinMaxDate_tempDB(
            dbKey="SPY", data_name="Close"
        )
        assert min_date == spy_min
        assert max_date == spy_max
        return

    def test_loadFromDB_TempDB(
        self,
        stock_tempDB: StockTempDBMixin,
        agg_data: pd.DataFrame,
        spy_data: pd.DataFrame,
    ):
        # test basic call with no extra input for sanity check
        Util.assert_equal(
            stock_tempDB.loadFromDB_tempDB(dbKey="AGG"), agg_data, order_matters=False
        )
        Util.assert_equal(
            stock_tempDB.loadFromDB_tempDB(dbKey="SPY"), spy_data, order_matters=False
        )

        # data_names permutations [(arg_value, comparision_value), ..., ]
        data_name_options: List[Tuple[Union[None, str, List[str]], List[str]]] = [
            (None, agg_data.columns.tolist()),  # None
            ("Close", ["Close"]),  # str
            (["Close"], ["Close"]),  # list of 1
            (["Open", "Close"], ["Open", "Close"]),  # list > 1
        ]
        # time permutations [(start, end, comparison_index), ..., ]
        start_ref = agg_data.index[5]
        end_ref = agg_data.index[10]
        time_options = [
            (None, None, agg_data.index),
            (None, end_ref, agg_data.index[:10]),
            (start_ref, None, agg_data.index[5:]),
            (start_ref, end_ref, agg_data.index[5:10]),
            ([start_ref], None, agg_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), None, agg_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), end_ref, agg_data.index[5:6]),
            (agg_data.index[5:10], None, agg_data.index[5:10]),
            (agg_data.index[5:10], end_ref, agg_data.index[5:10]),
        ]

        # test call with a variety of data_names and time specifications
        for (t_start, t_end, t_compare) in time_options:
            for (d_n_arg, d_n_compare) in data_name_options:
                print(t_start, t_end, d_n_arg)
                Util.assert_equal(
                    stock_tempDB.loadFromDB_tempDB(
                        dbKey="AGG", start=t_start, end=t_end, data_names=d_n_arg
                    ),
                    agg_data.loc[t_compare, d_n_compare],
                    order_matters=False,
                )

    def test_getValidDate_tempDB(
        self,
        agg_data: pd.DataFrame,
        spy_data: pd.DataFrame,
        stock_tempDB: StockTempDBMixin,
    ):
        target_date = pd.Timestamp(2019, 2, 1)  # larger than any date in test data
        weekday = pd.Timestamp(
            year=2017, month=11, day=10
        )  # Friday that is already a row
        weekend = pd.Timestamp(year=2017, month=11, day=11)  # not in the db
        # test date that is found
        assert stock_tempDB.getValidDate_tempDB(weekday) == weekday
        assert (
            stock_tempDB.getValidDate_tempDB(weekend, columns=["Open", "Close"])
            == weekday
        )

        # test close boolean and assigning specific dbKey
        assert spy_data.index.max() == stock_tempDB.getValidDate_tempDB(
            target_date, tckr="SPY"
        )
        # test close bool is default and assigning different dbKey
        assert agg_data.index.max() == stock_tempDB.getValidDate_tempDB(
            target_date, tckr="AGG"
        )
        # test the assigning of dbKey when it is Null and assigning a string or list to columns
        default_result = stock_tempDB.getValidDate_tempDB(target_date)
        assert stock_tempDB.getValidDate_tempDB(
            target_date, columns="Close"
        ) == stock_tempDB.getValidDate_tempDB(
            target_date, tckr=stock_tempDB.tempdb_cols[0], columns=["Close"]
        )
        return

    # todo:
    def test_getValidIndices_tempDB(self, agg_data, spy_data, stock_tempDB):
        weekdays = pd.date_range(
            start=pd.Timestamp("11/6/2017"), end=pd.Timestamp("11/10/2017"), freq="1d"
        )
        weekend = pd.DatetimeIndex(
            [pd.Timestamp("11/11/2017"), pd.Timestamp("11/12/2017")]
        )
        spy_only_dates = pd.DatetimeIndex(
            [pd.Timestamp("11/1/2017"), pd.Timestamp("11/2/2017")]
        )
        # test raw_dates set to none
        Util.assert_equal(
            agg_data.index, stock_tempDB.getValidIndices_tempDB(tckr="AGG")
        )
        # test datetimeindex with 0 elements
        Util.assert_equal(
            pd.DatetimeIndex([]),
            stock_tempDB.getValidIndices_tempDB(
                raw_dates=pd.DatetimeIndex([]), tckr="AGG"
            ),
        )
        # test single valid
        Util.assert_equal(
            weekdays[:1],
            stock_tempDB.getValidIndices_tempDB(raw_dates=weekdays[0], tckr="AGG"),
        )
        # test single missing
        Util.assert_equal(
            pd.DatetimeIndex([]),
            stock_tempDB.getValidIndices_tempDB(raw_dates=weekend[0], tckr="AGG"),
        )
        # test with a pull for AGG
        Util.assert_equal(
            weekdays,
            stock_tempDB.getValidIndices_tempDB(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), tckr="AGG"
            ),
        )
        # test with a pull for a different key, SPY
        Util.assert_equal(
            spy_only_dates.append(weekdays),
            stock_tempDB.getValidIndices_tempDB(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), tckr="SPY"
            ),
        )
        # test that an empty dbKey is filled with the default key and that a str or
        # list containing a str is the same
        Util.assert_equal(
            stock_tempDB.getValidIndices_tempDB(
                raw_dates=weekdays.append(weekend), columns="Close"
            ),
            stock_tempDB.getValidIndices_tempDB(
                raw_dates=weekdays.append(weekend),
                tckr=stock_tempDB.tempdb_cols[0],
                columns=["Close"],
            ),
        )
        return

    def test_getValidRows_tempDB(self, agg_data, spy_data, stock_tempDB):
        weekdays = pd.date_range(
            start=pd.Timestamp("11/6/2017"), end=pd.Timestamp("11/10/2017"), freq="1d"
        )
        weekend = pd.DatetimeIndex(
            [pd.Timestamp("11/11/2017"), pd.Timestamp("11/12/2017")]
        )
        spy_only_dates = pd.DatetimeIndex(
            [pd.Timestamp("11/1/2017"), pd.Timestamp("11/2/2017")]
        )
        empty_df = pd.DataFrame(
            columns=stock_tempDB.tempdb_cols, index=pd.DatetimeIndex([])
        )

        # test raw_dates set to none
        Util.assert_equal(agg_data, stock_tempDB.getValidRows_tempDB(tckr="AGG"))
        # test datetimeindex with 0 elements
        Util.assert_equal(
            empty_df,
            stock_tempDB.getValidRows_tempDB(
                raw_dates=pd.DatetimeIndex([]), tckr="AGG"
            ),
        )
        # test single valid
        Util.assert_equal(
            agg_data.loc[weekdays[:1], :],
            stock_tempDB.getValidRows_tempDB(raw_dates=weekdays[0], tckr="AGG"),
        )
        # test single missing
        Util.assert_equal(
            empty_df, stock_tempDB.getValidRows_tempDB(raw_dates=weekend[0], tckr="AGG")
        )
        # test larger range with a AGG as a key
        Util.assert_equal(
            agg_data.loc[weekdays, :],
            stock_tempDB.getValidRows_tempDB(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), tckr="AGG"
            ),
        )
        # test with a different key, SPY
        Util.assert_equal(
            spy_data.loc[spy_only_dates.append(weekdays), :],
            stock_tempDB.getValidRows_tempDB(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), tckr="SPY"
            ),
        )
        # test that an empty tckr is filled with the default key
        Util.assert_equal(
            stock_tempDB.getValidRows_tempDB(raw_dates=weekdays.append(weekend)),
            stock_tempDB.getValidRows_tempDB(
                raw_dates=weekdays.append(weekend), tckr=stock_tempDB.tempdb_cols[0]
            ),
        )
        return

    def test_map_to_index(self, stock_tempDB: StockTempDBMixin):
        in_dates = pd.DatetimeIndex(
            [
                pd.Timestamp(year=2017, month=11, day=6),  # Monday
                pd.Timestamp(year=2017, month=11, day=7),  # Tuesday
                pd.Timestamp(year=2017, month=11, day=8),  # Wednesday
                pd.Timestamp(year=2017, month=11, day=9),  # Thursday
                pd.Timestamp(year=2017, month=11, day=10),  # Friday
                pd.Timestamp(year=2017, month=11, day=11),  # Saturday
                pd.Timestamp(year=2017, month=11, day=12),  # Sunday
            ]
        )
        out_dates = pd.DatetimeIndex(
            [
                pd.Timestamp(year=2017, month=11, day=6),  # Monday
                pd.Timestamp(year=2017, month=11, day=7),  # Tuesday
                pd.Timestamp(year=2017, month=11, day=8),  # Wednesday
                pd.Timestamp(year=2017, month=11, day=9),  # Thursday
                pd.Timestamp(year=2017, month=11, day=10),  # Friday
                pd.Timestamp(year=2017, month=11, day=10),  # Friday
                pd.Timestamp(year=2017, month=11, day=10),  # Friday
            ]
        )
        Util.assert_equal(stock_tempDB.map_to_index(in_dates), out_dates)
        Util.assert_equal(stock_tempDB.map_to_index(in_dates, tckr="AGG"), out_dates)
        Util.assert_equal(stock_tempDB.map_to_index(in_dates, tckr="SPY"), out_dates)
        Util.assert_equal(
            stock_tempDB.map_to_index(in_dates, columns=stock_tempDB.tempdb_keys),
            out_dates,
        )
        return
