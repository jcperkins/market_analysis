# noinspection PyUnresolvedReferences
import numpy as np
import pandas as pd
import pytest
import os
import src.Utility as Util

# noinspection PyUnresolvedReferences
from ... import context
from src.DataSource.Mixins import BaseStorageDBMixin


class TestBaseStorageDBMixin(object):
    def test_init(self, abs_temp_dir, stock_storageDB):
        # stock_storageDB is the most complicated, so use that one
        expected_tckrs = ["AGG", "SPY", "ixSPX"]
        expected_names = [
            "Open",
            "High",
            "Low",
            "Close",
            "AdjClose",
            "Volume",
            "Dividends",
            "SplitFactor",
        ]
        # fresh_initilization
        target_fPath = os.path.join(abs_temp_dir, "Data", "test1_storage_db.h5")
        if os.path.exists(target_fPath):
            os.remove(target_fPath)
        temp_obj = BaseStorageDBMixin("test1_storage_db", abs_temp_dir)
        assert temp_obj.storageName == "test1_storage_db"
        assert temp_obj.mainDir == abs_temp_dir
        assert isinstance(temp_obj.storage, pd.HDFStore)
        assert temp_obj.storage.filename == target_fPath
        assert temp_obj.keyList == []
        assert temp_obj.no_update_keys == []
        assert temp_obj.default_key == ""
        assert temp_obj.storageCols == []
        assert temp_obj.data_columns == []
        assert temp_obj.data_types is None
        assert not temp_obj.storage.is_open

        # existing initilization
        target_fPath = os.path.join(abs_temp_dir, "Data", "TckrData.h5")
        temp_obj = BaseStorageDBMixin("TckrData", abs_temp_dir)
        assert temp_obj.storageName == "TckrData"
        assert temp_obj.mainDir == abs_temp_dir
        assert isinstance(temp_obj.storage, pd.HDFStore)
        assert temp_obj.storage.filename == target_fPath
        print(temp_obj.keyList)
        print([Util.convertTckrToUniversalFormat(x) for x in temp_obj.keyList])
        Util.assert_equal(temp_obj.keyList, expected_tckrs)
        assert temp_obj.no_update_keys == []
        assert temp_obj.default_key in expected_tckrs
        Util.assert_equal(temp_obj.storageCols, expected_names, order_matters=False)
        assert temp_obj.data_columns == []
        assert temp_obj.data_types is None
        assert not temp_obj.storage.is_open

        # now test the copy function of the constructor
        temp_obj = BaseStorageDBMixin(stock_storageDB)
        assert temp_obj.storageName == stock_storageDB.storageName
        assert temp_obj.mainDir == stock_storageDB.mainDir
        assert isinstance(temp_obj.storage, pd.HDFStore)
        assert temp_obj.storage.filename == stock_storageDB.storage.filename
        Util.assert_equal(temp_obj.keyList, stock_storageDB.keyList)
        Util.assert_equal(
            temp_obj.no_update_keys, stock_storageDB.no_update_keys, order_matters=False
        )
        assert temp_obj.default_key == stock_storageDB.default_key
        Util.assert_equal(
            temp_obj.storageCols, stock_storageDB.storageCols, order_matters=False
        )
        Util.assert_equal(
            temp_obj.data_columns, stock_storageDB.data_columns, order_matters=False
        )
        assert temp_obj.data_types is None
        assert not temp_obj.storage.is_open
        assert not stock_storageDB.storage.is_open
        return

    def test_copy(self, stock_storageDB):
        temp_obj: BaseStorageDBMixin = stock_storageDB.copy()
        assert temp_obj.storageName == stock_storageDB.storageName
        assert temp_obj.mainDir == stock_storageDB.mainDir
        assert isinstance(temp_obj.storage, pd.HDFStore)
        assert temp_obj.storage.filename == stock_storageDB.storage.filename
        Util.assert_equal(temp_obj.keyList, stock_storageDB.keyList)
        Util.assert_equal(
            temp_obj.no_update_keys, stock_storageDB.no_update_keys, order_matters=False
        )
        assert temp_obj.default_key == stock_storageDB.default_key
        Util.assert_equal(
            temp_obj.storageCols, stock_storageDB.storageCols, order_matters=False
        )
        Util.assert_equal(
            temp_obj.data_columns, stock_storageDB.data_columns, order_matters=False
        )
        assert temp_obj.data_types is None
        assert not temp_obj.storage.is_open
        return

    def test_getMinMaxDate_storage(self, agg_data, spy_data, stock_storageDB):
        min_date, max_date = stock_storageDB.getMinMaxDate_storage()
        assert min_date == spy_data.index.min()
        assert max_date == spy_data.index.max()

        min_date, max_date = stock_storageDB.getMinMaxDate_storage("SPY")
        assert min_date == spy_data.index.min()
        assert max_date == spy_data.index.max()

        min_date, max_date = stock_storageDB.getMinMaxDate_storage("AGG")
        assert min_date == agg_data.index.min()
        assert max_date == agg_data.index.max()
        assert not stock_storageDB.storage.is_open

        return

    def test_getValidDate_storage(
        self,
        agg_data: pd.DataFrame,
        spy_data: pd.DataFrame,
        stock_storageDB: BaseStorageDBMixin,
        inflation_storageDB: BaseStorageDBMixin,
    ):
        # use stock_storageDB to test different key functionality as it is the most complicated
        target_date = pd.Timestamp("2019/2/1")  # larger than any date in test data
        default_result = stock_storageDB.getValidDate_storage(target_date)
        assert not stock_storageDB.storage.is_open
        # test close boolean and assigning specific dbKey
        assert spy_data.index.max() == stock_storageDB.getValidDate_storage(
            target_date, dbKey="SPY", close_storage=False
        )
        assert stock_storageDB.storage.is_open
        # test close bool is default and assigning different dbKey
        assert agg_data.index.max() == stock_storageDB.getValidDate_storage(
            target_date, dbKey="AGG"
        )
        assert not stock_storageDB.storage.is_open
        # test the assigning of dbKey when it is Null and assigning a string or list to columns
        assert stock_storageDB.getValidDate_storage(
            target_date, columns="Close", close_storage=True
        ) == stock_storageDB.getValidDate_storage(
            target_date,
            dbKey=stock_storageDB.default_key,
            columns=["Close"],
            close_storage=True,
        )
        assert not stock_storageDB.storage.is_open

        # use inflation_storage for date selection logic as it is easier to work with
        # max data in the inflaction test data is 11/15/18
        assert inflation_storageDB.getValidDate_storage(
            pd.Timestamp("2018-10-15")
        ) == pd.Timestamp("2018-10-15")
        assert inflation_storageDB.getValidDate_storage(
            pd.Timestamp("2018-10-29")
        ) == pd.Timestamp("2018-10-15")
        assert inflation_storageDB.getValidDate_storage(
            pd.Timestamp("2018-11-10")
        ) == pd.Timestamp("2018-10-15")
        return

    def test_getValidIndices_storage(self, agg_data, spy_data, stock_storageDB):
        # use stock_storageDB to test different key functionality as it is the most complicated
        weekdays = pd.DatetimeIndex(
            start=pd.Timestamp("11/6/2017"), end=pd.Timestamp("11/10/2017"), freq="1d"
        )
        weekend = pd.DatetimeIndex(
            [pd.Timestamp("11/11/2017"), pd.Timestamp("11/12/2017")]
        )
        spy_only_dates = pd.DatetimeIndex(
            [pd.Timestamp("11/1/2017"), pd.Timestamp("11/2/2017")]
        )

        # test raw_dates set to none
        Util.assert_equal(
            agg_data.index, stock_storageDB.getValidIndices_storage(dbKey="AGG")
        )
        assert not stock_storageDB.storage.is_open
        # test datetimeindex with 0 elements
        Util.assert_equal(
            pd.DatetimeIndex([]),
            stock_storageDB.getValidIndices_storage(
                raw_dates=pd.DatetimeIndex([]), dbKey="AGG"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test single valid
        Util.assert_equal(
            weekdays[:1],
            stock_storageDB.getValidIndices_storage(raw_dates=weekdays[0], dbKey="AGG"),
        )
        assert not stock_storageDB.storage.is_open
        # test single missing
        Util.assert_equal(
            pd.DatetimeIndex([]),
            stock_storageDB.getValidIndices_storage(raw_dates=weekend[0], dbKey="AGG"),
        )
        assert not stock_storageDB.storage.is_open
        # test with a pull for AGG
        Util.assert_equal(
            weekdays,
            stock_storageDB.getValidIndices_storage(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), dbKey="AGG"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test with a pull for a different key, SPY
        Util.assert_equal(
            spy_only_dates.append(weekdays),
            stock_storageDB.getValidIndices_storage(
                raw_dates=spy_only_dates.append(weekdays.append(weekend)), dbKey="SPY"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test that an empty dbKey is filled with the default key and that a str or
        # list containing a str is the same
        Util.assert_equal(
            stock_storageDB.getValidIndices_storage(
                raw_dates=weekdays.append(weekend), columns="Close"
            ),
            stock_storageDB.getValidIndices_storage(
                raw_dates=weekdays.append(weekend),
                dbKey=stock_storageDB.default_key,
                columns=["Close"],
            ),
        )
        assert not stock_storageDB.storage.is_open
        return

    def test_loadFromDB_storage(
        self,
        agg_data: pd.DataFrame,
        spy_data: pd.DataFrame,
        stock_storageDB: BaseStorageDBMixin,
    ):
        assert stock_storageDB.default_key == "SPY"
        # test basic call with no extra input for sanity check
        out = stock_storageDB.loadFromDB_storage()
        Util.assert_equal(out, spy_data, order_matters=False)
        # test basic call with only key specified
        Util.assert_equal(
            stock_storageDB.loadFromDB_storage(dbKey="AGG"),
            agg_data,
            order_matters=False,
        )

        ################# Test Permutations ################
        # data_names permutations [(arg_value, comparision_value), ..., ]
        data_name_options = [
            (None, spy_data.columns),  # None
            ("Close", ["Close"]),  # str
            (["Close"], ["Close"]),  # list of 1
            (["Close", "AdjClose"], ["Close", "AdjClose"]),  # list > 1
        ]
        # time permutations [(start, end, comparison_index), ..., ]
        start_ref = spy_data.index[5]
        end_ref = spy_data.index[10]
        time_options = [
            (None, None, spy_data.index),
            (None, end_ref, spy_data.index[:10]),
            (start_ref, None, spy_data.index[5:]),
            (start_ref, end_ref, spy_data.index[5:10]),
            ([start_ref], None, spy_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), None, spy_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), end_ref, spy_data.index[5:6]),
            (spy_data.index[5:10], None, spy_data.index[5:10]),
            (spy_data.index[5:10], end_ref, spy_data.index[5:10]),
        ]

        # test call with a variety of data_names and time specifications
        for (t_start, t_end, t_compare) in time_options:
            for (d_n_arg, d_n_compare) in data_name_options:
                Util.assert_equal(
                    stock_storageDB.loadFromDB_storage(
                        start=t_start, end=t_end, columns=d_n_arg
                    ),
                    spy_data.loc[t_compare, d_n_compare],
                    order_matters=False,
                )
        return

    def test_getValidRows_storage(self, agg_data, spy_data, stock_storageDB):
        # use stock_storageDB to test different key functionality as it is the most complicated
        weekdays = pd.DatetimeIndex(
            start=pd.Timestamp("11/6/2017"), end=pd.Timestamp("11/10/2017"), freq="1d"
        )
        weekend = pd.DatetimeIndex(
            [pd.Timestamp("11/11/2017"), pd.Timestamp("11/12/2017")]
        )
        spy_only_dates = pd.DatetimeIndex(
            [pd.Timestamp("11/1/2017"), pd.Timestamp("11/2/2017")]
        )
        empty_df = pd.DataFrame(
            columns=stock_storageDB.storageCols, index=pd.DatetimeIndex([])
        )

        # test dates set to none
        Util.assert_equal(agg_data, stock_storageDB.getValidRows_storage(dbKey="AGG"))
        assert not stock_storageDB.storage.is_open
        # test datetimeindex with 0 elements
        Util.assert_equal(
            empty_df,
            stock_storageDB.getValidRows_storage(
                dates=pd.DatetimeIndex([]), dbKey="AGG"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test single valid
        Util.assert_equal(
            agg_data.loc[weekdays[:1], :],
            stock_storageDB.getValidRows_storage(dates=weekdays[0], dbKey="AGG"),
        )
        assert not stock_storageDB.storage.is_open
        # test single missing
        Util.assert_equal(
            empty_df,
            stock_storageDB.getValidRows_storage(dates=weekend[0], dbKey="AGG"),
        )
        assert not stock_storageDB.storage.is_open
        # test larger range with a AGG as a key
        Util.assert_equal(
            agg_data.loc[weekdays, :],
            stock_storageDB.getValidRows_storage(
                dates=spy_only_dates.append(weekdays.append(weekend)), dbKey="AGG"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test with a different key, SPY
        Util.assert_equal(
            spy_data.loc[spy_only_dates.append(weekdays), :],
            stock_storageDB.getValidRows_storage(
                dates=spy_only_dates.append(weekdays.append(weekend)), dbKey="SPY"
            ),
        )
        assert not stock_storageDB.storage.is_open
        # test that an empty dbKey is filled with the default key
        Util.assert_equal(
            stock_storageDB.getValidRows_storage(dates=weekdays.append(weekend)),
            stock_storageDB.getValidRows_storage(
                dates=weekdays.append(weekend), dbKey=stock_storageDB.default_key
            ),
        )
        assert not stock_storageDB.storage.is_open
        return

    def test_map_to_index(self, inflation_storageDB):
        in_dates = pd.DatetimeIndex(
            [
                pd.Timestamp("2018-10-15"),
                pd.Timestamp("2018-10-30"),
                pd.Timestamp("2018-11-10"),
                pd.Timestamp("2018-11-30"),
            ]
        )
        out_dates = pd.DatetimeIndex(
            [
                pd.Timestamp("2018-10-15"),
                pd.Timestamp("2018-10-15"),
                pd.Timestamp("2018-10-15"),
                pd.Timestamp("2018-11-15"),
            ]
        )
        Util.assert_equal(inflation_storageDB.map_to_index(in_dates), out_dates)
        Util.assert_equal(
            inflation_storageDB.map_to_index(
                in_dates, columns=inflation_storageDB.storageCols
            ),
            out_dates,
        )
        return

    def test_subset_summary(
        self,
        stock_storageDB,
        stock_storage_summary,
        libor_storageDB,
        libor_storage_summary,
        inflation_storageDB,
        inflation_storage_summary,
    ):
        Util.assert_equal(stock_storageDB.subset_summary(), stock_storage_summary)
        Util.assert_equal(libor_storageDB.subset_summary(), libor_storage_summary)
        Util.assert_equal(
            inflation_storageDB.subset_summary(), inflation_storage_summary
        )
        return
