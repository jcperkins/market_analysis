# noinspection PyUnresolvedReferences
import numpy as np
import pandas as pd
import pytest
import src.Utility as Util

# noinspection PyUnresolvedReferences
from ... import context
from src.DataSource.Mixins import BaseTempDBMixin


class TestBaseTempDBMixin(object):
    def test_init(
        self,
        libor_data,
        libor_tempDB,
        libor_storage_summary,
        inflation_data,
        inflation_tempDB,
        inflation_storage_summary,
    ):
        libor_cols = ["1d", "1w", "1m", "2m", "3m", "6m", "1yr"]
        inflation_cols = ["Year", "Month", "CPI"]
        temp_obj = BaseTempDBMixin(None)
        assert temp_obj.tempDB is None
        assert isinstance(temp_obj.tempdb_cols, list)
        assert len(temp_obj.tempdb_cols) == 0

        # now test the instance populated with just the data
        temp_obj = BaseTempDBMixin(libor_data)
        assert not Util.is_same(temp_obj.tempDB, libor_data)
        Util.assert_equal(temp_obj.tempDB, libor_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, libor_cols, order_matters=False)

        temp_obj = BaseTempDBMixin(inflation_data)
        assert not Util.is_same(temp_obj.tempDB, inflation_data)
        Util.assert_equal(temp_obj.tempDB, inflation_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, inflation_cols, order_matters=False)

        # now test the instance populated with just a storage summary
        temp_obj = BaseTempDBMixin(libor_storage_summary)
        assert not Util.is_same(temp_obj.tempDB, libor_storage_summary["LIBOR"])
        Util.assert_equal(temp_obj.tempDB, libor_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, libor_cols, order_matters=False)

        temp_obj = BaseTempDBMixin(inflation_storage_summary)
        assert not Util.is_same(temp_obj.tempDB, inflation_storage_summary["CPI"])
        Util.assert_equal(temp_obj.tempDB, inflation_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, inflation_cols, order_matters=False)

        # now test the copy function of the constructor
        temp_obj = BaseTempDBMixin(libor_tempDB)
        assert not Util.is_same(temp_obj, libor_tempDB)
        assert not Util.is_same(temp_obj.tempDB, libor_tempDB.tempdb_cols)
        assert not Util.is_same(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempDB, libor_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, libor_cols, order_matters=False)

        temp_obj = BaseTempDBMixin(inflation_tempDB)
        assert not Util.is_same(temp_obj.tempDB, inflation_tempDB)
        assert not Util.is_same(temp_obj.tempDB, inflation_tempDB.tempdb_cols)
        assert not Util.is_same(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempDB, inflation_data, order_matters=False)
        Util.assert_equal(temp_obj.tempdb_cols, inflation_cols, order_matters=False)
        return

    def test_copy(self, libor_tempDB, inflation_tempDB):
        temp_obj = libor_tempDB.copy()
        assert not Util.is_same(temp_obj, libor_tempDB)
        assert not Util.is_same(temp_obj.tempDB, libor_tempDB.tempDB)
        Util.assert_equal(temp_obj.tempDB, libor_tempDB.tempDB, order_matters=False)
        assert not Util.is_same(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)
        Util.assert_equal(
            temp_obj.tempdb_cols, libor_tempDB.tempdb_cols, order_matters=False
        )

        temp_obj = inflation_tempDB.copy()
        assert not Util.is_same(temp_obj, inflation_tempDB)
        assert not Util.is_same(temp_obj.tempDB, inflation_tempDB.tempDB)
        Util.assert_equal(temp_obj.tempDB, inflation_tempDB.tempDB, order_matters=False)
        assert not Util.is_same(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        Util.assert_equal(
            temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols, order_matters=False
        )
        return

    def test_set_db(self, libor_tempDB, inflation_tempDB):
        temp_obj = BaseTempDBMixin(None)
        temp_obj.set_tempDB(libor_tempDB.tempDB)
        assert not Util.is_same(temp_obj, libor_tempDB)
        assert not Util.is_same(temp_obj.tempDB, libor_tempDB.tempDB)
        Util.assert_equal(temp_obj.tempDB, libor_tempDB.tempDB)
        assert not Util.is_same(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)

        temp_obj = BaseTempDBMixin(None)
        temp_obj.set_tempDB(inflation_tempDB.tempDB)
        assert not Util.is_same(temp_obj, inflation_tempDB)
        assert not Util.is_same(temp_obj.tempDB, inflation_tempDB.tempDB)
        Util.assert_equal(temp_obj.tempDB, inflation_tempDB.tempDB)
        assert not Util.is_same(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        return

    def test_populate_tempDB_from_dict(
        self,
        libor_tempDB,
        libor_storage_summary,
        inflation_tempDB,
        inflation_storage_summary,
    ):
        temp_obj = BaseTempDBMixin(None)
        temp_obj.populate_tempDB_from_dict(libor_storage_summary)
        assert not Util.is_same(temp_obj, libor_tempDB)
        assert not Util.is_same(temp_obj.tempDB, libor_storage_summary["LIBOR"])
        Util.assert_equal(temp_obj.tempDB, libor_tempDB.tempDB)
        assert not Util.is_same(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempdb_cols, libor_tempDB.tempdb_cols)

        temp_obj = BaseTempDBMixin(None)
        temp_obj.populate_tempDB_from_dict(inflation_storage_summary)
        assert not Util.is_same(temp_obj, inflation_tempDB)
        assert not Util.is_same(temp_obj.tempDB, inflation_storage_summary["CPI"])
        Util.assert_equal(temp_obj.tempDB, inflation_tempDB.tempDB)
        assert not Util.is_same(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        Util.assert_equal(temp_obj.tempdb_cols, inflation_tempDB.tempdb_cols)
        return

    def test_getMinMaxDate_tempDB(
        self, libor_data, libor_tempDB, inflation_data, inflation_tempDB
    ):
        min_date, max_date = libor_tempDB.getMinMaxDate_tempDB()
        assert min_date == libor_data.index.min()
        assert max_date == libor_data.index.max()

        min_date, max_date = inflation_tempDB.getMinMaxDate_tempDB()
        assert min_date == inflation_data.index.min()
        assert max_date == inflation_data.index.max()
        return

    def test_loadFromDB_TempDB(
        self, libor_tempDB, inflation_tempDB, libor_data, inflation_data
    ):
        # test basic call with no extra input for sanity check
        Util.assert_equal(
            libor_tempDB.loadFromDB_tempDB(), libor_data, order_matters=False
        )
        Util.assert_equal(
            inflation_tempDB.loadFromDB_tempDB(), inflation_data, order_matters=False
        )

        ################# Test Libor Data ################
        # data_names permutations [(arg_value, comparision_value), ..., ]
        data_name_options = [
            (None, libor_data.columns),  # None
            ("1d", ["1d"]),  # str
            (["1d"], ["1d"]),  # list of 1
            (["1d", "1w"], ["1d", "1w"]),  # list > 1
        ]
        # time permutations [(start, end, comparison_index), ..., ]
        start_ref = libor_data.index[5]
        end_ref = libor_data.index[10]
        time_options = [
            (None, None, libor_data.index),
            (None, end_ref, libor_data.index[:10]),
            (start_ref, None, libor_data.index[5:]),
            (start_ref, end_ref, libor_data.index[5:10]),
            ([start_ref], None, libor_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), None, libor_data.index[5:6]),
            (pd.DatetimeIndex([start_ref]), end_ref, libor_data.index[5:6]),
            (libor_data.index[5:10], None, libor_data.index[5:10]),
            (libor_data.index[5:10], end_ref, libor_data.index[5:10]),
        ]

        # test call with a variety of data_names and time specifications
        for (t_start, t_end, t_compare) in time_options:
            for (d_n_arg, d_n_compare) in data_name_options:
                Util.assert_equal(
                    libor_tempDB.loadFromDB_tempDB(
                        start=t_start, end=t_end, columns=d_n_arg
                    ),
                    libor_data.loc[t_compare, d_n_compare],
                    order_matters=False,
                )
        return

    def test_getValidDate_tempDB(self, inflation_tempDB):
        # max data in the inflaction test data is 11/15/18
        assert inflation_tempDB.getValidDate_tempDB(
            pd.Timestamp("2018-10-15")
        ) == pd.Timestamp("2018-10-15")
        assert inflation_tempDB.getValidDate_tempDB(
            pd.Timestamp("2018-10-29")
        ) == pd.Timestamp("2018-10-15")
        assert inflation_tempDB.getValidDate_tempDB(
            pd.Timestamp("2018-11-10")
        ) == pd.Timestamp("2018-10-15")
        return

    def test_getValidIndices_tempDB(self, inflation_tempDB, inflation_data):
        # return the whole index if raw_dates and columns are none
        Util.assert_equal(
            inflation_tempDB.getValidIndices_tempDB(), inflation_data.index
        )
        # return the subset of dates that are valid records in the df index as a DatetimeIndex
        Util.assert_equal(
            inflation_tempDB.getValidIndices_tempDB(
                pd.DatetimeIndex(
                    [
                        pd.Timestamp("2018-10-15"),
                        pd.Timestamp("2018-10-30"),
                        pd.Timestamp("2018-11-10"),
                        pd.Timestamp("2018-11-30"),
                    ]
                )
            ),
            pd.DatetimeIndex([pd.Timestamp("2018-10-15")]),
        )
        return

    def test_getValidRows_tempDB(self, inflation_data, inflation_tempDB):
        target_date = pd.Timestamp("2018-10-15")
        # should return an empty dataframe if no dates match
        out = inflation_tempDB.getValidRows_tempDB(
            [
                pd.Timestamp("2018-10-14"),
                pd.Timestamp("2018-10-30"),
                pd.Timestamp("2018-11-10"),
                pd.Timestamp("2018-11-30"),
            ]
        )
        assert isinstance(out, pd.DataFrame)
        Util.assert_iterable_equal(
            list(out.columns), ["Year", "Month", "CPI"], order_matters=False
        )
        assert len(out.index) == 0

        # should always return a df
        out = inflation_tempDB.getValidRows_tempDB([target_date])
        assert isinstance(out, pd.DataFrame)
        Util.assert_equal(
            out, inflation_data.loc[[target_date], :], order_matters=False
        )
        out = inflation_tempDB.getValidRows_tempDB(target_date)
        print(type(out))
        assert isinstance(out, pd.DataFrame)
        Util.assert_equal(out, inflation_data.loc[[target_date], :])
        return

    def test_map_to_index(self, inflation_tempDB):
        Util.assert_equal(
            inflation_tempDB.map_to_index(
                pd.DatetimeIndex(
                    [
                        pd.Timestamp("2018-10-15"),
                        pd.Timestamp("2018-10-30"),
                        pd.Timestamp("2018-11-10"),
                        pd.Timestamp("2018-11-30"),
                    ]
                )
            ),
            pd.DatetimeIndex(
                [
                    pd.Timestamp("2018-10-15"),
                    pd.Timestamp("2018-10-15"),
                    pd.Timestamp("2018-10-15"),
                    pd.Timestamp("2018-11-15"),
                ]
            ),
        )
        return
