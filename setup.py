#!/usr/bin/env python
# coding: utf-8
import os
import sys
from setuptools import setup, find_packages


# ----------------- Custom Commands -----------------------------
# clean all __pycache__ folders from project
if sys.argv[-1] == "clean":
    import pathlib

    for p in pathlib.Path(".").rglob("__pycache__"):
        print("Removing " + str(p.resolve()))
        for f in p.iterdir():
            print(f"\tDel {f.name}")
            f.unlink()
        p.rmdir()
    sys.exit()

# create a custom format command to track all the directories
# that need formatting
if sys.argv[-1] == "format":
    # exclude_str = (r"(\.eggs|\.git|\.hg|\.mypy_cache|\.tox|\.venv|_build|buck-out|build|dist|\.idea" +
    #                r"|\.vscode|\.vs|\.mypy_cache|\.pytest_cache|Data|te?mp|v?envs?)")
    exclude = [
        "\.eggs",
        "\.git",
        "\.hg",
        "\.mypy_cache",
        "\.tox",
        "\.venv",
        "_build",
        "buck-out",
        "build",
        "dist",
        "\.idea",
        "\.vscode",
        "\.vs",
        "\.mypy_cache",
        "\.pytest_cache",
        "Data",
        "te?mp",
        "v?envs?",
    ]
    command = f'black . --exclude="({"|".join(exclude)})"'
    print(f"running command: {command}")
    os.system(command)
    sys.exit()

# ----------------- End Custom Commands -------------------------

install_requires = [
    "ffn",
    "lxml",
    "matplotlib",
    # "numba",
    "numpy",
    "pandas",
    # "pytables",
    "requests",
    "scipy",
    "selenium",
    "sqlalchemy",
]

dev_requires = ["black", "mypy", "pytest-cov", "rope", "seaborn"]


setup(
    name="MarketAnalysis",
    description="Analysis and study of the US markets using python",
    author="Cory Perkins",
    author_email="jcperkins12@gmail.com",
    version="0.0.1",
    url="https://jcperkins@bitbucket.org/jcperkins/market_analysis",
    license="MIT",
    packages=find_packages(),
    zip_safe=True,
    classifiers=["Programming Language :: Python :: 3.7"],
    install_requires=install_requires,
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "tables"],
    extras_require={"dev": dev_requires},
)

# to run mypy on the whole directory, use
# mypy --config-file mypy.ini src/ --html-report .\MyPyReports
# to run on a single fle use something like... (the -m or -p args can specify modules and packages)
# mypy --config-file mypy.ini scripts/MarketSummary.py --html-report \MyPyReports

# todo: Utility format_timedeltaindex and format_timedelta and format_timedeltainput tests
# todo: Util.daily_volatility and annualized_volatility tests
# todo: Prediction Model Volatility30DayMixin and populate_volatility_df tests
# todo: Prediction Model NormDist and GenericDistribution tests
# todo: Get Option pulls working!
