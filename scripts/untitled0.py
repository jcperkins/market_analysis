# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 09:30:09 2017

@author: a0225347
"""

import numpy as np
from scipy import signal as sig
from matplotlib import pyplot as plt

if __name__ == "__main__":
    # Signal and analysis parameters
    fs = 4000.0  # Sampling frequency
    T = 2  # Duration of a signal
    f0 = 100  # Fundamental frequency
    A = 1  # Sine amplitude
    # th = np.pi/4   # Phase shift in radians (45 degrees)
    th = 0

    # Time vector
    t = np.arange(0, T * fs) / fs

    # Generate the signal with higher harmonics
    x = (
        A * np.sin(2 * np.pi * f0 * t + th)
        + A * np.sin(2 * np.pi * 2 * f0 * t + 2 * th) / 8
        + A * np.sin(2 * np.pi * 3 * f0 * t + 3 * th) / 16
    )

    # Add some noise
    x += 0.5 * np.random.randn(x.size)

    # Number of all samples
    N = x.size

    # Apply the window
    win = sig.gaussian(N, N)
    x = x * win

    # Perform the DFT
    X = np.fft.fft(x)
    # Get the frequency vector
    freq = np.linspace(0, fs - 1 / fs, N)

    # Get the phase and magnitude
    Xmag = np.abs(X) / np.sum(win) * 2
    Xph = np.angle(X)

    # Magnitude in logarithmic scale
    Xmagl = 10 * np.log10(Xmag / Xmag.max())

    # Find the amplitude of the dominant frequency
    ind_max = np.argmax(Xmag[0 : int(np.ceil(N / 2))])

    # Extract the amplitude ...
    A_ = Xmag[ind_max]
    # ... frequency ..
    f0_ = freq[ind_max]
    # ... phase
    th_ = Xph[ind_max] + np.pi / 2

    # Reconstruct the signal
    x_ = A_ * np.sin(2 * np.pi * f0_ * t + th_)

    # Do some plotting
    plt.subplot(311)
    plt.plot(t, x)
    plt.grid(True)
    plt.hold(True)
    plt.plot(t, x_, "r")
    plt.title("Time domain signal")

    plt.subplot(312)
    plt.plot(freq[0 : N / 2], Xmagl[0 : N / 2])
    plt.grid(True)
    plt.title("Magnitude")

    plt.subplot(313)
    plt.plot(freq[0 : N / 2], Xph[0 : N / 2])
    plt.grid(True)
    plt.title("Phase")
    plt.show()
