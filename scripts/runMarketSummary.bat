rem **** convert prompt to conda prompt
call C:\Users\cp035982\bin\Continuum\anaconda3\Scripts\activate.bat C:\Users\cp035982\bin\Continuum\anaconda3\

rem **** move to project directory
call cd "C:\Users\cp035982\Python\Projects\MarketAnalysis"

rem **** activate environment
call conda activate Markets

rem **** Run the python code that will read and store the SPY and ^SPX option data
python "C:\Users\cp035982\Python\Projects\MarketAnalysis\scripts\MarketSummary.py"
