import datetime
import os
from pathlib import Path
from typing import List, Optional

# noinspection PyUnresolvedReferences,PyPackageRequirements
import context
import numpy as np
import pandas as pd
import src.Utility as Util
from config import default_settings
from matplotlib import pyplot as plt
from src.DataSource import DataTools, StockData, OptionData
from src.DataSource.sql_db import set_engine, session_factory

# from src.DataSource.sql_db.Models import OptionPrice
from src.DataSource.sql_db.UpdateFunctions import mine_option_data
from src.DataSource.Web.selenium_driver_utility import get_driver
from src.Model import getDefaultPredictionModel_HDF5, BaseModel_HDF5


def run_market_summary(
    run_datamine: bool = True, update_databases: bool = True, work_proxy: bool = False
):
    # txt = input("Is it a good time to collect options data? [y/n]").upper()
    # if "Y" in txt:
    # noinspection PyArgumentList
    today = pd.Timestamp.today()

    set_engine(
        Path(default_settings.data_dir).joinpath("MarketData.sqlite"), echo_parm=False
    )
    db_session = session_factory()

    sDB, oDB, lDB, iDB = (
        DataTools.updateAllDBs(db_session, workProxy=work_proxy)
        if update_databases
        else DataTools.initiateAllDataBases(workProxy=work_proxy)
    )

    if run_datamine:
        fid_data = mine_option_data(
            driver=get_driver(),
            db_session=db_session,
            symbols=[".SPX", "SPY"],
            return_data=True,
            load_batches=True,
            keep_driver=False,
        )

        # oDB.mineFidelityOptions(['.SPX', 'SPY', 'IVV'], loadInBatches=True)
        # data_dict = oDB.mineFidelityOptions(
        #     [".SPX", "SPY"], loadInBatches=True, returnData=True
        # )
        # for df in data_dict.values():
        #     OptionPrice.upload_df(df=df, session=db_session, commit=True, debug=True)
        # fid_data: pd.DataFrame = oDB.options_for_date("ixSPX", today, method="closest")
        fid_data = fid_data.drop_duplicates(subset="Symbol")

        current_price: float = fid_data.stock_price.values[0]
        # query_date: pd.Timestamp = fid_data.QueryTimeStamp.iloc[0]

        # TODO: SQL Conversion
        prediction_model = getDefaultPredictionModel_HDF5(
            workProxy=work_proxy, stockDB=sDB
        )
        daily_dir: str = os.path.join(
            default_settings.data_dir, today.strftime("%Y_%m_%d")
        )
        if not os.path.exists(daily_dir):
            os.makedirs(daily_dir)
        # analyze SPX trend
        analyze_trend(
            sDB,
            predictionModel=prediction_model,
            tckr="ixSPX",
            work_proxy=work_proxy,
            save_dir=daily_dir,
            today=today,
            current_price=current_price,
        )

    # FIXME: oDB.calculate_best_strategies() doesn't work with numba.jit and takes too long
    #  to run without JIT speedup
    # # calculate different option strategies
    # option_strategies: pd.DataFrame = oDB.calculate_best_strategies(
    #     "ixSPX", today, prediction_model=prediction_model, fid_data=fid_data
    # )
    #
    # # summarize option plays for the day
    # unique_strategies = summarize_option_plays(
    #     oDB, option_strategies, today=today, save_dir=daily_dir
    # )
    #
    # return champion_option_strategies(
    #     option_strategies=option_strategies,
    #     unique_strategies=unique_strategies,
    #     today=today,
    #     save_dir=daily_dir,
    # )
    return None


# analyze SPX trend
def analyze_trend(
    stockDB: StockData,
    predictionModel: Optional[BaseModel_HDF5] = None,
    save_dir: Optional[str] = None,
    today: Optional[pd.Timestamp] = None,
    current_price: Optional[float] = None,
    tckr: str = "ixSPX",
    work_proxy: bool = False,
):
    if today is None:
        # noinspection PyArgumentList
        today = pd.Timestamp.today()
    if current_price is None:
        current_price = stockDB.getPrice(tckr, today, adjusted=False)
    if save_dir is None:
        save_dir = os.path.join(default_settings.data_dir, today.strftime("%Y_%m_%d"))
    if predictionModel is None:
        predictionModel = getDefaultPredictionModel_HDF5(
            workProxy=work_proxy, stockDB=stockDB
        )
    days_out_list: List[int] = [7, 14, 30, 60, 180, 365]
    # colors: List[str] = ["k", "b", "r", "g", "c"]
    historical_days_back: List[int] = [3, 7, 14, 30, 60, 90, 180, 365]
    start_date = Util.formatDateInput(datetime.datetime.today().date())
    end_date_list = pd.DatetimeIndex(
        [start_date + pd.Timedelta(days=d) for d in days_out_list]
    )
    model_start: float = stockDB.getPrice(
        tckr, start_date, adjusted=False, source="storage"
    )
    # model_start: float = predictionModel.get_current_price(start_date, tckr)[0]
    fig, ax = plt.subplots(1, 2)
    # logic for SPX prediction summary
    # prediction moving forward
    plt.sca(ax[0])
    forward_report = None
    future_price = np.array(
        [
            predictionModel.predict_mean(start_date, predicted_date=end_date, tckr=tckr)
            for end_date in end_date_list
        ]
    )
    future_std = np.array(
        [
            predictionModel.predict_volatility(
                start_date, predicted_date=end_date, tckr=tckr, unit="Dollar"
            )
            for end_date in end_date_list
        ]
    )
    report: pd.DataFrame = pd.DataFrame(
        data=zip(
            future_price,
            future_std,
            future_price + future_std,
            future_price - future_std,
        ),
        columns=[
            "Predict_Price",
            "Predict_Std",
            "Predict_Price+Std",
            "Predict_Price-Std",
        ],
    )
    report["Start_Date"] = start_date
    report["Final_Date"] = end_date_list
    report["Days_out"] = days_out_list
    report["Start_Price"] = current_price
    report["Predict_Gain"] = report.Predict_Price / report.Start_Price
    plt.plot(
        report.Days_out,
        report.Predict_Price,
        "b-o",
        report.Days_out,
        report["Predict_Price+Std"],
        "b--",
        report.Days_out,
        report["Predict_Price-Std"],
        "b--",
    )
    plt.xlabel("Prediction Days Out")
    plt.ylabel("Predicted Price $")
    plt.title("Future Price Prediction")
    if forward_report is None:
        forward_report = report
    else:
        forward_report = forward_report.append(report, ignore_index=True)
    report_name = "ModelPredictions.csv"
    forward_report.to_csv(os.path.join(save_dir, report_name))
    # test historical accuracy
    plt.sca(ax[1])
    backward_report = None
    end_date = start_date
    final_price = current_price
    start_date_list = pd.DatetimeIndex(
        [start_date - pd.Timedelta(days=lag) for lag in historical_days_back]
    )
    start_price = stockDB.getPrice(
        tckr, start_date_list, adjusted=False, source="storage"
    ).values
    # start_price = np.array(
    #     [predictionModel.get_current_price(start, tckr) for start in start_date_list]
    # )
    future_price = np.array(
        [
            predictionModel.predict_mean(
                start,
                end_date,
                tckr=tckr,
                model_source="storage",
                tckr_source="storage",
            )
            for start in start_date_list
        ]
    )
    future_std = np.array(
        [
            predictionModel.predict_volatility(
                start,
                end_date,
                tckr=tckr,
                unit="Dollar",
                model_source="storage",
                tckr_source="storage",
            )
            for start in start_date_list
        ]
    )
    report = pd.DataFrame(
        data=zip(
            np.array((end_date - start_date_list).days, dtype=int),
            future_price,
            future_std,
            future_price + future_std,
            future_price - future_std,
        ),
        columns=[
            "Days_out",
            "Predict_Price",
            "Predict_Std",
            "Predict_Price+Std",
            "Predict_Price-Std",
        ],
    )
    report["Start_Date"] = start_date_list
    report["Final_Date"] = end_date
    report["Start_Price"] = start_price
    report["Final_Price"] = final_price
    report["Predict_Gain"] = report.Predict_Price / report.Start_Price
    report["Actual_Gain"] = final_price / start_price
    color_list: List[str] = [
        plt.get_cmap("winter")(i_c)
        for i_c in np.linspace(0, 1, len(historical_days_back))
    ]
    upper_tick = (report.Predict_Price + report.Predict_Std) / report.Start_Price
    lower_tick = (report.Predict_Price - report.Predict_Std) / report.Start_Price
    gain_lines: List[plt.plot] = []
    for i_x in range(len(historical_days_back)):
        gain_lines += plt.plot(
            report.Actual_Gain[i_x],
            report.Predict_Gain[i_x],
            "o",
            color=color_list[i_x],
            label=str(historical_days_back[i_x]) + " days",
        )
        plt.plot(report.Actual_Gain[i_x], upper_tick[i_x], "^", color=color_list[i_x])
        plt.plot(report.Actual_Gain[i_x], lower_tick[i_x], "v", color=color_list[i_x])
    plot_range = [
        min(report.Actual_Gain.min(), report.Predict_Gain.min()),
        max(report.Actual_Gain.max(), report.Predict_Gain.max()),
    ]
    gain_lines += plt.plot(plot_range, plot_range, "--", color="r", label="m=1")
    plt.legend(handles=gain_lines)
    plt.xlabel("Actual Gain of %s" % tckr)
    plt.ylabel("Predicted Gain of %s" % tckr)
    plt.title("Prior Accuracy Predicting Today")
    if backward_report is None:
        backward_report = report
    else:
        backward_report = backward_report.append(report, ignore_index=True)
    report_name = "ModelAccuracy.csv"
    backward_report.to_csv(os.path.join(save_dir, report_name))
    plt.tight_layout()
    figure_name = "SummaryChart.png"
    plt.savefig(os.path.join(save_dir, figure_name))


def summarize_option_plays(
    optionDB: OptionData,
    option_strategies: pd.DataFrame,
    today: Optional[pd.Timestamp] = None,
    save_dir: Optional[str] = None,
) -> List[str]:
    if today is None:
        # noinspection PyArgumentList
        today = pd.Timestamp.today()
    if save_dir is None:
        save_dir = os.path.join(default_settings.data_dir, today.strftime("%Y_%m_%d"))

    report_name = "Best_Strategies.csv"
    option_strategies.to_csv(os.path.join(save_dir, report_name))
    # calculate the median - 1 std dev and keep every strategy whose mean
    #  is above that level for each strategy and exDate. I can choose
    #  fewer samples once I know what I'm looking for
    unique_strategies: List[str] = list(set(option_strategies.Strategy))
    unique_exdates = list(set(option_strategies.ExpirationDate))
    keep_index: List[pd.DataFrame.index] = []
    for strategy_name in unique_strategies:
        for exDate in unique_exdates:
            sub_DF = option_strategies.loc[
                np.logical_and(
                    option_strategies.Strategy == strategy_name,
                    option_strategies.ExpirationDate == exDate,
                ),
                :,
            ]
            low_bound = (sub_DF.Median_PF_Return - sub_DF.Std_PF_Return).max()
            keep_index += sub_DF.index[
                np.logical_and(
                    sub_DF.Median_PF_Return > low_bound, sub_DF.Avg_Ann_Return < 50000
                )
            ].tolist()
    option_strategies = option_strategies.loc[keep_index, :]
    option_strategies.sort_values("Avg_PF_Return_PerDay", ascending=False, inplace=True)
    option_strategies.index = range(len(option_strategies.index))
    option_strategies.to_csv(os.path.join(save_dir, report_name))
    mean = option_strategies.Mean_PF_Return
    median = option_strategies.Median_PF_Return
    std = option_strategies.Std_PF_Return
    ann_return = option_strategies.Avg_Ann_Return
    ann_vol = option_strategies.Ann_Volatility
    days = option_strategies.DaysInvested
    win_perc = option_strategies.Win_Percentage
    ann_low = (mean - std) ** (365.0 / days)
    ann_high = (mean + std) ** (365.0 / days)

    fig = plt.figure("Option Strategies", (12, 12))
    ax = fig.subplots(2, 2)

    for strategy_name in unique_strategies:
        select_strategy = option_strategies.Strategy == strategy_name
        plt.sca(ax[0, 0])
        plt.plot(
            days[select_strategy], ann_vol[select_strategy], ".", label=strategy_name
        )
        plt.sca(ax[0, 1])
        plt.plot(
            win_perc[select_strategy],
            ann_return[select_strategy],
            ".",
            label=strategy_name,
        )
        plt.sca(ax[1, 0])
        plt.plot(
            std[select_strategy], ann_return[select_strategy], ".", label=strategy_name
        )
        plt.sca(ax[1, 1])
        plt.errorbar(
            days[select_strategy],
            mean[select_strategy],
            yerr=[
                ann_return[select_strategy] - ann_low[select_strategy],
                ann_high[select_strategy] - ann_return[select_strategy],
            ],
            ls="",
            marker=".",
        )
    plt.sca(ax[0, 0])
    plt.legend()
    plt.xlabel("DaysInvested")
    plt.ylabel("Ann_Volatility")
    # ax[0, 0].set_yscale('log')
    plt.title("Acceleration Risk")
    plt.sca(ax[0, 1])
    plt.legend()
    plt.xlabel("Win_Percentage")
    plt.ylabel("Avg_PF_Return_PerDay")
    plt.title("Strategy Consistency")
    plt.sca(ax[1, 0])
    plt.legend()
    plt.xlabel("Std_PF_Return")
    plt.ylabel("Mean_PF_Return")
    plt.title("Strategy Dist.")
    plt.sca(ax[1, 1])
    plt.xlabel("DaysInvested")
    plt.ylabel("Mean_PF_Return")
    plt.title("Probable Range")

    figure_name = "Strategy_Summaries.png"
    plt.savefig(os.path.join(save_dir, figure_name))
    return unique_strategies


# find the best three records of each strategy for a short term,
#  intermediate, and a long term timeline
def champion_option_strategies(
    option_strategies: pd.DataFrame,
    unique_strategies: List[str],
    today: Optional[pd.Timestamp] = None,
    save_dir: Optional[str] = None,
) -> pd.DataFrame:
    if today is None:
        # noinspection PyArgumentList
        today = pd.Timestamp.today()
    if save_dir is None:
        save_dir = os.path.join(default_settings.data_dir, today.strftime("%Y_%m_%d"))

    champion_list: List[pd.DataFrame] = []
    for strategy_name in unique_strategies:
        select_strategy = option_strategies.Strategy == strategy_name
        # short term
        short_index = np.logical_and(select_strategy, option_strategies.DaysToExp > 4)
        short_index = np.logical_and(short_index, option_strategies.DaysToExp < 30)
        if short_index.sum() > 0:
            temp_DF = option_strategies.loc[short_index.values, :]
            temp_DF.loc[:, "Span"] = "Short"
            temp_DF = temp_DF.sort_values(
                "Avg_Ann_Return", ascending=False, inplace=False
            )
            champion_list.append(temp_DF.iloc[: min(3, temp_DF.shape[0]), :])
        # intermediate term
        middle_index = np.logical_and(select_strategy, option_strategies.DaysToExp > 30)
        middle_index = np.logical_and(middle_index, option_strategies.DaysToExp < 180)
        if middle_index.sum() > 0:
            temp_DF = option_strategies.loc[middle_index.values, :]
            temp_DF.loc[:, "Span"] = "Intermediate"
            temp_DF = temp_DF.sort_values(
                "Avg_Ann_Return", ascending=False, inplace=False
            )
            champion_list.append(temp_DF.iloc[: min(3, temp_DF.shape[0]), :])
        # long term
        long_index = np.logical_and(select_strategy, option_strategies.DaysToExp > 180)
        if long_index.sum() > 0:
            temp_DF = option_strategies.loc[long_index.values, :]
            temp_DF.loc[:, "Span"] = "Long"
            temp_DF = temp_DF.sort_values(
                "Avg_Ann_Return", ascending=False, inplace=False
            )
            champion_list.append(temp_DF.iloc[: min(3, temp_DF.shape[0]), :])
    champion_strategies: pd.DataFrame = pd.concat(champion_list, axis=0)

    condense_cols = [
        "Strategy",
        "Span",
        "ExpirationDate",
        "DaysToExp",
        "CurrentPrice",
        "Prediction_Price",
        "Prediction_Std",
        "Strike_0",
        "Strike_1",
        "Strike_2",
        "Strike_3",
        "Price_0",
        "Price_1",
        "Price_2",
        "Price_3",
        "Premium",
        "Max_Loss",
        "Max_Profit",
        "Median_PF_Return",
        "Mean_PF_Return",
        "Std_PF_Return",
        "Avg_PF_Return_PerDay",
        "Avg_Ann_Return",
        "Expected_Gain_Return",
        "Win_Percentage",
        "Ann_Volatility",
    ]
    print(champion_strategies.loc[:, condense_cols])
    report_name = "Champion_Strategies.csv"
    champion_strategies.to_csv(os.path.join(save_dir, report_name))
    return champion_strategies


if __name__ == "__main__":
    run_market_summary(run_datamine=True, update_databases=True, work_proxy=False)
    # run_market_summary(run_datamine=False, update_databases=False, work_proxy=False)
