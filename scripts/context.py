# -*- coding: utf-8 -*-
import sys
import os

print(__file__)
if os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) not in sys.path:
    print("Adding project root directory to system path")
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

# noinspection PyPep8
import src

# noinspection PyPep8
import config
