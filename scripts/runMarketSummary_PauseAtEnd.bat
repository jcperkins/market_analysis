rem **** convert prompt to conda prompt
call C:\Users\cp035982\bin\Continuum\Scripts\activate.bat C:\Users\cp035982\bin\Continuum\

rem **** move to project directory
call cd "C:\Users\cp035982\Python\Projects\Personal\MarketAnalysis"

rem **** activate environment
call conda activate Markets

rem **** Run the python code that will read and store the SPY and ^SPX option data
python .\scripts\MarketSummary.py

rem **** pause so we can see the exit codes
pause "done...hit a key to exit"