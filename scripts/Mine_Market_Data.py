import datetime
import os
from typing import List, cast, Optional

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

import context
import src.Utility as Util
from config import default_settings
from src.DataSource import DataTools, StockData, OptionData
from src.Model import getDefaultPredictionModel_HDF5, BaseModel_HDF5

# txt = input("Is it a good time to collect options data? [y/n]").upper()
# if "Y" in txt:
def mine_market_data(
    run_datamine: bool = True, update_databases: bool = True, work_proxy: bool = False
) -> pd.DataFrame:
    today = pd.Timestamp.today()
    daily_dir: str = os.path.join(default_settings.data_dir, today.strftime("%Y_%m_%d"))
    tckr: str = "ixSPX"

    if not os.path.exists(daily_dir):
        os.makedirs(daily_dir)
    sDB, oDB, lDB, iDB = (
        DataTools.updateAllDBs(workProxy=work_proxy)
        if update_databases
        else DataTools.initiateAllDataBases(workProxy=work_proxy)
    )
    predictionModel = getDefaultPredictionModel_HDF5(workProxy=work_proxy, stockDB=sDB)

    if run_datamine:
        # oDB.mineFidelityOptions(['.SPX', 'SPY', 'IVV'], loadInBatches=True)
        oDB.mineFidelityOptions([".SPX", "SPY"], loadInBatches=True)

    fid_data: pd.DataFrame = oDB.options_for_date(
        "ixSPX", pd.Timestamp.today(), method="closest"
    )
    fid_data = fid_data.drop_duplicates(subset="Symbol")
