# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 09:37:24 2017

@author: a0225347

Optimization Routines to have running in the background
"""
from MarketAnalysis import (
    MarketBackTest,
    MarketPortfolio_Depreciated,
    MarketData_Depreciated,
    OptionPositions,
)
import src.Utility as MH
from MarketAnalysis.Strategies import RebalanceStock, RollingSPXOption
import ffn

ffn.extend_pandas()


def runOptimize_RollingCalls(
    initialValue, indexOptions, metric, fullOptimization=False
):
    # initialValue=100000; indexOptions=['ixSPX','SPY']; metric='PureCAGR_V1'; fullOptimization=False
    optionType = "Call"
    purchaseSpan = "2y"
    rollAfterSpan = "6m"
    targetStrikeToPriceRatio = 0.84
    maxPfRisk = 0.7
    additionalConditions = []
    positionQty = 4
    targetStockPerc = {"AGG": 1}
    rebalanceFreq = "5y"
    defaultTckrs = {}
    rebalanceThreshold = 0.02
    pfName = None
    startDate = None
    endDate = None
    initialValue = initialValue
    reInvestDiv = True
    brokerageTradeFee = 4.95
    additionalOptionContractFee = 0.15
    printStatusBool = False
    showCharts = False
    fullOptimization = fullOptimization
    fullOptContVarSegQty = 5
    forceIterationPlotting = False
    indexOptions = indexOptions
    metric = metric
    strategyClass = RollingSPXOption.RollingSPXOption

    initArgParms = [
        optionType,
        purchaseSpan,
        rollAfterSpan,
        targetStrikeToPriceRatio,
        maxPfRisk,
        targetStockPerc,
        rebalanceFreq,
        rebalanceThreshold,
    ]
    initKwParms = {
        "defaultTckrs": defaultTckrs,
        "additionalConditions": additionalConditions,
        "positionQty": positionQty,
        "pfName": pfName,
        "indexOptions": indexOptions,
    }
    argOptimizationOrder = [
        (-1, {}),
        (4, {"Values": ["3m", "6m", "1y", "2y"]}),
        (3, {"Values": ["3m", "6m", "1y"]}),
        (1, {"Min": 0.75, "Max": 1.2, "ScoutOrder": 3}),
        (2, {"Min": 0.05, "Max": 0.75, "ScoutOrder": 3}),
    ]
    kwOptmizationOrder = {}
    argParmNames = [
        "optionType",
        "purchaseSpan",
        "rollAfterSpan",
        "targetStrikeToPriceRatio",
        "maxPfRisk",
        "targetStockPerc",
        "rebalanceFreq",
        "rebalanceThreshold",
    ]

    #    sDB,oDB,lDB,iDB = MarketData.initiateAllDataBases(workProxy=True)
    #    #self=bt=MarketBackTest.BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)
    #    bt=BackTest(workProxy=sDB.workProxyBool,stockDB=sDB,optionDB=oDB,liborDB=lDB)
    #    reload(MarketBackTest)
    #    reload(MarketData)
    #    reload(MarketPortfolio)
    #    reload(MH)
    #    reload(RebalanceStock)
    #    reload(RollingSPXOption)
    #    reload(OptionPositions)
    sDB, oDB, lDB, iDB = MarketData_Depreciated.initiateAllDataBases(workProxy=True)
    self = bt = MarketBackTest.Backtest(
        workProxy=sDB.workProxyBool,
        stockDB=sDB,
        optionDB=oDB,
        liborDB=lDB,
        metric=metric,
    )
    strategyArray = [RollingSPXOption.RollingSPXCalls()]
    strategy = strategyArray[0]
    bt.optimizeStrategy(
        strategyClass,
        initArgParms=initArgParms,
        initKwParms=initKwParms,
        argOptimizationOrder=argOptimizationOrder,
        kwOptmizationOrder=kwOptmizationOrder,
        argParmNames=argParmNames,
        startDate=startDate,
        endDate=endDate,
        initialValue=initialValue,
        reInvestDiv=reInvestDiv,
        brokerageTradeFee=brokerageTradeFee,
        additionalOptionContractFee=additionalOptionContractFee,
        printStatusBool=printStatusBool,
        showCharts=showCharts,
        fullOptimization=fullOptimization,
        fullOptContVarSegQty=fullOptContVarSegQty,
        forceIterationPlotting=forceIterationPlotting,
        debug=True,
    )


def runOptimize_LeveragedPortfolio(initialValue, metric, fullOptimization=True):
    startDate = None
    endDate = None
    initialValue = initialValue
    reInvestDiv = True
    brokerageTradeFee = 4.95
    additionalOptionContractFee = 0.15
    printStatusBool = False
    showCharts = False
    fullOptimization = fullOptimization
    fullOptContVarSegQty = 5
    forceIterationPlotting = False

    metric = metric
    strategyClass = RebalanceStock.optimizableLeverage

    initArgParms = []
    initKwParms = {
        "stkPrtn": 0.6,
        "rblncFrq": "6m",
        "rblncThrshld": 0.3,
        "aggPerc": 0.2,
        "bondPerc": 0.2,
        "tltPerc": 0.05,
    }
    argOptimizationOrder = []
    #    kwOptmizationOrder={'stkPrtn':(1,{'Max':1,'Min':0,'ScoutOrder':3}), 'rblncFrq':(2,{'Values':['5y','3y','2y','1y','9m','6m','3m']}), 'rblncThrshld':(3,{'Max':1,'Min':0,'ScoutOrder':3}), 'aggPerc':(6,{'Max':1,'Min':0,'ScoutOrder':3}), 'bondPerc':(5,{'Max':1,'Min':0,'ScoutOrder':5}), 'tltPerc':(4,{'Max':1,'Min':0,'ScoutOrder':3})}
    kwOptmizationOrder = {
        "stkPrtn": (1, {"Max": 0.75, "Min": 0.25, "ScoutOrder": 3}),
        "rblncFrq": (2, {"Values": ["5y", "2.5y", "1y", "6m", "3m"]}),
        "rblncThrshld": (3, {"Max": 1, "Min": 0, "ScoutOrder": 3}),
        "aggPerc": (6, {"Values": [0.2]}),
        "bondPerc": (5, {"Values": [0.2]}),
        "tltPerc": (4, {"Values": [0.05]}),
    }
    argParmNames = []

    sDB, oDB, lDB, iDB = MarketData_Depreciated.initiateAllDataBases(workProxy=True)
    self = bt = MarketBackTest.Backtest(
        workProxy=sDB.workProxyBool,
        stockDB=sDB,
        optionDB=oDB,
        liborDB=lDB,
        metric=metric,
    )
    strategyArray = [RollingSPXOption.RollingSPXCalls()]
    strategy = strategyArray[0]
    bt.optimizeStrategy(
        strategyClass,
        initArgParms=initArgParms,
        initKwParms=initKwParms,
        argOptimizationOrder=argOptimizationOrder,
        kwOptmizationOrder=kwOptmizationOrder,
        argParmNames=argParmNames,
        startDate=startDate,
        endDate=endDate,
        initialValue=initialValue,
        reInvestDiv=reInvestDiv,
        brokerageTradeFee=brokerageTradeFee,
        additionalOptionContractFee=additionalOptionContractFee,
        printStatusBool=printStatusBool,
        showCharts=showCharts,
        fullOptimization=fullOptimization,
        fullOptContVarSegQty=fullOptContVarSegQty,
        forceIterationPlotting=forceIterationPlotting,
        debug=True,
    )


# print("Rolling Call Options",100000,['SPY'],'PureCAGR_V1',True)
# runOptimize_RollingCalls(100000,['SPY'],'PureCAGR_V1',fullOptimization=True)
# print("Leveraged Portfolio",100000,'PureCAGR_V1',False)
# runOptimize_LeveragedPortfolio(100000,'PureCAGR_V1',fullOptimization=False)
# print("Leveraged Portfolio",100000,'PureCAGR_V1',True)
# runOptimize_LeveragedPortfolio(100000,'PureCAGR_V1',fullOptimization=True)


# print(100000,['ixSPX','SPY'],'AnnRet_V1')
# runOptimize_RollingCalls(100000,['SPY'],'AnnRet_V1')
# runOptimize_RollingCalls(100000,['ixSPX','SPY'],'AnnRet_V1')
# print(100000,['ixSPX','SPY'],'SharpeRatio_V1')
# runOptimize_RollingCalls(100000,['SPY'],'SharpeRatio_V1')
# runOptimize_RollingCalls(100000,['ixSPX','SPY'],'SharpeRatio_V1')
# print(100000,['ixSPX','SPY'],'CalmarRatio_V1')
# runOptimize_RollingCalls(100000,['SPY'],'CalmarRatio_V1')
# runOptimize_RollingCalls(100000,['ixSPX','SPY'],'CalmarRatio_V1')
# print(100000,['ixSPX','SPY'],'PureCalmar_V1')
# runOptimize_RollingCalls(100000,['SPY'],'RetDrwDwnProd_V1')
# runOptimize_RollingCalls(100000,['ixSPX','SPY'],'PureCalmar_V1')
print(100000, ["ixSPX", "SPY"], "PureCAGR_V1", True)
runOptimize_RollingCalls(100000, ["ixSPX", "SPY"], "PureCAGR_V1")
print(100000, ["ixSPX", "SPY"], "RetDrwDwnProd_V1")
runOptimize_RollingCalls(100000, ["ixSPX", "SPY"], "RetDrwDwnProd_V1")
print(30000, ["ixSPX", "SPY"], "AnnRet_V1")
runOptimize_RollingCalls(30000, ["SPY"], "AnnRet_V1")
runOptimize_RollingCalls(30000, ["ixSPX", "SPY"], "AnnRet_V1")
print(30000, ["ixSPX", "SPY"], "RetDrwDwnProd_V1")
runOptimize_RollingCalls(30000, ["SPY"], "RetDrwDwnProd_V1")
runOptimize_RollingCalls(30000, ["ixSPX", "SPY"], "RetDrwDwnProd_V1")
print(30000, ["ixSPX", "SPY"], "SharpeRatio_V1")
runOptimize_RollingCalls(30000, ["SPY"], "SharpeRatio_V1")
runOptimize_RollingCalls(30000, ["ixSPX", "SPY"], "SharpeRatio_V1")
print(30000, ["ixSPX", "SPY"], "CalmarRatio_V1")
runOptimize_RollingCalls(30000, ["SPY"], "CalmarRatio_V1")
runOptimize_RollingCalls(30000, ["ixSPX", "SPY"], "CalmarRatio_V1")


runOptimize_RollingCalls(100000, ["ixSPX"], "AnnRet_V1")
runOptimize_RollingCalls(100000, ["ixSPX"], "RetDrwDwnProd_V1")
runOptimize_RollingCalls(100000, ["ixSPX"], "SharpeRatio_V1")
runOptimize_RollingCalls(100000, ["ixSPX"], "CalmarRatio_V1")
runOptimize_RollingCalls(30000, ["ixSPX"], "AnnRet_V1")
runOptimize_RollingCalls(30000, ["ixSPX"], "RetDrwDwnProd_V1")
runOptimize_RollingCalls(30000, ["ixSPX"], "SharpeRatio_V1")
runOptimize_RollingCalls(30000, ["ixSPX"], "CalmarRatio_V1")
