import os
from typing import Dict, NamedTuple

root: str = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))


class UserSettings(NamedTuple):
    root_dir: str = root
    data_dir: str = os.path.abspath(os.path.join(root, "Data"))
    chromedriver: str = os.path.abspath(
        os.path.join(
            root,
            "..",
            "..",
            "..",
            "WebDrivers",
            "chromedriver_win32",
            "chromedriver.exe",
        )
    )
    phantomJSDriver: str = os.path.abspath(
        os.path.join(
            root,
            "..",
            "..",
            "..",
            "WebDrivers",
            "phantomjs-2.1.1-windows",
            "bin",
            "phantomjs.exe",
        )
    )
    work_proxy: bool = False
    proxies: Dict[str, str] = {}
    run_datamine: bool = True
    update_databases: bool = True
    cookieValue: str = "b7thnptblu4mf&b=3&s=v2"
    crumbValue: str = "S9m6uSRL6La"
    fidelity_usr: str = "coryperk"
    fidelity_pwd: str = "Hebrews135"


if __name__ == "__main__":
    UserSettings()
