from .MainSettings import UserSettings

default_settings: UserSettings = UserSettings()
# over-ride default settings here...
# default_settings.work_proxy = True

print(default_settings._fields)
for key in default_settings._fields:
    print("default setting for " + key + " is " + str(getattr(default_settings, key)))
