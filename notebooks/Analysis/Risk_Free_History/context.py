import sys
import os

root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..",".."))
if root_path not in sys.path:
    print("Adding project root directory to system path")
    sys.path.insert(0, root_path)

# noinspection PyPep8,PyUnresolvedReferences
import src
