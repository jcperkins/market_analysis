import sys
import os


if os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) not in sys.path:
    print("Adding project root directory to system path")
    root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", ".."))
    if sys.path[0] != root_dir:
        sys.path.insert(0, root_dir)

# noinspection PyPep8,PyUnresolvedReferences
import src